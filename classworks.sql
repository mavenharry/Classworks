-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2018 at 12:12 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `classworks`
--

-- --------------------------------------------------------

--
-- Table structure for table `affective_domain`
--

CREATE TABLE `affective_domain` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `affective_domain`
--

INSERT INTO `affective_domain` (`id`, `name`) VALUES
(1, 'attendance'),
(2, 'neatness'),
(3, 'punctuality'),
(4, 'friendship'),
(5, 'perseverance'),
(6, 'responsibility'),
(7, 'diligence'),
(8, 'self control'),
(9, 'honesty'),
(10, 'reliability'),
(11, 'initiative'),
(12, 'humility'),
(13, 'attentiveness'),
(14, 'creativity'),
(15, 'organisation'),
(16, 'co-operative'),
(17, 'sensational'),
(18, 'persistence'),
(19, 'endurance'),
(20, 'motivational'),
(21, 'concentration'),
(22, 'agility'),
(23, 'relationship');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `mod_name` text NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `mod_name`, `price`) VALUES
(1, 'Class Management', 200),
(2, 'Subject Management', 100),
(3, 'Human Resource Management', 300),
(4, 'Financial Accounting', 500);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `partner_no` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `profilePhoto` text NOT NULL,
  `nationality` text NOT NULL,
  `sor` text NOT NULL,
  `city` text NOT NULL,
  `gender` text NOT NULL,
  `address` text NOT NULL,
  `number` text NOT NULL,
  `bankName` text NOT NULL,
  `accountNumber` text NOT NULL,
  `accountName` text NOT NULL,
  `subAccountCode` text NOT NULL,
  `date_created` date NOT NULL,
  `account_state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `fullname`, `partner_no`, `password`, `email`, `profilePhoto`, `nationality`, `sor`, `city`, `gender`, `address`, `number`, `bankName`, `accountNumber`, `accountName`, `subAccountCode`, `date_created`, `account_state`) VALUES
(1, 'Abeng Peter', 'Classworks/Ptnr/001', '$2y$10$V019gKgJbPjnOaKyoQGUhu1QHDCyFHk.d11bDTEMjxJihu/Yx/YPO', 'partner@gmail.com', '', 'nigeria', 'Rivers State', 'Portharcourt', 'Male', 'No 1 rumuomasi Portharcourt', '09029302930', 'ACCESS BANK NIGERIA', '0065387616', 'Abeng Emmanuel', 'ACCT_5of41wlmdjpixpf', '2018-08-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL,
  `package` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `per_student` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing`
--

INSERT INTO `pricing` (`id`, `package`, `price`, `per_student`) VALUES
(1, 1, 40000, 200),
(2, 2, 60000, 300),
(3, 3, 100000, 500);

-- --------------------------------------------------------

--
-- Table structure for table `psychomotor_domain`
--

CREATE TABLE `psychomotor_domain` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `psychomotor_domain`
--

INSERT INTO `psychomotor_domain` (`id`, `name`) VALUES
(1, 'Arts and Craft'),
(2, 'Sports and Games'),
(3, 'Musical Skills'),
(4, 'Accuracy'),
(5, 'Physical Activity'),
(6, 'Legibility'),
(7, 'Dexterity');

-- --------------------------------------------------------

--
-- Table structure for table `publicholiday`
--

CREATE TABLE `publicholiday` (
  `id` int(11) NOT NULL,
  `holidayName` text NOT NULL,
  `holidayDate` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publicholiday`
--

INSERT INTO `publicholiday` (`id`, `holidayName`, `holidayDate`) VALUES
(1, 'Democracy Day', '12 June'),
(2, 'Valentine\'s Day', '14 February'),
(3, 'Independence Day', '1 October'),
(4, 'Children\'s Day', '27 May'),
(5, 'Workers Day', '1 May'),
(6, 'Christmas Day', '25 December'),
(7, 'Boxing Day', '26 December'),
(8, 'Mid Term Break', '');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `school_name` text NOT NULL,
  `domain_name` text NOT NULL,
  `admin_name` text NOT NULL,
  `admin_email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `school_name`, `domain_name`, `admin_name`, `admin_email`) VALUES
(1, 'George International Academy', 'GIC', 'Abeng Emmanuel', 'emmanuelabeng27@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `school_partner`
--

CREATE TABLE `school_partner` (
  `id` int(11) NOT NULL,
  `partnerCode` text NOT NULL,
  `schoolName` text NOT NULL,
  `schoolData` text NOT NULL,
  `accountState` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_partner`
--

INSERT INTO `school_partner` (`id`, `partnerCode`, `schoolName`, `schoolData`, `accountState`) VALUES
(1, 'Classworks/Ptnr/001', 'George International Academy', 'clasmpid_GIC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `school_name` varchar(45) DEFAULT NULL,
  `school_domain` text NOT NULL,
  `reg_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `email` text NOT NULL,
  `uniqueNumber` text NOT NULL,
  `password` text NOT NULL,
  `school_domain` text NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `fullname`, `email`, `uniqueNumber`, `password`, `school_domain`, `role`) VALUES
(1, 'Abeng Emmanuel', 'emmanuelabeng27@gmail.com', '2018/Admn/GIC/001', '$2y$10$EsjVqUekx8Xis.L9wQGXseJtBXEcff3uVNayQvNOIK6jmJtkws.xO', 'GIC', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `systemkeys`
--

CREATE TABLE `systemkeys` (
  `id` int(11) NOT NULL,
  `keyName` text NOT NULL,
  `keyValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systemkeys`
--

INSERT INTO `systemkeys` (`id`, `keyName`, `keyValue`) VALUES
(1, 'paystack_public', 'pk_test_08f0d7db01cb29861669d3a1be197e3a61bf0bda'),
(2, 'paystack_secret', 'sk_test_f0517ea6576dcd862ea15e1973ba0e3e51e19a08'),
(3, 'smsAuth', 'hello@classworks.xyz'),
(4, 'smsPass', 'Kny2pdGY9JrR');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `templateName` text NOT NULL,
  `folderName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `templateName`, `folderName`) VALUES
(1, 'template1.png', 'template');

-- --------------------------------------------------------

--
-- Table structure for table `timetable_activities`
--

CREATE TABLE `timetable_activities` (
  `id` int(11) NOT NULL,
  `activityName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetable_activities`
--

INSERT INTO `timetable_activities` (`id`, `activityName`) VALUES
(1, 'Devotion Time'),
(2, 'Class Time'),
(3, 'Sports Time'),
(4, 'Lab Time'),
(5, 'Launch Time'),
(6, 'Break Time');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `activated` int(11) NOT NULL DEFAULT '0',
  `refCode` text NOT NULL,
  `trialCode` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `password`, `activated`, `refCode`, `trialCode`) VALUES
(1, 'Abeng Emmanuel', 'emmanuelabeng27@gmail.com', '$2y$10$EsjVqUekx8Xis.L9wQGXseJtBXEcff3uVNayQvNOIK6jmJtkws.xO', 1, 'Classworks/Ptnr/001', 1234);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `affective_domain`
--
ALTER TABLE `affective_domain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `psychomotor_domain`
--
ALTER TABLE `psychomotor_domain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicholiday`
--
ALTER TABLE `publicholiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_partner`
--
ALTER TABLE `school_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `systemkeys`
--
ALTER TABLE `systemkeys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timetable_activities`
--
ALTER TABLE `timetable_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `affective_domain`
--
ALTER TABLE `affective_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `psychomotor_domain`
--
ALTER TABLE `psychomotor_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `publicholiday`
--
ALTER TABLE `publicholiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `school_partner`
--
ALTER TABLE `school_partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `systemkeys`
--
ALTER TABLE `systemkeys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `timetable_activities`
--
ALTER TABLE `timetable_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
