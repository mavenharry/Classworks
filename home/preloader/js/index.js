$(document).ready( function() {
    up();
});


function up(){
    $("#wing1").css({"transform": "rotate(90deg)"});
    $("#wing2").css({"transform": "rotate(-90deg)"});
    $("#head").css("top", "5px");
    var interval_1 = setInterval(function() {
        clearInterval(interval_1);
        down()
    }, 500);
}


function down(){
    $("#wing1").css({"transform": "rotate(0deg)"});
    $("#wing2").css({"transform": "rotate(0deg)"});
    $("#head").css("top", "0px");
    var interval_2 = setInterval(function() {
        clearInterval(interval_2);
        up()
    }, 500);
}