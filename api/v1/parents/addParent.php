<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;
global $reply;

$default_key = "test";

$api_key = $_POST['apiKey'];

if(isset($_POST["apiKey"]) && !empty($_POST["apiKey"])){

	if(isset($_POST["action"]) && !empty($_POST["action"]) && $_POST["action"] == "addParent"){

		$parentName = $_POST["parentName"];
		$parentEmail = $_POST["email"];
		$parentPhone = $_POST["parentPhone"];
		$parentRel = $_POST["relationship"];
		$accountIds = json_decode($_POST["accountIds"]);
		if(count($accountIds) > 0){
			// $accountIds = $_POST["accountIds"];
			foreach($accountIds as $key => $student){
				if(!empty($student)){
					$accountId = $student;
					$schoolName = explode("/", $accountId)[2];

					if(file_exists("./../../../".$schoolName."/config.php")){
						include_once ("../apibtn.php");

						if($api_key == $default_key){
							// GET PARENT NUMBER
							$parentNumber = $apiClass->newParentNumber();
							
							$reply = $apiClass->addNewParent($parentNumber, $parentName, "nigeria", @$parentState, @$parentCity, $parentRel, @$parentGender, @$parentMaritalStatus, @$parentAddress, @$parentOccupation, $parentPhone, @$parentEmail, @$filename);
							
							$updateStudentPhone = $apiClass->updateStudentPhoneWithParent($parentPhone, $accountId, $parentRel);
						
						}else{
							echo json_encode(
									array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
								);
						} // INCORRECT API KEY
					}else{
						echo json_encode(
								array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
						);
					} // CONFIG ENDS HERE
				}else{
						echo json_encode(
								array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
						);
				} // EMPTY STUDENT ENDS HERE
			} // FOREACH ENDS HERE
			if($reply == "true"){
				echo json_encode(
					array("response_status" => 1, "response_message" => "successful"), JSON_PRETTY_PRINT
				);
			}
		}else{
			//  NO STUDENT SENT
				echo json_encode(
						array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
				);
		}
	}else{
		// INCORRECT ACTION
			echo json_encode(
					array("response_status" => 0, "response_message" => "Invalid action type."), JSON_PRETTY_PRINT
			);
	}
}else{
	// NO API KEY
		echo json_encode(
			array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
		);
}






// if(isset($_GET['accountId'])){

// 	$accountId = $_GET['accountId'];

// 	$schoolName = explode("/", $accountId)[2];

// 	if(file_exists("./../../../".$schoolName."/config.php")){
// 	include_once ("../apibtn.php");
	
// 		if (!empty($api_key) && $api_key == $defualt_key) {

// 		$output_data = $apiClass->getStudentDetails($accountId);
		
// 		if(strtolower($output_data->fullname) != "n/a" && !empty($output_data->fullname) && $output_data->fullname != null){
// 			$output_data->response_status = 1;
// 			$output_data->response_message = "successful";
// 			if(!empty($output_data->profilePhoto)){
// 				$output_data->profilePhoto = "https://www.classworks.xyz/".$schoolName."/passports/".$output_data->profilePhoto;							
// 			}
// 			echo json_encode($output_data, JSON_PRETTY_PRINT);
// 		}else{
// 			echo json_encode(
// 				array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
// 			);
// 		}
		
// 		} else {
// 			echo json_encode(
// 				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
// 			);
// 		}

// 	}else{
// 		echo json_encode(
// 			array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
// 		);
// 	}
	
// }else{

// 	echo json_encode(
// 		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
// 	);

// }

?>