<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$default_key = "test";

$api_key = $_GET['apiKey'];

if(isset($_GET['accountId']) && !empty($_GET["accountId"]) && isset($_GET["action"])){

    $action = htmlentities(trim($_GET["action"]));
    $accountId = htmlentities(trim($_GET['accountId']));
    $schoolName = explode("/", $accountId)[2];

    if(file_exists("./../../../".$schoolName."/config.php")){
        include_once ("../apibtn.php");
        // echo $_SESSION["folderName"];
        
        if(!empty($api_key) && $api_key == $default_key){

            $resp = $apiClass->checkIfExist("students", "id", "student_number", $accountId);

            if($resp === true){

                if($action == "fetchTutor"){

                    $reply = $apiClass->fetchStudentTutors($accountId);
                    $obj = new stdClass();
                    $obj->response_message = "successful";
                    $obj->response_status = 1;
                    $reply[] = $obj;
                    echo json_encode($reply, JSON_PRETTY_PRINT);

                }else{
                    echo json_encode(
                        array("response_status" => 0, "response_message" => "Invalid API Call."), JSON_PRETTY_PRINT
                    );
                }

            }else{
                session_destroy();
                echo json_encode(
                    array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
                );
            }


        }else{
            session_destroy();
            echo json_encode(
				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
			);
        }
    }else{
        session_destroy();
        echo json_encode(
			array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
		);
    }
	
}else{
    session_destroy();
	echo json_encode(
		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
	);

}

?>