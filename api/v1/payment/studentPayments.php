<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$default_key = "test";

$api_key = $_GET['apiKey'];

if(isset($_GET['accountId']) && !empty($_GET["accountId"]) && isset($_GET["action"])){

    $action = htmlentities(trim($_GET["action"]));
    $accountId = htmlentities(trim($_GET['accountId']));
    $schoolName = explode("/", $accountId)[2];

    if(file_exists("./../../../".$schoolName."/config.php")){
        include_once ("../apibtn.php");
        // echo $_SESSION["folderName"];

        $payingSession = $apiClass->getCurrentAcademicSession();
        
        if(!empty($api_key) && $api_key == $default_key){

            $resp = $apiClass->checkIfExist("students", "id", "student_number", $accountId);

            if($resp === true){

                if(isset($_GET["payingTerm"]) && !empty($_GET["payingTerm"])){
                    $payingTerm = htmlentities(trim($_GET["payingTerm"]));
                    
                    if($action == "getPayment"){
        
                        $output_data = $apiClass->getStudentFeesPayment($accountId, $payingSession, $payingTerm);
                        $resp = new stdClass();
                        $resp->response_status = 1;
                        $resp->response_message = "successful";
                        $output_data[] = $resp;
                        session_destroy();
                        echo json_encode($output_data, JSON_PRETTY_PRINT);
                        
                    }

                }else{

                    // INCOMPLETE PARAMETER
                    session_destroy();
                    echo json_encode(
                        array("response_status" => 0, "response_message" => "Incomplete Paramaters."), JSON_PRETTY_PRINT
                    );

                }

            }else{
                session_destroy();
                echo json_encode(
                    array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
                );
            }


        }else{
            session_destroy();
            echo json_encode(
				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
			);
        }
    }else{
        session_destroy();
        echo json_encode(
			array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
		);
    }
	
}else{
    session_destroy();
	echo json_encode(
		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
	);

}

?>