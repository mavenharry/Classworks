<?php

    class paymentClass{

        function getKey($seckey){
            $hashedkey = md5($seckey);
            $hashedkeylast12 = substr($hashedkey, -12);
          
            $seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
            $seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);
          
            $encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
            return $encryptionkey;
          
        }

        function encrypt3Des($data, $key){
            $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
            return base64_encode($encData);
        }

        function payviacard($cardNo, $expDate, $cvv, $amount, $customerEmail){ // set up a function to test card payment.
    
            $splittedExpDate = explode("/", $expDate);
            error_reporting(E_ALL);
            ini_set('display_errors',1);
            
            $data = array('PBFPubKey' => 'FLWPUBK-ef9f21a683a8a5d9923e6e3c2a09f6f9-X',
            'cardno' => $cardNo,
            'currency' => 'NGN',
            'country' => 'NG',
            'cvv' => $cvv,
            'amount' => $amount,
            'expiryyear' => $splittedExpDate[1],
            'expirymonth' => $splittedExpDate[0],
            'suggested_auth' => 'pin',
            'email' => $customerEmail,
            'txRef' => time());
            // 'device_fingerprint' => '69e6b7f0sb72037aa8428b70fbe03986c');
            
            $SecKey = 'FLWSECK-b7e3c2f883fa645a988215f429bc69f9-X';
            
            $key = $this->getKey($SecKey); 
            
            $dataReq = json_encode($data);
            
            $post_enc = $this->encrypt3Des( $dataReq, $key );
        
            var_dump($dataReq);
            
            $postdata = array(
             'PBFPubKey' => 'FLWPUBK-ef9f21a683a8a5d9923e6e3c2a09f6f9-X',
             'client' => $post_enc,
             'alg' => '3DES-24');
            
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/charge");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata)); //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            
            
            $headers = array('Content-Type: application/json');
            
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            
            $request = curl_exec($ch);
            
            if ($request) {
                $result = json_decode($request, true);
                echo "<pre>";
                print_r($result);
            }else{
                if(curl_error($ch))
                {
                    echo 'error:' . curl_error($ch);
                }
            }
            
            curl_close($ch);
        }


    }
?>