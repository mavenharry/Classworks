<?php
session_start();
include_once ("./../../../".$schoolName."/config.php");

class apiClass extends logic {
    

    function setQuizQuestions($question){
        $questionData = json_decode($question);
        $id = $questionData->data->id;
        $subject = $questionData->subject;
        $question = $questionData->data->question;
        $option = json_encode($questionData->data->option);
        $answer = $questionData->data->answer;
        $solution = $questionData->data->solution;
        $examtype = $questionData->data->examtype;
        $examyear = $questionData->data->examyear;
        $stmt2 = $this->dconn->prepare("SELECT * FROM quiz_questions WHERE question_id = ? AND `subject` = ?");
        $stmt2->bind_param("is", $id, $subject);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        if(count($stmt2_result) == 0){
            $stmt = $this->dconn->prepare("INSERT INTO quiz_questions (`question_id`, `subject`, `question`, `options`, `answer`, `solution`, `examtype`, `examyear`) VALUES (?,?,?,?,?,?,?,?)");
            $stmt->bind_param("isssssss", $id, $subject, $question, $option, $answer, $solution, $examtype, $examyear);
            $stmt->execute();
            $stmt->close();
            return "true";
        }
    }


    function verifyClassQuiz($phone, $id){
        $sub_state_zero = 0;
        $sub_state_used = 1;
        $stmt2 = $this->dconn->prepare("SELECT * FROM classquiz_subscribers WHERE phone LIKE CONCAT('%',?,'%') AND sub_state = ?");
        $stmt2->bind_param("si", $phone, $sub_state_zero);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        if(count($stmt2_result) > 0){
            $stmt = $this->dconn->prepare("UPDATE classquiz_subscribers SET `sub_state` = ?, `phone_id` = ? WHERE phone LIKE CONCAT('%',?,'%')");
            $stmt->bind_param("iss", $sub_state_used, $id, $phone);
            $stmt->execute();
            $stmt->close();
            return "true";
        }else{
            $stmt3 = $this->dconn->prepare("SELECT * FROM classquiz_subscribers WHERE phone LIKE CONCAT('%',?,'%') AND phone_id = ?");
            $stmt3->bind_param("ss", $phone, $id);
            $stmt3->execute();
            $stmt3_result = $this->get_result($stmt3);
            if(count($stmt3_result) > 0){
                return "true";
            }
        }
    }


    function getQuestions(){
        $stmt2 = $this->dconn->prepare("SELECT * FROM quiz_questions");
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        $questions = array();
        while ($row = array_shift($stmt2_result)) {
            $questions[] = $row;
        }
        return $questions;
    }


    function submitScore($phone, $score){
        $dates = date("Y-m-d");
        $stmt2 = $this->dconn->prepare("INSERT INTO quiz_scores (`phone`, `score`, `date`) VALUES (?,?,?)");
        $stmt2->bind_param("sss", $phone, $score, $dates);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
    }


    function csvpusher(){
        function setQuizQuestions($data, $id){
            $fullname = $data[0]." ".$data[1];
            $data2 = $data[4];
            if(strlen($data[5]) > 5){
                $data3 = "0".$data[5];
            }else{
                $data3 = $data[5];
            }
            
            $gender = "male";
            $password = "tantua";
            
            if($id < 10){
                echo $student_number = "2018/Prnt/Tantua/00".$id;
            }else{
                if($id < 100){
                    $student_number = "2018/Prnt/Tantua/0".$id;
                }else{
                    $student_number = "2018/Prnt/Tantua/".$id;
                }
            }
    
            $stmt = $this->uconn->prepare("INSERT INTO parents (`fullname`, `guardian_number`, `gender`, `phone`, `email`, `password`) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param("ssssss", $fullname, $student_number, $gender, $data3, $data2, $password);
            $stmt->execute();
            $stmt->close();
            return "true";
        
        }
    }


}

?>
