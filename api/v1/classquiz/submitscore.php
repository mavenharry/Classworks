<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$schoolName = "demo"; //demo

if(file_exists("./../../../".$schoolName."/config.php")){
		
	include_once ("../apibtn.php");
	
	if(isset($_GET['score']) && isset($_GET['apikey']) && $_GET['apikey'] == "maven@7The"){
		$output_data = $apiClass->submitScore($_GET['phone'], $_GET['score']);
	
		if($output_data == true){
			echo json_encode(
				array("response_status" => 1, "response_message" => $_GET['score']), JSON_PRETTY_PRINT
			);
		}else{
			echo json_encode(
				array("response_status" => 0, "response_message" => "questions not found."), JSON_PRETTY_PRINT
			);
		}
	}else{
		echo json_encode(
			array("response_status" => 0, "response_message" => "authentication failed."), JSON_PRETTY_PRINT
		);
	}
	

}else{
	echo json_encode(
		array("response_status" => 0, "response_message" => "Demo school account missing"), JSON_PRETTY_PRINT
	);
}

?>