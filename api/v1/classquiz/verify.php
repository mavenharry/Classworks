<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$schoolName = "demo"; //demo

if(file_exists("./../../../".$schoolName."/config.php")){
		
	include_once ("../apibtn.php");
	
	if(isset($_GET['phone']) && isset($_GET['id'])){
		$phone = $_GET['phone'];
		$id = $_GET['id'];
		$output_data = $apiClass->verifyClassQuiz($phone, $id);
	
		if($output_data == true){
			echo json_encode(
				array("response_status" => 1, "response_message" => $id), JSON_PRETTY_PRINT
			);
		}else{
			echo json_encode(
				array("response_status" => 0, "response_message" => "user not found."), JSON_PRETTY_PRINT
			);
		}
	}else{
		echo json_encode(
			array("response_status" => 0, "response_message" => "authentication parameter missing."), JSON_PRETTY_PRINT
		);
	}
	

}else{
	echo json_encode(
		array("response_status" => 0, "response_message" => "Demo school account missing"), JSON_PRETTY_PRINT
	);
}

?>