<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$default_key = "test";

$api_key = $_POST['apiKey'];


if(isset($_POST['apiKey']) && !empty($_POST['apiKey'])){

    if(isset($_POST["action"]) && $_POST["action"] == "addPickup"){

        if($_POST["apiKey"] == $default_key){

            // VERIFY COMPLETE PARAMETER
            $pickupName = htmlentities(trim($_POST["fullname"]));
            $pickupEmail = htmlentities(trim($_POST["email"]));
            $pickupPhone = htmlentities(trim($_POST["phoneNumber"]));
            $parentName = htmlentities(trim($_POST["parentName"]));
            $accountIds = json_decode($_POST["accountId"]);

            if(count($accountIds) > 0){

                if(!empty($pickupName) && !empty($pickupEmail) && !empty($pickupPhone)){

                    foreach($accountIds as $key => $accountId){
                        $schoolName = explode("/", $accountId)[2];
                        
                        if(file_exists("./../../../".$schoolName."/config.php")){
                            include_once ("../apibtn.php");

                            $resp = $apiClass->checkIfExist("students", "id", "student_number", $accountId);
                            if($reply === true){

                                // ADD NEW PICKUP PERSON
                                $pickupNumber = $apiClass->newPickupNumber();

                                // PROCESS IMAGE
                                if($_FILES["profilePhoto"]["size"] > 0){
                                    if($_FILES["profilePhoto"]["error"]){
                                        $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
                                    }else{
                                        $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
                                        $imageFolder = "./../../../".$schoolName."/passports";
                                        $filename = str_replace("/","_",$pickupNumber).".".$extension;
                                        if(file_exists($imageFolder."/".$filename)){
                                            unlink($imageFolder."/".$filename);
                                        }
                                        move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);
                            
                                    }
                                }

                                $reply = $apiClass->addNewPickup($pickupNumber, $pickupName, "", "", "", "", "", "", "", "", $pickupPhone, $pickupEmail, $accountId, @$filename, $parentName);

                            }else{
                                session_destroy();
                                echo json_encode(
                                    array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
                                );
                            }

                        }else{
                            session_destroy();
                            echo json_encode(
                                array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
                            );
                        }
                    } //FOREACH ENDS HERE

                    echo json_encode(
                        array("response_status" => 1, "response_message" => "successful"), JSON_PRETTY_PRINT
                    );
                }else{
                    echo json_encode(
                        array("response_status" => 0, "response_message" => "Incomplete Parameter."), JSON_PRETTY_PRINT
                    );
                }

            }else{
                echo json_encode(
                        array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
                );
            }

        }else{
            session_destroy();
            echo json_encode(
				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
			);
        }

    }else{
        // WRONG CALL
        echo json_encode(
            array("response_status" => 0, "response_message" => "Invalid API Call."), JSON_PRETTY_PRINT
        );
    }
}else{
    session_destroy();
    echo json_encode(
        array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
    );
}






// if(isset($_POST['accountId']) && !empty($_POST["accountId"]) && isset($_POST["action"])){

//     $action = htmlentities(trim($_POST["action"]));
//     $accountId = htmlentities(trim($_POST['accountId']));
//     $schoolName = explode("/", $accountId)[2];

//     if(file_exists("./../../../".$schoolName."/config.php")){
//         include_once ("../apibtn.php");
//         // echo $_SESSION["folderName"];
        
//         if(!empty($api_key) && $api_key == $default_key){

//             $resp = $apiClass->checkIfExist("students", "id", "student_number", $accountId);

//             if($resp === true){

//                 if($action == "addPickup"){
//                     // VERIFY COMPLETE PARAMETER
//                     $pickupName = htmlentities(trim($_POST["fullname"]));
//                     $pickupEmail = htmlentities(trim($_POST["email"]));
//                     $pickupPhone = htmlentities(trim($_POST["phoneNumber"]));
//                     $parentName = htmlentities(trim($_POST["parentName"]));
//                     if(!empty($pickupName) && !empty($pickupEmail) && !empty($pickupPhone)){
//                         // ADD NEW PICKUP PERSON
//                         $pickupNumber = $apiClass->newPickupNumber();

//                         // PROCESS IMAGE
//                         if($_FILES["profilePhoto"]["size"] > 0){
//                             if($_FILES["profilePhoto"]["error"]){
//                                 $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
//                             }else{
//                                 $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
//                                 $imageFolder = "./../../../".$schoolName."/passports";
//                                 $filename = str_replace("/","_",$pickupNumber).".".$extension;
//                                 if(file_exists($imageFolder."/".$filename)){
//                                     unlink($imageFolder."/".$filename);
//                                 }
//                                 move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);
                    
//                             }
//                         }

//                         $reply = $apiClass->addNewPickup($pickupNumber, $pickupName, "", "", "", "", "", "", "", "", $pickupPhone, $pickupEmail, $accountId, @$filename, $parentName);
//                         // echo $reply;
//                         if($reply == "true"){
//                             $output_data = new stdClass();
//                             $output_data->pickupNumber = $pickupNumber;
//                             $output_data->profilePhoto = "https://www.classworks.xyz/".$schoolName."/passports/".$filename;
//                             $output_data->response_status = 1;
//                             $output_data->response_message = "successful";
//                             echo json_encode($output_data, JSON_PRETTY_PRINT);
//                         }else{
//                             echo json_encode(
//                                 array("response_status" => 0, "response_message" => "Server Error."), JSON_PRETTY_PRINT
//                             );
//                         }
//                     }else{
//                         // INCOMPLETE PARAMETER
//                         echo json_encode(
//                             array("response_status" => 0, "response_message" => "Incomplete Parameter."), JSON_PRETTY_PRINT
//                         );
//                     }
//                 }else{
//                     // WRONG CALL
//                     echo json_encode(
//                         array("response_status" => 0, "response_message" => "Invalid API Call."), JSON_PRETTY_PRINT
//                     );
//                 }

//             }else{
//                 session_destroy();
//                 echo json_encode(
//                     array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
//                 );
//             }


//         }else{
//             session_destroy();
//             echo json_encode(
// 				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
// 			);
//         }
//     }else{
//         session_destroy();
//         echo json_encode(
// 			array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
// 		);
//     }
	
// }else{
//     session_destroy();
// 	echo json_encode(
// 		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
// 	);

// }

?>