<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$defualt_key = "test";

$api_key = $_GET['apiKey'];

if(isset($_GET['accountId'])){

	$accountId = $_GET['accountId'];

	$schoolName = explode("/", $accountId)[2];

	if(file_exists("./../../../".$schoolName."/config.php")){
	include_once ("../apibtn.php");
	
		if (!empty($api_key) && $api_key == $defualt_key) {

		$output_data = $apiClass->getStudentDetails($accountId);
		
		if(strtolower($output_data->fullname) != "n/a" && !empty($output_data->fullname) && $output_data->fullname != null){
			$output_data->response_status = 1;
			$output_data->response_message = "successful";
			if(!empty($output_data->profilePhoto)){
				$output_data->profilePhoto = "https://www.classworks.xyz/".$schoolName."/passports/".$output_data->profilePhoto;							
			}
			echo json_encode($output_data, JSON_PRETTY_PRINT);
		}else{
			echo json_encode(
				array("response_status" => 0, "response_message" => "Student not found."), JSON_PRETTY_PRINT
			);
		}
		
		} else {
			echo json_encode(
				array("response_status" => 0, "response_message" => "Invalid API Key."), JSON_PRETTY_PRINT
			);
		}

	}else{
		echo json_encode(
			array("response_status" => 0, "response_message" => "Invalid Registration number."), JSON_PRETTY_PRINT
		);
	}
	
}else{

	echo json_encode(
		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
	);

}

?>