<<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
	<script src="main.js"></script>
</head>
<body>
<?php

// error_reporting(0);

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

global $schoolName;

$schoolName = "demo"; //demo

if(file_exists("./../../../".$schoolName."/config.php")){
		
include_once ("../apibtn.php");

	$subject = $_GET["subject"];
	$exam = "";
	$year = "";

	for ($i=0; $i < 50; $i++) { 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://questions.aloc.ng/api/q?subject='.$subject);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$apiClass->setQuizQuestions($result);
	}

	// $file_items = file('./tantua.csv');
	// $count = 1;
	// foreach($file_items as $item) {
	// 	$item_arr = explode(',', $item);
	// 	$apiClass->setQuizQuestions($item_arr, $count);
	// 	$count = $count + 1;
	// }

}else{

	echo json_encode(
		array("response_status" => 0, "response_message" => "Account ID missing."), JSON_PRETTY_PRINT
	);

}

?>
</body>
</html>