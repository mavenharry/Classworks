<?php
    // $ini_array = parse_ini_file("../../includes/.env");
    include_once ("db.php");
    

    class logic {
        
        private $defaultHost = host;
        private $defaultUser = user;
        private $defaultDatabase = database;
        private $defaultPassword = password;
        // private $defaultHost = "localhost";
        // private $defaultUser = "root";
        // private $defaultDatabase = "classworks";
        // private $defaultPassword = "";
        public $schoolFolder;
        private $ini_array;
        public $uconn;
        public $dconn;
        private $error;
        private $keys = array();
        public $path;


        function outputRefine($value){
            if(isset($value) && !empty($value)){
                return $value;
            }else{
                return "N/A";
            }
        }


        function removeAccents($rawData){
            $unwanted_array = array(    '&amp;' => 'and', '&' => 'and', '@' => 'at', '©' => 'c', '®' => 'r', 
            '̊'=>'','̧'=>'','̨'=>'','̄'=>'','̱'=>'',
            'Á'=>'a','á'=>'a','À'=>'a','à'=>'a','Ă'=>'a','ă'=>'a','ắ'=>'a','Ắ'=>'A','Ằ'=>'A',
            'ằ'=>'a','ẵ'=>'a','Ẵ'=>'A','ẳ'=>'a','Ẳ'=>'A','Â'=>'a','â'=>'a','ấ'=>'a','Ấ'=>'A',
            'ầ'=>'a','Ầ'=>'a','ẩ'=>'a','Ẩ'=>'A','Ǎ'=>'a','ǎ'=>'a','Å'=>'a','å'=>'a','Ǻ'=>'a',
            'ǻ'=>'a','Ä'=>'a','ä'=>'a','ã'=>'a','Ã'=>'A','Ą'=>'a','ą'=>'a','Ā'=>'a','ā'=>'a',
            'ả'=>'a','Ả'=>'a','Ạ'=>'A','ạ'=>'a','ặ'=>'a','Ặ'=>'A','ậ'=>'a','Ậ'=>'A','Æ'=>'ae',
            'æ'=>'ae','Ǽ'=>'ae','ǽ'=>'ae','ẫ'=>'a','Ẫ'=>'A',
            'Ć'=>'c','ć'=>'c','Ĉ'=>'c','ĉ'=>'c','Č'=>'c','č'=>'c','Ċ'=>'c','ċ'=>'c','Ç'=>'c','ç'=>'c',
            'Ď'=>'d','ď'=>'d','Ḑ'=>'D','ḑ'=>'d','Đ'=>'d','đ'=>'d','Ḍ'=>'D','ḍ'=>'d','Ḏ'=>'D','ḏ'=>'d','ð'=>'d','Ð'=>'D',
            'É'=>'e','é'=>'e','È'=>'e','è'=>'e','Ĕ'=>'e','ĕ'=>'e','ê'=>'e','ế'=>'e','Ế'=>'E','ề'=>'e',
            'Ề'=>'E','Ě'=>'e','ě'=>'e','Ë'=>'e','ë'=>'e','Ė'=>'e','ė'=>'e','Ę'=>'e','ę'=>'e','Ē'=>'e',
            'ē'=>'e','ệ'=>'e','Ệ'=>'E','Ə'=>'e','ə'=>'e','ẽ'=>'e','Ẽ'=>'E','ễ'=>'e',
            'Ễ'=>'E','ể'=>'e','Ể'=>'E','ẻ'=>'e','Ẻ'=>'E','ẹ'=>'e','Ẹ'=>'E',
            'ƒ'=>'f',
            'Ğ'=>'g','ğ'=>'g','Ĝ'=>'g','ĝ'=>'g','Ǧ'=>'G','ǧ'=>'g','Ġ'=>'g','ġ'=>'g','Ģ'=>'g','ģ'=>'g',
            'H̲'=>'H','h̲'=>'h','Ĥ'=>'h','ĥ'=>'h','Ȟ'=>'H','ȟ'=>'h','Ḩ'=>'H','ḩ'=>'h','Ħ'=>'h','ħ'=>'h','Ḥ'=>'H','ḥ'=>'h',
            'Ỉ'=>'I','Í'=>'i','í'=>'i','Ì'=>'i','ì'=>'i','Ĭ'=>'i','ĭ'=>'i','Î'=>'i','î'=>'i','Ǐ'=>'i','ǐ'=>'i',
            'Ï'=>'i','ï'=>'i','Ḯ'=>'I','ḯ'=>'i','Ĩ'=>'i','ĩ'=>'i','İ'=>'i','Į'=>'i','į'=>'i','Ī'=>'i','ī'=>'i',
            'ỉ'=>'I','Ị'=>'I','ị'=>'i','Ĳ'=>'ij','ĳ'=>'ij','ı'=>'i',
            'Ĵ'=>'j','ĵ'=>'j',
            'Ķ'=>'k','ķ'=>'k','Ḵ'=>'K','ḵ'=>'k',
            'Ĺ'=>'l','ĺ'=>'l','Ľ'=>'l','ľ'=>'l','Ļ'=>'l','ļ'=>'l','Ł'=>'l','ł'=>'l','Ŀ'=>'l','ŀ'=>'l',
            'Ń'=>'n','ń'=>'n','Ň'=>'n','ň'=>'n','Ñ'=>'N','ñ'=>'n','Ņ'=>'n','ņ'=>'n','Ṇ'=>'N','ṇ'=>'n','Ŋ'=>'n','ŋ'=>'n',
            'Ó'=>'o','ó'=>'o','Ò'=>'o','ò'=>'o','Ŏ'=>'o','ŏ'=>'o','Ô'=>'o','ô'=>'o','ố'=>'o','Ố'=>'O','ồ'=>'o',
            'Ồ'=>'O','ổ'=>'o','Ổ'=>'O','Ǒ'=>'o','ǒ'=>'o','Ö'=>'o','ö'=>'o','Ő'=>'o','ő'=>'o','Õ'=>'o','õ'=>'o',
            'Ø'=>'o','ø'=>'o','Ǿ'=>'o','ǿ'=>'o','Ǫ'=>'O','ǫ'=>'o','Ǭ'=>'O','ǭ'=>'o','Ō'=>'o','ō'=>'o','ỏ'=>'o',
            'Ỏ'=>'O','Ơ'=>'o','ơ'=>'o','ớ'=>'o','Ớ'=>'O','ờ'=>'o','Ờ'=>'O','ở'=>'o','Ở'=>'O','ợ'=>'o','Ợ'=>'O',
            'ọ'=>'o','Ọ'=>'O','ọ'=>'o','Ọ'=>'O','ộ'=>'o','Ộ'=>'O','ỗ'=>'o','Ỗ'=>'O','ỡ'=>'o','Ỡ'=>'O',
            'Œ'=>'oe','œ'=>'oe',
            'ĸ'=>'k',
            'Ŕ'=>'r','ŕ'=>'r','Ř'=>'r','ř'=>'r','ṙ'=>'r','Ŗ'=>'r','ŗ'=>'r','Ṛ'=>'R','ṛ'=>'r','Ṟ'=>'R','ṟ'=>'r',
            'S̲'=>'S','s̲'=>'s','Ś'=>'s','ś'=>'s','Ŝ'=>'s','ŝ'=>'s','Š'=>'s','š'=>'s','Ş'=>'s','ş'=>'s',
            'Ṣ'=>'S','ṣ'=>'s','Ș'=>'S','ș'=>'s',
            'ſ'=>'z','ß'=>'ss','Ť'=>'t','ť'=>'t','Ţ'=>'t','ţ'=>'t','Ṭ'=>'T','ṭ'=>'t','Ț'=>'T',
            'ț'=>'t','Ṯ'=>'T','ṯ'=>'t','™'=>'tm','Ŧ'=>'t','ŧ'=>'t',
            'Ú'=>'u','ú'=>'u','Ù'=>'u','ù'=>'u','Ŭ'=>'u','ŭ'=>'u','Û'=>'u','û'=>'u','Ǔ'=>'u','ǔ'=>'u','Ů'=>'u','ů'=>'u',
            'Ü'=>'u','ü'=>'u','Ǘ'=>'u','ǘ'=>'u','Ǜ'=>'u','ǜ'=>'u','Ǚ'=>'u','ǚ'=>'u','Ǖ'=>'u','ǖ'=>'u','Ű'=>'u','ű'=>'u',
            'Ũ'=>'u','ũ'=>'u','Ų'=>'u','ų'=>'u','Ū'=>'u','ū'=>'u','Ư'=>'u','ư'=>'u','ứ'=>'u','Ứ'=>'U','ừ'=>'u','Ừ'=>'U',
            'ử'=>'u','Ử'=>'U','ự'=>'u','Ự'=>'U','ụ'=>'u','Ụ'=>'U','ủ'=>'u','Ủ'=>'U','ữ'=>'u','Ữ'=>'U',
            'Ŵ'=>'w','ŵ'=>'w',
            'Ý'=>'y','ý'=>'y','ỳ'=>'y','Ỳ'=>'Y','Ŷ'=>'y','ŷ'=>'y','ÿ'=>'y','Ÿ'=>'y','ỹ'=>'y','Ỹ'=>'Y','ỷ'=>'y','Ỷ'=>'Y',
            'Z̲'=>'Z','z̲'=>'z','Ź'=>'z','ź'=>'z','Ž'=>'z','ž'=>'z','Ż'=>'z','ż'=>'z','Ẕ'=>'Z','ẕ'=>'z',
            'þ'=>'p','ŉ'=>'n','А'=>'a','а'=>'a','Б'=>'b','б'=>'b','В'=>'v','в'=>'v','Г'=>'g','г'=>'g','Ґ'=>'g','ґ'=>'g',
            'Д'=>'d','д'=>'d','Е'=>'e','е'=>'e','Ё'=>'jo','ё'=>'jo','Є'=>'e','є'=>'e','Ж'=>'zh','ж'=>'zh','З'=>'z','з'=>'z',
            'И'=>'i','и'=>'i','І'=>'i','і'=>'i','Ї'=>'i','ї'=>'i','Й'=>'j','й'=>'j','К'=>'k','к'=>'k','Л'=>'l','л'=>'l',
            'М'=>'m','м'=>'m','Н'=>'n','н'=>'n','О'=>'o','о'=>'o','П'=>'p','п'=>'p','Р'=>'r','р'=>'r','С'=>'s','с'=>'s',
            'Т'=>'t','т'=>'t','У'=>'u','у'=>'u','Ф'=>'f','ф'=>'f','Х'=>'h','х'=>'h','Ц'=>'c','ц'=>'c','Ч'=>'ch','ч'=>'ch',
            'Ш'=>'sh','ш'=>'sh','Щ'=>'sch','щ'=>'sch','Ъ'=>'-',
            'ъ'=>'-','Ы'=>'y','ы'=>'y','Ь'=>'-','ь'=>'-',
            'Э'=>'je','э'=>'je','Ю'=>'ju','ю'=>'ju','Я'=>'ja','я'=>'ja','א'=>'a','ב'=>'b','ג'=>'g','ד'=>'d','ה'=>'h','ו'=>'v',
            'ז'=>'z','ח'=>'h','ט'=>'t','י'=>'i','ך'=>'k','כ'=>'k','ל'=>'l','ם'=>'m','מ'=>'m','ן'=>'n','נ'=>'n','ס'=>'s','ע'=>'e',
            'ף'=>'p','פ'=>'p','ץ'=>'C','צ'=>'c','ק'=>'q','ר'=>'r','ש'=>'w','ת'=>'t'
            );

            return $accentsRemoved = strtr($rawData , $unwanted_array);
        }


        function ordinalSuffix( $n ){
        return $n.date( 'S', mktime( 1, 1, 1, 1, ( (($n>=10)+($n%100>=20)+($n==0))*10 + $n%10)));
        }
        
        function getallClassStudents($class_name){
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE present_class = ?");
            $stmt->bind_param("s", $class_name);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $studentArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $studentArray[] = $obj;
            }
            $stmt->close();
            return $studentArray;
        }


        function fetchAllParent(){
            $stmt = $this->uconn->prepare("SELECT * FROM parents");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $parentArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->phone = $this->outputRefine($row["phone"]);
                $obj->email = $this->outputRefine($row["email"]);
                $obj->relationship = $this->outputRefine($row["relation_student"]);
                $obj->profilePhoto = $row["ProfilePhoto"];
                $parentArray[] = $obj;
            }
            $stmt->close();
            return $parentArray;
        }



        function fetchSingleParent($parent_id){
            $stmt = $this->uconn->prepare("SELECT * FROM parents WHERE guardian_number = ?");
            $stmt->bind_param("s", $parent_id);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj = new stdClass();
            while($row = array_shift($stmt_result)){
                $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->phone = $this->outputRefine($row["phone"]);
                $obj->email = $this->outputRefine($row["email"]);
                $obj->nationality = $this->outputRefine($row["nationality"]);
                $obj->state = $this->outputRefine($row["state"]);
                $obj->city = $this->outputRefine($row["city"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->m_status = $this->outputRefine($row["m_status"]);
                $obj->occupation = $this->outputRefine($row["occupation"]);
                $obj->address = $this->outputRefine($row["address"]);                                
                $obj->relationship = $this->outputRefine($row["relation_student"]);
                $obj->profilePhoto = $row["ProfilePhoto"];
                $obj->appAccess =$row["app_access"];
            }
            $stmt->close();
            return $obj;
        }


        function fetchAllParentStudent($parent_id){
            $stmt1 = $this->uconn->prepare("SELECT phone FROM parents WHERE guardian_number = ?");
            $stmt1->bind_param("s", $parent_id);
            $stmt1->execute();
            $stmt1_result = $this->get_result($stmt1);
            $row1 = array_shift($stmt1_result);
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE father_phone = ? OR mother_phone = ?");
            $stmt->bind_param("ss", $row1["phone"], $row1["phone"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $studentArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $studentArray[] = $obj;
            }
            $stmt->close();
            return $studentArray;
        }


        function getallClassSubjects($class_name){
            $stmt = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectClass = ?");
            $stmt->bind_param("s", $class_name);            
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subjectArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->subject_code = $this->outputRefine($row["subjectCode"]);
                $obj->subject_name = $this->outputRefine($row["subjectName"]);
                $obj->subject_class = $this->outputRefine($row["subjectClass"]);
                $subjectArray[] = $obj;
            }
            $stmt->close();
            return $subjectArray;
        }
        

        function getGradingSystem(){
            $stmt = $this->uconn->prepare("SELECT * FROM grading_system");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $gradeArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->from = $row["froms"];
                $obj->to = $row["tos"];
                $obj->grade = $this->outputRefine($row["grade"]);
                $obj->remark = $this->outputRefine($row["remark"]);
                $gradeArray[] = $obj;
            }
            $stmt->close();
            return $gradeArray;
        }


        public function __construct(){
            $pageFolder = $_SERVER['REQUEST_URI'];
            $pageSplit = explode("/", $pageFolder);
            $count = count($pageSplit);
            if($count > 3 && $pageSplit[$count -3] == "views"){
                $path = "../../".$_SESSION["folderName"]."/.env";
            }else{
                $path = ".env";
            }
            $this->verifyAccess();
            $this->ini_array = parse_ini_file($path);
            $this->defaultConnection();
            $this->userConnection($this->ini_array);
            $this->getSystemKeys();
            $this->checkRenewalState();
            $this->isSchoolStaff();
        }

        private function verifyAccess(){
            if(!isset($_SESSION["fullname"])){
                $pageFolder = $_SERVER['REQUEST_URI'];
                $pageSplit = explode("/", $pageFolder);
                if($pageSplit[2] == "views"){
                    if($pageSplit[3] != "admission" && $pageSplit[4] != "checkResult" && $pageSplit[4] != "fees_payment" && explode("?", $pageSplit[4])[0] != "report_sheet" && explode("=", explode("?", $pageSplit[4])[1])[0] != "allow" && explode("=", explode("?", $pageSplit[4])[1])[0] != "allow"){
                        // PAGES TO ALLOW
                        //echo "no access";
                        echo "<script>window.location.assign('../../error')</script>";
                    }
                }
            }
        }

        private function isSchoolStaff(){
            if(isset($_SESSION["loggedInSchool"]) && $_SESSION["loggedInSchool"] != $_SESSION["folderName"]){
                //session_destroy();
                $pageFolder = $_SERVER['REQUEST_URI'];
                $pageSplit = explode("/", $pageFolder);
                $count = count($pageSplit);
                 if($count > 3 && $pageSplit[$count -3] == "views"){
                //     // SOMEWHERE IN VIEWS BUT NOT RENEW PAGE
                     echo "<script>window.location.assign('../authentication/')</script>";
                 }
            }
        }

        private function checkRenewalState(){
            if(empty($_GET)){
                $nextSubDate = $this->getSchoolDetails()->nextSubDate;
                $today = Date("Y-m-d");
                if($today > $nextSubDate){
                    $_SESSION["fullname"] = "Allow";
                    $pageFolder = $_SERVER['REQUEST_URI'];
                    $pageSplit = explode("/", $pageFolder);
                    $count = count($pageSplit);
                    if($count > 3 && $pageSplit[$count - 3] == "views" && $pageSplit[$count - 1] == "renew"){
                        // IN RENEW PAGE
                    }else{
                        if($count <= 3 && $pageSplit[$count - 1] == "" || $pageSplit[$count - 1] == "index.php" || $pageSplit[$count - 1] == "index" || $pageSplit[$count - 1] == "login.php" || $pageSplit[$count - 1] == "login"){
                            // IN INDEX PAGE
                            $pageName = $_SERVER["DOCUMENT_ROOT"]."/views/settings/renew.php";
                            echo "<script>window.location.assign('$pageName')</script>";
                        }else{
                            if($count > 3 && $pageSplit[$count -3] == "views" && $pageSplit[$count - 1] != "renew"){
                                // SOMEWHERE IN VIEWS BUT NOT RENEW PAGE
                                echo "<script>window.location.assign('../settings/renew')</script>";
                            }else{
                                $pageName = $_SERVER["DOCUMENT_ROOT"]."/views/settings/renew.php";
                                echo "<script>window.location.assign('$pageName')</script>";    
                            }
                        }
                    }
                }
            }
        }


        private function defaultConnection(){
            $this->dconn = new mysqli($this->defaultHost, $this->defaultUser, $this->defaultPassword, $this->defaultDatabase);
            if(!$this->dconn){
                $this->error = "Fatal Error: Can't connect to database ".$this->dconn->connect_error;
                return false;
            }
        }

        private function getSystemKeys(){
            $stmt = $this->dconn->prepare("SELECT * FROM systemkeys");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $this->keys[$row["keyName"]] = $row["keyValue"];
            }
        }

        private function userConnection($ini_array){
             if(empty($ini_array["user"])){
                //  DO NOTHING
             }else{
                 $_SESSION["folderName"] = $ini_array["folderName"];
                $this->schoolFolder = $ini_array["folderName"];
                $this->uconn = new mysqli($ini_array["host"], $ini_array["user"], $ini_array["password"], $ini_array["database"]);
                if(!$this->uconn){
                    $this->error = "Fatal Error: Can't connect to database ".$this->uconn->connect_error;
                    return false;
                }
             }
        }

        function get_result( $Statement ) {
            $RESULT = array();
            $Statement->store_result();
            for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
                $Metadata = $Statement->result_metadata();
                $PARAMS = array();
                while ( $Field = $Metadata->fetch_field() ) {
                    $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
                }
                call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
                $Statement->fetch();
            }
            $object = (object) $RESULT;
            return $RESULT;
        }

        function login($unique, $password, $uniqueType){
            $stmt = $this->uconn->prepare("SELECT * FROM administrators WHERE email = ? OR adminNumber = ?");
            $stmt->bind_param("ss", $unique, $unique);
            if($stmt->execute()){
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    // FOUND IN ADMIN TABLE
                    $row = array_shift($stmt_result);
                    if(password_verify($password, $row["password"])){
                        $obj = new stdClass();
                        $obj->fullname = $row["fullname"];
                        $obj->role = "admin";
                        $obj->uniqueNumber = $row["adminNumber"];
                        $obj->email = $row["email"];
                        return $obj;
                    }else{
                        return "password";
                    }
                }else{
                    // CHECK TEACHERS
                    $stmt = $this->uconn->prepare("SELECT * FROM teachers WHERE email = ? OR staff_number = ?");
                    $stmt->bind_param("ss", $unique, $unique);
                    if($stmt->execute()){
                        $stmt_result = $this->get_result($stmt);
                        if(count($stmt_result) > 0){
                            // FOUND IN TEACHERS
                            $row = array_shift($stmt_result);
                            if(password_verify($password, $row["password"])){
                                $obj = new stdClass();
                                $obj->fullname = $row["fullname"];
                                $obj->role = $row["department"];
                                $obj->uniqueNumber = $row["staff_number"];
                                $obj->email = $row["email"];
                                $stmt->close();
                                return $obj;
                            }else{
                                return "password";
                            }
                        }else{
                            // NOT FOUND
                            return "not found";
                        }
                    }else{
                        // COULDNT RUN CHECK ON TEACHERS TABLE
                        $stmt->close();
                        return "error";
                    }
                }
            }else{
                // COULDNT RUN CHECK ON ADMINISTRATOR TABLE
                $stmt->close();
                return "error";
            }
        }

        function getIndexStudents(){
            $stmt = $this->uconn->prepare("SELECT MAX(id) AS max_id, MIN(id) AS min_id FROM students");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $max = $row["max_id"];
            $min = $row["min_id"];
            $random = rand($min, $max);
            $stmt = $this->uconn->prepare("SELECT profilePhoto, fullname, present_class FROM students WHERE id >= ? AND profilePhoto != NULL LIMIT 0, 4");
            $stmt->bind_param("i", $random);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $student = array();
            if(count($stmt_result) == 0){
                $stmt->close();
                return $student;
            }else{
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->fullname = $row["fullname"];
                    $obj->presentClass = $row["present_class"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $student[] = $obj;
                }
                $stmt->close();
                return $student;
            }
        }

        public function getSchoolDetails(){
            $stmt = $this->uconn->prepare("SELECT * FROM settings");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->schoolFolder = $this->schoolFolder;
            $obj->school_name = $row["school_name"];
            $obj->school_address = $row["schoolAddress"];
            $obj->state = $row["state"];
            $obj->country = $row["country"];
            $obj->city = $row["city"];
            $obj->logo = "SchoolImages/".$row["logo"];
            $obj->stamp = "SchoolImages/".$row["stamp_name"];
            $obj->domain_name = $row["domain_name"];
            $obj->pathToPassport = "../../".$this->schoolFolder."/passports";
            $obj->init_slider = $row["init_slider"];
            $obj->staffAttendanceState = $row["staff_attendance_state"];
            $obj->staffLatenessStart = $row["staff_lateness_start"];
            $obj->latenessFee = $row["lateness_fee"];
            $obj->absentFee = $row["absent_fee"];
            $obj->studentAttendanceState = $row["student_attendance_state"];
            $obj->studentLatenessStart = $row["student_lateness_start"];
            $obj->absentDays = $row["absent_days"];
            $obj->parentDailyAlert = $row["parent_daily_alert"];
            $obj->attendanceMessage = $row["attendanceMessage"];
            $obj->studentChildPickupState = $row["child_pickup_state"];
            $obj->pickupPersons = $row["max_pickup_person"];
            $obj->parentAlertOnPickup = $row["pickup_alert"];
            $obj->pickupMessage = $row["pickupMessage"];
            $obj->addPickupPersons = json_decode($row["add_pickup"], JSON_PRETTY_PRINT);
            $obj->verifyPickup = json_decode($row["verify_pickup"], JSON_PRETTY_PRINT);
            $obj->category = json_decode($row["categories"], JSON_PRETTY_PRINT);
            $obj->email = $row["email"];
            $obj->slogan = $row["slogan"];
            $obj->website = $row["website"];
            $obj->phone = $row["telephone"];
            $obj->currency = $row["currency"];
            $obj->grading = $row["grading_system"];
            $obj->color = $row["school_color"];
            $obj->curriculum = $row["school_curriculum"];
            $obj->modules = $row["modules"];
            $obj->phone = $row["telephone"];
            $obj->bankName = $row["bank_name"];
            $obj->acctNumber = $row["account_number"];
            $obj->acctName = $row["account_name"];
            $obj->disburseState = $row["disbursement_state"];
            $obj->disburseDate = $row["disbursement_date"];
            $obj->disburseNumber = $row["disbursement_number"];
            $obj->deductLateness = $row["deduct_lateness"];
            $obj->deductAbsent = $row["deduct_absent"];
            $obj->autoAlert = $row["auto_alert_outstanding"];
            $obj->alertTime = $row["alert_time"];
            $obj->onlinePayment = $row["online_payment"];
            $obj->disburseBank = $row["disburse_bank"];
            $obj->offlinePayment = $row["offline_payment"];
            $obj->autoRenew = $row["automatic_renewal"];
            $obj->notifyRenewal = $row["renewal_notify_number"];
            $obj->academic_session = $row["academic_session"];
            $obj->academic_term = $row["academic_term"];
            $obj->next_resumption_date = $row["next_resumption_date"];
            $obj->smsUnit = $row["smsUnit"];
            $obj->payNo = $row["pay_no"];
            $obj->nextSubDate = $row["next_sub_date"];
            $obj->currencySymbol = $this->currencyCode($row["currency"]);
            $obj->keys = $this->keys;
            $obj->subAccount = $row["subAccount"];
            $earlier = new DateTime("today");
            $later = new DateTime($row["next_sub_date"]);
            $diff = $later->diff($earlier)->format("%a");
            $obj->daysLeft = $diff;
            $obj->quickNotice = $row["quick_notice"];
            $obj->pinAmount = $row["pinAmount"];
            $stmt->close();
            return $obj;
        }

        function currencyCode($currency){
            $currencyArray = ["Naira" => "&#8358;", "Dollar" => "&#036;", "Pounds" => "&#163;", "Euro" => "&#128;", "Cedi" => "&#8373;", "Rand" => "&#x52;", "Shilling" => "Ksh"];
            return $currencyArray[$currency];
        }


        function getStudentDetails($studentNumber){
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
            $stmt->bind_param("s", $studentNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $obj->nationality = $this->outputRefine($row["nationality"]);
            $obj->state = $this->outputRefine($row["state"]);
            $obj->city = $this->outputRefine($row["city"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->blood_group = $this->outputRefine($row["blood_group"]);
            $from = new DateTime($row["dob"]);
            $to   = new DateTime('today');
            $obj->age = $from->diff($to)->y;
            $obj->dob = $this->outputRefine($row["dob"]);
            $obj->enrolment_year = $this->outputRefine($row["enrolment_year"]);
            $obj->pickup_verification = $row["pickup_verification"];
            $obj->bus_pickup = $row["bus_pickup"];
            $obj->bus_dropoff = $row["bus_dropoff"];
            $obj->student_portal = $row["student_portal"];
            $obj->parent_portal = $row["parent_portal"];
            $obj->languages = json_decode($row["languages"], JSON_PRETTY_PRINT);
            $obj->student_desc = $this->outputRefine($row["student_desc"]);
            $stmt->close();
            return $obj;
        }

        public function getPickupDetails($CardNo){
            if(strtolower(explode("/", $CardNo)[1]) == "stdn"){
                $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, id, pickupNumber FROM childpickup WHERE studentNumber = ?");
                $stmt->bind_param("s", $CardNo);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                $pickupArray = array();
                if(count($stmt_result) > 0){
                    $stmt2 = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, classes.class_name FROM students INNER JOIN classes ON classes.class_name = students.present_class WHERE students.student_number = ?");
                    $stmt2->bind_param("s", $CardNo);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    if(count($stmt2_result) > 0){
                        $studentDetails = array_shift($stmt2_result);
                        $obj = new stdClass();
                        $obj->studentProfilePhoto = $studentDetails["profilePhoto"];
                        $obj->studentName = $studentDetails["fullname"];
                        $obj->studentClass = $studentDetails["class_name"];
                        $obj->studentNumber = $CardNo;
                        $pickupArray[] = $obj;
                    }
                    while($pickupDetails = array_shift($stmt_result)){
                        $obj = new stdClass();
                        $obj->fullname = $pickupDetails["fullname"];
                        $obj->profilePhoto = $pickupDetails["profilePhoto"];
                        $obj->pickupNumber = $pickupDetails["pickupNumber"];
                        $pickupArray[] = $obj;
                    }
                    $stmt->close();
                    $stmt2->close();
                    return $pickupArray;
                }else{
                    // NO PICKUP PERSON
                    $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, classes.class_name FROM students INNER JOIN classes ON classes.class_name = students.present_class WHERE students.student_number = ?");
                    $stmt->bind_param("s", $CardNo);
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    if(count($stmt_result) > 0){
                        $studentDetails = array_shift($stmt_result);
                        $obj = new stdClass();
                        $obj->studentProfilePhoto = $studentDetails["profilePhoto"];
                        $obj->studentName = $studentDetails["fullname"];
                        $obj->studentClass = $studentDetails["class_name"];
                        $obj->studentNumber = $CardNo;
                        $pickupArray[] = $obj;
                    }
                    $stmt->close();
                    return $pickupArray;
                }
            }else{
                $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, id, studentNumber FROM childpickup WHERE pickupNumber = ?");
                $stmt->bind_param("s", $CardNo);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                $pickupArray = array();
                if(count($stmt_result) > 0){
                    $row = array_shift($stmt_result);
                    $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, classes.class_name FROM students INNER JOIN classes ON classes.class_name = students.present_class WHERE students.student_number = ?");
                    $stmt->bind_param("s", $row["studentNumber"]);
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    if(count($stmt_result) > 0){
                        $studentDetails = array_shift($stmt_result);
                        $obj = new stdClass();
                        $obj->studentProfilePhoto = $studentDetails["profilePhoto"];
                        $obj->studentName = $studentDetails["fullname"];
                        $obj->studentClass = $studentDetails["class_name"];
                        $obj->studentNumber = $row["studentNumber"];
                        $pickupArray[] = $obj;
                    }
                        $obj = new stdClass();
                        $obj->fullname = $row["fullname"];
                        $obj->profilePhoto = $row["profilePhoto"];
                        $obj->pickupNumber = $CardNo;
                        $pickupArray[] = $obj;

                        $stmt->close();
                        return $pickupArray;

                }else{
                    $stmt->close();
                    return $pickupArray;
                }
            }
            
        }

        public function getLastNumber($category){
            if($category == "parents" || $category == "students" || $category == "childpickup" || $category == "teachers"){
                switch($category){
                    case "parents":
                        $value = "guardian_number";
                        break;
                    case "students":
                        $value = "student_number";
                        break;
                    case "childpickup":
                        $value = "pickupNumber";
                        break;
                    case "teachers":
                        $value = "staff_number";
                        break;
                    default :
                        $value = "";
                        break;
                }
                $stmt = $this->uconn->prepare("SELECT SUBSTRING_INDEX($value,'/',-1) AS id FROM $category ORDER BY id DESC LIMIT 1");
            }else{
                $stmt = $this->uconn->prepare("SELECT id FROM $category ORDER BY id DESC LIMIT 1");
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $lastId = array_shift($stmt_result)["id"];
            $stmt->close();
            return $lastId;
        }

        function verifyLastNumber($number, $table){
            switch($table){
                case "parents":
                    $value = "guardian_number";
                    $abbr = "Prnt";
                    break;
                case "students":
                    $value = "student_number";
                    $abbr = "Stdn";
                    break;
                case "childpickup":
                    $value = "pickupNumber";
                    $abbr = "Pkup";
                    break;
                case "teachers":
                    $value = "staff_number";
                    $abbr = "Stff";
                    break;
                default :
                    $value = "";
                    break;
            }
            $ifExist = $this->checkIfExist($table, "id", $value, $number);
                if($ifExist === true){
                    // GENERATE NEW NUMBER
                    $i = 0;
                    do{
                        $i++;
                        $end = $this->getLastNumber($table) + $i;
                        switch (strlen($end)) {
                            case 1:
                                # code...
                                $end = "00".$end;
                                break;
                            case 2:
                                # code...
                                $end = "0".$end;
                                break;
                            case 3:
                                # code
                                $end = $end;
                                break;
                            default:
                                # code...
                                break;
                        }
                        $domain_name =  $this->getSchoolDetails()->domain_name;
                        $number = date("Y")."/".$abbr."/".ucfirst($domain_name)."/".$end;
                        $ifExist = $this->checkIfExist($table, "id", $value, $number);
                    }while($ifExist === true);
                }else{
                    $number = $number;
                }
                return $number;
        }
        

        function allModules(){
            $stmt = $this->dconn->prepare("SELECT * FROM modules");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $modules = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->price = $row["price"];
                $obj->name = $row["mod_name"];
                $modules[] = $obj;
            }
            return $modules;
        }

        
        
        public function getClassCode($className){
            $className = strtolower($className);
            $stmt = $this->uconn->prepare("SELECT class_code FROM classes WHERE class_name = ?");
            $stmt->bind_param("s", $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $class_code = $this->outputRefine(array_shift($stmt_result)["class_code"]);
            return $class_code;
        }


        public function getClassName($classCode){
            $stmt = $this->uconn->prepare("SELECT class_name FROM classes WHERE class_code = ?");
            $stmt->bind_param("s", $classCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $class_name = $this->outputRefine(array_shift($stmt_result)["class_name"]);
            return ucfirst($class_name);
        }


        public function getCurrentAcademicSession(){
            $stmt = $this->uconn->prepare("SELECT academic_session FROM settings");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $academic_session = $this->outputRefine(array_shift($stmt_result)["academic_session"]);
            return $academic_session;
        }


        public function getCurrentAcademicTerm(){
            $stmt = $this->uconn->prepare("SELECT academic_term FROM settings");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $academic_term = $this->outputRefine(array_shift($stmt_result)["academic_term"]);
            return $academic_term;
        }

        
        public function getSubjectCode($subjectName, $subjectClass){
            $stmt = $this->uconn->prepare("SELECT subjectCode FROM subjects WHERE subjectName = ? AND subjectClass = ?");
            $stmt->bind_param("ss", $subjectName, $subjectClass);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subjectCode = $this->outputRefine(array_shift($stmt_result)["subjectCode"]);
            return $subjectCode;
        }

        public function getSubjectName($subjectCode){
            $stmt = $this->uconn->prepare("SELECT subjectName FROM subjects WHERE subjectCode = ?");
            $stmt->bind_param("s", $subjectCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subjectName = $this->outputRefine(array_shift($stmt_result)["subjectName"]);
            return ucfirst($subjectName);
        }

        public function getTimeName($timeCode){
            $stmt = $this->uconn->prepare("SELECT timeName FROM schedule_times WHERE timeCode = ?");
            $stmt->bind_param("s", $timeCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $timeName = $this->outputRefine(array_shift($stmt_result)["timeName"]);
            return strtoupper($timeName);
        }


        public function getDayName($dayCode){
            $stmt = $this->uconn->prepare("SELECT dayName FROM schedule_days WHERE dayCode = ?");
            $stmt->bind_param("s", $dayCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $dayName = $this->outputRefine(array_shift($stmt_result)["dayName"]);
            return strtoupper($dayName);
        }

        public function getSingleStaffDetailsWithName($staffName){
            $stmt = $this->uconn->prepare("SELECT * FROM teachers WHERE fullname = ?");
            $stmt->bind_param("s", strtolower($staffName));
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffDetails = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->staff_number = $this->outputRefine($row["staff_number"]);
                $obj->nationality = $this->outputRefine($row["nationality"]);
                $obj->state = $this->outputRefine($row["state"]);
                $obj->city = $this->outputRefine($row["city"]);
                $obj->highest_qualification = $this->outputRefine($row["highest_qualification"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->m_status = $this->outputRefine($row["m_status"]);
                $obj->address = $this->outputRefine($row["address"]);
                $obj->department = $this->outputRefine($row["department"]);
                $obj->phone = $this->outputRefine($row["phone"]);
                $obj->email = $this->outputRefine($row["email"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $staffDetails = $obj;
            }
            $stmt->close();
            return $this->outputRefine($staffDetails);
        }

        public function getSingleStaffDetails($staffNo){
            $stmt = $this->uconn->prepare("SELECT * FROM teachers WHERE staff_number = ?");
            $stmt->bind_param("s", $staffNo);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            //$staffDetails = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $this->outputRefine($row["id"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->staff_number = $row["staff_number"];
                $obj->nationality = $row["nationality"];
                $obj->state = $row["state"];
                $obj->city = $row["city"];
                $obj->highest_qualification = $row["highest_qualification"];
                $obj->gender = $row["gender"];
                $obj->m_status = $row["m_status"];
                $obj->address = $row["address"];
                $obj->department = $row["department"];
                $obj->phone = $row["phone"];
                $obj->date_added = $row["date_added"];
                $obj->email = $row["email"];
                $obj->staff_portal = $row["staff_portal"];
                $obj->profilePhoto = $row["profilePhoto"];
                $staffDetails = $obj;
            }
            $stmt->close();
            return $staffDetails;
        }


        public function getAllStaffs(){
            $stmt = $this->uconn->prepare("SELECT * FROM teachers ORDER BY fullname ASC");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffDetails = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->fullname = $row["fullname"];
                $obj->staff_number = $row["staff_number"];
                $obj->nationality = $row["nationality"];
                $obj->state = $row["state"];
                $obj->city = $row["city"];
                $obj->highest_qualification = $row["highest_qualification"];
                $obj->gender = $row["gender"];
                $obj->m_status = $row["m_status"];
                $obj->address = $row["address"];
                $obj->department = $row["department"];
                $obj->phone = $row["phone"];
                $obj->email = $row["email"];
                $obj->profilePhoto = $row["profilePhoto"];
                $staffDetails[] = $obj;
            }
            $stmt->close();
            return $staffDetails;
        }

        public function getTutors(){
            $dept = "tutor";
            $dept2 = "form teacher";
            $stmt = $this->uconn->prepare("SELECT * FROM teachers WHERE department = ? OR department = ? ORDER BY fullname ASC");
            $stmt->bind_param("ss", $dept, $dept2);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffDetails = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->fullname = $row["fullname"];
                $obj->staff_number = $row["staff_number"];
                $obj->nationality = $row["nationality"];
                $obj->state = $row["state"];
                $obj->city = $row["city"];
                $obj->highest_qualification = $row["highest_qualification"];
                $obj->gender = $row["gender"];
                $obj->m_status = $row["m_status"];
                $obj->address = $row["address"];
                $obj->department = $row["department"];
                $obj->phone = $row["phone"];
                $obj->email = $row["email"];
                $obj->profilePhoto = $row["profilePhoto"];
                $staffDetails[] = $obj;
            }
            $stmt->close();
            return $staffDetails;
        }



        function fetchAllSubjectStaff($subject_Code){
            $stmt1 = $this->uconn->prepare("SELECT staffCode FROM subjectTeachers WHERE subjectCode = ?");
            $stmt1->bind_param("s", $subject_Code);
            $stmt1->execute();
            $stmt1_result = $this->get_result($stmt1);
            $row1 = array_shift($stmt1_result);
            $stmt = $this->uconn->prepare("SELECT * FROM teachers WHERE staff_number = ?");
            $stmt->bind_param("s", $row1["staffCode"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->staff_number = $row["staff_number"];
                $obj->fullname = $row["fullname"];
                $obj->phone = $row["phone"];
                $obj->department = $row["department"];
                $obj->profilePhoto = $row["profilePhoto"];
                $staffArray[] = $obj;
            }
            $stmt->close();
            return $staffArray;
        }


        public function getClasses(){
            $stmt = $this->uconn->prepare("SELECT * FROM classes");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $classes = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->class_name = $row["class_name"];
                $obj->class_code = $row["class_code"];
                $classes[] = $obj;
            }
            $stmt->close();
            return $classes;
        }

        public function getAllDistinctSubjects(){
            $stmt = $this->uconn->prepare("SELECT * FROM subjects");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subjects = array();
            if(count($stmt_result) == 0){
                return $subjects;
            }else{
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->subjectName = $row["subjectName"];
                    $obj->subjectCode = $row["subjectCode"];
                    $obj->subjectClass = $row["subjectClass"];
                    $subjects[] = $obj;
                }
                $stmt->close();
                return $subjects;
            }
            $stmt->close();
            return $subjects;  
        }

        public function getAllSubjects(){
            $stmt = $this->uconn->prepare("SELECT * FROM subjects");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subjects = array();
            if(count($stmt_result) == 0){
                return $subjects;
            }else{
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->subjectName = $row["subjectName"];
                    $obj->subjectCode = $row["subjectCode"];
                    $obj->subjectClass = $row["subjectClass"];
                    $subjects[] = $obj;
                }
                $stmt->close();
                return $subjects;
            }
            $stmt->close();
            return $subjects;
        }

        public function getClassSubject($className){
            $stmt = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectClass = ? ORDER BY subjectName ASC");
            $stmt->bind_param("s", $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $allSubject = array();
            while ($row = array_shift($stmt_result)) {
                # code...
                $obj = new stdClass();
                $obj->subjectCode = $row["subjectCode"];
                $obj->subjectName = $row["subjectName"];
                $allSubject[] = $obj;
            }
            $stmt->close();
            return $allSubject;
        }

        public function checkIfExist($tableName, $selector, $columnName, $value){
            $stmt = $this->uconn->prepare("SELECT $selector FROM $tableName WHERE $columnName = ?");
            $stmt->bind_param("s", $value);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }

        public function getPhoneNumberName($phone, $table, $columnName, $selector){
            if(is_array($columnName)){
                $stmt = $this->uconn->prepare("SELECT $selector FROM $table WHERE $columnName[0] = ? OR $columnName[1] = ?");
                $stmt->bind_param("ss", $phone, $phone);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $fullname = ucwords(array_shift($stmt_result)[$selector]);
                    $stmt->close();
                    return $fullname;
                }else{
                    $stmt->close();
                    return "Name Not Found";
                }
            }else{
                $stmt = $this->uconn->prepare("SELECT $selector FROM $table WHERE $columnName = ?");
                $stmt->bind_param("s", $phone);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $fullname = ucwords(array_shift($stmt_result)[$selector]);
                    $stmt->close();
                    return $fullname;
                }else{
                    $stmt->close();
                    return "Name Not Found";
                }
            }
        }

        function getAudienceNumbers($table, $columnName){
            $stmt = $this->uconn->prepare("SELECT $columnName FROM $table");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            // $numbers = array();
            $stmt->close();
            return $stmt_result;
        }

        function getTimingActivities(){
            $stmt = $this->dconn->prepare("SELECT * FROM timetable_activities");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $activitiesArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->name = $row["activityName"];
                $activitiesArray[] = $obj;
            }
            return $activitiesArray;
        }

        function getDistinctFeeName(){
            $stmt = $this->uconn->prepare("SELECT DISTINCT fee_name FROM fees");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $fees = array();
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $fees[] = $row["fee_name"];
                }
                $stmt->close();
                return $fees;
            }else{
                $stmt->close();
                return $fees;
            }
        }

        function verifyRef($verifier){
            $stmt = $this->dconn->prepare("SELECT id FROM completed_tran WHERE trans_ref = ?");
            $stmt->bind_param("s", $verifier);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }
        //NAME REFINEMENT FUNCTIONS

        function split_full_name($full_name) {
            $fname = null;
            $lname = null;
            $initials = null;
            $full_name = trim($full_name);
            $unfiltered_name_parts = explode(" ",$full_name);
            foreach ($unfiltered_name_parts as $word) {
                if ($word{0} != "");
                $name_parts[] = $word;
            }
            $num_words = sizeof($name_parts);
            $salutation = $this->is_salutation($name_parts[0]);
            $suffix = $this->is_suffix($name_parts[sizeof($name_parts)-1]);
            $start = ($salutation) ? 1 : 0;
            $end = ($suffix) ? $num_words-1 : $num_words;
            for ($i=$start; $i < $end-1; $i++) {
            $word = $name_parts[$i];
            if ($this->is_compound_lname($word) && $i != $start)
                break;
            if ($this->is_initial($word)) {
                if ($i == $start) {
                    if ($this->is_initial($name_parts[$i+1]))
                        $fname .= " ".strtoupper($word);
                    else
                        $initials .= " ".strtoupper($word); 
                } else {
                    $initials .= " ".strtoupper($word); 
                }
            } else {
                $fname .= " ".$this->fix_case($word);
            }   
        }
        if ($end-$start > 1) {
            for ($i; $i < $end; $i++) {
                $lname .= " ".$this->fix_case($name_parts[$i]);
            }
        } else {
            $fname = $this->fix_case($name_parts[$i]);
        }
        $name['salutation'] = $salutation;
        $name['fname'] = trim($fname);
        $name['initials'] = trim($initials);
        $name['lname'] = trim($lname);
        $name['suffix'] = $suffix;
        return $name;
    }

    function is_salutation($word) {
        $word = str_replace('.','',strtolower($word));
        if ($word == "mr" || $word == "master" || $word == "mister")
            return "Mr.";
        else if ($word == "mrs")
            return "Mrs.";
        else if ($word == "miss" || $word == "ms")
            return "Ms.";
        else if ($word == "dr")
            return "Dr.";
        else if ($word == "rev")
            return "Rev.";
        else if ($word == "fr")
            return "Fr.";
        else
            return false;
    }

    function is_suffix($word) {
        $word = str_replace('.','',$word);
        $suffix_array = array('I','II','III','IV','V','Senior','Junior','Jr','Sr','PhD','APR','RPh','PE','MD','MA','DMD','CME');
        foreach ($suffix_array as $suffix) {
            if (strtolower($suffix) == strtolower($word))
                return $suffix;
        }
        return false;
    }

    function is_compound_lname($word) {
        $word = strtolower($word);
        $words = array('vere','von','van','de','del','della','di','da','pietro','vanden','du','st.','st','la','ter');
        return array_search($word,$words);
    }

    function is_initial($word) {
        return ((strlen($word) == 1) || (strlen($word) == 2 && $word{1} == "."));
    }

    function is_camel_case($word) {
        if (preg_match("|[A-Z]+|s", $word) && preg_match("|[a-z]+|s", $word)) 
            return true;
        return false;
    }

    function fix_case($word) {
        $word = $this->safe_ucfirst("-",$word);
        $word = $this->safe_ucfirst(".",$word);
        return $word;
    }

    function safe_ucfirst($seperator, $word) {
        $parts = explode($seperator,$word);
        foreach ($parts as $word) {
            $words[] = ($this->is_camel_case($word)) ? $word : ucfirst(strtolower($word));
        }
        return implode($seperator,$words);
    }

    //CREATE NAME ACRONYM

    public function createAcronym($string){
        $refinedString = $this->split_full_name($string);
        $string = $refinedString['fname']." ".$refinedString['lname'];
        $output = null;
        $token = strtok(trim($string), ' ');
        while ($token !== false){
            $output .= $token[0];
            $token = strtok(' ');
        }
        return strtoupper($output);
    }

    public function getAllFees($class_code){
        $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE class_code = ?");
        $stmt->bind_param("s", $class_code);        
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $feesArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->fee_name = $row["fee_name"];
            $obj->fee_id = $row["id"];
            $obj->amount = $row["amount"];
            $feesArray[] = $obj;
        }
        return $feesArray;
    }


    public function getFeeDetails($feeInfo, $state, $student_id){
        $obj = new stdClass();
        if($state == 1){
            // OUTSTANDING FEE
            if(!empty($student_id)){
                $stmt = $this->uconn->prepare("SELECT outstanding_fee.feeId, outstanding_fee.id, outstanding_fee.amount, fees.fee_name FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.id = ? AND outstanding_fee.studentNumber = ?");
                $stmt->bind_param("is", $feeInfo, $student_id);
                $stmt->execute();
            }else{
                $stmt = $this->uconn->prepare("SELECT outstanding_fee.feeId, outstanding_fee.id, outstanding_fee.amount, fees.fee_name FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.id = ?");
                $stmt->bind_param("i", $feeInfo);
                $stmt->execute();
            }
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                // $obj = new stdClass();
                $obj->fee_name = $row["fee_name"];
                $obj->fee_id = $row["feeId"];
                $obj->amount = $row["amount"];
            }
        }else{
            // NORMAL FEE
            if(!empty($student_id)){
                $stmt = $this->uconn->prepare("SELECT outstanding_fee.feeId, outstanding_fee.id, outstanding_fee.amount, fees.fee_name FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.id = ? AND outstanding_fee.studentNumber = ?");
                $stmt->bind_param("is", $feeInfo, $student_id);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    while($row = array_shift($stmt_result)){
                        $obj->fee_name = $row["fee_name"];
                        $obj->fee_id = $row["feeId"];
                        $obj->amount = $row["amount"];
                    }
                }else{
                    $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE id = ?");
                    $stmt->bind_param("i", $feeInfo);
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    while($row = array_shift($stmt_result)){
                        $obj->fee_name = $row["fee_name"];
                        $obj->fee_id = $row["id"];
                        $obj->amount = $row["amount"];
                    }
                }
            }else{
                $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE id = ?");
                $stmt->bind_param("i", $feeInfo);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                while($row = array_shift($stmt_result)){
                    $obj->fee_name = $row["fee_name"];
                    $obj->fee_id = $row["id"];
                    $obj->amount = $row["amount"];
                }
            }
        }
        return $obj;
    }


    public function getTimeSheet($className, $weekDay, $identifier){
        $session = $this->getCurrentAcademicSession();
        $term = $this->getCurrentAcademicTerm();
        $classCode = $this->getClassCode($className);
        
        if($weekDay == "none"){
            $stmt = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeClass = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sss", $classCode, $session, $term);
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeClass = ? AND timeDay = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("ssss", $classCode, $weekDay, $session, $term);   
        }

        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $timeSheetArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->timeCode = $row["timeTime"];
            $obj->subjectCode = $row["timeSubject"];
            $obj->subjectName = $this->getSubjectName($row["timeSubject"]);
            $obj->tutorCode = $row["timeTutor"];
            $obj->tutorName = $this->getSingleStaffDetails($row["timeTutor"])->fullname;
                $stmt2 = $this->uconn->prepare("SELECT * FROM schedule_times WHERE timeCode = ? ORDER BY timeName ASC");
                $stmt2->bind_param("s", $row["timeTime"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $row2 = array_shift($stmt2_result);
            $obj->timeName = $row2["timeName"];
            $obj->activity = $row2["activity"];
            if($identifier == "1"){
                $timeSheetArray[] = $obj;
            }else{
                if($row2["activity"] != "sports" && $row2["activity"] != "lunch" && $row2["activity"] != "break" && $row2["activity"] != "devotion"){
                    $timeSheetArray[] = $obj;   
                }
            }
        }
        return $timeSheetArray;
    }

    public function getTimeSheetStaff($staffNumber, $weekDay, $identifier){
        $session = $this->getCurrentAcademicSession();
        $term = $this->getCurrentAcademicTerm();
        $staffNumber = $staffNumber;
        
        if($weekDay == "none"){
            $stmt = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeTutor = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sss", $staffNumber, $session, $term);
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeTutor = ? AND timeDay = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("ssss", $staffNumber, $weekDay, $session, $term);   
        }

        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $timeSheetArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->timeCode = $row["timeTime"];
            $obj->subjectCode = $row["timeSubject"];
            $obj->subjectName = $this->getSubjectName($row["timeSubject"]);
            $obj->tutorCode = $row["timeTutor"];
            $obj->tutorName = $this->getSingleStaffDetails($row["timeTutor"])->fullname;
                $stmt2 = $this->uconn->prepare("SELECT * FROM schedule_times WHERE timeCode = ? ORDER BY timeName ASC");
                $stmt2->bind_param("s", $row["timeTime"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $row2 = array_shift($stmt2_result);
            $obj->timeName = $row2["timeName"];
            $obj->activity = $row2["activity"];
            $obj->className = $this->getClassName($row["timeClass"]);
            if($identifier == "1"){
                $timeSheetArray[] = $obj;
            }else{
                if($row2["activity"] != "sports" && $row2["activity"] != "lunch" && $row2["activity"] != "break" && $row2["activity"] != "devotion"){
                    $timeSheetArray[] = $obj;   
                }
            }
        }
        return $timeSheetArray;
    }

    public function getStaffSubject($staffNumber){
        $stmt = $this->uconn->prepare("SELECT * FROM subjectTeachers WHERE staffCode = ? ORDER BY subjectCode ASC");
        $stmt->bind_param("s", $staffNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $allSubject = array();
        while ($row = array_shift($stmt_result)) {
            # code...
            $obj = new stdClass();
            $obj->subjectCode = $row["subjectCode"];
            $obj->classCode = $row["classCode"];
            $obj->subjectName = $this->getSubjectName($row["subjectCode"]);
            $allSubject[] = $obj;
        }
        $stmt->close();
        return $allSubject;
    }


    function getOverallStudentPerformance($student_id){
        $session = $this->getCurrentAcademicSession();
        $term = $this->getCurrentAcademicTerm();
        $classCode = $this->getClassCode($this->getStudentDetails($student_id)->present_class);
        $stmt = $this->uconn->prepare("SELECT total FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        $stmt->bind_param("sss", $student_id, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $OverallArray = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $OverallArray[] = $row["total"];
                $number_of_subjects = count($OverallArray);
                $grossTotal = 100*$number_of_subjects;
                $obtainedTotal = array_sum($OverallArray);
                $perfomance = ($obtainedTotal/$grossTotal)*100;
                }
                $remark = $this->getGrade($obtainedTotal, $classCode)->remark;
        }else{
            $remark = "N/A";
        }
        return $remark;
        $stmt->close();
    }

    function getRealAssessmentId($assName, $part){
        $stmt = $this->uconn->prepare("SELECT assId FROM assessment_settings WHERE name = ? AND mid_term = ?");
        $stmt->bind_param("si", $assName, $part);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $id = array_shift($stmt_result)["assId"];
        $stmt->close();
        return $id;
    }


    function getGrade($value, $classCode){
        $stmt = $this->uconn->prepare("SELECT * FROM grading_system WHERE `froms` <= ? AND `tos` >= ? AND class_code = ?");
        $stmt->bind_param("iis", $value, $value, $classCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->grade = $this->outputRefine($row["grade"]);
        $obj->remark = $this->outputRefine($row["remark"]);
        $stmt->close();
        return $obj;
    }
    
    function getParentMail($studentNumber){
        $stmt = $this->uconn->prepare("SELECT parents.email AS parentsEmail FROM `students` INNER JOIN parents ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE students.student_number = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $parentEmail = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                if(!empty($row["parentsEmail"])){
                    $parentEmail[] = $row["parentsEmail"];
                }
            }
            $stmt->close();
            return $parentEmail;
        }else{
            $stmt->close();
            return $parentEmail;
        }
    }

    function getModulePricePerStudent($module){
        $stmt = $this->dconn->prepare("SELECT per_student FROM pricing WHERE package = ?");
        $stmt->bind_param("i", $module);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $price = array_shift($stmt_result)["per_student"];
        $stmt->close();
        return $price;
    }

    function totalStudentNumber(){
        $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM students");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $number = array_shift($stmt_result)["num"];
        $stmt->close();
        return $number;
    }

    function checkPayNo(){
        $stmt = $this->uconn->prepare("SELECT pay_no FROM settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $val = array_shift($stmt_result)["pay_no"];
        return $val;
    }

    function getCharge(){
        $pay_no = $this->checkPayNo();
        if($pay_no == 0){
            // FIRST PAYMENT......CHARGE FIRST FEE
            $module = $this->getSchoolDetails()->modules;
            $stmt = $this->dconn->prepare("SELECT pricing.price FROM pricing  WHERE pricing.package = ?");
            $stmt->bind_param("i", $module);
            if($stmt->execute()){
                $stmt_result = $this->get_result($stmt);
                $row = array_shift($stmt_result);
                $charge = $row["price"];
                return $charge;
            }else{
                $stmt->close();
                return false;
            }
        }else{
            // RENEWAL
            $totalStudent = $this->totalStudentNumber();
            $module = $this->getSchoolDetails()->modules;
            $modulePrice = $this->getModulePricePerStudent($module);
            $charge = $modulePrice * $totalStudent;
            return $charge;
        }
    }

    function otherPlanPrice($module){
        $pay_no = $this->checkPayNo();
        if($pay_no == 0){
            // FIRST PAYMENT......CHARGE FIRST FEE
            $stmt = $this->dconn->prepare("SELECT pricing.price FROM pricing  WHERE pricing.package = ?");
            $stmt->bind_param("i", $module);
            if($stmt->execute()){
                $stmt_result = $this->get_result($stmt);
                $row = array_shift($stmt_result);
                $charge = $row["price"];
                return $charge;
            }else{
                $stmt->close();
                return false;
            }
        }else{
            $totalStudent = $this->totalStudentNumber();
            $modulePrice = $this->getModulePricePerStudent($module);
            $charge = $modulePrice * $totalStudent;
            return $charge;
        }
    }

    function renewDetails($module){
        $pay_no = $this->checkPayNo();
        // GET SUBACCOUNT CODE
        $envFile = parse_ini_file(".env");
        $dname = $envFile["database"];
        $stmt = $this->dconn->prepare("SELECT partners.subAccountCode FROM partners INNER JOIN school_partner ON school_partner.partnerCode = partners.partner_no WHERE school_partner.schoolData = ?");
        $stmt->bind_param("s", $dname);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $subAccountCode = array_shift($stmt_result)["subAccountCode"];
        if($pay_no == 0){
            // FIRST PAYMENT......CHARGE FIRST FEE
            $stmt = $this->dconn->prepare("SELECT pricing.price FROM pricing  WHERE pricing.package = ?");
            $stmt->bind_param("i", $module);
            if($stmt->execute()){
                $stmt_result = $this->get_result($stmt);
                $row = array_shift($stmt_result);
                $charge = $row["price"];
                $obj = new stdClass();
                $obj->charge = $charge;
                $obj->refCode = $subAccountCode;
                return $obj;
            }else{
                $stmt->close();
                return false;
            }
        }else{
            $totalStudent = $this->totalStudentNumber();
            $modulePrice = $this->getModulePricePerStudent($module);
            $charge = $modulePrice * $totalStudent;
            $obj = new stdClass();
            $obj->charge = $charge;
            $obj->refCode = $subAccountCode;
            return $obj;
        }
    }

    function getFamilyDetails($family_id){
        $stmt = $this->uconn->prepare("SELECT family.family_name, family.balance, family.father_phone, family.mother_phone, parents.email FROM family INNER JOIN parents ON parents.phone = family.father_phone OR parents.phone = family.mother_phone WHERE family.id = ?");
        $stmt->bind_param("i", $family_id);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            $obj->fullname = $row["family_name"];
            $obj->balance = $row["balance"];
            $obj->fatherPhone = $row["father_phone"];
            $obj->motherPhone = $row["mother_phone"];
            $obj->parentEmail = $row["email"];
            $stmt->close();
            return $obj;
        }else{
            $stmt->close();
            return $obj;
        }
    }

    function getFamilyChildren($familyId){
        $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class FROM students INNER JOIN family ON students.father_phone = family.father_phone OR students.mother_phone = family.mother_phone WHERE family.id = ?");
        $stmt->bind_param("i", $familyId);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $children = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fullname = $row["fullname"];
                $obj->studentNumber = $row["student_number"];
                $obj->presentClass = $row["present_class"];
                $children[] = $obj;
            }
            $stmt->close();
            return $children;
        }else{
            $stmt->close();
            return $children;
        }
    }

    function getStudentFeesPayment($studentNumber, $payingSession, $payingTerm){
        $className = $this->getStudentDetails($studentNumber)->present_class;
        $reply = $this->getStudentFees($payingSession, $payingTerm, $className, $studentNumber);
        $totalPayingFee = array();
        $totalSchoolFeeAmount = 0;
        if(count($reply) > 0){
            foreach($reply as $key => $fee){
                if($fee->isSchoolFeePart == 1){
                    $totalSchoolFeeAmount += $fee->amount;
                }else{
                    $totalPayingFee[] = $fee;
                }
            }
            $obj = new stdClass();
            $obj->id = 0; // TAKE NOTE OF ID 0...
            $obj->feeName = "School Fee";
            $obj->amount = $totalSchoolFeeAmount;
            // $obj->isSchoolFeePart = $row["schoolFeePart"];
            $obj->state = 0;
            $totalPayingFee[] = $obj;
        }
        return $totalPayingFee;
    }

    function getStudentFees($paymentSession, $paymentTerm, $selectedClass, $studentId){
        $class_code = $this->getClassCode($selectedClass);
        $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE academic_session = ? AND academic_term = ? AND class_code = ?");
        $stmt->bind_param("sis", $paymentSession, $paymentTerm, $class_code);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $fees = array();
        $studentPaymentArray = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->feeName = $row["fee_name"];
                $obj->amount = $row["amount"];
                $obj->isSchoolFeePart = $row["schoolFeePart"];
                $obj->state = 0;
                $studentPaymentArray[] = $obj;
            }
        }
        // GET OUTSTANDING PAYMENT
        $stmt = $this->uconn->prepare("SELECT outstanding_fee.session, outstanding_fee.term, outstanding_fee.id, outstanding_fee.feeId, outstanding_fee.amount, fees.fee_name FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.studentNumber = ?");
        $stmt->bind_param("s", $studentId);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                switch($row["term"]){
                    case 1:
                        $term = "1st Term";
                        break;
                    case 2:
                        $term = "2nd Term";
                        break;
                    case 3:
                        $term = "3rd Term";
                        break;
                    default:
                        $term = "";
                        break;
                }
                $session = ($row["session"] - 1)."/".$row["session"];
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->amount = $row["amount"];
                $obj->isSchoolFeePart = 0; // NOT SCHOOL FEE PART AT ALL
                $obj->feeName = $session." ".$term." Outstanding Fee - ".$row["fee_name"];
                $obj->state = 1;
                $studentPaymentArray[] = $obj;
            }
        }
        $stmt->close();
        return $studentPaymentArray;
    }

    function newPickupNumber(){
        $end = $this->getLastNumber("childpickup") + 1;
        switch (count($end)) {
            case 1:
                # code...
                $end = "00".$end;
                break;
            case 2:
                # code...
                $end = "0".$end;
                break;
            case 3:
                # code
                $end = $end;
                break;
            
            default:
                # code...
                break;
        }
        $domain_name =  $this->getSchoolDetails()->domain_name;
        $newNum = date("Y")."/Pkup"."/".ucfirst($domain_name)."/".$end;
        return $newNum;
    }

    function addNewPickup($pickupNumber, $pickupName, $pickupCountry, $pickupState, $pickupCity, $pickupRelation, $pickupGender, $pickupMaritalStatus, $pickupAddress, $pickupOccupation, $pickupPhone, $pickupEmail, $pickupStudent, $filename, $name){
        $stmt = $this->uconn->prepare("SELECT id FROM childpickup WHERE studentNumber = ? AND email = ?");
        $stmt->bind_param("ss", $pickupStudent, $pickupEmail);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            return "exist";
        }else{
            $pickupNumber = $this->verifyLastNumber($pickupNumber, "childpickup");
            $stmt = $this->uconn->prepare("INSERT INTO childpickup (fullname, pickupNumber, nationality, state, city, relation_student, gender, m_status, occupation, phone, email, studentNumber, profilePhoto, address, added_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("sssssssssssssss", $pickupName, $pickupNumber, $pickupCountry, $pickupState, $pickupCity, $pickupRelation, $pickupGender, $pickupMaritalStatus, $pickupOccupation, $pickupPhone, $pickupEmail, $pickupStudent, $filename, $pickupAddress, $name);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                return "false";
            }
        }
    }

    function getDistinctAssesssment(){
        $stmt = $this->uconn->prepare("SELECT name, assId FROM assessment_settings GROUP BY name");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $assessmentArray = [];
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->assId = $row["assId"];
                $obj->assName = $row["name"];
                $assessmentArray[] = $obj;
            }
        }
        $stmt->close();
        return $assessmentArray;
    }

    function getMidTermState(){
        $midTerm = 1;
        $stmt = $this->uconn->prepare("SELECT id FROM assessment_settings WHERE mid_term = ? LIMIT 1");
        $stmt->bind_param("i", $midTerm);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    // TEXT MESSAGING STARTS HERE
    function logMessage($audienceBroadcast, $broadcastDate, $broadcastMessage, $logNumbers, $state){
        $logNumbers = json_encode($logNumbers);
        $audienceBroadcast = json_encode($audienceBroadcast);
        $stmt = $this->uconn->prepare("INSERT INTO messages (audienceBroadcast, broadcastDate, broadcastMessage, logNumbers, state) VALUES (?,?,?,?,?)");
        $stmt->bind_param("ssssi", $audienceBroadcast, $broadcastDate, $broadcastMessage, $logNumbers, $state);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function updateSmsUnit($unit){
        $stmt = $this->uconn->prepare("UPDATE settings SET smsUnit = ?");
        $stmt->bind_param("i", $unit);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }
    // CHECKS SCHOOL'S SMS BALANCE
    function checkSmsBalance(){
        $stmt = $this->uconn->prepare("SELECT smsUnit FROM settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $smsUnit = array_shift($stmt_result)["smsUnit"];
        $stmt->close();
        return $smsUnit;
    }

    function checkIfAlreadySent($message, $receiver){
        $stmt = $this->uconn->prepare("SELECT * FROM messages ORDER BY id DESC LIMIT 1");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            if($row["broadcastMessage"] == $message && json_encode($receiver) == $row["logNumbers"]){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }else{
            $stmt->close();
            return false;
        }
    }
    


    //SEND SMS
    public function sendSMS($message, $sender, $receiver){
        // CHECK SCHOOLS SMS BALANCE
        $reply = $this->checkIfAlreadySent($message, $receiver);
        if($reply === true){
            // MESSAGE ALREADY SENT
            return "alreadySent";
        }else{
            $smsUnit = $this->checkSmsBalance();
            $pageNumber = ceil(strlen($message)/160);
            $totalNumber = 1 * ($pageNumber * count($receiver));

            if($totalNumber > $smsUnit){
                return "insufficient";
            }else{
                // SEND MESSAGES
                $reply = $this->pushMessage($message, $sender, $receiver);
                if($reply === true){
                    // MESSAGE SENT
                    $unit = $smsUnit - $totalNumber;
                    $reply = $this->updateSmsUnit($unit);
                    if($reply === true){
                        return true;
                    }else{
                        return "updatesms";
                    }
                }else{
                    switch($reply){
                        case 1702 :
                            $resp = "invalidparam";
                            break;
                        case 1703 :
                            $resp = "invalidlogin";
                            break;
                        case 1704:
                            $resp = "insufficientunit";
                            break;
                        case 1705:
                            $resp = "numbers";
                            break;
                        case 1706:
                            $resp = "internal";
                            break;
                        default:
                            $resp = "smserror";
                            break;
                    }
                    return $resp;
                }
            }
        }
    }

    function pushMessage($message, $sender, $receiver){
        if(is_array($receiver)){
            $formattedReceiver = implode(",", $receiver);
        }else{
            $formattedReceiver = $receiver;
        }
        $authEmail = $this->keys["smsAuth"];
        $authPassword = $this->keys["smsPass"];
        $smsContent = urlencode($message);
        $smsSender = $sender;
        $smsReceiver = $formattedReceiver;
        // $smsEndpoint = "https://kullsms.com/customer/api/?username="+$authEmail+"&password="+$authPassword+"&message="+$smsContent+"&sender="+$smsSender+"&mobiles="+$smsReceiver;
        //https://api.loftysms.com/simple/sendsms?username=emmylex&password=emmylexo&sender=demo&recipient=08189386761&message=YourURLencodedMessage
        // 'https://api.loftysms.com/simple/sendsms?username='.$authEmail.'&password='.$authPassword.'&sender='.$smsSender.'&recipient='.$smsReceiver.'&message='.$smsContent;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://kullsms.com/customer/api/?username='.$authEmail.'&password='.$authPassword.'&message='.$smsContent.'&sender='.$smsSender.'&mobiles='.$smsReceiver);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        // 1701 = SUCCESS
        // 1702 = INVALID URL/PARAMETERS
        // 1703 = INVALID USERNAME/PASSWORD
        // 1704 = INSUFFICIENT CREDIT
        // 1705 = MOBILES TO LONG (MAX.500)
        // 1706 = INTERNAL ERROR
        if($result == 1701){
            return true;
            //Perform any other operations here
            // Return true;
        }else{
            return $result;
        }
    }


    // GET SMS UNITS BALANCE
    public function getSmsCreditBalance(){
        $authEmail = "";
        $authPassword = "";
        $url = 'https://kullsms.com/customer/api/';
        $fields = array(
            'username' => urlencode($authEmail),
            'password' => urlencode($authPassword),
            'request' => urlencode("balance")
        );
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        $smsCreditBalance = curl_exec($ch);
        curl_close($ch);
    }


    //RAVEPAY ENCRYPTION

    function getKey($seckey){
        $hashedkey = md5($seckey);
        $hashedkeylast12 = substr($hashedkey, -12);
        
        $seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
        $seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);
        
        $encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
        return $encryptionkey;
        
    }

    function encrypt3Des($data, $key)
    {
    $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
            return base64_encode($encData);
    }


    //MAKE PAYMENT


    public function payviaaccount(){ 
        
        error_reporting(E_ALL);
        ini_set('display_errors',1);
        
        $data = array(
        "PBFPubKey" => "FLWPUBK-7adb6177bd71dd43c2efa3f1229e3b7f-X",
        "accountbank" => "058",// get the bank code from the bank list endpoint.
        "accountnumber" => "0000000",
        "currency" => "NGN",
        "payment_type" => "account",
        "country" => "NG",
        "amount" => "100", // amount must be greater than NGN100
        "email" => "desola.ade1@gmail.com",
        "phonenumber" => "0902620185",
        "firstname" => "temi",
        "lastname" => "desola",
        "IP" => "355426087298442",
        "redirect_url" => "https://rave-webhook.herokuapp.com/receivepayment",
        "txRef" => "MC-0292920" // merchant unique reference
        );

        $SecKey = 'FLWSECK-bb971402072265fb156e90a3578fe5e6-X';
        
        $key = getKey($SecKey); 
        
        $dataReq = json_encode($data);
        
        $post_enc = encrypt3Des( $dataReq, $key );

        var_dump($dataReq);
        
        $postdata = array(
            'PBFPubKey' => 'FLWPUBK-e634d14d9ded04eaf05d5b63a0a06d2f-X',
            'client' => $post_enc,
            'alg' => '3DES-24');
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/charge");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata)); //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        
        
        $headers = array('Content-Type: application/json');
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $request = curl_exec($ch);
        
        if ($request) {
            $result = json_decode($request, true);
            echo "<pre>";
            print_r($result);
        }else{
            if(curl_error($ch))
            {
                echo 'error:' . curl_error($ch);
            }
        }
        
        curl_close($ch);
    }


    public function payviacard(){ 
        
        error_reporting(E_ALL);
        ini_set('display_errors',1);
        
        $data = array(
        'PBFPubKey' => 'FLWPUBK-e634d14d9ded04eaf05d5b63a0a06d2f-X',
        'cardno' => '5438898014560229',
        'currency' => 'NGN',
        'country' => 'NG',
        'cvv' => '789',
        'amount' => '300',
        'expiryyear' => '19',
        'expirymonth' => '09',
        'suggested_auth' => 'pin',
        'pin' => '3310',
        'email' => 'tester@flutter.co',
        'IP' => '103.238.105.185', //GET CUSTOMER IP HERE
        'txRef' => 'MXX-ASC-4578', //GENERATE UNIQUE TRANSACTION REF
        'redirect_url' => ''
        );
        
        $SecKey = 'FLWSECK-bb971402072265fb156e90a3578fe5e6-X';
        
        $key = getKey($SecKey); 
        
        $dataReq = json_encode($data);
        
        $post_enc = encrypt3Des( $dataReq, $key );

        var_dump($dataReq);
        
        $postdata = array(
            'PBFPubKey' => 'FLWPUBK-e634d14d9ded04eaf05d5b63a0a06d2f-X',
            'client' => $post_enc,
            'alg' => '3DES-24');
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/charge");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata)); //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        
        
        $headers = array('Content-Type: application/json');
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $request = curl_exec($ch);
        
        if ($request) {
            $result = json_decode($request, true);
            echo "<pre>";
            print_r($result);
        }else{
            if(curl_error($ch))
            {
                echo 'error:' . curl_error($ch);
            }
        }
        
        curl_close($ch);
    }



    public function webhook(){
        // Retrieve the request's body
        $body = @file_get_contents("php://input");

        // retrieve the signature sent in the reques header's.
        $signature = (isset($_SERVER['verif-hash']) ? $_SERVER['verif-hash'] : '');

        /* It is a good idea to log all events received. Add code *
        * here to log the signature and body to db or file       */

        if (!$signature) {
            // only a post with rave signature header gets our attention
            exit();
        }

        // Store the same signature on your server as an env variable and check against what was sent in the headers
        $local_signature = getenv('SECRET_HASH');

        // confirm the event's signature
        if( $signature !== $local_signature ){
        // silently forget this ever happened
        exit();
        }

        http_response_code(200); // PHP 5.4 or greater
        // parse event (which is json string) as object
        // Give value to your customer but don't give any output
        // Remember that this is a call from rave's servers and 
        // Your customer is not seeing the response here at all
        $response = json_decode($body);
        if ($response->body->status == 'successful') {
            # code...
            // TIP: you may still verify the transaction
                    // before giving value.
        }
        exit();
    }


    }

    // $logic = new logic($ini_array);

?>
