<?php
if(!isset($_SESSION["folderName"])){
    session_start();
}
    include_once ("../../".$_SESSION["folderName"]."/config.php");

    class modal extends logic {

        function outputRefine($value){
            if(isset($value) && !empty($value)){
                return ucfirst($value);
            }else{
                return "N/A";
            }
        }


        function removeAccents($string){
            return $string;
        }
        

        function get_result( $Statement ) {
            $RESULT = array();
            $Statement->store_result();
            for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
                $Metadata = $Statement->result_metadata();
                $PARAMS = array();
                while ( $Field = $Metadata->fetch_field() ) {
                    $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
                }
                call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
                $Statement->fetch();
            }
            $object = (object) $RESULT;
            return $RESULT;
        }


        function fetchAllStudent(){
            $stmt = $this->uconn->prepare("SELECT * FROM students");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $studentArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $studentArray[] = $obj;
            }
            $stmt->close();
            return $studentArray;
        }
    

        function newStudentNumber(){
            $end = $this->getLastNumber("students") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $domain_name =  $this->getSchoolDetails()->domain_name;
            $newNum = date("Y")."/Stdn"."/".ucfirst($domain_name)."/".$end;
            return $newNum;
            
        }


        function addNewStudent($studentNumber, $fullname, $class, $studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $studentFatherContact, $studentMotherContact, $profilePhoto){
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE fullname = ? AND father_phone = ? AND mother_phone = ?");
            $fullname = $this->removeAccents($fullname);
            $stmt->bind_param("sss", $fullname, $studentFatherContact, $studentMotherContact);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $studentNumber = $this->verifyLastNumber($studentNumber, "students");
                $stmt1 = $this->uconn->prepare("INSERT INTO students (fullname, student_number, gender, profilePhoto, dob, nationality, state, city, blood_group, present_class, father_phone, mother_phone) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt1->bind_param("ssssssssssss", strtolower($fullname), strtolower($studentNumber), strtolower($studentGender), strtolower($profilePhoto), strtolower($studentDob), strtolower($studentCountry), strtolower($studentState), strtolower($studentCity), strtolower($studentBloodGroup), $class, strtolower($studentFatherContact), strtolower($studentMotherContact));
                if($stmt1->execute()){
                    $stmt1->close();
                    $stmt->close();
                    // BACKGROUND PROCESS TO COMPLETE NEW STUDENT PROCESSES
                    // $class = str_replace(" ", "*", $class);
                    // shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'addNewStudent' '".$studentNumber."' '".$class."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                    $reply = $this->backgroundProcessCreateAssessmentNewStudent($studentNumber, $class);
                    return "true";
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }

        function backgroundProcessCreateAssessmentNewStudent($studentNumber, $class){
            $stmt = $this->uconn->prepare("SELECT classes.class_code FROM classes WHERE classes.class_name = ?");
            $stmt->bind_param("s", $class);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            $classCode = $row["class_code"];
            $stmt = $this->uconn->prepare("SELECT subjectCode  FROM subjects WHERE subjectClass = ?");
            $stmt->bind_param("s", $class);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            foreach($stmt_result as $key => $value){
                $stmt = $this->uconn->prepare("SELECT timeTutor FROM schedule_timings WHERE timeSubject = ? AND timeClass = ?");
                $stmt->bind_param("ss", $value["subjectCode"], $classCode);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $row = array_shift($stmt_result);
                    $tutor = $row["timeTutor"];
                }else{
                    $tutor = "";
                }
                $stmt = $this->uconn->prepare("SELECT id FROM assessments WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
                $stmt->bind_param("ssss", $studentNumber, $value["subjectCode"], $academicSession, $academicTerm);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) == 0){
                    $date = Date("Y-m-d");
                    $stmt = $this->uconn->prepare("INSERT INTO assessments (student_no, class_code, subject_code, academic_session, tutor, academic_term, date_created) VALUES (?,?,?,?,?,?,?)");
                    $stmt->bind_param("sssssss", $studentNumber, $classCode, $value["subjectCode"], $academicSession, $tutor, $academicTerm, $date);
                    $stmt->execute();
                }
            }
        }

        
        function newStaffNumber(){
            $end = $this->getLastNumber("teachers") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $domain_name =  $this->getSchoolDetails()->domain_name;
            $newNum = date("Y")."/Stff"."/".ucfirst($domain_name)."/".$end;
            return $newNum;
        }

        function newSubjectNumber(){
            $end = $this->getLastNumber("subjects") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $newNum = "Subject".$end;
            return $newNum;
        }

        function newClassNumber(){
            $end = $this->getLastNumber("classes") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $newNum = "Class".$end;
            return $newNum;
        }

        function addNewSubject($code, $name, $classes){
            $totalNumber = count($classes);
            $count = 0;
            $subjectCodeArray = array();
            $name = $this->removeAccents($name);
            foreach ($classes as $key => $value) { 
                $newVal = $this->getLastNumber("subjects") + 1;
                if($newVal < 10){
                    $code = "subject00".$newVal;
                }else{
                    if($newVal < 100){
                        $code = "subject0".$newVal;
                    }else{
                        $code = "subject".$newVal;
                    }
                }
                $stmt = $this->uconn->prepare("SELECT id FROM subjects WHERE subjectName = ? AND subjectClass = ?");
                $stmt->bind_param("ss", strtolower($name), strtolower($value));
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    return "exist";
                }else{
                    $stmt = $this->uconn->prepare("INSERT INTO subjects (subjectCode, subjectName, subjectClass) VALUES (?,?,?)");
                    $stmt->bind_param("sss", strtolower($code), strtolower($name), strtolower($value));
                    $stmt->execute();
                    $subjectCodeArray[] = $code;
                    $count += 1;
                }
            }
            if($count == $totalNumber){
                // BACKGROUND PROCESS TO COMPLETE NEW SUBJECT PROCESSES
                // $subjectCodeString = implode("*", $subjectCodeArray);
                // shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'addNewSubject' '".$subjectCodeString."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                $reply = $this->backgroundProcessUpdateAssessmentNewSubject($subjectCodeArray);
                return true;
            }else{
                return false;
            }
        }

        function addNewStaff($staffNumber, $staffName, $staffCountry, $staffState, $staffCity, $staffQualification, $staffGender, $staffMaritalStatus, $staffAddress, $staffDepartment, $staffPhone, $staffEmail, $staffSubjects, $filename){
            $date = date("Y-m-d");
            $staffName = $this->removeAccents($staffName);
            if(!empty($staffEmail)){
                $ifExist = $this->checkIfExist("teachers", "id", "email", $staffEmail);
            }else{
                $ifExist = false;
            }
            if($ifExist === true){
                return "exist";
            }else{
                $password = $staffPhone;
                $staffNumber = $this->verifyLastNumber($staffNumber, "teachers");
                $stmt = $this->uconn->prepare("INSERT INTO teachers (fullname, staff_number, nationality, state, city, highest_qualification, gender, m_status, address, department, phone, email, profilePhoto, date_added, password) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssssssssssss", strtolower($staffName), strtolower($staffNumber), strtolower($staffCountry), strtolower($staffState), strtolower($staffCity), strtolower($staffQualification), strtolower($staffGender), strtolower($staffMaritalStatus), strtolower($staffAddress), strtolower($staffDepartment), strtolower($staffPhone), strtolower($staffEmail), strtolower($filename), strtolower($date), password_hash($password, PASSWORD_BCRYPT));
                if($stmt->execute()){
                    // BACKGROUND PROCESS TO COMPLETE NEW STAFF PROCESSES
                    // $stringSubject = implode("*", $staffSubjects);
                    // shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'addNewStaff' '".$staffNumber."' '".$stringSubject."' '".$password."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                    $reply = $this->backgroundProcessAddStaff($staffNumber, $password, $staffSubjects);
                    return "true";
                }else{
                    $stmt->close();
                    return "false";
                }
            }
        }

        function newParentNumber(){
            $end = $this->getLastNumber("parents") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $domain_name =  $this->getSchoolDetails()->domain_name;
            $newNum = date("Y")."/Prnt"."/".ucfirst($domain_name)."/".$end;
            return $newNum;
        }

        function newPickupNumber(){
            $end = $this->getLastNumber("childpickup") + 1;
            switch (count($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $domain_name =  $this->getSchoolDetails()->domain_name;
            $newNum = date("Y")."/Pkup"."/".ucfirst($domain_name)."/".$end;
            return $newNum;
        }

        function addNewParent($parentNumber, $parentName, $parentCountry, $parentState, $parentCity, $parentRelation, $parentGender, $parentMaritalStatus, $parentAddress, $parentOccupation, $parentPhone, $parentEmail, $filename){
            $parentName = $this->removeAccents($parentName);
            if(!empty($parentEmail)){
                $ifExist = $this->checkIfExist("parents", "id", "email", $parentEmail);
            }else{
                $ifExist = false;
            }
            if($ifExist === true){
                return "exist";
            }else{
                $parentNumber = $this->verifyLastNumber($parentNumber, "parents");
                $stmt = $this->uconn->prepare("INSERT INTO parents (fullname, guardian_number, nationality, state, city, relation_student, gender, m_status, occupation, phone, email, profilePhoto, address) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssssssssss", strtolower($parentName), strtolower($parentNumber), strtolower($parentCountry), strtolower($parentState), strtolower($parentCity), strtolower($parentRelation), strtolower($parentGender), strtolower($parentMaritalStatus), strtolower($parentOccupation), strtolower($parentPhone), strtolower($parentEmail), strtolower($filename), strtolower($parentAddress));
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    return "false";
                }
            }
        }

        function createClasses($classCode, $className, $classSubjects){
            $ifExist = $this->checkIfExist("classes", "id", "class_name", $className);
            $className = $this->removeAccents($className);
            if($ifExist === true){
                return "exist";
            }else{
                $date = Date("Y-m-d");
                $stmt = $this->uconn->prepare("INSERT INTO classes (class_code, class_name, date_created) VALUES (?,?,?)");
                $stmt->bind_param("sss", $classCode, $className, $date);
                if($stmt->execute()){
                    if(count($classSubjects) > 0){
                        foreach ($classSubjects as $key => $value) {
                            # code...
                            $stmt = $this->uconn->prepare("SELECT subjectName FROM subjects WHERE subjectCode = ?");
                            $stmt->bind_param("s", $value);
                            $stmt->execute();
                            $stmt_result = $this->get_result($stmt);
                            $subjectName = array_shift($stmt_result)["subjectName"];
                            $stmt = $this->uconn->prepare("INSERT INTO subjects (subjectCode, subjectName, subjectClass) VALUES (?,?,?)");
                            $stmt->bind_param("sss", $value, $subjectName, $className);
                            $stmt->execute();
                        }
                    }
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }

            }
        }

        function addNewPickup($pickupNumber, $pickupName, $pickupCountry, $pickupState, $pickupCity, $pickupRelation, $pickupGender, $pickupMaritalStatus, $pickupAddress, $pickupOccupation, $pickupPhone, $pickupEmail, $pickupStudent, $filename){
            $stmt = $this->uconn->prepare("SELECT id FROM childpickup WHERE studentNumber = ? AND email = ?");
            $pickupName = $this->removeAccents($pickupName);
            $stmt->bind_param("ss", $pickupStudent, $pickupEmail);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $name = $_SESSION["fullname"];
                $pickupNumber = $this->verifyLastNumber($pickupNumber, "childpickup");
                $stmt = $this->uconn->prepare("INSERT INTO childpickup (fullname, pickupNumber, nationality, state, city, relation_student, gender, m_status, occupation, phone, email, studentNumber, profilePhoto, address, added_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssssssssssss", $pickupName, $pickupNumber, $pickupCountry, $pickupState, $pickupCity, $pickupRelation, $pickupGender, $pickupMaritalStatus, $pickupOccupation, $pickupPhone, $pickupEmail, $pickupStudent, $filename, $pickupAddress, $name);
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    return "false";
                }
            }
        }

        function updateTiming($timeTableSubject, $timetableTutor, $timetableClass, $timetableDay, $timetableTime, $session, $term){
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $stmt = $this->uconn->prepare("UPDATE schedule_timings SET timeSubject =  ?, timeTutor = ? WHERE timeClass = ? AND timeDay = ? AND timeTime = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssssss", $timeTableSubject, $timetableTutor, $timetableClass, $timetableDay, $timetableTime, $current_academic_session, $current_academic_term);
            if($stmt->execute()){
                // BACKGROUND PROCESS TO UPDATE TUTOR IN TIMETABLE PROCESSES
                //shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'updateTimetable' '".$timetableTutor."' '".$timeTableSubject."' '".$timetableClass."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                // PROCESS ENDS HERE
                $reply = $this->backgroundProcessUpdateTutorAssessment($timetableTutor, $timeTableSubject, $timetableClass);
                $stmt = $this->uconn->prepare("UPDATE subjectTeachers SET staffCode = ? WHERE classCode = ? AND subjectCode = ?");
                $stmt->bind_param("sss", strtolower($timetableTutor), strtolower($timetableClass), strtolower($timeTableSubject));
                $stmt->execute();
                return true;
                $stmt->close();
            }
        }


        function updateTimingStaff($timeTableSubject, $timetableTutor, $timetableClass, $timetableDay, $timetableTime, $session, $term){
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $stmt = $this->uconn->prepare("UPDATE schedule_timings SET timeSubject =  ?, timeClass = ? WHERE timeTutor = ? AND timeDay = ? AND timeTime = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssssss", $timeTableSubject, $timetableClass, $timetableTutor, $timetableDay, $timetableTime, $current_academic_session, $current_academic_term);
            if($stmt->execute()){
                return true;
                $stmt->close();
            }
        }


        function deleteTiming($timeTableSubject, $timetableClass, $timetableDay, $timetableTime, $session, $term){
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeClass = ? AND timeDay = ? AND timeTime = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssss", $timetableClass, $timetableDay, $timetableTime, $current_academic_session, $current_academic_term);
            if($stmt->execute()){
                $timetableTutor = "";
                // BACKGROUND PROCESS TO UPDATE TUTOR IN TIMETABLE PROCESSES
                //shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'updateTimetable' '".$timetableTutor."' '".$timeTableSubject."' '".$timetableClass."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                // PROCESS ENDS HERE
                $reply = $this->backgroundProcessUpdateTutorAssessment($timetableTutor, $timeTableSubject, $timetableClass);
                $stmt = $this->uconn->prepare("DELETE FROM subjectTeachers WHERE staffCode = ? AND classCode = ? AND subjectCode = ?");
                $stmt->bind_param("sss", strtolower($timetableTutor), strtolower($timetableClass), strtolower($timeTableSubject));
                $stmt->execute();
                return true;
                $stmt->close();
            }
        }


        function deleteTimingStaff($timetableTutor, $timetableDay, $timetableTime, $session, $term){
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeTutor = ? AND timeDay = ? AND timeTime = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssss", $timetableTutor, $timetableDay, $timetableTime, $current_academic_session, $current_academic_term);
            if($stmt->execute()){
                return true;
                $stmt->close();
            }
        }


        function addNewTiming($timeTableSubject, $timetableTutor, $timetableClass, $timetableDay, $timetableTime, $session, $term){
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $dateCreated = date("Y-m-d H:i:s");
            $newVal = $this->getLastNumber("schedule_timings") + 1;
            if($newVal < 10){
                $timingCode = "timing00".$newVal;
            }else{
                if($newVal < 100){
                    $timingCode = "timing0".$newVal;
                }else{
                    $timingCode = "timing".$newVal;
                }
            }
            $stmt1 = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeClass = ? AND timeDay = ? AND timeTime = ? AND academic_session = ? AND academic_term = ?");
            $stmt1->bind_param("sssss", $timetableClass, $timetableDay, $timetableTime, $current_academic_session, $current_academic_term);
            $stmt1->execute();
            $stmt1_result = $this->get_result($stmt1);
            $row = array_shift($stmt1_result);
            if(count($row) < 1){
                $stmt = $this->uconn->prepare("INSERT INTO schedule_timings (timeCode, timeDay, timeTime, timeSubject, timeClass, timeTutor, academic_session, academic_term, dateCreated) VALUES (?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssssss", strtolower($timingCode), strtolower($timetableDay), strtolower($timetableTime), strtolower($timeTableSubject), strtolower($timetableClass), strtolower($timetableTutor), strtolower($current_academic_session), strtolower($current_academic_term), strtolower($dateCreated));
                if($stmt->execute()){
                    // BACKGROUND PROCESS TO COMPLETE NEW STUDENT PROCESSES
                    //shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'updateTimetable' '".$timetableTutor."' '".$timeTableSubject."' '".$timetableClass."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                    // PROCESS ENDS HERE
                    $reply = $this->backgroundProcessUpdateTutorAssessment($timetableTutor, $timeTableSubject, $timetableClass);
                    $stmt = $this->uconn->prepare("SELECT id FROM subjectTeachers WHERE classCode = ? AND subjectCode = ?");
                    $stmt->bind_param("ss", strtolower($timetableClass), strtolower($timeTableSubject));
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    if(count($stmt_result) > 0){
                        $id = array_shift($stmt_result)["id"];
                        $stmt = $this->uconn->prepare("UPDATE subjectTeachers SET staffCode = ? WHERE id = ?");
                        $stmt->bind_param("si", strtolower($timetableTutor), $id);
                        $stmt->execute();
                    }else{
                        $stmt = $this->uconn->prepare("INSERT INTO subjectTeachers (classCode, subjectCode, staffCode, date_assigned) VALUES (?,?,?,?)");
                        $stmt->bind_param("ssss", strtolower($timetableClass), strtolower($timeTableSubject), strtolower($timetableTutor), $dateCreated);
                        $stmt->execute();
                    }
                    return true;
                    $stmt->close();
                }    
            }
        }

        function backgroundProcessUpdateTutorAssessment($timetableTutor, $timeTableSubject, $timetableClass){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            $stmt = $this->uconn->prepare("UPDATE assessments SET tutor = ? WHERE class_code = ? AND subject_code = ? AND academic_term = ? AND academic_session = ?");
            $stmt->bind_param("sssss", $timetableTutor, $timetableClass, $timeTableSubject, $academicTerm, $academicSession);
            $stmt->execute();
        }

        function DeleteUser($numberToDelete, $whoToDelete){
            switch ($whoToDelete) {
                case 'Staff':
                    # code...
                    $reply = $this->deleteStaff($numberToDelete);
                    return $reply;
                    break;
                case 'Student':
                    # code...
                    $reply = $this->deleteStudent($numberToDelete);
                    return $reply;
                    break;
                case 'Parent':
                    # code...
                    $reply = $this->deleteParent($numberToDelete);
                    return $reply;
                    break;
                case 'Pickup':
                    # code...
                    $reply = $this->deletePickup($numberToDelete);
                    return $reply;
                    break;
                default:
                    # code...
                    return 0;
                    break;
            }
        }

        function deletePickup($numberToDelete){
            $ifExist = $this->checkIfExist("childpickup", "id", "pickupNumber", $numberToDelete);
            if($ifExist === true){
                $stmt = $this->uconn->prepare("DELETE FROM childpickup WHERE pickupNumber = ?");
                $stmt->bind_param("s", $numberToDelete);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM pickuprecords WHERE pickupNumber = ?");
                    $stmt->bind_param("s", $numberToDelete);
                    if($stmt->execute()){
                        $stmt->close();
                        return true;
                    }else{
                        $stmt->close();
                        return 0;
                    }
                }
            }else{
                return "not found";
            }
        }

        function deleteParent($numberToDelete){
            $ifExist = $this->checkIfExist("parents", "id", "guardian_number", $numberToDelete);
            if($ifExist === true){
                $stmt = $this->uconn->prepare("DELETE FROM parents WHERE guardian_number = ?");
                $stmt->bind_param("s", $numberToDelete);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM pickuprecords WHERE pickupNumber = ?");
                    $stmt->bind_param("s", $numberToDelete);
                    if($stmt->execute()){
                        $stmt->close();
                        return true;
                    }else{
                        $stmt->close();
                        return 0;
                    }
                }
            }else{
                return "not found";
            }
        }

        function deleteStudent($numberToDelete){
            $ifExist = $this->checkIfExist("students", "id", "student_number", $numberToDelete);
            if($ifExist === true){
                $stmt = $this->uconn->prepare("SELECT father_phone, mother_phone FROM students WHERE student_number = ?");
                $stmt->bind_param("s", $numberToDelete);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                $row = array_shift($stmt_result);
                if($row["father_phone"] != ""){
                    $stmt2 = $this->uconn->prepare("SELECT id FROM students WHERE student_number != ? AND father_phone = ?");
                    $stmt2->bind_param("ss", $numberToDelete, $row["father_phone"]);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    if(count($stmt2_result) == 0){
                        $stmt2 = $this->uconn->prepare("DELETE FROM parents WHERE phone = ?");
                        $stmt2->bind_param("s", $row["father_phone"]);
                        $stmt2->execute();
                        $stmt2->close();
                    }

                }
                if($row["mother_phone"] != ""){
                    $stmt2 = $this->uconn->prepare("SELECT id FROM students WHERE student_number != ? AND mother_phone = ?");
                    $stmt2->bind_param("ss", $numberToDelete, $row["mother_phone"]);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    if(count($stmt2_result) == 0){
                        $stmt2 = $this->uconn->prepare("DELETE FROM parents WHERE phone = ?");
                        $stmt2->bind_param("s", $row["mother_phone"]);
                        $stmt2->execute();
                        $stmt2->close();
                    }

                }
                $stmt = $this->uconn->prepare("DELETE FROM students WHERE student_number = ?");
                $stmt->bind_param("s", $numberToDelete);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM assessments WHERE student_no = ?");
                    $stmt->bind_param("s", $numberToDelete);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("DELETE FROM attendancerecord WHERE attendanceNumber = ?");
                        $stmt->bind_param("s", $numberToDelete);
                        if($stmt->execute()){
                            $stmt = $this->uconn->prepare("DELETE FROM childpickup WHERE studentNumber = ?");
                            $stmt->bind_param("s", $numberToDelete);
                            if($stmt->execute()){
                                $stmt = $this->uconn->prepare("DELETE FROM pickuprecords WHERE studentNumber = ?");
                                $stmt->bind_param("s", $numberToDelete);
                                if($stmt->execute()){
                                    $stmt->close();
                                    return true;
                                }else{
                                    $stmt->close();
                                    return 0;
                                }
                            }
                        }
                    }
                }
            }else{
                return "not found";
            }
        }

        function deleteStaff($numberToDelete){
            $ifExist = $this->checkIfExist("teachers", "id", "staff_number", $numberToDelete);
            if($ifExist === true){
                $stmt = $this->uconn->prepare("DELETE FROM teachers WHERE staff_number = ?");
                $stmt->bind_param("s", $numberToDelete);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM attendancerecord WHERE attendanceNumber = ?");
                    $stmt->bind_param("s", $numberToDelete);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeTutor = ?");
                        $stmt->bind_param("s", $numberToDelete);
                        if($stmt->execute()){
                            $stmt = $this->uconn->prepare("DELETE FROM subjectTeachers WHERE staffCode = ?");
                            $stmt->bind_param("s", $numberToDelete);
                            if($stmt->execute()){
                                $stmt->close();
                                return true;
                            }else{
                                $stmt->close();
                                return 0;
                            }
                        }
                    }
                }
            }else{
                return "not found";
            }
        }

        function deleteClass($classes){
            foreach ($classes as $key => $classCode) {
                # code...
                $className = $this->getClassName($classCode);
                $stmt = $this->uconn->prepare("DELETE FROM classes WHERE class_code = ?");
                $stmt->bind_param("s", $classCode);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM assessments WHERE class_code = ?");
                    $stmt->bind_param("s", $classCode);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeClass = ?");
                        $stmt->bind_param("s", $classCode);
                        if($stmt->execute()){
                            $stmt = $this->uconn->prepare("DELETE FROM subjectTeachers WHERE classCode = ?");
                            $stmt->bind_param("s", $classCode);
                            if($stmt->execute()){
                                $stmt = $this->uconn->prepare("DELETE FROM subjects WHERE subjectClass = ?");
                                $stmt->bind_param("s", $className);
                                if($stmt->execute()){
                                    $stmt->close();
                                    return true;
                                }else{
                                    $stmt->close();
                                    return false;    
                                }
                            }
                        }
                    }
                }
            }
        }

        function renameClass($formerClassName, $newClassName){
            $ifExist = $this->checkIfExist("classes", "id", "class_name", $newClassName);
            if($ifExist === true){
                return "exist";
            }else{
                $stmt = $this->uconn->prepare("SELECT class_name FROM classes WHERE class_code = ?");
                $stmt->bind_param("s", $formerClassName);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                $className = array_shift($stmt_result)["class_name"];
                $stmt = $this->uconn->prepare("UPDATE classes SET class_name = ? WHERE class_code = ?");
                $stmt->bind_param("ss", $newClassName, $formerClassName);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("UPDATE students SET present_class = ? WHERE present_class = ?");
                    $stmt->bind_param("ss", $newClassName, $className);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("UPDATE payed_student SET className = ? WHERE className = ?");
                        $stmt->bind_param("ss", $newClassName, $className);
                        if($stmt->execute()){
                            $stmt->close();
                            return "true";
                        }else{
                            $stmt->close();
                            return "false";
                        }
                    }else{
                        $stmt->close();
                        return "false";
                    }
                }else{
                    $stmt->close();
                    return "false";
                }
            }
        }

        function deleteSubject($delSubjects){
            foreach ($delSubjects as $key => $subjectCode) {
                # code...
                $stmt = $this->uconn->prepare("DELETE FROM subjects WHERE subjectCode = ?");
                $stmt->bind_param("s", $subjectCode);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("DELETE FROM assessments WHERE subject_code = ?");
                    $stmt->bind_param("s", $subjectCode);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeSubject = ?");
                        $stmt->bind_param("s", $subjectCode);
                        if($stmt->execute()){
                            $stmt = $this->uconn->prepare("DELETE FROM subjectTeachers WHERE subjectCode = ?");
                            $stmt->bind_param("s", $subjectCode);
                            $stmt->execute();
                        }
                    }
                }
            }
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }

        function updateAcademicInfo($academicSession, $academicTerm, $sessionEndDate){
            $stmt = $this->uconn->prepare("UPDATE settings SET academic_session = ?, academic_term = ?, next_resumption_date = ?");
            $stmt->bind_param("sss", $academicSession, $academicTerm, $sessionEndDate);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }

        function publishQuickNotice($broadcastMessage){
            $stmt = $this->uconn->prepare("UPDATE settings SET quick_notice = ?");
            $stmt->bind_param("s", $broadcastMessage);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }

        function changePassword($oldPassword, $newPassword){
            $stmt = $this->dconn->prepare("SELECT password FROM staffs WHERE uniqueNumber = ?");
            $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $pass = array_shift($stmt_result)["password"];
            if(password_verify($oldPassword, $pass)){
                // OLD PASSWORD MATCH
                $stmt = $this->dconn->prepare("UPDATE staffs SET password = ? WHERE uniqueNumber = ?");
                $stmt->bind_param("ss", password_hash(strtolower($newPassword), PASSWORD_BCRYPT), $_SESSION["uniqueNumber"]);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("UPDATE administrators SET password = ? WHERE adminNumber = ?");
                    $stmt->bind_param("ss", password_hash(strtolower($newPassword), PASSWORD_BCRYPT), $_SESSION["uniqueNumber"]);
                    if($stmt->execute()){
                        $stmt = $this->uconn->prepare("UPDATE teachers SET password = ? WHERE staff_number = ?");
                        $stmt->bind_param("ss", password_hash(strtolower($newPassword), PASSWORD_BCRYPT), $_SESSION["uniqueNumber"]);
                        if($stmt->execute()){
                            $stmt->close();
                            return "changed";
                        }else{
                            $stmt->close();
                            return "error";
                        }
                    }else{
                        $stmt->close();
                        return "error";
                    }
                }else{
                    $stmt->close();
                    return "error";
                }
            }else{
                $stmt->close();
                return "wrongpassword";
            }
        }

        // BACKGROUND PROCESSES GOES HERE
        function backgroundProcessAddStaff($staffNumber, $password, $staffSubjects){
            if(count($staffSubjects) != 0){
                foreach ($staffSubjects as $key => $value) {
                    # code...
                    $stmt2 = $this->uconn->prepare("SELECT classes.class_code, subjects.subjectClass FROM classes INNER JOIN subjects ON subjects.subjectClass = classes.class_name WHERE subjects.subjectCode = ?");
                    $stmt2->bind_param("s", $value);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    $row = array_shift($stmt2_result);
                    $date = Date("Y-m-d");
                    $stmt3 = $this->uconn->prepare("INSERT INTO subjectTeachers (classCode, subjectCode, staffCode, date_assigned) VALUES (?,?,?,?)");
                    $stmt3->bind_param("ssss", $row["class_code"], $value, $staffNumber, $date);
                    $stmt3->execute();
                    $stmt2->close();
                    $stmt3->close();
                }
            }
            $stmt = $this->uconn->prepare("SELECT fullname, email, password, department, phone FROM teachers WHERE staff_number = ?");
            $stmt->bind_param("s", $staffNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $stmt = $this->dconn->prepare("INSERT INTO staffs (fullname, email, uniqueNumber, password, school_domain, role) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param("ssssss", strtolower($row["fullname"]), strtolower($row["email"]), strtolower($staffNumber), $row["password"], $_SESSION["folderName"], $row["department"]);
            $stmt->execute();
            $allowedArray = ["tutor", "form teacher", "accountant", "librarian", "admin", "principal", "director", "store keeper"];
            if(in_array($row["department"], $allowedArray)){
                $message = "Hi, ".ucwords($row["fullname"]).", your staff account for ".$this->getSchoolDetails()->school_name." on https://www.ClassWorks.xyz has been created. Your login details are; \n Email:".$row["email"]."\n Staff Number: ".ucwords($staffNumber)."\n Password: ".$password.". Your password can be changed in your account. \n Best Regards";
                if(!empty($row["phone"])){
                    $reply = $this->sendSMS($message, $this->getSchoolDetails()->domain_name, $row["phone"]);
                    if($reply === true){
                        $stmt->close();
                        return "true";
                    }else{
                        $stmt->close();
                        return $reply;
                    }
                }
            }else{
                $stmt->close();
                return "true";
            }
        }

        function backgroundProcessUpdateAssessmentNewSubject($subjectCodeArray){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            foreach($subjectCodeArray as $key => $value){
                $stmt = $this->uconn->prepare("SELECT subjects.subjectClass, classes.class_code FROM subjects INNER JOIN classes ON classes.class_name = subjects.subjectClass WHERE subjects.subjectCode = ?");
                $stmt->bind_param("s", $value);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                $row = array_shift($stmt_result);
                $classCode = $row["class_code"];
                $subjectClass = $row["subjectClass"];
                $subjectCode = $value;
                // GET ALL STUDENT IN CURRENT CLASS LOOP
                $studentsArray = array();
                $stmt = $this->uconn->prepare("SELECT student_number FROM students WHERE present_class = ?");
                $stmt->bind_param("s", $subjectClass);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                //$row = array_shift($stmt_result);
                foreach($stmt_result as $key => $value){
                    $studentNumber = $value["student_number"];
                    $stmt = $this->uconn->prepare("SELECT id FROM assessments WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
                    $stmt->bind_param("ssss", $studentNumber, $subjectCode, $academicSession, $academicTerm);
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    if(count($stmt_result) == 0){
                        $date = Date("Y-m-d");
                        $stmt = $this->uconn->prepare("INSERT INTO assessments (student_no, class_code, subject_code, academic_session, academic_term, date_created) VALUES (?,?,?,?,?,?)");
                        $stmt->bind_param("ssssss", $value["student_number"], $classCode, $subjectCode, $academicSession, $academicTerm, $date);
                        $stmt->execute();
                        $stmt->close();
                        return true;
                    }
                }
            }
        }

        function createFamily($familyName, $studentFatherContact, $studentMotherContact){
            if(!empty($studentFatherContact)){
                $ifExist = $this->checkIfExist("family", "id", "father_phone", $studentFatherContact);
            } else{
                $ifExist = false;
            }  
            if($ifExist === true){
                return "fatherexist";
            }
            if(!empty($studentMotherContact)){
                $ifExist = $this->checkIfExist("family", "id", "mother_phone", $studentMotherContact);
            }else{
                $ifExist = false;
            }
            if($ifExist === true){
                return "motherexist";
            }
            $dateAdded = Date("Y-m-d");
            $stmt = $this->uconn->prepare("INSERT INTO family (family_name, father_phone, mother_phone, date_created) VALUES (?,?,?,?)");
            $stmt->bind_param("ssss", $familyName, $studentFatherContact, $studentMotherContact, $dateAdded);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }

        function updateEnrollmentTable($enrollYear, $enrollBatch, $enrollClasses, $enrollStartDate, $enrollEndDate, $enrollFeeState, $enrollFeeAmount, $enrollslot){
            $enrollClasses = json_encode($enrollClasses);
            $stmt = $this->uconn->prepare("SELECT COUNT(id) AS num FROM enrol_settings");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(array_shift($stmt_result)["num"] > 0){
                // UPDATE TABLE
                $id = 1;
                $stmt = $this->uconn->prepare("UPDATE enrol_settings SET enrol_session = ?, enrol_batch = ?, enrol_classes = ?, enrol_startDate = ?, enrol_endDate = ?, enrol_feeState = ?, enrol_amount = ?, enrol_slot = ? WHERE id = ?");
                $stmt->bind_param("sssssiiii", $enrollYear, $enrollBatch, $enrollClasses, $enrollStartDate, $enrollEndDate, $enrollFeeState, $enrollFeeAmount, $enrollslot, $id);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }else{
                // INSERT
                $stmt = $this->uconn->prepare("INSERT INTO enrol_settings (enrol_session, enrol_batch, enrol_classes, enrol_startDate, enrol_endDate, enrol_feeState, enrol_amount, enrol_slot) VALUES (?,?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssiii", $enrollYear, $enrollBatch, $enrollClasses, $enrollStartDate, $enrollEndDate, $enrollFeeState, $enrollFeeAmount, $enrollslot);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }

        function addNewInventory($ItemName, $ItemSize, $ItemColor, $ItemQuantity, $ItemPrice, $ItemLocation){
            $stmt = $this->uconn->prepare("SELECT id FROM inventory_items WHERE itemName = ? AND itemSize = ? AND itemColor = ?");
            $stmt->bind_param("sss", $ItemName, $ItemSize, $ItemColor);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                return "exist";
            }else{
                $date = DATE("Y-m-d");
                $end = $this->getLastNumber("inventory_items") + 1;
                switch (strlen($end)) {
                    case 1:
                        # code...
                        $end = "00".$end;
                        break;
                    case 2:
                        # code...
                        $end = "0".$end;
                        break;
                    case 3:
                        # code
                        $end = $end;
                        break;
                    
                    default:
                        # code...
                        break;
                }
                $itemNum = "item".$end;
                $stmt = $this->uconn->prepare("INSERT INTO inventory_items (itemNo, itemName, itemSize, itemColor, itemQuantity, itemPrice, itemLocation, added_by, added_date) VALUES (?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("ssssiisss", $itemNum, $ItemName, $ItemSize, $ItemColor, $ItemQuantity, $ItemPrice, $ItemLocation, $_SESSION["fullname"], $date);
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    $stmt->close();
                    return "false";
                }
            }
        }

        function inventoryConfigure($lowStockCount, $outStockCount){
            $stmt = $this->uconn->prepare("SELECT * FROM inventory_settings");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt = $this->uconn->prepare("UPDATE inventory_settings SET lowStock = ?, outStock = ?");
                $stmt->bind_param("ii", $lowStockCount, $outStockCount);
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    $stmt->close();
                    return "false";
                }
            }else{
                $stmt = $this->uconn->prepare("INSERT INTO inventory_settings (lowStock, outStock) VALUES (?,?)");
                $stmt->bind_param("ii", $lowStockCount, $outStockCount);
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    $stmt->close();
                    return "false";
                }
            }
        }

        function getStockItem(){
            $stmt = $this->uconn->prepare("SELECT DISTINCT itemName FROM inventory_items WHERE itemQuantity > 0");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $items = array();
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->itemName = $row["itemName"];
                    $items[] = $obj;
                }
                $stmt->close();
                return $items;
            }else{
                $stmt->close();
                return $items;
            }
        }

        function recordCollect($itemName, $itemColor, $itemSize, $itemQuantity, $CollectedBy){
            $stmt = $this->uconn->prepare("SELECT itemQuantity, itemNo FROM inventory_items WHERE itemName = ? AND itemSize = ? AND itemColor = ?");
            $stmt->bind_param("sss", $itemName, $itemSize, $itemColor);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $qty = $row["itemQuantity"];
            if($qty > $itemQuantity){
                $date = Date("Y-m-d");
                $stmt = $this->uconn->prepare("INSERT INTO inventory_record (itemNo, itemQuantity, collectedBy, issuedBy, date_collected) VALUES (?,?,?,?,?)");
                $stmt->bind_param("sisss", $row["itemNo"], $itemQuantity, $CollectedBy, $_SESSION["fullname"], $date);
                if($stmt->execute()){
                    $newQty = $qty - $itemQuantity;
                    $stmt = $this->uconn->prepare("UPDATE inventory_items SET itemQuantity = ? WHERE itemNo = ?");
                    $stmt->bind_param("is", $newQty, $row["itemNo"]);
                    $stmt->execute();
                    $stmt->close();
                    return "true";
                }else{
                    $stmt->close();
                    return "false";
                }
            }else{
                $stmt->close();
                return "insufficient";
            }
        }

        function staffchangePassword($staffId, $password){
            $stmt = $this->uconn->prepare("UPDATE teachers SET password = ? WHERE staff_number = ?");
            $stmt->bind_param("ss", password_hash(strtolower($password), PASSWORD_BCRYPT), $staffId);
            if($stmt->execute()){
                $stmt = $this->uconn->prepare("UPDATE administrators SET password = ? WHERE adminNumber = ?");
                $stmt->bind_param("ss", password_hash(strtolower($password), PASSWORD_BCRYPT), $staffId);
                $stmt->execute();
                $stmt = $this->dconn->prepare("UPDATE staffs SET password = ? WHERE uniqueNumber = ?");
                $stmt->bind_param("ss", password_hash(strtolower($password), PASSWORD_BCRYPT), $staffId);
                $stmt->execute();
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }

        function addShopProduct($itemName, $itemQuantity, $itemPrice){
            $stmt = $this->uconn->prepare("SELECT product_quantity FROM shop_product WHERE product_name = ? AND product_price = ?");
            $stmt->bind_param("si", $itemName, $itemPrice);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $newQuantity = array_shift($stmt_result)["product_quantity"] + $itemQuantity;
                $stmt = $this->uconn->prepare("UPDATE shop_product SET product_quantity = ? WHERE product_name = ?");
                $stmt->bind_param("is", $newQuantity, $itemName);
                if($stmt->execute()){
                    $stmt->close();
                    return "updated";
                }else{
                    $stmt->close();
                    return false;
                }
            }else{
                $date = DATE("Y-m-d");
                $end = $this->getLastNumber("shop_product") + 1;
                switch (strlen($end)) {
                    case 1:
                        # code...
                        $end = "00".$end;
                        break;
                    case 2:
                        # code...
                        $end = "0".$end;
                        break;
                    case 3:
                        # code
                        $end = $end;
                        break;
                    
                    default:
                        # code...
                        break;
                }
                $product_id = "Product".$end;
                $stmt = $this->uconn->prepare("INSERT INTO shop_product (product_id, product_name, product_quantity, product_price, date_added, added_by) VALUES (?,?,?,?,?,?)");
                $stmt->bind_param("ssiiss", $product_id, $itemName, $itemQuantity, $itemPrice, $date, $_SESSION["fullname"]);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }

        function configureTuckshop($alertWalletBalance, $amountToAlert, $bankName, $accountName, $accountNumber){
            $stmt = $this->uconn->prepare("SELECT accountState, accountNumber, accountCode FROM tuckshop_setting");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $bankAccountName = $accountName;
            $accountName = $this->getSchoolDetails()->school_name." ( Tuck Shop )";
            $key = $this->getSchoolDetails()->keys["paystack_secret"];
            if(count($stmt_result) > 0){
                // RECORD ALREADY EXIST UPDATE
                $row = array_shift($stmt_result);
                $accountCode = $row["accountCode"];
                $action = 1;
                include ("epaymentUpdate.php");
                if($result->status == 1 && $result->message == "Subaccount updated"){
                    $accountCode = $result->data->subaccount_code;
                    $accountState = 1;
                    $stmt = $this->uconn->prepare("UPDATE tuckshop_setting SET alertWalletBalance = ?, amountToAlert = ?, bankName = ?, accountName = ?, accountNumber = ?, accountCode = ?");
                    $stmt->bind_param("iissss", $alertWalletBalance, $amountToAlert, $bankName, $bankAccountName, $accountNumber, $accountCode);
                    if($stmt->execute()){
                        $stmt->close();
                        return "updated";
                    }else{
                        $stmt->close();
                        return "error";
                    }
                }else{
                    return "updateError";
                }
            }else{
                if(!empty($bankName) && !empty($accountNumber)){
                    $action = 1;
                    include ("epaymentSetup.php");
                    if($result->status == true && $result->message == "Subaccount created"){
                        $accountCode = $result->data->subaccount_code;
                        $stmt = $this->uconn->prepare("INSERT INTO tuckshop_setting (alertWalletBalance, amountToAlert, bankName, accountName, accountNumber, accountState, accountCode) VALUES (?,?,?,?,?,?,?)");
                        $stmt->bind_param("iisssis", $alertWalletBalance, $amountToAlert, $bankName, $bankAccountName, $accountNumber, $action, $accountCode);
                        if($stmt->execute()){
                            $stmt->close();
                            return "true";
                        }
                    }else{
                        $stmt->close();
                        return "setup";
                    }
                }
            }
        }

        function getFamilies(){
            $stmt = $this->uconn->prepare("SELECT id, family_name FROM family");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $families = array();
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->fullname = $row["family_name"];
                    $obj->id = $row["id"];
                    $families[] = $obj;
                }
                $stmt->close();
                return $families;
            }else{
                $stmt->close();
                return $families;
            }
        }


        function promoteClassStudents($fromClass, $toClass){
            $stmt = $this->uconn->prepare("UPDATE students SET present_class =  ?, promotion_status = 1 WHERE present_class = ? AND promotion_status = 0");
            $stmt->bind_param("ss", $toClass, $fromClass);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }


        function promoteSingleStudents($studentNumber, $toClass){
            $stmt = $this->uconn->prepare("UPDATE students SET present_class =  ?, promotion_status = 1 WHERE student_number = ?");
            $stmt->bind_param("ss", $toClass, $studentNumber);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }

        function configureExam($examClass, $examSubject, $examDuration, $examSupervisor, $startTime){
            $examStart = new DateTime($startTime);
            $examStart->modify('+ '.$examDuration.' seconds');
            $endTime = $examStart->format('H:i');
            $classCode = $this->getClassCode($examClass);
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            $stmt = $this->uconn->prepare("SELECT id FROM cbt_subjects WHERE classCode = ? AND subjectCode = ? AND term = ? AND session = ?");
            $stmt->bind_param("ssis", $classCode, $examSubject, $academicTerm, $academicSession);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt = $this->uconn->prepare("UPDATE cbt_subjects SET duration = ?, startTime = ?, endTime = ?, supervisorCode = ? WHERE classCode = ? AND subjectCode = ? AND term = ? AND session = ?");
                $stmt->bind_param("isssssis", $examDuration, $startTime, $endTime, $examSupervisor, $classCode, $examSubject, $academicTerm, $academicSession);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }else{
                $state = 1;
                $stmt = $this->uconn->prepare("INSERT INTO cbt_subjects (classCode, subjectCode, duration, supervisorCode, examState, startTime, endTime, term, session) VALUES (?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("ssisissis", $classCode, $examSubject, $examDuration, $examSupervisor, $state, $startTime, $endTime, $academicTerm, $academicSession);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }

        function getSystemGrades(){
            $stmt = $this->dconn->prepare("SELECT * FROM school_grades");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $grades = array();
            while($row = array_shift($stmt_result)){
                $grades[] = $row["grades"];
            }
            return $grades;
        }

    }

?>