<?php 
	session_start();
	include_once ("userbtn.php");
	$schoolDetails = $index->getSchoolDetails();
	$categoryNumber = count($schoolDetails->category);
	$students = $index->getIndexStudents();
	switch ($categoryNumber){
		case 1:
		$category = ucwords($schoolDetails->category[0]);
		break;
		case 2:
			$category = ucwords($schoolDetails->category[0])." and ".ucwords($schoolDetails->category[1]);
		break;
		case 3:
			$category = ucwords($schoolDetails->category[0]).", ".ucwords($schoolDetails->category[1])." and ".ucwords($schoolDetails->category[2]);
			break;
		case 4:
			$category = ucwords($schoolDetails->category[0]).", ".ucwords($schoolDetails->category[1]).", ".ucwords($schoolDetails->category[2])." and ".ucwords($schoolDetails->category[3]);
			break;
		case 5:
			$category = ucwords($schoolDetails->category[0]).", ".ucwords($schoolDetails->category[1]).", ".ucwords($schoolDetails->category[2]).", ".ucwords($schoolDetails->category[3])." and ".ucwords($schoolDetails->category[4]);
		default:
		$category = "";
	}

	switch($schoolDetails->academic_term){
        case 1:
            $term = "1st Term";
            break;
        case 2:
            $term = "2nd Term";
            break;
        case 3;
            $term = "3rd Term";
            break;
        default:
            $term = "N/A";
	}
	$session = $schoolDetails->academic_session == "" ? "N/A" : ($schoolDetails->academic_session - 1)."/".$schoolDetails->academic_session;
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <title><?php echo ucwords($schoolDetails->school_name); ?> - Portal</title>  
    <!-------------------- START - Meta -------------------->
    <?php include("../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/font-awesome.min.css">
	<link rel="stylesheet" href="./css/bootstrap.offcanvas.min.css">
	<link rel="stylesheet" href="./css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="./css/bootstrap-select.min.css">
	<link rel="stylesheet" href="./css/featherlight.min.css">
	<link rel="stylesheet" href="./css/hover.min.css">
	<link rel="stylesheet" href="./css/core.css">

	<link href="../home/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="../home/assets/css/flaticon.css" rel="stylesheet">
    <link href="../home/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../home/assets/css/animate.css" rel="stylesheet">
    <link href="../home/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="../home/assets/css/owl.theme.css" rel="stylesheet">
    <link href="../home/assets/css/slick.css" rel="stylesheet">
    <link href="../home/assets/css/slick-theme.css" rel="stylesheet">
    <link href="../home/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="../home/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="./css/header.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link rel="stylesheet" href="./css/style.php">
	<link rel="stylesheet" href="./css/responsive.css"> 
	
	</head>

	<body>

		
        <!-- Start header -->
        <header id="header" class="site-header header-style-2">
            <nav class="navigation navbar navbar-default" style="background:#fff">
                <div class="site-logo">
                    <a href="#" style="font-size: 1.2rem; font-weight: 600; color: #08acf0; font-family:Montserrat-Bold">
					<div style="
						background:  #fff;
						width: 120px;
						height: 120px;
						margin-top: -10px;
						border-radius:  10px;
					"><img src="<?php echo $schoolDetails->logo; ?>" style="object-fit:scale-down;width: 120px;display: block;margin-left: auto;margin-right: auto;padding-top: 20px;vertical-align: middle;height: 100px;">
					</div>
					</a>
				</div>
                <div class="container" style="margin-right: -40px">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse navbar-right collapse navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li><a href="../views/assessment/checkResult">Check Result</a></li>
                            <li><a href="#">Fee Payment</a></li>
                            <li><a href="../views/admission">Admission</a></li>
                            <li><a href="../views/educational_games">Games</a></li>
							<li><a href="../views/library">E-Library</a></li>
                            <li class="my_hover_up" style="background:#08acf0; padding-right:30px; padding-left:30px;"><a href="./login" style="color:#FFFFFF">LOGIN TO PORTAL</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->


		<section class="hero-1" style="padding-top: 150px">
			<div class="container">
				<div class="row">
					<div class="hero-content">
						<div class="hero-img col-sm-5 col-md-5">
							<img style="height: 530px;" src="images/teacher-1.png" alt="image">
						</div>
						
						<div class="hero-caption col-sm-7 col-md-7">
						    <?php 
							$json_tag = file_get_contents('../includes/taglines.json');
							$json_data_tag = json_decode($json_tag,true); 
							$random_tag = rand(0,count($json_data_tag) - 1); 
							?>
							<h2 style="font-size:3rem"><?php echo ucwords($schoolDetails->school_name)?></h2>
							<?php 
							$json = file_get_contents('../includes/quotes.json');
							$json_data = json_decode($json,true); 
							$random = rand(0,count($json_data) - 1); 
							?>
							<p style="font-size:25px;"><?php echo $schoolDetails->slogan; ?>...</p>
							<p><?php echo $json_data[$random]["text"]?> <br> - <?php echo $json_data[$random]["author"]?></p>
							<a href="./login" class="action-btn hvr-push">Download Desktop App</a>
							<img style="width:200px; height:77px; cursor:pointer" class="hvr-push" src="https://www.yourwelcome.com/wp-content/uploads/2015/10/get-it-on-the-google-play-store-button.png"/>
							<!-- <img style="width:200px; height:70px; cursor:pointer" class="hvr-push" src="https://atlona.com/wp-content/uploads/2017/12/Apple-App-Store-_logo.png"/> -->
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="testimonials">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="tab-content">
							<div class="tab-pane  active" id="testimonial-3">
								<span style="color:#08ACF0"><?php echo ucwords($schoolDetails->school_name)?></span> is an ICT oriented and high technology powered school poised to make learning and education for <span style="color:#08ACF0"><?php echo $category ?></span> schooling effective and fun. We are located at <span style="color:#08ACF0"><?php echo ucwords($schoolDetails->school_address.", ".$schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country)?>.</span> Our school is fully automated by one of the best school automation solutions <span style="color:#08ACF0">(<a style="color:#08ACF0;" href="https://classworks.xyz">ClassWorks SAS Suites</a>)</span>.
								<br><span style="color:#08ACF0">You can reach us on <?php echo $schoolDetails->phone ?>.</span> 
							</div>
						</div>
					</div>
				</div>
				<?php
					if(count($students) == 0){ ?>

					<?php }else{ ?>
						<div class="row">
							<div class="col-sm-12">
								<ul class="nav">
									<?php
										foreach($students as $key => $value){ ?>
											<li>
												<a href="#testimonial-1" data-toggle="tab">
													<img src="images/testimonial-img-1.jpg" alt="image">
													<strong><?php echo ucwords($value->fullname); ?></strong>
													<span><?php echo ucwords($value->presentClass); ?></span>
												</a>
											</li>
										<?php }
									?>
								</ul>
							</div>
						</div>
					<?php }
				?>
			</div>
		</section>
		
		<section class="achievements">
			<div class="container">
				<div class="row">
					<div class="section-header">
						<h2>BASIC SCHOOL CURRENT ACADEMIC INFORMATIONS</h2>
					</div>
				</div>
				<div class="row">
					<div class="section-content">
						<div class="col-sm-4">
							<h3 class="home-count"><?php echo $session; ?></h3>
							<p>Current Academic Session</p>
						</div>
						<div class="col-sm-4">
							<h3 class="home-count"><?php echo $term ?></h3>
							<p>Current Academic Term</p>
						</div>
						<div class="col-sm-3">
							<h3 class="home-count"><?php echo ucwords($schoolDetails->curriculum)?></h3>
							<p>Curriculum Used</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="newsletter">
			<div class="container" style="width: 100%;">
				<div class="row">
					<div class="newsletter-content">
						<div class="col-md-5">
							<h2><strong>Have any inquiries or complains?</strong> Contact School helpdesk now</h2>
						</div>
						<div class="col-md-7">
							<input type="email" placeholder="Enter your message here">
							<input class="hvr-push" type="submit" value="Send Message">
						</div>
					</div>
				</div>
			</div>
		</section>

		<footer id="colophon" class="site-footer">
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<p>Copyright <?php echo date("Y") ?> | ALL RIGHTS RESERVED - Powered by: <a href="https://classworks.xyz" style="color:#ffffff; font-weight:bold;">CLASSWORKS SAS SUITE</a> <a href="http://classworks.xyz" style="color:#ffffff; font-style:italic;">Click Here</a></p>
					</div>
				</div>
			</div>
		</footer>

<script src="./js/jquery-3.2.0.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/bootstrap-datepicker.min.js"></script>
<script src="./js/bootstrap-select.min.js"></script>
<script src="./js/featherlight.min.js"></script>
<script src="./js/bootstrap.offcanvas.min.js"></script>
<script src="./js/main.js"></script>
<script src="../home/assets/js/script.js"></script>

<!-------------------- START - Top Bar -------------------->
<?php include("../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

	</body>
</html>