<?php
    include_once ("config.php");
    $index = new logic();
    
    if(isset($_POST["log"])){
        $unique = htmlentities(trim($_POST["email_no"]));
        $password = htmlentities(trim($_POST["pass"]));
        $val = explode("/", $unique);
        $reply;
        if(count($val) > 1){
            $reply = $index->login($unique, $password, "number");
        }else{
            $reply = $index->login($unique, $password, "email");
        }
        if(is_object($reply)){
            include_once ('session.php');
            switch($reply->role){
                case 'admin':
                    $page = "../views/home";
                    break;
                case 'tutor':
                    $page = "../views/home/staff_dashboard";
                    break;
                case 'partner':
                    $page = "../views/partners/";
                    break;
                default:
                    $page = "../error";
            }
            echo "<script>window.location.assign('$page')</script>";
        }else{
            switch ($reply) {
                case 'password':
                    # code...
                    $err = "Incorrect password entered. Seems like you have forgotten your password";
                    break;
                case 'not found':
                    $err = "Opps..Incorrect login details provided";
                    break;
                case 'error';
                    $err = "Opps... Couldn't complete this operation. Please check back later";
                    break;
                default:
                    # code...
                    $err = "An unexpected error occured. Please try again later";
                    break;
            }
        }
    }


    if(isset($_POST["forgetBtn"])){
        $uniqueValue = htmlentities(trim($_POST["unique"]));
        
        $reply = $index->recoverPassword($uniqueValue);
        if($reply == "true"){
            $mess = "A new password has been sent to your email address";
        }else{
            switch($reply){
                case 'email':
                    $err = "Sorry this email/Reg No. is not recognized";
                    break;
                default:
                    $err = "An unexpected error occured. Please try again later";
                    break;
            }
        }
    }

?>