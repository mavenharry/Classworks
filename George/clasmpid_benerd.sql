-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2018 at 04:39 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clasmpid_benerd`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `email` tinytext,
  `adminNumber` tinytext,
  `profilePhoto` text,
  `password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `fullname`, `email`, `adminNumber`, `profilePhoto`, `password`) VALUES
(1, 'Abeng Emmanuel', 'emmanuelabeng27@gmail.com', '2018/Admn/Benerd/001', NULL, '$2y$10$wybRBGPOMU7qkeQNET1BDukTZWwJMNi73bvvPhQauPROFZKuKq0Qm');

-- --------------------------------------------------------

--
-- Table structure for table `affective_domain`
--

CREATE TABLE `affective_domain` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `student_number` tinytext,
  `gender` tinytext,
  `email` tinytext,
  `password` text,
  `profilePhoto` text,
  `dob` date DEFAULT NULL,
  `nationality` text,
  `state` text,
  `city` text,
  `blood_group` text,
  `languages` text,
  `last_school` text,
  `present_class` text,
  `student_desc` text,
  `enrolment_year` text,
  `father_phone` text,
  `mother_phone` text,
  `pickup_verification` int(11) DEFAULT '0',
  `bus_pickup` int(11) DEFAULT '0',
  `bus_dropoff` int(11) DEFAULT '0',
  `student_portal` int(11) DEFAULT '0',
  `parent_portal` int(11) DEFAULT '0',
  `walletAmount` int(11) DEFAULT '0',
  `promotion_status` int(11) DEFAULT '0',
  `accountBalance` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `applicant_no` text NOT NULL,
  `health_stat` text NOT NULL,
  `nationality` text NOT NULL,
  `state` text NOT NULL,
  `city` text NOT NULL,
  `dob` date NOT NULL,
  `gender` text NOT NULL,
  `bloodGroup` text NOT NULL,
  `profilePhoto` text NOT NULL,
  `guardianName` text NOT NULL,
  `guardianNationality` text NOT NULL,
  `guardianState` text NOT NULL,
  `guardianCity` text NOT NULL,
  `guardianRel` text NOT NULL,
  `guardianGender` text NOT NULL,
  `guardianMarital` text NOT NULL,
  `guardianAddress` text NOT NULL,
  `guardianOccupation` text NOT NULL,
  `guardianPhone` text NOT NULL,
  `guardianEmail` text NOT NULL,
  `applyClass` text NOT NULL,
  `applySession` text NOT NULL,
  `applyTerm` text NOT NULL,
  `applyBatch` text NOT NULL,
  `applyType` text NOT NULL,
  `learnType` text NOT NULL,
  `prevSchoolName` text NOT NULL,
  `prevSchoolCountry` text NOT NULL,
  `prevSchoolState` text NOT NULL,
  `applyDate` date NOT NULL,
  `applyState` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL,
  `student_no` text NOT NULL,
  `class_code` text NOT NULL,
  `subject_code` text NOT NULL,
  `ass003` int(11) NOT NULL,
  `ass002` int(11) NOT NULL,
  `ass001` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `remark` text NOT NULL,
  `grade` text NOT NULL,
  `tutor` text NOT NULL,
  `term_admitted` text NOT NULL,
  `academic_session` text NOT NULL,
  `academic_term` text NOT NULL,
  `comment` text NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `student_no`, `class_code`, `subject_code`, `ass003`, `ass002`, `ass001`, `total`, `remark`, `grade`, `tutor`, `term_admitted`, `academic_session`, `academic_term`, `comment`, `date_created`) VALUES
(1, '2018/stdn/benerd/001', 'class001', 'subject001', 0, 0, 0, 0, '', '', '', '', '2019', '1', '', '2018-11-05 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_settings`
--

CREATE TABLE `assessment_settings` (
  `id` int(11) NOT NULL,
  `assId` text,
  `name` text,
  `mark` int(11) DEFAULT NULL,
  `ass_state` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendancerecord`
--

CREATE TABLE `attendancerecord` (
  `id` int(11) NOT NULL,
  `attendanceNumber` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idType` int(11) NOT NULL COMMENT '0 for staff and 1 for student'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cbt_subjects`
--

CREATE TABLE `cbt_subjects` (
  `id` int(11) NOT NULL,
  `classCode` text,
  `subjectCode` text,
  `duration` int(11) DEFAULT NULL,
  `supervisorCode` text,
  `examState` int(11) DEFAULT NULL,
  `startTime` text,
  `endTime` text,
  `term` int(11) DEFAULT NULL,
  `session` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `childpickup`
--

CREATE TABLE `childpickup` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `pickupNumber` text,
  `nationality` tinytext,
  `state` tinytext,
  `city` tinytext,
  `relation_student` tinytext,
  `gender` tinytext,
  `m_status` tinytext,
  `occupation` tinytext,
  `phone` tinytext,
  `email` text,
  `studentNumber` text,
  `profilePhoto` text,
  `address` text,
  `added_by` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `class_code` tinytext,
  `class_name` tinytext,
  `class_teacher` int(11) DEFAULT '0',
  `date_created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `class_code`, `class_name`, `class_teacher`, `date_created`) VALUES
(1, 'class001', 'Grade 1', 0, '2018-11-05'),
(2, 'class002', 'Grade 2', 0, '2018-11-05'),
(3, 'class003', 'Grade 3', 0, '2018-11-05'),
(4, 'class004', 'Grade 4', 0, '2018-11-05'),
(5, 'class005', 'Grade 5', 0, '2018-11-05'),
(6, 'class006', 'Grade 6', 0, '2018-11-05'),
(7, 'class007', 'Grade 7', 0, '2018-11-05'),
(8, 'class008', 'Grade 8', 0, '2018-11-05'),
(9, 'class009', 'Grade 9', 0, '2018-11-05'),
(10, 'class010', 'Grade 10', 0, '2018-11-05'),
(11, 'class011', 'Grade 11', 0, '2018-11-05'),
(12, 'class012', 'Grade 12', 0, '2018-11-05');

-- --------------------------------------------------------

--
-- Table structure for table `domain_map`
--

CREATE TABLE `domain_map` (
  `id` int(11) NOT NULL,
  `student_number` text NOT NULL,
  `domain_name` text NOT NULL,
  `session` text NOT NULL,
  `term` text NOT NULL,
  `attr_id` text NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enrol_settings`
--

CREATE TABLE `enrol_settings` (
  `id` int(11) NOT NULL,
  `enrol_session` text NOT NULL,
  `enrol_batch` text NOT NULL,
  `enrol_classes` text NOT NULL,
  `enrol_startDate` text NOT NULL,
  `enrol_endDate` text NOT NULL,
  `enrol_feeState` int(11) NOT NULL,
  `enrol_amount` int(11) NOT NULL,
  `enrol_slot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `examination_question`
--

CREATE TABLE `examination_question` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `mark` text NOT NULL,
  `choice1` text,
  `choice2` text,
  `choice3` text,
  `choice4` text,
  `class` text,
  `subject` text,
  `session` text,
  `term` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `family`
--

CREATE TABLE `family` (
  `id` int(11) NOT NULL,
  `family_name` tinytext,
  `father_phone` tinytext,
  `mother_phone` tinytext,
  `balance` int(11) DEFAULT NULL,
  `date_created` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `fee_name` text NOT NULL,
  `amount` int(11) NOT NULL,
  `class_code` text NOT NULL,
  `academic_term` text NOT NULL,
  `academic_session` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grading_system`
--

CREATE TABLE `grading_system` (
  `id` int(11) NOT NULL,
  `froms` int(11) NOT NULL,
  `tos` int(11) NOT NULL,
  `grade` text NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_items`
--

CREATE TABLE `inventory_items` (
  `id` int(11) NOT NULL,
  `itemNo` text NOT NULL,
  `itemName` text NOT NULL,
  `itemSize` text,
  `itemColor` text,
  `itemQuantity` int(11) NOT NULL,
  `itemPrice` int(11) NOT NULL,
  `itemLocation` text,
  `added_by` text NOT NULL,
  `added_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_record`
--

CREATE TABLE `inventory_record` (
  `id` int(11) NOT NULL,
  `itemNo` text,
  `itemQuantity` int(11) NOT NULL,
  `collectedBy` text,
  `issuedBy` text,
  `date_collected` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_settings`
--

CREATE TABLE `inventory_settings` (
  `id` int(11) NOT NULL,
  `lowStock` int(11) NOT NULL,
  `outStock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `audienceBroadcast` text NOT NULL,
  `broadcastDate` date NOT NULL,
  `broadcastMessage` text NOT NULL,
  `logNumbers` text NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `audienceBroadcast`, `broadcastDate`, `broadcastMessage`, `logNumbers`, `state`) VALUES
(1, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(2, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(3, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(4, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(5, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(6, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(7, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1),
(8, '[\"Specify\"]', '2018-11-05', 'Testing this sms function', '[\"08189386761\"]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `outstanding_fee`
--

CREATE TABLE `outstanding_fee` (
  `id` int(11) NOT NULL,
  `studentNumber` text NOT NULL,
  `feeId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `term` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `added_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `guardian_number` tinytext,
  `nationality` tinytext,
  `state` tinytext,
  `city` tinytext,
  `relation_student` tinytext,
  `gender` tinytext,
  `m_status` tinytext,
  `occupation` tinytext,
  `phone` tinytext,
  `email` text,
  `password` text,
  `ProfilePhoto` text,
  `address` text,
  `app_access` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payed_student`
--

CREATE TABLE `payed_student` (
  `id` int(11) NOT NULL,
  `studentNumber` text NOT NULL,
  `className` text NOT NULL,
  `feeId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `session` int(11) NOT NULL,
  `term` int(11) NOT NULL,
  `payMode` int(11) NOT NULL,
  `transRef` text NOT NULL,
  `payDate` date DEFAULT NULL,
  `processed_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pickuprecords`
--

CREATE TABLE `pickuprecords` (
  `id` int(11) NOT NULL,
  `studentNumber` text NOT NULL,
  `pickupNumber` text NOT NULL,
  `datePicked` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `psychomotor_domain`
--

CREATE TABLE `psychomotor_domain` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` int(11) NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publicholiday`
--

CREATE TABLE `publicholiday` (
  `id` int(11) NOT NULL,
  `defaultId` int(11) NOT NULL,
  `holidayName` text NOT NULL,
  `holidayDate` text NOT NULL,
  `owner` int(11) NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `research_keywords`
--

CREATE TABLE `research_keywords` (
  `id` int(11) NOT NULL,
  `keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_days`
--

CREATE TABLE `schedule_days` (
  `id` int(11) NOT NULL,
  `dayCode` text NOT NULL,
  `dayName` text NOT NULL,
  `date_created` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule_days`
--

INSERT INTO `schedule_days` (`id`, `dayCode`, `dayName`, `date_created`, `state`) VALUES
(1, 'day001', 'Monday', '2018-11-05 00:00:00', 0),
(2, 'day002', 'Tuesday', '2018-11-05 00:00:00', 0),
(3, 'day003', 'Wednesday', '2018-11-05 00:00:00', 0),
(4, 'day004', 'Thursday', '2018-11-05 00:00:00', 0),
(5, 'day005', 'Friday', '2018-11-05 00:00:00', 0),
(6, 'day006', 'Saturday', '2018-11-05 00:00:00', 0),
(7, 'day007', 'Sunday', '2018-11-05 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `schedule_times`
--

CREATE TABLE `schedule_times` (
  `id` int(11) NOT NULL,
  `timeCode` text NOT NULL,
  `timeName` text NOT NULL,
  `activity` text NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_timings`
--

CREATE TABLE `schedule_timings` (
  `id` int(11) NOT NULL,
  `timeCode` text NOT NULL,
  `timeDay` text NOT NULL,
  `timeTime` text NOT NULL,
  `timeSubject` text NOT NULL,
  `timeClass` text NOT NULL,
  `timeTutor` text NOT NULL,
  `academic_session` text NOT NULL,
  `academic_term` text NOT NULL,
  `dateCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `school_name` varchar(45) DEFAULT NULL,
  `schoolAddress` text NOT NULL,
  `domain_name` varchar(100) DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `categories` text,
  `country` text,
  `state` text,
  `city` text,
  `website` text,
  `email` text,
  `telephone` text,
  `logo` text,
  `currency` text,
  `slogan` text,
  `grading_system` text,
  `school_color` text,
  `school_curriculum` text,
  `modules` text,
  `init_slider` int(1) DEFAULT '0',
  `templateId` int(1) DEFAULT '0',
  `idCardId` int(1) DEFAULT '0',
  `staff_attendance_state` int(1) DEFAULT '0',
  `staff_lateness_start` time DEFAULT NULL,
  `lateness_fee` int(11) DEFAULT '0',
  `absent_fee` int(11) DEFAULT '0',
  `student_attendance_state` int(1) DEFAULT '0',
  `student_lateness_start` time DEFAULT NULL,
  `absent_days` int(1) DEFAULT '0',
  `parent_daily_alert` int(1) DEFAULT '0',
  `attendanceMessage` text NOT NULL,
  `child_pickup_state` int(1) DEFAULT '0',
  `max_pickup_person` int(1) DEFAULT '0',
  `pickup_alert` int(1) DEFAULT '0',
  `pickupMessage` text NOT NULL,
  `add_pickup` text,
  `verify_pickup` text,
  `bank_name` text,
  `account_number` text,
  `account_name` text,
  `disbursement_state` text,
  `disbursement_date` int(3) DEFAULT '0',
  `disbursement_number` tinytext,
  `deduct_lateness` int(1) DEFAULT '0',
  `deduct_absent` int(1) DEFAULT '0',
  `auto_alert_outstanding` int(1) DEFAULT '0',
  `alert_time` text,
  `online_payment` int(1) DEFAULT '0',
  `disburse_bank` text,
  `disburse_account` text,
  `disburse_account_name` text,
  `offline_payment` int(1) DEFAULT '0',
  `automatic_renewal` int(1) DEFAULT '0',
  `renewal_notify_number` text,
  `academic_session` text NOT NULL,
  `academic_term` text NOT NULL,
  `next_resumption_date` text NOT NULL,
  `smsUnit` int(11) NOT NULL,
  `subAccount` text,
  `sub_date` date DEFAULT NULL,
  `next_sub_date` date DEFAULT NULL,
  `pay_no` int(11) DEFAULT '0',
  `account_state` int(2) DEFAULT '0',
  `quick_notice` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `school_name`, `schoolAddress`, `domain_name`, `reg_date`, `categories`, `country`, `state`, `city`, `website`, `email`, `telephone`, `logo`, `currency`, `slogan`, `grading_system`, `school_color`, `school_curriculum`, `modules`, `init_slider`, `templateId`, `idCardId`, `staff_attendance_state`, `staff_lateness_start`, `lateness_fee`, `absent_fee`, `student_attendance_state`, `student_lateness_start`, `absent_days`, `parent_daily_alert`, `attendanceMessage`, `child_pickup_state`, `max_pickup_person`, `pickup_alert`, `pickupMessage`, `add_pickup`, `verify_pickup`, `bank_name`, `account_number`, `account_name`, `disbursement_state`, `disbursement_date`, `disbursement_number`, `deduct_lateness`, `deduct_absent`, `auto_alert_outstanding`, `alert_time`, `online_payment`, `disburse_bank`, `disburse_account`, `disburse_account_name`, `offline_payment`, `automatic_renewal`, `renewal_notify_number`, `academic_session`, `academic_term`, `next_resumption_date`, `smsUnit`, `subAccount`, `sub_date`, `next_sub_date`, `pay_no`, `account_state`, `quick_notice`) VALUES
(1, 'Benerd College', 'No 1 rumuomasi Portharcourt', 'benerd', '2018-11-05', '[\"Nursery\",\"Primary\",\"Secondary\"]', 'nigeria', 'Rivers State', 'Portharcourt', '', 'emmanuelabeng27@gmail.com', '08189386761', 'logo.png', 'Naira', 'Education at its best', 'Percentage', '#0318F0', 'american', '0', 1, 0, 0, 0, NULL, 0, 0, 0, NULL, 0, 0, '', 0, 0, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, NULL, 0, NULL, NULL, NULL, 0, 0, NULL, '2019', '1', '05/03/2019', 42, NULL, '2018-11-05', '2018-12-05', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `shop_product`
--

CREATE TABLE `shop_product` (
  `id` int(11) NOT NULL,
  `product_id` text NOT NULL,
  `product_name` text NOT NULL,
  `product_quantity` int(11) DEFAULT '0',
  `product_price` int(11) DEFAULT '0',
  `date_added` date DEFAULT NULL,
  `added_by` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `student_number` tinytext,
  `gender` tinytext,
  `email` tinytext,
  `password` text,
  `profilePhoto` text,
  `dob` date DEFAULT NULL,
  `nationality` text,
  `state` text,
  `city` text,
  `blood_group` text,
  `languages` text,
  `last_school` text,
  `present_class` text,
  `student_desc` text,
  `enrolment_year` text,
  `father_phone` text,
  `mother_phone` text,
  `pickup_verification` int(11) DEFAULT '0',
  `bus_pickup` int(11) DEFAULT '0',
  `bus_dropoff` int(11) DEFAULT '0',
  `student_portal` int(11) DEFAULT '0',
  `parent_portal` int(11) DEFAULT '0',
  `walletAmount` int(11) DEFAULT '0',
  `promotion_status` int(11) DEFAULT '0',
  `accountBalance` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `fullname`, `student_number`, `gender`, `email`, `password`, `profilePhoto`, `dob`, `nationality`, `state`, `city`, `blood_group`, `languages`, `last_school`, `present_class`, `student_desc`, `enrolment_year`, `father_phone`, `mother_phone`, `pickup_verification`, `bus_pickup`, `bus_dropoff`, `student_portal`, `parent_portal`, `walletAmount`, `promotion_status`, `accountBalance`) VALUES
(1, 'sam jack', '2018/stdn/benerd/001', 'male', NULL, NULL, '', '2010-11-02', 'nigeria', 'rivers state', 'portharcourt', 'a', NULL, NULL, 'Grade 1', NULL, NULL, '08189386761', '0', 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subjectCode` tinytext,
  `subjectName` tinytext,
  `subjectClass` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subjectCode`, `subjectName`, `subjectClass`) VALUES
(1, 'subject001', 'english language', 'grade 1');

-- --------------------------------------------------------

--
-- Table structure for table `subjectteachers`
--

CREATE TABLE `subjectteachers` (
  `id` int(11) NOT NULL,
  `classCode` tinytext,
  `subjectCode` tinytext,
  `staffCode` tinytext,
  `date_assigned` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `fullname` tinytext,
  `staff_number` varchar(100) DEFAULT NULL,
  `nationality` tinytext,
  `state` tinytext,
  `city` tinytext,
  `highest_qualification` tinytext,
  `gender` tinytext,
  `m_status` tinytext,
  `address` tinytext,
  `department` tinytext,
  `phone` tinytext,
  `email` tinytext,
  `password` tinytext,
  `profilePhoto` tinytext,
  `date_added` date DEFAULT NULL,
  `staff_portal` int(2) DEFAULT '1',
  `bank_name` tinytext,
  `account_name` tinytext,
  `account_number` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tuckshop_setting`
--

CREATE TABLE `tuckshop_setting` (
  `id` int(11) NOT NULL,
  `alertWalletBalance` int(11) DEFAULT NULL,
  `amountToAlert` int(11) DEFAULT NULL,
  `bankName` text,
  `accountName` text,
  `accountNumber` text,
  `accountState` int(11) DEFAULT '0',
  `accountCode` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `affective_domain`
--
ALTER TABLE `affective_domain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessments`
--
ALTER TABLE `assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment_settings`
--
ALTER TABLE `assessment_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendancerecord`
--
ALTER TABLE `attendancerecord`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cbt_subjects`
--
ALTER TABLE `cbt_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `childpickup`
--
ALTER TABLE `childpickup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domain_map`
--
ALTER TABLE `domain_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrol_settings`
--
ALTER TABLE `enrol_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examination_question`
--
ALTER TABLE `examination_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grading_system`
--
ALTER TABLE `grading_system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_items`
--
ALTER TABLE `inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_record`
--
ALTER TABLE `inventory_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_settings`
--
ALTER TABLE `inventory_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstanding_fee`
--
ALTER TABLE `outstanding_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payed_student`
--
ALTER TABLE `payed_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickuprecords`
--
ALTER TABLE `pickuprecords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `psychomotor_domain`
--
ALTER TABLE `psychomotor_domain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicholiday`
--
ALTER TABLE `publicholiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `research_keywords`
--
ALTER TABLE `research_keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_days`
--
ALTER TABLE `schedule_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_times`
--
ALTER TABLE `schedule_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_timings`
--
ALTER TABLE `schedule_timings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjectteachers`
--
ALTER TABLE `subjectteachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuckshop_setting`
--
ALTER TABLE `tuckshop_setting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `affective_domain`
--
ALTER TABLE `affective_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assessments`
--
ALTER TABLE `assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `assessment_settings`
--
ALTER TABLE `assessment_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attendancerecord`
--
ALTER TABLE `attendancerecord`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cbt_subjects`
--
ALTER TABLE `cbt_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `childpickup`
--
ALTER TABLE `childpickup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `domain_map`
--
ALTER TABLE `domain_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enrol_settings`
--
ALTER TABLE `enrol_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `examination_question`
--
ALTER TABLE `examination_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `family`
--
ALTER TABLE `family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grading_system`
--
ALTER TABLE `grading_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_items`
--
ALTER TABLE `inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_record`
--
ALTER TABLE `inventory_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_settings`
--
ALTER TABLE `inventory_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `outstanding_fee`
--
ALTER TABLE `outstanding_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payed_student`
--
ALTER TABLE `payed_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pickuprecords`
--
ALTER TABLE `pickuprecords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `psychomotor_domain`
--
ALTER TABLE `psychomotor_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `publicholiday`
--
ALTER TABLE `publicholiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `research_keywords`
--
ALTER TABLE `research_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schedule_days`
--
ALTER TABLE `schedule_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `schedule_times`
--
ALTER TABLE `schedule_times`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schedule_timings`
--
ALTER TABLE `schedule_timings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_product`
--
ALTER TABLE `shop_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subjectteachers`
--
ALTER TABLE `subjectteachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tuckshop_setting`
--
ALTER TABLE `tuckshop_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
