<?php
    include_once ("../logic/config.php");

    class examPrepClass extends logic {

        function sendExamPrepDownloadLink($name, $phone){
            $dates = date("Y-m-d");
            $acctState = 0;
            $stmt = $this->dconn->prepare("SELECT id FROM classquiz_subscribers WHERE phone = ?");
            $stmt->bind_param("s", $phone);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt = $this->dconn->prepare("UPDATE classquiz_subscribers SET `sub_state` = ?, `sub_date` = ? WHERE phone = ?");
                $stmt->bind_param("iss", $acctState, $dates, $phone);
                if($stmt->execute()){
                    $stmt->close();
                    //Send SMS here
                    $message = "Congrats ".ucwords($name).", follow link below to download classQuiz; https://goo.gl/FFYQbP";
                    $this->pushMessage($message,"ClassQuiz",$phone);
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }else{
                $stmt = $this->dconn->prepare("INSERT INTO classquiz_subscribers (`name`, phone, sub_date, sub_state) VALUES (?,?,?,?)");
                $stmt->bind_param("sssi", $name, $phone, $dates, $acctState);
                if($stmt->execute()){
                    $stmt->close();
                    //Send SMS here
                    $message = "Congrats ".ucwords($name).", follow link below to download classQuiz; https://goo.gl/FFYQbP";
                    $this->pushMessage($message,"ClassQuiz",$phone);
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }


        function pushMessage($message, $sender, $receiver){
            if(is_array($receiver)){
                $formattedReceiver = implode(",", $receiver);
            }else{
                $formattedReceiver = $receiver;
            }
            $authEmail = "hello@classworks.xyz";
            $authPassword = "Kny2pdGY9JrR";
            $smsContent = urlencode($message);
            $smsSender = $sender;
            $smsReceiver = $formattedReceiver;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://kullsms.com/customer/api/?username='.$authEmail.'&password='.$authPassword.'&message='.$smsContent.'&sender='.$smsSender.'&mobiles='.$smsReceiver);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($ch);
            curl_close($ch);
            if($result == 1701){
                return true;
            }else{
                return $result;
            }
        }

    }
?>
