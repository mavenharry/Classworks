<?php
    include_once ("config.php");
    $index = new logic();
    
    if(isset($_POST["log"])){
        $unique = htmlentities(trim($_POST["email_no"]));
        $password = htmlentities(trim($_POST["pass"]));
        $val = explode("/", $unique);
        $reply;
        if(count($val) > 1){
            $reply = $index->login($unique, $password, "number");
        }else{
            $reply = $index->login($unique, $password, "email");
        }
        if(is_object($reply)){
            include_once ('session.php');
            switch($reply->role){
                case 'admin':
                case 'principal':
                case 'director':
                    $page = "../views/home";
                    break;
                case 'tutor':
                case 'form teacher':
                    $page = "../views/home/staff_dashboard";
                    break;
                case 'accountant':
                    $page = "../views/payments";
                    break;
                case 'store keeper':
                    $page = "../views/inventory";
                    break;
                case 'partner':
                    $page = "../views/partners/";
                    break;
                default:
                    $page = "../views/error";
            }
            echo "<script>window.location.assign('$page')</script>";
        }else{
            switch ($reply) {
                case 'password':
                    # code...
                    $err = "Incorrect password entered. Seems like you have forgotten your password";
                    break;
                case 'not found':
                    $err = "Opps..Incorrect login details provided";
                    break;
                case 'error';
                    $err = "Opps... Couldn't complete this operation. Please check back later";
                    break;
                default:
                    # code...
                    $err = "An unexpected error occured. Please try again later";
                    break;
            }
        }
    }
?>