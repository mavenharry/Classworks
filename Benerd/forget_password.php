<?php include_once ("logic/logicbtn.php"); ?>
<html lang="en" itemscope="" itemtype="//schema.org/MobileApplication">
    <head>
        <title>ClassWorks - School Automation Solution - Login</title>
        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
        <!-------------------- END - Meta -------------------->
        <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="../../css/toastr.min.css">
    </head>
    <body class="common signup-new">
        <div class="navigation-backdrop" id="backdrop-nav"></div>
        <div class="main-content">
            <section class="site--body" id="512">
                <div class="row" id="components-signup-form">
                    <div class="col-lg-6">
                        <div class="container col-lg-6__top-bar">
                            <a class="main-nav__logo" href="/" tabindex="-1">
                            <a href="../../"><img style="width:70px; height:70px; margin-top:-10px; margin-bottom:-20px;" alt="" src="../../images/logo.png"></a>
                            </a>
                        </div>
                        <div class="components-signup-form padding-top-sm" style="margin-top:50px;">
                            <div class="tw-form">
                                <form class="form padding-md" id="components-signup-form-step-one" autocomplete="false" method="POST">
                                    <p class="form__focus-message">Easy schooling. Recover your password!</p>
                                    <p class="form__focus-message-statement">We just need your email address to recover your account password.</p>
                                    <div class="form-group">
                                        <input class="form-field" required id="email" placeholder="Email Address" type="text" name="unique" tabindex="1">
                                    </div>
                                    <input class="button button_green button_large button_full-width margin-top-sm" type="submit" value="Recover Password" tabindex="0" name="forgetBtn">
                                </form>
                                <a href="./login" style="font-weight:bold; font-size:1.5rem; color: #8095a0; float: right; margin-right: 32px; margin-top: -25px;" >Remember password? Login now.</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-6--cartoon hidden-md-down">
                        <div class="container col-lg-6__top-bar">
                            <div class="pull-right">
                                <p class="col-lg-6__top-bar__paragraph">No account created yet?</p>
                                <a class="col-lg-6__top-bar__login-button" href="../authentication/signup.php">Create an account</a>
                            </div>
                        </div>
                        <div class="steps">
                            <div class="step-one">
                            <blockquote class="blockquote" style="text-align:justify; width:80%; margin-left:10%">
                              <h4 style="font-weight:normal; color:#ffffff; font-size:7rem; margin-bottom:-40px">“</h4>
                              <?php 
                              $json = file_get_contents('../../includes/quotes.json');
                              $json_data = json_decode($json,true); 
                              $random = rand(0,count($json_data) - 1); 
                              ?>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem;"><?php echo $json_data[$random]["text"]?></h4>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem; margin-top:20px; float:right">― <?php echo $json_data[$random]["author"]?></h4>                              
                            </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
<!-------------------- START - Top Bar -------------------->
<?php include_once ("../../includes/support.php"); ?>
<?php include_once ("../../includes/scripts.php"); ?>
<!-------------------- END - Top Bar -------------------->

    </body>
</html>
