<meta charset="utf-8">
<meta content="ie=edge" http-equiv="x-ua-compatible">
<meta content="ClassWorks" name="author">
<meta content="width=device-width, initial-scale=1" name="viewport">
<link href="../../css/toastr.min.css" rel="stylesheet">

<link rel="apple-touch-icon" sizes="57x57" href="../../favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../../favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../../favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../../favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../../favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../../favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../../favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../../favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../../favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../../favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../../favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../../favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../../favicon/favicon-16x16.png">
<link rel="manifest" href="../../favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="../../favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta name="description" content="All-in-one school management software and school management system with features like: timetable, attendance, parent-teacher-student communication and more.">
<meta name="keywords" content="school automation solution, School management software, School management system, education, eduTech, Technology, student record system, Nigeria, Africa, World">

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="ClassWorks" />
<meta itemprop="description" content="All-in-one school management software and school management system with features like: timetable, attendance, parent-teacher-student communication and more."/>
<meta itemprop="image" content="http://classworks.xyz/assets/images/logo.png" />

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary"/>
<meta name="twitter:site" content="@ClassWorksSAS"/>
<meta name="twitter:creator" content="@classWorksSAS"/>
<meta name="twitter:url" content="//ClassWorks.xyz/"/>
<meta name="twitter:title" content="ClassWorks - School Management Software"/>
<meta name="twitter:description" content="ClassWorks is a multipurpose school management software & management information system used by education institutions worldwide for administration & management related activities."/>
<meta name="twitter:image" content="http://classworks.xyz/assets/images/logo.png"/>

<!-- Open Graph data -->
<meta property="og:type" content="website" />
<meta property="og:url" content="//ClassWorks.xyz/"/>
<meta property="og:image" content="http://classworks.xyz/assets/images/logo.png"/>
<meta property="og:title" content="ClassWorks - School Management Software"/>
<meta property="og:description" content="Classworks is a multipurpose school management software & management information system used by education institutions worldwide for administration & management related activities."/>
<meta property="og:site_name" content="ClassWorks" />


<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />