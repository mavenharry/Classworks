    <div class="no-print top-bar color-scheme-light">
    
    <?php

    $pathArr = explode("/", $_SERVER['REQUEST_URI']);
    $fileName = $pathArr[count($pathArr)-1];
    $fileName2 = $pathArr[count($pathArr)-2];
    if($fileName == "home" || $fileName == "staff_assessment" || $fileName2 == "library"){ ?>

        <div class="logo-w">
        <a class="logo" href="../staffs/home">
            <img alt="" style="margin-top:0px; width:60px; height:60px" src="<?php echo $schoolDetails->logo;?>">
            <div class="logo-label" style="margin-top:5px"><?php echo $schoolDetails->school_name .", ". $schoolDetails->city.", ".$schoolDetails->state; ?></div>
        </a>
        </div>

    <?php }

    ?>
<!-------------------- START - Top Menu Controls -------------------->
<div class="top-menu-controls">
    
    <!-------------------- START - Messages Link in secondary top menu -------------------->
    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
        <i class="os-icon os-icon-mail"></i>
        <div class="new-messages-count">9+</div>
        <div class="os-dropdown light message-list">
            <ul>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="../../images/avatar.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">John Mayers</h6>
                            <h6 class="message-title">Account Update</h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="../../images/avatar.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">Phil Jones</h6>
                            <h6 class="message-title">Secutiry Updates</h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="../../images/avatar.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">Bekky Simpson</h6>
                            <h6 class="message-title">Vacation Rentals</h6>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="user-avatar-w">
                            <img alt="" src="../../images/avatar.jpg">
                        </div>
                        <div class="message-content">
                            <h6 class="message-from">Alice Priskon</h6>
                            <h6 class="message-title">Payment Confirmation</h6>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-------------------- END - Messages Link in secondary top menu -------------------->

    <div class="nametag">
        <h5 style="font-size: 1rem; padding-right:10px; padding-left:10px; padding-top:5px;">Maven Harry</h5>
    </div>
    <!-------------------- START - User avatar and menu in secondary top menu -------------------->
    <div class="logged-user-w">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="../../images/avatar.jpg">
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w">
                        <img alt="" src="../../images/avatar.jpg">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">Maria Gomez</div>
                        <div class="logged-user-role">Administrator</div>
                    </div>
                </div>
                <div class="bg-icon">
                    <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                    <li>
                        <a href="users_profile_big.html">
                            <i class="os-icon os-icon-user-male-circle2"></i>
                            <span>Profile Details</span>
                        </a>
                    </li>
                    <li>
                        <a href="users_profile_small.html">
                            <i class="os-icon os-icon-coins-4"></i>
                            <span>Billing Details</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="os-icon os-icon-others-43"></i>
                            <span>Notifications</span>
                        </a>
                    </li>
                    <li>
                        <a href="../authentication/lock_screen.php">
                            <i class="os-icon os-icon-signs-11"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-------------------- END - User avatar and menu in secondary top menu -------------------->
</div>
<!-------------------- END - Top Menu Controls -------------------->
</div>
<!-------------------- START - MODAL CALLERS -------------------->
    <?php
    include_once ("modals/modalbtn.php");
        if(isset($err)){ ?>
            <a hidden data-target="#errorModal" id="errorCaller" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                class="my_hover_up btn btn-white btn-sm" href="#">
            </a>
     <?php   }
        if(isset($mess)){ ?>
            <a hidden data-target="#successModal" id="successCaller" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                class="my_hover_up btn btn-white btn-sm" href="#">
            </a>
    <?php    }
    ?>
<!-------------------- END - MODAL CALLERS -------------------->