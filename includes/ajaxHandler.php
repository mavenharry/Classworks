<?php
    include_once ("./validate.php");

    if(isset($_GET["domain"])){
        $domain = htmlentities(trim($_GET["domain"]));
        $reply = $validate->validateDomain($domain);
        echo $reply;
    }

    if(isset($_GET["package"]) && isset($_GET["email"])){
        $email = htmlentities(trim($_GET["email"]));
        $package = htmlentities(trim($_GET["package"]));
        $reply = $validate->getPricing($package, $email);
        if($reply !== false){
            echo json_encode($reply);
        }else{
            echo "false";
        }
    }

    if(isset($_GET["action"]) && $_GET["action"] == "purchase sms"){
        $reply = $validate->getPaymentParams();
        echo json_encode($reply);
    }

    if(isset($_GET["action"]) && $_GET["action"] == "purchase classquiz"){
        $reply = $validate->getPaymentParams();
        echo json_encode($reply);
    }
?>