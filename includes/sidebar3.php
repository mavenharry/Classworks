<div style="height: 1000px;" class=" no-print menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-light color-style-default sub-menu-color-bright menu-position-side menu-side-left menu-layout-full sub-menu-style-over">
    <div class="logo-w">
    <?php
        if(isset($_SESSION["profilePhoto"]) && !empty($_SESSION["profilePhoto"])){ ?>
            <a class="logo" href="../partners">
                <img alt="" style="margin-left: 30px; margin-top:30px; width:120px; height:120px; object-fit:scale-down" src="./passports/<?php echo $_SESSION["profilePhoto"]; ?>">
                <div class="logo-label" style="margin-top:5px"><?php //echo $schoolDetails->school_name.", ". $schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country; ?></div>
            </a>
        <?php }else{ ?>
            <form method="Post" enctype="multipart/form-data">
                <div class=" no-print row" style="margin-left: 10px;">
                    <div class=" no-print pt-avatar-w" style="border-radius:0px;">
                        <img alt="" id="studentuploadPreview2" style="margin-left:40px; margin-top: 50px; border-radius:5px; border: 1px solid #08ACF0; height:110px; width:100px" src="../../images/male-user-profile-picture.png">
                    </div>
                    <div class=" no-print form-group col-sm-9">
                        <label for="studentupload-photo2" style="margin-left: 10px;; margin-top:10px">Upload passport</label>
                        <label style="cursor:pointer; margin-top:15px; margin-left: 5px;" class=" no-print btn btn-primary" for="studentupload-photo2"> Upload Image</label>
                        <input type="submit" name="uploadPhoto" style="background:#1BB4AD; border:1px solid #1BB4AD; cursor:pointer; margin-top:15px; margin-left: -12px;" class=" no-print btn btn-primary" value="Save Passport Now"/>                
                        <input id="studentupload-photo2" name="profilePhoto" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1; " class=" no-print form-control" type="file">
                        <input type="hidden" name="profileChecker" value="<?php //echo $studentDetails->profilePhoto; ?>" id="">
                    </div>
                </div>
            </form>
        <?php }
    ?>
    </div>

    <ul class=" no-print main-menu">
        <li id="home" class=" no-print has-sub-menu" style="border-top: 2px solid rgba(0,0,0,0.05);">
            <a href="../partners/">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-layout"></div>
                </div>
                <span>My Dashboard</span>
            </a>
        </li>
        <li id="students" class=" no-print has-sub-menu">
            <a href="./all_schools">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-tasks-checked"></div>
                </div>
                <span>My Schools</span>
            </a>
        </li>
        <!-- <li id="my_partners" class=" no-print has-sub-menu">
            <a href="./all_partners">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-users"></div>
                </div>
                <span>My Partners</span>
            </a>
        </li> -->
    </ul>
</div>