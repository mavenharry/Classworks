<?php
    include_once ("../logic/db.php");
    class validate {
        private $defaultHost = host;
        private $defaultUser = user;
        private $defaultDatabase = database;
        private $defaultPassword = password;
        private $dconn;

        public function __construct(){
            // $this->ini_array = parse_ini_file("../../includes/.env");
            $this->defaultConnection();
            // $this->userConnection($this->ini_array);
        }
        
        function get_result( $Statement ) {
            $RESULT = array();
            $Statement->store_result();
            for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
                $Metadata = $Statement->result_metadata();
                $PARAMS = array();
                while ( $Field = $Metadata->fetch_field() ) {
                    $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
                }
                call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
                $Statement->fetch();
            }
            $object = (object) $RESULT;
            return $RESULT;
        }

        private function defaultConnection(){
            $this->dconn = new mysqli($this->defaultHost, $this->defaultUser, $this->defaultPassword, $this->defaultDatabase);
            if(!$this->dconn){
                $this->error = "Fatal Error: Can't connect to database ".$this->dconn->connect_error;
                return false;
            }
        }

        function validateDomain ($domain){
            $stmt = $this->dconn->prepare("SELECT schools.admin_name, schools.admin_email, users.activated FROM schools INNER JOIN users ON schools.admin_email = users.email AND schools.admin_name = users.fullname WHERE schools.domain_name = ?");
            $stmt->bind_param("s", $domain);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $activated = array_shift($stmt_result)["activated"];
            if($activated == 0){
                return "true";
            }else{
                return "false";
            }
        }

        function getPricing($package, $email){
            $stmt = $this->dconn->prepare("SELECT partners.subAccountCode FROM partners INNER JOIN users ON users.refCode = partners.partner_no  WHERE users.email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $subAccountCode = array_shift($stmt_result)["subAccountCode"];
            $keyName = "paystack_public";
            $stmt = $this->dconn->prepare("SELECT pricing.price, systemkeys.keyValue FROM pricing INNER JOIN systemkeys WHERE pricing.package = ? AND systemkeys.keyName = ?");
            $stmt->bind_param("is", $package, $keyName);
            if($stmt->execute()){
                $resp = array();
                $stmt_result = $this->get_result($stmt);
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->price = $row["price"];
                    $obj->pkey = $row["keyValue"];
                    $obj->subAccountCode = $subAccountCode;
                }
                $stmt->close();
                return $obj;
            }else{
                $stmt->close();
                return false;
            }
        }

        function getPaymentParams(){
            $stmt = $this->dconn->prepare("SELECT * FROM systemkeys");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj = new stdClass();
            while($row = array_shift($stmt_result)){
                $row["keyName"] == "paystack_public" ? $obj->pkey = $row["keyValue"] : "";
                $row["keyName"] == "paystack_secret" ? $obj->skey = $row["keyValue"] : "";
            }
            return $obj;
        }

        function logPayment($ref, $amount, $email, $school_name, $action){
            $date = Date("Y-m-d");
            $stmt = $this->dconn->prepare("INSERT INTO completed_tran (trans_ref, amount, email, school_name, action, pay_date) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param("sissss", $ref, $amount, $email, $school_name, $action, $date);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return "error";
            }
        }
    }

    $validate = new validate;
?>