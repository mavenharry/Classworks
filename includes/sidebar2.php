<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-light color-style-default sub-menu-color-bright menu-position-side menu-side-left menu-layout-full sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="./staff_dashboard">
            <img alt="" style="margin-top:20px; width:120px; height:120px; object-fit:scale-down" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>">
            <div class="logo-label" style="margin-top:5px"><?php echo $schoolDetails->school_name.", ". $schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country; ?></div>
        </a>
    </div>

    <ul class=" no-print main-menu">
        <li id="home" class=" no-print has-sub-menu" style="border-top: 2px solid rgba(0,0,0,0.05);">
            <a href="../home/staff_dashboard">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-layout"></div>
                </div>
                <span>My Dashboard</span>
            </a>
        </li>
        <li id="students" class=" no-print has-sub-menu">
            <a href="../students/staff_students">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-user-male-circle"></div>
                </div>
                <span>My Students</span>
            </a>
        </li>
        <li id="classes" class=" no-print has-sub-menu">
            <a href="../classes/staff_classes">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-calendar-time"></div>
                </div>
                <span>My Classes</span>
            </a>
        </li>
        <li id="subjects" class=" no-print has-sub-menu">
            <a href="../subjects/staff_subjects">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-tasks-checked"></div>
                </div>
                <span>My Subjects</span>
            </a>
        </li>
        <li id="timetable" class=" no-print has-sub-menu">
            <a href="../timetable/my_timetable">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-clock"></div>
                </div>
                <span>My Timetable</span>
            </a>
        </li>
        <li id="attendance" class=" no-print has-sub-menu">
            <a href="../attendance/staff_attendance">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-newspaper"></div>
                </div>
                <span>Attendance</span>
            </a>
        </li>
        <li id="childpickup" class=" no-print has-sub-menu">
            <a href="../childpickup/staff_childpickup">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-fingerprint"></div>
                </div>
                <span>Child Pickup</span>
            </a>
        </li>
        <li id="assessment" class=" no-print has-sub-menu">
            <a href="../assessment/staff_assessment">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-edit-1"></div>
                </div>
                <span>Assessment</span>
            </a>
        </li>
        <li id="examination" class=" no-print has-sub-menu">
            <a href="#">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-pencil-1"></div>
                </div>
                <span>Examination</span>
            </a>
        </li>
    </ul>
</div>
<?php
        switch($schoolDetails->modules){
            case 0:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Examination", "Laboratory"];
                break;
            case 1:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings"];
                break;
            case 2:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Pin Vending"];
                break;
            default:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Library", "Examination", "Laboratory", "Pin Vending"];
                break;
        }

        // FOR USER ROLES
        switch($_SESSION["role"]){
            case 'admin':
            case 'principal':
            case 'director':
                $allowedPages = ["Dashboard", "Admission", "Settings", "Parents", "Students", "Staff", "Classes", "Subjects", "Payments", "Attendance", "Child Pickup", "Assessment", "Timetable", "Examination", "Laboratory", "Library", "Inventory", "TuckShop", "Games", "Reports"];
                break;
            case 'accountant':
                $allowedPages = ["Payments", "Reports"];
                break;
            case 'store keeper':
                $allowedPages = ["Inventory"];
                break;
            case 'tutor':
            case 'form teacher':
                $allowedPages = ["Students", "Assessment", "Examination"];
                break;
        }
        
    ?>