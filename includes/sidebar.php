<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-light color-style-default sub-menu-color-bright menu-position-side menu-side-left menu-layout-full sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="../home">
            <img alt="" style="margin-top:20px; width:120px; height:120px; object-fit:scale-down" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>">
            <div class="logo-label" style="margin-top:5px"><?php echo $schoolDetails->school_name.", ". $schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country; ?></div>
        </a>
    </div>
    <?php
        switch($schoolDetails->modules){
            case 0:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Examination", "Laboratory", "Messaging"];
                break;
            case 1:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings", "Messaging"];
                break;
            case 2:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Pin Vending", "Messaging"];
                break;
            default:
                $allowedArray = ["Dashboard", "Admission", "Payments", "Staff", "Students", "Parents", "Classes", "Subjects", "Assessment", "Timetable", "Examination", "Reports", "Settings", "Attendance", "Child Pickup", "Hostel", "Inventory", "TuckShop", "Games", "Library", "Examination", "Laboratory", "Pin Vending", "Messaging"];
                break;
        }

        // FOR USER ROLES
        switch($_SESSION["role"]){
            case 'admin':
            case 'principal':
            case 'director':
                $allowedPages = ["Dashboard", "Admission", "Settings", "Parents", "Students", "Staff", "Classes", "Subjects", "Payments", "Attendance", "Child Pickup", "Assessment", "Timetable", "Examination", "Laboratory", "Library", "Inventory", "TuckShop", "Games", "Reports", "Messaging"];
                break;
            case 'accountant':
                $allowedPages = ["Payments", "Reports", "Messaging"];
                break;
            case 'store keeper':
                $allowedPages = ["Inventory"];
                break;
            case 'tutor':
            case 'form teacher';
                $allowedPages = ["Students", "Assessment", "Examination"];
                break;
        }
        
    ?>
    <ul class=" no-print main-menu">
        <?php
            if(in_array("Dashboard", $allowedPages)){ ?>
                <li id="home" class=" no-print has-sub-menu" style="border-top: 2px solid rgba(0,0,0,0.05);">
                    <a href="../home">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-layout"></div>
                        </div>
                        <span>Dashboard</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Admission", $allowedArray) && in_array("Admission", $allowedPages)){ ?>
                <li id="admission" class=" no-print has-sub-menu">
                    <a href="../admission/applicant_list">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-delivery-box-2"></div>
                        </div>
                        <span>Admission</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Students", $allowedArray) && in_array("Students", $allowedPages)){ ?>
                <li id="students" class=" no-print has-sub-menu">
                    <a href="../students">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-user-male-circle"></div>
                        </div>
                        <span>Students</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Parents", $allowedArray) && in_array("Parents", $allowedPages)){ ?>
                <li id="parents" class=" no-print has-sub-menu">
                    <a href="../parents">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-users"></div>
                        </div>
                        <span>Parents</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Staff", $allowedArray) && in_array("Staff", $allowedPages)){ ?>
                <li id="staffs" class=" no-print has-sub-menu">
                    <a href="../staffs">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-user"></div>
                        </div>
                        <span>Staff</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Classes", $allowedArray) && in_array("Classes", $allowedPages)){ ?>
                <li id="classes" class=" no-print has-sub-menu">
                    <a href="../classes">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-calendar-time"></div>
                        </div>
                        <span>Classes</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Subjects", $allowedArray) && in_array("Subjects", $allowedPages)){ ?>
                <li id="subjects" class=" no-print has-sub-menu">
                    <a href="../subjects">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-tasks-checked"></div>
                        </div>
                        <span>Subjects</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Payments", $allowedArray) && in_array("Payments", $allowedPages)){ ?>
                <li id="payments" class=" no-print has-sub-menu">
                    <a href="../payments">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-wallet-loaded"></div>
                        </div>
                        <span>Payments</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Attendance", $allowedArray) && in_array("Attendance", $allowedPages)){ ?>
                <li id="attendance" class=" no-print has-sub-menu">
                    <a href="../attendance">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-newspaper"></div>
                        </div>
                        <span>Attendance</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Child Pickup", $allowedArray) && in_array("Child Pickup", $allowedPages)){ ?>
                <li id="childpickup" class=" no-print has-sub-menu">
                    <a href="../childpickup">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-fingerprint"></div>
                        </div>
                        <span>Child Pickup</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Assessment", $allowedArray) && in_array("Assessment", $allowedPages)){ ?>
                <li id="assessment" class=" no-print has-sub-menu">
                    <a href="../assessment">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-edit-1"></div>
                        </div>
                        <span>Assessment</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Timetable", $allowedArray) && in_array("Timetable", $allowedPages)){ ?>
                <li id="timetable" class=" no-print has-sub-menu">
                    <a href="../timetable">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-clock"></div>
                        </div>
                        <span>Timetable</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Examination", $allowedArray) && in_array("Examination", $allowedPages)){ ?>
                <li id="examination" class=" no-print has-sub-menu">
                    <a href="../examination">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-pencil-1"></div>
                        </div>
                        <span>Examination</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Messaging", $allowedArray) && in_array("Messaging", $allowedPages)){ ?>
                <li id="messaging" class=" no-print has-sub-menu">
                    <a href="../messaging">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-email-forward"></div>
                        </div>
                        <span>Messaging</span>
                    </a>
                </li>
           <?php }
        ?>
        <?php
            if(in_array("Library", $allowedArray) && in_array("Library", $allowedPages)){ ?>
                <li id="library" class=" no-print has-sub-menu">
                    <a href="../library">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-book"></div>
                        </div>
                        <span>Library</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Laboratory", $allowedArray) && in_array("Laboratory", $allowedPages)){ ?>
                <li id="laboratory" class=" no-print has-sub-menu">
                    <a href="../laboratory">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-edit-3"></div>
                        </div>
                        <span>Laboratory</span>
                    </a>
                </li>
            <?php }
        ?>
        <!-- <li id="hostel" class=" no-print has-sub-menu">
            <a href="../hostel">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-home"></div>
                </div>
                <span>Hostel</span>
            </a>
        </li> -->
        <!-- <li id="research_center" class=" no-print has-sub-menu">
            <a href="../research_center">
                <div class=" no-print icon-w">
                    <div class=" no-print os-icon os-icon-ui-02"></div>
                </div>
                <span>Research</span>
            </a>
        </li> -->
        <?php
            if(in_array("Inventory", $allowedArray) && in_array("Inventory", $allowedPages)){ ?>
                <li id="inventory" class=" no-print has-sub-menu">
                    <a href="../inventory">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-database"></div>
                        </div>
                        <span>Inventory</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("TuckShop", $allowedArray) && in_array("TuckShop", $allowedPages)){ ?>
                <li id="tuckshop" class=" no-print has-sub-menu">
                    <a href="../tuckshop">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-ui-44"></div>
                        </div>
                        <span>Tuckshop</span>
                    </a>
                </li>
           <?php }
        ?>
        <?php
            if(in_array("Games", $allowedArray) && in_array("Games", $allowedPages)){ ?>
                <li id="educational_games" class=" no-print has-sub-menu">
                    <a href="../educational_games">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-crown"></div>
                        </div>
                        <span>Games</span>
                    </a>
                </li>
            <?php }
        ?>
        <?php
            if(in_array("Reports", $allowedArray) && in_array("Reports", $allowedPages)){ ?>
                <li id="reports" class=" no-print has-sub-menu">
                    <a href="../reports">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-bar-chart-stats-up"></div>
                        </div>
                        <span>Report</span>
                    </a>
                </li>
           <?php }
        ?>
        <?php
            if(in_array("Settings", $allowedPages)){ ?>
                <li id="settings" class=" no-print has-sub-menu">
                    <a href="../settings">
                        <div class=" no-print icon-w">
                            <div class=" no-print os-icon os-icon-settings"></div>
                        </div>
                        <span>Settings</span>
                    </a>
                </li>
            <?php }
        ?>
    </ul>
    <div class=" no-print side-menu-magic">
        <h4>
        Do you need any help?
        </h4>
        <p style="margin-top:10px;">
        Click button below to get instant help on how to use the system! 
        </p>
        <div class=" no-print btn-w" style="margin-top:20px;">
        <a href="../documentation" class="btn btn-white btn-rounded" style="color:#8095A0; display: inline;" href="" >Help me now</a>
        </div>
    </div>
</div>