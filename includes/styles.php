    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="../../addons/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../../addons/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../../addons/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="../../addons/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../addons/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="../../addons/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="../../css/main.php" rel="stylesheet">
    <link href="../../css/toastr.min.css" rel="stylesheet">
    <link href="../../css/jquery-ui.min.css" rel="stylesheet">
    <link href="../../css/slick.css" rel="stylesheet">
    <link href="../../css/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
 