<div class="no-print top-bar color-scheme-light">

<?php

    $pathArr = explode("/", strtok($_SERVER["REQUEST_URI"],'?'));
    $fileName = $pathArr[count($pathArr)-1];
    $fileName2 = $pathArr[count($pathArr)-2];
    if($fileName == "staff_assessment" || $fileName2 == "library" || $fileName2 == "documentation" || $fileName == "chemistryLab" || $fileName == "biologyLab" || $fileName == "physicsLab"){ ?>
    <div class="logo-w">
    <?php
        if($_SESSION["role"] == "tutor" || $_SESSION["role"] == "form teacher"){ ?>
            <a class="logo" href="../../views/home/staff_dashboard">
                <img alt="" style="object-fit:scale-down margin-top:0px; width:45px; height:40px" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>">
                <div class="logo-label" style="margin-top:0px"><?php echo $schoolDetails->school_name .", ". $schoolDetails->city; ?></div>
            </a>
        <?php }else{ ?>
            <a class="logo" href="./">
                <img alt="" style="object-fit:scale-down margin-top:0px; width:45px; height:40px" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>">
                <div class="logo-label" style="margin-top:0px"><?php echo $schoolDetails->school_name .", ". $schoolDetails->city; ?></div>
            </a>
        <?php }
    ?>
    </div>

<?php }else{ ?>
    <!-- SPOT TO DISPLAY MESSAGES ACROSS SCHOOLS-->
<?php }

?>

<!-------------------- START - Top Menu Controls -------------------->
<div class="top-menu-controls">
    <!-------------------- START - Messages Link in secondary top menu -------------------->
    <a href="../messaging" style="text-decoration:none">
    <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
        <i class="os-icon os-icon-mail"></i>
        <div class="new-messages-count"><?php echo $schoolDetails->smsUnit ?></div>
    </div>
    </a>
    <!-------------------- END - Messages Link in secondary top menu -------------------->
    <div class="nametag">
        <h5 style="font-size: 1rem; padding-right:10px; padding-left:10px; padding-top:5px; margin-bottom: 8px;"><?php echo ucwords($_SESSION["fullname"]); ?></h5>
    </div>
    <!-------------------- START - User avatar and menu in secondary top menu -------------------->
    <div class="logged-user-w">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" style="object-fit:scale-down" src="../../images/male-user-profile-picture.png">
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w">
                        <img alt="" style="object-fit:scale-down" src="../../images/male-user-profile-picture.png">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name"><?php echo ucwords($_SESSION["fullname"]); ?></div>
                        <div class="logged-user-role"><?php echo $_SESSION["role"]; ?></div>
                    </div>
                </div>
                <div class="bg-icon">
                    <i class="os-icon os-icon-user"></i>
                </div>
                <ul>
                    <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){ ?>
                    <li>
                        <a href="#">
                            <i class="os-icon os-icon-user-male-circle2"></i>
                            <span>Profile Details</span>
                        </a>
                    </li>
                    <?php }else { ?>
                    <li>
                        <a href="../staffs/profile?staff_id=<?php echo base64_encode($_SESSION["uniqueNumber"]); ?>">
                            <i class="os-icon os-icon-user-male-circle2"></i>
                            <span>Profile Details</span>
                        </a>
                    </li>
                    <?php } ?>
                    <?php
                        if($_SESSION["role"] == "admin"){ ?>
                            <li>
                                <a href="../settings/?tab=subscriptionsPage">
                                    <i class="os-icon os-icon-coins-4"></i>
                                    <span>Billing Details</span>
                                </a>
                            </li>
                        <?php }
                    ?>
                    <li>
                        <a data-target="#changePasswordModal" data-toggle="modal" href="#">
                            <i class="os-icon os-icon-robot-1"></i>
                            <span>Change Password</span>
                        </a>
                    </li>
                    <li>
                        <a href="../authentication/logout.php">
                            <i class="os-icon os-icon-signs-11"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-------------------- END - User avatar and menu in secondary top menu -------------------->
</div>
<!-------------------- END - Top Menu Controls -------------------->
</div>
<!-------------------- START - MODAL CALLERS -------------------->
    <?php
    //include_once ("modals/modalbtn.php");
        if(isset($err)){ ?>
            <a hidden data-target="#errorModal" id="errorCaller" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                class="my_hover_up btn btn-white btn-sm" href="#">
            </a>
     <?php   }
        if(isset($mess)){ ?>
            <a hidden data-target="#successModal" id="successCaller" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                class="my_hover_up btn btn-white btn-sm" href="#">
            </a>
    <?php    }
    ?>
    <?php
        if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){
            if($schoolDetails->daysLeft <= 30 && $schoolDetails->daysLeft > 15){ ?>
                <div class="no-print alert alert-warning" style="display: flex; text-align: center; margin-left: 280px; margin-right: 350px; margin-top: -52px; padding: 10px;">
                    <h6>Your account will expire in <?php echo $schoolDetails->daysLeft; ?> day(s)<h6>
                    <a href="../settings/renew" class="btn btn-primary btn-rounded" style="margin-top : -15px; margin-bottom: -19px; margin-left: 30px;">Renew Plan</a>
                </div>
            <?php }else{
                if($schoolDetails->daysLeft < 15){ ?>
                    <div class="no-print alert alert-danger" style="display: flex; text-align: center; margin-left: 280px; margin-right: 350px; margin-top: -52px; padding: 10px;">
                        <h6 style="color: #ffffff;">Your account will expire in <?php echo $schoolDetails->daysLeft; ?> day(s)<h6>
                        <a href="../settings/renew" class="btn btn-primary btn-rounded" style="margin-top : -15px; margin-bottom: -19px; margin-left: 30px;">Renew Plan</a>
                    </div>
                <?php }
                }
        }
    ?>
<!-------------------- END - MODAL CALLERS -------------------->