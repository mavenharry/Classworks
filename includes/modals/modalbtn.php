<?php
include_once ("modalClass.php");
$modalClass = new modal;

if(isset($_POST["createStudent"])){
    $studentNumber = htmlentities(trim($_POST["studentNumber"]));
    $fullname = htmlentities(trim($_POST["studentName"]));
    $class = htmlentities(trim($_POST["studentClass"]));
    $studentCountry = htmlentities(trim($_POST["studentCountry"]));
    $studentState = htmlentities(trim($_POST["studentState"]));
    $studentCity = htmlentities(trim($_POST["studentCity"]));
    $studentDob = htmlentities(trim($_POST["studentDob"]));
    $studentGender = htmlentities(trim($_POST["studentGender"]));
    $studentBloodGroup = htmlentities(trim($_POST["studentBloodGroup"]));
    $studentFatherContact = "0".substr(htmlentities(trim($_POST["studentFatherContact"])), -10, 10);
    $studentMotherContact = "0".substr(htmlentities(trim($_POST["studentMotherContact"])), -10, 10);
    $dobArray = explode("/", $studentDob);
    $studentDob = $dobArray[2]."/".$dobArray[1]."/".$dobArray[0];
    // MOVE STUDENT PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $modalClass->getSchoolDetails()->pathToPassport;
            $filename = strtolower(str_replace("/","_",$studentNumber).".".$extension);
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }
    $reply = $modalClass->addNewStudent($studentNumber, $fullname, $class, $studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $studentFatherContact, $studentMotherContact, @$filename);
    switch ($reply) {
        case "exist":
            # code...
            $err = 'Couldnt add '.$fullname.' because a student already exist with similar record';
            break;
        case "true":
            # code....
            $mess = $fullname.' has been added to student list successfully';
            break;
        default:
            # code...
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
            break;
    }
    

}

if(isset($_POST["createStaff"])){
    $staffNumber = htmlentities(trim($_POST["staffNumber"]));
    $staffName = htmlentities(trim($_POST["staffName"]));
    $staffCountry = htmlentities(trim($_POST["staffCountry"]));
    $staffState = htmlentities(trim($_POST["staffState"]));
    $staffCity = htmlentities(trim($_POST["staffCity"]));
    $staffQualification = htmlentities(trim($_POST["staffQualification"]));
    $staffGender = htmlentities(trim($_POST["staffGender"]));
    $staffMaritalStatus = htmlentities(trim($_POST["staffMaritalStatus"]));
    $staffAddress = htmlentities(trim($_POST["staffAddress"]));
    $staffDepartment = htmlentities(trim($_POST["staffDepartment"]));
    $staffPhone = "0".substr(htmlentities(trim($_POST["staffPhone"])), -10, 10);
    $staffEmail = htmlentities(trim($_POST["staffEmail"]));
    @$staffSubjects = $_POST["staffSubjects"];

    // MOVE STUDENT PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $modalClass->getSchoolDetails()->pathToPassport;
            $filename = strtolower(str_replace("/","_",$staffNumber).".".$extension);
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }

    $reply = $modalClass->addNewStaff($staffNumber, $staffName, $staffCountry, $staffState, $staffCity, $staffQualification, $staffGender, $staffMaritalStatus, $staffAddress, $staffDepartment, $staffPhone, $staffEmail, $staffSubjects, @$filename);
    switch ($reply) {
        case "exist":
            # code...
            $err = 'Couldnt add '.$staffName.' because a staff already exist with that email';
            break;
        case "true":
            # code....
            $mess = $staffName.' has been added to staff list successfully and message will be sent';
            break;
        case 'insufficient':
            $err = $staffName.' has been added to staff list but message was not sent due to insufficient unit left';
            break;
        case 'updatesms':
            $err = $staffName.' has been added to staff list and message sent but could not update sms unit left';
            break;
        case 'invalidparam':
            $err = $staffName.' has been added successfully but didnt receive sms. Please contact support with error code: 1702';
            break;
        case 'invalidlogin':
            $err = $staffName.' has been added successfully but didnt receive sms. Please contact support with error code: 1703';
            break;
        case 'insufficientunit':
            $err = $staffName.' has been added successfully but didnt receive sms. Please contact support with error code: 1704';
            break;
        case 'numbers':
            $err = "Numbers to receive message too long. Max is 500";
            break;
        case 'internal':
            $err = $staffName.' has been added successfully but didnt receive sms. Please contact support with error code: 1706';
            break;
        case 'smserror':
            $err = $staffName.' has been added successfully but didnt receive sms. An unexpected error occured';
            break;
        default:
            # code...
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
            break;
    }
}

if(isset($_POST["createParent"])){
    $parentNumber = htmlentities(trim($_POST["parentNumber"]));
    $parentName = htmlentities(trim($_POST["parentName"]));
    $parentCountry = htmlentities(trim($_POST["parentCountry"]));
    $parentState = htmlentities(trim($_POST["parentState"]));
    $parentCity = htmlentities(trim($_POST["parentCity"]));
    $parentRelation = htmlentities(trim($_POST["parentRelation"]));
    $parentGender = htmlentities(trim($_POST["parentGender"]));
    $parentMaritalStatus = htmlentities(trim($_POST["parentMaritalStatus"]));
    $parentAddress = htmlentities(trim($_POST["parentAddress"]));
    $parentOccupation = htmlentities(trim($_POST["parentOccupation"]));
    $parentPhone = "0".substr(htmlentities(trim($_POST["parentPhone"])), -10, 10);
    $parentEmail = htmlentities(trim($_POST["parentEmail"]));

    // MOVE STUDENT PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $modalClass->getSchoolDetails()->pathToPassport;
            $filename = str_replace("/","_",$parentNumber).".".$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }

    $reply = $modalClass->addNewParent($parentNumber, $parentName, $parentCountry, $parentState, $parentCity, $parentRelation, $parentGender, $parentMaritalStatus, $parentAddress, $parentOccupation, $parentPhone, $parentEmail, @$filename);

    switch ($reply) {
        case "exist":
            # code...
            $err = 'Couldnt add '.$parentName.' because a parent already exist with that email';
            break;
        case "true":
            # code....
            $mess = $parentName.' has been added to the parent list successfully';
            break;
        default:
            # code...
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
            break;
    }
}

if(isset($_POST["createSubject"])){
    $subjectCode = htmlentities(trim($_POST["subjectCode"]));
    $subjectName = htmlentities(trim($_POST["subjectName"]));
    $subjectClasss = $_POST["subjectClasss"];
    $reply = $modalClass->addNewSubject($subjectCode, $subjectName, $subjectClasss);
    if($reply === true){
        $mess = $subjectName. ' has been added successfully';
    }else{
        switch ($reply) {
            case 'exist':
                # code...
                $err = 'Could not add '.$subjectName.' because it already exist.';
                break;
            
            default:
                # code...
                $err = 'Could not add '.$subjectName.' because of an unexpected error. Please try again later';
                break;
        }
    }
}

if(isset($_POST["createClass"])){
    $classCode = htmlentities(trim($_POST["classCode"]));
    $className = htmlentities(trim($_POST["className"]));
    @$classSubjects = $_POST["classSubjects"];

    $reply = $modalClass->createClasses($classCode, $className, $classSubjects);

    if($reply === true){
        $mess = $className.' has been created successfully';
    }else{
        switch ($reply) {
            case 'exist':
                # code...
                $err = 'Could not create '.$className.'. because it already exist';
                break;
            
            default:
                # code...
                $err = 'An unexpected error occurred. Could not create '.$className;
                break;
        }
    }
}


if(isset($_POST["sendMessage"])){
    @$audienceBroadcast = $_POST["audienceBroadcast"];
    @$broadcastMessage = htmlentities(trim($_POST["broadcastMessage"]));
    @$broadcastDate = htmlentities(trim($_POST["broadcastDate"]));
    @$specifiedNumbers = $_POST["specifiedNumbers"];
    @$whenBroadcast = htmlentities(trim($_POST["whenBroadcast"]));
    @$classAudience = htmlentities(trim($_POST["classAudience"]));
    @$BroadcastClasses = $_POST["BroadcastClasses"];
    $numbers = array();
    $logNumbers = array();

    // print_r($audienceBroadcast);


    foreach($audienceBroadcast as $key => $value){
        if($value == "Specify"){
            // FOR SPECIFIED PEOPLE
            $rawArray = explode(",", $specifiedNumbers);
            foreach($rawArray as $key => $value){
                array_push($numbers, $value);
            }
            $logNumbers = $numbers;
        }elseif($value == "classes"){
            // FOR CLASSES
            foreach($BroadcastClasses as $key => $classCode){
                $rawReply = $modalClass->getClassesNumbers($classAudience, $classCode);
                foreach($rawReply as $key => $value){
                    array_push($numbers, $value);
                }
                $logNumbers = $numbers;
            }
        }else{
            // FOR GENERAL
            $reply = $modalClass->getAudienceNumbers($value, "phone");
            while($row = array_shift($reply)){
                $numbers[] = $row["phone"];
            }
        }
    }

    //  echo count($numbers);
    if($whenBroadcast == "Now"){
        $broadcastDate = date("d/m/Y");
    }
    $explodeDate = explode("/", $broadcastDate);
    $formattedDate = $explodeDate[2]."-".$explodeDate[1]."-".$explodeDate[0];
    
    if($broadcastDate == date("d/m/Y")){
        $reply = $modalClass->sendSMS($broadcastMessage, $modalClass->getSchoolDetails()->domain_name, $numbers);
        if($reply === true){
            // LOG MESSAGE
            $broadcastDate = date("Y-m-d");
            $logMessage = $modalClass->logMessage($audienceBroadcast, $formattedDate, $broadcastMessage, @$logNumbers, 1);
            if($logMessage === true){
                $mess = "Message has been broadcasted";
            }else{
                $err = "Message has been broadcasted but has not been logged. Please contact support";
            }  
        }else{
            switch($reply){
                case 'alreadySent':
                    $mess = "Message has already been broadcasted";
                    break;
                case 'insufficient':
                    $err = "You do not have enough sms unit left. Please recharge and try again";
                    break;
                case 'updatesms':
                    $err = "Message has been broadcasted but could not update sms unit left. Please contact support";
                    break;
                case 'invalidparam':
                    $err = "System encountered error. Please contact support with error code: 1702";
                    break;
                case 'invalidlogin':
                    $err = "System encountered error. Please contact support with error code: 1703";
                    break;
                case 'insufficientunit':
                    $err = "System encountered error. Please contact support with error code: 1704";
                    break;
                case 'numbers':
                    $err = "Numbers to receive message too long. Max is 500";
                    break;
                case 'internal':
                    $err = "System encountered error. Please contact support with error code: 1706";
                    break;
                case 'smserror':
                    // $err = "An unexpected error occurred. Please try again";
                    $err = $reply;
                    break;
            }
        }
        
    }else{
        // LOG MESSAGE
        $pageNumber = ceil(strlen($broadcastMessage)/160);
        $totalNumber = $pageNumber * count($numbers);
        $smsUnit = $modalClass->checkSmsBalance();

        if($totalNumber > $smsUnit){
            $err = "You do not have enough sms unit left. Please recharge and try again";
        }else{
            $logMessage = $modalClass->logMessage(json_encode($audienceBroadcast), $formattedDate, $broadcastMessage, json_encode(@$logNumbers), 0);
            if($logMessage === true){
                $mess = "Message has been saved and will be sent on the ".$broadcastDate;
            }else{
                $err = "Could not save this message due to unexpected errors. Please try again later";
            }
        }
    }
}

if(isset($_POST["publishNotice"])){
    @$broadcastMessage = htmlentities(trim($_POST["broadcastMessage"]));
    $reply = $modalClass->publishQuickNotice($broadcastMessage);
    if($reply === true){
        $mess = "Quick notice has been published";
    }else{
        $err = "An error occurred while trying to publish this notice. Please try again later";
    }
}

if(isset($_POST["createPickup"])){

    $pickupNumber = htmlentities(trim($_POST["pickupNumber"]));
    $pickupName = htmlentities(trim($_POST["pickupName"]));
    $pickupCountry = htmlentities(trim($_POST["pickupCountry"]));
    $pickupState = htmlentities(trim($_POST["pickupState"]));
    $pickupCity = htmlentities(trim($_POST["pickupCity"]));
    $pickupRelation = htmlentities(trim($_POST["pickupRelation"]));
    $pickupGender = htmlentities(trim($_POST["pickupGender"]));
    $pickupMaritalStatus = htmlentities(trim($_POST["pickupMaritalStatus"]));
    $pickupAddress = htmlentities(trim($_POST["pickupAddress"]));
    $pickupOccupation = htmlentities(trim($_POST["pickupOccupation"]));
    $pickupPhone = "0".substr(htmlentities(trim($_POST["pickupPhone"])), -10, 10);
    $pickupEmail = htmlentities(trim($_POST["pickupEmail"]));
    $pickupStudent = htmlentities(trim($_POST["pickupStudent"]));

    // MOVE PICKUP PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $modalClass->getSchoolDetails()->pathToPassport;
            $filename = str_replace("/","_",$pickupNumber).".".$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }

    $reply = $modalClass->addNewPickup($pickupNumber, $pickupName, $pickupCountry, $pickupState, $pickupCity, $pickupRelation, $pickupGender, $pickupMaritalStatus, $pickupAddress, $pickupOccupation, $pickupPhone, $pickupEmail, $pickupStudent, @$filename, $_SESSION["fulname"]);

    switch ($reply) {
        case "exist":
            # code...
            $err = 'Couldnt add '.$pickupName.' because someboby already exist with that email';
            break;
        case "true":
            # code....
            $mess = $pickupName.' has been added to the pickup list successfully';
            break;
        default:
            # code...
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
            break;
    }


}



if(isset($_POST["updateTimeTable"])){
    $timeTableSubjects = htmlentities(trim($_POST["timeTableSubjects"]));
    $timetableTutor = htmlentities(trim($_POST["timetableTutor"]));
    $timetableModalClass = htmlentities(trim($_POST["timetableModalClass"]));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    if(!empty($timeTableSubjects) && !empty($timetableTutor)){
        $reply = $modalClass->updateTiming($timeTableSubjects, $timetableTutor, $timetableModalClass, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
}


if(isset($_POST["updateStaffTimeTable"])){
    $timeTableSubjects = htmlentities(trim($_POST["stafftimeTableSubjects"]));
    $timetableTutor = htmlentities(trim($_POST["timetableModalStaffCode"]));
    $timetableModalClass = htmlentities(trim($modalClass->getClassCode($_POST["stafftimeTableclasses"])));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    if(!empty($timeTableSubjects) && !empty($timetableTutor)){
        $reply = $modalClass->updateTimingStaff($timeTableSubjects, $timetableTutor, $timetableModalClass, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
} 


if(isset($_POST["deleteTimeTable"])){
    $timetableModalClass = htmlentities(trim($_POST["timetableModalClass"]));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    $subject = htmlentities(trim($_POST["timeTableSubjects"]));
    if(!empty($timetableModalTime) && !empty($timetableModalClass) && !empty($timetableModalDay)){
        $reply = $modalClass->deleteTiming($subject, $timetableModalClass, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
}


if(isset($_POST["deleteStaffTimeTable"])){
    $timetableTutor = htmlentities(trim($_POST["timetableModalStaffCode"]));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    if(!empty($timetableModalTime) && !empty($timetableTutor) && !empty($timetableModalDay)){
        $reply = $modalClass->deleteTimingStaff($timetableTutor, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
}


if(isset($_POST["addTimeTable"])){
    $timeTableSubjects = htmlentities(trim($_POST["timeTableSubjects"]));
    $timetableTutor = htmlentities(trim($_POST["timetableTutor"]));
    $timetableModalClass = htmlentities(trim($_POST["timetableModalClass"]));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    if(!empty($timeTableSubjects) && !empty($timetableTutor) && !empty($timetableModalClass) && !empty($timetableModalDay) && !empty($timetableModalTime)){
        $reply = $modalClass->addNewTiming($timeTableSubjects, $timetableTutor, $timetableModalClass, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
}   



if(isset($_POST["addStaffTimeTable"])){
    $timeTableSubjects = htmlentities(trim($_POST["stafftimeTableSubjects"]));
    $timetableTutor = htmlentities(trim($_POST["timetableModalStaffCode"]));
    $timetableModalClass = htmlentities(trim($modalClass->getClassCode($_POST["stafftimeTableclasses"])));
    $timetableModalDay = htmlentities(trim($_POST["timetableModalDay"]));
    $timetableModalTime = htmlentities(trim($_POST["timetableModalTime"]));
    $session = htmlentities(trim($_POST["timetableModalSession"]));
    $term = htmlentities(trim($_POST["timetableModalTerm"]));
    if(!empty($timeTableSubjects) && !empty($timetableTutor) && !empty($timetableModalClass) && !empty($timetableModalDay) && !empty($timetableModalTime)){
        $reply = $modalClass->addNewTiming($timeTableSubjects, $timetableTutor, $timetableModalClass, $timetableModalDay, $timetableModalTime, $session, $term);
        if($reply === true){
            //echo '<script type="text/javascript">toastr.success("Timetable updated successfully!")</script>'; 
        }else{
            //<script>toastr.error('Sorry an error occured!')</script>
        }
    }else{
        // <script>toastr.error('Sorry an error occured!')</script>
    }
}   

if(isset($_POST["deleteUser"])){
    $numberToDelete = htmlentities(trim($_POST["idToDelete"]));
    $whoToDelete = htmlentities(trim($_POST["whoToDelete"]));

    $reply = $modalClass->DeleteUser($numberToDelete, $whoToDelete);

    if($reply === true){
        $mess = $whoToDelete." has been deleted succesfully";
    }else{
        switch ($reply) {
            case 'not found':
                # code...
                $err = "A ".$whoToDelete." with number ".$numberToDelete." was not found in the system";
                break;
            case 0:
                # code...
                $err = "An error occured while trying to delete this person. Please try again later";
                break;
            
            default:
                # code...
                $err = "An error occured while trying to delete this person. Please try again later";
                break;
        }
    }
}

if(isset($_POST["classDelBtn"])){
    $classes = $_POST["deleteClass"];
    $reply = $modalClass->deleteClass($classes);
    if($reply === true){
        $mess = "You have successfully deleted a class";
    }else{
        $err = "Couldn't complete your last operation. Please try again later";
    }
}

if(isset($_POST["editClassName"])){
    $formerClassName = htmlentities(trim($_POST["formerName"]));
    $newClassName = htmlentities(trim($_POST["newName"]));

    $reply = $modalClass->renameClass($formerClassName, $newClassName);
    switch ($reply) {
        case 'exist':
            # code...
            $err = "This name already exist in the system";
            break;
        case 'true':
            # code
            $mess = "Class name changed successfully";
            break;
        case 'error':
            # code
            $err = "An error occured while trying to change class name. Please try again later";
            break;
        default:
            # code...
            $err = "An error occured while trying to perform your last operation. Please try again later";
            break;
    }
}

if(isset($_POST["changePassword"])){
    $oldPassword = htmlentities(trim($_POST["oldPassword"]));
    $newPassword = htmlentities(trim($_POST["newPassword"]));
    $newPassword2 = htmlentities(trim($_POST["newPassword2"]));
    
    if($newPassword == $newPassword2){
        $reply = $modalClass->changePassword($oldPassword, $newPassword);
        switch($reply){
            case 'changed':
                $mess = "Password has been changed successfully";
                break;
            case 'error':
                $err = "An error occurred while trying to update password. Please try again later";
                break;
            case 'wrongpassword':
                $err = "The password you entered is wrong";
                break;
            default:
                $err = "An unexpected error occurred while trying to update your password. Please contact support";
                break;
        }
    }else{
        $err = "New passwords do not match";
    }
}

if(isset($_POST["delSubjectBtn"])){
    $delSubjects = $_POST["delSubjects"];
    
    $reply = $modalClass->deleteSubject($delSubjects);

    if($reply === true){
        $mess = "The subject(s) you selected has been deleted";
    }else{
        $err = "Couldn't delete the subject you selected. Please try again later";
    }
}

if(isset($_POST["academicInfo"])){
    @$academicSession = htmlentities(trim($_POST["academicSession"]));
    @$academicTerm = htmlentities(trim($_POST["academicTerm"]));
    @$sessionEndDate = htmlentities(trim($_POST["sessionEndDate"]));

    $reply = $modalClass->updateAcademicInfo($academicSession, $academicTerm, $sessionEndDate);

    if($reply === true){
        $mess = "Academic session has been updated";
    }else{
        $err = "Couldn't update academic session. Please try again later";
    }
}

if(isset($_POST["createFamily"])){
    $studentFatherContact = htmlentities(trim($_POST["studentFatherContact"]));
    $studentMotherContact = htmlentities(trim($_POST["studentMotherContact"]));
    $familyName = htmlentities(trim($_POST["familyName"]));

    $reply = $modalClass->createFamily($familyName, $studentFatherContact, $studentMotherContact);

    if($reply === true){
        $mess = "Family has been created successfully";
    }else{
        switch($reply){
            case 'motherexist':
                $err = "Mothers phone number already exist with a family";
                break;
            case 'fatherexist':
                $err = "Mothers phone number already exist with a family";
                break;
            default:
                $err = "An error occurred while trying to perform your last operation, Please contact support";
                break;

        }
    }
}

if(isset($_POST["publishEnrollment"])){
    $enrollYear = htmlentities(trim($_POST["enrollYear"]));
    $enrollBatch = htmlentities(trim($_POST["enrollBatch"]));
    $enrollClasses = $_POST["enrollClasses"];
    $enrollStartDate = htmlentities(trim($_POST["enrollStartDate"]));
    $enrollEndDate = htmlentities(trim($_POST["enrollEndDate"]));
    $enrollFeeState = htmlentities(trim($_POST["enrollFeeState"]));
    $enrollFeeAmount = htmlentities(trim($_POST["enrollFeeAmount"]));
    $enrollslot = htmlentities(trim($_POST["enrollslot"]));

    // START DATE
    $explodedStart = explode("/", $enrollStartDate);
    $enrollStartDate = $explodedStart[2]."-".$explodedStart[1]."-".$explodedStart[0];
    // END DATE
    $explodedEnd = explode("/", $enrollEndDate);
    $enrollEndDate = $explodedEnd[2]."-".$explodedEnd[1]."-".$explodedEnd[0];

    $reply = $modalClass->updateEnrollmentTable($enrollYear, $enrollBatch, $enrollClasses, $enrollStartDate, $enrollEndDate, $enrollFeeState, $enrollFeeAmount, $enrollslot);
    if($reply === true){
        $mess = "Enrollment settings has been update.";
    }else{
        $err = "Could not update enrollment settings now. Please contact support";
    }
}

if(isset($_POST["addNewStock"])){
    $ItemName = strtolower(htmlentities(trim($_POST["ItemName"])));
    $ItemSize = strtolower(htmlentities(trim($_POST["ItemSize"])));
    $ItemColor = strtolower(htmlentities(trim($_POST["ItemColor"])));
    $ItemQuantity = htmlentities(trim($_POST["ItemQuantity"]));
    $ItemPrice = htmlentities(trim($_POST["ItemPrice"]));
    $ItemLocation = htmlentities(trim($_POST["ItemLocation"]));
    
    $reply = $modalClass->addNewInventory($ItemName, $ItemSize, $ItemColor, $ItemQuantity, $ItemPrice, $ItemLocation);
    switch($reply){
        case "true":
            $mess = "Item added successfully";
            break;
        case 'exist':
            $err = "An item already exist with this name";
            break;
        case "false":
            $err = "An unexpected error occurred while adding item to stock";
            break;
        default:
            $err = "An unexpected error occurred while adding item to stock. Please contact support";
            break;
    }
}

if(isset($_POST["inventoryConfiguration"])){
    $lowStockCount = htmlentities(trim($_POST["lowStockCount"]));
    $outStockCount = htmlentities(trim($_POST["outStockCount"]));

    $reply = $modalClass->inventoryConfigure($lowStockCount, $outStockCount);
    if($reply == "true"){
        $mess = "Inventory configuration has been updated";
    }else{
        $err = "An error occurred while trying to update inventory configuration. Please contact support";
    }
}

if(isset($_POST["collectBtn"])){
    $itemName = strtolower(htmlentities(trim($_POST["itemName"])));
    $itemColor = strtolower(htmlentities(trim($_POST["itemColor"])));
    $itemSize = strtolower(htmlentities(trim($_POST["itemSize"])));
    $itemQuantity = htmlentities(trim($_POST["itemQuantity"]));
    $CollectedBy = htmlentities(trim($_POST["CollectedBy"]));
    
    $reply = $modalClass->recordCollect($itemName, $itemColor, $itemSize, $itemQuantity, $CollectedBy);
    switch($reply){
        case 'true':
            $mess = "Record has been taken";
            break;
        case 'insufficient':
            $err = "Cannot process request because quantity demanded for is higher than quantity in stock";
            break;
        case 'false':
            $err = "An unexpected error occurred while processing request. Please contact support";
            break;
        default :
            $err = "An unexpected error occurred. Please contact support";
            break;
    }
}

if(isset($_POST["changeStaffPassword"])){
    $staffId = htmlentities(trim(base64_decode($_GET["staff_id"])));
    $password = htmlentities(trim($_POST["newPassword"]));
    $password2 = htmlentities(trim($_POST["newPassword2"]));

    if(empty($password) || empty($password2)){
        $err = "Password field cannot be empty";
    }else{
        if($password != $password2){
            $err = "Password do not match";
        }else{
            if($password == $password2){
                $reply = $modalClass->staffchangePassword($staffId, $password);
                if($reply == "true"){
                    $mess = "Staff Password has been changed";
                }else{
                    if($reply == "false"){
                        $err = "Could not complete this operation. Please try again later or contact support bellow";
                    }else{
                        $err = "An unexpected error occurred. Please check back later while we fix it";
                    }
                }
            }else{
                $err = "An unexpected error occured. Please confirm ypur input and try again";
            }
        }
    }
}

if(isset($_POST["addProduct"])){
    $itemName = strtolower(htmlentities(trim($_POST["ItemName"])));
    $itemQuantity = htmlentities(trim($_POST["ItemQuantity"]));
    $itemPrice = htmlentities(trim($_POST["ItemPrice"]));
    $reply = $modalClass->addShopProduct($itemName, $itemQuantity, $itemPrice);
    if($reply === true){
        $mess = "Product has been added to stock";
    }else{
        if($reply == "updated"){
            $mess = "A product was found with same name and quantity has been updated";
        }else{
            $err = "An unexpected error occurred while trying to add item to stock. Please contact support";
        }
    }
}

if(isset($_POST["tuckshopConfiguration"])){
    $alertWalletBalance = isset($_POST["alertWalletBalance"]) ? 1 : 0;
    $amountToAlert = htmlentities(trim($_POST["amountToAlert"]));
    $bankName = htmlentities(trim($_POST["bankName"]));
    $accountName = htmlentities(trim($_POST["accountName"]));
    $accountNumber = htmlentities(trim($_POST["accountNumber"]));

    $reply = $modalClass->configureTuckshop($alertWalletBalance, $amountToAlert, $bankName, $accountName, $accountNumber);
    switch($reply){
        case 'true':
            $mess = "Tuckshop has been successfully configured";
            break;
        case 'updated':
            $mess = "Tuckshop configuration has been updated successfully";
            break;
        case 'setup':
            $err = "Could not create tuckshop account. Please contact support";
            break;
        case 'updateError':
            $err = "System encountered an issue when updating tuckshop account. Please contact support";
            break;
        case 'error':
            $err = "System encountered an internal error updating tuckshop account. Please contact support";
            break;
    }
}


if(isset($_POST["promoteButton"])){
    $fromClass = htmlentities(trim($_POST["fromClass"]));
    $toClass = htmlentities(trim($_POST["toClass"]));
    if($fromClass != $toClass){
        $reply = $modalClass->promoteClassStudents($fromClass, $toClass);
        switch($reply){
            case 'true':
                $mess = "Students Promoted Successfully";
                break;
            case 'false':
                $err = "Sorry, we encountered a problem promoting students.";
                break;
        }
    }else{
        $err = "Sorry, you can't promote student to the same class.";
    }
}


if(isset($_POST["promoteSingleButton"])){
    $studentNumber = base64_decode($_GET["stdn"]);
    $toClass = htmlentities(trim($_POST["studentToClass"]));
    $reply = $modalClass->promoteSingleStudents($studentNumber, $toClass);
    switch($reply){
        case 'true':
            $mess = "Student has been Promoted Successfully";
            break;
        case 'false':
            $err = "Sorry, we encountered a problem promoting this student.";
            break;
    }
}

if(isset($_POST["setupExam"])){
    $examClass = htmlentities(trim($_POST["examClass"]));
    $examSubject = htmlentities(trim($_POST["examSubject"]));
    $examDuration = htmlentities(trim($_POST["examDuration"]));
    $examSupervisor = htmlentities(trim($_POST["examSupervisor"]));
    $startTime = htmlentities(trim($_POST["startTime"]));
    $reply = $modalClass->configureExam($examClass, $examSubject, $examDuration, $examSupervisor, $startTime);
    if($reply === true){
        $mess = "Exam settings has been updated successfully";
    }else{
        $err = "An error occurred while trying to update exam settings. Please contact support";
    }
}

if(isset($_POST["schoolStampBtn"])){
    // MOVE STUDENT PASSPORT
    $stampClasses = $_POST['stampClasses'];
    if($_FILES["stampLogo"]["size"] > 0){
        if($_FILES["stampLogo"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["stampLogo"]["error"];
            $err = $error;
        }else{
            $extension = @end(explode(".",$_FILES["stampLogo"]["name"]));
            $imageFolder = "../../".$_SESSION["folderName"]."/SchoolImages";
            $filename = "stamp_".substr(uniqid(), -4).'.'.$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["stampLogo"]["tmp_name"], $imageFolder."/".$filename);
            $reply = $modalClass->updateStamp($stampClasses, $filename);
            if($reply === true){
                $mess = "School stamp updated successfully";
            }else{
                $err = "An error occurred while trying to update school stamp. Please contact support";
            }
        }
    }else{
        echo "Sdsd";
    }
}
?>