<?php
    include_once ("modalbtn.php");
    $classes = $modalClass->getClasses();
    $allSubject = $modalClass->getAllSubjects();
    // $subjects = $modalClass->getAllSubjects();
    $subjects_distinct = $modalClass->getAllDistinctSubjects();
    $allStaffs = $modalClass->getAllStaffs();
    $allStudents = $modalClass->fetchAllStudent();
    $allGrades = $modalClass->getSystemGrades();
    //$allFees = $modalClass->getAllFees("class001");
    $distinctFee = $modalClass->getDistinctFeeName();
    if(isset($_GET["class"])){
        $classSubject = $modalClass->getClassSubject($modalClass->getClassName($_GET["class"]));
    }
    $distinctAssessment = $modalClass->getDistinctAssesssment();
    $midTermState = $modalClass->getMidTermState();
    $items = $modalClass->getStockItem();
    $families = $modalClass->getFamilies();
?>

<!-- NEW STUDENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newStudentModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new student</h4>
              <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="row form-group col-sm-12" style="margin-top:40px; margin-bottom:20px;">
                <div class="col-sm-6">
                    <label style="cursor:pointer; margin-top:20px; border-color:#08ACF0; background-color:#08ACF0" class="my_hover_up btn btn-primary" for="studentupload-photo"> Student Photo from Camera</label>
                    <label style="cursor:pointer; margin-top:5px; border-color:#FC9933; background-color:#FC9933" class="my_hover_up btn btn-primary" for="studentupload-photo"> Student Photo from Computer</label>
                    <input id="studentupload-photo" accept="image/png, image/jpeg, image/gif"  style="opacity: 0; position: absolute; z-index: -1; " class="form-control" type="file" name="profilePhoto">                    
                </div>
                <div class="pt-avatar-w col-sm-6" style="border-radius:0px;">
                    <img alt="" id="studentuploadPreview" style="margin-left:80px; border-radius:5px; border: 2px solid #08ACF0; height:130px; width:120px" src="../../images/male-user-profile-picture.png">
                </div>
                </div>

                <div class="form-group">
                  <label for=""> Student's Registration No.</label><input name="studentNumber" class="form-control" readonly value="<?php echo $modalClass->newStudentNumber(); ?>" type="text">
                </div>
                <div class="row">
                <div class="form-group col-sm-7">
                    <label for=""> Student's Full name <span style="color:#F91F14">*</span></label><input required name="studentName" class="form-control" placeholder="E.g; Kings Smart Amadi" type="text">
                </div>
                <div class="form-group col-sm-5">
                <label for="">Student's Class</label>
                <select name="studentClass" class="form-control">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                </select>
                </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label>
                        <Select name="studentCountry" class="form-control">
                        <option value="nigeria">Nigeria</option>
                        <option value="algeria">Algeria</option>
                        <option value="angola">Angola</option>
                        <option value="benin">Benin</option>
                        <option value="botswana">Botswana</option>
                        <option value="burkina Faso">Burkina Faso</option>
                        <option value="burundi">Burundi</option>
                        <option value="cameroon">Cameroon</option>
                        <option value="cape-verde">Cape Verde</option>
                        <option value="central-african-republic">Central African Republic</option>
                        <option value="chad">Chad</option>
                        <option value="comoros">Comoros</option>
                        <option value="congo-brazzaville">Congo - Brazzaville</option>
                        <option value="congo-kinshasa">Congo - Kinshasa</option>
                        <option value="ivory-coast">Côte d’Ivoire</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="egypt">Egypt</option>
                        <option value="equatorial-guinea">Equatorial Guinea</option>
                        <option value="eritrea">Eritrea</option>
                        <option value="ethiopia">Ethiopia</option>
                        <option value="gabon">Gabon</option>
                        <option value="gambia">Gambia</option>
                        <option value="ghana">Ghana</option>
                        <option value="guinea">Guinea</option>
                        <option value="guinea-bissau">Guinea-Bissau</option>
                        <option value="kenya">Kenya</option>
                        <option value="lesotho">Lesotho</option>
                        <option value="liberia">Liberia</option>
                        <option value="libya">Libya</option>
                        <option value="madagascar">Madagascar</option>
                        <option value="malawi">Malawi</option>
                        <option value="mali">Mali</option>
                        <option value="mauritania">Mauritania</option>
                        <option value="mauritius">Mauritius</option>
                        <option value="mayotte">Mayotte</option>
                        <option value="morocco">Morocco</option>
                        <option value="mozambique">Mozambique</option>
                        <option value="namibia">Namibia</option>
                        <option value="niger">Niger</option>
                        <option value="nigeria">Nigeria</option>
                        <option value="rwanda">Rwanda</option>
                        <option value="reunion">Réunion</option>
                        <option value="saint-helena">Saint Helena</option>
                        <option value="senegal">Senegal</option>
                        <option value="seychelles">Seychelles</option>
                        <option value="sierra-leone">Sierra Leone</option>
                        <option value="somalia">Somalia</option>
                        <option value="south-africa">South Africa</option>
                        <option value="sudan">Sudan</option>
                        <option value="south-sudan">Sudan</option>
                        <option value="swaziland">Swaziland</option>
                        <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                        <option value="tanzania">Tanzania</option>
                        <option value="togo">Togo</option>
                        <option value="tunisia">Tunisia</option>
                        <option value="uganda">Uganda</option>
                        <option value="western-sahara">Western Sahara</option>
                        <option value="zambia">Zambia</option>
                        <option value="zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                        <input required name="studentState" class="form-control" placeholder="E.g; Lagos" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region <span style="color:#F91F14">*</span></label>
                        <input required  name="studentCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Date of Birth <span style="color:#F91F14">*</span></label>
                        <div class="date-input">
                        <input required  name="studentDob" class="date_of_birth form-control" placeholder="Date of birth" type="text">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Gender</label>
                        <Select id="studentGender" name="studentGender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option><option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Blood Group</label>
                        <Select name="studentBloodGroup" class="form-control">
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="AB">AB</option>
                        <option value="O">O</option>
                        <option value="Not Known">Not Known</option>
                        </Select>
                     </div>
                  </div>
               </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Father's Phone</label>
                        <input name="studentFatherContact" id="studentFatherContact" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Mother's Phone</label>
                        <input name="studentMotherContact" id="studentMotherContact" class="form-control" placeholder="E.g; +2348746389437" type="text">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for=""> Student's Mother's Name</label>
                <input name="studentMotherName" id="studentMotherName" class="form-control" placeholder="Mother's Name" disabled type="text">
                </div>
                <div class="form-group">
                <label for=""> Student's Father's Name</label>
                <input name="studentFatherName" id="studentFatherName" class="form-control" placeholder="Father's Name" disabled type="text">
                </div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input id="studentCheck" class="form-check-input" type="checkbox">I hereby verify that this person is a student.</label></div>
                <div class="form-buttons-w" style="margin-top:15px; ">
                    <input type="submit" value=" Save This Student" id="createStudent" disabled name="createStudent" class="my_hover_up btn btn-primary">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW STAFF -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newStaffModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new staff</h4>
              <form method="POST" action="#" enctype="multipart/form-data">

                <div class="row form-group col-sm-12" style="margin-top:40px; margin-bottom:20px;">
                <div class="col-sm-6">
                    <label style="cursor:pointer; margin-top:20px; border-color:#974A6D; background-color:#974A6D" class="my_hover_up btn btn-primary" for="staffupload-photo"> Staff Photo from Camera</label>
                    <label style="cursor:pointer; margin-top:5px; background-color:#FC9933; border-color:#FC9933" class="my_hover_up btn btn-primary" for="staffupload-photo"> Staff Photo from Computer</label>
                    <input id="staffupload-photo" accept="image/png, image/jpeg, image/gif"  style="opacity: 0; position: absolute; z-index: -1; " class="form-control" name="profilePhoto" type="file">                    
                </div>
                <div class="pt-avatar-w col-sm-6" style="border-radius:0px;">
                    <img alt="" id="staffuploadPreview" style="margin-left:80px; border-radius:5px; border: 2px solid #974A6D; height:130px; width:120px" src="../../images/male-user-profile-picture.png">
                </div>
                </div>

                <div class="form-group">
                  <label for=""> Staff's Registration No.</label><input name="staffNumber" class="form-control"  readonly value="<?php echo $modalClass->newStaffNumber(); ?>" type="text">
                </div>
                <div class="form-group">
                    <label for=""> Staff's Full name <span style="color:#F91F14">*</span></label><input required  name="staffName" class="form-control" placeholder="E.g; Mrs. Happiness Smart" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label>
                        <Select name="staffCountry" class="form-control">
                        <option value="nigeria">Nigeria</option>
                        <option value="algeria">Algeria</option>
                        <option value="angola">Angola</option>
                        <option value="benin">Benin</option>
                        <option value="botswana">Botswana</option>
                        <option value="burkina Faso">Burkina Faso</option>
                        <option value="burundi">Burundi</option>
                        <option value="cameroon">Cameroon</option>
                        <option value="cape-verde">Cape Verde</option>
                        <option value="central-african-republic">Central African Republic</option>
                        <option value="chad">Chad</option>
                        <option value="comoros">Comoros</option>
                        <option value="congo-brazzaville">Congo - Brazzaville</option>
                        <option value="congo-kinshasa">Congo - Kinshasa</option>
                        <option value="ivory-coast">Côte d’Ivoire</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="egypt">Egypt</option>
                        <option value="equatorial-guinea">Equatorial Guinea</option>
                        <option value="eritrea">Eritrea</option>
                        <option value="ethiopia">Ethiopia</option>
                        <option value="gabon">Gabon</option>
                        <option value="gambia">Gambia</option>
                        <option value="ghana">Ghana</option>
                        <option value="guinea">Guinea</option>
                        <option value="guinea-bissau">Guinea-Bissau</option>
                        <option value="kenya">Kenya</option>
                        <option value="lesotho">Lesotho</option>
                        <option value="liberia">Liberia</option>
                        <option value="libya">Libya</option>
                        <option value="madagascar">Madagascar</option>
                        <option value="malawi">Malawi</option>
                        <option value="mali">Mali</option>
                        <option value="mauritania">Mauritania</option>
                        <option value="mauritius">Mauritius</option>
                        <option value="mayotte">Mayotte</option>
                        <option value="morocco">Morocco</option>
                        <option value="mozambique">Mozambique</option>
                        <option value="namibia">Namibia</option>
                        <option value="niger">Niger</option>
                        <option value="nigeria">Nigeria</option>
                        <option value="rwanda">Rwanda</option>
                        <option value="reunion">Réunion</option>
                        <option value="saint-helena">Saint Helena</option>
                        <option value="senegal">Senegal</option>
                        <option value="seychelles">Seychelles</option>
                        <option value="sierra-leone">Sierra Leone</option>
                        <option value="somalia">Somalia</option>
                        <option value="south-africa">South Africa</option>
                        <option value="sudan">Sudan</option>
                        <option value="south-sudan">Sudan</option>
                        <option value="swaziland">Swaziland</option>
                        <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                        <option value="tanzania">Tanzania</option>
                        <option value="togo">Togo</option>
                        <option value="tunisia">Tunisia</option>
                        <option value="uganda">Uganda</option>
                        <option value="western-sahara">Western Sahara</option>
                        <option value="zambia">Zambia</option>
                        <option value="zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                        <input required name="staffState" class="form-control" placeholder="E.g; Lagos" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region <span style="color:#F91F14">*</span></label>
                        <input required name="staffCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Highest Qualification</label>
                        <Select name="staffQualification" class="form-control">
                        <option value="SSCE">SSCE</option>
                        <option value="OND">OND</option>
                        <option value="HND">HND</option>
                        <option value="B.ED">B.A</option>
                        <option value="B.SC">B.SC</option>
                        <option value="B.ED">B.ED</option>
                        <option value="B.ED">B.ENG</option>
                        <option value="B.ED">B.TECH</option>
                        <option value="M.ED">M.A</option>
                        <option value="M.SC">M.SC</option>
                        <option value="M.ED">M.ED</option>
                        <option value="M.ED">M.TECH</option>
                        <option value="M.ED">M.ENG</option>
                        <option value="B.ED">MSPH</option>
                        <option value="M.ED">NCE</option>
                        <option value="PGD">PGD</option>
                        <option value="PHD">PHD</option>
                        <option value="DBA">DBA</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Gender</label>
                        <Select name="staffGender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option><option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Marital Status</label>
                        <Select name="staffMaritalStatus" class="form-control">
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                        <option value="Divorced">Divorced</option>
                        <option value="Separated">Separated</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for="">Residential Address</label>
                <input name="staffAddress" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Department/Role</label>
                        <Select name="staffDepartment" id="staffDepartment" class="form-control">
                        <option value="">Select Department</option>
                        <option value="Tutor">Tutor</option>
                        <option value="form teacher">Form Teacher</option>
                        <option value="Accountant">Accountant</option>
                        <option value="Librarian">Librarian</option>
                        <option value="Security">Security</option>
                        <option value="Cleaner">Cleaner</option>
                        <option value="Caretaker">Caretaker</option>
                        <option value="Store Keeper">Store Keeper</option>
                        <option value="Secretary">Secretary</option>
                        <option value="Caterer">Caterer</option>
                        <option value="Admin">Administrator</option>
                        <option value="Principal">Principal</option>
                        <option value="Director">Director</option>
                        <option value="ICT Officer">ICT Officer</option>                        
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Phone Number <span style="color:#F91F14">*</span></label>
                        <input required name="staffPhone" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                    <div class="col-sm-12">
                     <div class="form-group">
                        <label for=""> Email Address</label>
                        <input name="staffEmail" class="form-control" placeholder="E.g; James@gmail.com" type="email">
                     </div>
                    </div>
               </div>
               <div class="form-group">
                    <label for="">Select Subject(s)</label>
                    <select class="form-control select2" name="staffSubjects[]" style="width:100%" multiple="multiple">
                        <?php
                            for($i = 0; $i < count($allSubject); $i++){ ?>
                                <option value="<?php echo $allSubject[$i]->subjectCode; ?>"><?php echo ucfirst($allSubject[$i]->subjectName)." - ". ucwords($allSubject[$i]->subjectClass)?></option>
                        <?php    }
                        ?>
                    </select>
                </div>


                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input id="staffCheck" class="form-check-input" type="checkbox">I hereby verify that this person is a staff.</label></div>
                <div class="form-buttons-w" style="margin-top:15px; ">
                    <input type="submit" value=" Save Staff Details" name="createStaff" id="createStaff" disabled class="my_hover_up btn btn-primary" style="border-color:#1BB4AD; background-color:#1BB4AD">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW PICKUP PERSON -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newPickupModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="text-align:right; margin-top:-40px; color:#8095A0">Add a new pickup person</h4>
              <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="row form-group col-sm-12" style="margin-top:40px; margin-bottom:20px;">
                <div class="col-sm-6">
                    <label style="cursor:pointer; margin-top:20px; border-color:#FC9933; background-color:#FC9933" class="my_hover_up btn btn-primary" for="pickupUpload-photo"> Pickup Photo from Camera</label>
                    <label style="cursor:pointer; margin-top:5px; background-color:#8095A0; border-color:#8095A0" class="my_hover_up btn btn-primary" for="pickupUpload-photo"> Pickup Photo from Computer</label>
                    <input required id="pickupUpload-photo" accept="image/png, image/jpeg, image/gif"  style="opacity: 0; position: absolute; z-index: -1; " class="form-control" name="profilePhoto" type="file">                    
                </div>
                <div class="pt-avatar-w col-sm-6" style="border-radius:0px;">
                    <img alt="" id="pickupUploadPreview" style="margin-left:80px; border-radius:5px; border: 2px solid #FC9933; height:130px; width:120px" src="../../images/male-user-profile-picture.png">
                </div>
                </div>

                <div class="form-group">
                  <label for=""> Pickup's Registration No.</label><input name="pickupNumber" class="form-control" readonly value="<?php echo $modalClass->newPickupNumber(); ?>" type="text">
                </div>
                <div class="form-group">
                    <label for=""> Pickup's Full name</label><input name="pickupName" class="form-control" placeholder="E.g; Kings Smart Amadi" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label>
                        <Select name="pickupCountry" class="form-control">
                        <option value="nigeria">Nigeria</option>
                        <option value="algeria">Algeria</option>
                        <option value="angola">Angola</option>
                        <option value="benin">Benin</option>
                        <option value="botswana">Botswana</option>
                        <option value="burkina Faso">Burkina Faso</option>
                        <option value="burundi">Burundi</option>
                        <option value="cameroon">Cameroon</option>
                        <option value="cape-verde">Cape Verde</option>
                        <option value="central-african-republic">Central African Republic</option>
                        <option value="chad">Chad</option>
                        <option value="comoros">Comoros</option>
                        <option value="congo-brazzaville">Congo - Brazzaville</option>
                        <option value="congo-kinshasa">Congo - Kinshasa</option>
                        <option value="ivory-coast">Côte d’Ivoire</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="egypt">Egypt</option>
                        <option value="equatorial-guinea">Equatorial Guinea</option>
                        <option value="eritrea">Eritrea</option>
                        <option value="ethiopia">Ethiopia</option>
                        <option value="gabon">Gabon</option>
                        <option value="gambia">Gambia</option>
                        <option value="ghana">Ghana</option>
                        <option value="guinea">Guinea</option>
                        <option value="guinea-bissau">Guinea-Bissau</option>
                        <option value="kenya">Kenya</option>
                        <option value="lesotho">Lesotho</option>
                        <option value="liberia">Liberia</option>
                        <option value="libya">Libya</option>
                        <option value="madagascar">Madagascar</option>
                        <option value="malawi">Malawi</option>
                        <option value="mali">Mali</option>
                        <option value="mauritania">Mauritania</option>
                        <option value="mauritius">Mauritius</option>
                        <option value="mayotte">Mayotte</option>
                        <option value="morocco">Morocco</option>
                        <option value="mozambique">Mozambique</option>
                        <option value="namibia">Namibia</option>
                        <option value="niger">Niger</option>
                        <option value="nigeria">Nigeria</option>
                        <option value="rwanda">Rwanda</option>
                        <option value="reunion">Réunion</option>
                        <option value="saint-helena">Saint Helena</option>
                        <option value="senegal">Senegal</option>
                        <option value="seychelles">Seychelles</option>
                        <option value="sierra-leone">Sierra Leone</option>
                        <option value="somalia">Somalia</option>
                        <option value="south-africa">South Africa</option>
                        <option value="sudan">Sudan</option>
                        <option value="south-sudan">Sudan</option>
                        <option value="swaziland">Swaziland</option>
                        <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                        <option value="tanzania">Tanzania</option>
                        <option value="togo">Togo</option>
                        <option value="tunisia">Tunisia</option>
                        <option value="uganda">Uganda</option>
                        <option value="western-sahara">Western Sahara</option>
                        <option value="zambia">Zambia</option>
                        <option value="zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province</label>
                        <input name="pickupState" class="form-control" placeholder="E.g; Lagos" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region</label>
                        <input name="pickupCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Relation to Student</label>
                        <Select name="pickupRelation" class="form-control">
                        <option value="Grand Father">Grand Father</option>
                        <option value="Grand Mother">Grand Mother</option>
                        <option value="Father">Father</option>
                        <option value="Mother">Mother</option>
                        <option value="Uncle">Uncle</option>
                        <option value="Aunt">Aunt</option>
                        <option value="Brother">Brother</option>
                        <option value="Sister">Sister</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Gender</label>
                        <Select name="pickupGender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option><option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Marital Status</label>
                        <Select name="pickupMaritalStatus" class="form-control">
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                        <option value="Divorced">Divorced</option>
                        <option value="Separated">Separated</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for=""> Residential Address</label>
                <input name="pickupAddress" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Occupation</label>
                        <input name="pickupOccupation" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Phone Number</label>
                        <input name="pickupPhone" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                    <div class="col-sm-12">
                     <div class="form-group">
                        <label for=""> Email Address</label>
                        <input name="pickupEmail" class="form-control" placeholder="E.g; James@gmail.com" type="email">
                     </div>
                    </div>
               </div>

               <div class="row">
                    <div class="col-sm-12">
                     <div class="form-group">
                        <label for=""> Student's Number</label>
                        <input name="pickupStudent" class="form-control" id="pickupStudent" placeholder="E.g; <?php echo date("Y")?>/Stdn/<?php echo $schoolDetails->domain_name ?>/002" type="text">
                     </div>
                    </div>
               </div>

                <div class="form-group">
                <label for=""> Student's Name</label>
                <input name="pickupStudentName" id="pickupStudentName" class="form-control" id="pickupStudentName" placeholder="Student's Name" disabled type="text">
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this person is a parent.</label></div>
                <div class="form-buttons-w" style="margin-top:15px; ">
                    <input type="submit" id="createPickup" value="Add Pickup Person & Save" name="createPickup" disabled class="my_hover_up btn btn-primary" style="border-color:#1BB4AD; background-color:#1BB4AD">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW PARENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newParentModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new parent/guardian</h4>
              <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="row form-group col-sm-12" style="margin-top:40px; margin-bottom:20px;">
                <div class="col-sm-6">
                    <label style="cursor:pointer; margin-top:20px; border-color:#FC9933; background-color:#FC9933" class="my_hover_up btn btn-primary" for="parentupload-photo"> Guardian Photo from Camera</label>
                    <label style="cursor:pointer; margin-top:5px; background-color:#8095A0; border-color:#8095A0" class="my_hover_up btn btn-primary" for="parentupload-photo"> Guardian Photo from Computer</label>
                    <input id="parentupload-photo" accept="image/png, image/jpeg, image/gif"  style="opacity: 0; position: absolute; z-index: -1; " class="form-control" name="profilePhoto" type="file">                    
                </div>
                <div class="pt-avatar-w col-sm-6" style="border-radius:0px;">
                    <img alt="" id="parentuploadPreview" style="margin-left:80px; border-radius:5px; border: 2px solid #FC9933; height:130px; width:120px" src="../../images/male-user-profile-picture.png">
                </div>
                </div>

                <div class="form-group">
                  <label for=""> Guardian's Registration No.</label><input name="parentNumber" class="form-control" readonly value="<?php echo $modalClass->newParentNumber(); ?>" type="text">
                </div>
                <div class="form-group">
                    <label for=""> Guardian's Full name <span style="color:#F91F14">*</span></label><input required  name="parentName" class="form-control" placeholder="E.g; Mr. Mika Amadi" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label>
                        <Select name="parentCountry" class="form-control">
                        <option value="nigeria">Nigeria</option>
                        <option value="algeria">Algeria</option>
                        <option value="angola">Angola</option>
                        <option value="benin">Benin</option>
                        <option value="botswana">Botswana</option>
                        <option value="burkina Faso">Burkina Faso</option>
                        <option value="burundi">Burundi</option>
                        <option value="cameroon">Cameroon</option>
                        <option value="cape-verde">Cape Verde</option>
                        <option value="central-african-republic">Central African Republic</option>
                        <option value="chad">Chad</option>
                        <option value="comoros">Comoros</option>
                        <option value="congo-brazzaville">Congo - Brazzaville</option>
                        <option value="congo-kinshasa">Congo - Kinshasa</option>
                        <option value="ivory-coast">Côte d’Ivoire</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="egypt">Egypt</option>
                        <option value="equatorial-guinea">Equatorial Guinea</option>
                        <option value="eritrea">Eritrea</option>
                        <option value="ethiopia">Ethiopia</option>
                        <option value="gabon">Gabon</option>
                        <option value="gambia">Gambia</option>
                        <option value="ghana">Ghana</option>
                        <option value="guinea">Guinea</option>
                        <option value="guinea-bissau">Guinea-Bissau</option>
                        <option value="kenya">Kenya</option>
                        <option value="lesotho">Lesotho</option>
                        <option value="liberia">Liberia</option>
                        <option value="libya">Libya</option>
                        <option value="madagascar">Madagascar</option>
                        <option value="malawi">Malawi</option>
                        <option value="mali">Mali</option>
                        <option value="mauritania">Mauritania</option>
                        <option value="mauritius">Mauritius</option>
                        <option value="mayotte">Mayotte</option>
                        <option value="morocco">Morocco</option>
                        <option value="mozambique">Mozambique</option>
                        <option value="namibia">Namibia</option>
                        <option value="niger">Niger</option>
                        <option value="nigeria">Nigeria</option>
                        <option value="rwanda">Rwanda</option>
                        <option value="reunion">Réunion</option>
                        <option value="saint-helena">Saint Helena</option>
                        <option value="senegal">Senegal</option>
                        <option value="seychelles">Seychelles</option>
                        <option value="sierra-leone">Sierra Leone</option>
                        <option value="somalia">Somalia</option>
                        <option value="south-africa">South Africa</option>
                        <option value="sudan">Sudan</option>
                        <option value="south-sudan">Sudan</option>
                        <option value="swaziland">Swaziland</option>
                        <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                        <option value="tanzania">Tanzania</option>
                        <option value="togo">Togo</option>
                        <option value="tunisia">Tunisia</option>
                        <option value="uganda">Uganda</option>
                        <option value="western-sahara">Western Sahara</option>
                        <option value="zambia">Zambia</option>
                        <option value="zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                        <input required name="parentState" class="form-control" placeholder="E.g; Lagos" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region <span style="color:#F91F14">*</span></label>
                        <input required name="parentCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Relation to Student</label>
                        <Select name="parentRelation" class="form-control">
                        <option value="Grand Father">Grand Father</option>
                        <option value="Grand Mother">Grand Mother</option>
                        <option value="Father">Father</option>
                        <option value="Mother">Mother</option>
                        <option value="Uncle">Uncle</option>
                        <option value="Aunt">Aunt</option>
                        <option value="Brother">Brother</option>
                        <option value="Sister">Sister</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Gender</label>
                        <Select name="parentGender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option><option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Marital Status</label>
                        <Select name="parentMaritalStatus" class="form-control">
                        <option value="Single">Single</option>
                        <option value="Married">Married</option>
                        <option value="Divorced">Divorced</option>
                        <option value="Separated">Separated</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for=""> Residential Address</label>
                <input name="parentAddress" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Occupation</label>
                        <input name="parentOccupation" class="form-control" placeholder="E.g; Lawyer" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Phone Number <span style="color:#F91F14">*</span></label>
                        <input required name="parentPhone" id="parentPhone" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                    <div class="col-sm-12">
                     <div class="form-group">
                        <label for=""> Email Address</label>
                        <input name="parentEmail" class="form-control" placeholder="E.g; James@gmail.com" type="email">
                     </div>
                    </div>
               </div>

                <div class="form-group">
                <label for=""> Student's Name</label>
                <input name="parentStudentName" id="parentStudentName" class="form-control" placeholder="Student's Name" disabled type="text">
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input id="parentChecker" class="form-check-input" type="checkbox">I hereby verify that this person is a parent.</label></div>
                <div class="form-buttons-w" style="margin-top:15px; ">
                    <input type="submit" id="createParent" value="Create this Guardian & Save" name="createParent" disabled class="my_hover_up btn btn-primary" style="border-color:#1BB4AD; background-color:#1BB4AD">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW CLASS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newClassModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new class</h4>
              <form id="formValidate"  method="POST" action="">

                <div class="form-group">
                  <label for=""> Class Code.</label><input name="classCode" class="form-control" readonly value="<?php echo $modalClass->newClassNumber(); ?>" type="text">
                </div>
                <div class="form-group">
                    <label for=""> Class Name</label><input required name="className" class="form-control" placeholder="E.g; JSS 1A" type="text">
                </div>
                
                <div class="form-group">
                    <label for="">Select Subject(s)</label>
                    <select class="form-control select2" name="classSubjects[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($allSubject); $i++){ ?>
                            <option value="<?php echo $allSubject[$i]->subjectCode; ?>"><?php echo ucfirst($allSubject[$i]->subjectName)." - ". ucwords($allSubject[$i]->subjectClass)?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that these are valid subjects.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <input type="submit" class="my_hover_up btn btn-primary" name="createClass" style="border-color:#1BB4AD; background-color:#1BB4AD" value=" Assign Subject(s) & Save">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW SUBJECTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newSubjectModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new subject</h4>
              <form id="formValidate" method="POST" action="">

                <div class="form-group">
                  <label for=""> Subject Code.</label><input name="subjectCode" class="form-control" readonly value="<?php echo $modalClass->newSubjectNumber(); ?>" type="text">
                </div>
                <div class="form-group">
                    <label for=""> Subject Name <span style="color:#F91F14">*</span></label><input required name="subjectName" class="form-control" placeholder="E.g; English Language" type="text">
                </div>

                <div class="form-group">
                <label for="">Subject Class(es) <span style="color:#F91F14">*</span></label>
                <select class="form-control select2" id="subjectClass" required name="subjectClasss[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                </select>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby verify that these are valid subjects.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <input name="createSubject" type="submit" value=" Assign Class(es) & Save" class="my_hover_up btn btn-primary" style="border-color:#1BB4AD; background-color:#1BB4AD">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- NEW ENROLLMENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newEnrollModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Publish new enrollments</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Select Academic Session</label>
                    <select class="form-control" name="enrollYear">
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Select Enrollment Batch</label>
                    <select class="form-control" name="enrollBatch">
                        <option value="Batch A">Batch A</option>
                        <option value="Batch B">Batch B</option>
                        <option value="Batch C">Batch C</option>
                        <option value="Batch D">Batch D</option>
                        <option value="Batch E">Batch E</option>
                        <option value="Batch F">Batch F</option>
                        <option value="Batch G">Batch G</option>
                    </select>
                </div>
                </div>

                <div class="form-group">
                    <label for="">Select Class(es) for Enrollment</label>
                    <select class="form-control select2" name="enrollClasses[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Starts Date</label>
                            <div class="date-input">
                                <input name="enrollStartDate" class="date_of_birth form-control" placeholder="Admission start date" type="text" value="<?php echo date("d/m/Y") ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Ends Date</label>
                            <div class="date-input">
                                <input name="enrollEndDate" class="date_of_birth form-control" placeholder="Admission end date" type="text" value="<?php $today = time();  $strToTime = strtotime("+30 days", $today); echo date("d/m/Y", $strToTime) ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Application Fee</label>
                            <select class="form-control" name="enrollFeeState">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Application Fee Amount</label>
                            <div class="">
                                <input name="enrollFeeAmount" class="form-control" placeholder="E.g, 5000" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for=""> Admission Slots Available</label>
                    <div class="">
                        <input name="enrollslot" class="form-control" placeholder="Number of admission slots" type="number" value="100">
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby verify that this is a valid publication.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="publishEnrollment" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Publish this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- UPDATE ACADEMIC INFO -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newAcademicModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Update academic info</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Select Academic Year</label>
                        <select required class="form-control" name="academicSession">
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">Select Academic Term</label>
                        <select class="form-control" name="academicTerm">
                            <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                            <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                            <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option> 
                        </select>
                    </div>
                </div>

                <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                    <label for=""> Next term begins</label>
                    <div class="date-input">
                        <input name="sessionEndDate" class="date_of_birth form-control" placeholder="Admission end date" type="text" value="<?php $today = time();  $strToTime = strtotime("+120 days", $today); echo date("d/m/Y", $strToTime) ?>">
                    </div>
                    </div>
                </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" required type="checkbox">I hereby verify that this is a valid academic info.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="academicInfo" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW BROADCAST -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="broadcastModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Broadcast notice</h4>
              <form id="formValidate" method="POST" asction="#">

                <div class="row">
                    <div class="form-group col-sm-7">
                        <label for="">Broadcast Audience</label>
                        <select class="form-control select2" style="width:100%" multiple="true" name="audienceBroadcast[]" id="audienceBroadcast">
                            <option value="teachers">Staffs</option>
                            <option value="students">Students</option>
                            <option value="parents">Parents</option>
                            <option value="Specify">Specify</option>
                            <option value="classes">Classes</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-5">
                        <label for="">When to send?</label>
                        <select class="form-control" name="whenBroadcast" id="whenBroadcast">
                        <option value="Now">Now</option>
                        <option value="Schedule">Schedule for later date</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-7" id="BroadcastClasses" style="display:none">
                        <label for="">Broadcast Class</label>
                        <select class="form-control select2" style="width:100%" multiple="true" name="BroadcastClasses[]" id="audienceBroadcast">
                            <option value="">Select Class</option>
                            <?php
                                foreach($classes as $class){ ?>
                                    <option value="<?php echo $class->class_code; ?>"><?php echo $class->class_name; ?></option>
                                <?php }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-sm-5" id="BroadcastCategory" style="display:none">
                        <label for="">Class Audience</label>
                        <select class="form-control" style="width:100%" name="classAudience">
                            <option value="">Select Audience</option>
                            <option value="parent">Parent</option>
                            <option value="teachers">Teachers</option>
                        </select>
                    </div>
                </div>

                <div class="form-group" id="specifyNumbers" style="display: none;"><label> Specify numbers to receive this message</label><textarea name="specifiedNumbers" class="form-control" rows="5" placeholder="Phone numbers should be seperated by a comma e.g 08018230291,09029182930"></textarea></div>

                <div class="row" id="broadcastDate" style="display:none">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for=""> Set Broadcast Date</label>
                        <div class="date-input">
                        <input name="broadcastDate" class="date_of_birth form-control" placeholder="Broadcast start date" type="text" value="<?php echo date("d/m/Y") ?>">
                    </div>
                </div>
            </div>
        </div>

                <div class="form-group"><label> Message to send</label><label style="float: right" id="textCount">0</label><textarea name="broadcastMessage" id="messageCount" class="form-control" rows="5" placeholder="E.g. All staff should immediately report to the conference hall."></textarea></div>
                <h6 style="float:right; margin-top: -10px;" id="pages">0 Page(s)</h6>
                
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid broadcast.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <input type="submit" style="border-color:#08ACF0; background-color:#08ACF0" name="sendMessage" class="my_hover_up btn btn-primary" value=" Broadcast this now">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- QUICK NOTICE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="noticeModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Quick notice</h4>
              <form id="formValidate" method="POST" asction="#">

                <div class="form-group"><label> Message to send</label><textarea name="broadcastMessage" id="messageCount" class="form-control" rows="5" placeholder="E.g. All staff should immediately report to the conference hall."></textarea></div>
                
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid broadcast.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <input type="submit" style="border-color:#08ACF0; background-color:#08ACF0" name="publishNotice" class="my_hover_up btn btn-primary" value=" Publish Notice">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- CHANGE PASSWORD MODAL-->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="changePasswordModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Change Password</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Old Password <span style="color:#F91F14">*</span></label>
                        <input required name="oldPassword" class="form-control" placeholder="Old Password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> New Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Repeat Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword2" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" required type="checkbox">I hereby verify that this is a valid academic info.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="changePassword" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- CHANGE STAFF PASSWORD MODAL-->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="changeStaffPasswordModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Change Staff Password</h4>
              <form id="formValidate" method="POST">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> New Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Repeat Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword2" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" required type="checkbox">I hereby verify that this is a valid academic info.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="changeStaffPassword" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- CONTACT PARENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="contactParentModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Contact Parents</h4>
                <form id="formValidate" method="POST" asction="#">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="">Parent's Contact</label>
                            <select class="form-control select2" id="parentContact" style="width:100%" multiple="true" name="parentsContact[]" id="audienceBroadcast">

                            </select>
                        </div>
                    </div>

                    <div class="form-group"><label> Message to send</label><label style="float: right" id="textCount">0</label><textarea name="contactMessage" id="messageCount" class="form-control" rows="5" placeholder="E.g. All staff should immediately report to the conference hall."></textarea></div>
                    <h6 style="float:right; margin-top: -10px;" id="pages">0 Page(s)</h6>

                    <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid message.</label></div>
                    <div class="form-buttons-w" style="margin-top:15px;">
                        <input type="submit" style="border-color:#08ACF0; background-color:#08ACF0" name="contactParent" class="my_hover_up btn btn-primary" value=" Send Message">
                        <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- DELETE USERS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newDeleteModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Delete a <span class="agreeWho">student</span></h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for="">Who do you want to delete?</label>
                    <select class="form-control" id="whoToDelete" name="whoToDelete">
                    <option selected value="Student">Student</option>
                    <option value="Staff">Staff</option>
                    <option value="Parent">Parent</option>
                    <option value="Pickup">Pickup person</option>
                    </select>
                </div>

                <div class="form-group">
                <label for="" id="whichIdToDelete"> Student number</label><input id="idToDelete" name="idToDelete" class="form-control" placeholder="E.g; 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/016" type="text">
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby authenticate the deletion of this <span class="agreeWho">student</span>.</label></div>                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="deleteUser" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Delete <span class="agreeWho">student</span> now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>




<!-- DELETE ITEM-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="deleteItemModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Delete an Item</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for="" id="whichIdToDelete"> Item number</label><input name="itemToDelete" class="form-control" placeholder="E.g; Item001" type="text">
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby delete this item.</label></div>                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="deleteUser" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Delete this item now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>




<!-- DELETE PRODUCT-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="deleteProductModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Delete a Product</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for="" id="whichIdToDelete"> Product number</label><input name="productToDelete" class="form-control" placeholder="E.g; Product001" type="text">
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby delete this product.</label></div>                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="deleteUser" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Delete this product now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH PRODUCT-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchProductModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search a Product</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for="" id="whichIdToDelete"> Product number</label><input name="productToSearch" class="form-control" placeholder="E.g; Product001" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="deleteUser" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this product now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH ITEM-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchItemModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search a Product</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for="" id="whichIdToDelete"> Item number</label><input name="itemToSearch" class="form-control" placeholder="E.g; Item001" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="deleteUser" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this item now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- DELETE CLASSES-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newDeleteClassModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Delete a class</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for=""> Class Name</label>
                <select class="form-control select2" name="deleteClass[]" style="width:100%" multiple="multiple">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                        ?>
                </select>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby authenticate the deletion of this class.</label></div>                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="classDelBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Delete this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- DELETE SUBJECTS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newDeleteSubjectModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Delete a subject</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for=""> Select Subject(s)</label>
                    <select class="form-control select2" name="delSubjects[]" style="width:100%" multiple="multiple">
                        <?php
                            for($i = 0; $i < count($allSubject); $i++){ ?>
                                <option value="<?php echo $allSubject[$i]->subjectCode; ?>"><?php echo ucfirst($allSubject[$i]->subjectName)." - ". ucwords($allSubject[$i]->subjectClass)?></option>
                        <?php    }
                        ?>
                    </select>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby authenticate the deletion of this subject.</label></div>                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="delSubjectBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Delete this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH/FILTER ENROLLMENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchEnrollModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Enrollments</h4>
              <form id="formValidate">

                <div class="row">

                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                    <option value="JSS 1">JSS 1</option>
                    <option value="JSS 2">JSS 2</option>
                    <option value="JSS 3">JSS 3</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for=""> Date</label>
                    <div class="date-input">
                    <input name="sortDate" class="date_of_birth form-control" placeholder="Application date" type="text" value="<?php echo date("d/m/Y") ?>">
                    </div>
                </div>
                
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Applicant name or number" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH/FILTER STUDENTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchStudentsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Students</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                    <option value="">Select Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Gender</label>
                    <select class="form-control" name="sortGender">
                    <option value="">Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option><option value="Others">Others</option>
                    </select>
                </div>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Student number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="studentSort" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH/FILTER PAID STUDENTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchPaidStudentsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Paid Fees Students</h4>
              <form id="formValidate" method="POST">

                <div class="row">

                    <div class="form-group col-sm-6">
                        <label for="">Class</label>
                        <select class="form-control" name="sortClass" id="sortClassModal">
                        <option value="all">All Classes</option>
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                        <?php    }
                        ?>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Term</label>
                        <select class="form-control" name="sortTerm" id="sortTermModal">
                            <option value="all">All Terms</option>
                            <option value="1">1st Term</option>
                            <option value="2">2nd Term</option>
                            <option value="3">3rd Term</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Session</label>
                        <select class="form-control" name="sortSession">
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Fee Name</label>
                        <select class="form-control" name="sortFee" id="sortFeeModal">
                            <option value="All">All Fees</option>
                        </select>
                    </div>

                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Student number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" name="searchPayedStydent" style="border-color:#08ACF0; background-color:#08ACF0" value="Search this now">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH/FILTER OUTSTANDING STUDENTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchUnpaidStudentsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Outstanding Fees Students</h4>
              <form id="formValidate" method="POST">

                <div class="row">

                    <div class="form-group col-sm-6">
                        <label for="">Class</label>
                        <select id="outstandingClass" class="form-control" name="sortClass">
                        <option value="all">All Classes</option>
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                        <?php    }
                        ?>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Term</label>
                        <select class="form-control" name="sortTerm" id="outstandingTerm">
                        <option value="all">All Terms</option>
                        <option value="1">1st Term</option>
                        <option value="2">2nd Term</option>
                        <option value="3">3rd Term</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Session</label>
                        <select class="form-control" name="sortSession">
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Fee Name</label>
                        <select class="form-control" name="sortFee" id="outstandingFee">
                            <option value="all">All Fees</option>
                        </select>
                    </div>

                </div>

                <h4 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                    <div class="form-group">
                        <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Student number or name" type="text">
                    </div>

                    <div class="form-buttons-w" style="margin-top:30px;">
                        <input type="submit" name="searchOutstanding" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Search this now">
                        <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- ASSIGN OUTSTANDING STUDENTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="assignUnpaidStudentsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Assign Outstanding Fee to a Students</h4>
              <form id="formValidate" method="POST">

                <div class="row">

                    <div class="form-group col-sm-6">
                        <label for="">Class</label>
                        <select required id="sortSlipClass" class="form-control" name="assignClass">
                            <option value="">Select Class</option>
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                        <?php    }
                        ?>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for=""> Student Name</label>
                        <select required class="sortSlipStudent form-control" name="assignStudent">
                            <option value="">Select Student</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Session</label>
                        <select required class="form-control" name="assignSession">
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Term</label>
                        <select required id="sortSlipTerm" class="form-control" name="assignTerm">
                            <option value="">Select Fee</option>
                            <option value="1">1st Term</option>
                            <option value="2">2nd Term</option>
                            <option value="3">3rd Term</option>
                        </select>
                    </div>

                    <div  style="margin-top:10px;" class="form-group col-sm-6">
                        <label for="">Fee Name</label>
                        <select required id="sortSlipFee" class="form-control" name="assignFee">
                            <option value="">Select Fee</option>
                        </select>
                    </div>


                    <div style="margin-top:10px;" class="form-group col-sm-6">
                        <label for=""> Amount</label>
                        <input required name="assignAmount" class="form-control" placeholder="&#x20a6;0.00" type="text">
                    </div>

                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" name="assignFeeBtn" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Assign this fee now">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- PAYROLL SEARCH/FILTER -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="salarySearchModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Payroll Search/Filter</h4>
              <form id="formValidate">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for=""> Last Pay Date</label>
                    <input name="staffName" class="form-control date_of_birth" placeholder="Last pay date" value="<?php echo date("d/m/Y") ?>" type="text">
                </div>

                <div class="form-group col-sm-6">
                    <label for=""> Next Pay Date</label>
                    <input name="staffName" class="form-control date_of_birth" placeholder="Next pay date" value="<?php $today = time();  $strToTime = strtotime("+30 days", $today); echo date("d/m/Y", $strToTime) ?>" type="text">
                </div>

                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>     

                <div style="margin-top:10px;" class="form-group">
                <label for=""> Staff Name</label>
                <input name="staffName" class="form-control" placeholder="E.g; Joyce Amadi" type="text">
                </div>           

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SALARY DISBURSEMENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="salaryDisburseModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Salary Disbursement</h4>
              <form id="formValidate">

                <div class="row" style="margin-top:40px;">

                <div class="col-sm-5 el-tablo">
                    <div class="label">
                    Total Payout Amount
                    </div>
                    <div class="value">
                    &#x20a6;2,756,600
                    </div>
                </div>


                <h2 style="margin-top:7px; margin-left:-10px; margin-right:20px; height:0px;">|</h2>

                <div class="col-sm-5 el-tablo">
                    <div class="label">
                    Total Deductions
                    </div>
                    <div class="value">
                    &#x20a6;56,600
                    </div>
                </div>
                

                </div>

                <div style="margin-top:40px;" class="form-group">
                    <label for="">Select Staffs to Omit</label>
                    <select class="form-control select2" name="staffSubjects" style="width:100%" multiple="multiple">
                    <?php
                    if(count($allStaffs) > 0){
                        for($i = 0; $i < count($allStaffs); $i++){ ?>
                            <option value="<?php echo $allStaffs[$i]->staff_number; ?>"><?php echo ucfirst($allStaffs[$i]->fullname) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No tutor found in record.</option> 
                    <?php } ?>
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Make payment now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH/FILTER PARENTS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchParentsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Guardian</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                        <option value="">Select Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Relationship</label>
                    <select class="form-control" name="sortRelationship">
                    <option value="">Relationship</option>
                    <option value="Grand Father">Grand Father</option>
                    <option value="Grand Mother">Grand Mother</option>
                    <option value="Father">Father</option>
                    <option value="Mother">Mother</option>
                    <option value="Uncle">Uncle</option>
                    <option value="Aunt">Aunt</option>
                    <option value="Brother">Brother</option>
                    <option value="Sister">Sister</option>
                    <option value="Others">Others</option>
                    </select>
                </div>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Parent/guardian number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="parentSearch" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH/FILTER STAFFS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchStaffsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Staffs</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass" id="teacherSortClass">
                    <option value="">Select a class</option>
                    <option value="All">All Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                    <?php    }
                    ?>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Subjects</label>
                    <select class="form-control" name="sortSubject" id="teacherSortSubject">
                    </select>
                </div>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Staff number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="searchStaff" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH CLASS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchClassModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search for a class</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                <label for=""> Enter Class Name</label>
                    <select required class="form-control stafftimeTableclasses" name="searchClass">
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                        <?php }  ?>
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="searchClassBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- EDIT CLASS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="editNameModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Edit class name</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                    <label for=""> Select Class Name</label>
                    <select required class="form-control stafftimeTableclasses" name="formerName" id="formerName">
                        <option value="">Select Class</option>
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                        <?php }  ?>
                    </select>
                </div>
                <div class="form-group" id="newName" style="display : none">
                    <div class="form-group">
                        <label for=""> New Class Name</label><input name="newName" class="form-control" placeholder="E.g; <?php echo $classes[1]->class_name;?>" type="text">
                    </div>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button disabled id="editBtn" name="editClassName" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Edit name</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH SUBJECTS-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchSubjectsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search for a subject</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for=""> Select Subject Name</label>
                    <select class="form-control" name="searchSubject" style="width:100%">
                        <?php
                            for($i = 0; $i < count($allSubject); $i++){ ?>
                                <option value="<?php echo $allSubject[$i]->subjectCode; ?>"><?php echo ucfirst($allSubject[$i]->subjectName)." - ". ucwords($allSubject[$i]->subjectClass)?></option>
                        <?php    }
                        ?>
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="searchSubjectBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH STAFF ATTENDANCE-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchStaffAttendanceModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search for a Staff Attendance</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Enter Staff Name OR Number</label><input name="searchKeyword" class="form-control" placeholder="E.g; George Oweiye OR 2017/Stff/ElPraise/938" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH STUDENT ATTENDANCE-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchStudentAttendanceModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search for a Student Attendance</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Enter Student Name OR Number</label><input name="searchKeyword" class="form-control" placeholder="E.g; Mary Amadi OR 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/016" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH STUDENT CHILD PICKUP ENABLED-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchPickupEnabledModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search Student With Pickup Verification</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Student Class</label>
                <select class="form-control">
                <?php
                    if(count($classes) > 0){
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No classes found in record.</option> 
                    <?php } ?>
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Enter Student Name OR Number</label><input name="searchKeyword" class="form-control" placeholder="E.g; Mary Amadi OR 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/016" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH STUDENT CHILD PICKUP DISABLED-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchPickupDisabledModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search Student Without Pickup Verification</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Student Class</label>
                <select class="form-control">
                <?php
                    if(count($classes) > 0){
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No classes found in record.</option> 
                    <?php } ?>
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Enter Student Name OR Number</label><input name="searchKeyword" class="form-control" placeholder="E.g; Mary Amadi OR 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/016" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SINGLE REPORT -->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="singleReportModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <!-- <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Maven Harry's Attendance Report</h4> -->
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:center; margin-top:-40px; color:#8095A0">Currently undergoing upgrade to make it better. Please check back later</h4>

            <!-- <center>
                    <div class="el-chart-w" style="width:400px; margin-top:50px;">
                    <canvas height="120" id="donutChart" width="120"></canvas>
                    <div class="inside-donut-chart-label">
                        <strong style="font-size:4rem;">67%</strong><span style="font-size:1.2rem">Overall Attendance Summary</span>
                    </div>
                    </div>
                    <div class="el-legend row">
                    <div class="legend-value-w" style="margin-right:15px;">
                        <div class="legend-pin" style="background-color: #85c751;"></div>
                        <div class="legend-value" style="margin-left:-10px;">Present & Early</div>
                    </div>
                    <div class="legend-value-w" style="margin-right:15px;">
                        <div class="legend-pin" style="background-color: #f8bc34;"></div>
                        <div class="legend-value" style="margin-left:-10px;">Present & Late</div>
                    </div>
                    <div class="legend-value-w" style="margin-right:15px;">
                        <div class="legend-pin" style="background-color: #d97b70;"></div>
                        <div class="legend-value" style="margin-left:-10px;">Absent</div>
                    </div>
                    </div>
            </center> -->

           <!-- <form style="margin-top:40px;">
           <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> TIMELINE</label>
            <div class="col-sm-8">
              <select class="form-control">
                <option value="Today">Today</option>
                <option value="Yesterday">Yesterday</option>
                <option value="Last Week">Last Week</option>
                <option value="This Month">This Month</option>
                <option value="Last Month">Last Month</option>
                <option value="This Quarter">This Quarter</option>
                <option value="Last Quarter">Last Quarter</option>
                <option value="This Year">This Year</option>
                <option value="Last Year">Last Year</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> TODAY: </label>
            <div class="col-sm-8">
            <label class="col-form-label" for=""> PRESENT & EARLY </label>
            </div>
            </div>
            <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> PRESENT & EARLY: </label>
            <div class="col-sm-8">
            <label class="col-form-label" for=""> 15 DAYS </label>
            </div>
            </div>
            <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> PRESENT & LATE: </label>
            <div class="col-sm-8">
            <label class="col-form-label" for=""> 8 DAYS </label>
            </div>
            </div>
            <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> ABSENT: </label>
            <div class="col-sm-8">
            <label class="col-form-label" for=""> 7 DAYS </label>
            </div>
            </div>

            <?php
            //$pathArr = explode("/", $_SERVER['REQUEST_URI']);
            //$fileName = $pathArr[count($pathArr)-1];
            //if($fileName != "student#" && $fileName != "student"){ ?>

            <div class="form-group row">
            <label style="font-weight:normal" class="col-form-label col-sm-4" for=""> FEE DEDUCTED: </label>
            <div class="col-sm-8">
            <label class="col-form-label" for="">&#8358;350 (&#8358;50/Day)</label>
            </div>
            </div>

            <div class="form-buttons-w" style="margin-top:30px;">
            <button class="my_hover_up btn btn-primary create-school-btn" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Forward report to this staff</button>
            <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
            </div>

            <?php //}else{ ?>
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Forward report to student's parents</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            <?php //}
            ?>

        </form> -->

            </div>
        </div>
    </div>
</div>




<!-- TIMETABLE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="editTimetableModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="timetableTitle onboarding-title" style="font-size:1.3rem;font-size:1.2rem; text-align:right; margin-top:-40px; color:#8095A0"></h4>
              <form id="formValidate" method="post">

                <div class="form-group">
                    <label for="">Subject <span style="color:#F91F14">*</span></label>
                    <select required class="form-control" name="timeTableSubjects">
                    <?php
                    for($i = 0; $i < count($classSubject); $i++){ ?>
                        <option value="<?php echo $classSubject[$i]->subjectCode; ?>"><?php echo ucfirst($classSubject[$i]->subjectName) ?></option>
                    <?php    }
                    ?> 
                </select>
                </div>

                <input type="hidden" name="timetableModalClass" class="timetableModalClass" value="">
                <input type="hidden" name="timetableModalDay" class="timetableModalDay" value="">
                <input type="hidden" name="timetableModalTime" class="timetableModalTime" value="">
                <input type="hidden" name="timetableModalSession" class="" value="<?php echo isset($_GET["session"]) ? $_GET["session"] : "N/A" ?>">
                <input type="hidden" name="timetableModalTerm" class="" value="<?php echo isset($_GET["term"]) ? $_GET["term"] : "N/A" ?>">
                
                    <div class="form-group">
                    <label for="">Teacher/Tutor <span style="color:#F91F14">*</span></label>
                    <select required class="form-control timetableTutor" name="timetableTutor">
                    <?php
                    if(count($allStaffs) > 0){
                        for($i = 0; $i < count($allStaffs); $i++){ ?>
                            <option value="<?php echo $allStaffs[$i]->staff_number; ?>"><?php echo ucfirst($allStaffs[$i]->fullname) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No tutor found in record.</option> 
                    <?php } ?>
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="updateTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button name="deleteTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#FB5574; background-color:#FB5574"> Delete this</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>                    
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

 <!-- ADD TEACHER TO TIMETABLE -->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newTimetableModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="timetableTitle onboarding-title" style="font-size:1.3rem;font-size:1.2rem; text-align:right; margin-top:-40px; color:#8095A0"></h4>
              <form id="formValidate" method="post">
                
                <div class="form-group">
                    <label for="">Subject <span style="color:#F91F14">*</span></label>
                <select required class="form-control" name="timeTableSubjects">
                <?php
                    for($i = 0; $i < count($classSubject); $i++){ ?>
                        <option value="<?php echo $classSubject[$i]->subjectCode; ?>"><?php echo ucfirst($classSubject[$i]->subjectName) ?></option>
                <?php    }
                ?>    
                </select>
                </div>

                <input type="hidden" name="timetableModalClass" class="timetableModalClass" value="">
                <input type="hidden" name="timetableModalDay" class="timetableModalDay" value="">
                <input type="hidden" name="timetableModalTime" class="timetableModalTime" value="">
                <input type="hidden" name="timetableModalSession" class="" value="<?php echo isset($_GET["session"]) ? $_GET["session"] : "N/A" ?>">
                <input type="hidden" name="timetableModalTerm" class="" value="<?php echo isset($_GET["session"]) ? $_GET["term"] : "N/A" ?>">
                
                <div class="form-group">
                <label for="">Teacher/Tutor <span style="color:#F91F14">*</span></label>
                <select required class="form-control timetableTutor" name="timetableTutor">
                <?php
                if(count($allStaffs) > 0){
                    for($i = 0; $i < count($allStaffs); $i++){ ?>
                        <option value="<?php echo $allStaffs[$i]->staff_number; ?>"><?php echo ucwords($allStaffs[$i]->fullname) ?></option>
                <?php } }else{ ?>
                    <option disabled selected value="">No tutor found in record.</option> 
                <?php } ?>
                </select>
            </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="addTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Create this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>                    
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- # -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="editStaffTimetableModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="timetableTitle onboarding-title" style="font-size:1.3rem;font-size:1.2rem; text-align:right; margin-top:-40px; color:#8095A0"></h4>
              <form id="formValidate" method="post">

                <div class="form-group">
                    <label for="">Classes <span style="color:#F91F14">*</span></label>
                    <select required class="form-control stafftimeTableclasses" name="stafftimeTableclasses">
                    <?php
                    if(count($classes) > 0){
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No classes found in record.</option> 
                    <?php } ?>
                    </select>
                </div>

                <input type="hidden" name="timetableModalStaffCode" class="timetableModalStaffCode" value="">
                <input type="hidden" name="timetableModalDay" class="timetableModalDay" value="">
                <input type="hidden" name="timetableModalTime" class="timetableModalTime" value="">
                <input type="hidden" name="timetableModalSession" class="" value="<?php echo isset($_GET["session"]) ? $_GET["session"] : "N/A" ?>">
                <input type="hidden" name="timetableModalTerm" class="" value="<?php echo isset($_GET["term"]) ? $_GET["term"] : "N/A" ?>">
                
                    <div class="form-group">
                    <label for="">Subjects <span style="color:#F91F14">*</span></label>
                    <select class="form-control stafftimeTableSubjects" name="stafftimeTableSubjects">
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="updateStaffTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button name="deleteStaffTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#FB5574; background-color:#FB5574"> Delete this</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>                    
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newStaffTimetableModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="timetableTitle onboarding-title" style="font-size:1.3rem;font-size:1.2rem; text-align:right; margin-top:-40px; color:#8095A0"></h4>
              <form id="formValidate" method="post">

                <div class="form-group">
                    <label for="">Classes <span style="color:#F91F14">*</span></label>
                    <select required class="form-control stafftimeTableclasses" name="stafftimeTableclasses">
                    <?php
                    if(count($classes) > 0){
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo ucfirst($classes[$i]->class_name) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No classes found in record.</option> 
                    <?php } ?>
                    </select>
                </div>

                <input type="hidden" name="timetableModalStaffCode" class="timetableModalStaffCode" value="">
                <input type="hidden" name="timetableModalDay" class="timetableModalDay" value="">
                <input type="hidden" name="timetableModalTime" class="timetableModalTime" value="">
                <input type="hidden" name="timetableModalSession" class="" value="<?php echo isset($_GET["session"]) ? $_GET["session"] : "" ?>">
                <input type="hidden" name="timetableModalTerm" class="" value="<?php echo isset($_GET["term"]) ? $_GET["term"] : "N/A" ?>">
                
                    <div class="form-group">
                    <label for="">Subjects <span style="color:#F91F14">*</span></label>
                    <select required class="form-control stafftimeTableSubjects" name="stafftimeTableSubjects">
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="addStaffTimeTable" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Create this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>                    
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- Fee payment -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newFeeModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a new fee</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for=""> Fee Name</label><input name="feeName" required class="form-control" placeholder="E.g; Day care fee" type="text">
                </div>
                
                <div class="form-group">
                    <label for="">Which class(es) pays this fee?</label>
                    <select required class="form-control select2" name="classToPay[]" style="width:100%" multiple="true">
                    <option value="Applicant">Applicant</option>
                    <option value="Newly Enrolled">Newly Enrolled</option>
                    <option value="Graduating">Graduating</option>
                    <!-- CLASSES STARTS HERE -->
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for=""> Fee Amount</label><input name="feeAmount" class="form-control" placeholder="E.g; <?php echo $schoolDetails->currencySymbol ?>0.00" type="text">
                </div>
                <div class="form-group">
                    <label for="">Is this part of school fees?</label>
                    <select class="form-control" name="schoolFeePart" style="width:100%" required>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid fee.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <input type="submit" name="addFee" class="my_hover_up btn btn-primary" value="Create Fee & Save" style="border-color:#08ACF0; background-color:#08ACF0">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH/FILTER PICKUP PERSONS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchPickPerson" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter pickup persons</h4>
              <form id="formValidate">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Relationship</label>
                    <select class="form-control" name="sortRelationship">
                    <option value="Grand Father">Grand Father</option>
                    <option value="Grand Mother">Grand Mother</option>
                    <option value="Father">Father</option>
                    <option value="Mother">Mother</option>
                    <option value="Uncle">Uncle</option>
                    <option value="Aunt">Aunt</option>
                    <option value="Brother">Brother</option>
                    <option value="Sister">Sister</option>
                    <option value="Others">Others</option>
                    </select>
                </div>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Pickup person number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SEARCH/FILTER PICKED STUDENTS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchPickedStudents" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter picked students</h4>
              <form id="formValidate">

                <div class="form-group">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Student's number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SWITCH ASSESSMENTS SESSION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="assessmentSwitch" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Assessment Session & Term</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select class="form-control" id="assessmentAssignSession">
                <option value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                <option selected="true" value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                <option value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                <option value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                <option value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>                
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select class="form-control" id="assessmentAssignTerm">
                <option value="1">1st Term</option>
                <option value="2">2nd Term</option>
                <option value="3">3rd Term</option>
                </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button onclick="goto('home')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Go to assessments</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SWITCH TIMETABLE CLASSES -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="timetableClassSwitch" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Select Timetable Session & Class</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select class="form-control" id="timetableAssignSession">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>               
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select class="form-control" id="timetableAssignTerm">
                    <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                    <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                    <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option> 
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select class="form-control" id="timetableAssignClass">
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <button onclick="goto_timetable('class_timetable')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Go to class timetable</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SWITCH TIMETABLE STAFFS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="timetableStaffSwitch" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Select Timetable Session & Staff</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select class="form-control" id="timetableAssignSessionStaff">
                <option value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                <option selected="true" value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                <option value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                <option value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                <option value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>                
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select class="form-control" id="timetableAssignTermStaff">
                <option value="1">1st Term</option>
                <option value="2">2nd Term</option>
                <option value="3">3rd Term</option>
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Staff</label>
                <select class="form-control" id="timetableAssignStaff">
                <?php
                if(count($allStaffs) > 0){
                    for($i = 0; $i < count($allStaffs); $i++){ ?>
                        <option value="<?php echo $allStaffs[$i]->staff_number; ?>"><?php echo ucfirst($allStaffs[$i]->fullname) ?></option>
                <?php } }else{ ?>
                    <option disabled selected value="">No tutor found in record.</option> 
                <?php } ?>
                </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <button onclick="goto_timetable_staff('staff_timetable')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Go to staff timetable</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SWITCH PAYMENT PROCESSING -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="paymentSwitch" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Process a Student's Fee(s) Payment</h4>
              <form id="formValidate" method="POST" action="../../views/payments/process_payment">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select required name="session" class="form-control" id="paymentSession">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>               
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select required name="term" class="form-control" id="paymentTerm">
                    <option value="">Select Term</option>
                    <option value="1">1st Term</option>
                    <option value="2">2nd Term</option>
                    <option value="3">3rd Term</option> 
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select required class="form-control" id="paymentClass">
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-group">
                    <label for="">Select Student</label>
                    <select name="student_id" required class="form-control" id="paymentStudent" style="width:100%">
                        <option value="">Select Student</option>
                    </select>
                </div>


                <!-- <div class="form-group">
                    <label for="">Select Fee(s)</label>
                    <select name="fee" required class="form-control" id="paymentFee" style="width:100%">
                        <option>Select Fee</option>
                    </select>
                </div> -->

                <div class="form-group">
                    <label for="">Select Fee(s)</label>
                    <select required class="form-control select2" id="paymentFee" name="fee[]" style="width:100%" multiple="true">
                        <!-- STUDENTS FEE -->
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Proceed to payment">
                    <!-- <button onclick="goto_payment('../../views/payments/process_payment.php')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Proceed to payment</button> -->
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SWITCH PAYMENT PROCESSING BULK-->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="paymentSwitchBulk" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Process school fee bulk Payment</h4>
              <form id="formValidate" method="POST" action="process_bulk_payment">
                        
                <div class="form-group">
                    <label for=""> Select Session</label>
                    <select name="session" class="form-control" id="paymentSession">
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>               
                    </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                    <label for=""> Select Term</label>
                    <select name="term" class="form-control" id="paymentTerm">
                        <option value="">Select Term</option>
                        <option value="1">1st Term</option>
                        <option value="2">2nd Term</option>
                        <option value="3">3rd Term</option> 
                    </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                    <label for=""> Select Fee</label>
                    <select name="feeName" class="form-control" id="paymentTerm">
                        <?php
                            if(count($distinctFee) == 0){ ?>
                                <option value="">No fee added</option>
                            <?php }else{ 
                                foreach($distinctFee as $key => $value){ ?>
                                <option value="<?php echo $value; ?>"><?php echo ucwords($value)?></option>
                            <?php }
                            }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for=""> Select Family</label>
                    <select required name="famId" class="form-control" id="familyId">
                            <?php
                                if(count($families) == 0){ ?>
                                    <option required value="">No family added</option>
                                <?php }else{ ?>
                                    <option required value="">Select family</option>
                                    <?php foreach($families as $key => $value){ ?>
                                        <option value="<?php echo $value->id ?>"><?php echo $value->fullname ?></option>
                                        <?php }
                                }
                            ?>              
                    </select>
                </div>
                <div id="familyStudent"></div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Proceed to payment">
                    <!-- <button onclick="goto_payment('../../views/payments/process_payment.php')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Proceed to payment</button> -->
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- OTHER PAYMENT PROCESSING -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="selectPaymentMethod" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Select Payment Option</h4>
              <form id="formValidate" method="GET" action="../../views/payments/process_payment.php">

                <div class="form-group" style="margin-top:20px;">
                    <label for=""> Select Payment Method</label>
                    <select name="paymentMethod" class="form-control" id="paymentMethod">
                        <option value="">Select Payment Method</option>
                        <option value="offlinebank">Paid at bank</option>
                        <option value="pos">Paid paid POS</option>
                    </select>
                </div>

                <!-- <div class="form-group">
                    <label for=""> Bank Name</label>
                    <select class="form-control" name="bankName">
                        <?php
                        //CHECK IF COUNTRY IS NIGERIA, KENYA OR GHANA
                        $countries = array("nigeria", "kenya", "ghana");
                        $country = strtolower("nigeria");
                        if(in_array($country, $countries)){
                        $json = file_get_contents('../../includes/'.$country.'_banks.json');
                        $json_data = json_decode($json,true);
                        foreach ($json_data as $key => $value) { ?>
                            <option <?php echo $schoolDetails->bankName == $json_data[$key]["name"] ? "selected" : "";?> value="<?php echo $json_data[$key]["name"]?>"><?php echo strtoupper($json_data[$key]["name"]) ?></option>
                        <?php }
                        }else{ ?>
                            <option value="">Your country is not available yet!</option>
                        <?php }?>
                    </select>
                </div> -->

                
                <div class="form-group">
                    <label for="">Receipt No.</label>
                    <input name="receiptNo" id="receiptNo" style="height: 40px; font-size : 20px;" class="form-control" type="text" value="" placeholder="Receipt No.">
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input id="selectedMethod" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0; color: #ffffff; width:200px" value="Save">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- CASH RECEIPT NO -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="cashPaymentReceiptNoModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Payment Receipt No.</h4>
              <form id="formValidate" method="" action="">
                
                <div class="form-group">
                    <label for="">Receipt No.</label>
                    <input name="receiptNo" id="cashReceiptNo" style="height: 40px; font-size : 20px;" class="form-control" readonly type="text" value="<?php echo time(); ?>" placeholder="Receipt No.">
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input id="cashReceiptBtn" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0; color: #ffffff; width:200px" value="Save">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- BULK PAYMENT PROCESSING -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="bulkPayment" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Process bulk fee payment</h4>
              <form id="formValidate" method="GET" action="../../views/payments/process_payment.php">

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Student Family Tree</label>
                <select name="term" class="form-control" id="paymentTerm">
                    <option value="">Select Family</option>
                    <option value="1">Bukola Saraki Family</option> 
                </select>
                </div>

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Student Name</label>
                    <input name="name" class="form-control" type="text" value="Mike Owemuni" disabled>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Amount</label>
                    <input name="amount" class="form-control" type="text" placeholder="&#x20A6;0">
                    </select>
                </div>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Process bulk payment">
                    <!-- <button onclick="goto_payment('../../views/payments/process_payment.php')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Proceed to payment</button> -->
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- NEW FAMILY -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newFamilyModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Create a new family</h4>
              <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="form-group">
                  <label for=""> Family Name</label><input name="familyName" placeholder="John Amadi" class="form-control" value="" type="text">
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Father's Phone</label>
                        <input name="studentFatherContact" id="studentFatherContact" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Mother's Phone</label>
                        <input name="studentMotherContact" id="studentMotherContact" class="form-control" placeholder="E.g; +2348746389437" type="text">
                     </div>
                  </div>
               </div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input id="familyCheck" class="form-check-input" type="checkbox">I hereby verify that this is a family.</label></div>
                <div class="form-buttons-w" style="margin-top:15px; ">
                    <input type="submit" value=" Save This Family" id="createFamily" name="createFamily" class="my_hover_up btn btn-primary">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- IDCARD SELECTOR -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="idcardSelector" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">View Student School ID</h4>
              <form id="formValidate" method="GET" action="./idcard">

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select required class="form-control" id="idClassSelector">
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-group">
                    <label for="">Select Student</label>
                    <select name="student_id" required class="form-control" id="idClassStudent" style="width:100%">
                        <option value="">No Class Selected</option>
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="View School ID Card">
                    <!-- <button onclick="goto_payment('../../views/payments/process_payment.php')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Proceed to payment</button> -->
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- IDCARD SELECTOR -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="staffIdcardSelector" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">View Staff School ID</h4>
              <form id="formValidate" method="GET" action="./idcard">


                <div class="form-group">
                    <label for="">Select Staff</label>
                    <select name="staff_id" required class="form-control" style="width:100%">
                    <?php
                    if(count($allStaffs) > 0){
                        for($i = 0; $i < count($allStaffs); $i++){ ?>
                            <option value="<?php echo base64_encode($allStaffs[$i]->staff_number) ?>"><?php echo ucfirst($allStaffs[$i]->fullname) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No tutor found in record.</option> 
                    <?php } ?>
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="View Staff ID Card">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- ADMISSION STATUS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="admissionStatusModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Check Admission Status</h4>
              <form action="../../views/admission/status" method="POST">
                <div class="form-group">
                <label for=""> Student Application Number</label>
                <input required id="applyNumber" name="applyNumber" class="form-control" placeholder="E.g; Appl/2018/<?php echo $schoolDetails->domain_name?>/014" type="text">
                </div>
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="applyNumberBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Check status now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- SEARCH/FILTER EXAM SCORES -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchStudentsExamModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Examination Scores</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="">Session</label>
                        <select class="form-control" name="examinationSession">
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="">Term</label>
                        <select class="form-control" name="examinationTerm">
                        <option value="1">1st Term</option>
                        <option value="2">2nd Term</option>
                        <option value="3">3rd Term</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">Class</label>
                        <select class="form-control" name="examClass" id="examinationClassModal">
                        <option value="All">All Classes</option>
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                        <?php    }
                        ?>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="">Subject</label>
                        <select class="form-control" name="examinationSubject" id="examinationSubjectModal">
                        <option value="">Select Class</option>
                                
                        </select>
                    </div>

                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Student number or name" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- SEARCH/FILTER EXAM QUESTIONS AND ANSWERS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="searchQuestionsAnswersModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Questions/Answers</h4>
              <form id="formValidate">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Subject</label>
                    <select class="form-control" name="sortFee">
                    <option value="All">All Subjects</option>                    
                    <?php
                    if(count($subjects_distinct) > 0){
                    for($i = 0; $i < count($subjects_distinct); $i++){ ?>
                        <option value="<?php echo $subjects_distinct[$i]->subjectCode; ?>"><?php echo ucfirst($subjects_distinct[$i]->subjectName) ?></option>
                    <?php } }else{ ?>
                        <option disabled selected value="">No subject found in record.</option> 
                    <?php } ?>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">Class</label>
                    <select class="form-control" name="sortClass">
                    <option value="All">All Classes</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Session</label>
                    <select class="form-control" name="sortSession">
                        <option value="All">All Sessions</option>
                        <option value="<?php echo date('Y', strtotime('-2 year')) ?>"><?php echo date('Y', strtotime('-3 year')) ?>/<?php echo date('Y', strtotime('-2 year')) ?></option>
                        <option value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                        <option selected="true" value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                        <option value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y', strtotime('+0 year')) ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="">Term</label>
                    <select class="form-control" name="sortTerm">
                    <option value="All">All Terms</option>
                    <option value="1">1st Term</option>
                    <option value="2">2nd Term</option>
                    <option value="3">3rd Term</option>
                    </select>
                </div>

                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Enter Search Keyword</label><input name="searchKeyword" class="form-control" placeholder="Enter question or answer" type="text">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>




<!-- SET NEW EXAMINATION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="setupNewExam" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Setup/Configure New Examination</h4>
              <form id="formValidate" method="post">

                <div class="form-group">
                <label for=""> Select Class</label>
                <select class="form-control" name="examClass" id="examClass" required>
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Subject</label>
                <select class="form-control" name="examSubject" id="examSubject" required>
                    <option>Select class</option>
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Start time</label>
                    <input class="form-control" placeholder="8:00 AM" name="startTime" id="startTime" required>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Duration</label>
                <select required class="form-control" name="examDuration">
                    <option value="900">15 Minutes</option>
                    <option value="1800">30 Minutes</option>
                    <option value="2700">45 Minutes</option>
                    <option value="3600">1 Hour</option>
                    <option value="4500">1 Hour, 15 Minutes</option>
                    <option value="5400">1 Hour, 30 Minutes</option>
                    <option value="6300">1 Hour, 45 Minutes</option>
                    <option value="7200">2 Hours</option>
                    <option value="8100">2 Hours, 15 Minutes</option>
                    <option value="9000">2 Hours, 30 Minutes</option>
                    <option value="9900">2 Hours, 45 Minutes</option>
                    <option value="10800">3 Hours</option>
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Supervisor</label>
                <select required class="form-control" name="examSupervisor" id="examSupervisor">
                <?php
                if(count($allStaffs) > 0){
                    for($i = 0; $i < count($allStaffs); $i++){ ?>
                        <option value="<?php echo $allStaffs[$i]->staff_number; ?>"><?php echo ucfirst($allStaffs[$i]->fullname) ?></option>
                <?php } }else{ ?>
                    <option disabled selected value="">No tutor found in record.</option> 
                <?php } ?>
                </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="setupExam" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Setup This Exam</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- INVENTORY ITEM COLLECTION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="processItemCollection" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Process New Item Collection</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for="">Item Name</label>
                    <select class="form-control" name="itemName" id="itemName">
                        <option value="">Select Item</option>
                        <?php
                            foreach($items as $key => $value){ ?>
                                <option><?php echo ucwords($value->itemName); ?></option>
                            <?php }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Item Size</label>
                    <select class="form-control" name="itemSize" id="itemSize">
                        <option value="">Select Item</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="">Item Color</label>
                    <select class="form-control" name="itemColor" id="itemColor">
                        <option value="">Select Item</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Item Quantity</label>
                        <input name="itemQuantity" class="form-control" type="number" placeholder="0">
                    </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Item Collected By</label>
                    <input name="CollectedBy" class="form-control" type="text" placeholder="E.g; Patrick Owunale">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="collectBtn" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Process This Collection</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- ADD NEW INVENTORY ITEM -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="addNewItem" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a New Inventory Item</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for="">Item Name</label>
                    <input name="ItemName" class="form-control" type="text" placeholder="E.g; Uniform">
                </div>

                <div class="form-group">
                    <label for="">Item Size</label>
                    <input name="ItemSize" class="form-control" type="text" placeholder="E.g; Small">
                </div>
                
                <div class="form-group">
                    <label for="">Item Color</label>
                    <input name="ItemColor" class="form-control" type="text" placeholder="E.g; Blue">
                </div>

                <div class="form-group">
                    <label for="">Item Quantity</label>
                        <input name="ItemQuantity" class="form-control" type="number" placeholder="0">
                </div>

                <div class="form-group">
                    <label for="">Item Price</label>
                        <input name="ItemPrice" class="form-control" type="number" placeholder="&#8358;0">
                </div>

                <div class="form-group">
                    <label for="">Item Location</label>
                        <input name="ItemLocation" class="form-control" type="text" placeholder="E.g; Store">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="addNewStock" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Add This New Item</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- INVENTORY CONFIGURATION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="inventoryConfiguration" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Inventory Configuration/Settings</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for="">Low stock starts at which quantity</label>
                    <input name="lowStockCount" class="form-control" type="number" placeholder="0">
                </div>

                <div class="form-group">
                    <label for="">Out of stock starts at which quantity</label>
                    <input name="outStockCount" class="form-control" type="number" placeholder="0">
                </div>
    
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="inventoryConfiguration" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Save Inventory Settings</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- PURCHASE SMS UNITS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="smsPurchaseModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Purchase SMS Units</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                <label for=""> SMS amount to purchase</label>
                <input id="smsAmount" required name="smsAmount" class="form-control" placeholder="E.g; &#x20A6;5000" type="number">
                </div>
                <input id="smsEmail" type="hidden" value="<?php echo $schoolDetails->email ?>">
                <input id="smsSchoolName" type="hidden" value="<?php echo $schoolDetails->school_name ?>">

                <div class="form-group">
                <label for=""> You are getting:</label>
                <h4 class="onboarding-title" style="font-size:1.3rem; color:#8095A0"><span id="equivSMS">0</span> Units @ &#x20A6;3/SMS</h4>                
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button id="buySmsBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Purchase this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- ADD NEW INVENTORY ITEM -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="addNewProduct" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add a New Tuckshop Product</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                    <label for="">Product Name</label>
                    <input name="ItemName" class="form-control" type="text" placeholder="E.g; Malta Biscuit">
                </div>

                <div class="form-group">
                    <label for="">Product Quantity</label>
                        <input name="ItemQuantity" class="form-control" type="number" placeholder="0">
                </div>

                <div class="form-group">
                    <label for="">Price Per Product</label>
                        <input name="ItemPrice" class="form-control" type="number" placeholder="&#8358;0">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="addProduct" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Add This New Product</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>




<!-- CHECK WALLET BALANCE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="checkWalletBalance" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Check Tuckshop Wallet Balance</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                <label for=""> Enter Student Number</label>
                <input name="studentNumber" id="walletOwner" class="form-control" placeholder="E.g; 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/001" type="text">
                </div>
                
                <div id="walletResult"></div>
                

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="" id="checkBalance" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Check wallet balance</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- RECHARGE BALANCE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="rechargeWallet" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Recharge Tuckshop Wallet</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                <label for=""> Enter Student Number</label>
                <input name="studentNumber" id="rechargeStudentNumber" class="form-control" placeholder="E.g; 2018/Stdn/<?php echo ucfirst($modalClass->getSchoolDetails()->domain_name) ?>/001" type="text">
                </div>

                <div class="form-group">
                <label for=""> Amount to recharge</label>
                <input name="rechargeAmount" id="rechargeAmount" class="form-control" placeholder="E.g; &#x20A6;3000" type="number">
                </div>
                <div id="studentName"></div>
                <!-- <div style="margin-top:20px;" class="form-group">
                <label for=""> Actual Recharge Amount:</label>
                <h4 class="onboarding-title" style="font-size:1.3rem; color:#8095A0"> &#x20A6;<span>5000 @&#x20A6;120 (Transaction fee)</span></h4>                
                </div> -->

                <div class="form-check"><label class="form-check-label"><input id="rechargeCheck" class="form-check-input" type="checkbox">Verify details.</label></div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="searchClassBtn" disabled class="my_hover_up btn btn-primary" id="rechargeBtn" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> Recharge this wallet</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- INVENTORY CONFIGURATION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="tuckshopConfiguration" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Tuckshop Configuration/Settings</h4>
              <form id="formValidate" method="POST">

                <div class="form-group row">
                <label class="col-sm-9 col-form-label" for=""> Auto alert parents for low wallet balance</label>
                <div class="col-sm-2">
                <div class="c-toggle-btn">
                    <input name="alertWalletBalance" type="checkbox">
                    <div>
                        <label class="on">On</label>
                        <label class="off">Off</label>
                        <span class="c-toggle-thumb"></span>
                    </div>
                    </div>
                </div>
                </div>  

                <div class="form-group">
                    <label for="">Alert when balance is less than</label>
                    <input name="amountToAlert" class="form-control" type="number" placeholder="&#x20A6;1000">
                </div>

                <div class="form-group">
                    <label for=""> Tuckshop Bank Name</label>
                    <select class="form-control" name="bankName">
                    <?php
                        //CHECK IF COUNTRY IS NIGERIA, KENYA OR GHANA
                        $countries = array("nigeria", "kenya", "ghana");
                        $country = strtolower("nigeria");
                        if(in_array($country, $countries)){
                        $json = file_get_contents('../../includes/'.$country.'_banks.json');
                        $json_data = json_decode($json,true);
                        foreach ($json_data as $key => $value) { ?>
                            <option <?php echo $schoolDetails->bankName == $json_data[$key]["name"] ? "selected" : "";?> value="<?php echo $json_data[$key]["name"]?>"><?php echo strtoupper($json_data[$key]["name"]) ?></option>
                        <?php }
                        }else{ ?>
                            <option value="">Your country is not available yet!</option>
                        <?php }?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Tuckshop Bank Account Name</label>
                    <input name="accountName" class="form-control" type="text" placeholder="Bank Account Name">
                </div>

                <div class="form-group">
                    <label for="">Tuckshop Bank Account Number</label>
                    <input name="accountNumber" class="form-control" type="text" placeholder="Bank Account Number">
                </div>
    
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button class="my_hover_up btn btn-primary" name="tuckshopConfiguration" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Save Tuckshop Settings</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- RECEIPT MERGER PRINT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="allReceipts" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Print All Payment Receipts in 1</h4>
              <form id="formValidate" method="Post">

                
                <div class="form-group">
                    <label for="">Class</label>
                    <select required id="sortSlipClassReceipt" class="form-control" name="assignClass">
                        <option value="">Select Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for=""> Student Name</label>
                    <select required class="sortSlipStudent form-control" name="assignStudent" id="receiptStudent">
                        <option value="">Select Student</option>
                    </select>
                </div>
                
                <div class="form-group">
                <label for=""> Select Fee</label>
                <select name="feeName" class="form-control" id="receiptFee" required>
                    <?php
                        if(count($distinctFee) == 0){ ?>
                            <option value="">No fee added</option>
                        <?php }else{ 
                            foreach($distinctFee as $key => $value){ ?>
                            <option value="all">All Fees</option>
                            <option value="<?php echo $value; ?>"><?php echo ucwords($value)?></option>
                        <?php }
                        }
                    ?>
                </select>
                </div>

                <div class="form-group">
                    <label for="">Select Academic Year</label>
                    <select required class="form-control" name="academicSession" id="receiptSession">
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Select Academic Term</label>
                    <select class="form-control" name="academicTerm" id="receiptTerm">
                        <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                        <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                        <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option> 
                    </select>
                </div>
            
                

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="" onclick="goto_receipts('view_receipt')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> View all reciepts</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- Promote Class -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="promoteClass" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Promote Students of a class</h4>
              <form id="formValidate" method="Post">
                
                <div class="form-group">
                    <label for="">Promote From</label>
                    <select required class="form-control" name="fromClass">
                        <option value="">Select From Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Promote To</label>
                    <select required class="form-control" name="toClass">
                        <option value="">Select To Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php }
                    ?>
                    </select>
                </div>

                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="promoteButton" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Promote Students Now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- Promote Class -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="promoteStudent" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Promote This Student to a Class</h4>
              <form id="formValidate" method="Post">

                <div class="form-group">
                    <label for="">Promote To</label>
                    <select required class="form-control" name="studentToClass">
                        <option value="">Select To Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php }
                    ?>
                    </select>
                </div>

                
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="promoteSingleButton" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Promote This Student Now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SUBJECT EXAM SCORE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="subjectExamScoreModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">View Exam Scores</h4>
              <form id="formValidate" method="POST" action="../../views/examination/exam_scores">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select name="examinationSession" class="form-control" id="examinationSession">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>              
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select name="examinationTerm" class="form-control" id="examinationTerm">
                    <option value="">Select Term</option>
                    <option value="1">1st Term</option>
                    <option value="2">2nd Term</option>
                    <option value="3">3rd Term</option> 
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select class="form-control" name="examClass" id="examinationClass">
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-group">
                    <label for="">Select Subject</label>
                    <select name="examinationSubject" required class="form-control" id="examinationSubject" style="width:100%">
                        <option value="">Select Class</option>
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="View Scores">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- VIEW EXAM QUESTIONS -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="subjectExamQuestionsModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">View Exam Questions</h4>
              <form id="formValidate" method="GET" action="../../views/examination/exam_questions_answers">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select name="esn" class="form-control" id="examinationSession">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>              
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select name="etm" class="form-control" id="examinationTerm">
                    <option value="">Select Term</option>
                    <option value="1">1st Term</option>
                    <option value="2">2nd Term</option>
                    <option value="3">3rd Term</option> 
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select class="form-control" name="eqc" id="examinationQuestionClass">
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-group">
                    <label for="">Select Subject</label>
                    <select name="eqs" required class="form-control" id="examinationQuestionSubject" style="width:100%">
                        <option value="">Select Class</option>
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="View Questions">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>



<!-- ADD NEW QUESTION -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newQuestionModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Add new question</h4>
              <form id="formValidate" method="GET" action="../../views/examination/type_question">

                <div class="form-group" style="margin-top:20px;">
                    <label for=""> Select Class</label>
                    <select class="form-control" required name="questionClass" id="questionClass">
                        <option value="">Select Class</option>
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                    </select>
                </div>


                <div class="form-group">
                    <label for="">Select Subject</label>
                    <select name="questionSubject" required class="form-control" id="questionSubject" style="width:100%">
                        <option value="">Select Class</option>
                    </select>
                </div>


                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value=" Proceed ">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- CLASS GRADING SYSTEM -->


<div aria-hidden="true" class="onboarding-modal modal fade animated" id="gradingSystemModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Grading System</h4>
             <form method="POST" id="formValidate" action="#">
             <div class="form-group">
                <label for="">Select Class(es)</label>
                <select class="form-control select2" name="gradeClasses[]" style="width:100%" multiple="true">
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php } ?>
                </select>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> From</label>
                        <div class="">
                            <input name="from" class="form-control" placeholder="E.g, 80" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> To</label>
                        <div class="">
                            <input name="to" class="form-control" placeholder="E.g, 100" type="text">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Grade <span style="color:#F91F14">*</span></label>
                        <select class="form-control timetableTime" name="grade">
                            <?php
                                foreach($allGrades as $key => $value){ ?>
                                    <option value="<?php echo $value; ?>"><?php echo ucwords($value); ?></option>
                                <?php }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Remark</label>
                        <div class="">
                            <input name="remark" class="form-control" placeholder="E.g, Excellent" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid grade.</label></div>
            <div class="form-buttons-w" style="margin-top:15px;">
                <button name="saveGrading" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Save Grading</button>
                <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
            </div>
             </form>
            </div>
        </div>
    </div>
</div>

<!-- PUBLISH RESULT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="publishResultModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Publish Result</h4>
              <form id="formValidate" class="publishModal" method="POST">
                <div class="form-group">
                    <label for="">Select Class(es)</label>
                    <select class="form-control select2" id="publishClasses" name="publishClasses[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Pin Vending</label>
                            <div class="">
                                <select name="pinVending" id="pinVending" class="form-control" style="width:100%">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Amount</label>
                            <div class="">
                                <input name="amount" value="" id="pinAmount" class="form-control" placeholder="E.g, 100" type="number">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify the publishing of this result.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="publishResult" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Publish Result</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- CLASS SHEET -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="classSheet" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Class Report Session & Term</h4>
              <form id="formValidate">

                <div class="form-group">
                <label for="">Select Class</label>
                <select class="form-control" id="classAssessmentAssignClass">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php    }
                    ?>
                </select>
                </div>

                <div class="form-group">
                <label for=""> Select Academic Session</label>
                <select class="form-control" id="classAssessmentAssignSession">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Academic Term</label>
                <select class="form-control" id="classAssessmentAssignTerm">
                    <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                    <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                    <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option> 
                </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button onclick="goto_classsheet('classsheet')" class="my_hover_up btn btn-primary" type="button" style="border-color:#08ACF0; background-color:#08ACF0"> View Class Assessments Report</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SEARCH/FILTER MESSAGES -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="messageSearchModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Messages</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Start Date</label>
                    <div class="date-input">
                        <input required  name="startDate" class="date_of_birth form-control" placeholder="Start Date" type="text">
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="">End Date</label>
                    <div class="date-input">
                        <input required  name="startDate" class="date_of_birth form-control" placeholder="End Date" type="text">
                    </div>
                </div>
                </div>

                <h5 style="color:#8095A0; margin-left:10px; margin-bottom:15px;">OR</h4>

                <div class="form-group">
                <label for=""> Search By Audience</label>
                <select class="form-control" name="sortGender">
                    <option value="">Select audience</option>
                    <option value="teachers">Staffs</option>
                    <option value="students">Students</option>
                    <option value="parents">Parents</option>
                    <option value="Specify">Specify</option>
                </select>
                </div>
    
                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="studentSort" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Search this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<!-- NEW ASSESSMENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newAssessmentModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Set Assessment Scores</h4>
              <form id="formValidate" method="POST">
                <div class="form-group">
                    <label for="">Select Class(es)</label>
                    <select class="form-control select2" name="assessmentClasses[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Assessment Name</label>
                            <div class="">
                                <input name="assessmentName[]" class="form-control" placeholder="E.g, 1st Assessment" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Mark</label>
                            <div class="">
                                <input name="assessmentMark[]" class="form-control" placeholder="%. E.g 10" type="number">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="lastInput"></div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-12" for="" id="addAssessment" style="text-align:center; margin-top:15px; color:#08ACF0; text-decoration:underline; cursor:pointer"> CLICK HERE TO ADD NEW ASSESSMENT</label>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid assessment.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="submitAssessment" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Save Assessment</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- NEW MID TERM SCORE -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newMidTermModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Set Mid Term Scores</h4>
              <form id="formValidate" method="POST">
              <div class="form-group">
                    <label for="">Forms Part Of Which Assessment</label>
                    <select class="form-control" name="partOf" style="width:100%">
                    <?php
                        for($i = 0; $i < count($distinctAssessment); $i++){ ?>
                            <option value="<?php echo $distinctAssessment[$i]->assId; ?>"><?php echo $distinctAssessment[$i]->assName?></option>
                    <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Select Class(es)</label>
                    <select class="form-control select2" name="assessmentClasses[]" style="width:100%" multiple="true">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                    <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Assessment Name</label>
                            <div class="">
                                <input name="assessmentName[]" class="form-control" placeholder="E.g, 1st Assessment" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Mark</label>
                            <div class="">
                                <input name="assessmentMark[]" class="form-control" placeholder="%. E.g 10" type="number">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="lastMidTermInput"></div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-12" for="" id="addMidTerm" style="text-align:center; margin-top:15px; color:#08ACF0; text-decoration:underline; cursor:pointer"> CLICK HERE TO ADD NEW ASSESSMENT</label>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid assessment.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="submitMidTerm" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Save Assessment</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SELECT ASSESSMENT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="assessmentSelect" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Select Assessment</h4>
              <form id="formValidate" method="GET" action="../../views/assessment/staff_assessment">
              
                <div class="form-group">
                    <label for=""> Select Session</label>
                    <select required name="session" class="form-control">
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                        <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>               
                    </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                    <label for=""> Select Term</label>
                    <select required name="term" class="form-control">
                        <option value="">Select Term</option>
                        <option value="1">1st Term</option>
                        <option value="2">2nd Term</option>
                        <option value="3">3rd Term</option> 
                    </select>
                </div>

              <div class="form-group">
                    <label for="">Which assessment do you want to record? </label>
                    <select class="form-control" name="part" style="width:100%">
                            <?php
                                if($midTermState === true){ ?>
                                    <option value="1">Mid Term</option>
                                <?php }
                            ?>
                            <option value="0">Full Term</option>
                    </select>
                </div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Record Assessment</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- STAMP UPLOAD -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="stampUploadModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Upload School Stamp Image</h4>
              <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="">Select Class(es)</label>
                    <select class="form-control select2" name="stampClasses[]" style="width:100%" multiple="true">
                        <?php
                            for($i = 0; $i < count($classes); $i++){ ?>
                                <option selected value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="row form-group col-sm-12" style="margin-top:40px; margin-bottom:20px;">
                    <center>
                        <div class="col-sm-12">
                            <label style="cursor:pointer; margin-top:5px; border-color:#FC9933; background-color:#FC9933" class="my_hover_up btn btn-primary" for="stampLogo-photo"> School Stamp Picture from Computer</label>
                            <input name="stampLogo" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1;" class="form-control" id="stampLogo-photo" type="file">                    
                        </div>
                    </center>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I hereby verify that this is a valid stamp.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="schoolStampBtn" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Upload Stamp Now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- SWITCH PRINTING RESULT -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="switchResultPrint" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Print a Student's Result</h4>
              <form id="formValidate" method="GET" action="../../views/assessment/report_sheet">

                <div class="form-group">
                <label for=""> Select Session</label>
                <select required name="session" class="form-control">
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>               
                </select>
                </div>

                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Term</label>
                <select required name="term" class="form-control">
                    <option value="">Select Term</option>
                    <option value="1">1st Term</option>
                    <option value="2">2nd Term</option>
                    <option value="3">3rd Term</option> 
                </select>
                </div>


                <div class="form-group" style="margin-top:20px;">
                <label for=""> Select Class</label>
                <select required class="form-control" id="printClass">
                    <option value="">Select Class</option>
                <?php
                    for($i = 0; $i < count($classes); $i++){ ?>
                        <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                <?php    }
                ?>
                </select>
                </div>


                <div class="form-group">
                    <label for="">Select Student</label>
                    <select name="student_id" required class="form-control" id="printStudent" style="width:100%">
                        <option value="">Select Student</option>
                    </select>
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <input type="submit" class="my_hover_up btn btn-primary" style="border-color:#08ACF0; background-color:#08ACF0" value="Proceed to result">
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- GENERATE PIN -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="generatePin" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Search/Filter Students</h4>
              <form id="formValidate" method="POST">

                <div class="form-group">
                <label for=""> Number of pin to be generated</label><input name="genPinNumber" class="form-control" placeholder="Total number of pin" type="number">
                </div>

                <div class="form-buttons-w" style="margin-top:30px;">
                    <button name="genPin" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Generate pins now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>