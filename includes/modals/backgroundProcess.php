<?php
session_start();
// COLLECT PARAMETERS
$params = $_SERVER["argv"];

// GET ACTION.....1st Parameter
$action = str_replace("'", "", $params[1]);

if($action == "addNewStaff"){
    $_SESSION["folderName"] = str_replace("'", "", $params[5]);
    include_once ("modalClass.php");
    $modalClass = new modal;

    $staffNumber = str_replace("'", "", $params[2]);
    $staffSubjects = explode("*", str_replace("'", "", $params[3]));
    $password = str_replace("'", "", $params[4]);
    $reply = $modalClass->backgroundProcessAddStaff($staffNumber, $password, $staffSubjects);
}

if($action == "addNewSubject"){
    $_SESSION["folderName"] = str_replace("'", "", $params[3]);
    include_once ("modalClass.php");
    $modalClass = new modal;

    $subjectCodeArray = explode("*", str_replace("'", "", $params[2]));
    $reply = $modalClass->backgroundProcessUpdateAssessmentNewSubject($subjectCodeArray);
}

if($action == "addNewStudent"){
    $_SESSION["folderName"] = str_replace("'", "", $params[4]);
    include_once ("modalClass.php");
    $modalClass = new modal;

    $studentNumber = str_replace("'", "", $params[2]);
    $class = str_replace ("*", " ", str_replace("'", "", $params[3]));
    $reply = $modalClass->backgroundProcessCreateAssessmentNewStudent($studentNumber, $class);
}

if($action == "updateTimetable"){
    $_SESSION["folderName"] = str_replace("'", "", $params[5]);
    include_once ("modalClass.php");
    $modalClass = new modal;

    $timetableTutor = str_replace("'", "", $params[2]);
    $timeTableSubject = str_replace("'", "", $params[3]);
    $timetableClass = str_replace("'", "", $params[4]);

    $reply = $modalClass->backgroundProcessUpdateTutorAssessment($timetableTutor, $timeTableSubject, $timetableClass);
}
?>