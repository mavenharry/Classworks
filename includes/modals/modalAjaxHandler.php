<?php
    include_once ("modalClass.php");
    $logic = new modal();

    if(isset($_GET["className"])){
        $reply = $logic->getClassSubject($_GET["className"]);
        foreach ($reply as $key => $value) { ?>
            <option value="<?php echo $value->subjectCode; ?>"><?php echo $value->subjectName; ?></option>
    <?php    }
    }

    if(isset($_GET["studentMotherContact"])){
        $phone = htmlentities(trim($_GET["studentMotherContact"]));
        $reply = $logic->getPhoneNumberName($phone, "parents", "phone", "fullname");
        echo $reply;
    }

    if(isset($_GET["pickupStudentNumber"])){
        $studentNumber = htmlentities(trim($_GET["pickupStudentNumber"]));
        $reply = $logic->getPhoneNumberName($studentNumber, "students", "student_number", "fullname");
        echo $reply;
    }

    if(isset($_GET["studentFatherContact"])){
        $phone = htmlentities(trim($_GET["studentFatherContact"]));
        $reply = $logic->getPhoneNumberName($phone, "parents", "phone", "fullname");
        echo $reply;
    }

    if(isset($_GET["parentPhone"])){
        $phone = htmlentities(trim($_GET["parentPhone"]));
        $reply = $logic->getPhoneNumberName($phone, "students", ["father_phone", "mother_phone"], "fullname");
        echo $reply;
    }

    if(isset($_GET["timetableClass"])){
        $timetableClass = htmlentities(trim($_GET["timetableClass"]));
        $reply = $logic->getClassSubject($timetableClass);
        if(count($reply) > 0){
            for($i = 0; $i < count($reply); $i++){ ?>
                <option value="<?php echo $reply[$i]->subjectCode; ?>"><?php echo ucfirst($reply[$i]->subjectName); ?></option>
            <?php   }
        }else{ ?>
                <option disabled selected value="">No subjects for this class.</option>            
        <?php }
    }


    if(isset($_GET["timetableModalClass"])){
        $timetableModalClass = htmlentities(trim($_GET["timetableModalClass"]));
        $timetableModalDay = htmlentities(trim($_GET["timetableModalDay"]));
        $timetableModalTime = htmlentities(trim($_GET["timetableModalTime"]));
        $timetableModalClass = $logic->getClassName($timetableModalClass); 
        $timetableModalDay = $logic->getDayName($timetableModalDay); 
        $timetableModalTime = $logic->getTimeName($timetableModalTime); 
        ?>
        <span><?php echo ucfirst(strtolower($timetableModalDay)); ?></span> <span><?php echo $timetableModalTime; ?></span> for <span><?php echo $timetableModalClass; ?></span>
    <?php }


    if(isset($_GET["timetableModalStaffCode"])){
        $timetableModalStaffCode = htmlentities(trim($_GET["timetableModalStaffCode"]));
        $timetableModalDay = htmlentities(trim($_GET["timetableModalDay"]));
        $timetableModalTime = htmlentities(trim($_GET["timetableModalTime"]));
        $timetableModalStaffCode = $logic->getSingleStaffDetails($timetableModalStaffCode)->fullname; 
        $timetableModalDay = $logic->getDayName($timetableModalDay); 
        $timetableModalTime = $logic->getTimeName($timetableModalTime); 
        ?>
        <span><?php echo ucfirst(strtolower($timetableModalDay)); ?></span> <span><?php echo $timetableModalTime; ?></span> for <span><?php echo $timetableModalStaffCode; ?></span>
    <?php }

if(isset($_GET["classCodeSort"])){
    $classCode = htmlentities(trim($_GET["classCodeSort"]));
    $className = $logic->getClassName($classCode);
    $reply = $logic->getClassSubject($className);
    if(count($reply) > 0){ ?>
        <option value="">Select Subject</option>
        <?php for($i = 0; $i < count($reply); $i++){ ?>
            <option value="<?php echo $reply[$i]->subjectCode; ?>"><?php echo ucfirst($reply[$i]->subjectName); ?></option>
        <?php   }
    }else{ ?>
            <option disabled selected value="">No subjects for this class.</option>            
    <?php }
}

if(isset($_GET["getTiming"])){
    $activities = $logic->getTimingActivities(); 
    sort($activities);?>
        <!-- # code... -->
        <div class="form-group row timingDiv">
            <div class="col-sm-5">
                <select class="form-control timetableTime" name="timingActivity[]">
                    <?php
                        foreach ($activities as $key => $value) { ?>
                            <option value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
                        <?php }
                    ?>
                </select>
            </div>
            <div class="col-sm-4">
                <input class="form-control timeselect" id="setTime" name="timing[]" placeholder="8:00 AM" type="text">
            </div>
            <div class="col-sm-3">
                <button class="btn btn-primary" id="rTiming" style="background-color:#FB5574; border-color:#FB5574" type="button"> Delete Time</button>
            </div>
        </div>
<?php }

if(isset($_GET["addGradingDiv"])){ 
    $allGrades = $logic->getSystemGrades();
    $classes = $logic->getClasses();
    ?>
    <!-- # code... -->
    <div class="form-group row timingDiv">
        <div class="form-group col-sm-2">
            <label for=""> Class(es)</label>
            <select class="form-control" name="gradeClass[]" style="width:100%" multiple="true">
            <?php
                for($i = 0; $i < count($classes); $i++){ ?>
                    <option value="<?php echo $classes[$i]->class_code; ?>"><?php echo $classes[$i]->class_name?></option>
            <?php } ?>
            </select>
        </div>
        <div class="form-group col-sm-2">
            <label for=""> From <span style="color:#F91F14">*</span></label><input required name="from[]" class="form-control" placeholder="E.g; 80" type="text">
        </div>
        <div class="form-group col-sm-2">
            <label for=""> To <span style="color:#F91F14">*</span></label><input required name="to[]" class="form-control" placeholder="E.g; 100" type="text">
        </div>
        <div class="col-sm-2">
        <label for=""> Grade <span style="color:#F91F14">*</span></label>
            <select class="form-control timetableTime" name="grade">
                <?php
                    foreach($allGrades as $key => $value){ ?>
                        <option value="<?php echo $value; ?>"><?php echo ucwords($value); ?></option>
                    <?php }
                ?>
            </select>
        </div>
        <div class="form-group col-sm-2">
            <label for=""> Remark <span style="color:#F91F14">*</span></label><input required name="remark[]" class="form-control" placeholder="E.g; Excellent" type="text">
        </div>
        <div class="col-sm-2">
            <button class="btn btn-primary" id="rGrade" style="margin-top:27px; background-color:#FB5574; border-color:#FB5574" type="button"> Delete</button>
        </div>
    </div>
<?php }

if(isset($_GET["delStudent"])){
    $studentNumber = htmlentities(trim($_GET["delStudent"]));

    $reply = $logic->deleteStudent($studentNumber);
    if($reply === true){
        echo "true";
    }else{
        switch ($reply) {
            case 0:
                # code...
                echo "false";
                break;
            case 'not found':
                echo "not found";
                break;
            default:
                # code...
                echo "error";
                break;
        }
    }
}

if(isset($_GET["delStaff"])){
    $stffNumber = htmlentities(trim($_GET["delStaff"]));
    $reply = $logic->DeleteUser($stffNumber, "Staff");
    if($reply === true){
        echo "true";
    }else{
        switch ($reply) {
            case 0:
                # code...
                echo "false";
                break;
            case 'not found':
                echo "not found";
                break;
            default:
                # code...
                echo "error";
                break;
        }
    }
}

?>