<?php
    if(isset($_GET["ref"]) && isset($_GET["action"])){
        include_once ("./validate.php");
        $keys = $validate->getPaymentParams();
        $ref = htmlentities(trim($_GET["ref"]));
        $action = htmlentities(trim($_GET["action"]));
        $SchoolName = htmlentities(trim($_GET["SchoolName"]));
        $result = array();
        //The parameter after verify/ is the transaction reference to be verified
        $url = 'https://api.paystack.co/transaction/verify/'.$ref;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
        $ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$keys->skey.'']
        );
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $request = curl_exec($ch);
        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            // print_r($result);
            if($result){
            if($result['data']){
                //something came in
                // echo "Transaction was successful";
                if($result['data']['status'] == 'success'){
                // the transaction was successful, you can deliver value
                $amount = $result["data"]["amount"] / 100;
                $email = $result["data"]["customer"]["email"];
                $reply = $validate->logPayment($ref, $amount, $email, $SchoolName, $action);
                    if($reply === true){
                        echo "true";
                    }else{
                        echo "Wrong Parameters";
                    }
                }else{
                // the transaction was not successful, do not deliver value'
                // print_r($result);  //uncomment this line to inspect the result, to check why it failed.
                echo $result['data']['gateway_response'];
                // echo "Transaction was not successful: Last gateway response was: ".$result['data']['gateway_response'];
                }
            }else{
                echo $result['message'];
            }

            }else{
            //print_r($result);
            echo "Unidentified data";
            //die("Something went wrong while trying to convert the request variable to json. Uncomment the print_r command to see what is in the result variable.");
            }
        }else{
            //var_dump($request);
            echo "Bad Request";
            //die("Something went wrong while executing curl. Uncomment the var_dump line above this line to see what the issue is. Please check your CURL command to make sure everything is ok");
        }
    }else{
        echo "Page not found";
    }
?>