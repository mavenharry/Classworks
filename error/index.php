<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Error Page</title>
  
  
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<style>
.error-404 {
    margin: 0px auto;
    width: 70%;
    text-align: center;
    padding: 30px 0px 80px 0px;
}
.error-404 h2 {
    font-size: 60px;
    text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    -moz-text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    -webkit-text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    margin: 0px 0px 40px 0px;
    padding: 15px 0px;
    border-top: solid 3px #ccc;
    border-bottom: solid 3px #ccc;
    color: #08ACF0;
}
.page-404 {
    float: left;
    width: 100%;
    position: relative;
    margin: 0px 0px 30px 0px;
}
.page-404 p {
    font-size: 350px;
    font-weight: bold;
    line-height: 300px;
    text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    -moz-text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    -webkit-text-shadow: 2px 2px 2px rgba(0,0,0,0.2);
    color: #08ACF0;
}
.page-404 span {
    font-size: 48px;
    content: #fff;
    font-weight: bold;
    background-color: #fff;
    margin: -60px 0px 0px 0px;
    width: 100%;
    text-align: center;
    position: absolute;
    left: 0px;
    top: 50%;
    text-transform: uppercase;
    color: #08acf0;
    line-height: 60px;
}
.error-404 > p {
    margin: 0px 0px 30px 0px;
    font-size: 16px;
}
.error-404 a.go-back {
    font-size: 20px;
    text-transform: uppercase;
    font-weight: bold;
    display: inline-block;
    padding: 15px 30px;
    text-decoration: none;
    border: solid 1px;
    color: #333;
    transition: all 0.3s ease-in-out;
    color: #08ACF0;
    border-color: #08ACF0;
}
.error-404 a.go-back:hover {
    color: #fff !important;
    box-shadow: 0px 10px 20px 0px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 10px 20px 0px rgba(0,0,0,0.3);
    -webkit-box-shadow: 0px 10px 20px 0px rgba(0,0,0,0.3);
    background-color: #08ACF0;
}
</style>

<body>

  <div class="kode-content">
	<div class="container"><br><br><br>
		<div class="error-404">
			<div class="page-404">
				<p>40?</p>
				<span>Hello! We think you are lost.</span>
			</div>
			<p>We could not find the page you are looking for. Please try another page and verify the URL you have entered.</p>
			<a href="../views/authentication" class="go-back">Go back to home page</a>
		</div>   
	</div>   
</div>
<script src="./home/assets/js/script.js"></script>
</body>

</html>
