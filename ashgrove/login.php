<?php 
	session_start();
	include_once ("userbtn.php");
	$schoolDetails = $index->getSchoolDetails();
 ?>
<html lang="en" itemscope="" itemtype="//schema.org/MobileApplication">
    <head>
        <title><?php echo ucwords($schoolDetails->school_name); ?> - Login</title>        
        <!-------------------- START - Meta -------------------->
        <?php include("../includes/meta.php") ?>
        <!-------------------- END - Meta -------------------->
        <script src="../addons/jquery.js"></script>
        <script src="../addons/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/toastr.min.css">
        <link rel="stylesheet" href="../addons/bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="./css/login.css">
    </head>
    <body class="common signup-new">
        <div class="navigation-backdrop" id="backdrop-nav"></div>
        <div class="main-content">
            <section class="site--body" id="512">
                <div class="row" id="components-signup-form">
                    <div class="col-lg-6">
                        <div class="container col-lg-6__top-bar">
                            <a class="main-nav__logo" href="/" tabindex="-1">
                            <a href="../" style="display:inline; text-decoration:none">
                            <img style="object-fit:scale-down; width:70px; height:70px; margin-top:-10px; margin-bottom:-20px;" alt="" src="<?php echo $schoolDetails->logo; ?>">
                            <h4 class="schoolName" style="margin-left:80px; margin-top:-35px; font-size:3rem; color:#08ACF0"><?php echo ucwords($schoolDetails->school_name)?></h4>
                            </a>
                            </a>
                        </div>
                        <div class="components-signup-form padding-top-sm" style="margin-top:80px;">
                            <div class="tw-form">
                                <form class="form padding-md" id="components-signup-form-step-one" autocomplete="false" action="#" method="POST">
                                    <p class="form__focus-message">Easy schooling. Login to your account now!</p>
                                    <p class="form__focus-message-statement">We just need your login details to get you into your account.</p>
                                    <div class="form-group">
                                        <input class="form-field" required value="" id="email_no" name="email_no" placeholder="Email or Staff Number" type="text" tabindex="1">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-field" required placeholder="Password" type="password" value="" id="password" name="pass" tabindex="1">
                                    </div>
                                    <button id="login_btn" name="log" class="button button_green button_large button_full-width margin-top-sm" type="submit" tabindex="0">
                                        <span class="ladda-label">Login now</span>
                                    </button>
                                </form>
                                <a href="./forget_password" style="font-weight:bold; font-size:1.5rem; color: #8095a0; float: right; margin-right: 32px; margin-top: -25px;" >Recover your password?</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-6--cartoon hidden-md-down">
                        <div class="container col-lg-6__top-bar">
                            <div class="pull-right">
                                <p class="col-lg-6__top-bar__paragraph">Lost or want to leave?</p>
                                <a class="col-lg-6__top-bar__login-button" href="./">Go back home</a>
                            </div>
                        </div>
                        <div class="steps">
                            <div class="step-one">
                            <blockquote class="blockquote" style="text-align:justify; width:80%; margin-left:10%">
                              <h4 style="font-weight:normal; color:#ffffff; font-size:7rem; margin-bottom:-40px">“</h4>
                              <?php 
                              $json = file_get_contents('../includes/quotes.json');
                              $json_data = json_decode($json,true); 
                              $random = rand(0,count($json_data) - 1); 
                              ?>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem;"><?php echo $json_data[$random]["text"]?></h4>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem; margin-top:20px; float:right">― <?php echo $json_data[$random]["author"]?></h4>                              
                            </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
<script src="../js/toastr.min.js"></script>
<?php include_once ("../includes/modals/modalCaller.php"); ?>
<!-------------------- START - Top Bar -------------------->
<?php include("../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

    </body>
</html>
