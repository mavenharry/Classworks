<?php 
    include_once ("partners/ambBtn.php");
    $newNumber = $amb->newNumber();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <title> ClassWorks - School Automation System</title>

    <!-------------------- START - Meta -------------------->
    <?php include("./includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->

    <link href="./home/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="./home/assets/css/flaticon.css" rel="stylesheet">
    <link href="./home/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="./home/assets/css/animate.css" rel="stylesheet">
    <link href="./home/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="./home/assets/css/owl.theme.css" rel="stylesheet">
    <link href="./home/assets/css/slick.css" rel="stylesheet">
    <link href="./home/assets/css/slick-theme.css" rel="stylesheet">
    <link href="./home/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="./home/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="./home/assets/css/style.css" rel="stylesheet">
    <link href="./css/toastr.min.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="./favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="./favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon/favicon-16x16.png">
    <link rel="manifest" href="./favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

</head>

<body>
    <!-- <div style="margin-left:40px">
    <a href=""><img  style="width:80px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAMAAAC8EZcfAAAAvVBMVEX///8AAAC7AABvb28/Pz8PDw8vLy+fn58fHx9fX1+vr68FBQVPT0/8/PwVFRXPz8/z8/Pv7+/m5ubc3NzCwsKHh4dnZ2d+fn54eHiSkpJXV1dBQUHKyso2NjbT09MZGRlJSUm4uLiWlpYlJSWkpKTBFxf99va+CwspKSnjl5czMzPWZ2fRU1ODg4P34eH01NTGKyvqr6/PSkrZcnLSV1fuwMDmoKDegoLLOzvEIiLVYGD12Njnpqb55+ffh4c2QppCAAAI40lEQVR4nO2aa3uivBaGpYigGAUUUDzhodY67fQ0885M+87+/z9rcwjJSggG3Ra6r4vnUxsSuElWsg7YajVq1KhRo0aNGjVq1KjR/7X2/zz8ubm5e35/QXWjiITub28y/bn/eohPv2+gnp/qBmK1f7vh9bZHH//+en99fXj7d18338ufHF9kjHf0z2/f68T7/iDA42n/qQ0P3d/J+SL9qomP2xwndF8HnmBzFOtn9Xwvt3Isqj9/K8b7/noOXqSHSvHKbg6oSs3w+9l40WFTqX95v4DwuUqf8nH+Et/cfKsQsHXOEUP0UiHg/qxDBuuuSq/8cskU/q4yVPxxCWGVTnkvCrOkqtLlPV2yk2+rdHkXmeFrhYCt+0sIK3V5/7kAsNKz5pI5fKuSL7LDc3dK5bH101mnzW0NkfX+W3m+16rj6lQ/n8vh1Zd+wtJMsd7rmb5U+3uZKf6ou2CDfv4o3tB3bx814yXav3wTWePt+0vtBSSqvy+/Hp7JVN7+fr9/+nrlwmgu/348PX38/YpojRo1atSo0RcXsss01abxzFTM/hA26QdFGXhOXUSspkoic0ebvLRJm3/uk3vh9uhKg5+xgmWOsiY9a1pZn8k3SZ4xkxHOMhrFwy32gDRNyz1q1DU0QyUWYblHbRCOJYN6+BkTST+T0HRwi0dalH4pvrGWzjdeAuuYDt6cHpXNjCaZQkpjpA1z2qK0y/BZK9x7kf6fvaC5PDmMrNPoZLccIOoAQO/02FTEZJVkU1lkTdSTw7Ssm+RA4wGngE8ptY2pSSQmS3ZdNqMSwNO9coCORhvKTWArJP0Te99xbywDlJgqDxgAvkE5b9ImA5I1pSuulQE0ZQ9hAddwgXeSoVcBDGS3ZwDRFvDNyvFdCoh38foswEfAZ0q2//8K2E/6bKWuDgKO6KmtKH5JvksBR/HxaUonkAEMAd+2tB++ELDlqLPu8HQXDrAH+AS2Mdqp3TAMJjtu7WWAlq56UxC6DX1vwt7d7k2CMOyqusiqKNLAAHz85rKmR3rxqMPplQCOk91g6rg3UpNrbUq8DKhlLXY5m1SE0thgFW0G7HWjVxZwmD0dT9oE/9vJ3tGFhh+9PD+LYkA2znLa+R5dMomnAYldb9NbERz8CBA7pVpxhEK+IzPRw5WozyJzAScBEZ2fJECkrj5M50/2cDEgEyQsNWEfZWGVALRpf5+ZUOWY3Jtd31Q9KSATJNiGqEus4LwZjIloLJaOhq5f/HgRIBskCO+RSpcDYoeRyKFxvpJ6eks0gUpXBsjskHUxH36T04AASYdbQosNhMaOSt8jluTLABfQShcnABVXDoioBUeLSrdbl+uLWk5mn0sZIIwhYYaitP2N/hiCZTHkgK0ugBjSocmxCML3IFoNPXHPXCopAgTHtEpbZ/jNHOCy53LANe099gFsfI1G35G9TFELjTc7vp4hAgTp5oHykYVHtNGVAyK6rBNqL+lOBTMaydiIYi8hIDmKLPFlokAOCBYBHFip40PcEbbtlQXMih5D8WWiRQnAuWDcCs/VlL8Q5FIUek2DhxI+LMf8DTi1SwC2tvlxWcqMcofEodgGDR92TL3dWgKolgH08+NI4cbp8JcOdiGgBV/1gIqWByjJW6SAo9w4kDFbff5iUAjYWsN+j+J7Q2mJqctD/tw6MhUtnYs1FTYPYF4L+t1kdoAfMDVOBy+N3OSAua3AllQsn42XioIFgyt8JHkxLR8eihJEOaDD8W3BaBSfF5YLZ/FQDMjGjzr78qBYNdr4LpmFElndTGFEo4F5tPrGxI5nkZ4h5glApvgWezwHnD3Y16F1nwEuAUjbEpFoAMerg9iWgds7AcgefPF+YnKGTjfKPIkZjEsD2kzc1yHtZAd3l+DJbD7NAbJAc84sWfmlAdmo95E0A8sDr8DWdXlAG5prvJ82hYBqeUCmJkDTNvHLuycBGXtpM+vAC4eNdAdwgCv6FHBcpdkS/25AGutK6NQe8qOSsMsqCKpNh1++R3ay4HFBw1Y4QdzmyV2PRZ+e5SogFUyDWysU3YfE3fRoSoIleugFwscoIBpA3PkTK+TOW7q7iQPPqhMkvUOP+ezLJHkBcYcDi2UBwR1wmcwWsHOL3M7FW9m2pXsrW1OTfixa8oZ4BN4qK3viknFWjYFfgYADYFMONGHe3ZwI/NUmjskNWJC21NimQya7Gqo0+tWCXv4ONBoetnOPAuc/H+85E3LjgSqu6qLlmL+ARst8AdPpbVx3s1vn78LfwZnPmeFLyieqfDtrfepPd8v6fvYFQlbZZ5F6RNNA6WeRWgRSr7BuFqEmFLDkx6GKtf3iKwwC6nJfyKuW/dVXmG5i7fq/0ih1R1kn4u9Lf14DcjuZUu867NAgRF+YitnxE8MOaBTkxXGfi7vZj0faKbo2I7QexcHeun+Jq1ANHSt1kl2NZK2BqY6XY3+lxUFDhyZ17djU1TSz6GmGG3Vyt1rqits0yGqDWvM82K7aeusSqR3mX8ecbvEjejhespLQQAy4Vrx0VpCa9m5vScDZZovhl4oD9DVrikNldQUvCAGtAT03giRUb3d9ku19BiAaqC1LS41nwpyqQsDsXWI5ZlJu7aJw4HweoB6nXGoaGi/NzphatRBwBoOncJFS2Ub6meBagEYPK6bqxM8e4S80a0MxvB7elZ1+1q93IIADWKbyNUw1N71rApoGlhPXFZKCWohnFa09QzG7SSza0bJ+hkkAzUdwp6mCMNUmecPPWOJ+WndaKyAb8bXkd4TCJRbPYHRWmcNPARxlH1G20Kk7RqcIcAZTtMwGI1kdw/4MQBC3wRTDVazCXUz7Zbs4+Wekhej6gJYWDFPNzQiHnCCuiYrOwRWd6v7ApoCRlfjXB9zQ6nBXs9AWf7OwjbBV5EnGJv5pgNVN9xeh8hXt6oAHalHDKPvqaQN1PRy7hjEqBGytByt/PVz7xgD74owKhcp1AKekZL1sg1+zeFGz7S40Eqh4tCKg+mCcFXfSFi72OioJeuyQqwI1atSoUaNGjRo1atSoUaMr6r850IuUeiJIQAAAAABJRU5ErkJggg=="/></a>
    </div> -->

    <!-- start page-wrapper -->
    <div class="page-wrapper" id="home">

        <!-- Start header -->
        <header id="header" class="site-header header-style-2">
            <nav class="navigation navbar navbar-default">
                <div class="site-logo">
                    <a href="#"><img src="./images/logo.png" style="width:auto; height: 60px; margin-top:10px; margin-left:15px" /></a>
                </div>
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="navbar-collapse navbar-right collapse navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li><a href="./">Home</a></li>
                            <li><a href="#about">About Us</a></li>
                            <li><a href="#contact">Contact Us</a></li>
                            <li><a href="#features">Features</a></li>
                            <li><a href="" id="examPrepButton" data-toggle="modal" data-target="#examPrep">Exam Prep App</a></li>
                            <li class="my_hover_up" style="background:#08acf0; padding-right:10px; padding-left:10px;"><a href="./views/authentication" style="color:#FFFFFF">Login</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->


        <!-- start hero-section -->
        <section class="hero-section-s3">
            <div class="container">
                <div class="row">
                    <div class="col col-md-6 col-sm-8" style="margin-top: -50px;">
                        <div class="content">
                            <h2 style="">Making Schools Operation Digital & Paperless</h2>
                            <p style="font-size:1.4rem">with simplified & automated <strong>attendance</strong>, <strong>assessment</strong>, <strong>admission</strong>, <strong>payment</strong>, <strong>performance</strong>, <strong>communication</strong> etc. from the tutor's to the management's table.</p>
                            <div class="row">
                                <div class="btns" style="margin-top:-40px !important">
                                   <center class="center_line">
                                        <a href="#contact" style="border-radius:5px; font-size:1.5rem; background-color:#08acf0; padding-right:20px; padding-left:20px; padding-bottom:10px; padding-top:10px; color:#FFF">Contact Setup Team</a>
                                   </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="phone" style="width: 500px; margin-top: -50px;">
                    <!-- <div style="position:absolute; display: block;">
                        <img style="width: 250px; height: 250px; object-fit: fill;" src="http://pluspng.com/img-png/student-thinking-png-hd-the-g4s-model-is-rooted-in-girls-believing-in-themselves-and-their-ability-to-contribute-to-517.png">
                        <h2 style="line-height: 10px; font-size: 1.2em; font-weight: 600; color: #41516a;">Maryam Oroghenedumo</h2>
                        <h2 style="line-height: 0px; font-size: 1em; font-weight: 500; color: #08acf0;">Best Mathematics Student, 2018</h2>
                        <h2 style="line-height: 0px; font-size: 1em; font-weight: 200; color: #41516a;">Tantua International Academy</h2>
                    </div> -->
                    <img style="width: 500px; height: auto; object-fit: contain;" src="./home/assets/images/home_img_1.png" alt>
                </div>
                
            </div>
        </section>
        <!-- end hero-section -->


        <!-- start features-section -->
        <section class="features-section-s2 section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title-s7">
                            <h2>Simple, Powerful & Affordable <br> School Automation System</h2>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-xs-12 center">
                        <div class="features-grids">
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-settings"></i>
                                </div>
                                <div class="details">
                                    <h3>Simple & easy to use</h3>
                                    <p>Designed to empower teachers and parents collaboration. No expertise required to quickly accomplish a task.</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-analytics"></i>
                                </div>
                                <div class="details">
                                    <h3>Detailed</h3>
                                    <p>ClassWorks is designed to support schools of all sizes and structure, with customizable grading, Attendance and much more.</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-team"></i>
                                </div>
                                <div class="details">
                                    <h3>User Experience</h3>
                                    <p>ClassWorks is simple and intuitive. The intuitive interface negates the need for constant training and maximises usage..</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-speedometer"></i>
                                </div>
                                <div class="details">
                                    <h3>Reliable</h3>
                                    <p>ClassWorks is built with the latest technological tools,
                                        standards and platforms, guaranteeing security, speed and a 99.99% uptime</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end features-section -->


        <!-- start about-section-s4 -->
        <section class="about-section-s4" id="about">
            <div class="content">
                <div class="left-grid">
                    <div class="video-holder">
                        <a href="./home/assets/videos/obama.mp4" class="video-btn" data-type="iframe">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
                <div class="right-grid">
                    <div class="inner">
                        <div class="section-title">
                            <h3 style="color:#FFF">Complete School IT Solution</h3>
                            <h1 style="color:#FFF">ALL-IN-1 DIGITAL NEEDS</h1>
                        </div>
                        <div class="details">
                                <a href="#contact" style="border-radius:5px; font-size:1.5rem; background-color:#FFF; padding:10px 30px; color:#08acf0">Contact Us Now</a>
                        </div>
                        <div class="about-pic">
                            <img src="./home/assets/images/about-pic.png" alt>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end about-section-s4 --> 


        <!-- start screenshot-section -->
        <!-- <section class="screenshot-section section-padding" id="app-showcase">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="section-title-s7">
                            <h2 style="margin-bottom: -100px; margin-top: 20px;">ClassWorks System Preview</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->


        <!-- start cat-section -->
        <section class="cat-section" id="features">
            <div class="container">
                <div class="row first-row">
                    <div class="col col-md-6">
                        <div class="img-holder">
                            <img src="./home/assets/images/attendance.png" style="width: 500px" alt>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="cta-text" style="margin-top:-30px">
                            <span>Automate your Attendance with ClassWorks</span>
                            <h3>Attendance Monitoring System</h3>
                            <p>AMS Provides you with a unique platform of taking attendance digitally for both staff and students 
                                with the use of a digital ID card generated from the system. It helps to take track from the 
                            system as to whether a student/pupil or staff is absent, late, early at the end of the day.
                        The system also notifies parents immediately that their ward has arrived safely to school, thereby assuring 
                    their ward's safety while in school.</p>
                        </div>
                    </div>
                </div>
                <div class="row second-row">
                    <div class="col col-md-6 mobView">
                        <div class="img-holder">
                            <img src="./home/assets/images/childpickup.png" alt>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="cta-text">
                            <span>Automated Child safety System</span>
                            <h3>Child Safety System</h3>
                            <p>CSS is a child pick up system that gaurantees the security of pupils/students at the close
                                of school. Access to pick up students are strictly for parents and those approved by the parents
                                on our system are allowed to pick up pupils/students from the school. Parents get instant notification
                            from our system that their ward has left the school premises, this helps to boost the security system of the school.</p>
                        </div>
                    </div>
                    <div class="col col-md-6 deskView">
                        <div class="img-holder">
                            <img src="./home/assets/images/childpickup.png" alt>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end cat-section -->

            <!-- start cat-section -->
        <section class="cat-section">
            <div class="container">
                <div class="row first-row">
                    <div class="col col-md-6">
                        <div class="img-holder" style="margin-top:-50px">
                            <img src="./home/assets/images/timetable.png" alt>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="cta-text" style="margin-top:-20px">
                            <span>Automated Time Schedule Generation System</span>
                            <h3>Timetable Management System</h3>
                            <p>As easy as it could get, Classworks relieves school staffs/administrator the stress of generating time schedules for individual classes and staffs with a built in lecture time reminder system. This feature is completely customizable and personalized for individual classes and tutors.</p>
                        </div>
                    </div>
                </div>
                <div class="row second-row">
                    <div class="col col-md-6 mobView">
                        <div class="img-holder">
                            <img src="./home/assets/images/assessment.png" alt>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="cta-text">
                            <span>Automatic Assessment Collation and report sheet generation</span>
                            <h3>Assessment/Report Sheet Generator</h3>
                            <p>computerized assessment collations and grading system for school wide result formulation and grading. Remote grading system for tutor and electronic report sheet generation for parents preview/download. Track all term and session results for any students, free data backup/storage for as long as the school wants ensuring maximum data integrity and availability.</p>
                        </div>
                    </div>
                    <div class="col col-md-6 deskView">
                        <div class="img-holder">
                            <img src="./home/assets/images/assessment.png" alt>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <section class="cat-section">
                <div class="container">
                    <div class="row first-row">
                        <div class="col col-md-6">
                            <div class="img-holder">
                                <img src="./home/assets/images/account.png" alt>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="cta-text" style="margin-top:-20px">
                                <span>Process online and offline fees payments</span>
                                <h3>On-Desk Payment Terminal</h3>
                                <p>Manage and collect all students fees payments with ease, whether online or offline without internet connectivity restrictions and coverages. <br> Process card based and cardless payments easily on-site.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row second-row">
                        <div class="col col-md-6 mobView">
                            <div class="img-holder">
                                <img src="./home/assets/images/students.png" alt>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="cta-text">
                                <span>Complete school portal system</span>
                                <h3>Portal/School Management System</h3>
                                <p>Experience an intuitive and complete school portal system which includes staffs, parents and students panels with a unique and accessible functionalities built for productivity and capability.</p>
                            </div>
                        </div>
                        <div class="col col-md-6 deskView">
                            <div class="img-holder">
                                <img src="./home/assets/images/students.png" alt>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </section>
        <!-- end cat-section -->


        <!-- start testimonials-section-s3 -->
        <!-- <section class="testimonials-section-s3 section-padding" id="testimonials">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title-s8">
                            <h2>Our User’s Review</h2>
                            <p>Success Stories of Schools/Parents who have used ClassWorks School Automation System .</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="testimonial-grids testimonials-slider-s3">
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-quotation"></i>
                                </div>
                                <div class="client-info">
                                    <div class="img-holder">
                                        <img src="img/avatar1.jpg" height="50" width="50" alt>
                                    </div>
                                    <h4>Ugo Matt</h4>
                                    <span>App Developer</span>
                                </div>
                                <div class="details">
                                    <p>ClassWorks has so much been helpful to my school, Since i started using classworks, my school has been automated</p>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-quotation"></i>
                                </div>
                                <div class="client-info">
                                    <div class="img-holder">
                                        <img src="img/avatar1.jpg" height="50" width="50" alt>
                                    </div>
                                    <h4>Maven Harry</h4>
                                    <span>UX Designer</span>
                                </div>
                                <div class="details">
                                    <p>ClassWorks has so much been helpful to my school, Since i started using classworks, my school has been automated.</p>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="icon">
                                    <i class="fi flaticon-quotation"></i>
                                </div>
                                <div class="client-info">
                                    <div class="img-holder">
                                        <img src="img/avatar1.jpg" height="50" width="50" alt>
                                    </div>
                                    <h4>Emmanuel Abeng</h4>
                                    <span>App Developer</span>
                                </div>
                                <div class="details">
                                    <p>ClassWorks has so much been helpful to my school, Since i started using classworks, my school has been automated.</p>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- end testimonials-section-s3 -->


        <!-- start cta-s2-section -->
        <section class="cta-s2-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-7 col-md-7">
                        <div class="text">
                            <h2>All Your School <span>Digital & ICT </span>Needs At the Click Of A Button</h2>
                        </div>
                    </div>
                    <div class="col col-lg-4 col-md-4">
                        <div class="download">
                                <a href="#contact" style="border-radius:5px; font-size:1.5rem; background-color:#08acf0; padding-right:20px; padding-left:20px; padding-bottom:10px; padding-top:10px; color:#FFF">Contact Us Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- <section class="partners-section" id="patners">
            <h2 class="hidden">Partners</h2>
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="partners-slider">
                            <div class="grid">
                                <img src="./home/assets/images/partners/uniport.png" alt>
                            </div>
                            <div class="grid">
                                <img src="./home/assets/images/partners/img-2.png" alt>
                            </div>
                            <div class="grid">
                                <img src="./home/assets/images/partners/img-3.png" alt>
                            </div>
                            <div class="grid">
                                <img src="./home/assets/images/partners/img-4.png" alt>
                            </div>
                            <div class="grid">
                                <img src="./home/assets/images/partners/img-5.png" alt>
                            </div>
                            <div class="grid">
                                <img src="./home/assets/images/partners/img-3.png" alt>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section> -->

        <section class="contact-section-s2 section-padding" id="contact">
            <div class="container">
                <div class="row contact-form-info">
                    <div class="col col-md-4">
                        <div class="contact-info">
                            <h2>Get In Touch</h2>
                            <div>
                                <h5>Address:</h5>
                                <p>No. 10 NTA/Choba Road, Ada Odum Complex, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</p>
                            </div>
                            <div>
                                <h5>Call Us:</h5>
<<<<<<< HEAD
                                <p>Help Desk: +234 (0) 8109205582</p>
=======
                                <p>Support/Help: +234 (0) 8109205582, <br> Allison: +234 (0) 8085608359</p>
>>>>>>> eaedcec601d4bb767dcbd2011275a23f4dade813
                            </div>
                            <div>
                                <h5>Email:</h5>
                                <a href="mailto:hello@classworks.xyz"></a>                                <a href="mailto:"><p>Hello@classworks.xyz</p></a>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-8">
                        <div class="contact-form">
                            <form id="contact-form" class="contact-validation-active">
                                <div>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Full name..">
                                </div>
                                <div>
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email address..">
                                </div>
                                <div>
                                    <input type="text" id="subject" name="subject" class="form-control" placeholder="Subject..">
                                </div>
                                <div>
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone no..">
                                </div>
                                <div>
                                    <textarea class="form-control" id="message" name="message" placeholder="Write.."></textarea>
                                </div>
                                <div class="submit">
                                    <button type="submit">Send Now</button>
                                    <div id="loader">
                                        <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                                    </div>
                                </div>
                                <div class="error-handling-messages">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>            
            </div> 
        </section>

        <footer class="site-footer">
            
            <div class="copyright-info">
                <div class="container">
                    <p>©<?php echo date("Y") ?> Built with &hearts; by <a style="margin-left:5px;" href="http://CloudNetIQ.com">The IQ People</a></p>
                </div>
            </div>
        </footer>
    </div>


<div aria-hidden="true" class="onboarding-modal modal fade animated" id="affiliate" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="text-transform: uppercase; font-size:2rem;text-align:left; margin-top:50px; color:#8095A0; margin-left:60px">Become a partner</h4>
                <center>
                <form style="margin-top:30px; font-size: 1.1rem; width: 80%; float: none; text-align: left; color: #8095A0;" id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="form-group" style="margin-top:10px;">
                  <label for="" style="font-weight:normal">Partner Number (Please copy this)</label>
                  <input style="border: 1px solid #8095A0; color:#8095A0; height: 40px; font-size:1.1rem" name="partnerNumber" class="form-control" readonly value="<?php echo $newNumber; ?>" type="text">
                </div>

                <div class="form-group" style="margin-top:30px;">
                    <label style="font-weight:normal" for="">Your Full Name <span style="color:#F91F14">*</span></label>
                    <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerName" class="form-control" placeholder="E.g; Mr. Mika Amadi" type="text">
                </div>

                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group" style="margin-top:10px;">
                        <label for="" style="font-weight:normal"> Nationality</label>
                        <Select style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" name="partnerCountry" class="form-control">
                        <option value="nigeria">Nigeria</option>
                        <option value="algeria">Algeria</option>
                        <option value="angola">Angola</option>
                        <option value="benin">Benin</option>
                        <option value="botswana">Botswana</option>
                        <option value="burkina Faso">Burkina Faso</option>
                        <option value="burundi">Burundi</option>
                        <option value="cameroon">Cameroon</option>
                        <option value="cape-verde">Cape Verde</option>
                        <option value="central-african-republic">Central African Republic</option>
                        <option value="chad">Chad</option>
                        <option value="comoros">Comoros</option>
                        <option value="congo-brazzaville">Congo - Brazzaville</option>
                        <option value="congo-kinshasa">Congo - Kinshasa</option>
                        <option value="ivory-coast">Côte d’Ivoire</option>
                        <option value="djibouti">Djibouti</option>
                        <option value="egypt">Egypt</option>
                        <option value="equatorial-guinea">Equatorial Guinea</option>
                        <option value="eritrea">Eritrea</option>
                        <option value="ethiopia">Ethiopia</option>
                        <option value="gabon">Gabon</option>
                        <option value="gambia">Gambia</option>
                        <option value="ghana">Ghana</option>
                        <option value="guinea">Guinea</option>
                        <option value="guinea-bissau">Guinea-Bissau</option>
                        <option value="kenya">Kenya</option>
                        <option value="lesotho">Lesotho</option>
                        <option value="liberia">Liberia</option>
                        <option value="libya">Libya</option>
                        <option value="madagascar">Madagascar</option>
                        <option value="malawi">Malawi</option>
                        <option value="mali">Mali</option>
                        <option value="mauritania">Mauritania</option>
                        <option value="mauritius">Mauritius</option>
                        <option value="mayotte">Mayotte</option>
                        <option value="morocco">Morocco</option>
                        <option value="mozambique">Mozambique</option>
                        <option value="namibia">Namibia</option>
                        <option value="niger">Niger</option>
                        <option value="nigeria">Nigeria</option>
                        <option value="rwanda">Rwanda</option>
                        <option value="reunion">Réunion</option>
                        <option value="saint-helena">Saint Helena</option>
                        <option value="senegal">Senegal</option>
                        <option value="seychelles">Seychelles</option>
                        <option value="sierra-leone">Sierra Leone</option>
                        <option value="somalia">Somalia</option>
                        <option value="south-africa">South Africa</option>
                        <option value="sudan">Sudan</option>
                        <option value="south-sudan">Sudan</option>
                        <option value="swaziland">Swaziland</option>
                        <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                        <option value="tanzania">Tanzania</option>
                        <option value="togo">Togo</option>
                        <option value="tunisia">Tunisia</option>
                        <option value="uganda">Uganda</option>
                        <option value="western-sahara">Western Sahara</option>
                        <option value="zambia">Zambia</option>
                        <option value="zimbabwe">Zimbabwe</option>
                        <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group" style="margin-top:10px">
                        <label style="font-weight:normal" for=""> State/Province <span style="color:#F91F14">*</span></label>
                        <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerState" class="form-control" placeholder="E.g; Lagos" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6" style="margin-top:10px">
                     <div class="form-group">
                        <label style="font-weight:normal" for=""> City/Region <span style="color:#F91F14">*</span></label>
                        <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group" style="margin-top:10px;">
                        <label style="font-weight:normal" for=""> Gender</label>
                        <Select style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" name="partnerGender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </Select>
                     </div>
                   </div>
               </div>
               <div class="form-group" style="margin-top:10px">
                <label style="font-weight: normal" for=""> Residential Address</label>
                <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" name="partnerAddress" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group" style="margin-top:10px;">
                        <label style="font-weight:normal" for=""> Email Address <span style="color:#F91F14">*</span></label>
                        <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerEmail" class="form-control" placeholder="E.g; James@gmail.com" type="email">
                     </div>
                    </div>
                  <div class="col-sm-6" style="margin-top:10px;">
                     <div class="form-group">
                        <label style="font-weight:normal" for=""> Phone Number <span style="color:#F91F14">*</span> </label>
                        <input style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerPhone" class="form-control" placeholder="E.g; +2348188376352" type="text">
                     </div>
                  </div>
               </div>

                <div class="form-check" style="margin-top:12px;">
                <label style="font-weight:normal" class="form-check-label">
                <input style="margin-right:10px;" id="formChecker" class="form-check-input" type="checkbox">
                I hereby verify that i'm eligible and ready to work with classworks. All terms, conditions and company policies apply.
                </label>
                </div>
                <div class="form-buttons-w" style="margin-bottom:50px; margin-top:15px; ">
                    <input type="submit" id="submitAffiliate" disabled value="Submit this form now" name="partnerBtn" class="my_hover_up btn btn-primary" style="height:45px; text-align:center; font-size:1.1rem; vertical-align:middle; border-color:#08ACF0; background-color:#08ACF0">
                    <button data-dismiss="modal" class="my_hover_up btn btn-primary" type="button" style="height:45px; color:#afbabf; border-color:#afbabf; text-align:center; font-size:1.1rem; vertical-align:middle; background-color:transparent"> Cancel</button>
                </div>
                </form>
                </center>
            </div>
        </div>
    </div>
</div>

<!--Exam Prep -->

<div aria-hidden="true" class="onboarding-modal modal fade animated" id="examPrep" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="text-transform: uppercase; font-size:2rem;text-align:left; margin-top:50px; color:#8095A0; margin-left:60px">Download Exam Prep App</h4>
                <center>
                <form style="margin-top:30px; font-size: 1.1rem; width: 80%; float: none; text-align: left; color: #8095A0;" id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                <div class="form-group" style="margin-top:30px;">
                    <label style="font-weight:normal" for="">Your Full Name <span style="color:#F91F14">*</span></label>
                    <input id="examPrepName" style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerName" class="form-control" placeholder="E.g; Mika Amadi" type="text">
                </div>

                <div class="form-group" style="margin-top:20px;">
                    <label style="font-weight:normal" for="">Your Phone Number <span style="color:#F91F14">*</span></label>
                    <input id="examPrepPhone" style="border: 1px solid #8095A0;  height: 40px; font-size:1.1rem;" required name="partnerName" class="form-control" placeholder="E.g; 08123456789" type="text">
                </div>

                <div class="form-check" style="margin-top:12px;">
                <label style="font-weight:normal" class="form-check-label">
                ClassQuiz is a premium examination preparatory android app that enables you prepare for any exam on the go and <span style="font-weight:bold">win real daily cash rewards</span>. A yearly subscription fee of &#x20A6;500 is required to download.
                </label>
                </div>

                <div class="form-buttons-w" style="margin-bottom:50px; margin-top:15px; ">
                    <input type="button" onclick="payWithPaystack()" value="Get My Exam Prep Access" class="my_hover_up btn btn-primary" style="cursor:pointer; height:45px; text-align:center; font-size:1.1rem; vertical-align:middle; border-color:#08ACF0; background-color:#08ACF0">
                    <button data-dismiss="modal" class="my_hover_up btn btn-primary" type="button" style="cursor:pointer; height:45px; color:#afbabf; border-color:#afbabf; text-align:center; font-size:1.1rem; vertical-align:middle; background-color:transparent"> Cancel</button>
                </div>
                </form>
                </center>
            </div>
        </div>
    </div>
</div>


    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script src="./home/assets/js/jquery.min.js"></script>
    <script src="./home/assets/js/bootstrap.min.js"></script>

    <script src="./home/assets/js/jquery-plugin-collection.js"></script>

    <script src="./home/assets/js/script.js"></script>
    <script src="./home/preloader/js/index.js"></script>
    <script src="./js/toastr.min.js"></script>
    <script src="./js/app.js"></script>
    <?php include_once ("includes/modals/modalCaller.php"); ?>

    <script>
    $("#formChecker").on('click', function(){
        if (this.checked == true){
            $("#submitAffiliate").attr('disabled', false);
        } else {
            $("#submitAffiliate").attr('disabled', true);
        }
    })

    var urlKey = window.location.search.replace("?","")
    
    if(urlKey.toLowerCase() == "partners"){
        $('#partnerButton').trigger('click');
    }

    if(urlKey.toLowerCase() == "examprep"){
        $('#examPrepButton').trigger('click');
    }

  function payWithPaystack(){
    var name = $("#examPrepName").val()
    var phone = $("#examPrepPhone").val()
    if(name.length > 0 && phone.length > 10){
        var handler = PaystackPop.setup({
        key: 'pk_live_7cdb9890e5e8f181554e22a1a1ef13e6a4483009',
        email: 'hello@classworks.xyz',
        amount: 508*100,
        metadata: {
            custom_fields: [
                {
                    display_name: "Mobile Number",
                    variable_name: "mobile_number",
                    value: phone
                }
            ]
        },
        ref: ''+Math.floor((Math.random() * 1000000000) + 1),
        callback: function(response){
            toastr.success('success. transaction ref is ' + response.reference);
            $.ajax({
                url: "./examprep/examPrepBtn.php",
                method: 'GET',
                data: {name : name, phone : phone},
                timeout: 50000,
            }).done(function(data){
                if(data == "true"){
                    // PAYMENT VERIFIED
                    toastr.success("Successful, Please Check your phone a download link has been sent to you.");
                    var delay = 4000;
                    setTimeout(function(){
                        window.location.reload();
                    }, delay);
                }else{
                    //PAYMENT ERROR
                    $("#overlay").css("display", "none");
                    var message ="Oh sorry an unexpected error occurred payment can't be processed. Please try again";
                    toastr.error(message);
                }
            })
            .fail(function(data){
                $("#overlay").css("display", "none");
                var message ="Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference;
                toastr.error(message);
            });
        },
        onClose: function(){
            var message ="Transaction was cancelled";
            toastr.error(message);
        }
        });
        handler.openIframe();
    }else{
        var message ="Information provided is incomplete";
        toastr.error(message);
    }
  }
</script>

<!-------------------- START - Top Bar -------------------->
<?php include("./includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

</body>
</html>
