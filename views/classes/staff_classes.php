<?php include_once ("classbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Classes</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
        <!-------------------- START - MODAL -------------------->
        <?php include("../../includes/modals/index.php") ?>
        <!-------------------- END - MODAL -------------------->
<?php
$staffNo = $_SESSION["uniqueNumber"];
$schoolDetails = $classClass->getSchoolDetails();
$allClass = $classClass->fetchAllclass_staff($staffNo);
?>
<body  class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar2.php"); ?>
            <!-------------------- END - Main Menu -------------------->
            
            <div class="content-w">

             <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/topbar.php") ?>
        <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target=".bd-example-modal-sm" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL MY CLASS(ES)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Class name</th>
                                                    <th>No. of students</th>
                                                    <th>No. of tutors</th>
                                                    <th class="text-center">Student(s)</th>
                                                    <th class="text-center">Assessment</th>
                                                    <th class="text-center">Timetable</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allClass) == 0){
                                                    echo "No class in record";
                                                }else{
                                                    for($i = 0; $i < count($allClass); $i++){ ?>
                                                <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i + 1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucwords($allClass[$i]->class_name); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allClass[$i]->allStudent; ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allClass[$i]->allTutor; ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../students/staff_students?class_id=<?php echo $allClass[$i]->class_code ?>">View student(s)</a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../assessment/classsheet?class=<?php echo $allClass[$i]->class_code."&session=".$classClass->getCurrentAcademicSession()."&term=".$classClass->getCurrentAcademicTerm() ?>">View report</a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../timetable/class_timetable?class=<?php echo $allClass[$i]->class_code."&session=".$classClass->getCurrentAcademicSession()."&term=".$classClass->getCurrentAcademicTerm() ?>">View timetable</a>
                                                    </td>
                                                </tr>
                                                <?php } 
                                                }?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allClass) > 20){ ?>
                                            <div style="float: right;" class-"element-actions">
                                            <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <span style="font-size: .9rem;">Load more</span>
                                            </a>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>