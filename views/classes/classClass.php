<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class classClass extends logic {    

    function fetchAllclass(){
        $stmt = $this->uconn->prepare("SELECT classes.class_name, classes.class_code, (SELECT COUNT(id) FROM students where students.present_class = classes.class_name) AS allStudent, (SELECT COUNT(id) FROM subjectTeachers where subjectTeachers.classCode = classes.class_code) AS allTutor FROM classes");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $classArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->class_code = $row["class_code"];
            $obj->class_name = $this->outputRefine($row["class_name"]);
            $obj->allStudent = $row["allStudent"];
            $obj->allTutor = $row["allTutor"];
            $classArray[] = $obj;
        }
        $stmt->close();
        return $classArray;
    }

    
    function fetchAllclass_staff($staffNo){
        $stmte = $this->uconn->prepare("SELECT DISTINCT classCode FROM subjectTeachers WHERE staffCode = ?");
        $stmte->bind_param("s", $staffNo);
        $stmte->execute();
        $stmte_result = $this->get_result($stmte);
        $classArray = array();
        while($rowe = array_shift($stmte_result)){
        $stmt = $this->uconn->prepare("SELECT classes.class_name, classes.class_code, (SELECT COUNT(id) FROM students where students.present_class = classes.class_name) AS allStudent, (SELECT COUNT(id) FROM subjectTeachers where subjectTeachers.classCode = classes.class_code) AS allTutor FROM classes WHERE class_code = ?");
        $stmt->bind_param("s", $rowe["classCode"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->class_code = $row["class_code"];
                $obj->class_name = $this->outputRefine($row["class_name"]);
                $obj->allStudent = $row["allStudent"];
                $obj->allTutor = $row["allTutor"];
                $classArray[] = $obj;
            }
        }
        $stmte->close();
        return $classArray;
    }

    
    function searchClass($searchClass){
        $classArray = array();
        $stmt = $this->uconn->prepare("SELECT classes.class_name, classes.class_code, (SELECT COUNT(id) FROM students where students.present_class = classes.class_name) AS allStudent, (SELECT COUNT(id) FROM subjectTeachers where subjectTeachers.classCode = classes.class_code) AS allTutor FROM classes WHERE classes.class_code = ?");
        $stmt->bind_param("s", $searchClass);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->class_code = $row["class_code"];
                $obj->class_name = $this->outputRefine($row["class_name"]);
                $obj->allStudent = $row["allStudent"];
                $obj->allTutor = $row["allTutor"];
                $classArray[] = $obj;
                $stmt->close();
                return $classArray;
            }
        }else{
            $stmt->close();
            return $classArray;
        }
    }

}
?>