<!DOCTYPE html>
<html>
   <body style='background-color: #222533; padding: 20px; font-family: font-size: 14px; line-height: 1.43; font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif;'>
      <div style='max-width: 600px; margin: 10px auto 20px; font-size: 12px; color: #8095A0; text-align: center;'>If you are unable to see this message, <a href='#' style='color: #8095A0; text-decoration: underline;'>click here to view in browser</a></div>
      <div style='max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);'>
         <table style='width: 100%;'>
            <tr>
               <td style='background-color: #fff;'><img alt='' src='../../images/logo.png' style='width: 70px; margin-left:25px; padding: 20px'></td>
               <td style='padding-left: 50px; text-align: right; padding-right: 20px;'><a href='../../' style='color: #261D1D; text-decoration: underline; font-size: 14px; letter-spacing: 1px; font-weight:bold; background-color:#08ACF0; color:#FFF; padding:10px; border-radius:5px; text-decoration:none; margin-right:30px;'>Partner Program</a></td>
            </tr>
         </table>
         <div style='padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);'>
            <h1 style='margin-top: 0px; color:#8095A0'>Hi, <?php echo $fullname; ?></h1>
            <div style='color: #8095A0; font-size: 14px;'>
               <p>Thank you for creating account on ClassWorks.xyz, there is one more step before you can use it, you need to activate your account by clicking the link below. Once you click the button, just login to your account and you are set to go.</p>
            </div>
            <a href='https://classworks.xyz/views/setup?verifier=<?php echo base64_encode($email); ?>' style='padding: 8px 20px; background-color: #08ACF0; color: #fff; font-weight: bolder; font-size: 16px; display: inline-block; margin: 20px 0px; margin-right: 20px; text-decoration: none;'>Activate my account</a>
            <h4 style='margin-bottom: 10px; color:#8095A0'>Need Help?</h4>
            <div style='color: #8095A0; font-size: 12px;'>
               <p>If you have any questions you can simply reply to this email or find our contact information below. Also contact us at <a href='#' style='text-decoration: underline; color: #8095A0;'>hello@ClassWorks.xyz</a></p>
            </div>
         </div>
         <div style='background-color: #F5F5F5; padding: 40px; text-align: center;'>
            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/twitter.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/facebook.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/linkedin.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/instagram.png' style='width: 28px;'></a></div>
            <div style='color: #8095A0; font-size: 12px; margin-bottom: 20px; padding: 0px 50px;'>You are receiving this email because you signed up for Classworks.xyz. All emails are sent from Classworks systems.</div>
            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/market-google-play.png' style='height: 33px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='../../images/market-ios.png' style='height: 33px;'></a></div>
            <div style='margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);'>
               <div style='color: #8095A0; font-size: 10px; margin-bottom: 5px;'>No. 10 NTA/Choba Road, Ada Odum Complex, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</div>
               <div style='color: #8095A0; font-size: 10px;'>Copyright <?php echo date('Y') ?> ClassWorks.xyz. All rights reserved.</div>
            </div>
         </div>
      </div>
   </body>
</html>