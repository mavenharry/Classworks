<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Assessment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $resultClass->getSchoolDetails();
    $gradingSystem = $resultClass->getGradingSystem();
?>


<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                } 
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="no-print element-actions">
                                    <a data-target="#gradingSystemModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08acf0; border:#08acf0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Add New Grading System</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><strong style="font-weight: 800;">GRADING SYSTEM</strong></h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th scope="col">S/N</th>
                                                <th scope="col">Class</th>
                                                <th scope="col">From</th>
                                                <th scope="col">To</th>
                                                <th scope="col">Grade</th>
                                                <th scope="col">Remark</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach($gradingSystem as $key => $value){ ?>
                                                    <tr data-id="<?php echo $value->id; ?>">
                                                        <td><?php echo $key + 1; ?></td>
                                                        <td><?php echo ucwords($value->className); ?></td>
                                                        <td><?php echo ucwords($value->from); ?></td>
                                                        <td><?php echo ucwords($value->to); ?></td>
                                                        <td><?php echo ucwords($value->grade); ?></td>
                                                        <td><?php echo ucwords($value->remark); ?></td>
                                                        <td><button class="btn btn-primary" id="rGrade" style="width: 100px; background-color:#FB5574; border-color:#FB5574" type="button"> Delete</button></td>
                                                    </tr>
                                                <?php  }
                                            ?>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <!--END - Transactions Table-->

                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>