<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $resultClass->getSchoolDetails();
include_once ("../../includes/modals/index.php");
$academicTerm = $schoolDetails->academic_term;
$academicSession = $schoolDetails->academic_session;
$allGeneratedPins = $resultClass->allGeneratedPins($academicTerm, $academicSession);

if(isset($_POST['genPin'])){
    $academicTerm = $schoolDetails->academic_term;
    $academicSession = $schoolDetails->academic_session;
    $genPinNumber = htmlentities(trim($_POST['genPinNumber']));   
    $resultClass->generatePins($genPinNumber, $academicTerm, $academicSession);
    $allGeneratedPins = $resultClass->allGeneratedPins($academicTerm, $academicSession);
}
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#generatePin" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Generate Pins</span>
                                    </a>
                                    <a data-target="#searchStudentsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a onclick="window.print()" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL GENERATED PINS</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Pin</th>
                                                <th>Student number</th>
                                                <th>Name</th>
                                                <!-- <th>Gender</th> -->
                                                <th>Class</th>
                                                <th class="text-center">Academic Session</th>
                                                <th class="text-center">Academic Term</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allGeneratedPins) == 0){
                                                    echo "No pin generated yet";
                                                }else{
                                                    foreach($allGeneratedPins as $key => $pinDetails){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $key + 1 ?>.</span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $pinDetails->pin; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $pinDetails->studentNumber; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $pinDetails->studentName; ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <span><?php echo $pinDetails->studentClass; ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <span><?php echo $pinDetails->pinSession; ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <span><?php echo $pinDetails->pinTerm; ?></span>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allGeneratedPins) > 20){ ?>
                                                <div style="float: right;" class-"element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->
                        </div>
                    </div>
                </div>
            </div> 
                      <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->
               

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>