<?php
include_once ("resultClass.php");
$resultClass = new resultClass();

if(isset($_POST["saveGrading"])){
    $from = htmlentities(trim($_POST["from"]));
    $to = htmlentities(trim($_POST["to"]));
    $gradeClasses = $_POST["gradeClasses"];
    $remark = htmlentities(trim($_POST["remark"]));
    $grade = htmlentities(trim($_POST["grade"]));
    $reply = $resultClass->addGrading($gradeClasses, $from, $to, $remark, $grade);
    if($reply === true){
        $mess = "Grading system has been updated";
    }else{
        if($reply == "false"){
            $err = "Grading System for this classes already existed. Please verify and try again";
        }else{
            $err = "An error occurred while trying to update grading system";
        }
    }
}

if(isset($_POST["submitAssessment"])){
    $assessmentClasses = $_POST["assessmentClasses"];
    $assessmentNames = $_POST["assessmentName"];
    $assessmentMarks = $_POST["assessmentMark"];
    $reply = $resultClass->saveAssessmentSettings($assessmentClasses, $assessmentNames, $assessmentMarks);
    if($reply === true){
        $mess = "Assessment successfully updated";
    }else{
        if(empty($reply)){
            $err = "Assessment has already been added";
        }else{
            $err = "An error occurred while trying to perform this action, Please contact support";
        }
    }
}

if(isset($_POST["submitMidTerm"])){
    $partOf = htmlentities(trim($_POST["partOf"]));
    $assessmentClasses = $_POST["assessmentClasses"];
    $assessmentName = $_POST["assessmentName"];
    $assessmentMark = $_POST["assessmentMark"];

    $reply = $resultClass->addMidTerm($partOf, $assessmentClasses, $assessmentName, $assessmentMark);
    if($reply === true){
        $mess = "Mid term test successfully added";
    }else{
        if(empty($reply)){
            $err = "Assessment has already been added";
        }else{
            $err = "An error occurred while trying to perform this action, Please contact support";
        }
    }
}
?>