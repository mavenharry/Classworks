<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Assessment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $resultClass->getSchoolDetails();
    $defaultPsychomotor = $resultClass->getAllDefaultDomainName("psychomotor_domain");
    $defaultAffective = $resultClass->getAllDefaultDomainName("affective_domain");
    $psychomotor = $resultClass->getAllDomainName("psychomotor_domain");
    $affective = $resultClass->getAllDomainName("affective_domain");
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                } 
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                </div>
                                <h6 class="element-header">CONFIGURE ASSESSMENT DOMAINS</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-6">
                                    <div class="element-box">
                                        <form>
                                        <h5 class="form-header">
                                        CONFIGURE PSYCHOMOTOR DOMAINS
                                        </h5>
                                        <div class="form-desc">
                                        Easily configure all your school's psychomotor domains, turn on the domains applicable and off others.
                                        </div>

                                        <?php

                                            foreach ($defaultPsychomotor as $key => $value) { ?>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-sm-4" for=""> <?php echo ucwords($value->name)?></label>
                                                    <div class="col-sm-8">
                                                        <div class="center c-toggle-btn">
                                                            <input class="domain" data-domain="psychomotor_domain" data-id="<?php echo $value->id?>" <?php echo in_array($value->id, $psychomotor) ? "checked" : ""?> type="checkbox">
                                                            <div>
                                                                <label class="on">On</label>
                                                                <label class="off">Off</label>
                                                                <span class="c-toggle-thumb"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>
                                </div>


                                <div class="col-lg-6">

                                <div class="element-box">
                                <form>
                                <h5 class="form-header">
                                CONFIGURE AFFECTIVE DOMAINS
                                </h5>
                                <div class="form-desc">
                                Easily configure all your school's affective domains, turn on the domains applicable and off others.
                                </div>

                                <?php
                                    foreach ($defaultAffective as $key => $value) { ?>
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-4" for=""> <?php echo ucwords($value->name)?></label>
                                            <div class="col-sm-8">
                                                <div class="center c-toggle-btn">
                                                    <input class="domain" data-id="<?php echo $value->id; ?>" data-domain="affective_domain" <?php echo in_array($value->id, $affective) ? "checked" : ""?> type="checkbox">
                                                    <div>
                                                        <label class="on">On</label>
                                                        <label class="off">Off</label>
                                                        <span class="c-toggle-thumb"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </form>
                                </div>
                                </div>

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>