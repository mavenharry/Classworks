<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Management Solution | Results/Grading</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $resultClass->getSchoolDetails();
$classes = $resultClass->getClasses();
$assessmentScores = $resultClass->getAssessmentConfig("", "", 0);
$midTermState = $resultClass->getMidTermState();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

                
        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/topbar.php") ?>
        <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px;">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a class="btn-upper btn btn-sm" href="#">
                                        <select style="width:160px;font-size:1rem;  border-radius:6px; border: 2px solid #08ACF0" class="form-control form-control-sm bright" id="assessmentType">
                                            <option value="0">Assessment</option>
                                            <?php
                                                if($midTermState === true){ ?>
                                                    <option value="1">Mid Term</option>
                                                <?php }
                                            ?>
                                        </select>
                                    </a>
                                    <a class="btn-upper btn btn-sm" href="#">
                                    <select style="width:160px;font-size:1rem;  border-radius:6px; border: 2px solid #08ACF0" class="form-control form-control-sm bright" id="assessmentClass">
                                    <?php
                                        for($i = 0; $i < count($classes); $i++){ ?>
                                            <option value="<?php echo $classes[$i]->class_name; ?>"><?php echo $classes[$i]->class_name?></option>
                                    <?php    }
                                    ?>
                                    </select>  
                                    </a>
                                    <a class="btn-upper btn btn-sm" href="#">
                                    <select id="assessmentSubject" style="width:200px;font-size:1rem;  border-radius:6px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                    </select>  
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><span style="font-weight:800" id="assessmentSession"><?php echo $_GET["session"] ?></span> SESSION <span style="font-weight:800" id="assessmentTerm"><?php echo strtoupper($_GET["term"]) ?></span> TERM ASSESSMENT(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded" id="allAssessmentsAdmin">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Student name</th>
                                                <?php
                                                    if(count($assessmentScores) > 0){
                                                        foreach($assessmentScores as $key => $value){ ?>
                                                            <th><?php echo ucwords($value->name)?></th>
                                                        <?php }
                                                    }
                                                ?>
                                                <th>Total</th>
                                                <th>Remark</th>
                                                <th>Report sheet</th>
                                                </tr>
                                            </thead>
                                            <tbody id="allAssessmentsAdmin">
                                                
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                
                                <center class="error_state" style="display:none">
                                <div style="margin-top:50px;">
                                    <div>
                                    <img class="img-responsive" src="../../images/cant-find-your-glasses.png" alt="logo" width="250" height="250">
                                    </div>
                                    <div style="width:50%"><h3 style="font-size:1.5rem; margin-top:30px; color:#08ACF0">No students' assessment records found in record.</h3></div>
                                </div>
                                </center>

                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>