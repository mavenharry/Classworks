<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Results/Grading</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    
    <link href="../../css/result1.css" rel="stylesheet">
</head>
<?php
$active_student_id = base64_decode(htmlentities(trim($_GET["student_id"])));
$active_session = base64_decode(htmlentities(trim($_GET["session"])));
$active_term = base64_decode(htmlentities(trim($_GET["term"])));
$schoolDetails = $resultClass->getSchoolDetails();
$studentDetails = $resultClass->getStudentDetails($active_student_id);
$studentAssessment = $resultClass->fetchAllStudentAssessmentsParent($active_student_id, $active_session, $active_term);
$gradingSystem = $resultClass->getClassGradingSystem($studentDetails->present_class);
$psychomotor_domain_names = $resultClass->getDomainName("psychomotor_domain");
$affective_domain_names = $resultClass->getDomainName("affective_domain");
$assessmentConfig = $resultClass->getAssessmentConfig($active_student_id, "studentNumber", 0);
switch($active_term){
    case 1:
        $termName = "1ST TERM";
        break;
    case 2:
        $termName = "2ND TERM";
        break;
    case 3:
        $termName = "3RD TERM";
        break;
    default:
        $termName = "";
        break;
}
?>

<body style="background: #e8ebee;" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <div class="content-w">

<div class="container" style="margin-bottom: 70px;">

<div class="row" style="margin-top:30px; margin-bottom:20px; margin-right:-20px;">
<div class="pull-right" style="float:right; margin-right:60px;">
<a onclick="window.print()" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
    <i class="os-icon os-icon-newspaper"></i>
    <span style="font-size: 1.5rem;">Print This Document</span>
</a>
</div>
</div>

<div class="row logo-bg">
<div class="ui-7">
    <div class="row" style="margin-top: 30px;">
        <div class="col-xs-2 llogo">
            <img class="img-responsive" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" alt="logo" width="120" height="120">
        </div>
        <div class="col-xs-8 font-400">
            <p class="header top">
                <span class="schname" style="color: #08ACF0"><?php echo strtoupper($schoolDetails->school_name) ?></span>
            </p>
            <address class="top" style="color: #8095A0"><?php echo strtoupper($schoolDetails->school_address)." ".strtoupper($schoolDetails->city)." ".strtoupper($schoolDetails->state)." ".strtoupper($schoolDetails->country) ?></address>
            <p class="fa fa-2x top stype" style="color: #8095A0">EMAIL: <?php echo strtoupper($schoolDetails->email) ?>    <span style="margin-left:10px;"></span> PHONE: <?php echo $schoolDetails->phone ?></p>
            <p class="fa fa-2x top reptitle" style="color: #8095A0; font-style:italic"><?php echo strtoupper($schoolDetails->slogan) ?></p>            
            <p class="fa fa-2x top reptitle" style="color: #08ACF0; margin-top:10px"><?php echo $termName; ?> REPORT</p>
        </div>
        <div class="col-xs-2 rlogo">
        <?php if(!empty($studentDetails->profilePhoto)){ ?>
            <img src="<?php echo $schoolDetails->pathToPassport."/".$studentDetails->profilePhoto;?>" class="img-thumbnail img-responsive pull-right" alt="logo" width="100" height="100">
            <?php }else{ ?>
                <img src="../../images/male-user-profile-picture.png" class="img-thumbnail img-responsive pull-right" alt="logo" width="100" height="100">
            <?php } ?>
        </div>
    </div>
    <br>

    <div class="row font-400">
        <div class="col-xs-4 f1">
            <ul>
                <li>
                    Name: <span id="sname"><?php echo strtoupper($studentDetails->fullname); ?></span>
                </li>
                <li>
                    Student's Class: <span id="sclass"><?php echo strtoupper($studentDetails->present_class); ?></span>
                </li>
                <li>
                    Student's Session: <span id="sesterm"><?php echo $active_session; ?></span>
                </li>
                <li>
                    Student's Term: <span id=""><?php echo $active_term; ?></span>
                </li>
            </ul>
        </div>
        <div class="col-xs-4 f2">
            <ul>
                <li>
                    Student's Gender: <span id="ssex"><?php echo strtoupper($studentDetails->gender); ?></span>
                </li>
                <li>
                    Best Subject: <span id="snoinclass"><?php echo strtoupper($resultClass->getSubjectName($resultClass->getsubjectPerformance("max", $active_student_id, $active_session, $active_term))) ?></span>
                </li>
                <li>
                    Least Subject: <span id="scavg"><?php echo strtoupper($resultClass->getSubjectName($resultClass->getsubjectPerformance("min", $active_student_id, $active_session, $active_term))) ?></span>
                </li>
                <?php 
                if(strtolower($schoolDetails->grading) == "percentage"){ ?>
                   <li>
                    Performace Rate: <span id="ssavg"><?php echo $resultClass->getOverallPerformance("percentage", $active_student_id, $active_session, $active_term) ?>%</span>
                    </li>
                <?php }else{
                if(strtolower($schoolDetails->grading) == "grade"){ ?>
                    <li>
                    Overall Grading: <span id="ssavg"><?php echo strtoupper($resultClass->getOverallPerformance("grade", $active_student_id, $active_session, $active_term)) ?></span>
                    </li>
                <?php }else{
                if(strtolower($schoolDetails->grading) == "position"){ ?>
                    <li>
                    Class Ranking: <span id="ssavg">1 Out of <?php echo count($resultClass->getallClassStudents($studentDetails->present_class)); ?></span>
                    </li>
                <?php }
                }
                }
                ?>
            </ul>
        </div>
        <div class="col-xs-4 f3">
            <ul>
                <li>
                    School Performance: <span id="genperf">0%</span>
                </li>
                <li>
                    Class Performance: <span id="gperf">0%</span>
                </li>
                <li>
                    Termly Performance: <span id="sperf">0% | Upward</span>
                </li>
                <li>
                    Session Performance: <span id="bperf">0% | Downward</span>
                </li>
            </ul>
        </div>
    </div>
    <br>

    <?php
        if(count($studentAssessment) > 0){ ?>
    <div class="row">
        <div style="float:left;width:415pt;padding-left:10px">
            <table class="cogtb table-header-rotated">
                <caption>
                    Cognitive Domain
                </caption>
                    <thead class="ctitle">
                        <tr>
                        <th class="subtitle text-center"> S/N</th>                        
                        <th class="row-header subtitle"> ALL OFFERED SUBJECTS</th>
                        <?php
                            if(count($assessmentConfig) > 0){ 
                                foreach($assessmentConfig as $key => $value){ ?>
                                    <th class="rotate-45">
                                        <div><span><?php echo strtoupper($value->name)."(".$value->mark."%)";?></span></div>
                                    </th>
                                <?php } 
                                ?>
                            <?php }
                        ?>
                        <!-- <th class="rotate-45">
                            <div><span> FINAL <br> EXAMINATION (60%)</span></div>
                        </th>
                        <th class="rotate-45 ca-th">
                            <div><span> CONTINUOUS ASSESSMENT 1 (10%)</span></div>
                        </th>
                        <th class="rotate-45">
                            <div><span> CONTINUOUS ASSESSMENT 2 (10%)</span></div>
                        </th>
                        <th class="rotate-45">
                            <div><span> CONTINUOUS ASSESSMENT 3 (10%)</span></div>
                        </th>
                        <th class="rotate-45">
                            <div><span> ASSIGNMENTS (10%)</span></div>
                        </th> -->
                        <th class="rotate-45 ca-th">
                            <div><span> TOTAL </span></div>
                        </th>
                        <th class="rotate-45">
                        <div><span class="rema"> SUBJECT'S RANKING</span></div>
                        </th>
                        <th class="rotate-45">
                        <div><span class="rema"> SUBJECT'S AWARDED GRADE</span></div>
                        </th>
                        <th class="rotate-45">
                        <div><span class="rema"> SUBJECT TUTOR'S REMARK</span></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="ccontent">
                        <?php 
                            for($i = 0; $i < count($studentAssessment); $i++){ ?>
                            <tr>
                                <th class="text-center"><?php echo $i+1 ?></th>                                
                                <th><?php echo strtoupper($resultClass->getSubjectName($studentAssessment[$i]->subject_code)) ?></th>
                                <?php
                                    if(count($studentAssessment[$i]->myAssessment) > 0){
                                        foreach($studentAssessment[$i]->myAssessment as $key => $value){ 
                                            $total = $value->total; ?>
                                            <td><?php echo $value->score; ?></td>
                                        <?php }
                                    }
                                ?>
                                <td><?php echo $total; ?></td>
                                <td><?php echo strtoupper($studentAssessment[$i]->grade) ?></td>
                                <td><?php echo ucwords($studentAssessment[$i]->remark) ?></td>
                            </tr>
                        <?php } ?>
                        
                    </tbody>
            </table>
            <div class="row font-400" style="padding-top: 5px; padding-bottom: 10px;">
                <div class="col-sm-12">

                    <strong style="font-size:18px" class="spos">
                    </strong>
                </div>
                    <strong style="font-size:18px" class="spos">
                    </strong>
            </div>

                    <strong style="font-size:18px" class="spos">
                        <table class="cogtb" style="padding-top: 5px;">
                            <caption>
                                <i class="fa ion-chatbubble-working"></i> Overall Comment
                            </caption>
                            <tbody>
                            
                                <td class="comment" id="comment">
                                    <h2 style="margin:0px;"><strong></strong></h2>
                                    <p style="margin:0px;">
                                        <?php
                                            if(count($studentAssessment) == 0){ ?>
                                                <textarea readonly id="resultComment" style="border:none; height:100%; width:100%; padding:10px; font-weight:400; font-size:1.5rem" placeholder="Click here to describe student Performance..." rows="5"></textarea>
                                            <?php }else{ ?>
                                                <textarea readonly id="resultComment" style="border:none; height:100%; width:100%; padding:10px; font-weight:400; font-size:1.5rem" placeholder="Click here to describe student Performance..." rows="5"><?php echo ucfirst($studentAssessment[0]->comment) ?></textarea>
                                            <?php }
                                        ?>
                                    </p>
                                </td>
                            
                            </tbody>
                        </table>
                    </strong>

                <div class="row_none" style="padding-top: 20px; margin-bottom:200px">
                    <div class="col-xs-6">
                        <table class="kcogtb table-header-rotated">
                            <caption>
                                <i class="fa fa-key"></i> Cognitive Keys
                            </caption>
                            <tbody>
                            <tr>
                                <td class="comment" style="text-align: inherit;">
                                    <?php for($i = 0; $i < count($gradingSystem); $i++){ ?>
                                    <span><?php echo $gradingSystem[$i]->from." - ".$gradingSystem[$i]->to." : ".strtoupper($gradingSystem[$i]->grade)." (".ucfirst($gradingSystem[$i]->remark).")" ?></span><br>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-6 font-400 f-12">
                        <div>
                            <p>
                                Next Term Begins: <span id="sdate">
                                    <?php echo date('d F Y', strtotime($schoolDetails->next_resumption_date)) ?>
                                    </span>
                            </p>
                            <p>
                                Number Of Subject(s):
                                <span class="pname">
                                <?php echo count($studentAssessment) ?>
                                </span>
                            </p>
                            <p>
                                Number Of Tutor(s):
                                <span class="pname">
                                <?php echo count($resultClass->getOverallAssessmentTutors($active_student_id, $active_session, $active_term)) ?>
                                </span>
                            </p>
                            <p>
                                Prepared By:
                                <span class="pname">
                                <?php echo ucwords(implode(", ", $resultClass->getOverallAssessmentTutors($active_student_id, $active_session, $active_term))) ?>
                                </span>
                            </p>

                            <p>
                                Signed By: <span>Management</span>
                            </p>
                        </div>
                    </div>
                </div>
            </strong></div>
        <strong style="font-size:18px" class="spos">
            <div style="float:right;width:182.43pt;padding-right:10px">
                <table class="psytb">
                    <caption>
                        <i class="fa ion-android-hand"></i> Psychomotor Domain
                    </caption>
                    <thead>
                    <tr>
                        <th rowspan="2">Skills</th>
                        <th colspan="5" class="text-center">Ratings</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">5</th>
                        <th style="text-align: center;">4</th>
                        <th style="text-align: center;">3</th>
                        <th style="text-align: center;">2</th>
                        <th style="text-align: center;">1</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php for($i = 0; $i < count($psychomotor_domain_names); $i++){ ?>
                        <tr>
                        <th><?php echo ucwords($psychomotor_domain_names[$i]->name) ?></th>
                        <?php for($j = 5; $j > 0; $j--){
                        $psychomotor_domain = $resultClass->getDomain($psychomotor_domain_names[$i]->domain, $active_session, $active_term, $active_student_id, $psychomotor_domain_names[$i]->id, $j);
                        if(count($psychomotor_domain) > 0){ ?>                        
                            <td><input name="<?php echo str_replace(" ", "_", trim($psychomotor_domain_names[$i]->name)) ?>" type="radio" class="option-inputresult checkbox" checked/></td>
                        <?php }else{ ?>
                            <td><input name="<?php echo str_replace(" ", "_", trim($psychomotor_domain_names[$i]->name)) ?>" type="radio" readonly class="option-inputresult"/></td>                            
                        <?php } }?>
                        </tr>
                        <?php } ?>
                    </tbody>
                    </table>
                <br>
                <table class="afftb" style="margin-bottom: 50px;">
                    <caption>
                        <i class="fa ion-happy"></i> Affective Domain
                    </caption>
                    <thead>
                    <tr>
                        <th rowspan="2">Behaviours</th>
                        <th colspan="5" class="text-center">Ratings</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">5</th>
                        <th style="text-align: center;">4</th>
                        <th style="text-align: center;">3</th>
                        <th style="text-align: center;">2</th>
                        <th style="text-align: center;">1</th>
                    </tr>
                    <tbody>
                        <?php for($i = 0; $i < count($affective_domain_names); $i++){ ?>
                        <tr>
                        <th><?php echo ucwords($affective_domain_names[$i]->name) ?></th>
                        <?php for($j = 5; $j > 0; $j--){
                        $affective_domain = $resultClass->getDomain($affective_domain_names[$i]->domain, $active_session, $active_term, $active_student_id, $affective_domain_names[$i]->id, $j);
                        if(count($affective_domain) > 0){ ?>                        
                            <td><input name="<?php echo str_replace(" ", "_", trim($affective_domain_names[$i]->name)) ?>" type="radio" class="option-inputresult checkbox" checked/></td>
                        <?php }else{ ?>
                            <td><input name="<?php echo str_replace(" ", "_", trim($affective_domain_names[$i]->name)) ?>" type="radio" class="option-inputresult checkbox"/></td>                            
                        <?php } }?>
                        </tr>
                        <?php } ?>
                    </tbody>
                    </thead>
                    </table>
            </div>
        </strong>
    </div>
        <?php }else{ ?>
            <h5>No record found for this student</h5>
        <?php } ?>
    <!-- ENDS HERE -->
    <strong style="font-size:18px" class="spos">
    </strong></div>
</div>
<strong style="font-size:18px" class="spos">
</strong>
<strong style="font-size:18px" class="spos"></strong>
<footer class="end-report"></footer>
    
</div>

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>