<?php include_once ("resultbtn.php");

$schoolDetails = $resultClass->getSchoolDetails();
$classes = $resultClass->getClasses();
$pinDetails = $resultClass->getPinState();
if(isset($_GET["student_id"]) && isset($_GET["session"]) && isset($_GET["term"])){
  $studentNumber = htmlentities(trim($_GET["student_id"]));
  $term = htmlentities(trim($_GET["term"]));
  $session = htmlentities(trim($_GET["session"]));
  $studentDetails = $resultClass->getStudentDetails($studentNumber);
}

?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo ucwords($schoolDetails->school_name); ?> - Online Result Checker</title>
<!-------------------- START - Meta -------------------->
<?php include("../../includes/meta.php") ?>
<!-------------------- END - Meta -------------------->
<link href="resultChecker/css/index.css" rel="stylesheet">
<link href="../../css/main.css" rel="stylesheet">
<?php include_once ("../../includes/styles.php"); ?>
</head>

<body>
        <div class="categories">

        <div class="category-block">
          <div class="category-icon-cont">

            <div class="icon"><img style="margin-top: -20px; margin-bottom: 20px; width: 140px; object-fit: scale-down;" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>"></div>

            <h2 style="color: #09acef; font-size: 1.8rem; margin-bottom:0px; margin-top:0px;" ><?php echo ucwords($schoolDetails->school_name); ?></h2>
            <h2 style="color: #b7b7b7; font-weight:normal; font-style: italic; font-size: 1rem;" ><?php echo ucwords($schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country); ?></h2>

            <h2 style="color: #8095A0; font-size: 1.5rem; margin-top:10px; margin-bottom:10px;" >Online Result Checker</h2>            
           <!-- RESULT CHECKER -->
        <div id="checkResult">
          <form>
            <select class="pin" id="resultSession" name="class" >
                <option value="">Session</option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>   
            </select>
            <select class="pin" id="resultTerm" name="class" >
              <option value="">Term</option>
              <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
              <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
              <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option>     
            </select>
            <select class="pin" id="resultClass" name="class" >
              <option value="">Select Class</option>
              <?php
                foreach($classes as $key => $class){ ?>
                  <option value="<?php echo $class->class_name; ?>"><?php echo ucwords($class->class_name); ?></option>
                <?php }
              ?>   
            </select>
            <select class="pin" id="resultStudentNumber" name="class" >
              <option value="">Select student</option>  
            </select>
            <?php
              if($pinDetails->pinChecker == 1){ ?>
                <div style="margin-top: 0px;">
                  <input style="text-transform:uppercase; font-size:1.05rem;" class="pin" type="text" id="verifypin" name="verifypin" placeholder="Enter Checker Pin">         
                </div>  
              <?php }
            ?>
              <button name="showResult" id="showResult" class="button"><span>Check Result Now </span></button>
          </form>
          <?php
            if($pinDetails->pinChecker == 1){ ?>
              <div style="display:inline;">
                <button style="margin-top:10px; font-size:1rem; color: #8095A0; border:none; background:transparent">Don't have a checker pin?</button>          
                <button id="purchasePin" style="margin-top:10px; border-radius:3px; background-color:#fcaa28; color: #fff; padding-right: 15px; padding-left: 15px; padding-bottom: 5px; padding-top: 5px; border: none; transition: all 0.5s; cursor: pointer;">Purchase Pin</button>
              </div>
            <?php }
          ?>
        </div>
        <!-- RESULT CHECKER ENDS HERE -->

         <!-- PURCHASE PIN HERE -->
          <div id="pinDiv" style="display: none">
            <span id="pinDisplay" style="color: #8095A0; font-size: 1.2rem; font-weight: bold;"></span>
            <form>
                <div style="margin-top: 0px;">
                  <input style="text-transform:none; font-size:1.05rem;" class="pin" type="email" id="pinEmail" name="studentNumber" placeholder="Please Enter Your Email">         
                </div>
                <button name="payPin" id="payPin" class="button"><span>Proceed </span></button>
            </form>
              <div style="display:inline;">
                <button style="margin-top:10px; font-size:1rem; color: #8095A0; border:none; background:transparent">Have a checker pin?</button>          
                <button id="havePin" style="margin-top:10px; border-radius:3px; background-color:#fcaa28; color: #fff; padding-right: 15px; padding-left: 15px; padding-bottom: 5px; padding-top: 5px; border: none; transition: all 0.5s; cursor: pointer;">Check Result</button>
              </div>
          </div>
          <!-- PURCHASE PIN ENDS HERE -->
          </div>
        </div>

</body>
<?php include_once ("../../includes/scripts.php"); ?>
</html>