<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Assessments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $resultClass->getSchoolDetails();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-panel-toggler" style="margin-top:10px;">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">ASSESSMENTS AND RESULTS MANAGEMENT/ANALYSIS</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#assessmentSwitch" data-toggle="modal" href="" style="border: 0px dashed #08ACF0">
                                                    <div class="label">View all students assessments</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-pencil-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Results/Grading</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#" data-target="#classSheet" data-toggle="modal" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Class Termly Report Sheet</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Class Sheet</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#" data-target="#assessmentSelect" data-toggle="modal" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Record Assessment for Students</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Record Scores</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>
                                            
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./configuration.php" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Configure report sheet</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-pencil-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Configure Result</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#publishResultModal" data-toggle="modal" href="" style="border: 2px dashed #08ACF0">
                                                    <div class="label">Publish all results to parents</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-emoticon-smile"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Publish Results</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./grading_system" style="border: 2px dashed #08ACF0">
                                                    <div class="label">No. of all current students</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user-male-circle"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Grading System</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#stampUploadModal" data-toggle="modal" href="" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Upload School Stamp</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-fingerprint"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">School Stamp</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./configurAssessment.php" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Configure assessment</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-settings"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Configuration</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#switchResultPrint" data-toggle="modal" href="" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Print student result</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-settings"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Print Result</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="generatedPins.php" style="border: 0px dashed #08ACF0">
                                                    <div class="label">View generated pins</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-settings"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Generated Pins</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
<script>
    var selectedTimetableClass = $("#timetableClassSelector").val();
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableClass: selectedTimetableClass},
        cache: false,
        success: function(results){
            $(".timeTableSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
</script>
</body>

</html>