<?php
include ("resultClass.php");
$resultClass = new resultClass();

$schoolDetails = $resultClass->getSchoolDetails();

if(isset($_GET["staffClass"])){
    echo $staffClass = htmlentities(trim($_GET["staffClass"]));
    $reply = $resultClass->fetchAllClassSubject($staffClass);
    if(count($reply) > 0){ ?>
        <option value="">Select Subject</option>
        <?php for($i = 0; $i < count($reply); $i++){ ?>
            <option value="<?php echo $reply[$i]->subjectCode; ?>"><?php echo ucfirst($reply[$i]->subjectName); ?></option>
        <?php   }
    }else{ ?>
            <option disabled selected value="">No subjects for this class.</option>            
    <?php }
}

if(isset($_GET["assessmentClass"]) && isset($_GET["assessmentSubject"])){
    $assessmentClass = htmlentities(trim($_GET["assessmentClass"]));
    $assessmentSubject = htmlentities(trim($_GET["assessmentSubject"]));
    $part = htmlentities(trim($_GET["partToSelect"]));
    $session = "";
    $term = "";
    $assessmentScores = $resultClass->getAssessmentConfig($assessmentClass, "classCode", $part);
    $reply = $resultClass->fetchAllAssessments($assessmentClass, $assessmentSubject, $session, $term, $part); 
    echo $assessmentSubject;?>
    <thead>
        <tr>
        <th>S/N</th>
        <th>Passport</th>
        <th>Student name</th>
        <?php
            if(count($assessmentScores) > 0){
                foreach($assessmentScores as $key => $value){ ?>
                    <th><?php echo ucwords($value->name)."(".$value->mark."%)"?></th>
                <?php }
            }
        ?>
        <th>Total</th>
        <th>Action</th>
        </tr>
    </thead><?php echo count($reply);?>
    <?php 
    for($i = 0; $i < count($reply); $i++){ ?>
        <tr class="my_hover_up">
        <td><span><?php echo $i+1 ?>.</span></td>
        <td>
        <?php if(!empty($reply[$i]->profilePhoto)){ ?>
        <div class="user-with-avatar">
        <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$reply[$i]->profilePhoto;?>">
        </div>
        <?php }else{ ?>
            <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
            class="badge"><?php echo $nameInitials = $resultClass->createAcronym($reply[$i]->fullname); ?></a>
        <?php } ?>
        </td>
        <td><span><?php echo strtoupper($reply[$i]->fullname) ?></span></td>
        <?php
            if(count($reply[$i]->myAssessment) > 0){
                foreach($reply[$i]->myAssessment as $key => $value){ ?>
                    <td><span><input id="scoresInput" data-student="<?php echo $reply[$i]->student_no; ?>" data-subject="<?php echo $reply[$i]->subject_code; ?>" data-assName="<?php echo $value->assCode; ?>" data-row="<?php echo $i + 1?>" value="<?php echo $value->score ?>"  placeholder="0" style="padding-left:10px; border-radius:4px; text-align:left; color:#8095A0; border: 1px solid #8095A0; height:40px; margin:0px; width:120px" type="number"/></span></td>
                <?php }
            }
        ?>
        <td><span id="total<?php echo $i+1 ?>"><?php echo $reply[$i]->total ?></span></td>
        <td class="text-center">
            <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF" class="my_hover_up_bg badge" href="#<?php echo $reply[$i]->student_no?>">Save Record</a>
        </td>
        </tr>
    <?php   }
}


if(isset($_GET["student_no"]) && isset($_GET["subject_code"]) && isset($_GET["column_name"]) && isset($_GET["input_val"])){
    $student_no = htmlentities(trim($_GET["student_no"]));
    $subject_code = htmlentities(trim($_GET["subject_code"]));
    $column_name = htmlentities(trim($_GET["column_name"]));
    $input_val = htmlentities(trim($_GET["input_val"]));
    $part = htmlentities(trim($_GET["pathToUpdate"]));
    $term = htmlentities(trim($_GET["term"]));
    $session = htmlentities(trim($_GET["session"]));
    $reply = $resultClass->updateSubjectAssessment($student_no, $subject_code, $column_name, $input_val, $term, $session, $part);
    echo $reply->updatedVal;
}


if(isset($_GET["domain"]) && isset($_GET["session"]) && isset($_GET["term"]) && isset($_GET["student_no"]) && isset($_GET["attr_id"]) && isset($_GET["value"])){
    $domain = htmlentities(trim($_GET["domain"]));
    $session = htmlentities(trim($_GET["session"]));
    $term = htmlentities(trim($_GET["term"]));
    $student_no = htmlentities(trim($_GET["student_no"]));
    $attr_id = htmlentities(trim($_GET["attr_id"]));
    $value = htmlentities(trim($_GET["value"]));
    $reply = $resultClass->setDomain($domain, $session, $term, $student_no, $attr_id, $value);
    //echo $reply->updatedVal;
}

if(isset($_GET["resultComment"]) && isset($_GET["session"]) && isset($_GET["term"]) && isset($_GET["student_no"])){
    $comment = htmlentities(trim($_GET["resultComment"]));
    $session = htmlentities(trim($_GET["session"]));
    $term = htmlentities(trim($_GET["term"]));
    $student_no = htmlentities(trim($_GET["student_no"]));
    $reply = $resultClass->updateResultComment($comment, $session, $term, $student_no);
    //echo $reply->updatedVal;
}

if(isset($_GET["subjectClass"])){
    $subjectClass = htmlentities(trim($_GET["subjectClass"]));
    $reply = $resultClass->getClassSubject($subjectClass);
    if(count($reply) > 0){ ?>
        <option value="">Select Subject</option>
        <?php for($i = 0; $i < count($reply); $i++){ ?>
            <option value="<?php echo $reply[$i]->subjectCode; ?>"><?php echo ucfirst($reply[$i]->subjectName); ?></option>
        <?php   }
    }else{ ?>
            <option disabled selected value="">No subjects for this class.</option>            
    <?php }
}


if(isset($_GET["newDomainState"]) && isset($_GET["DomainNameId"]) && isset($_GET["motorType"])){
    $newDomainState = htmlentities(trim($_GET["newDomainState"]));
    $DomainNameId = htmlentities(trim($_GET["DomainNameId"]));
    $motorType = htmlentities(trim($_GET["motorType"]));
    $reply = $resultClass->updateDomainName($newDomainState, $DomainNameId, $motorType);
}


if(isset($_GET["assessmentClassAdmin"]) && isset($_GET["assessmentSubjectAdmin"])){
    $assessmentClass = htmlentities(trim($_GET["assessmentClassAdmin"]));
    $assessmentSubject = htmlentities(trim($_GET["assessmentSubjectAdmin"]));
    $assessmentClassCodeFromName = $resultClass->getClassCode($assessmentClass);
    $session = htmlentities(trim($_GET["assessmentSession"]));
    $term = htmlentities(trim($_GET["assessmentTerm"]));
    $part = htmlentities(trim($_GET["assessmentType"]));
    $assessmentScores = $resultClass->getAssessmentConfig($assessmentClassCodeFromName, "classCode", $part);
    $reply = $resultClass->fetchAllAssessments($assessmentClassCodeFromName, $assessmentSubject, $session, $term, $part);
    if(count($reply) < 1){
        echo "none";
    }else{ ?>
        <thead>
        <tr>
        <th>S/N</th>
        <th>Passport</th>
        <th>Student name</th>
        <?php
            if(count($assessmentScores) > 0){
                foreach($assessmentScores as $key => $value){ ?>
                    <th><?php echo ucwords($value->name). "(".$value->mark."%)"?></th>
                <?php }
            }
        ?>
        <th>Total</th>
        <th>Remark</th>
        <th>Report sheet</th>
        </tr>
    </thead> 
        <?php 
        for($i = 0; $i < count($reply); $i++){ ?>
            <tr class="my_hover_up">
            <td><span><?php echo $i+1 ?>.</span></td>
            <td>
            <?php if(!empty($reply[$i]->profilePhoto)){ ?>
            <div class="user-with-avatar">
            <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$reply[$i]->profilePhoto;?>">
            </div>
            <?php }else{ ?>
                <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                class="badge"><?php echo $nameInitials = $resultClass->createAcronym($reply[$i]->fullname); ?></a>
            <?php } ?>
            </td>
            <td><span><?php echo $reply[$i]->fullname ?></span></td>

            <?php
                if(count($reply[$i]->myAssessment) > 0){
                    foreach($reply[$i]->myAssessment as $key => $value){ ?>
                        <td><span><?php echo $value->score ?></span></td>
                    <?php }
                }
            ?>
            <td><span><?php echo $reply[$i]->total ?></span></td>
            <td><span><?php echo isset($reply[$i]->remark) ? ucfirst($reply[$i]->remark) :"-" ?></span></td>
            <td class="text-center">
            <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
            class="my_hover_up_bg badge" href="./report_sheet?student_id=<?php echo $reply[$i]->student_no ?>&session=<?php echo $session ?>&term=<?php echo $term ?>&part=<?php echo $part?>">View report</a>
            </td>
            </tr>
        <?php   }
    }
}

if(isset($_GET["newInput"])){ ?>
    <div class="form-group row timingDiv">
        <div class="col-sm-6">
            <input class="form-control" id="setName" name="assessmentName[]" placeholder="E.g 1st Assessment" type="text">
        </div>
        <div class="col-sm-6">
            <input class="form-control" id="setMark" name="assessmentMark[]" placeholder="%. E.g 10" type="number">
        </div>
        <!-- <div class="col-sm-2">
            <button class="btn btn-primary" id="rTiming" style="background-color:#FB5574; border-color:#FB5574" type="button"> Delete</button>
        </div> -->
    </div>
<?php }

if(isset($_GET["delAss"])){
    $delAss = htmlentities(trim($_GET["delAss"]));
    $reply = $resultClass->deleteAssessmentRow($delAss);
    echo $reply;
}

if(isset($_GET["action"]) && $_GET["action"] == "editAssessment"){
    $assName = htmlentities(trim($_GET["assName"]));
    $assMark = htmlentities(trim($_GET["assMark"]));
    $assId = htmlentities(trim($_GET["assId"]));
    $reply = $resultClass->updateAssessmentDetails($assName, $assMark, $assId);
    echo $reply;
}

if(isset($_GET["action"]) && $_GET["action"] == "publishResult"){
    $classes = isset($_GET["classes"]) && !empty($_GET["classes"] )? $_GET["classes"]: [];
    $pinVending = htmlentities(trim($_GET["vending"]));
    $amount = htmlentities(trim($_GET["pinAmount"]));
    $reply = $resultClass->publishResultState($classes, $pinVending, $amount);
    echo $reply;
}

if(isset($_GET["action"]) && $_GET["action"] == "deleteGrade"){
    $gradeId = htmlentities(trim($_GET["gradeId"]));
    $reply = $resultClass->deleteGrade($gradeId);
    echo $reply;
}

if(isset($_GET["action"]) && $_GET["action"] == "checkResult"){
    $pin = htmlentities(trim($_GET["userPin"]));
    $studentNumber = htmlentities(trim($_GET["studentNumber"]));
    $resultSession = htmlentities(trim($_GET["resultSession"]));
    $resultTerm = htmlentities(trim($_GET["resultTerm"]));
    $reply = $resultClass->verifyCheckerDetails($pin, $studentNumber, $resultSession, $resultTerm);
    echo json_encode($reply);
}

if(isset($_GET["action"]) && $_GET["action"] == "getName"){
    $studentNumber = htmlentities(trim($_GET["studentNumber"]));
    $reply = $resultClass->getStudentDetails($studentNumber);
    if($reply == "false"){
        echo $reply;
    }else{
        echo $reply->fullname;
    }
}

if(isset($_GET["action"]) && $_GET["action"] == "getSubaccountCode"){
    $obj = new stdClass();
    $obj->accountCode = $schoolDetails->subAccount;
    $obj->pinAmount = $schoolDetails->pinAmount;
    $obj->schoolName = $schoolDetails->school_name;
    $obj->pkey = $schoolDetails->keys["paystack_public"];
    echo json_encode($obj);
}

if(isset($_GET["action"]) && $_GET["action"] == "generatePin"){
    $session = $schoolDetails->academic_session;
    $term = $schoolDetails->academic_term;
    $reply = $resultClass->generatePins(1, $term, $session);
    echo $reply[0];
}

if(isset($_GET["action"]) && $_GET["action"] == "getClassStudent"){
    $className = htmlentities(trim($_GET["className"]));
    $reply = $resultClass->getallClassStudents($className);
    if(count($reply) > 0){?>
        <option value="">Select student</option>
        <?php foreach($reply as $key => $value){ ?>
            <option value="<?php echo $value->student_number; ?>"><?php echo ucwords($value->fullname);?></option>
        <?php }
    }else{ ?>
        <option value="">No student found</option>
    <?php }
}
?>