<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Management Solution | Results/Grading</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$part = htmlentities(trim($_GET["part"]));
$schoolDetails = $resultClass->getSchoolDetails();
$term = isset($_GET['term']) ? htmlentities(trim($_GET['term'])) : $schoolDetails->academic_term;
$session = isset($_GET['session']) ? htmlentities(trim($_GET['session'])) : $schoolDetails->academic_session;

if($_SESSION["role"] == "admin"){
    $allTutorClass = $resultClass->fetchAllAdminClass();
}else{
    $allTutorClass = $resultClass->fetchAllTutorClass();
}
$assessmentScores = $resultClass->getAssessmentConfig("", "", $part);

if(isset($_GET["status"]) && $_GET["status"] == 1){
    $reply = $resultClass->generateStudentsAssessment($term, $session, $part);
}

?>
    <style>
        /* Center the loader */
        #loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #08acf0;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }

        /* Add animation to "page content" */
        .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
        from { bottom:-100px; opacity:0 } 
        to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom { 
        from{ bottom:-100px; opacity:0 } 
        to{ bottom:0; opacity:1 }
        }

        #myDiv {
        display: none;
        }
    </style>
    <div id="loader"></div>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all" style="display:none;" id="myDiv" class="animate-bottom">

        <div class="layout-w">

            <div class="content-w">

                
        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/topbar.php") ?>
        <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px;">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                <input value="<?php echo $part?>" id="partOf" style="display:none">
                                    <a class="btn-upper btn btn-sm" href="#">
                                    <select style="width:200px;font-size:1rem;  border-radius:6px; border: 2px solid #08ACF0" id="staffSelectedClass" class="form-control form-control-sm bright">
                                    <?php
                                    if(count($allTutorClass) == 0){
                                        echo "No class(es) assigned yet)";
                                    }else{ ?>
                                        <option value="">Select Class</option>
                                     <?php for($i = 0; $i < count($allTutorClass); $i++){ ?>
                                        <option value="<?php echo strtoupper($allTutorClass[$i]->classCode) ?>"><?php echo strtoupper($allTutorClass[$i]->className) ?></option>
                                    <?php }} ?>
                                    </select>  
                                    </a>
                                    <a class="btn-upper btn btn-sm" href="#">
                                    <select id="allClassSubjects" style="width:200px;font-size:1rem;  border-radius:6px; border: 2px solid #08ACF0; margin-left:-10px;" class="form-control form-control-sm bright">
                                    </select>  
                                    </a>
                                    <a onclick="window.print()" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                    <span style="margin-left: 20px; margin-right: 5px;">Incomplete students?</span>
                                    <a style="color:#FFFFFF; background-color:#1BB4AD; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="?status=1&part=<?php echo $part?>">
                                        <i class="os-icon os-icon-users"></i>
                                        <span style="font-size: .9rem;">Click Here to Update</span>
                                    </a>
                                </div>
                                <h6 class="element-header">STUDENT(S) GRADES/ASSESSMENT</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <input type='hidden' name="editAssessmentTerm" value="<?php echo $term; ?>" readonly>
                                        <input type='hidden' name="editAssessmentSession" value="<?php echo $session; ?>" readonly>
                                        <table class="table table-padded" id="allAssessments">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Student name</th>
                                                <?php
                                                    if(count($assessmentScores) > 0){
                                                        foreach($assessmentScores as $key => $value){ ?>
                                                            <th><?php echo ucwords($value->name)."(".$value->mark."%)"?></th>
                                                        <?php }
                                                    }
                                                ?>
                                                <th>Total</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


    <!-------------------- START - Top Bar -------------------->
    <?php include("../../includes/support.php") ?>
    <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>