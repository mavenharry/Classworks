<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $resultClass->getSchoolDetails();
    $assessmentScores = $resultClass->getAssessmentConfig("all", "all", 0);
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#newMidTermModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#08acf0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Add Mid Term Scores</span>
                                    </a>
                                    <a data-target="#newAssessmentModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08acf0; border:#08acf0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Add New Assessment Scores</span>
                                    </a>
                                </div>
                                <h6 class="element-header">CONFIGURE ASSESSMENT SCORES</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-12">

                                <div class="element-box">

                                <h5 class="form-header">
                                SET ASSESSMENT SCORES
                                </h5>
                                <div class="form-desc">
                                Easily configure your school's assessment sheet with their individual scores .
                                </div>
                                <form action="" method="post">
                                    <?php
                                        if(count($assessmentScores) == 0){

                                        }else{
                                            foreach ($assessmentScores as $key => $value) { ?>
                                                <!-- # code... -->
                                                <div class="form-group row assessmentRow" data-id="<?php echo $value->id; ?>">
                                                    <div class="col-sm-3">
                                                        <input class="form-control" id="assessmentName" name="assessmentNames[]" value="<?php echo ucwords($value->name); ?>" type="text">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input class="form-control" id="assessmentMark" name="assessmentMarks[]" value="<?php echo $value->mark; ?>" type="number">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <input class="form-control" disabled value="<?php echo $value->className; ?>" type="text">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <button class="btn btn-primary" id="saveBtn" style="background-color:#08acf0; border: none" type="button"> Save This</button>
                                                    </div>
                                                    <!-- <div class="col-sm-2">
                                                        <button class="btn btn-primary" id="scoreBtn" style="background-color:#FB5574; border: none" type="button"> Delete This</button>
                                                    </div>     -->
                                                </div>
                                                
                                            <?php }
                                        }
                                    ?>

                                </form>
                                </div>
                                </div>

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>