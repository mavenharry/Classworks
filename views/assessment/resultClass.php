<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class resultClass extends logic {    

    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return $value;
        }else{
            return "N/A";
        }
    }
    
    function fetchAllAssessments($classCode, $subjectCode, $session, $term, $part){
        $assessmentArray = array();
        if($part == 1){
            // MID TERM ASSESSMENT
            // start here
            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $className = $this->getClassName($classCode);
            $assessmentName = array();
            $assessmentName = $this->getAssessmentConfig($classCode, "classCode", $part);
            $stmt = $this->uconn->prepare("SELECT * FROM mid_term_score WHERE class_code = ? AND subject_code = ? AND academic_session = ? AND academic_term = ? AND student_no IN (SELECT student_number FROM students WHERE present_class = ?)");
            $stmt->bind_param("sssss", $classCode, $subjectCode, $current_academic_session, $current_academic_term, $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_no = $row["student_no"];
                $obj->class_code = $row["class_code"];
                $obj->subject_code = $row["subject_code"];
                $allMyAssessment = array();
                if(count($assessmentName) > 0){
                    foreach($assessmentName as $key => $value){
                        $realAssId = $this->getRealAssessmentId($value->name, $part);
                        $obj2 = new stdClass();
                        $name = $value->name;
                        // $assCode = $value->assCode;
                        $obj2->score = $row[$realAssId];
                        $obj2->assName = $name;
                        $obj2->id = $name;
                        $obj2->assCode = $realAssId;
                        $allMyAssessment[] = $obj2;
                    }
                }
                $obj->myAssessment = $allMyAssessment;
                $obj->total = $row["total"];
                $obj->tutor = $row["tutor"];
                $obj->date_created = $row["date_created"];
                    $stmt2 = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
                    $stmt2->bind_param("s", $row["student_no"]);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    $reply_details = array_shift($stmt2_result);
                    $fullname = $reply_details["fullname"];
                    $profilePhoto = $reply_details["profilePhoto"];
                    $obj->fullname = $fullname;
                    $obj->profilePhoto = $profilePhoto;
                    // print_r($obj);
                $assessmentArray[] = $obj;
            }
            $stmt->close();
        }else{
            // NORMAL ASSESSMENT

            if(isset($session) && !empty($session)){
                $current_academic_session = $session;
            }else{
                $current_academic_session = $this->getCurrentAcademicSession();
            }
    
            if(isset($term) && !empty($term)){
                $current_academic_term = $term;
            }else{
                $current_academic_term = $this->getCurrentAcademicTerm();
            }
            $className = $this->getClassName($classCode);
            $assessmentName = array();
            $assessmentName = $this->getAssessmentConfig($classCode, "classCode", $part);
            $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE class_code = ? AND subject_code = ? AND academic_session = ? AND academic_term = ? AND student_no IN (SELECT student_number FROM students WHERE present_class = ?)");
            $stmt->bind_param("sssss", $classCode, $subjectCode, $current_academic_session, $current_academic_term, $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $assessmentArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_no = $row["student_no"];
                $obj->class_code = $row["class_code"];
                $obj->subject_code = $row["subject_code"];
                $allMyAssessment = array();
                if(count($assessmentName) > 0){
                    foreach($assessmentName as $key => $value){
                        $realAssId = $this->getRealAssessmentId($value->name, $part);
                        $obj2 = new stdClass();
                        $name = $value->name;
                        // $assCode = $value->assCode;
                        $obj2->score = $row[$realAssId];
                        $obj2->assName = $name;
                        $obj2->id = $name;
                        $obj2->assCode = $realAssId;
                        $allMyAssessment[] = $obj2;
                    }
                }
                $obj->myAssessment = $allMyAssessment;
                // $obj->assignment = $row["assignment"];
                // $obj->ca1 = $row["ca1"];
                // $obj->ca2 = $row["ca2"];
                // $obj->ca3 = $row["ca3"];
                // $obj->exam = $row["exam"];
                $obj->total = $row["total"];
                $obj->remark = $row["remark"];
                $obj->grade = $row["grade"];
                $obj->tutor = $row["tutor"];
                $obj->term_admitted = $row["term_admitted"];
                $obj->date_created = $row["date_created"];
                    $stmt2 = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
                    $stmt2->bind_param("s", $row["student_no"]);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    $reply_details = array_shift($stmt2_result);
                    $fullname = $reply_details["fullname"];
                    $profilePhoto = $reply_details["profilePhoto"];
                    $obj->fullname = $fullname;
                    $obj->profilePhoto = $profilePhoto;
                $assessmentArray[] = $obj;
            }
            $stmt->close();
        }
        return $assessmentArray;
    }


    function getDomainName($domain){
        $stmt = $this->uconn->prepare("SELECT * FROM $domain WHERE status = 1 ORDER BY `name` ASC");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $domainArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->name = $row["name"];
            $obj->status = $row["status"];
            $obj->domain = $domain;
            $obj->id = $row["id"];
            $domainArray[] = $obj;
        }
        $stmt->close();
        return $domainArray;
    }

    
    function updateDomainName($newDomainState, $DomainNameId, $motorType){
        $ifExist = $this->checkIfExist($motorType, "id", "state", $DomainNameId);
        if($ifExist === true){
            $stmt1 = $this->uconn->prepare("UPDATE $motorType SET `status` = ? WHERE state = ?");
            $stmt1->bind_param("ii", $newDomainState,$DomainNameId);
            if($stmt1->execute()){
                $stmt1->close();
                return true;
            }else{
                $stmt1->close();
                return false;
            }
        }else{
            $stmt = $this->dconn->prepare("SELECT name FROM $motorType WHERE id = ?");
            $stmt->bind_param("i", $DomainNameId);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $name = array_shift($stmt_result)["name"];

            $stmt = $this->uconn->prepare("INSERT INTO $motorType (`name`, `status`, `state`) VALUES (?,?,?)");
            $stmt->bind_param("sii", $name, $newDomainState, $DomainNameId);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }
        // $stmt3 = $this->uconn->prepare("UPDATE $motorType SET `status` = ? WHERE id = ?");
        // $stmt3->bind_param("ii", $newDomainState, $DomainNameId);
        // $stmt3->execute();
        // $stmt3->close();
    }

    function getAllDomainName($domain){
        $status = 1;
        $stmt = $this->uconn->prepare("SELECT * FROM $domain WHERE status = ? ORDER BY 'name' ASC");
        $stmt->bind_param("i", $status);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $domainArray = array();
        while($row = array_shift($stmt_result)){
            $domainArray[] = $row["state"];
        }
        $stmt->close();
        return $domainArray;
    }

    function getAllDefaultDomainName($domain){
        $stmt = $this->dconn->prepare("SELECT * FROM $domain ORDER BY `name` ASC");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $domainArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->name = $row["name"];
            $obj->domain = $domain;
            $obj->id = $row["id"];
            $domainArray[] = $obj;
        }
        $stmt->close();
        return $domainArray;
    }


    function getDomain($domain, $session, $term, $student_no, $attr_id, $value){
        $stmt = $this->uconn->prepare("SELECT * FROM domain_map WHERE domain_name = ? AND `session` = ? AND `term` = ? AND student_number = ? AND attr_id = ? AND `value` = ?");
        $stmt->bind_param("ssssss", $domain, $session, $term, $student_no, $attr_id, $value);        
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $domainArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->attr_id = $row["attr_id"];
            $obj->value = $row["value"];
            $domainArray[] = $obj;
        }
        $stmt->close();
        return $domainArray;
    }


    function setDomain($domain, $session, $term, $student_no, $attr_id, $value){
        $stmt = $this->uconn->prepare("SELECT * FROM domain_map WHERE domain_name = ? AND `session` = ? AND `term` = ? AND student_number = ? AND attr_id = ?");
        $stmt->bind_param("sssss", $domain, $session, $term, $student_no, $attr_id);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            $stmt2 = $this->uconn->prepare("UPDATE domain_map SET `value` = ? WHERE domain_name = ? AND `session` = ? AND `term` = ? AND student_number = ? AND attr_id = ?");
            $stmt2->bind_param("ssssss", $value, $domain, $session, $term, $student_no, $attr_id);
            $stmt2->execute();
            $stmt2->close();   
        }else{
            $stmt3 = $this->uconn->prepare("INSERT INTO domain_map (`value`, domain_name, `session`, term, student_number, attr_id) VALUES (?,?,?,?,?,?)");
            $stmt3->bind_param("ssssss", $value, $domain, $session, $term, $student_no, $attr_id);
            $stmt3->execute();
            $stmt3->close(); 
        }
    }


    function updateResultComment($comment, $session, $term, $student_no){
        $stmt3 = $this->uconn->prepare("UPDATE assessments SET comment = ? WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        $stmt3->bind_param("ssss", $comment, $student_no, $session, $term);
        $stmt3->execute();
        $stmt3->close();
    }


    function getsubjectPerformance($category, $student_id, $session, $term){
        if($category == "max"){
            $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND total > 0 ORDER BY total DESC limit 1");
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND total > 0 ORDER BY total ASC limit 1");
        }
        $stmt->bind_param("sss", $student_id, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $stmt->close();
        return $row["subject_code"];
    }


    function getOverallPerformance($system, $student_id, $session, $term){
        $className = $this->getStudentDetails($student_id)->present_class;
        $classCode = $this->getClassCode($className);
        $notShow = 0;
        $sumTotal = 1;
        $stmt = $this->uconn->prepare("SELECT total FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND total != ? AND subject_code IN (SELECT subjectCode FROM subjects WHERE subjectClass = ? AND sumTotal = ?)");
        $stmt->bind_param("sssisi", $student_id, $session, $term, $notShow, $className, $sumTotal);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $OverallArray = array();
        while($row = array_shift($stmt_result)){
        $OverallArray[] = $row["total"];
        $number_of_subjects = count($OverallArray);
        $grossTotal = 100*$number_of_subjects;
        $obtainedTotal = array_sum($OverallArray);
        $perfomance = ($obtainedTotal/$grossTotal)*100;
        }
        if(empty($perfomance)){
           $perfomance = "N/A";
        }
        if($system == "percentage"){
            return $perfomance;
        }else{
            if($system == "grade"){
                return $this->getGrade($perfomance, $classCode)->grade;
            }else{
                                
            }
        }
        $stmt->close();
    }


    function getOverallAssessmentTutors($student_id, $session, $term){
        $stmt = $this->uconn->prepare("SELECT DISTINCT tutor FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        $stmt->bind_param("sss", $student_id, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $assessmentTutors = array();
        while($row = array_shift($stmt_result)){
            if(!empty($row["tutor"])){
                $names = $this->getSingleStaffDetails($row["tutor"])->fullname;
                $assessmentTutors[] = $names;
            }
        }
        $stmt->close();
        return $assessmentTutors;
    }


    function getSubjectAssessmentTutor($subjectCode){
        $stmt = $this->uconn->prepare("SELECT staffCode FROM subjectTeachers WHERE subjectCode = ?");
        $stmt->bind_param("s", $subjectCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $name = "";
        while($row = array_shift($stmt_result)){
            if(!empty($row["staffCode"])){
                $name = $this->getSingleStaffDetails($row["staffCode"])->fullname;
            }
        }
        $stmt->close();
        return $name;
    }

    function getAssessmentClassStudents($class, $session, $term){
        $stmt = $this->uconn->prepare("SELECT DISTINCT student_no FROM assessments WHERE class_code = ? AND academic_session = ? AND academic_term = ?");
        $stmt->bind_param("sss", $class, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $studentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $stmt2 = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
            $stmt2->bind_param("s", $row["student_no"]);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $reply_details = array_shift($stmt2_result);
            $obj->student_number = $this->outputRefine($reply_details["student_number"]);
            $obj->fullname = $this->outputRefine($reply_details["fullname"]);
            $studentArray[] = $obj;
        }
        return $studentArray;
    }

    
    function fixWrongStudents($classCode){
        $className = $this->getClassName($classCode);
        $stmt = $this->uconn->prepare("SELECT DISTINCT student_no FROM assessments WHERE student_no NOT IN (SELECT student_number FROM students WHERE present_class = ?) AND class_code = ?");
        $stmt->bind_param("ss", $className, $classCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        while($row = array_shift($stmt_result)){
            $stmt2 = $this->uconn->prepare("DELETE FROM assessments WHERE student_no = ? AND class_code = ?");
            $stmt2->bind_param("ss", $row["student_no"], $classCode);
            $stmt2->execute();
        }
    }
    

    function getClassPosition($class, $session, $term, $student_id){
        $className = $this->getStudentDetails($student_id)->present_class;
        $sumTotal = 1;
        $stmt = $this->uconn->prepare("SELECT DISTINCT student_no FROM assessments WHERE class_code = ? AND academic_session = ? AND academic_term = ?");
        $stmt->bind_param("sss", $class, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $totalArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $stmt2 = $this->uconn->prepare("SELECT SUM(total) as totalScore, AVG(total) as averageScore FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND total > 0 AND subject_code IN (SELECT subjectCode FROM subjects WHERE subjectClass = ? AND sumTotal = ?) ORDER by totalScore ASC");
            $stmt2->bind_param("ssssi", $row["student_no"], $session, $term, $className, $sumTotal);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $reply_details = array_shift($stmt2_result);
            $obj->totalScore = $reply_details["totalScore"];
            $obj->averageScore = $reply_details["averageScore"];
            $obj->student_number = $row["student_no"];
            $totalArray[strtolower($row["student_no"])] = $reply_details["averageScore"];
        }
        $sortArray = $totalArray;
        arsort($sortArray);
        //error_log(print_r($student_id,true));..... Prints to error log
        $realNum = array_search(strtolower($student_id), array_keys($sortArray));
        return (int) $realNum + 1;
    }

    function getClassTotalAndAverage($class, $session, $term, $student_id){
        $obj = new stdClass();
        $stmt2 = $this->uconn->prepare("SELECT SUM(total) as totalScore, AVG(total) as averageScore FROM assessments WHERE class_code = ? AND student_no = ? AND academic_session = ? AND academic_term = ? AND total > 0 ORDER by totalScore ASC");
        $stmt2->bind_param("ssss", $class, $student_id, $session, $term);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        $reply_details = array_shift($stmt2_result);
        $obj->totalScore = $reply_details["totalScore"];
        $obj->averageScore = $reply_details["averageScore"];
        $obj->student_number = $student_id;
        $totalArray[] = $obj;
        return $obj;
    }


    function fetchAllStudentSubjectAssessments($student_id, $session, $term, $subject){
        $className = $this->getStudentDetails($student_id)->present_class;
        $sumTotal = 1;
        $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND subject_code = ?");
        $stmt->bind_param("ssss", $student_id, $session, $term, $subject);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        while($row = array_shift($stmt_result)){
            $obj->student_no = $row["student_no"];
            $obj->class_code = $row["class_code"];
            $obj->subject_code = $row["subject_code"];
            $obj->total = $row["total"];
            $obj->remark = $this->getGrade($row["total"], $row["class_code"])->remark;
            $obj->grade = $this->getGrade($row["total"], $row["class_code"])->grade;
            $obj->comment = $row["comment"];
            $obj->tutor = $row["tutor"];
            $stmt2 = $this->uconn->prepare("SELECT SUM(total) as totalScore, AVG(total) as averageScore FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ? AND total > 0 AND subject_code IN (SELECT subjectCode FROM subjects WHERE subjectClass = ? AND sumTotal = ?)");
            $stmt2->bind_param("ssssi", $student_id, $session, $term, $className, $sumTotal);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $reply_details = array_shift($stmt2_result);
            $obj->totalScore = $reply_details["totalScore"];
            $obj->averageScore = $reply_details["averageScore"];   
        }
        $stmt->close();
        return $obj;
    }


    function fetchAllStudentAssessments($student_id, $session, $term, $part){
        // FOR LOGGED IN PEOPLE... FOR OUTSIDE RESULT, USE THE NEXT FUNCTION
        if($part == 1){
            $stmt = $this->uconn->prepare("SELECT * FROM mid_term_score WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        }
        $stmt->bind_param("sss", $student_id, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $assessmentArray = array();
        $assessmentName = array();
        $assessmentName = $this->getAssessmentConfig($student_id, "studentNumber", $part);
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->student_no = $row["student_no"];
            $obj->class_code = $row["class_code"];
            $obj->subject_code = $row["subject_code"];
            $totalScore = 0;
            $allMyAssessment = array();
            if(count($assessmentName) > 0){
                $total = 0;
                foreach($assessmentName as $key => $value){
                    $realAssId = $this->getRealAssessmentId($value->name, $part);
                    $obj2 = new stdClass();
                    $name = $value->name;
                    $assCode = $value->assCode;
                    $obj2->score = $row[$realAssId];
                    $obj2->assName = $name;
                    $obj2->id = $name;
                    $obj2->assCode = $assCode;
                    $total += $row[$realAssId];
                    $obj2->total = $total;
                    $totalScore = $total;
                    $allMyAssessment[] = $obj2;
                }
            }
            $obj->myAssessment = $allMyAssessment;

            $obj->scoreColor = $this->getScoreColor($row["total"], $row['class_code'], $part);
            $obj->remark = $this->getGrade($row["total"], $row["class_code"])->remark;
            $obj->grade = $this->getGrade($row["total"], $row["class_code"])->grade;
            $obj->comment = isset($row["comment"]) ? $row["comment"]: "";
            $obj->tutor = $row["tutor"];
            $obj->date_created = $row["date_created"];
                $stmt2 = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
                $stmt2->bind_param("s", $row["student_no"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $reply_details = array_shift($stmt2_result);
                $fullname = $reply_details["fullname"];
                $profilePhoto = $reply_details["profilePhoto"];
                $obj->fullname = $fullname;
                $obj->profilePhoto = $profilePhoto;
                if($totalScore != 0){
                    $assessmentArray[] = $obj;
                }
        }
        $stmt->close();
        return $assessmentArray;
    }

    function fetchAllStudentAssessmentsParent($student_id, $session, $term, $part){
        // FOR OUTSIDE RESULT, USE THIS NEXT FUNCTION
        if($part == 0){
            // NORMAL ASSESSMENT
            $stmt = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        }else{
            // MID TERM
            $stmt = $this->uconn->prepare("SELECT * FROM mid_term_score WHERE student_no = ? AND academic_session = ? AND academic_term = ?");
        }
        $stmt->bind_param("sss", $student_id, $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $assessmentArray = array();
        $assessmentName = array();
        $assessmentName = $this->getAssessmentConfig($student_id, "studentNumber", $part);
        while($row = array_shift($stmt_result)){
            $totalScore = 0;
            $obj = new stdClass();
            $obj->student_no = $row["student_no"];
            $obj->class_code = $row["class_code"];
            $obj->subject_code = $row["subject_code"];
            $allMyAssessment = array();
            if(count($assessmentName) > 0){
                $total = 0;
                foreach($assessmentName as $key => $value){
                    $realAssId = $this->getRealAssessmentId($value->name, $part);
                    $obj2 = new stdClass();
                    $name = $value->name;
                    $assCode = $value->assCode;
                    $obj2->score = $row[$realAssId];
                    $obj2->assName = $name;
                    $obj2->id = $name;
                    $obj2->assCode = $assCode;
                    $total += $row[$realAssId];
                    $obj2->total = $total;
                    $obj2->singleScoreColor = $this->getSingleScoreColor($row[$realAssId], $value->mark);
                    $allMyAssessment[] = $obj2;
                    $totalScore = $total;
                }
            }
            $obj->myAssessment = $allMyAssessment;

            $obj->remark = $this->getGrade($row["total"], $row["class_code"])->remark;
            $obj->grade = $this->getGrade($row["total"], $row["class_code"])->grade;
            $obj->comment = isset($row["comment"]) ? $row["comment"]: "";
            $obj->tutor = $row["tutor"];
            // $obj->term_admitted = $row["term_admitted"];
            $obj->date_created = $row["date_created"];
            $stmt2 = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
            $stmt2->bind_param("s", $row["student_no"]);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $reply_details = array_shift($stmt2_result);
            $fullname = $reply_details["fullname"];
            $profilePhoto = $reply_details["profilePhoto"];
            $obj->fullname = $fullname;
            $obj->profilePhoto = $profilePhoto;
            if($totalScore != 0){
                $obj->scoreColor = $this->getScoreColor($totalScore, $row['class_code'], $part);
                $assessmentArray[] = $obj;
            }
        }
        $stmt->close();
        return $assessmentArray;
    }
    
    function getSingleScoreColor($score, $total){
        if($score >= ($total / 2)){
            return "#8095A0";
        }else{
            return "#ff0000";
        }
    }

    function getScoreColor($score, $classCode, $part){
        $stmt = $this->uconn->prepare("SELECT SUM(mark) AS totalMark FROM assessment_settings WHERE assClass = ? AND mid_term = ?");
        $stmt->bind_param("ss", $classCode, $part);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $total = array_shift($stmt_result)['totalMark'];
        if($_SESSION['folderName'] == 'bcmss'){
            if($part == 1){
                return "#8095A0";
            }else{
                if($score >= 40){
                    return "#8095A0";
                }else{
                    return "#ff0000";
                }   
            }
        }else{
            // FOR OTHER SCHOOLS
            if($score >= ($total / 2)){
                return "#8095A0";
            }else{
                return "#ff0000";
            }
        }
    }


    function fetchAllTutorClass(){
        $staff_number = $_SESSION["uniqueNumber"];
        $stmt = $this->uconn->prepare("SELECT DISTINCT classCode FROM subjectTeachers WHERE staffCode = ?");
        $stmt->bind_param("s", $staff_number);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $classArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->classCode = $row["classCode"];
                $stmt2 = $this->uconn->prepare("SELECT * FROM classes WHERE class_code = ?");
                $stmt2->bind_param("s", $row["classCode"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $className = array_shift($stmt2_result)["class_name"];
                $obj->className = $className;
            $classArray[] = $obj;
        }
        $stmt->close();
        return $classArray;
    }

    
    function fetchAllAdminClass(){
        $stmt = $this->uconn->prepare("SELECT DISTINCT classCode FROM subjectTeachers");
        // $stmt->bind_param("s", $staff_number);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $classArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->classCode = $row["classCode"];
                $stmt2 = $this->uconn->prepare("SELECT * FROM classes WHERE class_code = ?");
                $stmt2->bind_param("s", $row["classCode"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $className = array_shift($stmt2_result)["class_name"];
                $obj->className = $className;
            $classArray[] = $obj;
        }
        $stmt->close();
        return $classArray;
    }


    function fetchAllClassSubject($classCode){
        $staff_number = $_SESSION["uniqueNumber"];
        $classCode = strtolower($classCode);
        if($_SESSION["role"] == "admin"){
            $stmt = $this->uconn->prepare("SELECT * FROM subjectTeachers WHERE classCode = ?");
            $stmt->bind_param("s", $classCode);
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM subjectTeachers WHERE classCode = ? AND staffCode = ?");
            $stmt->bind_param("ss", $classCode, $staff_number);
        }
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $subjectArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->subjectCode = $row["subjectCode"];
                $stmt2 = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectCode = ?");
                $stmt2->bind_param("s", $row["subjectCode"]);
                $stmt2->execute();
                $stmt2_result = $this->get_result($stmt2);
                $subjectName = array_shift($stmt2_result)["subjectName"];
                $obj->subjectName = $subjectName;
            $subjectArray[] = $obj;
        }
        $stmt->close();
        return $subjectArray;
    }

    function shortenSubjectName($subjectName){
        switch (strtolower($subjectName)) {
            case 'christian religious knowledge':
            case 'crk/moral instruction':
                return "crk";
                break;
            case 'christian religious studies':
                return "crs";
                break;
            case 'introductory technology':
                return "intro tech";
                break;
            case 'physical and health education':
                return "physical education";
                break;
            case 'agricultural science':
                return "agriculture";
                break;
            case 'principles of account':
            case 'principle of account':
            case 'principles of accounting':
            case 'principle of accounting':
                return "accounting";
                break;
            case 'calligraphy/handwriting':
                return "handwriting";
                break;
            case 'quantitative reasoning':
                return "quantitative reason";
                break;
            case 'b.k/moral instruction':
                return "bible knowledge";
                break;
            
            default:
                return $subjectName;
                break;
        }
    }

    function fetchClassSubjects($className){
        $stmt = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectClass = ?");
        $stmt->bind_param("s", $className);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $subjectArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
                $obj->subjectName = $this->shortenSubjectName($row["subjectName"]);
                $obj->subjectCode = $row["subjectCode"];
            $subjectArray[] = $obj;
        }
        $stmt->close();
        return $subjectArray;
    }


    function updateSubjectAssessment($student_no, $subject_code, $column_name, $input_val, $term = null, $session = null, $part){
        $assessmentConfig = $this->getAssessmentConfig($student_no, "studentNumber", $part);
        $academicTerm = $term == null ? $this->getSchoolDetails()->academic_term : $term;
        $academicSession = $session == null ? $this->getSchoolDetails()->academic_session : $session;
        $assCodeArray = array();
        foreach($assessmentConfig as $key => $value){
            $realAssId = $this->getRealAssessmentId($value->name, $part);
            $assCodeArray[] = $realAssId;
        }
        if($part == 1){
            // MID TERM
            $stmt2 = $this->uconn->prepare("SELECT * FROM mid_term_score WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
        }else{
            // NORMAL ASSESSMENT
            $stmt2 = $this->uconn->prepare("SELECT * FROM assessments WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
        }
        $stmt2->bind_param("sssi", $student_no, $subject_code, $academicSession, $academicTerm);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        $total = 0;
        while($row = array_shift($stmt2_result)){
            foreach($assCodeArray as $key => $value){
                if($value != $column_name){
                    $total += $row[$value];
                }
            }
            $total += $input_val;
        }
        if($part == 1){
            // MID TERM
            $stmt = $this->uconn->prepare("UPDATE mid_term_score SET ".$column_name." = ?, total = ? WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
        }else{
            // NORMAL ASSESSMENT
            $stmt = $this->uconn->prepare("UPDATE assessments SET ".$column_name." = ?, total = ? WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
        }
        $stmt->bind_param('ssssss', $input_val, $total, $student_no, $subject_code, $academicSession, $academicTerm);
        $stmt->execute();
        $obj = new stdClass();
        $obj->updatedVal = $total;
        $stmt->close();
        $stmt2->close();
        return $obj;
    }


    function getStudentDetails($studentNumber){
        $stmt = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $obj->nationality = $this->outputRefine($row["nationality"]);
            $obj->state = $this->outputRefine($row["state"]);
            $obj->city = $this->outputRefine($row["city"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->blood_group = $this->outputRefine($row["blood_group"]);
            $from = new DateTime($row["dob"]);
            $to   = new DateTime('today');
            $obj->age = $from->diff($to)->y;
            $obj->dob = $this->outputRefine($row["dob"]);
            $obj->enrolment_year = $this->outputRefine($row["enrolment_year"]);
            $obj->pickup_verification = $row["pickup_verification"];
            $obj->bus_pickup = $row["bus_pickup"];
            $obj->bus_dropoff = $row["bus_dropoff"];
            $obj->student_portal = $row["student_portal"];
            $obj->parent_portal = $row["parent_portal"];
            $obj->languages = json_decode($row["languages"], JSON_PRETTY_PRINT);
            $obj->student_desc = $this->outputRefine($row["student_desc"]);
            $stmt->close();
            return $obj;
        }else{
            $stmt->close();
            return "false";
        }
    }

    function getGradingSystem(){
        $stmt = $this->uconn->prepare("SELECT * FROM grading_system ORDER BY class_code ASC");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $gradesArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->id = $row["id"];
            $obj->from = $row["froms"];
            $obj->to = $row["tos"];
            $obj->grade = $row["grade"];
            $obj->remark = $row["remark"];
            $obj->className = $this->getClassName($row["class_code"]);
            $gradesArray[] = $obj;
        }
        $stmt->close();
        return $gradesArray;
    }

    function getClassGradingSystem($className){
        $classCode = $this->getClassCode($className);
        $stmt = $this->uconn->prepare("SELECT * FROM grading_system WHERE class_code = ? ORDER BY froms DESC");
        $stmt->bind_param("s", $classCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $gradesArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->id = $row["id"];
            $obj->from = $row["froms"];
            $obj->to = $row["tos"];
            $obj->grade = $row["grade"];
            $obj->remark = $row["remark"];
            $obj->className = $this->getClassName($row["class_code"]);
            $gradesArray[] = $obj;
        }
        $stmt->close();
        return $gradesArray;
    }

    function addGrading($gradeClasses, $from, $to, $remark, $grade){
        foreach($gradeClasses as $key => $value){
            $stmt = $this->uconn->prepare("SELECT id FROM grading_system WHERE froms = ? AND tos = ? AND class_code = ?");
            $stmt->bind_param("iis", $from, $to, $value);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "false";
            }else{
                $stmt = $this->uconn->prepare("INSERT INTO grading_system (class_code, froms, tos, grade, remark) VALUES (?,?,?,?,?)");
                $stmt->bind_param("siiss", $value, $from, $to, $grade, $remark);
                $stmt->execute();
            }
        }
        $stmt->close();
        return true;
    }

    function saveAssessmentSettings($assessmentClasses, $assessmentNames, $assessmentMarks){
        foreach($assessmentClasses as $key => $class){
            for($i = 0; $i < count($assessmentNames); $i++){
                $assName = strtolower(htmlentities(trim($assessmentNames[$i])));
                $assMark = htmlentities(trim($assessmentMarks[$i]));
                $state = 1;
                $stmt = $this->uconn->prepare("SELECT id FROM assessment_settings WHERE name = ? AND assClass = ?");
                $stmt->bind_param("ss", $assName, $class);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    // ALREADY EXIST
                }else{
                    $ifExist = $this->checkIfExist("assessment_settings", "id", "name", $assName);
                    $end = $this->getLastNumber("assessment_settings") + 1;
                        switch (strlen($end)) {
                            case 1:
                                # code...
                                $end = "00".$end;
                                break;
                            case 2:
                                # code...
                                $end = "0".$end;
                                break;
                            case 3:
                                # code
                                $end = $end;
                                break;
                            
                            default:
                                # code...
                                break;
                        }
                    if($ifExist === true){
                        // NEW ASSESSMENT
                        $assId = "ass".$end;
                        $stmt = $this->uconn->prepare("INSERT INTO assessment_settings (assId, name, assClass, mark) VALUES (?,?,?,?)");
                        $stmt->bind_param("sssi", $assId, $assName, $class, $assMark);
                        $stmt->execute();
                    }else{
                        // alter here...add new assid
                        $assId = "ass".$end;
                        $stmt = $this->uconn->prepare("ALTER TABLE assessments ADD $assId INT(11) NOT NULL AFTER subject_code");
                        $stmt->execute();
                        $stmt = $this->uconn->prepare("INSERT INTO assessment_settings (assId, name, assClass, mark) VALUES (?,?,?,?)");
                        $stmt->bind_param("sssi", $assId, $assName, $class, $assMark);
                        $stmt->execute();
                    }
                }
            }
        }
        $stmt->close();
        return true;
    }

    function getAssessmentConfig($value, $detail, $midTerm){
        $assessmentConfig = array();
        if(empty($value) && empty($detail)){

        }else{
            $state = 1;
            if($detail == "studentNumber"){
                // STUDENT NUMBER SENT
                $stmt = $this->uconn->prepare("SELECT assessment_settings.* FROM assessment_settings INNER JOIN classes ON classes.class_code = assessment_settings.assClass INNER JOIN students ON students.present_class = classes.class_name WHERE students.student_number = ? AND assessment_settings.ass_state = ? AND assessment_settings.mid_term = ?");
                $stmt->bind_param("sii", $value, $state, $midTerm);
            }else{
                if($detail == "all"){
                    $stmt = $this->uconn->prepare("SELECT * FROM assessment_settings WHERE ass_state = ? AND assessment_settings.mid_term = ?");
                    $stmt->bind_param("ii", $state, $midTerm);
                }else{
                    // CLASS  CODE SENT
                    $stmt = $this->uconn->prepare("SELECT * FROM assessment_settings WHERE assClass = ? AND ass_state = ? AND mid_term = ?");
                    $stmt->bind_param("sii", $value, $state, $midTerm);
                }
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->mark = $row["mark"];
                    $obj->name = $row["name"];
                    $obj->id = $row["id"];
                    $obj->className = $this->getClassName($row["assClass"]);
                    $obj->assCode = $row["assId"];
                    $assessmentConfig[] = $obj;
                }
            }
            $stmt->close();
        }
        return $assessmentConfig;
    }

    function deleteAssessmentRow($delAss){
        $stmt = $this->uconn->prepare("DELETE FROM assessment_settings WHERE id = ?");
        $stmt->bind_param("i", $delAss);
        if($stmt->execute()){
            $stmt->close();
            return "done";
        }else{
            $stmt->close();
            return "false";
        }
    }

    function updateAssessmentDetails($assName, $assMark, $assId){
        $stmt = $this->uconn->prepare("UPDATE assessment_settings SET name = ?, mark = ? WHERE id = ?");
        $stmt->bind_param("sii", $assName, $assMark, $assId);
        if($stmt->execute()){
            $stmt->close();
            return "done";
        }else{
            $stmt->close();
            return "error";
        }
    }

    function publishResultState($classes, $pinVending, $amount){
        $academicTerm = $this->getSchoolDetails()->academic_term;
        $academicSession = $this->getSchoolDetails()->academic_session;
        $newClasses = array();
        $stmt = $this->uconn->prepare("UPDATE settings SET pinChecker = ?, pinAmount = ?");
        $stmt->bind_param("ii", $pinVending, $amount);
        $stmt->execute();
        $stmt = $this->uconn->prepare("SELECT classCode FROM published_result WHERE academic_term = ? AND academic_session = ?");
        $stmt->bind_param("is", $academicTerm, $academicSession);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            // UPDATE
            $classCodeArray = json_decode(array_shift($stmt_result)["classCode"], JSON_PRETTY_PRINT);
            foreach($classes as $key=> $value){
                if(!in_array($value, $classCodeArray)){
                    $newClasses[] = $value;
                    $classCodeArray[] = $value;
                }
            }
            $newArray = json_encode($classCodeArray);
            $stmt = $this->uconn->prepare("UPDATE published_result SET classCode = ? WHERE academic_term = ? AND academic_session = ?");
            $stmt->bind_param("sis", $newArray, $academicTerm, $academicSession);
        }else{
            // INSERT
            $newClasses = $classes;
            $newArray = json_encode($classes);
            $pState = 1;
            $stmt = $this->uconn->prepare("INSERT INTO published_result (classCode, publish_state, academic_session, academic_term) VALUES (?,?,?,?)");
            $stmt->bind_param("sisi", $newArray, $pState, $academicSession, $academicTerm);
            $stmt->execute();
        }
        $reply = $this->publishSchoolResult($newClasses, $academicTerm, $academicSession);
        $stmt->close();
        return $reply;
    }

    function publishSchoolResult($classes, $academicTerm, $academicSession){
        $receivers = array();
        $numbers = array();
        $stmt = $this->uconn->prepare("SELECT fullname, phone, email FROM parents");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->fullname = $row["fullname"];
            $obj->phone = $row["phone"];
            $obj->email = $row["email"];
            $receivers[] = $obj;
        }
        // FOREACH ENDS ABOVE
        $smsUnit = $this->checkSmsBalance();
        $totalNumber = count($receivers) * 1;
        if($totalNumber > $smsUnit){
            return "insuficientUnit";
        }else{
            // PUBLISH SMS
            $schoolAddress = $this->getSchoolDetails()->domain_name.".classworks.xyz";
            $sender = strtoupper($this->getSchoolDetails()->domain_name);
            $schoolDomain = strtolower($sender);
            $schoolName = $this->getSchoolDetails()->school_name;
            foreach($receivers as $key => $parent){
                $phoneNumber = $parent->phone;
                $dir = "";
                $stmt = $this->dconn->prepare("INSERT INTO links (linkAddress, schoolName) VALUES (?,?)");
                $stmt->bind_param("ss", $dir, $schoolDomain);
                $stmt->execute();
                $linkId = base64_encode($this->dconn->insert_id);
                $link = "http://www.classworks.xyz/views/links?lid=".$linkId;
                if(!empty($phoneNumber)){
                    $message = "Good day ".ucwords($parent->fullname).", your child's result has just been published, follow the link below or click the link in the mail sent to ".$parent->email."\n ".$link;
                    $reply = $this->pushMessage($message, $sender, $phoneNumber);
                    $reply = $this->sendSchoolMail($parent->fullname, $parent->email, $link, $schoolName, $schoolAddress);
                }else{
                    // SEND ONLY MAIL
                    $reply = $this->sendSchoolMail($parent->fullname, $parent->email, $link, $schoolName, $schoolAddress);
                }
            }
            $stmt->close();
            return "done";
        }
    }

    function sendSchoolMail($parentName, $receiverEmail, $link, $schoolName, $schoolAddress){
        // SEND MAIL
            // subject
            $subject = ucwords($schoolName)."'s Published Result.";
            $lastHeader = $schoolName." <hello@classworks.xyz>";
            $message = "<!DOCTYPE html>
            <html>
               <body style='background-color: #222533; padding: 20px; font-family: font-size: 14px; line-height: 1.43; font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif;'>
                  <div style='max-width: 600px; margin: 10px auto 20px; font-size: 12px; color: #8095A0; text-align: center;'>If you are unable to see this message, <a href='#' style='color: #8095A0; text-decoration: underline;'>click here to view in browser</a></div>
                  <div style='max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);'>
                     <table style='width: 100%;'>
                        <tr>
                           <td style='background-color: #fff;'><img alt='' src='http://www.classworks.xyz/images/logo.png' style='width: 70px; margin-left:25px; padding: 20px'></td>
                           <td style='padding-left: 50px; text-align: right; padding-right: 20px;'><a href='http://www.classworks.xyz' style='color: #261D1D; text-decoration: underline; font-size: 14px; letter-spacing: 1px; font-weight:bold; background-color:#08ACF0; color:#FFF; padding:10px; border-radius:5px; text-decoration:none; margin-right:30px;'>Contact Us</a></td>
                        </tr>
                     </table>
                     <div style='padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);'>
                        <h1 style='margin-top: 0px; color:#8095A0'>Hi, ".ucwords($parentName)."</h1>
                        <div style='color: #8095A0; font-size: 14px;'>
                           <p>We are glad to inform you that the result of your child for ".ucwords($schoolName)." has just been published, click on the button below to view result</p>
                        </div>
                        <a href='".$link."' style='padding: 8px 20px; background-color: #08ACF0; color: #fff; font-weight: bolder; font-size: 16px; display: inline-block; margin: 20px 0px; margin-right: 20px; text-decoration: none;'>View Result</a>
                        <h4 style='margin-bottom: 10px; color:#8095A0'>Need Help?</h4>
                        <div style='color: #8095A0; font-size: 12px;'>
                           <p>If you have any questions you can simply reply to this email or find our contact information below. Also contact us at <a href='#' style='text-decoration: underline; color: #8095A0;'>".$schoolAddress."</a></p>
                        </div>
                     </div>
                     <div style='background-color: #F5F5F5; padding: 40px; text-align: center;'>
                        <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='https://www.classworks.xyz/images/twitter.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/facebook.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/linkedin.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/instagram.png' style='width: 28px;'></a></div>
                        <div style='color: #8095A0; font-size: 12px; margin-bottom: 20px; padding: 0px 50px;'>You are receiving this email because ".strtoupper($schoolName)." is powered by Classworks.xyz. All emails are sent from Classworks systems.</div>
                        <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='https://www.classworks.xyz/images/market-google-play.png' style='height: 33px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/market-ios.png' style='height: 33px;'></a></div>
                        <div style='margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);'>
                           <div style='color: #8095A0; font-size: 10px; margin-bottom: 5px;'>No. 10 NTA/Choba Road, Ada Odum Complex, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</div>
                           <div style='color: #8095A0; font-size: 10px;'>Copyright <?php echo date('Y') ?> ClassWorks.xyz. All rights reserved.</div>
                        </div>
                     </div>
                  </div>
               </body>
            </html>";
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.$lastHeader.'' . "\r\n";
            
            // Mail it
            
            if(@mail($receiverEmail, $subject, $message, $headers)){
                return true;
            }else{
                return false;
            }
    }

    function deleteGrade($gradeId){
        $stmt = $this->uconn->prepare("DELETE FROM grading_system WHERE id = ?");
        $stmt->bind_param("i", $gradeId);
        if($stmt->execute()){
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }

    }

    function generateStudentsAssessment($term, $session, $part){
        //$midTermState = $this->getMidTermState();
        $classArray = array();
        $stmt = $this->uconn->prepare("SELECT class_code, class_name FROM classes");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->classCode = $row["class_code"];
            $obj->className = $row["class_name"];
            $classArray[] = $obj;
        }
        foreach($classArray as $key => $value){
            $subjectArray = array();
            $studentsArray = array();
            $className = $value->className;
            $classCode = $value->classCode;
            $stmt = $this->uconn->prepare("SELECT subjectCode FROM subjects WHERE subjectClass = ?");
            $stmt->bind_param("s", $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $subjectArray[] = $row["subjectCode"];
            }
            $stmt = $this->uconn->prepare("SELECT student_number FROM students WHERE present_class = ?");
            $stmt->bind_param("s", $className);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $studentsArray[] = $row["student_number"];
            }
            if($part == 0){
                // NORMAL ASSESSMENT
                foreach($studentsArray as $key => $value){
                    foreach($subjectArray as $skey => $subject){
                        $stmt = $this->uconn->prepare("SELECT id FROM assessments WHERE student_no = ? AND class_code = ? AND subject_code = ? AND academic_term = ? AND academic_session = ?");
                        $stmt->bind_param("sssis", $value, $classCode, $subject, $term, $session);
                        $stmt->execute();
                        $stmt_result = $this->get_result($stmt);
                        if(count($stmt_result) == 0){
                            $stmt = $this->uconn->prepare("SELECT staffCode FROM subjectTeachers WHERE classCode = ? AND subjectCode = ?");
                            $stmt->bind_param("ss", $classCode, $subject);
                            $stmt->execute();
                            $stmt_result = $this->get_result($stmt);
                            if(count($stmt_result) > 0){
                                $staffCode = array_shift($stmt_result)["staffCode"];
                            }else{
                                $staffCode = "";
                            }
                            $addedDate = date("Y-m-d");
                            $stmt = $this->uconn->prepare("INSERT INTO assessments (student_no, class_code, subject_code, tutor, academic_session, academic_term, date_created) VALUES (?,?,?,?,?,?,?)");
                            $stmt->bind_param("sssssis", $value, $classCode, $subject, $staffCode, $session, $term, $addedDate);
                            $stmt->execute();
                        }
                    }
                }
            }else{
                // MID TERM ASSESSMENT
                foreach($studentsArray as $key => $value){
                    foreach($subjectArray as $skey => $subject){
                        $stmt = $this->uconn->prepare("SELECT id FROM mid_term_score WHERE student_no = ? AND class_code = ? AND subject_code = ? AND academic_term = ? AND academic_session = ?");
                        $stmt->bind_param("sssis", $value, $classCode, $subject, $term, $session);
                        $stmt->execute();
                        $stmt_result = $this->get_result($stmt);
                        if(count($stmt_result) == 0){
                            $stmt = $this->uconn->prepare("SELECT staffCode FROM subjectTeachers WHERE classCode = ? AND subjectCode = ?");
                            $stmt->bind_param("ss", $classCode, $subject);
                            $stmt->execute();
                            $stmt_result = $this->get_result($stmt);
                            if(count($stmt_result) > 0){
                                $staffCode = array_shift($stmt_result)["staffCode"];
                            }else{
                                $staffCode = "";
                            }
                            $addedDate = date("Y-m-d");
                            $stmt = $this->uconn->prepare("INSERT INTO mid_term_score (student_no, class_code, subject_code, tutor, academic_session, academic_term, date_created) VALUES (?,?,?,?,?,?,?)");
                            $stmt->bind_param("sssssis", $value, $classCode, $subject, $staffCode, $session, $term, $addedDate);
                            $stmt->execute();
                        }
                    }
                }
            }
            // EMPTY STUDENTS ARRAY FOR NEXT SET
            unset($studentsArray);
            unset($subjectArray);
        }
        $stmt->close();
    }

    function getPinState(){
        $stmt = $this->uconn->prepare("SELECT pinChecker, pinAmount FROM settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        while($row = array_shift($stmt_result)){
            $obj->pinChecker = $row["pinChecker"];
            $obj->pinAmount = $row["pinAmount"];
        }
        $stmt->close();
        return $obj;
    }

    function verifyCheckerDetails($pin, $studentNumber, $resultSession, $resultTerm){
        $obj = new stdClass();
        // CHECK IF RESULT HAS BEEN PUBLISHED FOR THIS TERM AND SESSION;
        $studentClassCode = $this->getClassCode($this->getStudentDetails($studentNumber)->present_class);
        $stmt = $this->uconn->prepare("SELECT * FROM published_result WHERE academic_session = ? AND academic_term = ?");
        $stmt->bind_param("si", $resultSession, $resultTerm);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $classCodes = json_decode($row["classCode"], JSON_PRETTY_PRINT);
        if(in_array($studentClassCode, $classCodes)){
            // ALREADY PUBLISHED RESULT
            if(empty($pin)){
                // NO PIN
                $stmt = $this->uconn->prepare("SELECT id FROM students WHERE student_number = ?");
                $stmt->bind_param("s", $studentNumber);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $obj->message = "proceed";
                    $obj->link = "report_sheet?student_id=".$studentNumber."&session=".$resultSession."&term=".$resultTerm;
                }else{
                    $obj->message = "Student not found in system. Please confirm number and try again";
                }
            }else{
                // PIN USED
                $stmt = $this->uconn->prepare("SELECT * FROM generatedPins WHERE pin = ?");
                $stmt->bind_param("i", $pin);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) == 1){
                    $row = array_shift($stmt_result);
                    if($row["ownerNumber"] == $studentNumber){
                        if($row["pinTerm"] == $resultTerm && $row["pinSession"] == $resultSession){
                            // ALLOW
                            $obj->message = "proceed";
                            $obj->link = "report_sheet?student_id=".$studentNumber."&session=".$resultSession."&term=".$resultTerm;
                        }else{
                            // EXPIRED
                            switch($row["pinTerm"]){
                                case 1:
                                    $termName = "1ST TERM";
                                    break;
                                case 2:
                                    $termName = "2ND TERM";
                                    break;
                                case 3:
                                    $termName = "3RD TERM";
                                    break;
                                default:
                                    $termName = "";
                                    break;
                            }
                            $allowedSession = ($row["pinSession"] - 1)."/".$row["pinSession"];
                            $obj->message = "Sorry this pin is only valid for ".$allowedSession." session ".$termName;
                        }
                    }else{
                        if(empty($row["ownerNumber"])){
                            $stmt = $this->uconn->prepare("UPDATE generatedPins SET ownerNumber = ? WHERE pin = ?");
                            $stmt->bind_param("si", $studentNumber, $pin);
                            $stmt->execute();
                            $obj->message = "proceed";
                            $obj->link = "report_sheet?student_id=".$studentNumber."&session=".$resultSession."&term=".$resultTerm;
                        }else{
                        // ALREADY USED FOR ANOTHER NUMBER
                        $obj->message = "This pin has already been used for another student";
                        }
                    }
                }else{
                    // WORNG PIN USED
                    $obj->message = "Invalid pin entered";
                }
            }
        }else{
            $obj->message = "Result has not been published yet. Please try again later";
        }
        return $obj;
    }

    function generatePins($pinNum, $term, $session){
        $pinArray = array();
        for($i = 0; $i < $pinNum; $i++){
            $time = substr(abs( crc32( uniqid() )), 0, 7);
            $stmt = $this->uconn->prepare("INSERT INTO generatedPins(pin, pinTerm, pinSession) VALUES (?,?,?)");
            $stmt->bind_param("iis", $time, $term, $session);
            $stmt->execute();
            $pinArray[] = $time;
        }
        $stmt->close();
        return $pinArray;
    }

    function allGeneratedPins($academicTerm, $academicSession){
        $stmt = $this->uconn->prepare("SELECT gp.*, s.fullname, s.present_class FROM generatedPins AS gp LEFT JOIN students AS s ON gp.ownerNumber = s.student_number WHERE gp.pinSession = ? AND gp.pinTerm = ?");
        $stmt->bind_param("ss", $academicSession, $academicTerm);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $allPins = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->pin = $row['pin'];
                $obj->pinTerm = $row['pinTerm'];
                $obj->pinSession = $row['pinSession'];
                $obj->studentNumber = $row['ownerNumber'] == "" || $row['ownerNumber'] == null ? "Not Used" : $row['ownerNumber'];
                $obj->studentName = $row['fullname'] == "" || $row['fullname'] == null ? "Not Used" : $row['fullname'];
                $obj->studentClass = $row['present_class'] == "" || $row['present_class'] == null ? "Not Used" : $row['present_class'];
                $allPins[] = $obj;
            }
        }
        return $allPins;
    }
    
    function getResumtionDate($academic_session, $academic_term){
        $stmt = $this->uconn->prepare("SELECT resume_date FROM resumption_dates WHERE academic_session = ? AND academic_term = ?");
        $stmt->bind_param("ss", $academic_session, $academic_term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            return array_shift($stmt_result)['resume_date'];
        }else{
            $stmt->close();
            return "";
        }
    }

    function getStampName($classCode){
        $stmt = $this->uconn->prepare('SELECT stamp_name FROM school_stamp WHERE class_code = ?');
        $stmt->bind_param('s', $classCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $stamp_name = array_shift($stmt_result)['stamp_name'];
        $stmt->close();
        return $stamp_name;
    }

    function addMidTerm($partOf, $assessmentClasses, $assessmentNames, $assessmentMarks){
        foreach($assessmentClasses as $key => $class){
            for($i = 0; $i < count($assessmentNames); $i++){
                $assName = strtolower(htmlentities(trim($assessmentNames[$i])));
                $assMark = htmlentities(trim($assessmentMarks[$i]));
                $state = 1;
                $midTerm = 1;
                $stmt = $this->uconn->prepare("SELECT id FROM assessment_settings WHERE name = ? AND assClass = ? AND mid_term = ?");
                $stmt->bind_param("ssi", $assName, $class, $midTerm);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    // ALREADY EXIST
                    $stmt->close();
                }else{
                    $ifExist = $this->checkIfExist("assessment_settings", "id", "name", $assName);
                    $end = $this->getLastNumber("assessment_settings") + 1;
                        switch (strlen($end)) {
                            case 1:
                                # code...
                                $end = "00".$end;
                                break;
                            case 2:
                                # code...
                                $end = "0".$end;
                                break;
                            case 3:
                                # code
                                $end = $end;
                                break;
                            
                            default:
                                # code...
                                break;
                        }
                    if($ifExist === true){
                        // NEW ASSESSMENT
                        $assId = "mid".$end;
                        $midTerm = 1;
                        $stmt = $this->uconn->prepare("INSERT INTO assessment_settings (assId, name, assClass, mark, mid_term, part_of) VALUES (?,?,?,?,?,?)");
                        $stmt->bind_param("sssiis", $assId, $assName, $class, $assMark, $midTerm, $partOf);
                        $stmt->execute();
                    }else{
                        // alter here...add new assid
                        $assId = "mid".$end;
                        $midTerm = 1;
                        $stmt = $this->uconn->prepare("ALTER TABLE mid_term_score ADD $assId INT(11) NOT NULL AFTER subject_code");
                        $stmt->execute();
                        $stmt = $this->uconn->prepare("INSERT INTO assessment_settings (assId, name, assClass, mark, mid_term, part_of) VALUES (?,?,?,?,?,?)");
                        $stmt->bind_param("sssiis", $assId, $assName, $class, $assMark, $midTerm, $partOf);
                        $stmt->execute();
                        $stmt->close();
                    }
                }
            }
        }
        return true;
    }


}

// $resClass = new resultClass();
// $resp = $resClass->fetchAllAssessments("class002", "subject002", "2019", "1", "1");
// print_r($resp);
?>
