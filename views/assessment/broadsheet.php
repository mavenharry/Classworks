<?php include_once ("resultbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Assessment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <style>
    @media print {

    * {
        color: black            !important;
        background: transparent !important;
        text-shadow: none       !important;
        box-shadow: none        !important;
        /* border: none            !important; */
        float: none             !important;
        clear: none             !important;
        }
    html, body, article, header, section, footer, aside, div { 
        width: 100% !important;
        }
    .no-print, .no-print *
    {
        display: none !important;
    }
    a, a:link, a:visited, a:hover, a:active, abbr, acronym {
        text-decoration: none;
        border-bottom: 0 none;
        }
    h1, h2, h3, h4, h5, h6, p, li {
        page-break-inside: avoid;
        orphans: 3; widows: 3;
        }
    h1, h2, h3, h4, h5, h6 { 
        page-break-after: avoid;
        }
    thead   { display: table-header-group; }
    tr, img { page-break-inside: avoid; }
    nav     { display: none; }
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }

    @page {size: landscape}

    }
    </style>
</head>
<?php
    $schoolDetails = $resultClass->getSchoolDetails();
    $allTerms = $resultClass->getSessionTerms($_GET["session"]);
    $allClassStudents = $resultClass->getBroadsheetAssessment($_GET["class"], $_GET["session"]);    
?>

<style>
        /* Center the loader */
        #loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 150px;
        height: 150px;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #08acf0;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }

        /* Add animation to "page content" */
        .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
        from { bottom:-100px; opacity:0 } 
        to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom { 
        from{ bottom:-100px; opacity:0 } 
        to{ bottom:0; opacity:1 }
        }

        #myDiv {
        display: none;
        }
    </style>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <!-- <div id="loaderBg" style="background:#fff; position:fixed; width:100%; height:100%; top: 0; left: 0; z-index: 2000; overflow: hidden;">
    <div id="loader"></div>
    </div> -->

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Assessment", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                } 
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="no-print element-actions">
                                    
                                    <a id="btnExport" onclick="exportTableToExcel('classSheetTable', '<?php echo $_GET['session'] ?> Session <?php echo $_GET['term'] ?> Term <?php echo strtoupper($resultClass->getClassName($_GET['class'])) ?> Class Report Sheet')" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-book"></i>
                                        <span style="font-size: .9rem;">Export Excel Sheet</span>
                                    </a>
                                    <a onclick="window.print()" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm no-print" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><strong style="font-weight: 800;"><?php echo strtoupper($resultClass->getClassName($_GET["class"])) ?></strong> CLASS ASSESSMENT REPORT SHEET</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                    
                                    <table style="table-layout: fixed; width: 100%; margin-top:<?php if(count($allSubjects) > 0){echo "170px";}else{ echo "0px";} ?>" id="classSheetTable" class="table table-padded">
                                    <thead>
                                    <tr>
                                        <!-- <th style="width: 50px; text-align:center">SN</th> -->
                                        <!-- <th style="transform: rotate(270deg); text-align:center">Regr.No.</th> -->
                                        <th style="width:200px; text-align:center">Student's Name</th>
                                        <?php 
                                            foreach($allTerms as $key => $value){ ?>
                                            <th style="transform: rotate(270deg); text-align:center; width:auto"><?php echo strtoupper($value);?></th>
                                        <?php } 
                                        ?>
                                        <th style="width: 80px; text-align:center">Total</th>
                                        <th style="text-align: center; width: 100px;">Average</th>
                                        <th onclick="sortTable(this)" id="position" style="width: 100px; text-align:center">Position</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        foreach($allClassStudents as $key => $value){ ?>
                                        <tr style="font-size: 1em; border-bottom: 1px solid #91a3ad;">
                                        <!-- <td style="text-align:center">
                                        <?php
                                            echo $resultClass->getClassPosition($_GET["class"], $_GET["session"], $_GET["term"], $value->student_number);
                                        ?>
                                        </td> -->
                                        <!-- <td style="text-align:center"><?php echo explode("/", $value->student_number)[3] ?></td> -->
                                        <td style="text-align:center"><?php echo strtoupper($value->fullname) ?></td>
                                        <?php 
                                            foreach($allSubjects as $key2 => $value2){ ?>
                                            <td style="text-align:center;">
                                            <?php
                                                if($resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->total > 0){
                                                    echo $resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->total;
                                                }else{
                                                    echo "-";
                                                }
                                            ?>
                                            </td>
                                        <?php } 
                                        ?>
                                        <td style="text-align:center">
                                        <?php
                                            if($resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->totalScore > 0){
                                                echo $resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->totalScore;
                                            }else{
                                                echo "-";
                                            }
                                        ?>
                                        </td>
                                        <td style="text-align:center">
                                        <?php
                                            echo round($resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->averageScore, 1);
                                        ?>
                                        </td>
                                        <td style="text-align:center">
                                        <?php
                                            echo $resultClass->getClassPosition($_GET["class"], $_GET["session"], $_GET["term"], $value->student_number);
                                        ?>
                                        </td>
                                        <td style="text-align:center">
                                            <?php
                                                echo $resultClass->getGrade(round($resultClass->fetchAllStudentSubjectAssessments($value->student_number, $_GET["session"], $_GET["term"], $value2->subjectCode)->averageScore, 1), $_GET["class"])->remark;
                                            ?>
                                        </td>
                                        </tr>
                                        <?php } 
                                        ?>
                                    </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <!--END - Transactions Table-->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

<script>
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}

function sortTable(u) {
    var n = u.cellIndex
    var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("classSheetTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (Number(x.innerHTML) > Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (Number(x.innerHTML) < Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

$("document").ready(function() {
    setTimeout(function() {
        $("#position").trigger('click');
    },10);
    
    $(window).on("load", function() {
        $("#loaderBg").css("display","none")
    });

});

</script>

</body>

</html>