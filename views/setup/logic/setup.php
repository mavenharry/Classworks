<?php
session_start();
include_once ("../../logic/config.php");

class setup extends logic {

    function get_result( $Statement ) {
        $RESULT = array();
        $Statement->store_result();
        for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
            $Metadata = $Statement->result_metadata();
            $PARAMS = array();
            while ( $Field = $Metadata->fetch_field() ) {
                $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
            }
            call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
            $Statement->fetch();
        }
        $object = (object) $RESULT;
        return $RESULT;
    }

    function verify($email){
            $stmt = $this->dconn->prepare("UPDATE users SET activated = 1 WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->close();
            return true;
    }

    function checkVerify($email){
        $stmt = $this->dconn->prepare("SELECT activated FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $num = array_shift($stmt_result)["activated"];
        $stmt->close();
        return $num;
    }

    function verifyTrialCode($trialCode, $verifyEmail){
        $stmt = $this->dconn->prepare("SELECT activated FROM users WHERE email = ? AND trialCode = ?");
        $stmt->bind_param("ss", $verifyEmail, $trialCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $num = array_shift($stmt_result)["activated"];
            if($num == 0){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }else{
            $num = "not found";
            $stmt->close();
            return $num;
        }
    }
    

    function allModules(){
        $stmt = $this->dconn->prepare("SELECT * FROM modules");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $modules = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->price = $row["price"];
            $obj->name = $row["mod_name"];
            $modules[] = $obj;
        }
        return $modules;
    }
}
?>