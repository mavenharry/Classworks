<?php
include_once ("setup.php");
$setup = new setup();

if(isset($verifyEmail)){
    $reply = $setup->checkVerify($verifyEmail);
    if($reply == 1){
        echo "<script>window.location.assign('../authentication/')</script>";
    }
}

if(isset($_POST["trial"])){
    @$trialCode = htmlentities(trim($_POST["trialCode"]));
    if(empty($trialCode)){
        // SET DETAILS IN SESSION AND SELECT PLAN
        $_SESSION["adminEmail"] = $verifyEmail;
        $_SESSION["category"] = $_POST["category"];
        $_SESSION["domainName"] = strtolower(htmlentities(trim($_POST["domainName"])));
        $_SESSION["curriculum"] = htmlentities(trim($_POST["curriculum"]));
        $_SESSION["schoolName"] = htmlentities(trim($_POST["schoolName"]));
        $_SESSION["color"] = htmlentities(trim($_POST["color"]));
        $_SESSION["grading"] = htmlentities(trim($_POST["grading"]));
        $_SESSION["currency"] = $_POST["currency"];
        $_SESSION["slogan"] = htmlentities(trim($_POST["slogan"]));
        $_SESSION["schoolAddress"] = htmlentities(trim($_POST["schoolAddress"]));
        $_SESSION["phone"] = htmlentities(trim($_POST["phone"]));
        $_SESSION["email"] = htmlentities(trim($_POST["email"]));
        $_SESSION["website"] = htmlentities(trim($_POST["website"]));
        $_SESSION["city"] = htmlentities(trim($_POST["city"]));
        $_SESSION["state"] = htmlentities(trim($_POST["state"]));
        $_SESSION["country"] = htmlentities(trim($_POST["country"]));
        $_SESSION["trialCode"] = htmlentities(trim($_POST["trialCode"]));

        // MOVE TO TEMP FOLDER FIRST
        $imageFolder = $_SERVER["DOCUMENT_ROOT"]."/tempImages";
        if($_FILES["schoolLogo"]["size"] > 0){
            if($_FILES["schoolLogo"]["error"]){
                $error = "An error occured with the following error. ".$_FILES["schoolLogo"]["error"];
            }else{
                $extension = @end(explode(".",$_FILES["schoolLogo"]["name"]));

                $filename = $_SESSION["adminEmail"].".".$extension;
                $_SESSION["logoName"] = $filename;
                if(file_exists($imageFolder."/".$filename)){
                    unlink($imageFolder."/".$filename);
                }
                move_uploaded_file($_FILES["schoolLogo"]["tmp_name"], $imageFolder."/".$filename);

            }
        }

        echo "<script>window.location.assign('pricing.php')</script>";
    }else{
        $reply = $setup->verifyTrialCode($trialCode, $verifyEmail);
        if($reply === true){
            // CREATE SCHOOL
            @$adminEmail = $verifyEmail;
            @$category = $_POST["category"];
            @$domainName = strtolower(htmlentities(trim($_POST["domainName"])));
            @$curriculum = htmlentities(trim($_POST["curriculum"]));
            @$schoolName = htmlentities(trim($_POST["schoolName"]));
            @$subscription = 0;
            @$color = htmlentities(trim($_POST["color"]));
            @$grading = htmlentities(trim($_POST["grading"]));
            @$currency = $_POST["currency"];
            @$slogan = htmlentities(trim($_POST["slogan"]));
            @$schoolAddress = htmlentities(trim($_POST["schoolAddress"]));
            @$phone = htmlentities(trim($_POST["phone"]));
            @$email = htmlentities(trim($_POST["email"]));
            @$website = htmlentities(trim($_POST["website"]));
            @$city = htmlentities(trim($_POST["city"]));
            @$state = htmlentities(trim($_POST["state"]));
            @$country = htmlentities(trim($_POST["country"]));
            @$trialCode = htmlentities(trim($_POST["trialCode"]));
            @$module = 0;
            $subDate = date("Y-m-d");
            $nextSub = date("Y-m-d", strtotime("+30 days"));
            $smsUnit = 50;
            $accountState = 1;

            if( in_array("Primary" , $category) && $curriculum == "american" && !in_array("Secondary", $category)  ){
                // American Primary
                include_once ("Ltables/primaryAmerican.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Secondary" , $category) && $curriculum == "american" && !in_array("Primary", $category) ) {
                // echo "American Secondary";
                include_once ("Ltables/secondaryAmerican.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Primary" , $category) && $curriculum == "american" && in_array("Secondary", $category) ) {
                // echo "American Primary Secondary";
                include_once ("Ltables/secondaryPrimaryAmerican.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Primary" , $category) && $curriculum == "nigeria" && !in_array("Secondary", $category) ) {
                // echo "Primary  Nigeria";
                include_once ("Ltables/primaryNigeria.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Primary" , $category) && $curriculum == "nigeria" && in_array("Secondary", $category) ) {
                // echo "Primary and Secondary Nigeria";
                include_once ("Ltables/primarySecondaryNigeria.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (!in_array("Primary" , $category) && $curriculum == "nigeria" && in_array("Secondary", $category) ) {
                // echo "Secondary Nigeria";
                include_once ("Ltables/secondaryNigeria.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Primary" , $category) && $curriculum == "british" && !in_array("Secondary", $category) ) {
                // echo "British Primary";
                include_once ("Ltables/primarybritish.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (in_array("Primary" , $category) && $curriculum == "british" && in_array("Secondary", $category) ) {
                // echo "Primary and Secondary British";
                include_once ("Ltables/primarySecondarybritish.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
            elseif (!in_array("Primary" , $category) && $curriculum == "british" && in_array("Secondary", $category) ) {
                // echo "Secondary British";
                include_once ("Ltables/secondarybritish.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }else{
                // echo "Empty School";
                include_once ("Ltables/emptySchool.php");
                $reply = $setup->verify($adminEmail);
                echo "<script>window.location.assign('../authentication/')</script>";
            }
        }
        elseif($reply === false){
            $err = "The code you entered has already been used";
        }
        elseif($reply == "not found"){
            $err = "The trial code you entered could not be verified or is not correct. Please verify and try again";
        }
    }
}

if(isset($_POST["create"])){
    @$adminEmail = $_SESSION["adminEmail"];
    @$category = $_SESSION["category"];
    @$domainName = htmlentities(trim($_SESSION["domainName"]));
    @$curriculum = htmlentities(trim($_SESSION["curriculum"]));
    @$schoolName = htmlentities(trim($_SESSION["schoolName"]));
    @$subscription = 0;
    @$color = htmlentities(trim($_SESSION["color"]));
    @$grading = htmlentities(trim($_SESSION["grading"]));
    @$currency = $_SESSION["currency"];
    @$slogan = htmlentities(trim($_SESSION["slogan"]));
    @$schoolAddress = htmlentities(trim($_SESSION["schoolAddress"]));
    @$phone = htmlentities(trim($_SESSION["phone"]));
    @$email = htmlentities(trim($_SESSION["email"]));
    @$website = htmlentities(trim($_SESSION["website"]));
    @$city = htmlentities(trim($_SESSION["city"]));
    @$state = htmlentities(trim($_SESSION["state"]));
    @$country = htmlentities(trim($_SESSION["country"]));
    @$trialCode = htmlentities(trim($_SESSION["trialCode"]));
    @$filename = htmlentities(trim($_SESSION["logoName"]));
    @$module = htmlentities(trim($_POST["module"]));
    $subDate = date("Y-m-d");
    $nextSub = date("Y-m-d", strtotime("+120 days"));
    $smsUnit = 50;
    $accountState = 1;

    // UNSET THE SESSIONS
    unset($_SESSION["adminEmail"]);
    unset($_SESSION["category"]);
    unset($_SESSION["schoolName"]);
    unset($_SESSION["domainName"]);
    unset($_SESSION["curriculum"]);
    unset($_SESSION["schoolName"]);
    unset($_SESSION["color"]);
    unset($_SESSION["grading"]);
    unset($_SESSION["currency"]);
    unset($_SESSION["slogan"]);
    unset($_SESSION["schoolAddress"]);
    unset($_SESSION["phone"]);
    unset($_SESSION["email"]);
    unset($_SESSION["website"]);
    unset($_SESSION["city"]);
    unset($_SESSION["state"]);
    unset($_SESSION["country"]);
    unset($_SESSION["trialCode"]);
    unset($_SESSION["logoName"]);
    // UNSET ENDS HERE

    if( in_array("Primary" , $category) && $curriculum == "american" && !in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ){
        // American Primary
        include_once ("Ltables/primaryAmerican.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Secondary" , $category) && $curriculum == "american" && !in_array("Primary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "American Secondary";
        include_once ("Ltables/secondaryAmerican.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Primary" , $category) && $curriculum == "american" && in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "American Primary Secondary";
        include_once ("Ltables/secondaryPrimaryAmerican.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Primary" , $category) && $curriculum == "nigeria" && !in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "Primary  Nigeria";
        include_once ("Ltables/primaryNigeria.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Primary" , $category) && $curriculum == "nigeria" && in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "Primary and Secondary Nigeria";
        include_once ("Ltables/primarySecondaryNigeria.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (!in_array("Primary" , $category) && $curriculum == "nigeria" && in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "Secondary Nigeria";
        include_once ("Ltables/secondaryNigeria.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Primary" , $category) && $curriculum == "british" && !in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "British Primary";
        include_once ("Ltables/primarybritish.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (in_array("Primary" , $category) && $curriculum == "british" && in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "Primary and Secondary British";
        include_once ("Ltables/primarySecondarybritish.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
    elseif (!in_array("Primary" , $category) && $curriculum == "british" && in_array("Secondary", $category) && !in_array("Creche", $category) && !in_array("Kindergarten", $category) && !in_array("Nursery", $category) ) {
        // echo "Secondary British";
        include_once ("Ltables/secondarybritish.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }else{
        // echo "Empty School";
        include_once ("Ltables/emptySchool.php");
        $reply = $setup->verify($adminEmail);
        echo "<script>window.location.assign('../authentication/')</script>";
    }
}
?>