<?php include_once ("logic/setupbtn.php");?>
<!DOCTYPE html>
<html>
   <head>
    <title>ClassWorks - School Automation Solution | Payment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
   <body class="menu-position-side menu-side-left full-screen">
      <div class="all-wrapper no-padding-content white-bg-content solid-bg-all">
            <div class="content-w">
               <form> <input type="email" hidden value="<?php echo isset($_SESSION["email"]) ? $_SESSION["email"] : ""; ?>" id="cusEmail"><input type="text" hidden value="<?php echo isset($_SESSION["phone"]) ? $_SESSION["phone"] : ""; ?>" id="phone"><input type="text" hidden value="<?php echo isset($_SESSION["schoolName"]) ? $_SESSION["schoolName"] : ""; ?>" id="schoolName"></form>
               <div class="content-i">
                  <div class="content-box">
                     <div class="section-heading centered">
                        <h1>CHOOSE PAYMENT PLAN</h1>
                        <p style="padding-right:100px; padding-left:100px; text-align:center">We present to you the coolest pricing plans for schools and colleges automation/management systems accross the universe. Choose the plan that best suits your school needs or budget.</p>
                     </div>
                     <?php
                      if(!isset($_SESSION["email"]) && !isset($_SESSION["schoolName"])){ 
                        $err = 1; ?>
                        <div class="alert alert-warning" style="margin-left: 250px; margin-right: 250px; margin-bottom: 70px; margin-top: -70px;">
                          <h6>The System encountered an error while trying to handle your request. Please follow the activation link in the mail sent to you<h6>
                        </div>
                      <?php }
                     ?>
                     <div class="pricing-plans row no-gutters" style="margin-top: -20px;">
                        <div class="pricing-plan col-sm-offset-9 col-sm-3 with-hover-effect" style="margin-left:13%; border: 1px solid rgba(0,0,0,0.1);">
                           <div class="plan-head">
                              <div class="plan-name">Bronze</div>
                           </div>
                           <div class="plan-body">
                              <div class="plan-price-w">
                                 <div class="price-value">&#8358;200</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Starter Price : &#8358;40,000</div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="1" id="module"><input type="submit" class="btn btn-primary btn-rounded planPay" value="Choose this plan"><input hidden type="submit" id="planPay2" name="create"></form></div>
                           </div>
                           <div class="plan-description">
                              <h6>Description</h6>
                              <p style="text-align:justify">Basic system subscription plan specially packaged for schools and colleges with low budget or very minimal automation and management needs.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>1,000 Free SMS Units</li>
                              <li>Max of 200 Students</li>
                              </ul>
                           </div>
                        </div>
                        <div class="pricing-plan col-sm-3 with-hover-effect">
                           <div class="plan-head">
                              <div class="plan-name">Silver</div>
                           </div>
                           <div class="plan-body">
                              <div class="plan-price-w">
                              <div class="price-value">&#8358;300</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Starter Price : &#8358;60,000</div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="2" id="module"><input type="submit" class="planPay btn btn-primary btn-rounded" value="Choose this plan"><input hidden type="submit" id="planPay2" name="create"></form></div>
                           </div>
                           <div class="plan-description">
                              <h6>Description</h6>
                              <p style="text-align:justify">Standard system subscription plan standardized and packaged for schools and colleges with standard budget and for full automation systems.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Automated Attendance</li>
                              <li>Child Pickup System</li>
                              <li>Hostel Management</li>
                              <li>Result Checker (Pin Vending)</li>
                              <li>Inventory Management (Tuck Shop)</li>
                              <li>Educational Games</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>3,000 Free SMS Units</li>
                              <li>Max of 500 Students</li>
                              </ul>
                           </div>
                        </div>
                        <div class="pricing-plan col-sm-3 with-hover-effect">
                           <div class="plan-head">
                              <div class="plan-name">Gold</div>
                           </div>
                           <div class="plan-body">
                              <div class="plan-price-w">
                                 <div class="price-value">&#8358;500</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Starter Price : &#8358;100,000</div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="3" id="module"><input type="submit" class="planPay btn btn-primary btn-rounded" value="Choose this plan"><input hidden type="submit" id="planPay2" name="create"></form></div>
                           </div>
                           <div class="plan-description">
                           <h6>Description</h6>
                              <p style="text-align:justify">Premium system subscription plan carefully packaged for premium schools and colleges with good budget requires very high functionalities.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Automated Attendance</li>
                              <li>Child Pickup System</li>
                              <li>Virtual Library/Book Shop</li>
                              <li>Hostel Management</li>
                              <li>Result Checker (Pin Vending)</li>
                              <li>Inventory Management (Tuck Shop)</li>
                              <li>Educational Games</li>
                              <li>Human Resource Management/Payroll</li>
                              <li>Computer Based Examination/Test</li>
                              <li>Virtual Counselling</li>
                              <li>Discussion Forums</li>
                              <li>Custom Email Address</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>5,000 Free SMS Units</li>
                              <li>Unlimited Students</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="display-type"></div>
      </div>

        <!-------------------- START - Top Bar -------------------->
        <?php include_once ("../../includes/support.php") ?>
        <!-------------------- END - Top Bar -------------------->

        <?php include_once ("../../includes/scripts.php"); ?>
   </body>
   <?php
    if(isset($err) && $err == 1){?>
      <script>
        $(document).ready(function(){
          $(".planPay").attr("disabled", "true");
        })
      </script>
    <?php }
   ?>
</html>