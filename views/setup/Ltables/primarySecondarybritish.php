<?php
// CREATES ALL TABLE FOR Primary School, American Curriculum

// CREATE DATABASE
include_once ("../../logic/db.php");

$createConn = new mysqli(host, user, password, database);
$conn2 = new mysqli(host, user, password);
if(!$createConn){
    $error = "Fatal Error: Can't connect to database ".$createConn->connect_error;
    return false;
}

function get_result( $Statement ) {
    $RESULT = array();
    $Statement->store_result();
    for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
        $Metadata = $Statement->result_metadata();
        $PARAMS = array();
        while ( $Field = $Metadata->fetch_field() ) {
            $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
        }
        call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
        $Statement->fetch();
    }
    $object = (object) $RESULT;
    return $RESULT;
}
$stmt = $createConn->prepare("SELECT id FROM schools WHERE school_name = ?");
$stmt->bind_param("s", $schoolName);
$stmt->execute();
$stmt_result = get_result($stmt);
if(count($stmt_result) == 0){
    // NEW SCHOOL
    $stmt2 = $createConn->prepare("SELECT COUNT(id) AS num FROM schools");
    $stmt2->execute();
    $stmt2_result = get_result($stmt2);
    $num = array_shift($stmt2_result)["num"] + 1;
    $schoolNameUntouched = $schoolName;
    $schoolNameNoSpace = str_replace(" ", "", $schoolName);
    $schoolName = explode(" ",$schoolName)[0];
    $folderName = $domainName;
    // NEW DATABSE DETAILS
    $databaseName = prefix.$domainName;
    $databaseuser = prefix.$domainName;
    $databasepass = "@".rand(10000,99999)."#".rand(100,999);
    // CREATE DATABASE
    $stmt = $conn2->prepare("CREATE DATABASE ".$databaseName."");
    $stmt->execute();
    $conn3 = new mysqli(host, user, password, $databaseName);
    // SETTINGS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `settings` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `school_name` varchar(45) DEFAULT NULL,
        `schoolAddress` text NOT NULL,
        `domain_name` varchar(100) DEFAULT NULL,
        `reg_date` date DEFAULT NULL,
        `categories` text,
        `country` text,
        `state` text,
        `city` text,
        `website` text,
        `email` text,
        `telephone` text,
        `logo` text,
        `currency` text,
        `slogan` text,
        `grading_system` text,
        `school_color` text,
        `school_curriculum` text,
        `modules` text,
        `init_slider` int(1) DEFAULT '0',
        `templateId` int(1) DEFAULT '0',
        `idCardId` int(1) DEFAULT '0',
        `staff_attendance_state` int(1) DEFAULT '0',
        `staff_lateness_start` time DEFAULT NULL,
        `lateness_fee` int(11) DEFAULT '0',
        `absent_fee` int(11) DEFAULT '0',
        `student_attendance_state` int(1) DEFAULT '0',
        `student_lateness_start` time DEFAULT NULL,
        `absent_days` int(1) DEFAULT '0',
        `parent_daily_alert` int(1) DEFAULT '0',
        `attendanceMessage` text NOT NULL,
        `child_pickup_state` int(1) DEFAULT '0',
        `max_pickup_person` int(1) DEFAULT '0',
        `pickup_alert` int(1) DEFAULT '0',
        `pickupMessage` text NOT NULL,
        `add_pickup` text,
        `verify_pickup` text,
        `bank_name` text,
        `account_number` text,
        `account_name` text,
        `disbursement_state` text,
        `disbursement_date` int(3) DEFAULT '0',
        `disbursement_number` tinytext,
        `deduct_lateness` int(1) DEFAULT '0',
        `deduct_absent` int(1) DEFAULT '0',
        `auto_alert_outstanding` int(1) DEFAULT '0',
        `alert_time` text,
        `online_payment` int(1) DEFAULT '0',
        `disburse_bank` text,
        `disburse_account` text,
        `disburse_account_name` text,
        `offline_payment` int(1) DEFAULT '0',
        `automatic_renewal` int(1) DEFAULT '0',
        `renewal_notify_number` text,
        `academic_session` text NOT NULL,
        `academic_term` text NOT NULL,
        `next_resumption_date` text NOT NULL,
        `smsUnit` int(11) NOT NULL,
        `subAccount` TEXT,
        `sub_date` DATE DEFAULT NULL,
        `next_sub_date` DATE DEFAULT NULL,
        `pay_no` INT(11) DEFAULT 0,
        `account_state` INT(2) DEFAULT 0,
        `quick_notice` TEXT NOT NULL,
        `pinChecker` INT(11) NOT NULL,
        `pinAmount` INT(11) NOT NULL,
        `stamp_name` TEXT,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CLASS TABLE
    $stmt2 = $conn3->prepare("CREATE TABLE IF NOT EXISTS classes (
        id INT(11) NOT NULL AUTO_INCREMENT,
        class_code TEXT(45),
        class_name TEXT(45) DEFAULT NULL,
        class_teacher INT(11) DEFAULT 0,
        date_created DATE DEFAULT NULL,
        PRIMARY KEY (id)
    )");
    $stmt2->execute();

    // FAMILY TABLE
    $stmt2 = $conn3->prepare("CREATE TABLE IF NOT EXISTS family (
        id INT(11) NOT NULL AUTO_INCREMENT,
        family_name TEXT(45),
        father_phone TEXT(45),
        mother_phone TEXT(45),
        balance INT(11),
        date_created DATE DEFAULT NULL,
        PRIMARY KEY (id)
    )");
    $stmt2->execute();

    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS subjectTeachers (
        id INT(11) NOT NULL AUTO_INCREMENT,
        classCode TEXT(45) DEFAULT NULL,
        subjectCode TEXT(20) DEFAULT NULL,
        staffCode TEXT(100) DEFAULT NULL,
        date_assigned DATE,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE SUBJECTS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS subjects (
        id INT(11) NOT NULL AUTO_INCREMENT,
        subjectCode TEXT(100),
        subjectName TEXT(100),
        subjectClass TEXT(50),
        sumTotal  INT(1) DEFAULT 1,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // TEACHERS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS teachers (
        id INT(11) NOT NULL AUTO_INCREMENT,
        fullname TEXT(100) DEFAULT NULL,
        staff_number VARCHAR(100),
        nationality TEXT(100),
        state TEXT(50),
        city TEXT(50),
        highest_qualification TEXT(50),
        gender TEXT(10),
        m_status TEXT(15),
        address TEXT(100),
        department TEXT(50),
        phone TEXT(20),
        email TEXT(100),
        password TEXT(100),
        profilePhoto TEXT(100),
        date_added DATE DEFAULT NULL,
        staff_portal INT(2) DEFAULT 1,
        bank_name TEXT(100),
        account_name TEXT(100),
        account_number TEXT(100),
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // STUDENT TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS students (
        id INT(11) NOT NULL AUTO_INCREMENT,
        fullname TEXT(100),
        student_number TEXT(50),
        gender TEXT(10),
        email TEXT(50),
        password TEXT,
        profilePhoto TEXT,
        dob DATE,
        nationality TEXT,
        state TEXT,
        city TEXT,
        blood_group TEXT,
        languages TEXT,
        last_school TEXT,
        present_class TEXT,
        student_desc TEXT,
        enrolment_year TEXT,
        father_phone TEXT,
        mother_phone TEXT,
        pickup_verification INT DEFAULT 0,
        bus_pickup INT DEFAULT 0,
        bus_dropoff INT DEFAULT 0,
        student_portal INT DEFAULT 0,
        parent_portal INT DEFAULT 0,
        walletAmount INT DEFAULT 0,
        promotion_status INT DEFAULT 0,
        accountBalance INT DEFAULT 0,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // ALUMNI TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS alumni (
        id INT(11) NOT NULL AUTO_INCREMENT,
        fullname TEXT(100),
        student_number TEXT(50),
        gender TEXT(10),
        email TEXT(50),
        password TEXT,
        profilePhoto TEXT,
        dob DATE,
        nationality TEXT,
        state TEXT,
        city TEXT,
        blood_group TEXT,
        languages TEXT,
        last_school TEXT,
        present_class TEXT,
        student_desc TEXT,
        enrolment_year TEXT,
        father_phone TEXT,
        mother_phone TEXT,
        pickup_verification INT DEFAULT 0,
        bus_pickup INT DEFAULT 0,
        bus_dropoff INT DEFAULT 0,
        student_portal INT DEFAULT 0,
        parent_portal INT DEFAULT 0,
        walletAmount INT DEFAULT 0,
        promotion_status INT DEFAULT 0,
        accountBalance INT DEFAULT 0,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // ADMINISTRATION TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS administrators (
        id INT(11) NOT NULL AUTO_INCREMENT,
        fullname TEXT(100),
        email TEXT(100),
        adminNumber TEXT(50),
        profilePhoto TEXT DEFAULT NULL,
        password TEXT,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // AFFECTIVE DOMAIN
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `affective_domain` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` text NOT NULL,
        `status` int(11) NOT NULL,
        `state` int(11) NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE ASSESSMENT
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `assessments` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `student_no` text NOT NULL,
        `class_code` text NOT NULL,
        `subject_code` text NOT NULL,
        `total` int(11) NOT NULL,
        `remark` text NOT NULL,
        `grade` text NOT NULL,
        `tutor` text NOT NULL,
        `term_admitted` text NOT NULL,
        `academic_session` text NOT NULL,
        `academic_term` text NOT NULL,
        `comment` text NOT NULL,
        `date_created` datetime NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE MID TERM SCORES
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `mid_term_score` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `student_no` text NOT NULL,
        `class_code` text NOT NULL,
        `subject_code` text NOT NULL,
        `total` int(11) NOT NULL,
        `tutor` text NOT NULL,
        `academic_session` text NOT NULL,
        `academic_term` text NOT NULL,
        `date_created` datetime NOT NULL,
        PRIMARY KEY (id)
      )");
      $stmt->execute();

    // CREATE ATTENDANCE RECORD 
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `attendancerecord` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `attendanceNumber` text NOT NULL,
        `date_created` datetime NOT NULL,
        `idType` int(11) NOT NULL COMMENT '0 for staff and 1 for student',
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE CHILDPICKUP
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `childpickup` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `fullname` tinytext,
        `pickupNumber` text,
        `nationality` tinytext,
        `state` tinytext,
        `city` tinytext,
        `relation_student` tinytext,
        `gender` tinytext,
        `m_status` tinytext,
        `occupation` tinytext,
        `phone` tinytext,
        `email` text,
        `studentNumber` text,
        `profilePhoto` text,
        `address` text,
        `added_by` text NOT NULL,
        `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE DOMAIN MAP
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `domain_map` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `student_number` text NOT NULL,
        `domain_name` text NOT NULL,
        `session` text NOT NULL,
        `term` text NOT NULL,
        `attr_id` text NOT NULL,
        `value` text NOT NULL,
        PRIMARY KEY (id)
      )");
    $stmt->execute();

    // CREATE TABLE FEES
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `fees` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `fee_name` text NOT NULL,
        `amount` int(11) NOT NULL,
        `class_code` text NOT NULL,
        `academic_term` text NOT NULL,
        `academic_session` text NOT NULL,
        `schoolFeePart` INT(2) NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE GRADING_SYSTEM
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `grading_system` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `class_code` TEXT NOT NULL,
        `froms` int(11) NOT NULL,
        `tos` int(11) NOT NULL,
        `grade` text NOT NULL,
        `remark` text NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE MESSAGES
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `messages` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `audienceBroadcast` text NOT NULL,
        `broadcastDate` date NOT NULL,
        `broadcastMessage` text NOT NULL,
        `logNumbers` text NOT NULL,
        `state` int(11) NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE PICKUPRECORDS
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `pickuprecords` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `studentNumber` text NOT NULL,
        `pickupNumber` text NOT NULL,
        `datePicked` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE PSYCHOMOTOR DOMAIN
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `psychomotor_domain` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` text NOT NULL,
        `status` int(11) NOT NULL,
        `state` int(11) NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE SCHEDULE_TIMES
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `schedule_times` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `timeCode` text NOT NULL,
        `timeName` text NOT NULL,
        `activity` text NOT NULL,
        `date_created` datetime NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // SCHEDULE_TIMINGS
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `schedule_timings` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `timeCode` text NOT NULL,
        `timeDay` text NOT NULL,
        `timeTime` text NOT NULL,
        `timeSubject` text NOT NULL,
        `timeClass` text NOT NULL,
        `timeTutor` text NOT NULL,
        `academic_session` text NOT NULL,
        `academic_term` text NOT NULL,
        `dateCreated` datetime NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE SCHEDULE_DAYS
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `schedule_days` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `dayCode` text NOT NULL,
        `dayName` text NOT NULL,
        `date_created` datetime NOT NULL,
        `state` int(11) NOT NULL DEFAULT '1',
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // PUBLIC HOLIDAYS
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `publicholiday` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `defaultId` int(11) NOT NULL,
        `holidayName` text NOT NULL,
        `holidayDate` text NOT NULL,
        `owner` int(11) NOT NULL DEFAULT '1',
        `state` int(11) NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE PARENTS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS parents (
        id INT(11) NOT NULL AUTO_INCREMENT,
        fullname TEXT(100),
        guardian_number TEXT(100),
        nationality TEXT(50),
        state TEXT(50),
        city TEXT(20),
        relation_student TEXT(20),
        gender TEXT(20),
        m_status TEXT(15),
        occupation TEXT(100),
        phone TEXT(20),
        email TEXT,
        password TEXT,
        ProfilePhoto TEXT,
        address TEXT,
        app_access INT(11) DEFAULT 1,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE PAYED_STUDENT TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS payed_student (
        id INT(11) NOT NULL AUTO_INCREMENT,
        studentNumber TEXT NOT NULL,
        className TEXT NOT NULL,
        feeId INT(11) NOT NULL,
        amount INT(11) NOT NULL,
        session INT(11) NOT NULL,
        term INT(11) NOT NULL,
        payMode INT(11) NOT NULL,
        transRef TEXT NOT NULL,
        payDate DATE NOT NULL,
        processed_by TEXT NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE OUTSTANDING FEES TABLE;
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS outstanding_fee (
        id INT(11) NOT NULL AUTO_INCREMENT,
        studentNumber TEXT NOT NULL,
        feeId INT(11) NOT NULL,
        amount INT(11) NOT NULL,
        session INT(11) NOT NULL,
        term INT(11) NOT NULL,
        date_added DATE NOT NULL,
        added_by TEXT NOT NULL,
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE EXAMINATION_QUESTION TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS examination_question (
        id INT(11) NOT NULL AUTO_INCREMENT,
        type INT(11) NOT NULL,
        question TEXT NOT NULL,
        answer TEXT NOT NULL,
        mark TEXT NOT NULL,
        choice1 TEXT,
        choice2 TEXT,
        choice3 TEXT,
        choice4 TEXT,
        class TEXT,
        subject TEXT,
        session TEXT,
        term INT(11),
        PRIMARY KEY (id)
    )");
    $stmt->execute();

    // CREATE TABLE EXAMINATION_ONGOING
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS examination_ongoing (
        id INT(11) NOT NULL AUTO_INCREMENT,
        class TEXT NOT NULL,
        subject TEXT NOT NULL,
        questions INT(11) NOT NULL AUTO_INCREMENT,
        marks INT(11) NOT NULL,
        duration INT(11) NOT NULL,
        supervisor TEXT NOT NULL,
        session  TEXT NOT NULL,
        term INT(11) NOT NULL,
        start_term TIME,
        end_term  TIME,
        PRIMARY KEY (id) 
    )");
    $stmt->execute();

    // CREATE ENROLLMENT SETTING TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS enrol_settings (
        id INT(11) NOT NULL AUTO_INCREMENT,
        enrol_session TEXT NOT NULL,
        enrol_batch TEXT NOT NULL,
        enrol_classes TEXT NOT NULL,
        enrol_startDate TEXT NOT NULL,
        enrol_endDate TEXT NOT NULL,
        enrol_feeState INT(11) NOT NULL,
        enrol_amount INT(11) NOT NULL,
        enrol_slot INT(11) NOT NULL,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // CREATE APPLICANT TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `applicants` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `fullname` text NOT NULL,
        `applicant_no` text NOT NULL,
        `health_stat` text NOT NULL,
        `nationality` text NOT NULL,
        `state` text NOT NULL,
        `city` text NOT NULL,
        `dob` date NOT NULL,
        `gender` text NOT NULL,
        `bloodGroup` text NOT NULL,
        `profilePhoto` text NOT NULL,
        `guardianName` text NOT NULL,
        `guardianNationality` text NOT NULL,
        `guardianState` text NOT NULL,
        `guardianCity` text NOT NULL,
        `guardianRel` text NOT NULL,
        `guardianGender` text NOT NULL,
        `guardianMarital` text NOT NULL,
        `guardianAddress` text NOT NULL,
        `guardianOccupation` text NOT NULL,
        `guardianPhone` text NOT NULL,
        `guardianEmail` text NOT NULL,
        `applyClass` text NOT NULL,
        `applySession` text NOT NULL,
        `applyTerm` text NOT NULL,
        `applyBatch` text NOT NULL,
        `applyType` text NOT NULL,
        `learnType` text NOT NULL,
        `prevSchoolName` text NOT NULL,
        `prevSchoolCountry` text NOT NULL,
        `prevSchoolState` text NOT NULL,
        `applyDate` date NOT NULL,
        `applyState` int(11) NOT NULL,
        PRIMARY KEY(id)
      )");
      $stmt->execute();

      //   CREATE INVENTORY STOCK TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS `inventory_items` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `itemNo` TEXT NOT NULL,
        `itemName` TEXT NOT NULL,
        `itemSize` TEXT,
        `itemColor` TEXT,
        `itemQuantity` int(11) NOT NULL,
        `itemPrice` int(11) NOT NULL,
        `itemLocation` TEXT,
        `added_by` TEXT NOT NULL,
        `added_date` DATE NOT NULL,
        PRIMARY KEY(id)
      )");
      $stmt->execute();

      //   CREATE INVENTORY SETTINGS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS inventory_settings (
        id INT(11) NOT NULL AUTO_INCREMENT,
        lowStock INT(11) NOT NULL,
        outStock INT(11) NOT NULL,
        PRIMARY KEY(id) 
    )");
    $stmt->execute();

    // INVENTORY RECORD
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS inventory_record (
        id INT(11) NOT NULL AUTO_INCREMENT,
        itemNo TEXT,
        itemQuantity INT(11) NOT NULL,
        collectedBy TEXT,
        issuedBy TEXT,
        date_collected DATE,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // SHOP PRODUCT TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS shop_product (
        id INT(11) NOT NULL AUTO_INCREMENT,
        product_id TEXT NOT NULL,
        product_name TEXT NOT NULL,
        product_quantity INT(11) DEFAULT 0,
        product_price INT(11) DEFAULT 0,
        date_added DATE,
        added_by TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // TUCK SHOP SETTINGS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS tuckshop_setting (
        id INT(11) NOT NULL AUTO_INCREMENT,
        alertWalletBalance INT(11),
        amountToAlert INT(11),
        bankName TEXT,
        accountName TEXT,
        accountNumber TEXT,
        accountState INT(11) DEFAULT 0,
        accountCode TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // CREATE RESEARCH KEYWORD TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS research_keywords (
        id INT(11) NOT NULL AUTO_INCREMENT,
        keyword TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // CREATE CBT_SUBJECTS TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS cbt_subjects (
        id INT(11) NOT NULL AUTO_INCREMENT,
        classCode TEXT,
        subjectCode TEXT,
        duration INT(11),
        supervisorCode TEXT,
        examState INT(11),
        startTime TEXT,
        endTime TEXT,
        term INT(11),
        session TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // ASSESSMENT SETTINGS TABLE
    $stmt = $conn3->prepare("CREATE TABLE assessment_settings (
        id INT(11) NOT NULL AUTO_INCREMENT,
        assId TEXT,
        `name` TEXT,
        `assClass` TEXT,
        mark INT(11),
        ass_state INT(11) DEFAULT 1,
        `mid_term` INT(2),
        `part_of` TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // PUBLISHED RESULT TABLE
    $stmt = $conn3->prepare("CREATE TABLE published_result (
        id INT(11) NOT NULL AUTO_INCREMENT,
        classCode TEXT NOT NULL,
        publish_state INT(11) DEFAULT 0,
        academic_session TEXT,
        academic_term INT(11),
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // GENERATED PIN TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS generatedPins(
        id INT(11) NOT NULL AUTO_INCREMENT,
        pin BIGINT(40),
        pinTerm INT(11),
        pinSession TEXT,
        ownerNumber TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // SCHOOL STAMP TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS school_stamp(
        id INT(11) NOT NULL AUTO_INCREMENT,
        class_code TEXT,
        stamp_name TEXT,
        date_created DATETIME NOT NULL,
        PRIMARY KEY(id)
    )");
    $stmt->execute();

    // RESUMPTION DATE TABLE
    $stmt = $conn3->prepare("CREATE TABLE IF NOT EXISTS resumption_dates(
        id INT(11) NOT NULL AUTO_INCREMENT,
        academic_session TEXT,
        academic_term TEXT,
        resume_date TEXT,
        PRIMARY KEY(id)
    )");
    $stmt->execute();
    

    // CREATING OF TABLE SHOULD END ABOVE


    // FUNCTION TO COPY FILE
    function xcopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            xcopy("$source/$entry", "$dest/$entry", $permissions);
        }

        // Clean up
        $dir->close();
        return true;
    }

    // CREATE SCHOOL FOLDER HERE FOR LOCAL HOST...................

    $folderDir = "../../$folderName";
    mkdir($folderDir);
    $content = "user = $databaseuser
    password = $databasepass
    host = localhost
    folderName = $domainName
    database = $databaseName";
    $fp = fopen("$folderDir/.env", "wb");
    fwrite($fp, $content);
    fclose($fp);
    $imageFolder = $folderDir."/SchoolImages";
    mkdir($imageFolder);
    $passportFolder = $folderDir."/passports";
    mkdir($passportFolder);

    // MOVE SCHOOL LOGO THERE
    if(empty($filename)){
        if($_FILES["schoolLogo"]["size"] > 0){
            if($_FILES["schoolLogo"]["error"]){
                $error = "An error occured with the following error. ".$_FILES["schoolLogo"]["error"];
            }else{
                $extension = @end(explode(".",$_FILES["schoolLogo"]["name"]));
    
                $filename = "logo".".".$extension;
                if(file_exists($imageFolder."/".$filename)){
                    unlink($imageFolder."/".$filename);
                }
                move_uploaded_file($_FILES["schoolLogo"]["tmp_name"], $imageFolder."/".$filename);
    
            }
        }
    }else{
        $extension = @end(explode(".", $filename));
        $formerName = $filename;
        $filename = "logo".".".$extension;
        $tempFolderName = $_SERVER["DOCUMENT_ROOT"]."/SchoolMate/tempImages";
        if(file_exists($tempFolderName."/".$formerName)){
            copy($tempFolderName."/".$formerName, $imageFolder."/".$filename);
            unlink($tempFolderName."/".$formerName);
        }
    }


    // COPY CONNECTION FILE
    $src = $_SERVER["DOCUMENT_ROOT"]."/schoolmate/userConnect";
    $dst = $_SERVER["DOCUMENT_ROOT"]."/schoolmate/$folderName";
    xcopy($src, $dst, $permissions = 0755);

    // INSERTING STARTS HERE

    // CLASS INSERTION
    $date = date("Y-m-d");
    $currentVal = 0;
    for($i = 1; $i < 7; $i++){
        $class_name = "Basic ".$i;
        $class_code = "class00".$i;
        $stmt = $conn3->prepare("INSERT INTO classes (class_code, class_name, date_created) VALUES (?,?,?)");
        $stmt->bind_param("sss", $class_code, $class_name, $date);
        $stmt->execute();
    }

    for($i = 7; $i < 10; $i++){
        $currentVal = $i;
        $class_name = "Basic ".$i;
        if($i > 9){
            $class_code = "class0".$i;
        }else{
            $class_code = "class00".$i;
        }
        $stmt = $conn3->prepare("INSERT INTO classes (class_code, class_name, date_created) VALUES (?,?,?)");
        $stmt->bind_param("sss", $class_code, $class_name, $date);
        $stmt->execute();
    }

    for($i = 1; $i < 4; $i++){
        $currentVal += 1;
        $class_name = "SS ".$i;
        $class_code = "class0".$currentVal;
        $stmt = $conn3->prepare("INSERT INTO classes (class_code, class_name, date_created) VALUES (?,?,?)");
        $stmt->bind_param("sss", $class_code, $class_name, $date);
        $stmt->execute();
    }

    // SCHEDULE_DAYS INSERTION
    $daysName = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $date = date("Y-m-d");
    foreach ($daysName as $key => $value) {
        # code...
        $key += 1;
        $dayCode = "day00".$key;
        $dayState = 0;
        $stmt = $conn3->prepare("INSERT INTO schedule_days (dayCode, dayName, date_created, state) VALUES (?,?,?,?)");
        $stmt->bind_param("sssi", $dayCode, $value, $date, $dayState);
        $stmt->execute();
    }

    // SETTINGS INSERTION
    $category = json_encode($category);
    $stmt = $conn3->prepare("INSERT INTO settings (school_name, schoolAddress, domain_name, reg_date, categories, country, state, city, website, email, telephone, logo, currency, slogan, grading_system, school_color, school_curriculum, modules, sub_date, next_sub_date, account_state, smsUnit) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param("ssssssssssssssssssssii", $schoolNameUntouched, $schoolAddress, $domainName, $date, $category, $country, $state, $city, $website, $email, $phone, $filename, $currency, $slogan, $grading, $color, $curriculum, $module, $subDate, $nextSub, $accountState, $smsUnit);
    $stmt->execute();


    //  SCHOOL INSERTION
    $stmt = $createConn->prepare("SELECT fullname, password, refCode FROM users WHERE email = ?");
    $stmt->bind_param("s", $adminEmail);
    $stmt->execute();
    $stmt_result = get_result($stmt);
    $row = array_shift($stmt_result);
    $adminName = $row["fullname"];
    $password = $row["password"];
    $refCode = $row["refCode"];

    if(!empty($refCode)){
        // INSERT INTO PARTNERS TABLE
        $stmt = $createConn->prepare("INSERT INTO school_partner (partnerCode, schoolName, schoolData) VALUES (?,?,?)");
        $stmt->bind_param("sss", $refCode, $schoolNameUntouched, $databaseName);
        $stmt->execute();
    }

    $stmt1 = $createConn->prepare("INSERT INTO schools (school_name, domain_name, admin_name, admin_email) VALUES (?,?,?,?)");
    $stmt1->bind_param("ssss", $schoolNameUntouched, $domainName, $adminName, $adminEmail);
    $stmt1->execute();

    // ADMINISTRATION INSERTION
    $upperDomain = ucwords($domainName);
    $uniqueNumber = date("Y")."/"."Admn/".$upperDomain."/001";
    $stmt = $conn3->prepare("INSERT INTO administrators ( fullname, email, adminNumber, password) VALUES (?,?,?,?)");
    $stmt->bind_param("ssss", $adminName, $adminEmail, $uniqueNumber, $password);
    $stmt->execute();

    // INSERT INTO CLASSWORKS STAFF
    $role = "admin";
    $stmt = $createConn->prepare("INSERT INTO staffs (fullname, email, uniqueNumber, password, school_domain, role) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param("ssssss", $adminName, $adminEmail, $uniqueNumber, $password, $domainName, $role);
    $stmt->execute();
    // INSERTING ENDS HERE

}else{
    // ERROR SCHOOL ALREDY EXIST
}

?>