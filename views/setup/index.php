<?php
  if(!isset($_GET["verifier"])){
    echo "<script>window.location.assign('../authentication')</script>";
  }else{
    $verifyEmail = base64_decode(htmlentities(trim($_GET["verifier"])));
    // $verifyEmail = htmlentities(trim($_GET["verifier"]));
    include_once ("logic/setupbtn.php");
    $modules = $setup->allModules();
  }
?>
<!DOCTYPE html>
<html>

<head>
  <title>ClassWorks - School Management Solution | Create a school</title>
  <!-------------------- START - Meta -------------------->
  <?php include ("../../includes/meta.php"); ?>
  <!-------------------- END - Meta -------------------->
  <?php include_once ("../../includes/styles.php"); ?>
  <style type="text/css">
    a[disabled="disabled"] {
        pointer-events: none;
    }
</style>
</head>

<body style="height:auto" class="with-pattern full-screen">

  <div style="height:auto" class="all-wrapper with-pattern">
    <div class="layout-w">
      <div class="content-w3">
        <div class="content-i">
          <div class="content-box" style="padding-left: 20rem; padding-right: 20rem">
            <div class="content-i">
              <div class="content-box">
                <div class="element-wrapper">
                  <div class="element-box">
                    <form method="POST" action="#" enctype="multipart/form-data">
                      <div class="steps-w">
                        <div class="step-triggers">
                          <a class="step-trigger active" href="#stepContent1" style="font-weight: 600">DETAILS</a>
                          <a class="step-trigger" href="#stepContent2" disabled="disabled" style="font-weight: 600">SETUP</a>
                        </div>
                        <div class="step-contents">
                          <div class="step-content active" id="stepContent1">
                            <div class="form-group">
                              <label for="">School Category(s)</label>
                              <select class="form-control select2" multiple="true"  name="category[]" required>
                                <option value="Creche">Creche</option>
                                <option value="Kindergarten">Kindergarten</option>
                                <option selected value="Nursery">Nursery</option>
                                <option selected value="Primary">Primary</option>
                                <option selected value="Secondary">Secondary</option>
                              </select>
                            </div>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label for="">School Official Name</label>
                                  <input class="form-control" placeholder="Georgenia High School" type="text" name="schoolName" required>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="">School Prefix Name</label>
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" name="domainName" id="domainvAL" placeholder="Georgenia" required aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                  <span class="input-group-text" id="basic-addon2">.Classworks.xyz</span>
                                </div>
                              </div>  
                            </div>
                            <div id="message"></div>
                            <div class="row">
                              <div class="col-sm-12">
                                <div class="form-group">
                                  <label for="">School Address</label>
                                  <input class="form-control" placeholder="Your School address" type="text" name="schoolAddress" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-sm-6">
                                <label for="">Country</label>
                                <select class="form-control" name="country">
                                  <option selected value="nigeria">Nigeria</option>
                                  <option value="algeria">Algeria</option>
                                  <option value="angola">Angola</option>
                                  <option value="benin">Benin</option>
                                  <option value="botswana">Botswana</option>
                                  <option value="burkina Faso">Burkina Faso</option>
                                  <option value="burundi">Burundi</option>
                                  <option value="cameroon">Cameroon</option>
                                  <option value="cape-verde">Cape Verde</option>
                                  <option value="central-african-republic">Central African Republic</option>
                                  <option value="chad">Chad</option>
                                  <option value="comoros">Comoros</option>
                                  <option value="congo-brazzaville">Congo - Brazzaville</option>
                                  <option value="congo-kinshasa">Congo - Kinshasa</option>
                                  <option value="ivory-coast">Côte d’Ivoire</option>
                                  <option value="djibouti">Djibouti</option>
                                  <option value="egypt">Egypt</option>
                                  <option value="equatorial-guinea">Equatorial Guinea</option>
                                  <option value="eritrea">Eritrea</option>
                                  <option value="ethiopia">Ethiopia</option>
                                  <option value="gabon">Gabon</option>
                                  <option value="gambia">Gambia</option>
                                  <option value="ghana">Ghana</option>
                                  <option value="guinea">Guinea</option>
                                  <option value="guinea-bissau">Guinea-Bissau</option>
                                  <option value="kenya">Kenya</option>
                                  <option value="lesotho">Lesotho</option>
                                  <option value="liberia">Liberia</option>
                                  <option value="libya">Libya</option>
                                  <option value="madagascar">Madagascar</option>
                                  <option value="malawi">Malawi</option>
                                  <option value="mali">Mali</option>
                                  <option value="mauritania">Mauritania</option>
                                  <option value="mauritius">Mauritius</option>
                                  <option value="mayotte">Mayotte</option>
                                  <option value="morocco">Morocco</option>
                                  <option value="mozambique">Mozambique</option>
                                  <option value="namibia">Namibia</option>
                                  <option value="niger">Niger</option>
                                  <option value="nigeria">Nigeria</option>
                                  <option value="rwanda">Rwanda</option>
                                  <option value="reunion">Réunion</option>
                                  <option value="saint-helena">Saint Helena</option>
                                  <option value="senegal">Senegal</option>
                                  <option value="seychelles">Seychelles</option>
                                  <option value="sierra-leone">Sierra Leone</option>
                                  <option value="somalia">Somalia</option>
                                  <option value="south-africa">South Africa</option>
                                  <option value="sudan">Sudan</option>
                                  <option value="south-sudan">Sudan</option>
                                  <option value="swaziland">Swaziland</option>
                                  <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                                  <option value="tanzania">Tanzania</option>
                                  <option value="togo">Togo</option>
                                  <option value="tunisia">Tunisia</option>
                                  <option value="uganda">Uganda</option>
                                  <option value="western-sahara">Western Sahara</option>
                                  <option value="zambia">Zambia</option>
                                  <option value="zimbabwe">Zimbabwe</option>
                                  <option value="Others">Others</option>
                                </select>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="">State/Provice</label>
                                  <input class="form-control" placeholder="Lagos" type="text" name="state" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="">City/Region</label>
                                  <input class="form-control" placeholder="Ikeja" type="text" name="city" required>
                                </div>
                              </div>
                              <div class="form-group col-sm-6">
                                <label for="">Website
                                  <span style="font-size:0.7rem; font-weight:400px;">(Optional)</span>
                                </label>
                                <input class="form-control" placeholder="Georgenia.com" type="text" name="website">
                              </div>
                            </div>
                            <div class="row">
                              <div class="form-group col-sm-6">
                                <label for="">Email</label>
                                <input class="form-control" placeholder="Georgenia@gmail.com" type="email" name="email" required>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="">Telephone</label>
                                  <input class="form-control" placeholder="08188216769" type="text" name="phone" required>
                                </div>
                              </div>
                            </div>
                            <div class="form-buttons-w text-right">
                              <a class="my_hover_up btn btn-primary step-trigger-btn" id="CntBtn" style="color: #ffffff"> Continue to setup</a>
                            </div>
                          </div>

                          <!-- FORM 2 -->
                          <div class="step-content" id="stepContent2">
                                <div class="row">
                                  <div class="form-group col-sm-6">
                                    <label for="">School Logo</label>
                                    <input style="" class="form-control" placeholder="School Logo" accept="image/*" type="file" name="schoolLogo" required>
                                  </div>
                                  <div class="form-group col-sm-6">
                                    <label for="">Currency</label>
                                    <select id="school_currency" class="form-control" name="currency" required>
                                      <option value="Naira">Naira (&#8358;)</option>
                                      <option value="Dollar">Dollar (&#036;)</option>
                                      <option value="Pounds">Pounds (&#163;)</option>
                                      <option value="Euro">Euro (&#128;)</option>
                                      <option value="Cedi">Cedi (&#8373;)</option>
                                      <option value="Rand">Rand (&#x52;)</option>
                                      <option value="Shilling">Shilling (Ksh)</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-sm-6">
                                    <label for="">School Slogan/Motto</label>
                                    <input class="form-control" placeholder="In God we trust" type="text" name="slogan" required>
                                  </div>
                                  <div class="form-group col-sm-6">
                                    <label for="">Grading System</label>
                                    <select class="form-control" name="grading" >
                                      <option value="Percentage">Percentage</option>
                                      <option value="Grade">Grades</option>
                                      <option value="Position">Position</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="form-group col-sm-6">
                                    <label for="">School Official Color</label>
                                    <input readonly class="form-control" value="#0318F0" type="text" name="color">
                                  </div>
                                  <div class="form-group col-sm-6">
                                    <label for="">Curriculum Used</label>
                                    <select class="form-control" name="curriculum" required>
                                      <option value="nigeria">Nigerian</option>
                                      <option value="american">American</option>
                                      <option value="british">British</option>
                                      <option value="custom">Custom</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Enter Free Trial Code</label>
                                  <input class="form-control" placeholder="Classwork's Free Trial Code" type="text" name="trialCode">
                                </div>
                                <div class="form-buttons-w text-right">
                                  <input type="submit" value="OK I'm Done" name="trial" class="my_hover_up btn btn-primary setUpBtn">
                                </div>
                              </div>

                            </div>
                          </div>
                    </form>
                    </div>
                    </div>


                     <!-------------------- START - Top Bar -------------------->
                     <?php include("../../includes/support.php") ?>
                      <!-------------------- END - Top Bar -------------------->
            
            <!-- MODALS -->


        <!-- <div style="margin-top:20px" aria-hidden="true" class="onboarding-modal modal animated" id="createSchoolModal" role="dialog"
          tabindex="-1">
          <div class="modal-dialog modal-centered" role="document">
            <div class="modal-content text-center">
              <button aria-label="Close" class="close" data-dismiss="modal" type="button">
              </button>
              <div class="onboarding-media">
                <img alt="" src="../../images/bigicon5.png" width="200px">
              </div>
              <div class="onboarding-content with-gradient">
                <h4 class="onboarding-title">CREATE A NEW SCHOOL</h4>
                <button id="createSchoolModalClose" hidden aria-label="Close" class="close" data-dismiss="modal" type="button">
                  <span aria-hidden="true"> &times;</span>
                </button>
                <div class="onboarding-text">Your are few steps away from automating your school activities and management. We will take you through setting
                  up a new school account instantly and get u running within minutes.</div>
                <a style="font-size:18px; padding-top:7px; padding-bottom:35px; padding-right:20px; padding-left:20px;  margin-top:10px; background-color: #08ACF0; border-width:0"
                  class="my_hover_up btn btn-white btn-sm create-school-btn" href="#">
                  <i style="margin-top:50px">+</i>
                  <span>Get started with creation</span>
                </a>
              </div>
            </div>
          </div> -->


<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>