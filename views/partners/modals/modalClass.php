<?php
if(!isset($_SESSION["folderName"])){
    session_start();
}
    include_once ("../../logic/config.php");

    class modal extends logic {

        private $pk_secret;

        function outputRefine($value){
            if(isset($value) && !empty($value)){
                return ucfirst($value);
            }else{
                return "N/A";
            }
        }

        function get_result( $Statement ) {
            $RESULT = array();
            $Statement->store_result();
            for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
                $Metadata = $Statement->result_metadata();
                $PARAMS = array();
                while ( $Field = $Metadata->fetch_field() ) {
                    $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
                }
                call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
                $Statement->fetch();
            }
            $object = (object) $RESULT;
            return $RESULT;
        }

    }

?>