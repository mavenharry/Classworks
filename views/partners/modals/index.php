<?php
include_once ("modalbtn.php");
?>
<!-- CHANGE PASSWORD MODAL-->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="newAcademicModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Change Password</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Old Password <span style="color:#F91F14">*</span></label>
                        <input required name="oldPassword" class="form-control" placeholder="Old Password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> New Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Repeat Password <span style="color:#F91F14">*</span></label>
                        <input required name="newPassword2" class="form-control" placeholder="New Password" type="password">
                        </div>
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" required type="checkbox">I hereby verify that this is a valid academic info.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="changePassword" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- CHANGE BANK DETAILS MODAL-->
<div aria-hidden="true" class="onboarding-modal modal fade animated" id="accountModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-centered" role="document">
        <div class="modal-content text-center">
            <div class="onboarding-content with-gradient">
              <h4 class="onboarding-title" style="font-size:1.3rem;text-align:right; margin-top:-40px; color:#8095A0">Account Details</h4>
              <form id="formValidate" method="POST">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Bank Name <span style="color:#F91F14">*</span></label>
                        <select class="form-control" name="bankName">
                            <?php
                            //CHECK IF COUNTRY IS NIGERIA, KENYA OR GHANA
                            $countries = array("nigeria", "kenya", "ghana");
                            $country = strtolower("nigeria");
                            if(in_array($country, $countries)){
                            $json = file_get_contents('../../includes/'.$country.'_banks.json');
                            $json_data = json_decode($json,true);
                            foreach ($json_data as $key => $value) { ?>
                                <option <?php echo $myDetails->bankName == $json_data[$key]["name"] ? "selected" : ""?> value="<?php echo $json_data[$key]["name"]?>"><?php echo strtoupper($json_data[$key]["name"]) ?></option>
                            <?php }
                            }else{ ?>
                                <option value="">Your country is not available yet!</option>
                            <?php }?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Bank Account Number <span style="color:#F91F14">*</span></label>
                        <input required name="accountNumber" value="<?php echo $myDetails->accountNumber == "" ? "" : $myDetails->accountNumber; ?>" class="form-control" placeholder="Bank Account Number" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                        <label for=""> Bank Account Name <span style="color:#F91F14">*</span></label>
                        <input required name="accountName" value="<?php echo $myDetails->accountName == "" ? "" : $myDetails->accountName; ?>" class="form-control" placeholder="Bank Account Name" type="text">
                        </div>
                    </div>
                </div>

                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input class="form-check-input" required type="checkbox">I hereby verify that these are my valid account info.</label></div>
                <div class="form-buttons-w" style="margin-top:15px;">
                    <button name="accountInfo" class="my_hover_up btn btn-primary" type="submit" style="border-color:#08ACF0; background-color:#08ACF0"> Update this now</button>
                    <button class="my_hover_up btn btn-primary create-school-btn" type="button" style="color:#afbabf; border-color:#afbabf; background-color:transparent"> Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>