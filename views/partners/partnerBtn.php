<?php
include_once ("partnerClass.php");
$partner = new partnerClass;

if(isset($_POST["accountInfo"])){
    $bankName = htmlentities(trim($_POST["bankName"]));
    $accountName = htmlentities(trim($_POST["accountName"]));
    $accountNumber = htmlentities(trim($_POST["accountNumber"]));

    $reply = $partner->subAccount($bankName, $accountName, $accountNumber);
    switch($reply){
        case 'true':
            $mess = "Your account has been created";
            break;
        case 'error':
            $err = "An unexpected error occurred while trying to create account. Please try again later";
            break;
        case 'exist':
            $err = "An account already exist with this account details. Please confirm and try again";
            break;
        case 'updated':
            $mess = "Account details has been updated successfully";
            break;
        case 'updateError':
            $err = "An error occurred while trying to update your account details. Please contact support";
            break;
        default:
            $err = "An unexpected error occurred while trying to create account. Please try again later";
            break;
    }
}

if(isset($_POST["changePassword"])){
    $oldPassword = htmlentities(trim($_POST["oldPassword"]));
    $newPassword = htmlentities(trim($_POST["newPassword"]));
    $newPassword2 = htmlentities(trim($_POST["newPassword2"]));

    if($newPassword == $newPassword2){
        $reply = $partner->changePassword($oldPassword, $newPassword);
        switch($reply){
            case 'changed':
                $mess = "Password has been changed successfully";
                break;
            case 'error':
                $err = "An error occurred while trying to update password. Please try again later";
                break;
            case 'wrongpassword':
                $err = "The password you entered is wrong";
                break;
            default:
                $err = "An unexpected error occurred while trying to update your password. Please contact support";
                break;
        }
    }else{
        $err = "New passwords do not match";
    }
}

if(isset($_POST["uploadPhoto"])){
    // MOVE TO TEMP FOLDER FIRST
    $imageFolder = "./passports/";
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $err = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));

            $filename = str_replace("/","_",$_SESSION["uniqueNumber"]).".".$extension;
            $_SESSION["profilePhoto"] = $filename;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);
            $reply = $partner->updateProfilePicture($filename);
            if($reply === true){
                $mess = "Profile Picture has been updated";
            }else{
                $err = "An error occured while trying to update your profile picture. Please try again later";
            }

        }
    }
}
?>