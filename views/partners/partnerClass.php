<?php
session_start();
include_once ("../../logic/config.php");

class partnerClass extends logic {
    
    private $systemKeys = array();

    function setPartnerDetails(){
        $stmt = $this->dconn->prepare("SELECT profilePhoto FROM partners WHERE partner_no = ?");
        $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        if(!empty($row["profilePhoto"])){
            $_SESSION["profilePhoto"] = $row["profilePhoto"];
        }
        $stmt->close();
    }

    function homeCount(){
        $this->setPartnerDetails();
        $stmt = $this->dconn->prepare("SELECT SUM(accountState = 1) AS active, SUM(accountState = 0) AS expired FROM school_partner WHERE partnerCode = ?");
        $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->active = $row["active"] > 0 ? $row["active"] : 0;
        $obj->inactive = $row["expired"] > 0 ? $row["expired"] : 0;
        $obj->total = $row["active"] + $row["expired"];
        $stmt->close();
        return $obj;
    }

    function getSchools($state){
        switch($state){
            case 'all':
                $code = 2;
                break;
            case 'active':
                $code = 1;
                break;
            case 'expired':
                $code = 0;
                break;
            default:
                $code = 2;
                break;
        }
        
        if($code == 2){
            $stmt = $this->dconn->prepare("SELECT schoolData FROM school_partner WHERE partnerCode = ?");
            $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        }else{
            $stmt = $this->dconn->prepare("SELECT schoolData FROM school_partner WHERE partnerCode = ? AND accountState = ?");
            $stmt->bind_param("si", $_SESSION["uniqueNumber"], $code);
        }
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $schoolsArray = array();
        foreach($stmt_result as $key => $value){
            // echo $value["schoolData"];
            mysqli_select_db($this->dconn, $value["schoolData"]);
            $sid = 1;
            $stmt = $this->dconn->prepare("SELECT settings.school_name, settings.next_sub_date, settings.modules, settings.account_state, settings.telephone FROM settings WHERE settings.id = ?");
            $stmt->bind_param("i", $sid);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $stmt = $this->dconn->prepare("SELECT COUNT(id) AS num FROM students");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row2 = array_shift($stmt_result)["num"];
            $obj = new stdClass();
            $obj->schoolName = $row["school_name"];
            $obj->nextSubDate = $row["next_sub_date"];
            $planDetails = $this->getPlanDetails($row["modules"]);
            $obj->plan = $planDetails->price;
            $obj->title = $planDetails->title;
            $total = $planDetails->price * $row2;
            $obj->myCharge = 0.4 * $total < 1 ? 0 : 0.4 * $total;
            $obj->schoolCharge = $total;
            $obj->accountState = $row["account_state"];
            $obj->phone = $row["telephone"];
            $obj->students = $row2;
            $schoolsArray[] = $obj;
        }
        $stmt->close();
        mysqli_select_db($this->dconn, "classworks");
        return $schoolsArray;
    }

    function getDetails(){
        $stmt = $this->dconn->prepare("SELECT * FROM partners WHERE partner_no = ?");
        $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->bankName = $row["bankName"];
        $obj->accountNumber = $row["accountNumber"];
        $obj->accountName = $row["accountName"];
        $stmt->close();
        return $obj;
    }

    function getPlanDetails($module){
        switch($module){
            case 1:
                $title = "Bronze";
                break;
            case 2:
                $title = "Silver";
                break;
            case 3:
                $title = "Gold";
                break;
            default:
                $title = "N/A";
                break;
        }
        mysqli_select_db($this->dconn, "classworks");
        $stmt = $this->dconn->prepare("SELECT per_student FROM pricing WHERE package = ?");
        $stmt->bind_param("i", $module);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        $obj->price = array_shift($stmt_result)["per_student"];
        $obj->title = $title;
        $stmt->close();
        return $obj;
    }

    private function getKeys(){
        $stmt = $this->dconn->prepare("SELECT * FROM systemkeys");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $systemKeys = array();
        while($row = array_shift($stmt_result)){
            $systemKeys[$row["keyName"]] = $row["keyValue"];
        }
        return $systemKeys;
        $stmt->close();
    }


    function subAccount($bankName, $accountName, $accountNumber){
        $this->systemKeys = $this->getKeys();
        $stmt = $this->dconn->prepare("SELECT subAccountCode FROM partners WHERE partner_no = ?");
        $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $accountCode = array_shift($stmt_result)["subAccountCode"];
        $key = $this->systemKeys["paystack_secret"];
        if(empty($accountCode)){
            $action = 1;
            include_once ("modals/epaymentsetup.php");
            if($result->status == 1 && $result->message == "Subaccount created"){
                $accountCode = $result->data->subaccount_code;
                $accountState = 1;
                $stmt = $this->dconn->prepare("UPDATE partners SET subAccountCode = ?, account_state = ?, bankName = ?, accountNumber = ?, accountName = ? WHERE partner_no = ?");
                $stmt->bind_param("sissss", $accountCode, $accountState, $bankName, $accountNumber, $accountName, $_SESSION["uniqueNumber"]);
                if($stmt->execute()){
                    $stmt->close();
                    return "true";
                }else{
                    $stmt->close();
                    return "error";
                }
            }else{
                $stmt->close();
                return "paystackerror";
            }
        }else{
            // UPDATE BANK DETAILS AND PAYSTACK
            $action = 1;
            include_once ("modals/epaymentupdate.php");
            if($result->status == 1 && $result->message == "Subaccount updated"){
                $accountCode = $result->data->subaccount_code;
                $accountState = 1;
                $stmt = $this->dconn->prepare("UPDATE partners SET subAccountCode = ?, account_state = ?, bankName = ?, accountNumber = ?, accountName = ? WHERE partner_no = ?");
                $stmt->bind_param("sissss", $accountCode, $accountState, $bankName, $accountNumber, $accountName, $_SESSION["uniqueNumber"]);
                if($stmt->execute()){
                    $stmt->close();
                    return "updated";
                }else{
                    $stmt->close();
                    return "error";
                }
            }else{
                $stmt->close();
                return "updateError";
            }
        }
    }

    function changePassword($oldPassword, $newPassword){
        $stmt = $this->dconn->prepare("SELECT password FROM partners WHERE partner_no = ?");
        $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $pass = array_shift($stmt_result)["password"];
        if(password_verify($oldPassword, $pass)){
            // OLD PASSWORD MATCH
            $stmt = $this->dconn->prepare("UPDATE partners SET password = ? WHERE partner_no = ?");
            $stmt->bind_param("ss", password_hash(strtolower($newPassword), PASSWORD_BCRYPT), $_SESSION["uniqueNumber"]);
            if($stmt->execute()){
                $stmt->close();
                return "changed";
            }else{
                $stmt->close();
                return "error";
            }
        }else{
            $stmt->close();
            return "wrongpassword";
        }
    }

    function updateProfilePicture($filename){
        $stmt = $this->dconn->prepare("UPDATE partners SET profilePhoto = ? WHERE partner_no = ?");
        $stmt->bind_param("ss", $filename, $_SESSION["uniqueNumber"]);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }
}
?>