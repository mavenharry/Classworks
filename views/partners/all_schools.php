<?php include_once ("partnerBtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<?php
    if(isset($_GET["key"])){
        $state = htmlentities(trim($_GET["key"]));
    }else{
        $state = "all";
    }
    $text = $state != "all" ? $state : "";
    $schools = $partner->getSchools($state);
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar3.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL MY <?php echo strtoupper($text)?> SCHOOL(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>Serial no.</th>
                                                <th>School Name</th>
                                                <th>Plan</th>
                                                <th>Students</th>
                                                <th>Next Sub.</th>
                                                <th>Amount</th>
                                                <th>Payable</th>
                                                <th>Contact</th>
                                                <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                foreach($schools as $key => $value){ ?>
                                                    <tr class="my_hover_up">
                                                        <td class="nowrap">
                                                            <span><?php echo $key + 1?>.</span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->schoolName);?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->title); echo $value->title == "N/A" ? "" : " (&#8358;".$value->plan.")"?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->students;?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <span><?php echo $value->nextSubDate;?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <span>&#8358;<?php echo $value->schoolCharge?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <span>&#8358;<?php echo $value->myCharge; ?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <span><?php echo $value->phone;?></span>
                                                        </td>
                                                        <?php
                                                            if($value->accountState == 1){ ?>
                                                                <!-- ACTIVE -->
                                                                <td class="text-center">
                                                                    <a style="font-size:1rem; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="#">Active</a>
                                                                </td>
                                                            <?php }else{ ?>
                                                                <!-- EXPIRED -->
                                                                <td class="text-center">
                                                                    <a style="font-size:1rem; text-decoration:none; background-color:#e93737; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="#">Expired</a>
                                                                </td>
                                                            <?php }
                                                        ?>
                                                    </tr>
                                                <?php }
                                            ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(30 < 20){ ?>
                                                <div style="float: right;" class-"element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>