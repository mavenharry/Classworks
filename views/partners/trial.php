if(count($staffSubjects) != 0){
        foreach ($staffSubjects as $key => $value) {
            # code...
            $stmt2 = $this->uconn->prepare("SELECT classes.class_code, subjects.subjectClass FROM classes INNER JOIN subjects ON subjects.subjectClass = classes.class_name WHERE subjects.subjectCode = ?");
            $stmt2->bind_param("s", $value);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $row = array_shift($stmt2_result);
            $date = Date("Y-m-d");
            $stmt3 = $this->uconn->prepare("INSERT INTO subjectteachers (classCode, subjectCode, staffCode, date_assigned) VALUES (?,?,?,?)");
            $stmt3->bind_param("ssss", $row["class_code"], $value, $staffNumber, $date);
            $stmt3->execute();
            $stmt2->close();
            $stmt3->close();
        }
    }
    $stmt = $this->dconn->prepare("INSERT INTO staffs (fullname, email, uniqueNumber, password, school_domain, role) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param("ssssss", strtolower($staffName), strtolower($staffEmail), strtolower($staffNumber), password_hash($password, PASSWORD_BCRYPT), $_SESSION["folderName"], strtolower($staffDepartment));
    $stmt->execute();
    $message = "Hi, ".ucwords($staffName).", your staff account for ".$this->getSchoolDetails()->school_name." on https://www.ClassWorks.xyz has been created. Your login details are; \n Email:".$staffEmail."\n Staff Number: ".ucwords($staffNumber)."\n Password: ".$password.". Your password can be changed in your account. \n Best Regards";
    $reply = $this->sendSMS($message, $this->getSchoolDetails()->domain_name, $staffPhone);
    if($reply === true){
        $stmt->close();
        return "true";
    }else{
        $stmt->close();
        return $reply;
    }