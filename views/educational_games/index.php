<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>ClassWorks - School Automation Solution - Chess</title>
    
<link rel="stylesheet" href="css/style.css">

  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="../../userConnect/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../userConnect/css/style.php">
	<link rel="stylesheet" href="../../userConnect/css/responsive.css"> 

  
</head>

<body>

  <h2 style="text-align:center; margin:50px; font-size:5rem; font-family:sans-serif; font-weight:bold; color:#08ACF0">Educational Games</h2>
		
		<section class="featured-box">
			<div class="container" style="width: 80%;">
				<div class="row">

					<div class="col-sm-4 col-md-4">
						<div class="box-1" style="background:url() no-repeat center;">
							<a style="text-align:center; padding-top:75px; font-size:5rem; text-decoration: none;" href="./chess" > 
								Chess
							</a>
						</div>
          </div>
          
					<!-- <div class="col-sm-4 col-md-4">
						<div class="box-2">
              <a style="text-align:center; padding-top:80px; font-size:5rem;" href="../views/assessment/resultChecker" > 
                Chess
              </a>
						</div>
          </div> -->
          
					<!-- <div class="col-sm-4 col-md-4">
						<div class="box-3">
              <a style="text-align:center; padding-top:80px; font-size:5rem;" href="../views/assessment/resultChecker" > 
                SPELLS
              </a>
						</div>
          </div> -->
          
				</div>
			</div>
		</section>


<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="js/index.js"></script>

</body>

</html>
