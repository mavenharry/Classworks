<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>ClassWorks | Chess</title>
  <?php include("../../../includes/meta.php") ?>
  <link rel="stylesheet" href="css/style.css">
</head>

<body>

  <div style="float:right; margin:7.5rem; text-align:center;">
  <h4 style="color:#8095A0; font-size:1.3rem; font-family:arial">Your turn to play</h4>
  <hr style="margin-bottom:25px;">
  <button onclick="restartGame()" style="cursor:pointer; background:#08ACF0; border:none; color:#fff; border-radius:5px; font-size:1.2rem; padding-top:10px; padding-bottom:10px; padding-left:20px; padding-right:20px">Restart Game</button>
  </div>

  <?php include("../../../includes/support.php") ?>
  
  <script src='../../../addons/jquery/dist/jquery.min.js'></script>
  
  <script  src="js/index.js"></script>

</body>

</html>
