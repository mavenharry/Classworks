<?php
    include_once ("attendancebtn.php");

    if(isset($_GET["attendanceNumber"])){
        $attendanceNumber = htmlentities(trim($_GET["attendanceNumber"]));
        $reply = $attendanceClass->takeAttendance($attendanceNumber);
    }else{
        echo "Please use the barcode reader to scan your ID card";
    }
?>