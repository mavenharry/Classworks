<?php include_once ("attendancebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Attendance</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $attendanceClass->getSchoolDetails();
?>

<body  class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Attendance", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
        
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchStaffAttendanceModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-37"></i>
                                        <span style="font-size: .9rem;">Search for a staff</span>
                                    </a>
                                    <a data-target=".bd-example-modal-sm" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                    <form method="POST" action="" style="display: inline;">
                                        <a class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <select name="showPeriod" onchange="this.form.submit()" id="showPeriod" style="font-size:1rem;  border-radius:6px; border: 2px solid #1BB4AD" class="form-control form-control-sm bright">
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "Today" ? "selected" : "";?> value="Today">Today</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "Last Week" ? "selected" : "";?> value="Last Week">Last Week</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "This Month" ? "selected" : "";?> value="This Month">This Month</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "Last Month" ? "selected" : "";?> value="Last Month">Last Month</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "This Quarter" ? "selected" : "";?> value="This Quarter">This Quarter</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "Last Quarter" ? "selected" : "";?> value="Last Quarter">Last Quarter</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "This Year" ? "selected" : "";?> value="This Year">This Year</option>
                                                <option <?php echo isset($_POST["showPeriod"]) && $_POST["showPeriod"] == "Last Year" ? "selected" : "";?> value="Last Year">Last Year</option>
                                            </select>  
                                        </a>
                                    <form>
                                </div>
                                <h6 class="element-header"><?php echo isset($_POST["showPeriod"]) ? strtoupper(htmlentities(trim($_POST["showPeriod"])))."'S" : "ALL";?> STAFF'S ATTENDANCE</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                    <?php include_once ("staffAttendanceTable.php"); ?>
                                </div>
                                        

                                        <!-- <div style="float: right;" class-"pull-left element-actions">
                                        <a style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Previous</span>
                                        </a>
                                        <a style="margin-left:5px; margin-right:-10px; color:#FFFFFF; border-radius:100px; width:30px; height:30px;  background-color:#c7d4da; border:#c7d4da" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <h2 style="font-size: 1rem; text-align:center; color:#FFFFFF">3</h2>
                                        </a>
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Next</span>
                                        </a>
                                        </div> -->

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>