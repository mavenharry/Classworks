<?php
    if(isset($_POST["showPeriod"])){
        $showPeriod = htmlentities(trim($_POST["showPeriod"]));
    }else{
        $showPeriod = "Today";
    }
    $mark = date("Y-m-d");
    $date = new DateTime($mark);
    switch ($showPeriod) {
        case 'Today':
            # code...
            $date->modify('this week');
            $startTime = $date->format('Y-m-d');
            $date->modify('friday');
            $date->modify('+ 1 day');
            $endTime = $date->format('Y-m-d');
            $displayType = "All";
            break;
        case 'Last Week':
            $date->modify('last week');
            $startTime = $date->format('Y-m-d');
            $date->modify('friday');
            $date->modify('+ 1 day');
            $endTime = $date->format('Y-m-d');
            $displayType = "All";
            break;
        case 'This Month':
            $date->modify('first day of this month');
            $startTime = $date->format('Y-m-d');
            $date->modify('last day of this month');
            $date->modify('+ 1 day');
            $endTime = $date->format('Y-m-d');
            $displayType = "Unique";
            break;
        case 'Last Month':
            $date->modify('first day of last month');
            $startTime = $date->format('Y-m-d');
            $date->modify('last day of this month');
            $date->modify('+ 1 day');
            $endTime = $date->format('Y-m-d');
            $displayType = "Unique";
            break;
        case 'This Quarter':
            $reply = $attendanceClass->returnQuarterDate(date("m"), "this");
            $startTime = $reply[0];
            $endTime = $reply[1];
            $displayType = "Unique";
            break;
        case 'Last Quarter':
            $reply = $attendanceClass->returnQuarterDate(date("m"), "last");
            $startTime = $reply[0];
            $endTime = $reply[1];
            $displayType = "Unique";
        default:
            # code...
            break;
    }

    $tableDetails = $attendanceClass->attendanceTable($startTime, $endTime, 0, $displayType);
?>
<table class="table table-padded">
    <thead>
        <tr>
            <th>Serial no.</th>
            <th>Staff name</th>
            <?php echo $showPeriod == "Today" || $showPeriod == "Last Week" ? "<th class='text-center'>Monday</th>": "" ?>
            <?php echo $showPeriod == "Today" || $showPeriod == "Last Week" ? "<th class='text-center'>Tuesday</th>": "" ?>
            <?php echo $showPeriod == "Today" || $showPeriod == "Last Week" ? "<th class='text-center'>Wednesday</th>": "" ?>
            <?php echo $showPeriod == "Today" || $showPeriod == "Last Week" ? "<th class='text-center'>Thursday</th>": "" ?>
            <?php echo $showPeriod == "Today" || $showPeriod == "Last Week" ? "<th class='text-center'>Friday</th>": "" ?>
            <?php echo $showPeriod == "This Month" || $showPeriod == "Last Month" || $showPeriod == "This Quarter" || $showPeriod == "Last Quarter" ? "<th class='text-center'>% Record</th>" : ""?>
            <?php echo $showPeriod == "This Month" || $showPeriod == "Last Month" || $showPeriod == "This Quarter" || $showPeriod == "Last Quarter" ? "<th class='text-center'>View Record</th>" : ""?>
        </tr>
    </thead>
    <tbody>
        <?php
            if($showPeriod == "Today" || $showPeriod == "Last Week"){ 
                foreach($tableDetails as $key => $value){ ?>
                    <!-- // NORMAL TABLE -->
                    <tr class="my_hover_up">
                        <td class="nowrap">
                            <span><?php echo $key + 1; ?>.</span>
                        </td>
                        <td>
                            <span><?php echo ucwords($value->fullname); ?></span>
                        </td>
                        <?php
                            $days = $newDays =  ["Mon", "Tue", "Wed", "Thu", "Fri"];
                            $allPresentDates = $value->dates;
                            $newStarter = 0; // NEW STARTER HOLDS THE INDEX OF THE LAST FOUND DAY..... SO I DONT OUTPUT ANYTHING IF ITS BELOW THAT DAY
                            // while(count($value->dates) > 0){
                                $curDateIndex = 0;
                                $daysLooped = 0;
                                while(count($allPresentDates) > 0){
                                    if($curDateIndex >= count($allPresentDates)){
                                        break;
                                    }else{
                                        $currentLoopDate = $allPresentDates[$curDateIndex];
                                        $nameOfDay = date('D', strtotime($currentLoopDate));

                                        for($i = 0; $i < count($days); $i++){
                                            if($days[$i] == $nameOfDay){ ?>
                                                <td><div class="status-pill <?php echo $attendanceClass->timeState($currentLoopDate, "staff") ?>" data-title="Complete" data-toggle="tooltip"></div></td>
                                                <?php 
                                                $daysLooped = $days[$i];
                                                if (($key = array_search($days[$i], $days)) !== false) {
                                                    unset($days[$key]);
                                                    $days = array_values($days);
                                                }
                                                $newStarter = $i;
                                                break 1;
                                            }else{ 
                                                if($newStarter > $i){
                                                    // IF ITS BELOW THAT DAY, OUTPUT NOTHING
                                                }else{ ?>
                                                    <td><div class="status-pill red" data-title="Complete" data-toggle="tooltip"></div></td>
                                                    <?php 
                                                    $daysLooped = $days[$i];
                                                } 
                                            }
                                        }
                                    }
                                    $curDateIndex += 1;
                                }
                                $key = array_search($daysLooped, $newDays);
                                for($w = $key; $w < 4; $w++){ ?>
                                    <td><div class="status-pill red" data-title="Complete" data-toggle="tooltip"></div></td>
                                <?php } ?>
                                <td class="text-center">
                                    <a data-target="#singleReportModal" data-toggle="modal" style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                        class="my_hover_up_bg badge" href="">View This Report</a>
                                </td>
                    </tr>
                <?php } 
                ?>
         <?php   }
            elseif ($showPeriod == "Last Month" || $showPeriod == "This Month" || $showPeriod == "This Quarter" || $showPeriod == "Last Quarter" || $showPeriod == "This Year" || $showPeriod == "Last Year") { 
                foreach ($tableDetails as $key => $value) { ?>
                    <tr class="my_hover_up">
                        <td class="nowrap">
                            <span><?php echo $key + 1?>.</span>
                        </td>
                        <td>
                            <span><?php echo ucwords($value->fullname); ?></span>
                        </td>
                        <td><div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div></td>
                        <td class="text-center">
                            <a data-target="#singleReportModal" data-toggle="modal" style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                class="my_hover_up_bg badge" href="">View This Report</a>
                        </td>
                    </tr>
                 <?php }
                
                ?>
        <?php    }
        ?>
    </tbody>
</table>