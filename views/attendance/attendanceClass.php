<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");
date_default_timezone_set("Africa/Lagos");
class attendanceClass extends logic {    

    function takeAttendance($attendanceNumber){
        $checker = strtolower(explode("/", $attendanceNumber)[1]);
        if($checker == "stdn"){
            $type = 1;
            $stmt = $this->uconn->prepare("SELECT fullname, father_phone, mother_phone FROM students WHERE student_number = ?");
            $stmt->bind_param("s", $attendanceNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result)  == 1){
                $date = date("Y-m-d");
                $row = array_shift($stmt_result);
                $fullname = $row["fullname"];
                $stmt = $this->uconn->prepare("SELECT id FROM attendancerecord WHERE attendanceNumber = ? AND date_created LIKE CONCAT(?,'%')");
                $stmt->bind_param("ss", $attendanceNumber, $date);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $stmt->close();
                    echo "Attendance already taken";
                }else{
                    $date = date("Y-m-d H:i:s");
                    $stmt = $this->uconn->prepare("INSERT INTO attendancerecord (attendanceNumber, idType, date_created) VALUES (?,?,?)");
                    $stmt->bind_param("sis", $attendanceNumber, $type, $date);
                    if($stmt->execute()){
                        if($this->getSchoolDetails()->parentDailyAlert == 1){
                            // SEND MESSAGE
                            $reply = $this->checkSmsBalance();
                            if($reply > 1){
                                $date = date("Y-m-d");
                                $parentsContact[] = $row["father_phone"];
                                $parentsContact[] = $row["mother_phone"];
                                $message = "Good day, your ward ".$fullname." has arrived school and attendance has been taken";
                                $reply = $this->sendSMS($message, $this->getSchoolDetails()->domain_name, $parentsContact);
                                if($reply === true){
                                    $reply = $this->logMessage(["specify"], $date, $message, $parentsContact, 1);
                                    if($reply === true){
                                        $stmt->close();
                                        echo $fullname."'s attendance has been taken and message sent";
                                    }else{
                                        $stmt->close();
                                        echo $fullname."'s attendance has been taken and message has been sent";
                                    }
                                }else{
                                    $stmt->close();
                                    echo $fullname."'s attendance has been taken but message was not sent due to unexpected error";
                                }
                            }else{
                                $stmt->close();
                                echo $fullname."'s attendance has been taken but message was not sent due to insufficient balance";
                            }
                        }else{
                            $stmt->close();
                            echo $fullname."'s attendance has been taken";
                        }
                    }else{
                        $stmt->close();
                        echo "Couldn't complete this operation. Please try again later";
                    }
                }
            }
        }else{
            $type = 0;
            $stmt = $this->uconn->prepare("SELECT fullname FROM teachers WHERE staff_number = ?");
            $stmt->bind_param("s", $attendanceNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result)  == 1){
                $date = date("Y-m-d");
                $fullname = array_shift($stmt_result)["fullname"];
                $stmt = $this->uconn->prepare("SELECT id FROM attendancerecord WHERE attendanceNumber = ? AND date_created LIKE CONCAT(?,'%')");
                $stmt->bind_param("ss", $attendanceNumber, $date);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $stmt->close();
                    echo "Attendance already taken";
                }else{
                    $date = date("Y-m-d H:i:s");
                    $stmt = $this->uconn->prepare("INSERT INTO attendancerecord (attendanceNumber, idType, date_created) VALUES (?,?,?)");
                    $stmt->bind_param("sis", $attendanceNumber, $type, $date);
                    if($stmt->execute()){
                        $stmt->close();
                        echo $fullname."'s attendance has been taken";
                    }
                }
            }
        }
    }

    function getDailyAttendance(){
        $date = date("Y-m-d");
        $type1 = 1; // for students
        $type0 = 0; // for staffs
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS num FROM attendancerecord WHERE idType = ? AND SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ?");
        $stmt->bind_param("is", $type1, $date);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        $obj->todayStudent = array_shift($stmt_result)["num"];
        // STAFF NUMBER
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS num FROM attendancerecord WHERE idType = ? AND SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ?");
        $stmt->bind_param("is", $type0, $date);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->todayStaff = array_shift($stmt_result)["num"];
        // TOTAL STUDENT
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS num FROM students");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->totalStudent = array_shift($stmt_result)["num"];
        // TOTAL STUDENT
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS num FROM teachers");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->totalStaff = array_shift($stmt_result)["num"];
        // LATE STAFF
        $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM attendancerecord WHERE SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ? AND SUBSTRING_INDEX(attendancerecord.date_created, ' ', -1) > ? AND idType = ?");
        $stmt->bind_param("ssi", $date, $this->getSchoolDetails()->staffLatenessStart, $type0);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->staffLateness = array_shift($stmt_result)["num"];
        // LATE STUDENT
        $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM attendancerecord WHERE SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ? AND SUBSTRING_INDEX(attendancerecord.date_created, ' ', -1) > ? AND idType = ?");
        $stmt->bind_param("ssi", $date, $this->getSchoolDetails()->studentLatenessStart, $type1);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->studentLateness = array_shift($stmt_result)["num"];
        // ABSENT STUDENT
        $stmt = $this->uconn->prepare("SELECT COUNT(students.id) AS num FROM students WHERE students.student_number NOT IN (SELECT attendancerecord.attendanceNumber FROM attendancerecord WHERE SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ? AND idType = ?)");
        $stmt->bind_param("si", $date, $type1);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->absentStudent = array_shift($stmt_result)["num"];
        // ADSENT STAFF
        $stmt = $this->uconn->prepare("SELECT COUNT(teachers.id) AS num FROM teachers WHERE teachers.staff_number NOT IN (SELECT attendancerecord.attendanceNumber FROM attendancerecord WHERE SUBSTRING_INDEX(attendancerecord.date_created, ' ', 1) = ? AND idType = ?)");
        $stmt->bind_param("si", $date, $type0);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj->absentStaff = array_shift($stmt_result)["num"];

        $stmt->close();
        return $obj;
    }

    function attendanceTable($startTime, $endTime, $type, $displayType){
        $allArray = array();
        // if($type == 1){
        //     $stmt = $this->uconn->prepare("SELECT attendancerecord.attendanceNumber, GROUP_CONCAT(attendancerecord.date_created) AS timer, students.fullname, GROUP_CONCAT(attendancerecord.attendanceNumber) AS numbers FROM attendancerecord INNER JOIN students ON students.student_number = attendancerecord.attendanceNumber  WHERE attendancerecord.idType = ? AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') GROUP BY attendancerecord.attendanceNumber");
        // }else{
        //     $stmt = $this->uconn->prepare("SELECT attendancerecord.attendanceNumber, GROUP_CONCAT(attendancerecord.date_created ORDER BY attendancerecord.date_created SEPARATOR ',') AS timer, teachers.fullname, GROUP_CONCAT(attendancerecord.attendanceNumber) AS numbers FROM attendancerecord INNER JOIN teachers ON teachers.staff_number = attendancerecord.attendanceNumber  WHERE attendancerecord.idType = ? AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') GROUP BY attendancerecord.attendanceNumber");
        // }
        // GET THE DEFAULT MYSQL TIME SETTINGS
        $sql = "SET time_zone = '+01:00' ";
        $res = $this->uconn->prepare($sql);
        $res->execute();

        if($displayType == "All"){
            if($type == 1){
                // 1 IS FOR STUDENT
                $stmt = $this->uconn->prepare("SELECT DISTINCT(attendancerecord.id), attendancerecord.*, students.fullname FROM `students` INNER JOIN attendancerecord ON students.student_number = attendancerecord.attendanceNumber WHERE attendanceNumber IN (SELECT students.student_number FROM students) AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') ORDER BY attendancerecord.attendanceNumber, date_created ASC");
            }else{
                $stmt = $this->uconn->prepare("SELECT DISTINCT(attendancerecord.id), attendancerecord.*, teachers.fullname FROM `teachers` INNER JOIN attendancerecord ON teachers.staff_number = attendancerecord.attendanceNumber WHERE attendanceNumber IN (SELECT teachers.staff_number FROM teachers) AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') ORDER BY attendancerecord.attendanceNumber, date_created ASC");
            }
        }else{
            if($type == 1){
                // 1 IS FOR STUDENT
                $stmt = $this->uconn->prepare("SELECT DISTINCT(attendancerecord.id), attendancerecord.*, students.fullname FROM `students` INNER JOIN attendancerecord ON students.student_number = attendancerecord.attendanceNumber WHERE attendanceNumber IN (SELECT students.student_number FROM students) AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') GROUP BY attendancerecord.attendanceNumber ORDER BY attendancerecord.attendanceNumber, date_created ASC");
            }else{
                $stmt = $this->uconn->prepare("SELECT DISTINCT(attendancerecord.id), attendancerecord.*, teachers.fullname FROM `teachers` INNER JOIN attendancerecord ON teachers.staff_number = attendancerecord.attendanceNumber WHERE attendanceNumber IN (SELECT teachers.staff_number FROM teachers) AND attendancerecord.date_created BETWEEN CONCAT(?,'%') AND CONCAT(?,'%') GROUP BY attendancerecord.attendanceNumber ORDER BY attendancerecord.attendanceNumber, date_created ASC");
            }
        }

        // if($type == 1){
        //     $stmt = $this->uconn->prepare("SELECT attendancerecord.attendanceNumber, GROUP_CONCAT(MONTH(attendancerecord.date_created)) AS month, GROUP_CONCAT(WEEK(attendancerecord.date_created)) AS week, GROUP_CONCAT(attendancerecord.date_created) AS date, students.fullname FROM attendancerecord INNER JOIN students ON students.student_number = attendancerecord.attendanceNumber WHERE date(attendancerecord.date_created) BETWEEN CONCAT(?,'%') and CONCAT(?,'%') and attendancerecord.idType = ? group by attendancerecord.attendanceNumber ORDER BY attendancerecord.date_created");
        // }else{
        //     $stmt = $this->uconn->prepare("SELECT attendancerecord.attendanceNumber, GROUP_CONCAT(MONTH(attendancerecord.date_created)) AS month, GROUP_CONCAT(WEEK(attendancerecord.date_created) + 1) AS week, GROUP_CONCAT(attendancerecord.date_created) AS date, teachers.fullname FROM attendancerecord INNER JOIN teachers ON teachers.staff_number = attendancerecord.attendanceNumber WHERE date(attendancerecord.date_created) BETWEEN CONCAT(?,'%') and CONCAT(?,'%') and attendancerecord.idType = ? group by attendancerecord.attendanceNumber ORDER BY attendancerecord.date_created");
        // }
        $attendanceRecord = array();
        $stmt->bind_param("ss", $startTime, $endTime);
        if($stmt->execute());
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) == 0){
           return $attendanceRecord;
        }else{
            if($displayType == "All"){
                $currentNumber;
                $datesArray = array();
                while ($row = array_shift($stmt_result)){
                    if(isset($row["attendanceNumber"]) && isset($currentNumber) && $currentNumber == $row["attendanceNumber"]){
                        $datesArray[] = $row["date_created"];
                        $obj->dates = $datesArray;
                    }else{
                        $obj = new stdClass();
                        $obj->fullname = $row["fullname"];
                        $obj->attendanceNumber = $row["attendanceNumber"];
                        $currentNumber = $row["attendanceNumber"];
                        $datesArray[] = $row["date_created"];
                        $obj->dates = $datesArray;
                    }
                    if(count($attendanceRecord) > 0){
                        // AVOID PUTTING DUPLICATE ROW
                        if(end($attendanceRecord)->attendanceNumber != $obj->attendanceNumber){
                            unset($datesArray);
                            $attendanceRecord[] = $obj;
                        }
                    }else{
                        //unset($datesArray);
                        $attendanceRecord[] = $obj;
                    }
                    // if($obj->fullname == $attendanceRecord[])
                    //echo count($attendanceRecord);
                }
            }else{
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->fullname = $row["fullname"];
                    $obj->attendanceNumber = $row["attendanceNumber"];
                    $currentNumber = $row["attendanceNumber"];
                    $datesArray[] = $row["date_created"];
                    $obj->dates = $datesArray;
                    $attendanceRecord[] = $obj;
                }
            }
            return $attendanceRecord;
        }

    }

    function timeState($date, $status){
        if($status == "staff"){
            $latenessStart = $this->getSchoolDetails()->staffLatenessStart;
        }else{
            $latenessStart = $this->getSchoolDetails()->studentLatenessStart;
        }
        $time = explode(" ",$date)[1];
        if($time > $latenessStart){
            return "yellow";
        }else{
            return "green";
        }
    }

    function returnQuarterDate($month, $hint){
        $today = date("Y-m-d");
        $date = new DateTime($today);
        $quarter = ["january", "march","april","june","july","september","october","december"];
        $val = ceil($month / 3);
        if($hint != "this"){
            $val -= 1;
        }
        if($val == 1 || $val  == 0){
            $date->modify('first day of '.$quarter[0]);
            $startDate = $date->format('Y-m-d');
            $date->modify('last day of'.$quarter[1]);
            $endDate = $date->format('Y-m-d');
        }elseif ($val == 2) {
            # code...
            $date->modify('first day of '.$quarter[2]);
            $startDate = $date->format('Y-m-d');
            $date->modify('last day of'.$quarter[3]);
            $endDate = $date->format('Y-m-d');
        }elseif ($val == 3) {
            # code...
            $date->modify('first day of '.$quarter[4]);
            $startDate = $date->format('Y-m-d');
            $date->modify('last day of'.$quarter[5]);
            $endDate = $date->format('Y-m-d');
        }elseif ($val == 4) {
            # code...
            $date->modify('first day of '.$quarter[6]);
            $startDate = $date->format('Y-m-d');
            $date->modify('last day of'.$quarter[7]);
            $endDate = $date->format('Y-m-d');
        }

        return [$startDate, $endDate];
    }

    function periodState($period){
        $val = strtolower(explode(" ", $period)[1]);
        $states = ["month" => "week", "quarter" => "week"];
        return $states[$val];
    }
}
?>