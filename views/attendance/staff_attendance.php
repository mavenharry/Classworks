<?php include_once ("attendancebtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Attendance</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/scanner/style.css" rel="stylesheet">
</head>
<?php
    $schoolDetails = $attendanceClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar2.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <br>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">ATTENDANCE ANALYSIS PANEL</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#"> <!-- CHANGE THIS PART LATER...REAL PART *./student.php* -->
                                                    <div class="label">My students in school today</div>
                                                    <div style="margin-left:0; color:#FB5574" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2">0</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">My students absent today</div>
                                                    <div style="margin-left:0; color:#FB5574" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2">0</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">My students late today</div>
                                                    <div style="margin-left:0; color:#FB5574" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2">0</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./attendance_scanner.php">
                                                    <div class="label">Record today's attendance</div>
                                                    <div style="margin-left:0; color:#FB5574" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Take Attendance</div>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->


<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>