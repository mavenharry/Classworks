<?php include_once ("timetablebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $timetableClass->getSchoolDetails();
    $timingActivity = $timetableClass->getTimingActivities();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Timetable", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                </div>
                                <h6 class="element-header">CONFIGURE TIMETABLE TIMINGS</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-7">

                                <div class="element-box">

                                <h5 class="form-header">
                                SET TIMETABLE TIMES
                                </h5>
                                <div class="form-desc">
                                Easily configure your school daily time schedule for your school's lecture times where applicable.
                                </div>
                                
                                    <?php
                                        foreach ($timingActivity as $key => $value) { ?>
                                            <!-- # code... -->
                                            <div class="form-group row schoolActivity" data-id="<?php echo $value->timeCode; ?>">
                                                <div class="col-sm-5">
                                                    <input class="form-control " value="<?php echo ucwords($value->activity); ?>" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input class="form-control " value="<?php echo $value->timeName; ?>" type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <button class="btn btn-primary" id="timingBtn" style="background-color:#FB5574; border: none" type="button"> Delete Time</button>
                                                </div>    
                                            </div>
                                            
                                        <?php }
                                    ?>

                                <form action="" method="post">
                                    <div id="lastInput"></div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-12" for="" id="addTiming" style="text-align:center; margin-top:15px; color:#08ACF0; text-decoration:underline; cursor:pointer"> CLICK HERE TO ADD NEW TIME</label>
                                    </div>

                                    <div class="form-buttons-w">
                                        <button name="submitTiming" class="btn btn-primary" type="submit"> Save timetable times</button>
                                    </div>
                                </form>
                                </div>
                                </div>

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>