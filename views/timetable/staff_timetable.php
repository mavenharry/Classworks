<?php include_once ("timetablebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $timetableClass->getSchoolDetails();
    $staffs = $timetableClass->getAllStaffs();    
    $scheduleDays = $timetableClass->fetchScheduleDays();
    $ClassTimes = $timetableClass->fetchScheduleTimes();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Timetable", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px;">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><span style="font-weight:800"><?php echo $_GET["session"] ?></span> SESSION <span style="font-weight:800" id="classTimetableClass"><?php echo strtoupper($timetableClass->getSingleStaffDetails($_GET["staff"])->fullname) ?></span> <span style="font-weight:800"><?php echo strtoupper($timetableClass->ordinalSuffix($_GET["term"])) ?></span> TERM <span style="font-weight:700" id="timetableStaff"></span> LECTURES TIMETABLE</h6>                                

                                <div class="table-responsive" style="margin-top:30px;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="text-center">
                                    <th style="font-size:1rem; color:#FFFFFF; background-color:#f2f4f8"></th>
                                        <?php 
                                            for($i = 0; $i < count($scheduleDays); $i++){ ?>
                                            <th style="font-size:1.1rem; color:#FFFFFF; font-weight:500; background-color:#afbabf"><?php echo $scheduleDays[$i]->day; ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    for($i = 0; $i < count($ClassTimes); $i++){ ?>
                                    <tr class="text-center">
                                    <td style="font-size:1.1rem; color:#FFFFFF; background-color:#afbabf; font-weight:500;"><?php echo $ClassTimes[$i]->timeName ?></td>
                                    <?php for($j = 0; $j < count($scheduleDays); $j++){
                                    if(isset($_GET["staff"])){
                                    $timeSchedule = $timetableClass->fetchAllClassOrStaffTimeSchedule($_GET["staff"], $ClassTimes[$i]->timeCode, $scheduleDays[$j]->dayCode, $_GET["session"], $_GET["term"]); 
                                    }else{
                                    $timeSchedule = $timetableClass->fetchAllClassOrStaffTimeSchedule($staffs[0]->staff_number, $ClassTimes[$i]->timeCode, $scheduleDays[$j]->dayCode , $_GET["session"], $_GET["term"]);                                         
                                    }
                                    if(count($timeSchedule) > 0){
                                        for($k = 0; $k < count($timeSchedule); $k++){ 
                                        if(isset($_GET["staff"])){ ?>
                                        <td onclick="activeTimeTdStaff('<?php echo $_GET["staff"] ?>', '<?php echo $timeSchedule[$k]->timeDayCode?>', '<?php echo $timeSchedule[$k]->timeTimeCode?>')" class="my_hover_up" data-target="#editStaffTimetableModal" data-toggle="modal" style="font-weight:500; cursor:pointer; font-size:1.1rem; color:#FFFFFF; background-color:<?php echo $timeSchedule[$k]->timeColor ?>"><?php echo ucwords($timeSchedule[$k]->timeSubject) ?><span style="font-weight:400; display:block; font-size:0.9rem; font-style:italic"><?php echo ucwords($timeSchedule[$k]->timeClass) ?></td>   
                                    <?php }else{ ?>
                                        <td onclick="activeTimeTdStaff('<?php echo $staffs[0]->staff_number ?>', '<?php echo $timeSchedule[$k]->timeDayCode?>', '<?php echo $timeSchedule[$k]->timeTimeCode?>')" class="my_hover_up" data-target="#editStaffTimetableModal" data-toggle="modal" style="font-weight:500; cursor:pointer; font-size:1.1rem; color:#FFFFFF; background-color:<?php echo $timeSchedule[$k]->timeColor ?>"><?php echo ucwords($timeSchedule[$k]->timeSubject) ?><span style="font-weight:400; display:block; font-size:0.9rem; font-style:italic"><?php echo ucwords($timeSchedule[$k]->timeClass) ?></td>                                           
                                    <?php } } 
                                    }else{ 
                                        if(isset($_GET["staff"])){ ?>
                                        <td onclick="activeTimeTdStaff('<?php echo $_GET["staff"] ?>', '<?php echo $scheduleDays[$j]->dayCode ?>', '<?php echo $ClassTimes[$i]->timeCode ?>')" class="my_hover_up" data-target="#newStaffTimetableModal" data-toggle="modal" style="cursor:pointer; font-size:1.2rem; color:#FFFFFF; background-color:#afbabf"></td>                                            
                                    <?php }else{ ?>
                                        <td onclick="activeTimeTdStaff('<?php echo $staffs[0]->staff_number ?>', '<?php echo $scheduleDays[$j]->dayCode ?>', '<?php echo $ClassTimes[$i]->timeCode ?>')" class="my_hover_up" data-target="#newStaffTimetableModal" data-toggle="modal" style="cursor:pointer; font-size:1.2rem; color:#FFFFFF; background-color:#afbabf"></td>                                                                                    
                                    <?php } } } } ?>
                                    </tr>

                                    </tbody>
                                </table>
                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
<script>
    var changeStaff = $(".timetableStaffSelector").val();
    $("#timetableStaff").html(changeStaff.toUpperCase())
</script>
</body>

</html>