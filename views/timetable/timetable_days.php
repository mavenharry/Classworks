<?php include_once ("timetablebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $timetableClass->getSchoolDetails();
    $scheduleDays = $timetableClass->getDaysArray();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Timetable", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                </div>
                                <h6 class="element-header">CONFIGURE TIMETABLE DAYS</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-6">

                                <div class="element-box">
                                <form method="POST">
                                <h5 class="form-header">
                                SET TIMETABLE DAYS
                                </h5>
                                <div class="form-desc">
                                Easily configure your school days of the week activity schedule for your school's lecture timetable where applicable.
                                </div>
                                
                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> MONDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 0 && $scheduleDays[0]->state == 1 ? "checked" : ""; ?>  value="Monday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> TUESDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 1 && $scheduleDays[1]->state == 1 ? "checked" : ""; ?>  value="Tuesday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> WEDNESDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 2 && $scheduleDays[2]->state == 1 ? "checked" : ""; ?>  value="Wednesday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> THURSDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 3 && $scheduleDays[3]->state == 1 ? "checked" : ""; ?>  value="Thursday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> FRIDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 4 && $scheduleDays[4]->state == 1 ? "checked" : ""; ?>  value="Friday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> SATURDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 5 && $scheduleDays[5]->state == 1 ? "checked" : ""; ?>  value="Saturday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-form-label col-sm-4" for=""> SUNDAY</label>
                                <div class="col-sm-8">
                                <div class="center c-toggle-btn">
                                <input class="days" <?php echo count($scheduleDays) > 6 && $scheduleDays[6]->state == 1 ? "checked" : ""; ?> value="Sunday" type="checkbox">
                                <div>
                                    <label class="on">On</label>
                                    <label class="off">Off</label>
                                    <span class="c-toggle-thumb"></span>
                                </div>
                                </div>
                                </div>
                                </div>
                        
                                </form>
                                </div>
                                </div>

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>