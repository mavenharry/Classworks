<?php
include_once ("timetableClass.php");
$timetableClass = new timetableClass();

if(isset($_POST["submitTiming"])){
    $timingActivity = $_POST["timingActivity"];
    $timing = $_POST["timing"];
    
    $reply = $timetableClass->addTimingActivity($timingActivity, $timing);
    
    if($reply === true){
        $mess = "Timing Schedule has been updated";
    }else{
        $err = "Couldnt update school settings. Please try again later";
    }
}

if(isset($_POST["savePublic"])){
    if(isset($_POST["holidayName"])){
        $holidayName = $_POST["holidayName"];
        $holidayDate = $_POST["holidayDate"];
        $reply = $timetableClass->addSchoolHoliday($holidayName, $holidayDate);

        if($reply === true){
            $mess = "Public holiday has been updated";
        }else{
            $err = "Couldn't update public holidays. Please try again later";
        }
    }else{
        $err = "No holiday added";
    }
}

?>