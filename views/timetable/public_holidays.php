<?php include_once ("timetablebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $timetableClass->getSchoolDetails();
    $publicHolidays = $timetableClass->defaultPublicHolidays();
    $schoolHoliday = $timetableClass->getSchoolHoliday();
    $addedHolidays = $timetableClass->getAddedHolidays();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Timetable", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                </div>
                                <h6 class="element-header">CONFIGURE PUBLIC/SCHOOL HOLIDAYS & EVENTS</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-6">

                                <div class="element-box">
                                <form method="POST" action="#">
                                    <h5 class="form-header">
                                        SETUP PUBLIC HOLIDAYS
                                    </h5>
                                    <div class="form-desc">
                                        Edit or add new public/school holiday schedules to your school calendar to enable automatic holiday detection.
                                    </div>
                                    <?php
                                    foreach ($addedHolidays as $key => $value) { ?>
                                        <!-- # code... -->
                                        <div class="form-group row holiday" data-id="<?php echo $value->id ?>">
                                            <div class="col-sm-5">
                                                <input class="form-control" value="<?php echo $value->holidayName; ?>" type="text">
                                            </div>
                                            <div class="col-sm-4">
                                                <input class="form-control date_of_holiday" value="<?php echo $value->holidayDate; ?>" type="text">
                                            </div>
                                            <div class="col-sm-3">
                                                <button class="btn btn-primary delHol" style="background-color:#FB5574; border: none" type="button"> Delete This</button>
                                            </div>
                                        </div>
                                    <?php }
                                ?>
                                    <div id="holidays"></div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-12" for="" style="text-align:center; margin-top:15px; color:#08ACF0; text-decoration:underline; cursor:pointer" id="newHoliday"> CLICK HERE TO ADD NEW HOLIDAY DATE</label>
                                    </div>
                                    <div class="form-buttons-w">
                                    <button class="btn btn-primary" name="savePublic" type="submit"> Save public holidays</button>
                                    </div>
                                </form>
                                </div>
                                </div>

                                <div class="col-lg-6">

                                <div class="element-box">
                                <h5 class="form-header">
                                LIST OF PUBLIC HOLIDAYS
                                </h5>
                                <div class="form-desc">
                                Below are list of existing public/school holidays to pick from. Anyone applicable to your school? Just switch it on. 
                                </div>

                                <!-- STUDENTS SLIP -->

                                <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th class="text-center">Swicth</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach ($publicHolidays as $keys => $value) { ?>
                                                        <!-- # code... -->
                                                        <tr class="my_hover_up" data-id="<?php echo $value->id ?>">
                                                            <td class="nowrap">
                                                                <span><?php echo $keys + 1?>.</span>
                                                            </td>
                                                            <td>
                                                                <span class="holidayName"><?php echo $value->holidayName?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo empty($value->holidayDate) ? "-- --" : $value->holidayDate?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="center c-toggle-btn">
                                                                <input <?php echo in_array($value->id, $schoolHoliday) ? "checked" : ""?> class="dholidays" type="checkbox">
                                                                <div>
                                                                    <label class="on">On</label>
                                                                    <label class="off">Off</label>
                                                                    <span class="c-toggle-thumb"></span>
                                                                </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                ?>

                                            </tbody>
                                        </table>

                                </div>
                                </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>