<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class timetableClass extends logic {

    function fetchScheduleDays(){
        $stmt = $this->uconn->prepare("SELECT * FROM schedule_days WHERE state = 1");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $schedule_days = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->dayCode = $this->outputRefine($row["dayCode"]);
            $obj->day = $this->outputRefine($row["dayName"]);
            $schedule_days[] = $obj;
        }
        return $schedule_days;
        $stmt->close();   
    }

    function getDaysArray(){
        $stmt = $this->uconn->prepare("SELECT dayName, state FROM schedule_days");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $schedule_days = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->state = $this->outputRefine($row["state"]);
            $obj->dayName = $this->outputRefine($row["dayName"]);
            $schedule_days[] = $obj;
        }
        $stmt->close();
        return $schedule_days;
    }


    function fetchScheduleTimes(){
        $stmt = $this->uconn->prepare("SELECT * FROM schedule_times WHERE activity != 'Devotion Time' ORDER BY timeName ASC");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $schedule_times = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->timeName = $this->outputRefine($this->getTimeName($row["timeCode"]));
            $obj->timeCode = $this->outputRefine($row["timeCode"]);
            $schedule_times[] = $obj;
        }
        $stmt->close(); 
        return $schedule_times;  
    }


    function fetchAllClassOrStaffTimeSchedule($classOrStaffCode, $timeCode, $timeDay, $session, $term){
        $colorArray = ["#FB5574", "#FC9933", "#1BB4AD", "#F4B510", "#FE650B", "#08ACF0", "#974A6D"];
        
        if(isset($session) && !empty($session)){
            $current_academic_session = $session;
        }else{
            $current_academic_session = $this->getCurrentAcademicSession();
        }

        if(isset($term) && !empty($term)){
            $current_academic_term = $term;
        }else{
            $current_academic_term = $this->getCurrentAcademicTerm();
        }

        $stmt = $this->uconn->prepare("SELECT * FROM schedule_timings WHERE timeClass = ? AND timeTime = ? AND timeDay = ? AND academic_session = ? AND academic_term = ? OR timeTutor = ? AND timeTime = ? AND timeDay = ? AND academic_session = ? AND academic_term = ? ORDER BY timeDay ASC");
        $stmt->bind_param("ssssssssss", $classOrStaffCode, $timeCode, $timeDay, $current_academic_session, $current_academic_term,  $classOrStaffCode, $timeCode, $timeDay, $current_academic_session, $current_academic_term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $timingArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->timingCode = $this->outputRefine($row["timeCode"]);
            $obj->timeColor = $colorArray[mt_rand(0,6)];
            $obj->timeDay = $this->outputRefine($row["timeDay"]);
            $obj->timeTime = $this->outputRefine($this->getTimeName($row["timeTime"]));
            $obj->timeSubject = $this->outputRefine($this->getSubjectName($row["timeSubject"]));
            $obj->timeClass = $this->outputRefine($this->getClassName($row["timeClass"]));
            $obj->timeTutor = $this->outputRefine($this->getSingleStaffDetails($row["timeTutor"])->fullname);
            $obj->timeTimeCode = $row["timeTime"];
            $obj->timeDayCode = $row["timeDay"];
            $timingArray[] = $obj;
        }
        $stmt->close();
        return $timingArray;
    }

    function addTimingActivity($timingActivity, $timing){
        foreach ($timingActivity as $key => $value) {
            # code...
            $end = $this->getLastNumber("schedule_times") + 1;
            switch (strlen($end)) {
                case 1:
                    # code...
                    $end = "00".$end;
                    break;
                case 2:
                    # code...
                    $end = "0".$end;
                    break;
                case 3:
                    # code
                    $end = $end;
                    break;
                
                default:
                    # code...
                    break;
            }
            $timeCode = "time".$end;
            $stmt = $this->uconn->prepare("INSERT INTO schedule_times (timeCode, timeName, activity) VALUES (?,?,?)");
            $stmt->bind_param("sss", $timeCode, $timing[$key], $value);
            $stmt->execute();
        }
        $stmt->close();
        return true;
    }

    function getTimingActivities(){
        $stmt = $this->uconn->prepare("SELECT timeCode, id, timeName, activity FROM schedule_times");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $timingArray = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->timeCode = $row["timeCode"];
                $obj->timeName = $row["timeName"];
                $obj->activity = $row["activity"];
                $obj->id = $row["id"];
                $timingArray[] = $obj;
            }
            $stmt->close();
            return $timingArray;
        }else{
            $stmt->close();
            return $timingArray;
        }
    }

    function deleteTimeActivity($timeCode){
        $stmt = $this->uconn->prepare("DELETE FROM schedule_times WHERE timeCode = ?");
        $stmt->bind_param("s", $timeCode);
        if($stmt->execute()){
            $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeCode = ?");
            $stmt->bind_param("s", $timeCode);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }
    }

    function updateDays($day, $val){
        $stmt = $this->uconn->prepare("UPDATE schedule_days SET state = ? WHERE dayName = ?");
        $stmt->bind_param("is", $val, $day);
        if($stmt->execute()){
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }

    function defaultPublicHolidays(){
        $stmt = $this->dconn->prepare("SELECT * FROM publicholiday");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $holidayArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass;
            $obj->id = $row["id"];
            $obj->holidayName = $row["holidayName"];
            $obj->holidayDate = $row["holidayDate"];
            $holidayArray[] = $obj;
        }
        $stmt->close();
        return $holidayArray;
    }

    function updateHoliday($defaultId, $val){
        $ifExist = $this->checkIfExist("publicholiday", "id", "defaultId", $defaultId);
        if($ifExist === true){
            // UPDATE STATE
            $stmt = $this->uconn->prepare("UPDATE publicholiday SET state = ? WHERE defaultId = ?");
            $stmt->bind_param("ii", $val, $defaultId);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }else{
            $stmt = $this->dconn->prepare("SELECT * FROM publicholiday WHERE id = ?");
            $stmt->bind_param("i", $defaultId);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $holidayName = $row["holidayName"];
            $holidayDate = $row["holidayDate"];

            $owner = 1; // 1 means its coming from classworks official
            $stmt = $this->uconn->prepare("INSERT INTO publicholiday (defaultId, holidayName, holidayDate, owner, state) VALUES (?,?,?,?,?)");
            $stmt->bind_param("issii", $defaultId, $holidayName, $holidayDate, $owner, $val);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }
    }

    function getSchoolHoliday(){
        $stmt = $this->uconn->prepare("SELECT defaultId FROM publicholiday WHERE state = 1");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $schoolHoliday = array();
        while ($row = array_shift($stmt_result)) {
            $schoolHoliday[] = $row["defaultId"];
        }
        $stmt->close();
        return $schoolHoliday;
    }

    function addSchoolHoliday($holidayName, $holidayDate){
        foreach ($holidayName as $key => $value) {
            # code...
            $owner = 0;
            $state = 1;
            $defaultId = 0;
            $stmt = $this->uconn->prepare("INSERT INTO publicholiday (defaultId, holidayName, holidayDate, owner, state) VALUES (?,?,?,?,?)");
            $stmt->bind_param("issii", $defaultId, $value, $holidayDate[$key], $owner, $state);
            $stmt->execute();
        }

        $stmt->close();
        return true;
    }

    function getAddedHolidays(){
        $val = 0;
        $stmt = $this->uconn->prepare("SELECT id, holidayName, holidayDate FROM publicholiday WHERE defaultId = ?");
        $stmt->bind_param("i", $val);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $holidays = array();
        if(count($stmt_result) > 0){
            while ($row = array_shift($stmt_result)) {
                # code...
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->holidayName = $row["holidayName"];
                $obj->holidayDate = $row["holidayDate"];
                $holidays[] = $obj;
            }
            $stmt->close();
            return $holidays;
        }else{
            $stmt->close();
            return $holidays;
        }
    }

    function deleteHoliday($delId){
        $stmt = $this->uconn->prepare("DELETE FROM publicholiday WHERE id = ?");
        $stmt->bind_param("i", $delId);
        if($stmt->execute()){
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }


}
?>