<?php
include ("timetableClass.php");
$timetableClass = new timetableClass();

$schoolDetails = $timetableClass->getSchoolDetails();
$scheduleDays = $timetableClass->fetchScheduleDays();
$ClassTimes = $timetableClass->fetchScheduleTimes();

if(isset($_GET["timetableClass"])){
    $timetableClass = htmlentities(trim($_GET["timetableClass"]));
    for($i = 0; $i < count($ClassTimes); $i++){ ?>
    <tr class="text-center">
    <td style="font-size:1.1rem; color:#FFFFFF; background-color:#afbabf; font-weight:500;"><?php echo $ClassTimes[$i]->timeName ?></td>
    <?php for($j = 0; $j < count($scheduleDays); $j++){
    $timeSchedule = $timetableClass->fetchAllClassOrStaffTimeSchedule("class001", $ClassTimes[$i]->timeCode, $scheduleDays[$j]->dayCode); 
    if(count($timeSchedule) > 0){
        for($k = 0; $k < count($timeSchedule); $k++){ ?>
        <td onclick="activeTimeTd('class001', '<?php echo $timeSchedule[$k]->timeDayCode?>', '<?php echo $timeSchedule[$k]->timeTimeCode?>')" class="my_hover_up" data-target="#editTimetableModal" data-toggle="modal" style="font-weight:500; cursor:pointer; font-size:1.1rem; color:#FFFFFF; background-color:<?php echo $timeSchedule[$k]->timeColor ?>"><?php echo $timeSchedule[$k]->timeSubject ?><span style="font-weight:400; display:block; font-size:0.9rem; font-style:italic"><?php echo $timeSchedule[$k]->timeTutor ?></td>   
    <?php } 
    }else{  ?>
        <td onclick="activeTimeTd('class001', '<?php echo $scheduleDays[$j]->dayCode ?>', '<?php echo $ClassTimes[$i]->timeCode ?>')" class="my_hover_up" data-target="#newTimetableModal" data-toggle="modal" style="cursor:pointer; font-size:1.2rem; color:#FFFFFF; background-color:#afbabf"></td>                                            
    <?php } } } ?>
    </tr>
<?php }

if(isset($_GET["deleteTiming"])){
    $id = htmlentities(trim($_GET["deleteTiming"]));
    $reply = $timetableClass->deleteTimeActivity($id);
    if($reply === true){
        echo "done";
    }else{
        echo "error";
    }
}

if(isset($_GET["day"])){
    $day = htmlentities(trim($_GET["day"]));
    $val = htmlentities(trim($_GET["val"]));

    $reply = $timetableClass->updateDays($day, $val);
    echo $reply;
}

if(isset($_GET["defaultId"])){
    $defaultId = htmlentities(trim($_GET["defaultId"]));
    $val = htmlentities(trim($_GET["val"]));

    $reply = $timetableClass->updateHoliday($defaultId, $val);

    echo $reply;
}

if(isset($_GET["addHol"])){ ?>
    <div class="form-group row holiday">
        <div class="col-sm-5">
            <input class="form-control" name="holidayName[]" placeholder="E.g; Christmas Day" type="text">
        </div>
        <div class="col-sm-4">
            <input class="form-control date_of_holiday" name="holidayDate[]" placeholder="25 December" type="text">
        </div>
        <div class="col-sm-3">
            <button class="btn btn-primary delHolBtn" style="background-color:#FB5574; border: none" type="button"> Delete This</button>
        </div>
    </div>
<?php }

if(isset($_GET["delId"])){
    $delId = htmlentities(trim($_GET["delId"]));
    $reply = $timetableClass->deleteHoliday($delId);
    echo $reply;
}

?>