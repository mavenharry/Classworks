<?php include_once ("timetablebtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $timetableClass->getSchoolDetails();
    $scheduleDays = $timetableClass->fetchScheduleDays();
    $ClassTimes = $timetableClass->fetchScheduleTimes();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar2.php") ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px;">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">MY CLASSES TIMETABLE</h6>

                                <div class="table-responsive" style="margin-top:30px;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr class="text-center">
                                    <th style="font-size:1rem; color:#FFFFFF; background-color:#f2f4f8"></th>
                                        <?php 
                                            for($i = 0; $i < count($scheduleDays); $i++){ ?>
                                            <th style="font-size:1.1rem; color:#FFFFFF; font-weight:500; background-color:#afbabf"><?php echo $scheduleDays[$i]->day; ?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        for($i = 0; $i < count($ClassTimes); $i++){ ?>
                                        <tr class="text-center">
                                        <td style="font-size:1.1rem; color:#FFFFFF; background-color:#afbabf; font-weight:500;"><?php echo $ClassTimes[$i]->timeName ?></td>
                                        <?php for($j = 0; $j < count($scheduleDays); $j++){
                                        $timeSchedule = $timetableClass->fetchAllClassOrStaffTimeSchedule($_SESSION["uniqueNumber"], $ClassTimes[$i]->timeCode, $scheduleDays[$j]->dayCode, $schoolDetails->academic_session, $schoolDetails->academic_term); 
                                        if(count($timeSchedule) > 0){
                                            for($k = 0; $k < count($timeSchedule); $k++){ ?>
                                            <td onclick="alert('set personal remider')" class="my_hover_up" style="font-weight:500; cursor:pointer; font-size:1.1rem; color:#FFFFFF; background-color:<?php echo $timeSchedule[$k]->timeColor ?>"><?php echo $timeSchedule[$k]->timeSubject ?><span style="font-weight:400; display:block; font-size:0.9rem; font-style:italic"><?php echo $timeSchedule[$k]->timeClass ?></td>   
                                        <?php } 
                                        }else{ ?>
                                            <td class="" style="cursor:pointer; font-size:1.2rem; color:#FFFFFF; background-color:#afbabf"></td>                                            
                                        <?php } } } ?>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>