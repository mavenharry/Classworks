<?php include_once ("homebtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Home</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/.css" rel="stylesheet">
</head>
            <!-------------------- START - MODAL -------------------->
                <?php 
                 $schoolDetails = $homeClass->getSchoolDetails(); 
                include ("../../includes/modals/index.php"); 
                ?>
            <!-------------------- END - MODAL -------------------->
<?php
 $homeCount = $homeClass->homeCount();
 ?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
    
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            <?php
            if($schoolDetails->init_slider == 0){
                include_once ("../../includes/slider.php");
                $homeClass->updateInitSlider();
            } ?>
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Dashboard", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:0px;"><div>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <div style="margin-top:-15px;" class="element-actions">
                                        <div style="margin-right:10px">
                                            <h3><span style="font-weight:600" id="attendanceTime">00:00</span></h3>
                                        </div>
                                    </div>
                                    <h6 class="element-header">SUMMARY/OVERVIEW</h6>
                                    <div class="element-content">
                                        <div class="row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../students">
                                                    <div class="label">ALL Students</div>
                                                    <div style="margin-left:0; color: #e65252" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user-male-circle"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->studentNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../staffs">
                                                    <div class="label">ALL Staffs</div>
                                                    <div style="margin-left:0; color:#fd7e14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo $homeCount->staffNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../parents">
                                                    <div class="label">ALL Parents</div>
                                                    <div style="margin-left:0; color:#5bc0de" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-users"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo $homeCount->parentNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../payments/paid_fees_breakdown">
                                                    <div class="label">This term's paid fees</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="" style="font-size:2rem">&#x20a6;</div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis" style="margin-left:2px; margin-bottom:0px;"><?php echo number_format($homeCount->thisTermPayment); ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../messaging">
                                                    <div class="label">Total SMS units Left</div>
                                                    <div style="margin-left:0; color:#5bc0de" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-email-forward"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $schoolDetails->smsUnit; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to buy</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Books in Library</div>
                                                    <div style="margin-left:0; color:#e65252" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-newspaper"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../classes">
                                                    <div class="label">All Classes</div>
                                                    <div style="margin-left:0; color:#fd7e14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->classesNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../subjects">
                                                    <div class="label">All Subjects</div>
                                                    <div style="margin-left:0; color:#974A6D" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-tasks-checked"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->subjectNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../attendance/staff">
                                                    <div class="label">Staffs Attendance</div>
                                                    <div style="margin-left:0; color:#F91F14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->staffRate; ?><span>%</span></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../attendance/student">
                                                    <div class="label">Students Attendance</div>
                                                    <div style="margin-left:0; color:#191F6F" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->studentRate; ?><span>%</span></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Ongoing Classes</div>
                                                    <div style="margin-left:0; color:#1BB4AD" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../admission/applicant_list">
                                                    <div class="label">New Enrollments</div>
                                                    <div style="margin-left:0; color:#F4B510" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-edit-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
                    <!--------------------  START - Sidebar -------------------->
                    <div class="content-panel">
                        <div class="content-panel-close">
                            <i class="os-icon os-icon-close"></i>
                        </div>
                        <div class="element-wrapper">
                            <h6 class="element-header">QUICK ACTION LINKS</h6>
                            <div class="element-box-tp">
                                <div class="el-buttons-list full-width">

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" target="_blank" href="https://classworks.xyz/<?php echo $schoolDetails->domain_name ?>">
                                        <i class="os-icon os-icon-grid-18"></i>
                                        <span>View Official Web Page</span>
                                    </a>

                                    <a data-target="#broadcastModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-mail"></i>
                                        <span>Broadcast SMS Notice</span>
                                    </a>

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #08ACF0; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" data-target="#paymentSwitch" data-toggle="modal" href="#">
                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                        <span>Process New Payment</span>
                                    </a>

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" data-target="#noticeModal" data-toggle="modal" href="#">
                                        <i class="os-icon os-icon-mail"></i>
                                        <span>Publish Quick Notice</span>
                                    </a>

                                    <a id="academicUpdate" data-target="#newAcademicModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #974A6D; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Update Academic Info</span>
                                    </a>

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #FC9933; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="../attendance/attendance_scanner" target="_blank">
                                        <i class="os-icon os-icon-calendar-time"></i>
                                        <span>Take Today's Attendance</span>
                                    </a>

                                    <a href="../childpickup/childpickup_scanner.php" target="_blank" style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-fingerprint"></i>
                                        <span>Verify Child Pick Up</span>
                                    </a>

                                    <a data-target="#newPickupModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Add Pickup Person</span>
                                    </a>

                                    <a data-target="#newStudentModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #08ACF0; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Create a New Student</span>
                                    </a>

                                    <a data-target="#newStaffModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #974A6D; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Create a New Staff</span>
                                    </a>

                                    <a data-target="#newParentModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FC9933; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Create a New Parent</span>
                                    </a>

                                    <a data-target="#newClassModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Create a New Class</span>
                                    </a>

                                    <a data-target="#newSubjectModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Create a New Subject</span>
                                    </a>

                                    <a data-target="#newEnrollModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #8095A0; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Publish New Enrollments</span>
                                    </a>

                                </div>
                            </div>
                        </div>

                        
                        <!-------------------- END - Sidebar -------------------->
                    </div>
                </div>
                
                <!-- START CALENDER -->
                
                <div class="content-box" style="margin-top: -90px;">
                <div class="element-wrapper">
                <div class="element-box">
                  <h5 class="form-header">
                    Monthly Activity & Event Calendar
                  </h5>
                  <div id="fullCalendar"></div>
                </div>
                </div>
                
                <!-- END CALENDAR -->

            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

<script>
    // var today = new Date()
    // var curHr = today.getHours()
    // if (curHr < 12) {
    // speak("Good Morning, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // } else if (curHr < 18) {
    // speak("Good Afternoon, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // } else {
    // speak("Good Evening, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // }
</script>

</body>

</html>