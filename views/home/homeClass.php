<?php
session_start();
    // $pathToInclude = @end(explode("/", $_SERVER["DOCUMENT_ROOT"]));
    // $pathToSchoolImages = ../../$pathToInclude/SchoolImages/
    // include_once ("../../$pathToInclude/config.php");
    include_once ("../../".$_SESSION['folderName']."/config.php");

    class homeClass extends logic {

        function updateInitSlider(){
            $stmt = $this->uconn->prepare("UPDATE settings SET init_slider = 1");
            $stmt->execute();
        }

        function homeCount(){
            $term = $this->getSchoolDetails()->academic_term;
            $session = $this->getSchoolDetails()->academic_session;
            $obj = new stdClass();
            $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM teachers");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $teachersNumber = array_shift($stmt_result)["num"];
            $obj->staffNumber = $teachersNumber;
            $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM students");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $studentNumber = array_shift($stmt_result)["num"];
            $obj->studentNumber = $studentNumber;
            $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM parents");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->parentNumber = array_shift($stmt_result)["num"];
            $stmt->close();
            $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM classes");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->classesNumber = array_shift($stmt_result)["num"];
            $stmt->close();
            $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT subjectCode) AS num FROM subjects");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->subjectNumber = array_shift($stmt_result)["num"];
            $stmt = $this->uconn->prepare("SELECT SUM(amount) AS thisTermPayment FROM payed_student WHERE term = ? AND session = ?");
            $stmt->bind_param("ss", $term, $session);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->thisTermPayment = array_shift($stmt_result)["thisTermPayment"];
            $idType = 0;
            $date = date("Y-m-d");
            $stmt = $this->uconn->prepare("SELECT COUNT(id) AS staffAttendanceRate FROM attendancerecord WHERE idType = ? AND date_created LIKE CONCAT(?, '%')");
            $stmt->bind_param("is", $idType, $date);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffAttendanceRate = array_shift($stmt_result)["staffAttendanceRate"];
            $obj->staffRate = round(($staffAttendanceRate / $teachersNumber) * 100, 2);
            $idType = 1;
            $stmt = $this->uconn->prepare("SELECT COUNT(id) AS studentAttendanceRate FROM attendancerecord WHERE idType = ? AND date_created LIKE CONCAT(?, '%')");
            $stmt->bind_param("is", $idType, $date);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $studentAttendanceRate = array_shift($stmt_result)["studentAttendanceRate"];
            if($studentNumber == 0){
                $obj->studentRate = 0;    
            }else{
                $obj->studentRate = round(($studentAttendanceRate / $studentNumber) * 100, 2);
            }
            $stmt->close();
            return $obj;
        }



        function homeCount_staff(){
            $obj = new stdClass();
            $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT students.student_number) AS num FROM students INNER JOIN subjectTeachers ON subjectTeachers.staffCode = ? INNER JOIN classes ON classes.class_code = subjectTeachers.classCode AND students.present_class = classes.class_name");
            $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->studentNumber = array_shift($stmt_result)["num"];
            $stmt = $this->uconn->prepare("SELECT COUNT(*) AS num FROM parents");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->parentNumber = array_shift($stmt_result)["num"];
            $stmt->close();
            $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT classCode) AS num FROM subjectTeachers WHERE staffCode = ?");
            $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->classesNumber = array_shift($stmt_result)["num"];
            $stmt->close();
            $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT subjectCode) AS num FROM subjectTeachers WHERE staffCode = ?");
            $stmt->bind_param("s", $_SESSION["uniqueNumber"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj->subjectNumber = array_shift($stmt_result)["num"];
            $stmt->close();
            return $obj;
        }
        

        function allStaff(){
            $stmt = $this->uconn->prepare("SELECT * FROM teachers");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $staffArray = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->name = $row["fullname"];
                $staffArray[] = $obj;
            }
            $stmt->close();
            return $staffArray;
        }
    }
?>