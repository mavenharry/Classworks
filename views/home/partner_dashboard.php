<?php include_once ("partnerBtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Home</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/.css" rel="stylesheet">
</head>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
    
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar3.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:0px;"><div>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <div style="margin-top:-15px;" class="element-actions">
                                        <div style="margin-right:10px">
                                            <h3><span style="font-weight:600" id="attendanceTime">00:00</span></h3>
                                        </div>
                                    </div>
                                    <h6 class="element-header">SUMMARY/OVERVIEW</h6>
                                    <div class="element-content">
                                        <div class="row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./all_schools?key=all">
                                                    <div class="label">ALL My Schools</div>
                                                    <div style="margin-left:0; color: #e65252" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-tasks-checked"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./all_schools?key=active">
                                                    <div class="label">All Active Schools</div>
                                                    <div style="margin-left:0; color:#fd7e14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./all_schools?key=expired">
                                                    <div class="label">All expired Schools</div>
                                                    <div style="margin-left:0; color:#1BB4AD" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-12 col-xxxl-12">
                                                <a class="element-box el-tablo" style="border:2px dashed #08ACF0" href="#">
                                                    <h4 style="font-weight:normal; margin-right:30px; line-height:40px"><span style="font-weight:bold">NOTICE:</span> RICH OAK INTERNATIONAL SCHOOL is expiring soon, contact school to renew subscription.</h4>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
                    <!--------------------  START - Sidebar -------------------->
                    <div class="content-panel">
                        <div class="content-panel-close">
                            <i class="os-icon os-icon-close"></i>
                        </div>
                        <div class="element-wrapper">
                            <h6 class="element-header">QUICK ACTION LINKS</h6>
                            <div class="element-box-tp">
                                <div class="el-buttons-list full-width">

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="../authentication/signup.php">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Add a New School</span>
                                    </a>

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #08ACF0; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" data-target="#paymentSwitch" data-toggle="modal" href="#">
                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                        <span>Renew School Subscription</span>
                                    </a>

                                    <a data-target="#newParentModal" data-toggle="modal" href="" style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-settings"></i>
                                        <span>Change My Password</span>
                                    </a>

                                </div>
                            </div>
                        </div>

                        
                        <!-------------------- END - Sidebar -------------------->
                    </div>
                </div>
                
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../partners/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/partnerScript.php"); ?>

<!-- <script>
    var today = new Date()
    var curHr = today.getHours()
    if (curHr < 12) {
    speak("Good Morning, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    } else if (curHr < 18) {
    speak("Good Afternoon, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    } else {
    speak("Good Evening, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    }
</script> -->

</body>

</html>