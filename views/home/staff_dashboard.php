<?php include_once ("homebtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Home</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/.css" rel="stylesheet">
</head>
<?php
 $schoolDetails = $homeClass->getSchoolDetails(); 
 $homeCount = $homeClass->homeCount_staff();
 ?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
    
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            <?php
            if($schoolDetails->init_slider == 0){
                include("../../includes/slider.php");
                $homeClass->updateInitSlider();
            } ?>
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar2.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:0px;"><div>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <div style="margin-top:-15px;" class="element-actions">
                                        <div style="margin-right:10px">
                                            <h3><span style="font-weight:600" id="attendanceTime">00:00</span></h3>
                                        </div>
                                    </div>
                                    <h6 class="element-header">SUMMARY/OVERVIEW</h6>
                                    <div class="element-content">
                                        <div class="row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../students/staff_students">
                                                    <div class="label">ALL My Students</div>
                                                    <div style="margin-left:0; color: #e65252" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user-male-circle"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->studentNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../classes/staff_classes">
                                                    <div class="label">All My Classes</div>
                                                    <div style="margin-left:0; color:#fd7e14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->classesNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../subjects/staff_subjects">
                                                    <div class="label">All My Subjects</div>
                                                    <div style="margin-left:0; color:#974A6D" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-tasks-checked"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;"><?php echo $homeCount->subjectNumber; ?></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../attendance/staff">
                                                    <div class="label">Your Attendance Rate</div>
                                                    <div style="margin-left:0; color:#F91F14" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-newspaper"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0<span>%</span></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="../attendance/student">
                                                    <div class="label">My Students Attendance</div>
                                                    <div style="margin-left:0; color:#191F6F" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-newspaper"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 2rem; margin-left:10px;">0<span>%</span></div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Next Lesson Time</div>
                                                    <div style="margin-left:0; color:#1BB4AD" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value hideThis" style="font-size: 1.5rem; margin-left:10px;">2:00 PM</div>
                                                    <div style="display:none; margin-left:-10px" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-12 col-xxxl-12">
                                                <a class="element-box el-tablo" style="border:2px dashed #08ACF0" href="#">
                                                    <h4 style="font-weight:normal; margin-right:30px; line-height:40px"><span style="font-weight:bold">NOTICE:</span> <?php echo $schoolDetails->quickNotice == "" ? "Welcome ".$_SESSION["fullname"].", Classwroks is a full fledged school automation solution built to simplify daily school academic activities. " : $schoolDetails->quickNotice ?></h4>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
                    <!--------------------  START - Sidebar -------------------->
                    <div class="content-panel">
                        <div class="content-panel-close">
                            <i class="os-icon os-icon-close"></i>
                        </div>
                        <div class="element-wrapper">
                            <h6 class="element-header">QUICK ACTION LINKS</h6>
                            <div class="element-box-tp">
                                <div class="el-buttons-list full-width">

                                    <a data-target="#newAcademicModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #974A6D; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-clock"></i>
                                        <span>Send a Leave Request</span>
                                    </a>

                                    <a style="color:#FFFFFF; margin-top:12px; background-color: #FC9933; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="../attendance/attendance_scanner" target="_blank">
                                        <i class="os-icon os-icon-calendar-time"></i>
                                        <span>Take Today's Attendance</span>
                                    </a>

                                    <a href="../childpickup/childpickup_scanner.php" target="_blank" style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-fingerprint"></i>
                                        <span>Verify Child Pick Up</span>
                                    </a>

                                    <a data-target="#newQuestionModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #FB5574; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>Add Exam Question</span>
                                    </a>

                                    <a data-target="#subjectExamScoreModal" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #974A6D; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-tasks-checked"></i>
                                        <span>View Exam Scores</span>
                                    </a>

                                    <a href="../examination/staffOnGoing.php" style="color:#FFFFFF; margin-top:12px; background-color: #FC9933; border-width:0" class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span>View Ongoing Exams</span>
                                    </a>

                                    <a data-target="#assessmentSelect" data-toggle="modal" style="color:#FFFFFF; margin-top:12px; background-color: #1BB4AD; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-edit-1"></i>
                                        <span>Record Assessments</span>
                                    </a>

                                    <a data-target="#changePasswordModal" data-toggle="modal" href="" style="color:#FFFFFF; margin-top:12px; background-color: #08ACF0; border-width:0"
                                        class="my_hover_up btn btn-white btn-sm" href="#">
                                        <i class="os-icon os-icon-settings"></i>
                                        <span>Change Password</span>
                                    </a>

                                </div>
                            </div>
                        </div>

                        
                        <!-------------------- END - Sidebar -------------------->
                    </div>
                </div>
                
                <!-- START CALENDER -->
                
                <div class="content-box" style="margin-top: -90px;">
                <div class="element-wrapper">
                <div class="element-box">
                  <h5 class="form-header">
                    Monthly Activity & Event Calendar
                  </h5>
                  <div id="fullCalendar"></div>
                </div>
                </div>
                
                <!-- END CALENDAR -->

            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

<script>
    // var today = new Date()
    // var curHr = today.getHours()
    // if (curHr < 12) {
    // speak("Good Morning, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // } else if (curHr < 18) {
    // speak("Good Afternoon, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // } else {
    // speak("Good Evening, Welcome to ClassWorks, Do you need any help?. Don't forget to talk to me using the help desk button below.");
    // }
</script>

</body>

</html>