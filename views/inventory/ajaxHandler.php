<?php include_once ("inventorybtn.php"); 

    if(isset($_GET["getItemProp"])){
        $value = htmlentities(trim($_GET["getItemProp"]));
        $reply = $inventoryClass->getAllItemWithName($value);
        if(count($reply) == 0){ ?>
            <option>No size for this item</option>
        <?php }else{ ?>
            <option value="">Select Size</option>
            <?php foreach($reply as $key => $value){
                if(!empty($value->itemSize)){?>
                <option value="<?php echo $value->itemSize ?>"><?php echo $value->itemSize?></option>
            <?php }
                }
        }
    }

    if(isset($_GET["getItemColor"])){
        $value = htmlentities(trim($_GET["getItemColor"]));
        $reply = $inventoryClass->getAllItemWithName($value);
        if(count($reply) == 0){ ?>
            <option>No color for this item</option>
        <?php }else{ ?>
            <option value="">Select Color</option>
            <?php foreach($reply as $key => $value){
                if(!empty($value->itemColor)){?>
                <option value="<?php echo $value->itemColor ?>"><?php echo $value->itemColor?></option>
            <?php }
                }
        }
    }
?>