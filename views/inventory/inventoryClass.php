<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class inventoryClass extends logic {

    function getItems($state){
        switch($state){
            case 'low':
                $stmt = $this->uconn->prepare("SELECT * FROM inventory_items INNER JOIN inventory_settings on inventory_items.itemQuantity < inventory_settings.lowStock");
                break;
            case 'out':
                $stmt = $this->uconn->prepare("SELECT * FROM inventory_items INNER JOIN inventory_settings on inventory_items.itemQuantity < inventory_settings.outStock");
                break;
            default:
                $stmt = $this->uconn->prepare("SELECT * FROM inventory_items");
                break;
        }
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $items = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->itemNo = $row["itemNo"];
                $obj->itemName = $row["itemName"];
                $obj->itemSize = $row["itemSize"];
                $obj->itemColor = $row["itemColor"];
                $obj->itemQuantity = $row["itemQuantity"];
                $obj->itemPrice = $row["itemPrice"];
                $obj->itemLocation = $row["itemLocation"];
                $items[] = $obj;
            }
            $stmt->close();
            return $items;
        }else{
            $stmt->close();
            return $items;
        }
    }

    function getAllItemWithName($value){
        $stmt = $this->uconn->prepare("SELECT * FROM inventory_items WHERE itemName = ?");
        $stmt->bind_param("s", $value);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $items = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->itemSize = $row["itemSize"];
                $obj->itemColor = $row["itemColor"];
                $obj->itemPrice = $row["itemPrice"];
                $items[] = $obj;
            }
            $stmt->close();
            return $items;
        }else{
            $stmt->close();
            return $items;
       }
    }

    function getInventoryRecord(){
        $stmt = $this->uconn->prepare("SELECT inventory_items.itemName, inventory_items.itemPrice, inventory_items.itemNo, inventory_items.itemColor, inventory_items.itemSize, inventory_record.collectedBy, inventory_record.issuedBy, inventory_record.itemQuantity, inventory_record.date_collected FROM inventory_record INNER JOIN inventory_items ON inventory_record.itemNo = inventory_items.itemNo");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $record = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->itemName = $row["itemName"];
                $obj->itemPrice = $row["itemPrice"];
                $obj->itemNo = $row["itemNo"];
                $obj->itemColor = $row["itemColor"];
                $obj->itemSize = $row["itemSize"];
                $obj->collectedBy = $row["collectedBy"];
                $obj->issuedBy = $row["issuedBy"];
                $obj->itemQuantity = $row["itemQuantity"];
                $obj->dateCollected = $row["date_collected"];
                $record[] = $obj;
            }
            $stmt->close();
            return $record;
        }else{
            $stmt->close();
            return $record;
        }
    }
}
?>