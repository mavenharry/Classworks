<?php include_once ("inventorybtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Inventory Records</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $inventoryClass->getSchoolDetails();
    $record = $inventoryClass->getInventoryRecord();
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Inventory", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchStudentsExamModal" data-toggle="modal" style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL INVENTORY COLLECTED ITEMS RECORD</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Item No.</th>
                                                <th>Item Name</th>
                                                <th>Item Description</th>
                                                <th>Item Price</th>
                                                <th>Quantity Collected</th>
                                                <th>Collected By</th>
                                                <th>Processed By</th>
                                                <th>Collection Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($record) == 0){
                                                        echo "No data in record";
                                                    }else{
                                                        foreach($record as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->itemNo; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->itemName); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->itemColor.", ".$value->itemSize; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><span>&#8358;</span><?php echo number_format($value->itemPrice); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->itemQuantity; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->collectedBy); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->issuedBy)?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->dateCollected; ?></span>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>