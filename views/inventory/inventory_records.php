<?php include_once ("inventorybtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Inventory Records</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $inventoryClass->getSchoolDetails();
    $state = htmlentities(trim($_GET["status"]));
    $items = $inventoryClass->getItems($state);
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Inventory", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchItemModal" data-toggle="modal" style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a data-target="#deleteItemModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete an Item</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <?php if($_GET["status"] == "low"){ ?>
                                    <h6 class="element-header">INVENTORY LOW IN STOCK ITEMS</h6>
                                <?php }else{ 
                                    if($_GET["status"] == "out"){
                                    ?>
                                    <h6 class="element-header">INVENTORY OUT OF STOCK ITEMS</h6>
                                <?php }else{ ?>
                                    <h6 class="element-header">ALL INVENTORY STOCKED ITEMS</h6>
                                <?php }} ?>
                                
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Item No.</th>
                                                <th>Item Name</th>
                                                <th>Color</th>
                                                <th>Size</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                <th>Location</th>
                                                <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($items) == 0){
                                                        echo "No item in stock";
                                                    }else{
                                                        foreach($items as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->itemNo; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->itemName); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->itemColor); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->itemSize); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo number_format($value->itemQuantity); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><span><?php echo $schoolDetails->currencySymbol; ?></span><?php echo number_format($value->itemPrice); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->itemLocation); ?></span>
                                                                </td>
                                                                <td>
                                                                    <?php if($_GET["status"] == "low"){ ?>
                                                                        <span style="color:yellow; font-weight:bold;">Low-in-stock</span>
                                                                    <?php }else{ 
                                                                        if($_GET["status"] == "out"){
                                                                        ?>
                                                                        <span style="color:red; font-weight:bold;">Out-of-stock</span>
                                                                    <?php }else{ ?>
                                                                        <span style="color:green; font-weight:bold;">In-stock</span>
                                                                    <?php }} ?>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>