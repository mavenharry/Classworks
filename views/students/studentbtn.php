<?php
// session_start();
include_once ("studentClass.php");
$studentClass = new studentClass();

if(isset($_POST["bioData"])){
    @$fullname = htmlentities(trim($_POST["fullname"]));
    @$languages = json_encode($_POST["languages"]);
    @$studentsClass = htmlentities(trim($_POST["studentClass"]));
    @$studentDesc = htmlentities(trim($_POST["studentDesc"]));
    @$studentCountry = htmlentities(trim($_POST["studentCountry"]));
    @$studentState = htmlentities(trim($_POST["studentState"]));
    @$studentCity = htmlentities(trim($_POST["studentCity"]));
    @$studentDob = htmlentities(trim($_POST["studentDob"]));
    $all = explode("/", $studentDob);
    @$studentDob = $all[2]."-".$all[1]."-".$all[0];
    @$studentGender = strtolower(htmlentities(trim($_POST["studentGender"])));
    @$studentBloodGroup = strtolower(htmlentities(trim($_POST["studentBloodGroup"])));
    @$profileChecker = htmlentities(trim($_POST["profileChecker"]));
    @$studentNumber = htmlentities(trim($_POST["studentNumber"]));
    @$fatherPhone = "0".substr(htmlentities(trim($_POST["fatherPhone"])), -10, 10);
    @$motherPhone = "0".substr(htmlentities(trim($_POST["motherPhone"])), -10, 10);

    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $studentClass->getSchoolDetails()->pathToPassport;
            $filename = str_replace("/","_",$studentNumber).".".$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }else{
        $filename = $profileChecker;
    }

    $reply = $studentClass->updateStudentDetails($studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $fullname, $languages, $studentsClass, $studentDesc, $filename, $fatherPhone, $motherPhone, $studentNumber);

    if($reply === true){
        $mess = 'You have successfully updated a students biodata.';
    }else{
        $err = "We are so sorry an error occured during your last operation. Please try again later.";
    }
}

if(isset($_POST["saveSettings"])){
    $pickupVerification = htmlentities(trim($_POST["pickupVerification"]));
    $busPickup = htmlentities(trim($_POST["busPickup"]));
    $busDropoff = htmlentities(trim($_POST["busDropoff"]));
    $studentPortalAccess = htmlentities(trim($_POST["studentPortalAccess"]));
    $parentPortalAccess = htmlentities(trim($_POST["parentPortalAccess"]));
    $studentNumber = htmlentities(trim($_POST["studentNumber"]));

    $reply = $studentClass->updateSettings($pickupVerification, $busPickup, $busDropoff, $studentPortalAccess, $parentPortalAccess, $studentNumber);

    if($reply === true){
        $mess = 'You have successfully updated a students record.';
    }else{
        $err = "We are so sorry an error occured during your last operation. Please try again later.";
    }
}

if(isset($_POST["contactParent"])){
    $parentsContact = $_POST["parentsContact"];
    $message = htmlentities(trim($_POST["contactMessage"]));
    $pageNumber = ceil(strlen($message)/160);
    $totalNumber = $pageNumber * count($parentsContact);
    $smsUnit = $studentClass->checkSmsBalance();
    if($totalNumber > $smsUnit){
        $err = "You do not have enough sms unit left. Please recharge and try again";
    }else{
        $date = date("Y-m-d");
        $reply = $studentClass->sendSMS($message, $studentClass->getSchoolDetails()->domain_name, $parentsContact);
        if($reply === true){
            $unit = $smsUnit - $totalNumber;
            $reply = $studentClass->updateSmsUnit($unit);
            if($reply === true){
                $reply = $studentClass->logMessage(["specify"], $date, $message, $parentsContact, 1);
                if($reply === true){
                    $mess = "Message sent successfully";
                }else{
                    $err = "1The System encountered an error while trying to perform this operation. Please try again later";
                }
            }else{
                $err = "2The System encountered an error while trying to perform this operation. Please try again later";
            }
        }else{
            $err = "3The System encountered an error while trying to perform this operation. Please try again later";
        }
    }
}

?>