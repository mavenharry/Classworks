<?php include_once ("studentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>

    <!-------------------- START - MODAL -------------------->
    <?php include("../../includes/modals/index.php") ?>
    <!-------------------- END - MODAL -------------------->
<?php
$schoolDetails = $studentClass->getSchoolDetails();
if(isset($_GET["class_id"])){
    $allStudent = $studentClass->getallClassStudents($studentClass->getClassName($_GET['class_id']));
}else{
    $allStudent = $studentClass->fetchAllStudent_Staff($_SESSION["uniqueNumber"]);
}
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar2.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php");
                    if(!in_array("Students", $allowedPages)){
                        echo "<script>window.location.assign('../../error')</script>";
                    }
                ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL MY STUDENT(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>Serial no.</th>
                                                <th>Passport</th>
                                                <th>Student number</th>
                                                <th>Student name</th>
                                                <th>Gender</th>
                                                <th>Class name</th>
                                                <th>Academic Record</th>
                                                <th class="text-center">Profile</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allStudent) == 0){
                                                    echo "No student in record";
                                                }else{
                                                    for($i = 0; $i < count($allStudent); $i++){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $i + 1 ?>.</span>
                                                            </td>
                                                            <td>

                                                                <?php if(!empty($allStudent[$i]->profilePhoto)){ ?>
                                                                <div class="user-with-avatar">
                                                                <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$allStudent[$i]->profilePhoto;?>">
                                                                </div>
                                                                <?php }else{ ?>
                                                                    <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                    class="badge"><?php echo $nameInitials = $studentClass->createAcronym($allStudent[$i]->fullname); ?></a>
                                                                <?php } ?>

                                                            </td>
                                                            <td>
                                                                <span><?php echo $allStudent[$i]->student_number; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($allStudent[$i]->fullname); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucfirst($allStudent[$i]->gender); ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <span><?php echo ucwords($allStudent[$i]->present_class); ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="../assessment/report_sheet?student_id=<?php echo $allStudent[$i]->student_number ?>&session=<?php echo $schoolDetails->academic_session ?>&term=<?php echo $schoolDetails->academic_term ?>">View assessment</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="../students/profile.php?stdn=<?php echo base64_encode($allStudent[$i]->student_number); ?>">View profile</a>
                                                            </td>
                                                        </tr>
                                                <?php    }
                                                }
                                            ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allStudent) > 20){ ?>
                                                <div style="float: right;" class-"element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->
               
<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>