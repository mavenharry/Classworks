<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class studentClass extends logic {


    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return $value;
        }else{
            return "N/A";
        }
    }

    
    function fetchAllStudentLimit(){
        $stmt = $this->uconn->prepare("SELECT * FROM students ORDER BY RAND() LIMIT 0,100");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $studentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->student_number = $this->outputRefine($row["student_number"]);
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $studentArray[] = $obj;
        }
        $stmt->close();
        return $studentArray;
    }

    function studentPageCount(){
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS currentStudent FROM students");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        $currentStudent = array_shift($stmt_result)["currentStudent"];
        $obj->currentStudent = $currentStudent;
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS totalAlumni FROM alumni");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $totalAlumni = array_shift($stmt_result)["totalAlumni"];
        $obj->totalNumber = $currentStudent + $totalAlumni;
        $stmt->close();
        return $obj;
    }


    function fetchAllStudent(){
        $stmt = $this->uconn->prepare("SELECT * FROM students");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $studentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->student_number = $this->outputRefine($row["student_number"]);
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $studentArray[] = $obj;
        }
        $stmt->close();
        return $studentArray;
    }


    function fetchAllStudent_Staff($staffNo){
        $stmt = $this->uconn->prepare("SELECT * FROM students INNER JOIN classes WHERE classes.class_code IN (SELECT DISTINCT classCode FROM subjectTeachers WHERE subjectTeachers.staffCode = ?) AND students.present_class = classes.class_name");
        $stmt->bind_param("s", $staffNo);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $studentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->student_number = $this->outputRefine($row["student_number"]);
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $studentArray[] = $obj;
        }
        $stmt->close();
        return $studentArray;
    }

    function getStudentDetails($studentNumber){
        $stmt = $this->uconn->prepare("SELECT * FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->fullname = $this->outputRefine($row["fullname"]);
        $obj->present_class = $this->outputRefine($row["present_class"]);
        $obj->profilePhoto = $row["profilePhoto"];
        $obj->nationality = $this->outputRefine($row["nationality"]);
        $obj->state = $this->outputRefine($row["state"]);
        $obj->city = $this->outputRefine($row["city"]);
        $obj->gender = $this->outputRefine($row["gender"]);
        $obj->blood_group = $this->outputRefine($row["blood_group"]);
        $from = new DateTime($row["dob"]);
        $to   = new DateTime('today');
        $obj->age = $from->diff($to)->y;
        $dob = explode("-", $row["dob"]);
        $newDob = $dob[2]."/".$dob[1]."/".$dob[0];
        $obj->dob = $newDob;
        $obj->enrolment_year = $this->outputRefine($row["enrolment_year"]);
        $obj->pickup_verification = $row["pickup_verification"];
        $obj->bus_pickup = $row["bus_pickup"];
        $obj->bus_dropoff = $row["bus_dropoff"];
        $obj->student_portal = $row["student_portal"];
        $obj->parent_portal = $row["parent_portal"];
        $obj->languages = json_decode($row["languages"], JSON_PRETTY_PRINT);
        $obj->student_desc = $row["student_desc"];
        $obj->accountBalance = $row["accountBalance"];
        $obj->fatherPhone = $row["father_phone"];
        $obj->motherPhone = $row["mother_phone"];
        $stmt->close();
        return $obj;
    }

    function updateStudentDetails($studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $fullname, $languages, $studentClass, $studentDesc, $filename, $fatherPhone, $motherPhone, $studentNumber){
        $stmt = $this->uconn->prepare("UPDATE students SET nationality = ?, state = ?, city = ?, dob = ?, gender = ?, blood_group = ?, fullname = ?, languages = ?, present_class = ?, student_desc = ?, profilePhoto = ?, father_phone = ?, mother_phone = ? WHERE student_number = ?");
        $stmt->bind_param("ssssssssssssss", $studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $fullname, $languages, $studentClass, $studentDesc, $filename, $fatherPhone, $motherPhone, $studentNumber);
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    function updateSettings($pickupVerification, $busPickup, $busDropoff, $studentPortalAccess, $parentPortalAccess, $studentNumber){
        $stmt = $this->uconn->prepare("UPDATE students SET pickup_verification = ?, bus_pickup = ?, bus_dropoff = ?, student_portal = ?, parent_portal = ? WHERE student_number = ?");
        $stmt->bind_param("iiiiis", $pickupVerification, $busPickup, $busDropoff, $studentPortalAccess, $parentPortalAccess, $studentNumber);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function getParentDetails($studentNumber){
        $parent = array();
        $stmt = $this->uconn->prepare("SELECT father_phone, mother_phone FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        if(empty($row["father_phone"]) && empty($row["mother_phone"])){
            $stmt->close();
            return false;
        }else{
             foreach ($row as $key => $value) {
                 # code...
                 if(!empty($value)){
                    $stmt2 = $this->uconn->prepare("SELECT fullname, guardian_number, relation_student, profilePhoto, phone FROM parents WHERE phone = ?");
                    $stmt2->bind_param("s", $value);
                    $stmt2->execute();
                    $stmt2_result = $this->get_result($stmt2);
                    if(count($stmt2_result) == 0){
                        $obj = new stdClass();
                        $obj->val = false;
                        $obj->relationship = $key;
                        $parent[] = $obj;
                    }else{
                        $row = array_shift($stmt2_result);
                        $obj = new stdClass();
                        $obj->val = true;
                        $obj->fullname = $row["fullname"];
                        $obj->profilePhoto = $row["profilePhoto"];
                        $obj->relationship = $row["relation_student"];
                        $obj->phone = $row["phone"];
                        $obj->parentNumber = $row["guardian_number"];
                        $parent[] = $obj;
                    }
                 }
             }
             $stmt->close();
             $stmt2->close();
             return $parent;
        }
    }

    function getPickupDetails($studentNumber){
        $pickups = array();
        $stmt = $this->uconn->prepare("SELECT fullname, relation_student, profilePhoto FROM childpickup WHERE studentNumber = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) == 0){
            $stmt->close();
            return false;
        }else{
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->val = true;
                $obj->fullname = $row["fullname"];
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->relationship = $row["relation_student"];
                $pickups[] = $obj;
            }
            $stmt->close();
            return $pickups;
        }
    }

    function studentSort($sortClass, $sortGender, $searchKeyword){
        $studentArray = array();
        if(!empty($sortClass) && !empty($sortGender) && !empty($searchKeyword)){
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE gender = ? AND present_class = ? AND student_number = ? OR fullname = ?");
            $stmt->bind_param("ssss", $sortGender, $sortClass, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $row = array_shift($stmt_result);
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $studentArray[] = $obj;
                return $studentArray;
            }else{
                return $studentArray;
            }
        }
        elseif (!empty($sortClass) && !empty($sortGender) && empty($searchKeyword)) {
            # code...
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE gender = ? AND present_class = ?");
            $stmt->bind_param("ss", $sortGender, $sortClass);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
        }
        elseif (!empty($sortClass) && empty($sortGender) && empty($searchKeyword)) {
            # code...
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE present_class = ?");
            $stmt->bind_param("s", $sortClass);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
            
        }
        elseif (empty($sortClass) && !empty($sortGender) && empty($searchKeyword)){
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE gender = ?");
            $stmt->bind_param("s", $sortGender);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
        }
        elseif (empty($sortClass) && empty($sortGender) && !empty($searchKeyword)){
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE fullname LIKE CONCAT('%',?,'%') OR student_number = ?");
            $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
        }
        elseif (!empty($sortClass) && empty($sortGender) && !empty($searchKeyword)) {
            # code...
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE present_class = ? AND fullname = ? OR student_number = ?");
            $stmt->bind_param("sss", $sortClass, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
        }
        elseif (empty($sortClass) && !empty($sortGender) && !empty($searchKeyword)) {
            $stmt = $this->uconn->prepare("SELECT fullname, profilePhoto, student_number, gender, present_class FROM students WHERE gender = ? AND fullname = ? OR student_number = ?");
            $stmt->bind_param("sss", $sortGender, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->student_number = $this->outputRefine($row["student_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->gender = $this->outputRefine($row["gender"]);
                    $obj->present_class = $this->outputRefine($row["present_class"]);
                    $obj->profilePhoto = $row["profilePhoto"];
                    $studentArray[] = $obj;
                }
                return $studentArray;
            }else{
                $stmt->close();
                return $studentArray;
            }
        }
    }

    function getOutstandingFee($studentNumber){
        $stmt = $this->uconn->prepare("SELECT outstanding_fee.amount, fees.fee_name FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.studentNumber = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $outstandingFee = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->amount = $row["amount"];
                $obj->feeName = $row["fee_name"];
                $outstandingFee[] = $obj;
            }
            $stmt->close();
            return $outstandingFee;
        }else{
            $stmt->close();
            return $outstandingFee;
        }
    }

    function fetchAllSystemStudents(){
        $stmt = $this->uconn->prepare("SELECT * FROM alumni");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $allStudent = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->tableFrom = "alumni";
                $allStudent[] = $obj;
            }
        }
        $stmt = $this->uconn->prepare("SELECT * FROM students");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->student_number = $this->outputRefine($row["student_number"]);
                $obj->fullname = $this->outputRefine($row["fullname"]);
                $obj->gender = $this->outputRefine($row["gender"]);
                $obj->present_class = $this->outputRefine($row["present_class"]);
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->tableFrom = "students";
                $allStudent[] = $obj;
            }
        }
        $stmt->close();
        return $allStudent;
    }

    function moveToAlumni($studentId){
        $stmt = $this->uconn->prepare("INSERT INTO alumni SELECT * FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $studentId);
        if($stmt->execute()){
            $stmt = $this->uconn->prepare("DELETE FROM students WHERE student_number = ?");
            $stmt->bind_param("s", $studentId);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }else{
            $stmt->close();
            return "false";
        }
    }

    
}
?>