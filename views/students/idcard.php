<?php include_once ("studentbtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
include_once ("idcardGenerator.php");
$schoolDetails = $studentClass->getSchoolDetails();
$studentNumber = htmlentities(trim(base64_decode($_GET["student_id"])));
$studentDetails = $studentClass->getStudentDetails($studentNumber);
$ex3 = new QRGenerator($studentNumber,500,'ISO-8859-1'); 
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Students", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <div style="float:left; margin-left:200px;">
                    <div class="id-card-tag"></div>
                    <div class="id-card-tag-strip"></div>
                    <div class="id-card-hook"></div>
                    <div class="id-card-holder">
                        <div class="id-card">
                            <div class="header">
                                <img style="object-fit:scale-down; width:60px;" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>">
                                <h6 style="font-size:0.7em"><?php echo $schoolDetails->school_name?></h6>
                                <h6 style="font-size:0.6em; margin-top:-7px;"><?php echo $schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country ?></h6>
                                <h3 style="font-size:0.6em; margin-top:-5px; color:#08acf0"><?php echo substr($schoolDetails->phone." | ".$schoolDetails->email, 0, 35) ?></h3>
                            </div>
                            <?php
                                if(empty($studentDetails->profilePhoto)){ ?>
                                    <div class="photo" style="margin-top:-5px">
                                        <img height="85" src="../../images/male-user-profile-picture.png">
                                    </div>
                                <?php }else{ ?>
                                    <div class="photo" style="margin-top:-5px">
                                        <img height="85" src="<?php echo $schoolDetails->pathToPassport."/".$studentDetails->profilePhoto?>">
                                    </div>
                                <?php }
                            ?>
                            <h2 style="color:#08acf0; font-size:1.2em; margin-top:10px;"><?php echo ucwords($studentDetails->fullname); ?></h2>
                            <h3 style="font-size:0.9em; margin-top:-3px; font-weight:400">Student/<?php echo ucwords($studentDetails->gender); ?></h3>
                            <hr style="border-color:#08acf0">
                            <div class="photo" style="margin-top:-5px">
                                <!-- <img style="width:100%; margin-top:-8px;" height="54" src="./idcardGenerator.php?text=<?php echo $studentNumber; ?>"> -->
                                <div class="photo" style="margin-top:-5px">
                                <h2 style="font-size:1em; margin-top:-3px;"><?php echo $studentNumber; ?></h2>
                                <h3 style="font-size:0.7em; margin-top:-3px; font-weight:400; color:#08ACF0">Https://Classworks.xyz/<?php echo $schoolDetails->domain_name; ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="id-card-holder" style="margin-top:125px; margin-right:300px; float:right">
                        <div class="id-card">
                            <h3 style="font-size:0.8em; margin-top:15px; font-weight:400">Lost but found?</h3>
                            <h2 style="color:#08acf0; font-size:0.9em; margin-left:2px; margin-right:2px"><?php echo $schoolDetails->school_address ?></h2>
                            <hr style="border-color:#08acf0">
                            
                            <img style="height:100%; width:90%; margin-top:-5px;" src="<?php echo $ex3->generate()?>">
                            <hr style="border-color:#08acf0">
                            <div class="photo" style="margin-top:-5px">
                            <h2 style="font-size:0.8em; margin-top:-3px; font-weight:500">Signed: <span style="font-weight:normal; font-style:italic">School Management</span></h2>
                            </div>
                        </div>
                    </div>
                    

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>