<?php include_once ("studentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $studentClass->getSchoolDetails();
include_once ("../../includes/modals/index.php");

if(isset($_GET["class_id"])){
    $allStudent = $studentClass->getallClassStudents($studentClass->getClassName($_GET['class_id']));
}else{
    if(isset($_GET["parent_id"])){
        $allStudent = $studentClass->fetchAllParentStudent($_GET["parent_id"]);  
    }else{
        if(isset($_POST["sortClass"]) || isset($_POST["sortGender"]) || isset($_POST["searchKeyword"])){
            @$sortClass = htmlentities(trim($_POST["sortClass"]));
            @$sortGender = htmlentities(trim($_POST["sortGender"]));
            @$searchKeyword = htmlentities(trim($_POST["searchKeyword"]));
            $allStudent = $studentClass->studentSort($sortClass, $sortGender, $searchKeyword);
        }else{
            $allStudent = $studentClass->fetchAllStudentLimit(); 
        } 
    }  
}
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Students", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#newStudentModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Add a new student</span>
                                    </a>
                                    <a data-target="#newDeleteModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete a student</span>
                                    </a>
                                    <a data-target="#searchStudentsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><?php if(isset($_GET['class_id'])){echo "CURRENT ".strtoupper($studentClass->getClassName($_GET['class_id']))." STUDENT(S)";}else{ if(isset($_GET['parent_id'])){ echo strtoupper($studentClass->split_full_name($studentClass->fetchSingleParent($_GET['parent_id'])->fullname)["salutation"])." ".strtoupper($studentClass->split_full_name($studentClass->fetchSingleParent($_GET['parent_id'])->fullname)["lname"])."'S STUDENT(S)"; }else{echo "CURRENT STUDENT(S)";} } ?></h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Student number</th>
                                                <th>Name</th>
                                                <!-- <th>Gender</th> -->
                                                <th>Class</th>
                                                <th class="text-center">Process Payment</th>
                                                <th class="text-center">Academic records</th>
                                                <th class="text-center">Profile</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allStudent) == 0){
                                                    echo "No student found in record";
                                                }else{
                                                    for($i = 0; $i < count($allStudent); $i++){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $i + 1 ?>.</span>
                                                            </td>
                                                            <td>

                                                                <?php if(!empty($allStudent[$i]->profilePhoto)){ ?>
                                                                <div class="user-with-avatar">
                                                                <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$allStudent[$i]->profilePhoto;?>">
                                                                </div>
                                                                <?php }else{ ?>
                                                                    <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                    class="badge"><?php echo $nameInitials = $studentClass->createAcronym($allStudent[$i]->fullname); ?></a>
                                                                <?php } ?>

                                                            </td>
                                                            <td>
                                                                <span><?php echo $allStudent[$i]->student_number; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo strtoupper($allStudent[$i]->fullname); ?></span>
                                                            </td>
                                                            <!-- <td>
                                                                <span><?php echo ucfirst($allStudent[$i]->gender); ?></span>
                                                            </td> -->
                                                            <td class="text-center">
                                                                <span><?php echo ucfirst($allStudent[$i]->present_class); ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="" data-target="#paymentSwitch" data-toggle="modal">Process Payment</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="../assessment/report_sheet?student_id=<?php echo $allStudent[$i]->student_number ?>&session=<?php echo $schoolDetails->academic_session ?>&term=<?php echo $schoolDetails->academic_term ?>">View assessment</a>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="../students/profile.php?stdn=<?php echo base64_encode($allStudent[$i]->student_number); ?>">View profile</a>
                                                            </td>
                                                        </tr>
                                                <?php    }
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allStudent) > 20){ ?>
                                                <div style="float: right;" class-"element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->
                        </div>
                    </div>
                </div>
            </div> 
                      <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->
               

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>