<?php
    include_once ("studentbtn.php");
    if(!isset($_GET["stdn"])){
        echo "<script>window.location.assign('../authentication')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<?php
$studentNumber = base64_decode($_GET["stdn"]);
$schoolDetails = $studentClass->getSchoolDetails();
$studentDetails = $studentClass->getStudentDetails($studentNumber);
$studentParent = $studentClass->getParentDetails($studentNumber);
$studentPickup = $studentClass->getPickupDetails($studentNumber);
$classes = $studentClass->getClasses();
$getOutstandingFee = $studentClass->getOutstandingFee($studentNumber);
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <?php
                if($_SESSION["role"] == "tutor"){
                    include_once ("../../includes/sidebar2.php");
                }else{
                    include_once ("../../includes/sidebar.php");
                }
            ?>
            <!-------------------- START - Main Menu -------------------->
            <?php //include("../../includes/sidebar.php") ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php");
                if(!in_array("Students", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <br>

            <!--START - Profile-->
<div class="content-i" style="margin-top:-30px;">
<div class="content-box">
<div class="row">
    <div style="margin-left:13px; margin-bottom:20px; margin-top:-8px; float:right" class="element-actions">
    <a style="color:#FFFFFF; margin-top:10px; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="../assessment/report_sheet?student_id=<?php echo $studentNumber ?>&session=<?php echo $schoolDetails->academic_session ?>&term=<?php echo $schoolDetails->academic_term ?>">
        <i class="os-icon os-icon-agenda-1"></i>
        <span style="font-size: .9rem;">View Report Sheet</span>
    </a>
    <a data-target=".bd-example-modal-sm" data-id="<?php echo $studentNumber; ?>" id="delStudent" data-toggle="modal" style="color:#FFFFFF; margin-top:10px; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
        <i class="os-icon os-icon-ui-15"></i>
        <span style="font-size: .9rem;">Delete student</span>
    </a>
    <a data-target="" data-toggle="modal" id="contactParent" data-id="<?php echo $studentNumber; ?>" style="margin-top:10px; color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
        <i class="os-icon os-icon-mail-01"></i>
        <span style="font-size: .9rem;">Contact Parents</span>
    </a>
    <a style="margin-top:10px; color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="idcard.php?student_id=<?php echo base64_encode($studentNumber)?>">
        <i class="os-icon os-icon-cv-2"></i>
        <span style="font-size: .9rem;">Print ID Card</span>
    </a>
    <a data-target="#promoteStudent" data-toggle="modal" style="margin-top:10px; color:#FFFFFF; background-color:#1BB4AD; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="">
        <i class="os-icon os-icon-donut-chart-2"></i>
        <span style="font-size: .9rem;">Promote This Student</span>
    </a>
    </div>
   <div class="col-sm-6">
      <div class="user-profile compact">
      <div class="profile-tile" style="padding-top:20px; padding-left:30px; border-top-right-radius:10px; border-top-left-radius:10px; background-color:#FFFFFF">
            <div class="my_hover_up pt-avatar-w" style="border-radius:0px;">
                <?php if(!empty($studentDetails->profilePhoto)){ ?>
                <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="<?php echo $schoolDetails->pathToPassport."/".$studentDetails->profilePhoto; ?>">
                <?php }else { ?>
                    <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="../../images/male-user-profile-picture.png">            
                <?php } ?>
            </div>
            <div class="profile-tile-meta">
                <ul style="text-transform:capitalize">
                    <li style="color:#8095A0;">Name: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->fullname;?></strong></li>
                    <li style="color:#8095A0">Age: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->age." Years"?></strong></li>
                    <li style="color:#8095A0">Class: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->present_class; ?></strong></li>
                    <li style="color:#8095A0">Gender: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->gender; ?></strong></li>
                    <li style="color:#8095A0">Year of enrollment: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->enrolment_year; ?></strong></li>
                </ul>
            </div>
        </div>

         <div class="up-controls">
            <div class="row">
               <div class="col-sm-6">
                  <div class="value-pair" style="margin-top: 3px;">
                     <div class="label" style="font-weight:600; color:#8095A0">Academics :</div>
                     <div class="value badge badge-pill badge-primary"><?php echo strtoupper($studentClass->getOverallStudentPerformance($studentNumber)) ?></div>
                  </div>
               </div>
               <div class="col-sm-5 text-right">
                   <!-- <a class="btn btn-primary btn-sm" href=""><i class="os-icon os-icon-link-3"></i><span>Add to Friends</span></a> -->
                   <div class="value-pair" style="margin-top: 3px;">
                     <div class="label" style="font-weight:600; color:#8095A0">Attendance :</div>
                     <div class="value badge badge-pill badge-danger">N/A</div>
                  </div>
                </div>
            </div>
         </div>
         <div class="up-contents">
            <div class="m-b">
               <div class="row m-b">
                  <div class="col-sm-6 b-r b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#1BB4AD"><?php echo count($studentClass->getTimeSheet($studentDetails->present_class, "none", "2")) ?></div>
                        <div class="label" style="font-weight:600; color:#8095A0">LESSONS/WEEK</div>
                     </div>
                  </div>
                  <div class="col-sm-6 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#FE650B"><?php echo count($studentClass->getClassSubject($studentDetails->present_class)) ?></div>
                        <div class="label" style="font-weight:600; color:#8095A0">SUBJECTS OFFERED</div>
                     </div>
                  </div>
               </div>
               <div class="padded" style="margin-top:-10px;">

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Overall Attendance</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#1BB4AD">Good</span></div>
                     </div>
                    <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 30%; background-color:#1BB4AD"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Behavioural Acts</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#974A6D">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 65%; background-color:#974A6D"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Mental Wellness</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#FE650B">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 90%; background-color:#FE650B"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Physical Health</div>
                        <div class="bar-label-right"><span class="info" style="color:#08ACF0">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 30%; background-color:#08ACF0"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Punctuality</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#FB5574">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 10%; background-color:#FB5574"></div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="element-wrapper" style="margin-top: 1rem;">
        <div class="element-box">

            <!-- SUBJECTS -->
            <form>
            <div class="form-group" style="margin-top:0px;">
            <legend style="color:#FB5574; margin-top:30px;"><span>SUBJECT(S)</span></legend>
            <select class="form-control select2" multiple="true">
                <?php
                    for($i = 0; $i < count($studentClass->getClassSubject($studentDetails->present_class)); $i++){ ?>
                        <option selected="true" value=""><?php echo ucwords($studentClass->getClassSubject($studentDetails->present_class)[$i]->subjectName); ?></option>
                <?php    }
                ?>
            </select>
            <!-- <div class="form-buttons-w" style="margin-top:15px; "><button class="btn btn-primary" type="submit"> Update Subjects</button></div> -->
            </form>            
            </div>

            <!-- TIMESHEETS -->
            <legend style="margin-top:30px;"><span>TIME SHEET(S)</span></legend>
            <form>
            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#1BB4AD" for="">Monday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day001", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day001", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day001", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day001", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day001", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day001", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day001", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day001", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#974A6D" for="">Tuesday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day002", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day002", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day002", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day002", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day002", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day002", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day002", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day002", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#FE650B" for="">Wednesday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day003", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day003", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day003", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day003", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day003", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day003", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day003", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day003", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#08ACF0" for="">Thursday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day004", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day004", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day004", "1") == "lunch" ||  $studentClass->getTimeSheet($studentDetails->present_class, "day004", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day004", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day004", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day004", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day004", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#1BB4AD" for="">Friday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day005", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day005", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day005", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day005", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day005", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day005", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day005", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day005", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#974A6D" for="">Saturday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day006", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day006", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day006", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day006", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day006", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day006", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day006", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day006", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>


            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#F4B510" for="">Sunday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day007", "1")); $i++){
                    if($studentClass->getTimeSheet($studentDetails->present_class, "day007", "1") == "sports" || $studentClass->getTimeSheet($studentDetails->present_class, "day007", "1") == "lunch" || $studentClass->getTimeSheet($studentDetails->present_class, "day007", "1") == "break" || $studentClass->getTimeSheet($studentDetails->present_class, "day007", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day007", "1")[$i]->timeName)." (".ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day007", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day007", "1")[$i]->timeName); ?></option>                        
            <?php }} ?>
            </select>
            </div>
            <!-- <div class="form-buttons-w" style="margin-top:15px; "><button class="btn btn-primary" type="submit"> Update Timesheet</button></div> -->
        </form>            
        </div>

        <!-- PARENTS/GUARDIANS -->
        <div class="element-wrapper" style="margin-top: 1rem;">
        <div class="element-box">
        
            <div class="form-group row" style="margin-top:0px;">
                <legend style="margin-top:0px;"><span>PARENTS/GAURDIANS</span></legend>            
            </div>
            <div class="row" style="margin-top:-15px; margin-bottom:20px;">
            <?php
                if($studentParent === false){
                    echo "No parent added yet";
                }else{
                    foreach ($studentParent as $key => $value) {
                        # code...
                        if($value->val === false && strtolower(substr($value->relationship, 0, 1)) == "f"){ ?>
                            <div class="profile-tile col-sm-6">
                                <a class="profile-tile-box" href="#" data-target="#newParentModal" data-toggle="modal">
                                    <div class="pt-avatar-w">
                                        <img alt="" style="border:2px solid #08ACF0; border-radius:100px" src="../../images/male-user-profile-picture.png">
                                    </div>
                                    <h6>Not added yet</h6>
                                    <div class="pt-user-name" style="text-transform: capitalize; margin-top:-12px; color:#8095A0; border-top: 0px solid #8095A0;">Father</div>
                                </a>
                            </div>
                       <?php }
                        elseif ($value->val === false && strtolower(substr($value->relationship, 0, 1)) == "m") { ?>
                            <div class="profile-tile col-sm-6">
                                <a class="profile-tile-box" href="#" data-target="#newParentModal" data-toggle="modal">
                                    <div class="pt-avatar-w">
                                        <img alt="" style="border:2px solid #FB5574; border-radius:100px" src="../../images/male-user-profile-picture.png">
                                    </div>
                                    <h6>Not added yet</h6>
                                    <div class="pt-user-name" style="text-transform: capitalize; margin-top:-12px; color:#8095A0; border-top: 0px solid #8095A0;">Mother</div>
                                </a>
                            </div>
                      <?php  }
                      elseif ($value->val === true && strtolower(substr($value->relationship, 0, 1)) == "f") { ?>
                            <div class="profile-tile col-sm-6">
                                    <a class="profile-tile-box" href="../parents/profile?parent_id=<?php echo base64_encode($value->parentNumber); ?>">
                                        <div class="pt-avatar-w">
                                        <?php
                                            if(empty($value->profilePhoto)){ ?>
                                                <img alt="" style="border:2px solid #FB5574; border-radius:100px" src="../../images/male-user-profile-picture.png">
                                            <?php }else{ ?>
                                                <img alt="" style="border:2px solid #08ACF0; border-radius:100px" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto?>">
                                            <?php }
                                        ?>
                                        </div>
                                        <h6><?php echo ucwords($value->fullname)?></h6>
                                        <div class="pt-user-name" style="text-transform: capitalize; margin-top:-12px; color:#8095A0; border-top: 0px solid #8095A0;">Father</div>
                                    </a>
                            </div>
                      <?php }
                      elseif ($value->val === true && strtolower(substr($value->relationship, 0, 1)) == "m") { ?>
                            <div class="profile-tile col-sm-6">
                                    <a class="profile-tile-box" href="../parents/profile?parent_id=<?php echo base64_encode($value->parentNumber); ?>">
                                        <div class="pt-avatar-w">
                                            <?php
                                                if(empty($value->profilePhoto)){ ?>
                                                    <img alt="" style="border:2px solid #FB5574; border-radius:100px" src="../../images/male-user-profile-picture.png">
                                                <?php }else{ ?>
                                                    <img alt="" style="border:2px solid #08ACF0; border-radius:100px" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto?>">
                                                <?php }
                                            ?>
                                        </div>
                                        <h6><?php echo ucwords($value->fullname)?></h6>
                                        <div class="pt-user-name" style="text-transform: capitalize; margin-top:-12px; color:#8095A0; border-top: 0px solid #8095A0;">Mother</div>
                                    </a>
                            </div>
                      <?php }
                    }
                }
            ?>
            </div>

            <div class="form-group row" style="margin-top:0px;">
                <legend style="margin-top:0px;"><span>VERIFIED PICKUPS</span></legend>            
            </div>
            <div class="row" style="margin-top:-15px; margin-bottom:20px;">
            <?php
                if($studentPickup === false){
                    echo "No pickup person added yet";
                }else{
                    foreach ($studentPickup as $key => $value) { ?>
                        <!-- # code...  -->
                            <div class="profile-tile col-sm-6">
                                    <a class="profile-tile-box" href="#">
                                        <div class="pt-avatar-w">
                                            <img alt="" style="border:1px solid #08ACF0; border-radius:100px; height:70px;" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto?>">
                                        </div>
                                        <h6><?php echo $value->fullname?></h6>
                                        <div class="pt-user-name" style="text-transform: capitalize; margin-top:-12px; color:#8095A0; border-top: 0px solid #8095A0;"><?php echo $value->relationship?></div>
                                    </a>
                            </div>
                      <?php 
                    }
                }
            ?>
            </div>

            <div class="form-group row" style="margin-top:0px;">
                <legend style="margin-top:30px;"><span>OUTSTANDING PAYMENT(S)</span></legend>
            </div>
            <div class="form-group" style="margin-top:-10px;">
                <?php
                    if(count($getOutstandingFee) > 0){ 
                        foreach($getOutstandingFee as $key => $value){ ?>
                            <h3 for="">&#8358;<?php echo number_format($value->amount, 2)?></h3>
                            <h6 style="font-style:italic; font-weight:normal"><?php echo ucwords($value->feeName); ?></h6>
                        <?php } ?>
                        <button data-id="<?php echo $studentNumber; ?>" class="btn btn-primary" id="notifyOutstandingFee" style="border-color:#FB5574; background-color: #FB5574; margin-top:10px;" type="submit"> Notify Parents</button>
                     <?php }else{
                         echo "No Outstanding Payment";
                     }
                ?>
            </div>
            <!-- REFUND AMOUNT -->
            <div class="form-group row" style="margin-top:0px;">
                <legend style="margin-top:30px;"><span>REFUND AMOUNT</span></legend>
            </div>
            <div class="form-group" style="margin-top:-10px;">
                <h3 for="">&#8358;<?php echo number_format($studentDetails->accountBalance, 2); ?></h3>
            </div>
        </div>
        </div>


      </div>
   </div>
   
   <div class="col-sm-6">
      <div class="element-wrapper">
    
      <!-- STUDENT BIO DATA -->

      <div class="element-box" >
            <form id="formValidate" method="POST" action="" enctype="multipart/form-data">
               <div class="element-info">
                  <div class="element-info-with-icon">
                     <div class="element-info-icon">
                        <div class="os-icon os-icon-user-male-circle"></div>
                     </div>
                     <div class="element-info-text">
                        <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Student Bio Data</h5>
                     </div>
                  </div>
               </div>
               <div class="row" style="margin-top:-10px; margin-bottom:20px;">
                <div class="pt-avatar-w" style="border-radius:0px;">
                    <img alt="" id="studentuploadPreview2" style="margin-left:20px; border-radius:5px; border: 1px solid #08ACF0; height:100px; width:90px" src="../../images/male-user-profile-picture.png">
                </div>
               <div class="form-group col-sm-7" style="margin-left:20px;">
                  <label for="studentupload-photo2">Change passport</label>
                  <label style="cursor:pointer" class="btn btn-primary" for="studentupload-photo2"> Upload Image</label>
                  <input id="studentupload-photo2" name="profilePhoto" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1; " class="form-control" type="file">
                  <input type="hidden" name="profileChecker" value="<?php echo $studentDetails->profilePhoto; ?>" id="">
               </div>
               </div>
               <div class="form-group">
                  <label for=""> Student's Registration No.</label><input name="studentNumber" class="form-control" readonly value="<?php echo $studentNumber; ?>" type="text">
               </div>
               <div class="form-group">
                  <label for=""> Student's Full name</label><input name="fullname" class="form-control" value="<?php echo ucwords($studentDetails->fullname); ?>" type="text">
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                     <label for=""> Nationality</label>
                        <Select name="studentCountry" class="form-control">
                            <option value="nigeria">Nigeria</option>
                            <option value="algeria">Algeria</option>
                            <option value="angola">Angola</option>
                            <option value="benin">Benin</option>
                            <option value="botswana">Botswana</option>
                            <option value="burkina Faso">Burkina Faso</option>
                            <option value="burundi">Burundi</option>
                            <option value="cameroon">Cameroon</option>
                            <option value="cape-verde">Cape Verde</option>
                            <option value="central-african-republic">Central African Republic</option>
                            <option value="chad">Chad</option>
                            <option value="comoros">Comoros</option>
                            <option value="congo-brazzaville">Congo - Brazzaville</option>
                            <option value="congo-kinshasa">Congo - Kinshasa</option>
                            <option value="ivory-coast">Côte d’Ivoire</option>
                            <option value="djibouti">Djibouti</option>
                            <option value="egypt">Egypt</option>
                            <option value="equatorial-guinea">Equatorial Guinea</option>
                            <option value="eritrea">Eritrea</option>
                            <option value="ethiopia">Ethiopia</option>
                            <option value="gabon">Gabon</option>
                            <option value="gambia">Gambia</option>
                            <option value="ghana">Ghana</option>
                            <option value="guinea">Guinea</option>
                            <option value="guinea-bissau">Guinea-Bissau</option>
                            <option value="kenya">Kenya</option>
                            <option value="lesotho">Lesotho</option>
                            <option value="liberia">Liberia</option>
                            <option value="libya">Libya</option>
                            <option value="madagascar">Madagascar</option>
                            <option value="malawi">Malawi</option>
                            <option value="mali">Mali</option>
                            <option value="mauritania">Mauritania</option>
                            <option value="mauritius">Mauritius</option>
                            <option value="mayotte">Mayotte</option>
                            <option value="morocco">Morocco</option>
                            <option value="mozambique">Mozambique</option>
                            <option value="namibia">Namibia</option>
                            <option value="niger">Niger</option>
                            <option value="nigeria">Nigeria</option>
                            <option value="rwanda">Rwanda</option>
                            <option value="reunion">Réunion</option>
                            <option value="saint-helena">Saint Helena</option>
                            <option value="senegal">Senegal</option>
                            <option value="seychelles">Seychelles</option>
                            <option value="sierra-leone">Sierra Leone</option>
                            <option value="somalia">Somalia</option>
                            <option value="south-africa">South Africa</option>
                            <option value="sudan">Sudan</option>
                            <option value="south-sudan">Sudan</option>
                            <option value="swaziland">Swaziland</option>
                            <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                            <option value="tanzania">Tanzania</option>
                            <option value="togo">Togo</option>
                            <option value="tunisia">Tunisia</option>
                            <option value="uganda">Uganda</option>
                            <option value="western-sahara">Western Sahara</option>
                            <option value="zambia">Zambia</option>
                            <option value="zimbabwe">Zimbabwe</option>
                            <option value="Others">Others</option>
                        </Select>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province</label><input name="studentState" class="form-control" value="<?php echo ucfirst($studentDetails->state); ?>" type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region</label><input name="studentCity" class="form-control" value="<?php echo ucfirst($studentDetails->city); ?>" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                        <div class="form-group">
                            <label for=""> Date of Birth <span style="color:#F91F14">*</span></label>
                            <div class="date-input">
                                <input required name="studentDob" class="date_of_birth form-control" value="<?php echo $studentDetails->dob; ?>" placeholder="Date of birth" type="text">
                            </div>
                        </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Gender</label>
                        <Select id="studentGender" name="studentGender" class="form-control">
                        <option <?php echo $studentDetails->gender == "male" ? "selected" : "" ?> value="Male">Male</option>
                        <option <?php echo $studentDetails->gender == "female" ? "selected" : "" ?> value="Female">Female</option>
                        </Select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                        <label for=""> Blood Group</label>
                        <Select name="studentBloodGroup" class="form-control">
                        <option <?php echo $studentDetails->blood_group == "a" ? "selected" : "" ?> value="A">A</option>
                        <option <?php echo $studentDetails->blood_group == "b" ? "selected" : "" ?> value="B">B</option>
                        <option <?php echo $studentDetails->blood_group == "c" ? "selected" : "" ?> value="AB">AB</option>
                        <option <?php echo $studentDetails->blood_group == "o" ? "selected" : "" ?> value="O">O</option>
                        <option <?php echo $studentDetails->blood_group == "not known" ? "selected" : "" ?> value="Not Known">Not Known</option>
                        </Select>
                    </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for=""> Father's Phone</label><input name="fatherPhone" class="form-control" value="<?php echo $studentDetails->fatherPhone; ?>" type="text">
               </div>
               <div class="form-group">
                  <label for=""> Mother's Phone</label><input name="motherPhone" class="form-control" value="<?php echo $studentDetails->motherPhone; ?>" type="text">
               </div>
               <div class="form-group">
                  <label for=""> Languages Proficient</label>
                  <select class="form-control select2" multiple="true" name="languages[]">
                     <option <?php echo count($studentDetails->languages) > 0  && in_array("English", $studentDetails->languages) ? "selected" : "";?> value="English">English</option>
                     <option <?php echo count($studentDetails->languages) > 0  && in_array("French", $studentDetails->languages) ? "selected" : "";?> value="French">French</option>
                     <option <?php echo count($studentDetails->languages) > 0  && in_array("Spanish", $studentDetails->languages) ? "selected" : "";?> value="Spanish">Spanish</option>
                     <option <?php echo count($studentDetails->languages) > 0  && in_array("Chinese", $studentDetails->languages) ? "selected" : "";?> value="Chinese">Chinese</option>
                     <option <?php echo count($studentDetails->languages) > 0  && in_array("Swahili", $studentDetails->languages) ? "selected" : "";?> value="Swahili">Swahili</option>
                  </select>
               </div>
               <div class="form-group">
                <label for="">Student's Class</label>
                <select class="form-control" name="studentClass">
                    <?php
                        for($i = 0; $i < count($classes); $i++){ ?>
                            <option <?php echo $studentDetails->present_class == $classes[$i]->class_name ? "selected" : ""; ?> value="<?php echo $classes[$i]->class_name;?>"><?php echo $classes[$i]->class_name;?></option>
                    <?php    }
                    ?>
                </select>
                </div>
               <div class="form-group"><label> Student Description</label><textarea name="studentDesc" class="form-control" rows="3" placeholder="Fair in complexion, smart, the funniest in class."><?php echo $studentDetails->student_desc?></textarea></div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I agree to terms and conditions</label></div>
                <div class="form-buttons-w" style="margin-top:15px; "><input name="bioData" type="submit" class="btn btn-primary" value=" Update Bio Data"></div>
            </form>
         </div>

         
        <!-- SETTINGS -->
        <div class="element-box" style="margin-top: 1rem;">
            <form id="formValidate" method="POST" action="#">
            <div class="element-info">
                <div class="element-info-with-icon">
                    <div class="element-info-icon">
                    <div class="os-icon os-icon-ui-46"></div>
                    </div>
                    <div class="element-info-text">
                    <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Student Settings</h5>
                    </div>
                </div>
            </div>
            <input type="hidden" name="studentNumber" value="<?php echo $studentNumber; ?>">
            <div class="form-group" style="margin-top:-10px;">
            <label for="">Student Pickup Verification? <div class="value badge badge-pill badge-<?php echo $studentDetails->pickup_verification == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $studentDetails->pickup_verification == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="pickupVerification">
                <option value="1" <?php echo $studentDetails->pickup_verification == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $studentDetails->pickup_verification == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-group">
            <label for="">School Bus Pickup? <div class="value badge badge-pill badge-<?php echo $studentDetails->bus_pickup == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $studentDetails->bus_pickup == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="busPickup">
                <option value="1" <?php echo $studentDetails->bus_pickup == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $studentDetails->bus_pickup == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-group">
            <label for="">School Bus Dropoff? <div class="value badge badge-pill badge-<?php echo $studentDetails->bus_dropoff == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $studentDetails->bus_dropoff == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="busDropoff">
                <option value="1" <?php echo $studentDetails->bus_dropoff == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $studentDetails->bus_dropoff == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-group">
            <label for="">Student portal access? <div class="value badge badge-pill badge-<?php echo $studentDetails->student_portal == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $studentDetails->student_portal == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="studentPortalAccess">
                <option value="1" <?php echo $studentDetails->student_portal == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $studentDetails->student_portal == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-group">
            <label for="">Parent portal access? <div class="value badge badge-pill badge-<?php echo $studentDetails->parent_portal == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $studentDetails->parent_portal == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="parentPortalAccess">
                <option value="1" <?php echo $studentDetails->parent_portal == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $studentDetails->parent_portal == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-buttons-w" style="margin-top:15px; "><input type="submit" name="saveSettings" value=" Update Settings" class="btn btn-primary"></div>
            </form>
            </div>

      </div>
   </div>
</div>


</div><!--------------------
   START - Sidebar
   -------------------->

<!-- LESSON PLANS -->

<div class="element-box col-sm-3" style="margin-top:2rem; margin-right:2rem; margin-left:-0.5rem; height:auto ">

<div class="element-info">
    <div class="element-info-with-icon row">
        <div class="element-info-icon">
        <i style="font-size: 1.3rem; color: #08ACF0;" class="os-icon os-icon-pencil-1"></i>
        </div>
        <div class="element-info-text">
        <h5 class="element-inner-header" style="vertical-align: middle; margin-top: 4px; margin-left: 15px; margin-bottom:20px;">Lesson Plan(s)</h5>
        </div>
    </div>
</div>
<div class="form-buttons-w" style="margin-top:5px; margin-bottom: 10px;"></div>

<div class="timed-activities compact">

    <div class="timed-activity">
        <div class="ta-date" ><span style="color:#1BB4AD">Monday <i style="font-size:0.8rem; color:#8095A0">(<?php echo date('d/m/Y',time()+( 1 - date('w'))*24*3600) ?>)</i></span></div>
        <div class="ta-record-w">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day001", "2")); $i++){ ?>
                    <div class="ta-record">
                    <div class="ta-timestamp"><strong><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day001", "2")[$i]->timeName); ?></strong></div>
                    <div class="ta-activity"><Strong><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day001", "2")[$i]->subjectName); ?></Strong><br><i><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day001", "2")[$i]->tutorName); ?></i></div>
                    </div>
            <?php } ?>
        </div>
    </div>

    <div class="timed-activity">
        <div class="ta-date" ><span style="color:#974A6D">Tuesday <i style="font-size:0.8rem; color:#8095A0">(<?php echo date('d/m/Y',time()+( 2 - date('w'))*24*3600) ?>)</i></span></div>
        <div class="ta-record-w">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day002", "2")); $i++){ ?>
                    <div class="ta-record">
                    <div class="ta-timestamp"><strong><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day002", "2")[$i]->timeName); ?></strong></div>
                    <div class="ta-activity"><Strong><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day002", "2")[$i]->subjectName); ?></Strong><br><i><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day002", "2")[$i]->tutorName); ?></i></div>
                    </div>
            <?php } ?>
        </div>
    </div>

    <div class="timed-activity">
        <div class="ta-date" ><span style="color:#FE650B">Wednesday <i style="font-size:0.8rem; color:#8095A0">(<?php echo date('d/m/Y',time()+( 3 - date('w'))*24*3600) ?>)</i></span></div>
        <div class="ta-record-w">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day003", "2")); $i++){ ?>
                    <div class="ta-record">
                    <div class="ta-timestamp"><strong><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day003", "2")[$i]->timeName); ?></strong></div>
                    <div class="ta-activity"><Strong><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day003", "2")[$i]->subjectName); ?></Strong><br><i><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day003", "2")[$i]->tutorName); ?></i></div>
                    </div>
            <?php } ?>
        </div>
    </div>

    <div class="timed-activity">
        <div class="ta-date" ><span style="color:#08ACF0">Thursday <i style="font-size:0.8rem; color:#8095A0">(<?php echo date('d/m/Y',time()+( 4 - date('w'))*24*3600) ?>)</i></span></div>
        <div class="ta-record-w">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day004", "2")); $i++){ ?>
                    <div class="ta-record">
                    <div class="ta-timestamp"><strong><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day004", "2")[$i]->timeName); ?></strong></div>
                    <div class="ta-activity"><Strong><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day004", "2")[$i]->subjectName); ?></Strong><br><i><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day004", "2")[$i]->tutorName); ?></i></div>
                    </div>
            <?php } ?>
        </div>
    </div>

    <div class="timed-activity">
        <div class="ta-date" ><span style="color:#1BB4AD">Friday <i style="font-size:0.8rem; color:#8095A0">(<?php echo date('d/m/Y',time()+( 5 - date('w'))*24*3600) ?>)</i></span></div>
        <div class="ta-record-w">
            <?php
                for($i = 0; $i < count($studentClass->getTimeSheet($studentDetails->present_class, "day005", "2")); $i++){ ?>
                    <div class="ta-record">
                    <div class="ta-timestamp"><strong><?php echo strtoupper($studentClass->getTimeSheet($studentDetails->present_class, "day005", "2")[$i]->timeName); ?></strong></div>
                    <div class="ta-activity"><Strong><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day005", "2")[$i]->subjectName); ?></Strong><br><i><?php echo ucwords($studentClass->getTimeSheet($studentDetails->present_class, "day005", "2")[$i]->tutorName); ?></i></div>
                    </div>
            <?php } ?>
        </div>
    </div>

</div>
</div>
<!--------------------
   END - Sidebar
   -------------------->


            <!--END - Profile-->


<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->             

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>