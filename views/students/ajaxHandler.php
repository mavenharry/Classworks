<?php
include ("studentClass.php");
$studentClass = new studentClass();


if(isset($_GET["student"])){
    $student = htmlentities(trim($_GET["student"]));
    $reply = $studentClass->getParentDetails($student);
    if($reply === false){ ?>
        <option value="">No parent added yet</option>
    <?php }else{
        if(count($reply) == 1){
            if($reply[0]->val === false){ ?>
                <option value="">No parent added yet</option>
            <?php }else{ ?>
                <option selected value="<?php echo $reply[0]->phone?>"><?php echo $reply[0]->fullname." - ".$reply[0]->phone?></option>
            <?php }
        }else{
            foreach ($reply as $key => $value) {
                if($value->val === true){ ?>
                    <option selected value="<?php echo $value->phone?>"><?php echo $value->fullname." - ".$value->phone?></option>
                <?php }
             }
        }
    }
}

if(isset($_GET["className"])){
    $className = htmlentities(trim($_GET["className"]));
    $classStudents = $studentClass->getallClassStudents($className);

    if(count($classStudents) == 0){ ?>
        <option value="">No student in this class</option>
    <?php }else{ ?>
        <option value="">Select Student</option>
        <?php foreach($classStudents as $key => $value){ ?>
            <option value="<?php echo base64_encode($value->student_number)?>"><?php echo ucwords($value->fullname); ?></option>
        <?php }
    }
}

if(isset($_GET["moveStudentId"])){
    $studentId = htmlentities(trim($_GET["moveStudentId"]));
    $reply = $studentClass->moveToAlumni($studentId);
    echo $reply;
}




?>