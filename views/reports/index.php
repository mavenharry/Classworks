<?php include_once ("reportbtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Home</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/.css" rel="stylesheet">
</head>
<?php
$schoolDetails = $reportClass->getSchoolDetails();
$recordDetails = $reportClass->getReportDetails();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
         
         <div class="layout-w">
            
               <!-- BEGIN MAIN MENU -->
               <?php include("../../includes/sidebar.php");
                if(!in_array("Reports", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
               ?>
               <!-- END - Main Menu -->

            <div class="content-w">

               <!-- START - Top Bar -->
               <?php include("../../includes/topbar.php") ?>
               <!-- END - Top Bar -->
                  
               <div class="content-panel-toggler"><i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span></div>
               <div class="content-i">
                  <div class="content-box" style="margin-top:-15px;">
                     <div class="row">
                        <div class="col-sm-8">
                           <div class="element-wrapper">
                              <div class="element-box">
                                 <h5 class="form-header">Student Population Analysis</h5>
                                 <div class="form-desc">This bar chart gives you an overall insight on school student population and gender margins.</a></div>
                                 <div class="el-chart-w">
                                    <canvas height="98" id="barChart2" width="300"></canvas>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="element-wrapper">
                              <div class="element-box el-tablo">
                                 <div class="label">Overall Population</div>
                                 <div class="value" id="totalPopulation"><?php echo $recordDetails->male + $recordDetails->female;?></div>
                                 <div class="trending trending-up" style="position:absolute; right:40px; margin-top:-15px;"><span>12%</span><i class="os-icon os-icon-arrow-up2"></i></div>
                              </div>
                              <div class="element-box el-tablo">
                                 <div class="label">Boys Population</div>
                                 <div class="value" id="boysPopulation"><?php echo $recordDetails->male ?></div>
                                 <div class="trending trending-down-basic" style="position:absolute; right:40px; margin-top:-15px;"><span style="margin-right:10px;">9%</span><i class="os-icon os-icon-arrow-2-up"></i></div>
                              </div>
                              <div class="element-box el-tablo">
                                 <div class="label">Girls Population</div>
                                 <div class="value" id="girlsPopulation"><?php echo $recordDetails->female;?></div>
                                 <div class="trending trending-down-basic" style="position:absolute; right:40px; margin-top:-15px;"><span style="margin-right:10px;">12%</span><i class="os-icon os-icon-arrow-2-down"></i></div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="element-wrapper">
                        <div class="element-box" style="margin-top:-30px;">
                           <div class="os-tabs-w">
                              <div class="os-tabs-controls">
                                 <ul class="nav nav-tabs smaller">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview">Class Capacity Analysis</a></li>
                                 </ul>
                                 <ul class="nav nav-pills smaller">
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Today</a></li>
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">Last Week</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Last Month</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Last Term</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Last Session</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#">Last Year</a></li>
                                 </ul>
                              </div>
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab_overview">
                                    <div class="el-tablo">
                                       <div class="label">Classes Intake Graph</div>
                                       <div class="value"><?php echo $recordDetails->totalClassNumber; ?> Classes</div>
                                    </div>
                                    <div class="el-chart-w">
                                       <canvas height="150px" id="lineChart" width="600px"></canvas>
                                    </div>
                                 </div>
                                 <div class="tab-pane" id="tab_sales"></div>
                                 <div class="tab-pane" id="tab_conversion"></div>
                              </div>
                           </div>
                        </div>
                     </div>

                     
                      <!-- Section 3 -->
                      <div class="element-wrapper" style="margin-top:-30px">
                      <div class="element-content">
                          <div class="tablo-with-chart">
                            <div class="row">
                                <div class="col-sm-5 col-xxl-4">
                                  <div class="tablos">
                                      <div class="row mb-xl-2 mb-xxl-3">
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" href="apps_support_index.html">
                                              <div class="value">&#x20A6;<?php echo number_format($recordDetails->todayPayment);?></div>
                                              <div class="label">Today</div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" href="apps_support_index.html">
                                              <div class="value">&#x20A6;<?php echo number_format($recordDetails->thisWeek); ?></div>
                                              <div class="label">This Week</div>
                                            </a>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" href="apps_support_index.html">
                                              <div class="value">&#x20A6;<?php echo number_format($recordDetails->thisMonth); ?></div>
                                              <div class="label">This Month</div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="element-box el-tablo centered trend-in-corner padded bold-label" href="apps_support_index.html">
                                              <div class="value">&#x20A6;<?php echo number_format($recordDetails->thisTerm); ?></div>
                                              <div class="label">This Term</div>
                                            </a>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                                <div class="col-sm-7 col-xxl-8">
                                  <!--START - Chart Box-->
                                  <div class="element-box pl-xxl-5 pr-xxl-5">
                                      <div class="el-tablo bigger highlight bold-label">
                                        <div class="value">&#x20A6;0.00</div>
                                        <div class="label">Total Paid Fees</div>
                                      </div>
                                      <div class="el-chart-w">
                                        <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                            <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                              <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                              <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                            </div>
                                        </div>
                                        <canvas height="322" id="lineChart2" width="1138" class="chartjs-render-monitor" style="display: block; width: 569px; height: 161px;"></canvas>
                                      </div>
                                  </div>
                                  <!--END - Chart Box-->
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>

                     
                  </div>
                  
               </div>
            </div>
         </div>
         <div class="display-type"></div>
      </div>
   
    <!-------------------- START - MODAL -------------------->
    <?php include("../../includes/modals/index.php"); ?>
    <!-------------------- END - MODAL -------------------->

    <!-------------------- START - Top Bar -------------------->
    <?php include("../../includes/support.php") ?>
    <!-------------------- END - Top Bar -------------------->

    <?php include_once ("../../includes/scripts.php"); ?>

  </body>
</html>
