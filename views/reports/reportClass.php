<?php
    session_start();
    include_once ("../../".$_SESSION['folderName']."/config.php");

    class reportClass extends logic {

        function getReportDetails(){
            $term = $this->getSchoolDetails()->academic_term;
            $gender1 = "male";
            $gender2 = "female";
            $stmt = $this->uconn->prepare("SELECT SUM(students.gender = ?) as male, (SELECT COUNT(id) FROM classes) AS totalClassNumber, SUM(students.gender = ?) as female from students");
            $stmt->bind_param("ss", $gender1, $gender2);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $obj = new stdClass();
            if(count($stmt_result) > 0){
                $row = array_shift($stmt_result);
                $obj->male = $row["male"];
                $obj->female = $row["female"];
                $obj->totalClassNumber = $row["totalClassNumber"];
            }
            $stmt = $this->uconn->prepare("SELECT classes.class_name, classes.class_code, (SELECT COUNT(id) FROM students where students.present_class = classes.class_name) AS allStudent FROM classes");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $classArray = array();
            while($row = array_shift($stmt_result)){
                $objs = new stdClass();
                $objs->className = $row["class_name"];
                $objs->classCapacity = $row["allStudent"];
                $classArray[] = $objs;
            }
            $obj->classArray = $classArray;
            $stmt = $this->uconn->prepare("SELECT (SELECT SUM(amount) FROM payed_student WHERE payDate > DATE_SUB(NOW(), INTERVAL 1 DAY)) AS today, (SELECT SUM(amount) FROM payed_student WHERE payDate > DATE_SUB(NOW(), INTERVAL 1 WEEK)) as thisWeek, (SELECT SUM(amount) FROM payed_student WHERE payDate > DATE_SUB(NOW(), INTERVAL 1 MONTH)) AS thisMonth, (SELECT SUM(amount) FROM payed_student WHERE term = ?) AS thisTerm FROM payed_student");
            $stmt->bind_param("s", $term);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $row = array_shift($stmt_result);
            $obj->todayPayment = $row["today"];
            $obj->thisWeek = $row["thisWeek"];
            $obj->thisMonth = $row["thisMonth"];
            $obj->thisTerm = $row["thisTerm"];
            return $obj;
        }

    }
    
?>