<?php 
  $templates = $settingsClass->getTemplates();
?>
<div class="col-lg-12">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
SCHOOL PORTAL WEB PAGE DESIGN
</h5>
<div class="form-desc" style="margin-bottom:-30px;">
Customize the looks and feel of your portal web page based on your very own preference(s). 
</div>
<!-- PORTAL -->
  <form method="POST" action="">

    <div class="table-responsive">
      
      <div class="row" style="margin-top:20px;">
        <?php
          for($i = 0; $i < count($templates); $i++){
            if($i%2 == 0){ ?>
                <div class='col-lg-4' style="margin-right:60px; margin-top:30px; cursor:pointer">
                  <img data-id="<?php echo $templates[$i]->id; ?>"  style="border-radius:5px; width:420px; height:260px;" class="img-responsive webpage" alt="" src="../../templateImages/<?php echo $templates[$i]->templateName?>" />
                  <img alt="" id="<?php echo explode(".",$templates[$i]->templateName)[0] ?>" class="webpageMarker" style="z-index:200000; position:absolute; top:70px; left:170px; width:120px; height:120px;" src="../../images/verified_white.png">
                </div>
            <?php }else{ ?>
              <div class='col-lg-4' style="margin-left:60px; margin-top: 30px; cursor:pointer">
                <img data-id="<?php echo $templates[$i]->id; ?>"  style="border-radius:5px; width:420px; height:260px;" class="img-responsive webpage" alt="" src="../../templateImages/<?php echo $templates[$i]->templateName?>" />
                <img alt="" id="<?php echo explode(".",$templates[$i]->templateName)[0] ?>" class="webpageMarker" style="z-index:200000; position:absolute; top:105px; left:115px; width:120px; height:120px;" src="../../images/verified_white.png">
              </div>
          <?php  } 
          }
        ?>
      </div>
    </div>
    <input type="hidden" name="templateId" id="templateId">
    <div class="form-buttons-w">
      <input type="submit" name="saveTemplate" class="btn btn-primary" value=" Save portal we page settings">
    </div>
  </form>
</div>
</div>