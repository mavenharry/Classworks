<?php
$modules = $settingsClass->allModules();
?>
<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
SCHOOL OPERATIONS SETTINGS
</h5>
<div class="form-desc">
Customize the structure of your school. Configure the system to fit the operation mode of your school easily. 
</div>
<!-- PORTAL -->
<div class="table-responsive">

        <form method="POST" action="#" enctype="multipart/form-data">
        <fieldset class="form-group">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> School Logo</label>
              <div class="col-sm-8">
                <input style="" name="schoolLogo" accept="image/*" class="form-control" placeholder="School Logo" type="file">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> School Category</label>
              <div class="col-sm-8">
                <select class="form-control select2" name="category[]" style="width:100%" multiple="true">
                <option <?php echo in_array("Creche", $schoolDetails->category) ? "selected": "";?> value="Creche">Creche</option>
                <option <?php echo in_array("Kindergarten", $schoolDetails->category) ? "selected": "";?> value="Kindergarten">Kindergarten</option>
                <option <?php echo in_array("Nursery", $schoolDetails->category) ? "selected": "";?> value="Nursery">Nursery</option>
                <option <?php echo in_array("Primary", $schoolDetails->category) ? "selected": "";?> value="Primary">Primary</option>
                <option <?php echo in_array("Secondary", $schoolDetails->category) ? "selected": "";?> value="Secondary">Secondary</option>
                <option <?php echo in_array("Polytechnic", $schoolDetails->category) ? "selected": "";?> value="Polytechnic">Polytechnic</option>
                <option <?php echo in_array("University", $schoolDetails->category) ? "selected": "";?> value="University">University</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> School Name</label>
              <div class="col-sm-8">
                <input class="form-control" value="<?php echo $schoolDetails->school_name; ?>" name="schoolName" placeholder="Georgina International School" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> School Address</label>
              <div class="col-sm-8">
                <input class="form-control" value="<?php echo $schoolDetails->school_address; ?>" name="schoolAddress" placeholder="No 10 off ada road" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Email Address</label>
              <div class="col-sm-8">
                <input class="form-control" value="<?php echo $schoolDetails->email;?>" name="email" placeholder="Goerginia@gmail.com" type="email">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Telephone</label>
              <div class="col-sm-8">
                <input class="form-control" name="phone" value="<?php echo $schoolDetails->phone; ?>" placeholder="+2341234567890" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Website</label>
              <div class="col-sm-8">
                <input class="form-control" value="<?php echo $schoolDetails->website; ?>" name="website" placeholder="Goerginia.com" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Slogan</label>
              <div class="col-sm-8">
                <input class="form-control" value="<?php echo $schoolDetails->slogan; ?>" name="slogan" placeholder="In God we trust" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Currency</label>
              <div class="col-sm-8">
              <select id="school_currency" class="form-control" name="currency">
                    <option <?php echo $schoolDetails->currency == "&#8358;"? "selected":""?> value="Naira">Naira (&#8358;)</option>
                    <option <?php echo $schoolDetails->currency == "&#036;"? "selected":""?> value="Dollar">Dollar (&#036;)</option>
                    <option <?php echo $schoolDetails->currency == "&#163;"? "selected":""?> value="Pounds">Pounds (&#163;)</option>
                    <option <?php echo $schoolDetails->currency == "&#128;"? "selected":""?> value="Euro">Euro (&#128;)</option>
                    <option <?php echo $schoolDetails->currency == "&#8373;"? "selected":""?> value="Cedi">Cedi (&#8373;)</option>
                    <option <?php echo $schoolDetails->currency == "&#x52;"? "selected":""?> value="Rand">Rand (&#x52;)</option>
                    <option <?php echo $schoolDetails->currency == "ksh"? "selected":""?> value="Shilling">Shilling (Ksh)</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Grading System</label>
              <div class="col-sm-8">
              <select class="form-control" name="grading">
                    <option <?php echo $schoolDetails->grading == "Percentage" ? "selected" : "";?> value="Percentage">Percentage</option>
                    <option <?php echo $schoolDetails->grading == "Grades" ? "selected" : "";?> value="Grades">Grades</option>
                    <option <?php echo $schoolDetails->grading == "Position" ? "selected" : "";?> value="Position">Position</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> School Official Color</label>
              <div class="col-sm-8">
              <input class="form-control"value="<?php echo $schoolDetails->color; ?>" name="color" placeholder="#0318F0" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Curriculum Used</label>
              <div class="col-sm-8">
              <select class="form-control" name="curriculum">
                <option <?php echo $schoolDetails->curriculum == "nigeria" ? "selected" : "";?> value="nigeria">Nigerian</option>
                <option <?php echo $schoolDetails->curriculum == "american" ? "selected" : "";?> value="american">American</option>
                <option <?php echo $schoolDetails->curriculum == "british" ? "selected" : "";?> value="british">British</option>
                <option <?php echo $schoolDetails->curriculum == "custom" ? "selected" : "";?> value="custom">Custom</option>
            </select>
              </div>
            </div>
          </fieldset>
          <div class="form-buttons-w">
            <input type="submit" name="schoolSettings" value=" Save school settings" class="btn btn-primary">
          </div>
        </form>


</div>
</div>
</div>