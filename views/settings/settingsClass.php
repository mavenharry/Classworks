<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class settingsClass extends logic {

    function getTemplates(){
        $stmt = $this->dconn->prepare("SELECT * FROM templates");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $templates = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->id = $row["id"];
            $obj->templateName = $row["templateName"];
            $templates[] = $obj;
        }
        $stmt->close();
        return $templates;
    }

    // FUNCTION TO CLEAR PREVIOUS TEMPLATE
    function rrmdir($dir){
        if(is_dir($dir)){
            $objects = scandir($dir);
            foreach($objects as $object){
                if($object != "." && $object != ".."){
                    if(filetype($dir."/".$object) == "dir"){
                        $this->rrmdir($dir."/".$object);
                    }else{
                        unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    // FUNCTION TO COPY FILE
    function xcopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            $this->xcopy("$source/$entry", "$dest/$entry", $permissions);
        }

        // Clean up
        $dir->close();
        return true;
    }

    function setTemplate($templateId){
        $stmt = $this->uconn->prepare("UPDATE settings SET templateId = ?");
        $stmt->bind_param("i", $templateId);
        if($stmt->execute()){
            $stmt1 = $this->dconn->prepare("SELECT folderName FROM templates WHERE id = ?");
            $stmt1->bind_param("i", $templateId);
            $stmt1->execute();
            $stmt1_result = $stmt1->get_result();
            $newTemplate = $stmt1_result->fetch_assoc()["folderName"];
            $newTemplatePath = "../template/$newTemplate";
            $createFolder = "../../".$_SESSION["folderName"]."/pages";
            if( is_dir($createFolder) ){
                $this->rrmdir($createFolder);
            }
            mkdir($createFolder);
            //echo $newTemplatePath;
            $this->xcopy($newTemplatePath, $createFolder, $permissions = 0755);
            $stmt1->close();
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function setAttendance($staffAttendanceState, $latenessStart, $latenessFee, $absentFee, $studentAttendanceState, $studentLateness, $absentDays, $schoolArrivalAlert){
        $stmt = $this->uconn->prepare("UPDATE settings SET staff_attendance_state = ?, staff_lateness_start = ?, lateness_fee = ?, absent_fee = ?, student_attendance_state = ?, student_lateness_start = ?, absent_days = ?, parent_daily_alert = ?");
        $stmt->bind_param("isiiisii", $staffAttendanceState, $latenessStart, $latenessFee, $absentFee, $studentAttendanceState, $studentLateness, $absentDays, $schoolArrivalAlert);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function setChildPickUp($studentChildPickupState, $addPickupPersons, $pickupPersons, $parentAlertOnPickup, $verifyPickup){
        $stmt = $this->uconn->prepare("UPDATE settings SET child_pickup_state = ?, max_pickup_person = ?, pickup_alert = ?, add_pickup = ?, verify_pickup = ?");
        $stmt->bind_param("iiiss", $studentChildPickupState, $pickupPersons, $parentAlertOnPickup, $addPickupPersons, $verifyPickup);
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    function updateSchoolSettings($category, $schoolName, $email, $phone, $website, $slogan, $currency, $grading, $color, $curriculum, $schoolAddress, $filename){
        $stmt = $this->uconn->prepare("UPDATE settings SET categories = ?, school_name = ?, email = ?, telephone = ?, website = ?, slogan = ?, currency = ?, grading_system = ?, school_color = ?, school_curriculum = ?, schoolAddress = ?, logo = ?");
        $stmt->bind_param("ssssssssssss", $category, $schoolName, $email, $phone, $website, $slogan, $currency, $grading, $color, $curriculum, $schoolAddress, $filename);
        if($stmt->execute()){
            $stmt2 = $this->uconn->prepare("SELECT domain_name FROM settings");
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            $dname = array_shift($stmt2_result)["domain_name"];
            $stmt3 = $this->dconn->prepare("UPDATE schools SET school_name = ? WHERE domain_name = ?");
            $stmt3->bind_param("ss", $schoolName, $dname);
            if($stmt3->execute()){
                $stmt->close();
                $stmt2->close();
                $stmt3->close();
                return true;
            }else{
                $stmt->close();
                $stmt2->close();
                $stmt3->close();
                return false;
            }
        }else{
            return false;
        }
    }

    function updatePaymentSettings($bankName, $acctName, $acctNumber, $disburseState, $disburseDate, $disbursePhone, $deductLateness, $deductAbsent, $outstandingFeeAlert, $alertPeriod, $ePayment, $receiveBank, $receiveAcct, $receiveName, $offlinePayment){
        $stmt =  $this->uconn->prepare("UPDATE settings SET bank_name = ?, account_number = ?, account_name = ?, disbursement_state = ?, disbursement_date = ?, disbursement_number = ?, deduct_lateness = ?, deduct_absent = ?, auto_alert_outstanding = ?, alert_time = ?, online_payment = ?, disburse_bank = ?, disburse_account = ?, disburse_account_name = ?, offline_payment = ?");
        $stmt->bind_param("sssiisiiisisssi", $bankName, $acctNumber, $acctName, $disburseState, $disburseDate, $disbursePhone, $deductLateness, $deductAbsent, $outstandingFeeAlert, $alertPeriod, $ePayment, $receiveBank, $receiveAcct, $receiveName, $offlinePayment);
        if($stmt->execute()){
            if($ePayment == 1 && empty($this->getSchoolDetails()->subAccount)){
                // CREATE PAYSTACK SUBACCT
                if(empty($bankName) || empty($acctNumber)){
                    // BANK NAME OR ACCT NUMBER NOT SET
                    $stmt->close();
                    return "details";
                }else{
                    $action = 1;
                    $businessName = $this->getSchoolDetails()->school_name." ( ".$this->getSchoolDetails()->domain_name." )";
                    $key = $this->getSchoolDetails()->keys["paystack_secret"];
                    include ("ePaymentSetUp.php");
                    if($result->status == true && $result->message == "Subaccount created"){
                        $accountCode = $result->data->subaccount_code;
                        $stmt = $this->uconn->prepare("UPDATE settings SET subAccount = ?");
                        $stmt->bind_param("s", $accountCode);
                        if($stmt->execute()){
                            $stmt->close();
                            return true;
                        }else{
                            $stmt->close();
                            return "error";
                        }
                    }else{
                        $stmt->close();
                        return "exist";
                    }
                }
            }else{
                $stmt->close();
                return true;
            }
        }else{
            $stmt->close();
            return false;
        }

    }

    function updateSubscription($autoRenew, $otherPhone){
        $stmt = $this->uconn->prepare("UPDATE settings SET automatic_renewal = ?, renewal_notify_number = ?");
        $stmt->bind_param("is", $autoRenew, $otherPhone);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function disableBtn($module){
        $totalNumber = $this->totalStudentNumber();
        switch($module){
            case 1:
                $max = 200;
                break;
            case 2:
                $max = 500;
                break;
            case 3:
                $max = 500000;
                break;
            default:
                $max = 0;
        }

        if($max < $totalNumber){
            return false;
        }else{
            return true;
        }
    }
    
    function renewPlanCode($module){
        switch($module){
            case 1:
                $module = 1;
                $smsUnit = 1000;
                break;
            case 2:
                $module = 2;
                $smsUnit = 3000;
                break;
            case 3:
                $module = 3;
                $smsUnit = 5000;
                break;
            default:
                $module = 0;
                $smsUnit = 0;
                break;
        }
        $formerDate = $this->getSchoolDetails()->nextSubDate;
        $sms = $this->getSchoolDetails()->smsUnit + $smsUnit;
        $payNo = $this->getSchoolDetails()->payNo + 1;
        $today = date("Y-m-d");
        $nextSubDate = date("Y-m-d", strtotime($formerDate. '+ 120 days'));
        $stmt = $this->uconn->prepare("UPDATE settings SET sub_date = ?, next_sub_date = ?, smsUnit = ?, modules = ?, pay_no = ?");
        $stmt->bind_param("ssiii", $today, $nextSubDate, $sms, $module, $payNo);
        if($stmt->execute()){
            $stmt->execute();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }
}
?>