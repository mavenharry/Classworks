<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
SUBSCRIPTIONS SETTINGS
</h5>
<div class="form-desc">
Monitor and manage all system subscriptions and usage. 
</div>
<!-- PORTAL -->
<div class="table-responsive">

        <form method="POST" action="#">
          <fieldset class="form-group">
          <legend><span>AUTOMATIC SUBSCRIPTION RENEWAL</span></legend>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Automatic system subscription renewal</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->autoRenew == 1 ? "checked" : ""; ?> name="autoRenew" type="checkbox" id="autoRenew">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row autoRenewElements">
              <label class="col-sm-6 col-form-label" for=""> Who to notify after every renewal</label>
              <div class="col-sm-6">
              <select name="renewalNotify" class="form-control" id="subScriptionWhoToNotify">
                <option <?php echo $schoolDetails->notifyRenewal == $schoolDetails->phone ? "selected" : ""?> value="System Administrator">System Administrator</option>
                <option <?php echo $schoolDetails->notifyRenewal != $schoolDetails->phone ? "selected" : ""?> value="Other">Others</option>
                </select>
            </div>
            </div>

            <div class="form-group row" id="othersNumber4" style="display:<?php echo $schoolDetails->notifyRenewal != $schoolDetails->phone ? "flex" : "none";?>">
              <label class="col-sm-6 col-form-label" for=""> Other reciever's telephone</label>
              <div class="col-sm-6">
                <input class="form-control" value="<?php echo $schoolDetails->notifyRenewal; ?>" name="otherPhone" placeholder="E.g; 08012345678" type="text">
              </div>
            </div>
            
            <div class="form-buttons-w autoRenewElements">
            <input type="submit" value=" Save subscription settings" class="btn btn-primary" name="saveSubSettings">
            </div>
          </fieldset>
          <fieldset class="form-group manualPayment">
          <legend><span>MANUAL SUBSCRIPTION RENEWAL</span></legend>
            <h5><span style="font-weight:700">SYSTEM UTILITY FEE:</span></h5>
            <h5 style="font-weight:400">Number of Students = 150</h5>
            <h5 style="font-weight:400">Pricing plan = &#8358;700/Student</h5>
            <h5 style="font-weight:400">Total = &#8358;65000</h5>

            <br><h5><span style="font-weight:700">SMS UTILITY FEE:</span></h5>
            <h5 style="font-weight:400">Number of SMS sent = 206</h5>
            <h5 style="font-weight:400">Pricing plan = &#8358;3/SMS</h5>
            <h5 style="font-weight:400">Total = &#8358;2300</h5>

            <br><h5><span style="font-weight:700">TOTAL AMOUNT PAYABLE:</span></h5>
            <h5 style="font-weight:400">Amount = &#8358;67300</h5>
            <div class="form-buttons-w">
            <button class="btn btn-primary" style="float:right; background-color:##FC9933; border-color:##FC9933;" type="submit"> Pay with credit card</button>            
            <button class="btn btn-primary" style="float:right; background-color:#1BB4AD; border-color:#1BB4AD; margin-right:20px; color:#fff" type="submit"> Pay with bank account</button>
            </div>
          </fieldset>
        </form>


</div>
</div>
</div>