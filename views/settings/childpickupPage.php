<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
CHILD PICKUP SETTINGS
</h5>
<div class="form-desc">
Automate and secure student pickup. Assign who is responsible for picking up student after school close and be responsible for child safety. 
</div>
<!-- PORTAL -->
<div class="table-responsive">
    
        <form method="POST" action="#">
          <fieldset class="form-group">
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Child pickup verification</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->studentChildPickupState == 1 ? "checked" : "";?> name="studentChildPickupState" type="checkbox" id="studentChildPickupState">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row childPickupOption">
              <label class="col-sm-6 col-form-label" for=""> Maximum no. of pickup persons</label>
              <div class="col-sm-6">
              <select class="form-control" name="pickupPersons">
                <option <?php echo $schoolDetails->pickupPersons == 1 ? "selected": ""?> value="1">1</option>
                <option <?php echo $schoolDetails->pickupPersons == 2 ? "selected": ""?> value="2">2</option>
                <option <?php echo $schoolDetails->pickupPersons == 3 ? "selected": ""?> value="3">3</option>
                <option <?php echo $schoolDetails->pickupPersons == 4 ? "selected": ""?> value="4">4</option>
                <option <?php echo $schoolDetails->pickupPersons == 5 ? "selected": ""?> value="5">5</option>
                <option <?php echo $schoolDetails->pickupPersons == 6 ? "selected": ""?> value="6">6</option>
                <option <?php echo $schoolDetails->pickupPersons == 7 ? "selected": ""?> value="7">7</option>
                <option <?php echo $schoolDetails->pickupPersons == 8 ? "selected": ""?> value="8">8</option>
                <option <?php echo $schoolDetails->pickupPersons == 9 ? "selected": ""?> value="9">9</option>
                <option <?php echo $schoolDetails->pickupPersons == 10 ? "selected": ""?> value="10">10</option>
             </select>
              </div>
            </div>
            <div class="form-group row childPickupOption">
              <label class="col-sm-6 col-form-label" for=""> Alert parents when child is picked up</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->parentAlertOnPickup == 1 ? "checked" : "";?> name="parentAlertOnPickup" type="checkbox">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row childPickupOption">
              <label class="col-sm-6 col-form-label" for=""> Who can add pickup persons to the system?</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="addPickupPersons[]" style="width:100%" multiple="true">
                  <option <?php echo isset($schoolDetails->addPickupPersons) && in_array("Tutor", $schoolDetails->addPickupPersons) ? "selected":"";?> value="Tutor">Tutor</option>
                  <option <?php echo isset($schoolDetails->addPickupPersons) && in_array("System Administrator", $schoolDetails->addPickupPersons) ? "selected":"";?>  value="System Administrator">System Administrator</option>
                </select>
              </div>
            </div>
            <div class="form-group row childPickupOption">
              <label class="col-sm-6 col-form-label" for=""> Who performs pickup verification?</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="verifyPickup[]" style="width:100%" multiple="true">
                <option <?php echo isset($schoolDetails->verifyPickup) && in_array("Tutor", $schoolDetails->verifyPickup) ? "selected":"";?>  value="Tutor">Tutor</option>
                <option <?php echo isset($schoolDetails->verifyPickup) && in_array("System Administrator", $schoolDetails->verifyPickup) ? "selected":"";?> value="System Administrator">System Administrator</option>
                </select>
              </div>
            </div>
          </fieldset>
          <div class="form-buttons-w">
            <input type="submit" class="btn btn-primary" value=" Save child pickup settings" name="childPickupSettings">
          </div>
        </form>


</div>
</div>
</div>