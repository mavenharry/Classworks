<?php include_once ("settingsbtn.php"); 
  $schoolDetails = $settingsClass->getSchoolDetails(); 
  
  $normalCharge = $settingsClass->getCharge(); ?>
<!DOCTYPE html>
<html>
   <head>
    <title>ClassWorks - School Automation Solution | Renewal</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
   <body class="menu-position-side menu-side-left full-screen">
      <div class="all-wrapper no-padding-content white-bg-content solid-bg-all">
            <div class="content-w">
               <form> <input type="email" hidden value="<?php echo isset($_SESSION["email"]) ? $_SESSION["email"] : $schoolDetails->email; ?>" id="cusEmail"><input type="text" hidden value="<?php echo isset($_SESSION["phone"]) ? $_SESSION["phone"] : ""; ?>" id="phone"><input type="hidden" id="schoolName" name="schoolName" value="<?php echo $schoolDetails->school_name; ?>"></form>
               <div class="content-i">
                  <div class="content-box">
                     <div class="section-heading centered">
                        <h1>CHOOSE PAYMENT PLAN</h1>
                        <p style="padding-right:100px; padding-left:100px; text-align:center">We present to you the coolest pricing plans for schools and colleges automation/management systems accross the universe. Choose the plan that best suits your school needs or budget.</p>
                     </div>
                     <div class="pricing-plans row no-gutters" style="margin-top: -20px;">
                        <div class="pricing-plan col-sm-offset-9 col-sm-3 with-hover-effect" style="margin-left:13%; border: 1px solid rgba(0,0,0,0.1);">
                           <div class="plan-head">
                              <div class="plan-name">Bronze</div>
                           </div>
                           <?php
                            echo $schoolDetails->modules == 1 ? "<h4>Current Plan</h4>" : "";
                            $message = $schoolDetails->modules == 1 ? "Renew Plan" : "Choose this plan";
                            $charge = $schoolDetails->modules == 1 ? $normalCharge : $settingsClass->otherPlanPrice(1);
                           ?>
                           <div class="plan-body">
                              <div class="plan-price-w">
                                 <div class="price-value">&#8358;200</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Total Charge : &#8358;<?php echo number_format($charge); ?></div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="1" id="module"><input <?php echo $settingsClass->disableBtn(1) === false ? "disabled" : ""; ?> type="submit" name="create" class="btn btn-primary btn-rounded planRenew" value="<?php echo $message; ?>"></form></div>
                           </div>
                           <div class="plan-description">
                              <h6>Description</h6>
                              <p style="text-align:justify">Basic system subscription plan specially packaged for schools and colleges with low budget or very minimal automation and management needs.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>1,000 Free SMS Units</li>
                              <li>Max of 200 Students</li>
                              </ul>
                           </div>
                        </div>
                        <div class="pricing-plan col-sm-3 with-hover-effect">
                           <div class="plan-head">
                              <div class="plan-name">Silver</div>
                           </div>
                           <?php
                            echo $schoolDetails->modules == 2 ? "<h4>Current Plan</h4>" : "";
                            $message = $schoolDetails->modules == 2 ? "Renew Plan" : "Choose this plan";
                            $charge = $schoolDetails->modules == 2 ? $normalCharge : $settingsClass->otherPlanPrice(2);
                           ?>
                           <div class="plan-body">
                              <div class="plan-price-w">
                              <div class="price-value">&#8358;300</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Total Charge : &#8358;<?php echo number_format($charge)?></div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="2" id="module"><input <?php echo $settingsClass->disableBtn(2) === false ? "disabled" : ""; ?> type="submit" name="create" class="planRenew btn btn-primary btn-rounded" value="<?php echo $message; ?>"></form></div>
                           </div>
                           <div class="plan-description">
                              <h6>Description</h6>
                              <p style="text-align:justify">Standard system subscription plan standardized and packaged for schools and colleges with standard budget and for full automation systems.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Automated Attendance</li>
                              <li>Child Pickup System</li>
                              <li>Hostel Management</li>
                              <li>Result Checker (Pin Vending)</li>
                              <li>Inventory Management (Tuck Shop)</li>
                              <li>Educational Games</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>3,000 Free SMS Units</li>
                              <li>Max of 500 Students</li>
                              </ul>
                           </div>
                        </div>
                        <div class="pricing-plan col-sm-3 with-hover-effect">
                           <div class="plan-head">
                              <div class="plan-name">Gold</div>
                           </div>
                           <?php
                            echo $schoolDetails->modules == 3 ? "<h4>Current Plan</h4>" : "";
                            $message = $schoolDetails->modules == 3 ? "Renew Plan" : "Choose this plan";
                            $charge = $schoolDetails->modules == 3 ? $normalCharge : $settingsClass->otherPlanPrice(3);
                           ?>
                           <div class="plan-body">
                              <div class="plan-price-w">
                                 <div class="price-value">&#8358;500</div>
                                 <div class="price-label">Per Student</div> <br>
                                 <div class="price-label">Term Timeline (120 Days)</div>
                                 <div class="price-label">Total Charge : &#8358;<?php echo number_format($charge); ?></div>
                              </div>
                              <div class="plan-btn-w"><form method="POST" action="#"><input type="hidden" name="module" value="3" id="module"><input type="submit" <?php echo $settingsClass->disableBtn(3) === false ? "disabled" : ""; ?> name="create" class="planRenew btn btn-primary btn-rounded" value="<?php echo $message; ?>"></form></div>
                           </div>
                           <div class="plan-description">
                           <h6>Description</h6>
                              <p style="text-align:justify">Premium system subscription plan carefully packaged for premium schools and colleges with good budget requires very high functionalities.</p>
                              <h6>Features</h6>
                              <ul>
                              <li>Dedicated Mobile App</li>
                              <li>Online Fees Payment</li>
                              <li>On-Desk Payment Terminal (Virtual POS)</li>
                              <li>Finance Management</li>
                              <li>Remote Work Monitoring</li>
                              <li>Staff Management</li>
                              <li>Student Management</li>
                              <li>Parent Management</li>
                              <li>Automated Lessons Plan</li>
                              <li>Classroom Management</li>
                              <li>Assessment Management</li>
                              <li>Automated Attendance</li>
                              <li>Child Pickup System</li>
                              <li>Virtual Library/Book Shop</li>
                              <li>Hostel Management</li>
                              <li>Result Checker (Pin Vending)</li>
                              <li>Inventory Management (Tuck Shop)</li>
                              <li>Educational Games</li>
                              <li>Human Resource Management/Payroll</li>
                              <li>Computer Based Examination/Test</li>
                              <li>Virtual Counselling</li>
                              <li>Discussion Forums</li>
                              <li>Custom Email Address</li>
                              <li>Custom Domain Name</li>
                              <li>SSL Site Security</li>
                              <li>Data Backup/Recovery</li>
                              <li>1 Month Free Trial</li>
                              <li>5,000 Free SMS Units</li>
                              <li>Unlimited Students</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="display-type"></div>
      </div>

        <!-------------------- START - Top Bar -------------------->
        <?php include_once ("../../includes/support.php") ?>
        <!-------------------- END - Top Bar -------------------->

        <?php include_once ("../../includes/scripts.php"); ?>
   </body>
</html>