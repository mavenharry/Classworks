<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
STAFFS AND STUDENTS ATTENDANCE
</h5>
<div class="form-desc">
Customize and configure the mode of operation for your school and regulate how attendance is monitored. 
</div>
<!-- PORTAL -->
<div class="table-responsive">


        <form method="POST" action="">
          <fieldset class="form-group">
              <legend><span>STAFFS</span></legend>
              <div class="form-group row">
                <label class="col-sm-6 col-form-label" for=""> Staff attendance collection</label>
                <div class="col-sm-6">
                  <div class="c-toggle-btn">
                  <input <?php echo $schoolDetails->staffAttendanceState == 1 ? "checked":""; ?> name="staffAttendanceState" type="checkbox" id="staffAttendanceState">
                  <div>
                      <label class="on">On</label>
                      <label class="off">Off</label>
                      <span class="c-toggle-thumb"></span>
                  </div>
                  </div>
                </div>
              </div>
              <div class="form-group row attendanceStaffOption">
                <label class="col-sm-6 col-form-label" for=""> Lateness Starts</label>
                <div class="col-sm-6">
                  <input class="form-control timeselect" value="<?php echo $schoolDetails->staffLatenessStart == NULL ? "" : $schoolDetails->staffLatenessStart;?>" name="latenessStart" placeholder="8:00 AM" type="text">
                </div>
              </div>
              <div class="form-group row attendanceStaffOption">
                <label class="col-sm-6 col-form-label" for=""> Lateness Fee</label>
                <div class="col-sm-6">
                  <input class="form-control" value="<?php echo $schoolDetails->latenessFee == 0 ? "" : $schoolDetails->latenessFee;?>" name="latenessFee" placeholder="&#8358;0.00" type="text">
                </div>
              </div>
              <div class="form-group row attendanceStaffOption">
                <label class="col-sm-6 col-form-label" for=""> Absent Fee</label>
                <div class="col-sm-6">
                  <input class="form-control" value="<?php echo $schoolDetails->absentFee == 0 ? "" : $schoolDetails->absentFee;?>" name="absentFee" placeholder="&#8358;0.00" type="text">
                </div>
              </div>
          </fieldset>
          <fieldset class="form-group">
            <legend><span>STUDENTS</span></legend>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Student attendance collection</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->studentAttendanceState == 1 ? "checked":""; ?> name="studentAttendanceState" type="checkbox" id="studentAttendanceState">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row attendanceStudentOption">
              <label class="col-sm-6 col-form-label" for=""> Lateness Starts</label>
              <div class="col-sm-6">
                <input class="form-control timeselect" value="<?php echo $schoolDetails->studentLatenessStart == NULL ? "" : $schoolDetails->studentLatenessStart;?>" name="studentLateness" placeholder="8:00 AM" type="text">
              </div>
            </div>
            <div class="form-group row attendanceStudentOption">
              <label class="col-sm-6 col-form-label" for=""> Alert parents if student is absent for</label>
              <div class="col-sm-6">
                <select class="form-control" name="absentDays">
                <option <?php echo $schoolDetails->absentDays == 0 ? "selected":"";?> value="0">Don't alert</option>
                <option <?php echo $schoolDetails->absentDays == 1 ? "selected":"";?> value="1">1 days</option>
                <option <?php echo $schoolDetails->absentDays == 2 ? "selected":"";?> value="2">2 days</option>
                <option <?php echo $schoolDetails->absentDays == 3 ? "selected":"";?> value="3">3 days</option>
                <option <?php echo $schoolDetails->absentDays == 4 ? "selected":"";?> value="4">4 days</option>
                <option <?php echo $schoolDetails->absentDays == 5 ? "selected":"";?> value="5">5 days</option>
                <option <?php echo $schoolDetails->absentDays == 6 ? "selected":"";?> value="6">6 days</option>
                <option <?php echo $schoolDetails->absentDays == 7 ? "selected":"";?> value="7">7 days</option>
                <option <?php echo $schoolDetails->absentDays == 8 ? "selected":"";?> value="8">8 days</option>
                <option <?php echo $schoolDetails->absentDays == 9 ? "selected":"";?> value="9">9 days</option>
                <option <?php echo $schoolDetails->absentDays == 10 ? "selected":"";?> value="10">10 days</option>
                </select>
              </div>
            </div>
            <div class="form-group row attendanceStudentOption">
              <label class="col-sm-6 col-form-label" for=""> Parent's daily student school arrival alert</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->parentDailyAlert == 1 ? "checked":""; ?> name="schoolArrivalAlert" type="checkbox">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
          </fieldset>
          <div class="form-buttons-w">
            <input type="submit" value=" Save attendance settings" name="attendanceSettings" class="btn btn-primary">
          </div>
        </form>


</div>
</div>
</div>