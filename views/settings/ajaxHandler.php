<?php
    include_once ("settingsbtn.php"); 

    if(isset($_GET["packageRenew"])){
        $pkey = $settingsClass->getSchoolDetails()->keys["paystack_public"];
        $package = htmlentities(trim($_GET["packageRenew"]));
        $reply = $settingsClass->renewDetails($package);
        if($reply !== false){
            $obj = new stdClass();
            $obj->price = $reply->charge;
            $obj->pkey = $pkey;
            $obj->subAccountCode = $reply->refCode;
            echo json_encode($obj);
        }else{
            echo "false";
        }
    }

    if(isset($_GET["planAction"]) && $_GET["planAction"] == "renew"){
        $module = htmlentities(trim($_GET["module"]));
        $ref = htmlentities(trim($_GET["verify"]));
        $reply = $settingsClass->verifyRef($ref);
        if($reply === true){
            $reply = $settingsClass->renewPlanCode($module);
            echo $reply;
        }else{
            echo "wrongRef";
        }
    }
?>