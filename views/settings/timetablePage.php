<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
TIMETABLE SETTINGS
</h5>
<div class="form-desc">
Customize and configure the structure of your timetable as related to your school mode of lectures etc. 
</div>
<!-- PORTAL -->
<div class="table-responsive">


        <form>
            <div class="form-group row">
            <div class="col-sm-4">
                <select id="setTitle" class="form-control timetableTime">
                <option value="devotion">Devotion time</option>
                <option value="class">Class time</option>
                <option value="break">Break time</option>
                <option value="sports">Sports time</option>
                <option value="lunch">Lunch time</option>
                <option value="lab">Lab time</option>
                <option value="other">Other(s)</option>
                </select>
                </div>
                <div class="col-sm-5">
                <input class="form-control timeselect" id="setTime" placeholder="8:00 AM" type="text">
                </div>
                <div class="col-sm-3">
                <button class="btn btn-primary addTime" type="button"> Add Time</button>
                </div>
            </div>
            <div class="form-group row" id="othersNumber3" style="display:none; margin-top:30px;">
                <label class="col-sm-6 col-form-label" for=""> Specify the other time on the timetable title</label>
                <div class="col-sm-6">
                <input class="form-control" placeholder="E.g; Sermon Time" type="text">
                </div>
            </div>
          <fieldset class="form-group">
            <legend><span>WEEKLY ACTIVITY TIME SHEET</span></legend>
            
            <div id="timeSheetDiv">

            <div class="form-group row" id="time1">
            <div class="col-sm-4">
                <select class="form-control timetableTime">
                <option value="Devotion time">Devotion time</option>
                <option value="Class time">Class time</option>
                <option value="Break time">Break time</option>
                <option value="Sports time">Sports time</option>
                <option value="Lunch time">Lunch time</option>
                <option value="Lab time">Lab time</option>
                </select>
              </div>
              <div class="col-sm-5">
                <input class="form-control timeselect" placeholder="8:00 AM" type="text">
              </div>
              <div class="col-sm-3">
              <button class="btn btn-primary" onclick="removeTime('time1')" style="background-color:#FB5574; border-color:#FB5574" type="button"> Remove</button>
              </div>
            </div>

            </div>

          </fieldset>
          <div class="form-buttons-w">
            <button class="btn btn-primary" type="submit"> Save timetable settings</button>
          </div>
        </form>


</div>
</div>
</div>