<?php include_once ("settingsbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Timetable</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $settingsClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Settings", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#FB5574; border:#FB5574; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Reset All Settings to Default</span>
                                    </a>
                                </div>
                                <h6 class="element-header">SYSTEM CONFIGURATIONS/SETTINGS</h6>
                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-3">

                                <div class="element-box">
                                <div>
                                <label onclick="selectSetting('portalPage')" id="portalPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Portal</label>
                                <label onclick="selectSetting('idcardPage')" id="idcardPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> ID Card</label>
                                <label onclick="selectSetting('attendancePage')" id="attendancePage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Attendance</label>
                                <label onclick="selectSetting('timetablePage')" id="timetablePage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Timetable</label>
                                <label onclick="selectSetting('childpickupPage')" id="childpickupPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Child Pickup</label>
                                <label onclick="selectSetting('schoolPage')" id="schoolPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> School</label>
                                <!-- <label onclick="selectSetting('exams_quiz')" id="exams_quiz" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Exams & Quiz</label>                                                                                                 -->
                                <label onclick="selectSetting('paymentPage')" id="paymentPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Payments</label>
                                <label onclick="selectSetting('subscriptionsPage')" id="subscriptionsPage" class="my_hover_up col-form-label settings" style="margin-top:7px; margin-bottom:7px; text-align:center; border-radius:5px; cursor:pointer; color:#FFFFFF; width:100%; font-size:1.2rem;" for=""> Subscriptions</label>
                                </div>
                                </div>
                                </div>

                                <View id="portalPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/portalPage.php") ?>
                                </View>

                                <View id="idcardPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/idcardPage.php") ?>
                                </View>

                                <View id="attendancePageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/attendancePage.php") ?>
                                </View>

                                <View id="childpickupPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/childpickupPage.php") ?>
                                </View>

                                <View id="schoolPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/schoolPage.php") ?>
                                </View>

                                <View id="paymentPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/paymentPage.php") ?>
                                </View>

                                <View id="subscriptionsPageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/subscriptionsPage.php") ?>
                                </View>

                                <View id="timetablePageView" class="settingsView col-lg-9" style="display:none">
                                <?php include("../../views/settings/timetablePage.php") ?>
                                </View>

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

<?php 

if(isset($_GET['tab'])){
    echo '<script>
    $(document).ready(function(){
    $('.$_GET['tab'].').click()
    });
    </script>
    ';
}else{
    echo '<script>
    $(document).ready(function(){
    $("#portalPage").click()
    });
    </script>
    ';
}

?>

</body>

</html>