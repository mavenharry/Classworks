<?php
    include_once ("settingsClass.php");
    $settingsClass = new settingsClass;

    if(isset($_POST["saveTemplate"])){
        $templateId = htmlentities(trim($_POST["templateId"]));
        $reply = $settingsClass->setTemplate($templateId);
        if($reply === true){
            $mess = 'You have successfully chosen a template.';
        }else{
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
        }
    }

    if(isset($_POST["attendanceSettings"])){
        $staffAttendanceState = isset($_POST["staffAttendanceState"]) ? 1 : 0;
        if($staffAttendanceState == 1){
            $latenessStart = htmlentities(trim($_POST["latenessStart"]));
            $latenessFee = $_POST["latenessFee"] == "" ? "0" : htmlentities(trim($_POST["latenessFee"]));
            $absentFee = $_POST["absentFee"] == "" ? "0" : htmlentities(trim($_POST["absentFee"]));
        }
        $studentAttendanceState = isset($_POST["studentAttendanceState"]) ? 1 : 0;
        if($studentAttendanceState == 1){
            $studentLateness = htmlentities(trim($_POST["studentLateness"]));
            $absentDays = htmlentities(trim($_POST["absentDays"]));
            $schoolArrivalAlert = isset($_POST["schoolArrivalAlert"]) ? 1 : 0;
        }

        $reply = $settingsClass->setAttendance(@$staffAttendanceState, @$latenessStart, @$latenessFee, @$absentFee, @$studentAttendanceState, @$studentLateness, @$absentDays, @$schoolArrivalAlert);
        if($reply === true){
            $mess = 'You have successfully updated attendance settings.';
        }else{
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
        }
    }

    if(isset($_POST["childPickupSettings"])){
        $studentChildPickupState = isset($_POST["studentChildPickupState"]) ? 1 : 0;
        if($studentChildPickupState == 1){
            @$addPickupPersons = json_encode($_POST["addPickupPersons"]);
            @$pickupPersons = htmlentities(trim($_POST["pickupPersons"]));
            @$parentAlertOnPickup = isset($_POST["parentAlertOnPickup"]) ? 1 : 0;
            @$verifyPickup = json_encode($_POST["verifyPickup"]);
        }
        
        $reply = $settingsClass->setChildPickUp(@$studentChildPickupState, @$addPickupPersons, @$pickupPersons, @$parentAlertOnPickup, @$verifyPickup);
        if($reply === true){
            $mess = 'You have successfully updated child pickup settings.';
        }else{
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
        }
    }

    if(isset($_POST["schoolSettings"])){
        $category = json_encode($_POST["category"]);
        $schoolName = htmlentities(trim($_POST["schoolName"]));
        $email = htmlentities(trim($_POST["email"]));
        $phone = htmlentities(trim($_POST["phone"]));
        $website = htmlentities(trim($_POST["website"]));
        $schoolAddress = htmlentities(trim($_POST["schoolAddress"]));
        $slogan = htmlentities(trim($_POST["slogan"]));
        $currency = htmlentities(trim($_POST["currency"]));
        $grading = htmlentities(trim($_POST["grading"]));
        $color = htmlentities(trim($_POST["color"]));
        $curriculum = htmlentities(trim($_POST["curriculum"]));
        $imageFolder = "../../".$_SESSION["folderName"]."/"."SchoolImages/";
        if($_FILES["schoolLogo"]["size"] > 0){
            if($_FILES["schoolLogo"]["error"]){
                $error = "An error occured with the following error. ".$_FILES["schoolLogo"]["error"];
            }else{
                $extension = @end(explode(".",$_FILES["schoolLogo"]["name"]));
    
                $filename = "logo".".".$extension;
                if(file_exists($imageFolder."/".$filename)){
                    unlink($imageFolder."/".$filename);
                }
                move_uploaded_file($_FILES["schoolLogo"]["tmp_name"], $imageFolder."/".$filename);
    
            }
        }else{
            $logoPath = $settingsClass->getSchoolDetails()->logo;
            $pathArray = explode("/", $logoPath);
            $filename =  $pathArray[count($pathArray) - 1];
        }

        $reply = $settingsClass->updateSchoolSettings($category, $schoolName, $email, $phone, $website, $slogan, $currency, $grading, $color, $curriculum, $schoolAddress, $filename);
        if($reply === true){
            $mess = 'You have successfully updated school settings.';
        }else{
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
        }


    }

    if(isset($_POST["savePaymentSettings"])){
        $bankName = htmlentities(trim($_POST["bankName"]));
        $acctNumber = htmlentities(trim($_POST["acctNumber"]));
        $acctName = htmlentities(trim($_POST["acctName"]));
        $disburseState = isset($_POST["disburseState"]) ? 1 : 0;
        $disburseDate = htmlentities(trim($_POST["disburseDate"]));
        $disbursePerson = htmlentities(trim($_POST["disbursePerson"]));
        $disbursePhone = htmlentities(trim($_POST["disbursePhone"]));
        $deductLateness = isset($_POST["deductLateness"]) ? 1 : 0;
        $deductAbsent = isset($_POST["deductAbsent"]) ? 1 : 0;
        $outstandingFeeAlert = isset($_POST["outstandingFeeAlert"]) ? 1 : 0;
        $alertPeriod = htmlentities(trim($_POST["alertPeriod"]));
        $ePayment = isset($_POST["ePayment"]) ? 1 : 0;
        $receivePayment = htmlentities(trim($_POST["receivePayment"]));
        $offlinePayment = isset($_POST["offlinePayment"]) ? 1 : 0;

        if($disbursePerson == "System Administrator"){
            $disbursePhone = $settingsClass->getSchoolDetails()->phone;
        }

        if($receivePayment == "Main"){
            $receiveBank = $bankName;
            $receiveAcct = $acctNumber;
            $receiveName = $acctName;
        }

        $reply = $settingsClass->updatePaymentSettings($bankName, $acctName, $acctNumber, $disburseState, $disburseDate, $disbursePhone, $deductLateness, $deductAbsent, $outstandingFeeAlert, $alertPeriod, $ePayment, $receiveBank, $receiveAcct, $receiveName, $offlinePayment);
        if($reply === true){
            $mess = 'You have successfully updated payment settings.';
        }else{
            switch($reply){
                case false:
                case 'error':
                    $err = "We are so sorry an error occured during your last operation. Please try again later.";
                    break;
                case 'exist':
                    $err = "An error occured while trying to create your account. Please contact support with error message: EPAYMENT ACCOUNT ALREADY EXIST.";
                    break;
                case 'details':
                    $err = "Bank name and account number not set. Please fill in those fields and try again";
                    break;
                default :
                    $err = "We are so sorry an error occured during your last operation. Please try again later.";
            }
        }
    }

    if(isset($_POST["saveSubSettings"])){
        $autoRenew = isset($_POST["autoRenew"]) ? 1 : 0;
        @$renewalNotify = htmlentities(trim($_POST["renewalNotify"]));
        @$otherPhone = htmlentities(trim($_POST["otherPhone"]));
        
        if($renewalNotify == "System Administrator"){
            $otherPhone = $settingsClass->getSchoolDetails()->phone;
        }

        $reply = $settingsClass->updateSubscription($autoRenew, $otherPhone);
        if($reply === true){
            $mess = 'You have successfully updated your subscription settings.';
        }else{
            $err = "We are so sorry an error occured during your last operation. Please try again later.";
        }
    }
?>