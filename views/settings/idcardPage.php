<div class="col-lg-12">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
SCHOOL ID CARD DESIGN
</h5>
<div class="form-desc">
Customize the looks and feel of your school id card. Select from our wide ranges of designs and layouts that fits your school. 
</div>
<!-- PORTAL -->
<div class="table-responsive">


<div class="row" style="margin-top:20px;">

<div class="flip-container" onclick="idcardClick('idcard1')" ontouchstart="this.classList.toggle('hover');">
	<div class="flipper">
		<div class="front">
            <div class='col-lg-4' style="margin-right:0px; cursor:pointer">
            <img style="border-radius:5px; width:220px; height:320px;" class="img-responsive webpage" alt="" src="../../images/id_front.png" />
            <img alt="" id="idcard1" class="idcardMarker" style="display:none; z-index:200000; position:absolute; top:105px; left:90px; width:80px; height:80px;" src="../../images/verified.png">            
            </div>
		</div>
		<div class="back">
            <div class='col-lg-4' style="margin-right:0px; cursor:pointer">
            <img style="border-radius:5px; width:220px; height:320px;" class="img-responsive webpage" alt="" src="../../images/id_back.png" />
            </div>
		</div>
	</div>
</div>

</div>
</div>
<br>
<div class="form-buttons-w">
  <button class="btn btn-primary" type="submit"> Save id card settings</button>
</div>
</div>

</div>