
<div class="col-lg-9">
<div class="element-box">
<h5 style="color:#08ACF0" class="form-header">
PAYMENTS & PAYROLL SETTINGS
</h5>
<div class="form-desc">
Setup payment parameters and coditions to automate all your payments disbursements and collections. 
</div>
<!-- PORTAL -->
<div class="table-responsive">

        <form method="POST" action="#">
        <fieldset class="form-group">
            <legend><span>MAIN BANK ACCOUNT</span></legend>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Bank Name</label>
              <div class="col-sm-6">
                <select <?php echo !empty($schoolDetails->bankName) ? "readonly" : "" ?> class="form-control" name="bankName">
                    <?php
                    //CHECK IF COUNTRY IS NIGERIA, KENYA OR GHANA
                    $countries = array("nigeria", "kenya", "ghana");
                    $country = strtolower("nigeria");
                    if(in_array($country, $countries)){
                      $json = file_get_contents('../../includes/'.$country.'_banks.json');
                      $json_data = json_decode($json,true);
                      foreach ($json_data as $key => $value) { ?>
                        <option <?php echo $schoolDetails->bankName == $json_data[$key]["name"] ? "selected" : "";?> value="<?php echo $json_data[$key]["name"]?>"><?php echo strtoupper($json_data[$key]["name"]) ?></option>
                      <?php }
                    }else{ ?>
                        <option value="">Your country is not available yet!</option>
                    <?php }?>
                  </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Bank Account Number</label>
              <div class="col-sm-6">
                <input class="form-control" <?php echo !empty($schoolDetails->acctNumber) ? "readonly" : "" ?> value="<?php echo $schoolDetails->acctNumber; ?>" name="acctNumber" placeholder="E.g; 0474352638" type="text">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label" for=""> Bank Account Name</label>
              <div class="col-sm-8">
                <input class="form-control" <?php echo !empty($schoolDetails->acctName) ? "readonly" : "" ?> value="<?php echo $schoolDetails->acctName; ?>" name="acctName" placeholder="E.g; Georginia Academy" type="text">
              </div>
            </div>
          </fieldset>
        <fieldset class="form-group">
            <legend><span>STAFFS</span></legend>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Staff salary automatic disbursement</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->disburseState == 1 ? "checked" : ""; ?> name="disburseState" type="checkbox" id="Salaryautomaticdisbursement">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row salaryOption">
              <label class="col-sm-6 col-form-label" for=""> Day of every month to disburse salary</label>
              <div class="col-sm-6">
              <select name="disburseDate" class="form-control">
                <option <?php echo $schoolDetails->disburseDate == 1 ? "selected" : ""; ?> value="1">1st</option>
                <option <?php echo $schoolDetails->disburseDate == 2 ? "selected" : ""; ?> value="2">2nd</option>
                <option <?php echo $schoolDetails->disburseDate == 3 ? "selected" : ""; ?> value="3">3rd</option>
                <option <?php echo $schoolDetails->disburseDate == 4 ? "selected" : ""; ?> value="4">4th</option>
                <option <?php echo $schoolDetails->disburseDate == 5 ? "selected" : ""; ?> value="5">5th</option>
                <option <?php echo $schoolDetails->disburseDate == 6 ? "selected" : ""; ?> value="6">6th</option>
                <option <?php echo $schoolDetails->disburseDate == 7 ? "selected" : ""; ?> value="7">7th</option>
                <option <?php echo $schoolDetails->disburseDate == 8 ? "selected" : ""; ?> value="8">8th</option>
                <option <?php echo $schoolDetails->disburseDate == 9 ? "selected" : ""; ?> value="9">9th</option>
                <option <?php echo $schoolDetails->disburseDate == 10 ? "selected" : ""; ?> value="10">10th</option>
                <option <?php echo $schoolDetails->disburseDate == 11 ? "selected" : ""; ?> value="11">11th</option>
                <option <?php echo $schoolDetails->disburseDate == 12 ? "selected" : ""; ?> value="12">12th</option>
                <option <?php echo $schoolDetails->disburseDate == 13 ? "selected" : ""; ?> value="13">13th</option>
                <option <?php echo $schoolDetails->disburseDate == 14 ? "selected" : ""; ?> value="14">14th</option>
                <option <?php echo $schoolDetails->disburseDate == 15 ? "selected" : ""; ?> value="15">15th</option>
                <option <?php echo $schoolDetails->disburseDate == 16 ? "selected" : ""; ?> value="16">16th</option>
                <option <?php echo $schoolDetails->disburseDate == 17 ? "selected" : ""; ?> value="17">17th</option>
                <option <?php echo $schoolDetails->disburseDate == 18 ? "selected" : ""; ?> value="18">18th</option>
                <option <?php echo $schoolDetails->disburseDate == 19 ? "selected" : ""; ?> value="19">19th</option>
                <option <?php echo $schoolDetails->disburseDate == 20 ? "selected" : ""; ?> value="20">20th</option>
                <option <?php echo $schoolDetails->disburseDate == 21 ? "selected" : ""; ?> value="21">21st</option>
                <option <?php echo $schoolDetails->disburseDate == 22 ? "selected" : ""; ?> value="22">22nd</option>
                <option <?php echo $schoolDetails->disburseDate == 23 ? "selected" : ""; ?> value="23">23rd</option>
                <option <?php echo $schoolDetails->disburseDate == 24 ? "selected" : ""; ?> value="24">24th</option>
                <option <?php echo $schoolDetails->disburseDate == 25 ? "selected" : ""; ?> value="25">25th</option>
                <option <?php echo $schoolDetails->disburseDate == 26 ? "selected" : ""; ?> value="26">26th</option>
                <option <?php echo $schoolDetails->disburseDate == 27 ? "selected" : ""; ?> value="27">27th</option>
                <option <?php echo $schoolDetails->disburseDate == 28 ? "selected" : ""; ?> value="28">28th</option>
                <option <?php echo $schoolDetails->disburseDate == 29 ? "selected" : ""; ?> value="29">29th</option>
                <option <?php echo $schoolDetails->disburseDate == 30 ? "selected" : ""; ?> value="30">30th</option>
                </select>
              </div>
            </div>
            <div class="form-group row salaryOption">
              <label class="col-sm-6 col-form-label" for=""> Who to Notify(SMS) after every month disbursement</label>
              <div class="col-sm-6">
              <select name="disbursePerson" class="form-control" id="revieverNotify">
                <option <?php echo $schoolDetails->disburseNumber == $schoolDetails->phone ? "selected" : "";?> value="System Administrator">System Administrator</option>
                <option <?php echo $schoolDetails->disburseNumber != $schoolDetails->phone ? "selected" : "";?> value="Other">Other(s)</option>
                </select>
              </div>
            </div>
            <div class="form-group row othersNumber" style="display:<?php echo $schoolDetails->disburseNumber != $schoolDetails->phone ? "flex" : "none";?>">
              <label class="col-sm-6 col-form-label" for=""> Other reciever's telephone</label>
              <div class="col-sm-6">
                <input name="disbursePhone" value="<?php echo $schoolDetails->disburseNumber; ?>" class="form-control" placeholder="E.g; 08188216769" type="text">
              </div>
            </div>
            <div class="form-group row salaryOption">
              <label class="col-sm-6 col-form-label" for=""> Deduct Lateness fee from salary before disbursement</label>
              <div class="col-sm-6">
              <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->deductLateness == 1 ? "checked" : "";?> name="deductLateness" type="checkbox">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row salaryOption">
              <label class="col-sm-6 col-form-label" for=""> Deduct Absent fee from salary before disbursement</label>
              <div class="col-sm-6">
              <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->deductAbsent == 1 ? "checked" : "";?> name="deductAbsent" type="checkbox">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset class="form-group">
            <legend><span>STUDENTS</span></legend>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Auto alert parents of student's outstanding fees</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->autoAlert == 1 ? "checked" : "";?> name="outstandingFeeAlert" type="checkbox" id="outstandingFeeAlert">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row whenToNotifyFee">
              <label class="col-sm-6 col-form-label" for=""> When to alert parents of student's outstanding fees</label>
              <div class="col-sm-6">
                <select name="alertPeriod" class="form-control">
                <option <?php echo $schoolDetails->alertTime == "Every Week" ? "selected" : "";?> value="Every Week">Every week</option>
                <option <?php echo $schoolDetails->alertTime == "Every 2 Weeks" ? "selected" : "";?> value="Every 2 Weeks">Every 2 Weeks</option>
                <option <?php echo $schoolDetails->alertTime == "Every Month" ? "selected" : "";?> value="Every Month">Every Month</option>
                <option <?php echo $schoolDetails->alertTime == "Every 2 Months" ? "selected" : "";?> value="Every 2 Months">Every 2 Months</option>
                <option <?php echo $schoolDetails->alertTime == "Every Term" ? "selected" : "";?> value="Every Term">Every Term</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Student fees can be paid Online (E-Payment)</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->onlinePayment == 1 ? "checked" : "";?> name="ePayment" type="checkbox" id="onlinePayment">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
            <div class="form-group row selectFeeBank" style="display:none">
              <label class="col-sm-6 col-form-label" for=""> Bank Account to receive students fees payments</label>
              <div class="col-sm-6">
                <select name="receivePayment" class="form-control" id="selectedBankOption">
                <option <?php echo $schoolDetails->disburseBank == $schoolDetails->bankName ? "selected" : "";?> value="Main">Main bank account (Above)</option>
                <option <?php echo $schoolDetails->disburseBank != $schoolDetails->bankName ? "selected" : "";?> value="Other">Other bank account</option>
                </select>
              </div>
            </div>
            <div class="form-group row contactSupport" style="display:none">
              <div class="col-sm-12">
              <label class="col-form-label" style="margin-top:-15px; color:#FB5574" for=""> Please contact us for this change on <span style="font-weight:700">+2348188216769</span> or email us at <span style="font-weight:700">help@classworks.xyz</span></label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-6 col-form-label" for=""> Student fees can be paid Offline (CASH)</label>
              <div class="col-sm-6">
                <div class="c-toggle-btn">
                <input <?php echo $schoolDetails->offlinePayment == 1 ? "checked" : "";?> name="offlinePayment" type="checkbox">
                <div>
                    <label class="on">On</label>
                    <label class="off">Off</label>
                    <span class="c-toggle-thumb"></span>
                </div>
                </div>
              </div>
            </div>
          </fieldset>
          <div class="form-buttons-w">
            <input type="submit" class="btn btn-primary" value=" Save payment settings" name="savePaymentSettings">
          </div>
        </form>


</div>
</div>
</div>