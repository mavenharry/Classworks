<?php include_once ("staffbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Employee</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $staffClass->getSchoolDetails();
if(isset($_GET["subject_id"])){
$allStaff = $staffClass->fetchAllSubjectStaff($_GET["subject_id"]);
}else{
    if(isset($_POST["searchStaff"])){
        @$sortClass = htmlentities(trim($_POST["sortClass"]));
        @$sortSubject = htmlentities(trim($_POST["sortSubject"]));
        @$searchKeyword = htmlentities(trim($_POST["searchKeyword"]));
        $allStaff = $staffClass->sortStaff($sortClass, $sortSubject, $searchKeyword);
    }else{
        $allStaff = $staffClass->fetchAllStaff();
    }
}

?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Staff", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

                
        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/topbar.php") ?>
        <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#newStaffModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Add a new staff</span>
                                    </a>
                                    <a data-target="#newDeleteModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete a staff</span>
                                    </a>
                                    <a data-target="#searchStaffsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><?php if(isset($_GET['subject_id'])){echo "ALL ".substr(strtoupper($staffClass->getClassName($_GET['class_id'])),0,15)." ".substr(strtoupper($staffClass->getSubjectName($_GET['subject_id'])), 0, 10)." TUTOR(S)";}else{ echo "ALL STAFF(S)"; } ?></h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Staff number</th>
                                                <th>Staff name</th>
                                                <th>Telephone</th>
                                                <th>Department</th>
                                                <th class="text-center">Timetable</th>
                                                <th class="text-center">Profile</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allStaff) == 0){
                                                    echo "No staff in record";
                                                }else{
                                                    for($i = 0; $i < count($allStaff); $i++){ ?>
                                                <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i + 1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <?php if(!empty($allStaff[$i]->profilePhoto)){ ?>
                                                        <div class="user-with-avatar">
                                                        <img style="border: 2px solid #8095A0;" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$allStaff[$i]->profilePhoto;?>">
                                                        </div>
                                                        <?php }else{ ?>
                                                            <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                            class="badge"><?php echo $nameInitials = $staffClass->createAcronym($allStaff[$i]->fullname); ?></a>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStaff[$i]->staff_number; ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucwords($allStaff[$i]->fullname); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStaff[$i]->phone; ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allStaff[$i]->department); ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../timetable/staff_timetable?staff=<?php echo $allStaff[$i]->staff_number; ?>&session=<?php echo $schoolDetails->academic_session; ?>&term=<?php echo $schoolDetails->academic_term;?>">View timetable</a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../staffs/profile?staff_id=<?php echo base64_encode($allStaff[$i]->staff_number); ?>">View profile</a>
                                                    </td>
                                                </tr>
                                                <?php } 
                                                }?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                        if(count($allStaff) > 20){ ?>
                                        <div style="float: right;" class-"element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>