<?php
    include_once ("staffbtn.php");
    if(!isset($_GET["staff_id"])){
        echo "<script>window.location.assign('../authentication')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<?php
$staffNumber = base64_decode($_GET["staff_id"]);
$schoolDetails = $staffClass->getSchoolDetails();
$staffDetails = $staffClass->getSingleStaffDetails($staffNumber);
$allSubject = $staffClass->getAllSubjects();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <?php
                if($_SESSION["role"] == "tutor" || $_SESSION["role"] == "form teacher"){ ?>
                    <!-------------------- START - Main Menu -------------------->
                    <?php include("../../includes/sidebar2.php") ?>
                    <!-------------------- END - Main Menu -------------------->
                <?php }else{ ?>
                    <!-------------------- START - Main Menu -------------------->
                    <?php include("../../includes/sidebar.php");
                        if(!in_array("Staff", $allowedPages)){
                            echo "<script>window.location.assign('../../error')</script>";
                        }
                    ?>
                    <!-------------------- END - Main Menu -------------------->
                <?php }
            ?>

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

            <br>

            <!--START - Profile-->
<div class="content-i" style="margin-top:-30px;">
<div class="content-box">
<div class="row">
    <div style="margin-left:13px; margin-bottom:20px; margin-top:-8px; float:right" class="element-actions">
    <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){ ?>
        <a style="color:#FFFFFF; margin-top:10px; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="../timetable/staff_timetable?staff=<?php echo $staffNumber; ?>&session=<?php echo $schoolDetails->academic_session; ?>&term=<?php echo $schoolDetails->academic_term;?>">
            <i class="os-icon os-icon-agenda-1"></i>
            <span style="font-size: .9rem;">View Timetable</span>
        </a>
    <?php } ?>
    <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){ ?>
        <a data-target=".bd-example-modal-sm" data-id="<?php echo $staffNumber; ?>" id="delStaff" data-toggle="modal" style="color:#FFFFFF; margin-top:10px; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-ui-15"></i>
            <span style="font-size: .9rem;">Delete staff</span>
        </a>
        <a data-target="" data-toggle="modal" style="margin-top:10px; color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-mail-01"></i>
            <span style="font-size: .9rem;">Send Quick Notice</span>
        </a>
        <a style="margin-top:10px; color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-cv-2"></i>
            <span style="font-size: .9rem;">Print ID Card</span>
        </a>
    <?php } ?>
        <a data-target="#changeStaffPasswordModal" data-toggle="modal" style="margin-top:10px; color:#FFFFFF; background-color:#FB5574; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-cv-2"></i>
            <span style="font-size: .9rem;">Change Password</span>
        </a>
    </div>
    </div>
    <div class="row">
   <div class="col-sm-6">
      <div class="user-profile compact">
        <div class="profile-tile" style="padding-top:20px; padding-left:30px; border-top-right-radius:10px; border-top-left-radius:10px; background-color:#FFFFFF">
                <div class="my_hover_up pt-avatar-w" style="border-radius:0px;">
                    <?php if(!empty($staffDetails->profilePhoto)){ ?>
                    <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="<?php echo $schoolDetails->pathToPassport."/".$staffDetails->profilePhoto; ?>">
                    <?php }else { ?>
                        <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="../../images/male-user-profile-picture.png">            
                    <?php } ?>
                </div>
                <div class="profile-tile-meta">
                    <ul style="text-transform:capitalize">
                        <li style="color:#8095A0;">Name: <strong style="color:#8095A0; font-weight:600"><?php echo $staffDetails->fullname; ?></strong></li>
                        <li style="color:#8095A0">Staff Number: <strong style="color:#8095A0; font-weight:600"><?php echo $staffDetails->staff_number; ?></strong></li>
                        <li style="color:#8095A0">Department: <strong style="color:#8095A0; font-weight:600"><?php echo $staffDetails->department; ?></strong></li>
                        <li style="color:#8095A0">Gender: <strong style="color:#8095A0; font-weight:600"><?php echo $staffDetails->gender; ?></strong></li>
                        <li style="color:#8095A0">Date added: <strong style="color:#8095A0; font-weight:600"><?php echo date("d-m-Y", strtotime($staffDetails->date_added)); ?></strong></li>
                    </ul>
                </div>
            </div>

         <div class="up-contents">
            <div class="m-b">
               <div class="row m-b">
                  <div class="col-sm-6 b-r b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#1BB4AD"><?php echo count($staffClass->getTimeSheetStaff($staffNumber, "none", "2")) ?></div>
                        <div class="label" style="font-weight:600; color:#8095A0">LESSONS/WEEK</div>
                     </div>
                  </div>
                  <div class="col-sm-6 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#FE650B"><?php echo count($staffClass->getStaffSubject($staffNumber)) ?></div>
                        <div class="label" style="font-weight:600; color:#8095A0">SUBJECTS ASSIGNED</div>
                     </div>
                  </div>
               </div>
               <div class="padded" style="margin-top:-10px;">

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Overall Attendance</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#1BB4AD">Good</span></div>
                     </div>
                    <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 30%; background-color:#1BB4AD"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Behavioural Acts</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#974A6D">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 65%; background-color:#974A6D"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Mental Wellness</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#FE650B">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 90%; background-color:#FE650B"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Physical Health</div>
                        <div class="bar-label-right"><span class="info" style="color:#08ACF0">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 30%; background-color:#08ACF0"></div>
                    </div>
                  </div>

                  <div class="os-progress-bar primary">
                     <div class="bar-labels">
                        <div class="bar-label-left"><span>Punctuality</span></div>
                        <div class="bar-label-right"><span class="info" style="color:#FB5574">Good</span></div>
                     </div>
                     <div class="bar-level-2" style="width: 100%; background-color:#D4D4D9">
                        <div class="bar-level-3" style="width: 10%; background-color:#FB5574"></div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="element-wrapper" style="margin-top: 1rem;">
        <div class="element-box">
    
            <!-- SUBJECTS -->
            <?php
                $staffSubject = $staffClass->getStaffSubject($staffNumber);
                $staffSubjectCodes = array();
                foreach($staffSubject as $key => $subject){
                    $staffSubjectCodes[] = $subject->subjectCode;
                }
            ?>
            <form id="formValidate" method="POST">
            <div class="form-group" style="margin-top:0px;">
            <input type="hidden" value="<?php echo $staffNumber; ?>" name="staffNumber">
            <legend style="color:#FB5574; margin-top:30px;"><span>SUBJECT(S)</span></legend>
            <select class="form-control select2" multiple="true" name="staffNewSubs[]">
                <?php
                    foreach($allSubject as $key => $value){ ?>
                        <option <?php echo in_array($value->subjectCode, $staffSubjectCodes) ? "selected" : ""; ?> value="<?php echo $value->subjectCode; ?>"><?php echo ucfirst($value->subjectName)." - ".ucfirst($value->subjectClass);?></option>
                <?php } ?>
            </select>           
            </div>

            <!-- TIMESHEETS -->
            <legend style="margin-top:30px;"><span>TIME SHEET(S)</span></legend>
            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#1BB4AD" for="">Monday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day001", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day001", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day001", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day001", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#974A6D" for="">Tuesday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day002", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day002", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day002", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day002", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day002", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day002", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day002", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day002", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#FE650B" for="">Wednesday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day003", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day003", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day003", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day003", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day003", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day003", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day003", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day003", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#08ACF0" for="">Thursday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day004", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day004", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day004", "1") == "lunch" ||  $staffClass->getTimeSheetStaff($staffNumber, "day004", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day004", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day004", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day004", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day004", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#1BB4AD" for="">Friday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day005", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day005", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day005", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day005", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day005", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day005", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day005", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day005", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>

            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#974A6D" for="">Saturday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day006", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day006", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day006", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day006", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day006", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day006", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day006", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day006", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>


            <div class="form-group" style="margin-top:-10px;">
            <label style="color:#F4B510" for="">Sunday</label>
            <select class="form-control select2" multiple="true">
            <?php
                for($i = 0; $i < count($staffClass->getTimeSheetStaff($staffNumber, "day007", "1")); $i++){
                    if($staffClass->getTimeSheetStaff($staffNumber, "day007", "1") == "sports" || $staffClass->getTimeSheetStaff($staffNumber, "day007", "1") == "lunch" || $staffClass->getTimeSheetStaff($staffNumber, "day007", "1") == "break" || $staffClass->getTimeSheetStaff($staffNumber, "day007", "1") == "devotion"){
                    ?>
                    <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day007", "1")[$i]->timeName)." (".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day007", "1")[$i]->activity).")"; ?></option>
            <?php }else{ ?>
                <option selected="true" value=""><?php echo strtoupper($staffClass->getTimeSheetStaff($staffNumber, "day007", "1")[$i]->timeName)." - ".ucwords($staffClass->getTimeSheetStaff($staffNumber, "day001", "1")[$i]->className); ?></option>                        
            <?php }} ?>
            </select>
            </div>
            <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I agree to terms and conditions</label></div>
            <div class="form-buttons-w" style="margin-top:15px; "><input name="updateStaffSubject" type="submit" class="btn btn-primary" value=" Update Subjects"></div>
            <!-- <div class="form-buttons-w" style="margin-top:15px; "><button class="btn btn-primary" type="submit"> Update Timesheet</button></div> -->
        </form>            
        </div>

        
        <!-- PARENTS/GUARDIANS -->
        <div class="element-wrapper" style="margin-top: 1rem;">
        <div class="element-box">
        
            <div class="form-group" style="margin-top:0px;">

            <legend style="margin-top:0px;"><span>AMOUNT(S) DEDUCTED</span></legend>
                
            <div class="form-group" style="margin-top:-10px;">
                <h3 for="">&#8358;0</h3>
                <h6 style="font-style:italic; font-weight:normal">Absent Fee</h6>
            </div>

            <div class="form-group" style="margin-top:20px;">
                <h3 for="">&#8358;0</h3>
                <h6 style="font-style:italic; font-weight:normal">Lateness Fee</h6>
            </div>
        </div>
        </div>
        </div>                


      </div>
   </div>
   
   <div class="col-sm-6">
      <div class="element-wrapper">
    
      <!-- STAFF BIO DATA -->

      <div class="element-box" >
            <form id="formValidate" method="POST" action="" enctype="multipart/form-data">
               <div class="element-info">
                  <div class="element-info-with-icon">
                     <div class="element-info-icon">
                        <div class="os-icon os-icon-user"></div>
                     </div>
                     <div class="element-info-text">
                        <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Academic Staff Bio Data</h5>
                     </div>
                  </div>
               </div>
               <div class="row" style="margin-top:-10px; margin-bottom:20px;">
                <div class="pt-avatar-w" style="border-radius:0px;">
                    <img alt="" id="studentuploadPreview2" style="margin-left:20px; border-radius:5px; border: 1px solid #08ACF0; height:100px; width:90px" src="../../images/male-user-profile-picture.png">
                </div>
               <div class="form-group col-sm-7" style="margin-left:20px;">
                  <label for="studentupload-photo2">Change staff passport</label>
                  <label style="cursor:pointer" class="btn btn-primary" for="studentupload-photo2"> Upload Image</label>
                  <input id="studentupload-photo2" name="profilePhoto" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1; " class="form-control" type="file">
                  <input type="hidden" name="profileChecker" value="<?php echo $staffDetails->profilePhoto; ?>" id="">
               </div>
               </div>
               <div class="form-group">
                  <label for=""> Staff's Registration No.</label><input name="staffNumber" class="form-control" readonly value="<?php echo $staffDetails->staff_number; ?>" type="text">
               </div>
               <div class="form-group">
                  <label for=""> Staff's Full name</label><input name="fullname" class="form-control" value="<?php echo ucwords($staffDetails->fullname); ?>" type="text">
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label><input class="form-control" value="<?php echo ucfirst($staffDetails->nationality); ?>" disabled type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province</label><input class="form-control" value="<?php echo ucfirst($staffDetails->state); ?>" disabled type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region</label><input class="form-control" value="<?php echo ucfirst($staffDetails->city); ?>" disabled type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control" name="gender">
                            <option <?php echo $staffDetails->gender == "male" ? "selected" : ""?> value="male">Male</option>
                            <option <?php echo $staffDetails->gender == "female" ? "selected" : ""?> value="female">Female</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for=""> Marital Status</label>
                  <select class="form-control" name="m_status" <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){echo ""; }else{ echo "readonly"; } ?> >
                    <?php
                        $marital_status = ["single", "married", "Divorced", "separated", "others"];
                        for($i = 0; $i < count($marital_status); $i++){ ?>
                            <option <?php echo $staffDetails->m_status == strtolower($marital_status[$i]) ? "selected" : ""; ?> value="<?php echo $marital_status[$i];?>"><?php echo $marital_status[$i]?></option>
                    <?php } ?>
                </select>
               </div>
               <div class="form-group">
                  <label for=""> Address</label><input name="address" class="form-control" value="<?php echo ucfirst($staffDetails->address); ?>" type="text">
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Email Address</label><input name="email" class="form-control" value="<?php echo $staffDetails->email; ?>" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Phone Number</label><input name="phone" class="form-control" value="<?php echo ucfirst($staffDetails->phone);?>" type="text">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for="">Department</label>
                <select class="form-control" name="department" <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){echo ""; }else{ echo "readonly"; } ?> >
                    <?php
                        $departments = ["tutor", "form teacher", "accountant", "librarian", "security", "cleaner", "caretaker", "secretary", "caterer", "admin", "principal", "director", "ICT Officer"];
                        for($i = 0; $i < count($departments); $i++){ ?>
                            <option <?php echo $staffDetails->department == strtolower($departments[$i]) ? "selected" : ""; ?> value="<?php echo $departments[$i];?>"><?php echo $departments[$i]?></option>
                    <?php } ?>
                </select>
                </div>
                <div class="form-group">
                <label for="">Highest Academic Qualification</label>
                <select class="form-control" name="degree">
                    <?php
                        $qualification = ["SSCE", "OND", "HND", "B.ED", "B.SC", "B.ED", "B.ED", "B.ED", "M.ED", "M.SC", "M.ED", "M.ED", "M.ED", "B.ED", "M.ED", "PGD", "PHD", "DBA", "Others"];
                        for($i = 0; $i < count($qualification); $i++){ ?>
                            <option <?php echo strtolower($staffDetails->highest_qualification) == strtolower($qualification[$i]) ? "selected" : ""; ?> value="<?php echo $qualification[$i];?>"><?php echo $qualification[$i]?></option>
                    <?php }
                    ?>
                </select>
                </div>
                <div class="form-group">
                <label for=""> Bank Name</label>
                <select class="form-control" name="bank_name">
                    <?php
                    //CHECK IF COUNTRY IS NIGERIA, KENYA OR GHANA
                    $countries = array("nigeria", "kenya", "ghana");
                    $country = strtolower("nigeria");
                    if(in_array($country, $countries)){
                    $json = file_get_contents('../../includes/'.$country.'_banks.json');
                    $json_data = json_decode($json,true);
                    foreach ($json_data as $key => $value) { ?>
                        <option <?php echo $schoolDetails->bankName == $json_data[$key]["name"] ? "selected" : "";?> value="<?php echo $json_data[$key]["name"]?>"><?php echo strtoupper($json_data[$key]["name"]) ?></option>
                    <?php }
                    }else{ ?>
                        <option value="">Your country is not available yet!</option>
                    <?php }?>
                </select>
                </div>
                <div class="form-group">
                <label for=""> Bank Account Name</label>
                <input name="account_name" placeHolder="Micheal Amadi" class="form-control" value="" type="text">
                </div>
                <div class="form-group">
                <label for=""> Bank Account Number</label>
                <input name="account_number" placeholder="30784563244" class="form-control" value="" type="text">
                </div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I agree to terms and conditions</label></div>
                <div class="form-buttons-w" style="margin-top:15px; "><input name="bioData" type="submit" class="btn btn-primary" value=" Update Bio Data"></div>
            </form>
         </div>

         
        <!-- SETTINGS -->
        <div class="element-box" style="margin-top: 1rem;">
            <form id="formValidate" method="POST" action="#">
            <div class="element-info">
                <div class="element-info-with-icon">
                    <div class="element-info-icon">
                    <div class="os-icon os-icon-ui-46"></div>
                    </div>
                    <div class="element-info-text">
                    <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Staff Account Settings</h5>
                    </div>
                </div>
            </div>
            <input type="hidden" name="staffNumber" value="<?php echo $staffNumber; ?>">
            
            <div class="form-group">
            <label for="">Staff portal access? <div class="value badge badge-pill badge-<?php echo $staffDetails->staff_portal == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $staffDetails->staff_portal == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="staffPortalAccess" <?php if($_SESSION["role"] == "admin" || $_SESSION["role"] == "accountant"){echo ""; }else{ echo "disabled"; } ?> >
                <option value="1" <?php echo $staffDetails->staff_portal == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $staffDetails->staff_portal == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-buttons-w" style="margin-top:15px; "><input type="submit" name="saveSettings" value=" Update Settings" class="btn btn-primary" ></div>
            </form>
            </div>

      </div>
   </div>
</div>


</div><!--------------------
   START - Sidebar
   -------------------->
<!--------------------
   END - Sidebar
   -------------------->


            <!--END - Profile-->


<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->             

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>