<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class staffClass extends logic {
    
    function fetchAllStaff(){
        $stmt = $this->uconn->prepare("SELECT * FROM teachers ORDER BY staff_number ASC");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $staffArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->staff_number = $row["staff_number"];
            $obj->fullname = $row["fullname"];
            $obj->phone = $row["phone"];
            $obj->department = $row["department"];
            $obj->profilePhoto = $row["profilePhoto"];
            $staffArray[] = $obj;
        }
        $stmt->close();
        return $staffArray;
    }
    
    function sortStaff($sortClass, $sortSubject, $searchKeyword){
        $staffArray = array();
        if(!empty($sortClass) && !empty($sortSubject) && !empty($searchKeyword)){
            // ALL 3 PARAMETER SET...
            if($sortClass == "All"){
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode IN (SELECT class_code FROM classes) AND subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
                $stmt->bind_param("sss", $sortSubject, $searchKeyword, $searchKeyword);
            }else{
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode = ? AND subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
                $stmt->bind_param("ssss", $sortClass, $sortSubject, $searchKeyword, $searchKeyword);
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
            
        }
        elseif (!empty($sortClass) && !empty($sortSubject) && empty($searchKeyword)) {
            # NO KEYWORD...
            if($sortClass == "All"){
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode IN (SELECT class_code FROM classes) AND subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number GROUP BY teachers.staff_number");
                $stmt->bind_param("s", $sortSubject);
            }else{
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode = ? AND subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number GROUP BY teachers.staff_number");
                $stmt->bind_param("ss", $sortClass, $sortSubject);
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
            
        }
        elseif (!empty($sortClass) && empty($sortSubject) && empty($searchKeyword)) {
            # ONLY CLASS SET...
            if($sortClass == "All"){
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode IN (SELECT class_code FROM classes) AND subjectTeachers.staffCode = teachers.staff_number GROUP BY teachers.staff_number");
            }else{
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode = ? AND subjectTeachers.staffCode = teachers.staff_number GROUP BY teachers.staff_number");
                $stmt->bind_param("s", $sortClass);
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
            
        }
        elseif (empty($sortClass) && !empty($sortSubject) && empty($searchKeyword)){
            #ONLY SUBJECT IS SET
            $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number GROUP BY teachers.staff_number");
            $stmt->bind_param("s", $sortSubject);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
            
        }
        elseif (empty($sortClass) && empty($sortSubject) && !empty($searchKeyword)){
            // ONLY KEYWORD...
            $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
            $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
        }
        elseif (!empty($sortClass) && empty($sortSubject) && !empty($searchKeyword)) {
            # code...
            if($sortClass == "All"){
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode IN (SELECT class_code FROM classes) AND subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
                $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
            }else{
                $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.classCode = ? AND subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
                $stmt->bind_param("sss", $sortClass, $searchKeyword, $searchKeyword);
            }
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
            
        }
        elseif (empty($sortClass) && !empty($sortSubject) && !empty($searchKeyword)) {
            $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.staff_number, teachers.profilePhoto, teachers.phone, teachers.department, subjectTeachers.id FROM teachers INNER JOIN subjectTeachers ON subjectTeachers.subjectCode = ? AND subjectTeachers.staffCode = teachers.staff_number WHERE teachers.staff_number = ? OR teachers.fullname LIKE CONCAT('%', ?, '%') GROUP BY teachers.staff_number");
            $stmt->bind_param("sss", $sortSubject, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->staff_number = $row["staff_number"];
                    $obj->fullname = $row["fullname"];
                    $obj->phone = $row["phone"];
                    $obj->department = $row["department"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $staffArray[] = $obj;
                }
                $stmt->close();
                return $staffArray;
            }else{
                $stmt->close();
                return $staffArray;
            }
        }
    }


    function updateBioData($staffNumber, $fullname, $m_status, $address, $email, $phone, $department, $degree, $bank_name, $account_name, $account_number, $gender, $filename){
        $stmt = $this->uconn->prepare("UPDATE teachers SET fullname = ?, m_status = ?, address = ?, email = ?, phone = ?, department = ?, highest_qualification = ?, bank_name = ?, account_name = ?, account_number = ?, profilePhoto = ?, gender = ? WHERE staff_number = ?");
        $stmt->bind_param("sssssssssssss", $fullname, $m_status, $address, $email, $phone, $department, $degree, $bank_name, $account_name, $account_number, $filename, $gender, $staffNumber);
        if($stmt->execute()){
            $stmt = $this->dconn->prepare("UPDATE staffs SET fullname = ?, email = ?, role = ? WHERE uniqueNumber = ?");
            $stmt->bind_param("ssss", $fullname, $email, $department, $staffNumber);
            $stmt->execute();
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function updateAccess($staffNumber, $staffPortalAccess){
        $stmt = $this->uconn->prepare("UPDATE teachers SET staff_portal = ? WHERE staff_number = ?");
        $stmt->bind_param("is", $staffPortalAccess, $staffNumber);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }

    function newStaffSubject($staffNumber, $staffNewSubs){
        $subjectToDelete = array();
        $subjectToAdd = array();
        $staffSubjectCodes = array();
        $staffSubjects = $this->getStaffSubject($staffNumber);
        foreach($staffSubjects as $key => $subject){
            $staffSubjectCodes[] = $subject->subjectCode;
        }
        foreach($staffNewSubs as $key => $subject){
            if(!in_array($subject, $staffSubjectCodes)){
                $subjectToAdd[] = $subject;
            }
        }
        foreach($staffSubjectCodes as $key => $subject){
            if(!in_array($subject, $staffNewSubs)){
                $subjectToDelete[] = $subject;
            }
        }
        // ADD NEW SUBJECT
        $date = date("Y-m-d");
        $schoolDetails = $this->getSchoolDetails();
        $term = $schoolDetails->academic_term;
        $session = $schoolDetails->academic_session;
        foreach($subjectToAdd as $key => $value){
            $stmt = $this->uconn->prepare("SELECT classes.class_code FROM classes INNER JOIN subjects ON classes.class_name = subjects.subjectClass WHERE subjects.subjectCode = ?");
            $stmt->bind_param("s", $value);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $classCode = array_shift($stmt_result)["class_code"];
            $stmt = $this->uconn->prepare("INSERT INTO subjectTeachers (classCode, subjectCode, staffCode, date_assigned) VALUES (?,?,?,?)");
            $stmt->bind_param("ssss", $classCode, $value, $staffNumber, $date);
            $stmt->execute();
            $stmt = $this->uconn->prepare("UPDATE assessments SET tutor = ? WHERE subject_code = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssi", $staffNumber, $value, $session, $term);
            $stmt->execute();
        }
        // SUBJECT TO DELETE
        foreach($subjectToDelete as $key => $value){
            $stmt = $this->uconn->prepare("DELETE FROM subjectTeachers WHERE subjectCode = ? AND staffCode = ?");
            $stmt->bind_param("ss", $value, $staffNumber);
            $stmt->execute();
            $stmt = $this->uconn->prepare("DELETE FROM schedule_timings WHERE timeSubject = ? AND timeTutor = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssi", $value, $staffNumber, $session, $term);
            $stmt->execute();
            $staffNumber = "";
            $stmt = $this->uconn->prepare("UPDATE assessments SET tutor = ? WHERE subject_code = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("sssi", $staffNumber, $value, $session, $term);
            $stmt->execute();
        }
        $stmt->close();
        return true;
    }

}
?>