<?php include_once ("staffbtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Staff</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>

<!-------------------- START - MODAL -------------------->
<?php
$schoolDetails = $staffClass->getSchoolDetails();
include ("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
<?php
$allStaff = $staffClass->fetchAllStaff();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Staff", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">STAFF MANAGEMENT AND ANALYSIS</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./admin_staffs.php">
                                                    <div class="label">No. of all current staffs</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo count($allStaff) ?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Staffs currently on leave</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-signs-11"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis">0</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#">
                                                    <div class="label">Incoming leave request(s)</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-mail"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis">0</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="#">
                                                    <div class="label">Query/Suspend a staff</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-thumbs-down"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="margin-top:-10px;">Query a staff</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a data-target="#staffIdcardSelector" data-toggle="modal" class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="#">
                                                    <div class="label">View staffs school Cards</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-cv-2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Staff ID Cards</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a data-target="#newStaffModal" data-toggle="modal" class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="#">
                                                    <div class="label">Add New/Existing staffs</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Add a Staff</div>
                                                </a>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
<?php include_once ("../../includes/support.php"); ?>

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>