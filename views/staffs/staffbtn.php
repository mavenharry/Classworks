<?php
include_once ("staffClass.php");
$staffClass = new staffClass();

if(isset($_POST["bioData"])){
    $staffNumber = htmlentities(trim($_POST["staffNumber"]));
    $fullname = htmlentities(trim($_POST["fullname"]));
    $m_status = htmlentities(trim($_POST["m_status"]));
    $address = htmlentities(trim($_POST["address"]));
    $gender = htmlentities(trim($_POST["gender"]));
    $email = htmlentities(trim($_POST["email"]));
    $phone = htmlentities(trim($_POST["phone"]));
    $department = htmlentities(trim($_POST["department"]));
    $degree = htmlentities(trim($_POST["degree"]));
    $bank_name = htmlentities(trim($_POST["bank_name"]));
    $account_name = htmlentities(trim($_POST["account_name"]));
    $account_number = htmlentities(trim($_POST["account_number"]));
    $filename = htmlentities(trim($_POST["profileChecker"]));

    // MOVE STUDENT PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $staffClass->getSchoolDetails()->pathToPassport;
            $filename = strtolower(str_replace("/","_",$staffNumber).".".$extension);
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }

    $reply = $staffClass->updateBioData($staffNumber, $fullname, $m_status, $address, $email, $phone, $department, $degree, $bank_name, $account_name, $account_number, $gender, $filename);
    if($reply === true){
        $mess = "Staff Bio Data Updated";
    }else{
        $err = "An error occurred while trying to update record. Please try again later";
    }  
}

if(isset($_POST["saveSettings"])){
    $staffPortalAccess = htmlentities(trim($_POST["staffPortalAccess"]));
    $staffNumber = htmlentities(trim($_POST["staffNumber"]));
    $reply = $staffClass->updateAccess($staffNumber, $staffPortalAccess);
    if($reply === true){
        $mess = "Staff portal access has been Updated";
    }else{
        $err = "An error occurred while trying to update record. Please try again later";
    }  
}

if(isset($_POST["updateStaffSubject"])){
    $staffNewSubs = $_POST["staffNewSubs"];
    $staffNumber = htmlentities(trim($_POST["staffNumber"]));
    $reply = $staffClass->newStaffSubject($staffNumber, $staffNewSubs);
    if($reply === true){
        $mess = "Staff Subjects Updated successfully";
    }else{
        $err = "An error occurred while trying to perform this action";
    }
}
?>