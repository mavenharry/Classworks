<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title>ClassWorks</title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="fonts/flaticon.css" />
    
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">


</head>

<body>
    <div class="boxed_wrapper">

        <div class="header-top">
            <div class="container clearfix">
                <div class="top-left pull-left">
                    <ul class="links-nav clearfix">
                        <li><a href="#"><span class="fa fa-phone"></span> Call:  234 4561 5523 </a>
                        </li>
                        <li><a href="#"><span class="fa fa-envelope"></span>Email:  info@classworks.xyz</a>
                        </li>
                    </ul>
                </div>

               
            </div>
        </div>
        <!-- Header Top End -->

        <section class="mainmenu-area stricky">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="main-logo">
                            <a href="index-2.html"><img src="images/logo/logo.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-5 menu-column">
                        <nav class="main-menu">
                            <div class="navbar-header">     
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">

                                    <li class="current"><a href="index.html">Home</a>
                                    </li>
                                    <li><a href="#about">About</a>
                                    </li>
                                    <li><a href="#contact">contact us</a>
                                    </li>
                                   
                                </ul>

                                <ul class="mobile-menu clearfix">

                                    <li class="current"><a href="index.html">Home</a>
                                </li>
                                <li><a href="#about">About</a>
                                </li>
                                
                                <li><a href="#contact">contact us</a>
                                </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="col-md-2">
                        <div class="right-area pull-right" style="margin-top: 20px;">
                            
                                <a class="thm-btn yellow-bg" href="login.php">Portal</a>
                            </div>
                           
                       
                    </div>
                </div>
                
            </div>
        </section>



       

        <!--Start rev slider wrapper-->
        <section class="rev_slider_wrapper">
            <div id="slider1" class="rev_slider" data-version="5.0">
                <ul>

                    <li data-transition="fade">
                        <img src="images/slider/1.jpg" alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">


                       

                    </li>
                    <li data-transition="fade">
                        <img src="images/slider/2.jpg" alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">


                       

                    </li>
                    <li data-transition="fade">
                        <img src="images/slider/3.jpg" alt="" width="1920" height="700" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">


                       

                    </li>


                </ul>
            </div>
        </section>
        
        <div>
        <section class="about" style="background-image:url(images/background/about-bg.jpg);" id="about">
            <div class="container">
                <div class="item-box">
                    <div class="row">
                        <div class="single-column col-md-6 col-sm-12">
                            <div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                                <div class="post-content">
                                    <div class="sec-title">
                                        <h2>Welcome to ClassWorks</h2>
                                    </div>
                                    <div class="text">
                                        <p>Cupidatat non proident sunt culpa qui officia deserunt mollit anim idest laborum sed ut perspiciatis unde omnis iste natuserror sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.</p>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="single-column about_carousel col-md-6 col-sm-12">
                           
                                <div class="single-item">
                                    <div class="img-holder">
                                        <img src="images/about/about.png" alt="Awesome Image" />
                                    </div>
                                </div>
                                
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>

		
		 <!--footer start-->
         <section id="contact">
        <footer class="footer bg-style">
            <div class="container">
                <div class="footer-upper">

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <a href="#">
                                    <img src="images/logo/logo.jpg" alt="Awesome Image" />
                                </a>
                                <p>School Automate system<br>School Automate system.</p>

                                
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                           
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget contact">
                                <h3 class="title">Get in Touch With us</h3>

                                <div class="widget-content">
                                    <p>School Automate system. </p>
                                    <ul class="contact-info">
                                        <li><span class="icon fa fa-home"></span>1201 park street, Fifth
                                            <br>Avenue,</li>
                                        <li><span class="icon fa fa-phone"></span>+234 657 524 332</li>
                                        <li><span class="icon fa fa-envelope"></span>info@ClassMates.xyz</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
               
            <div class="footer-bottom text-center">
                <div class="container">
                    <div class="copyright-text">Copyright © ClassWorks 2018. All Rights Reserved</div>
                </div>
            </div>
        </footer>
    </section>
        <!--footer end-->



        <!-- Scroll Top Button -->
        <button class="scroll-top tran3s color2_bg">
            <span class="fa fa-angle-up"></span>
        </button>
        <!-- pre loader  -->
        <div class="preloader"></div>

        <!-- jQuery js -->
        <script src="js/jquery.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- jQuery ui js -->
        <script src="js/jquery-ui.js"></script>
        <!-- owl carousel js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- jQuery validation -->
        <script src="js/jquery.validate.min.js"></script>
        <!-- google map -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script>
        <script src="js/gmap.js"></script>
        <!-- mixit up -->
        <script src="js/wow.js"></script>
        <script src="js/jquery.mixitup.min.js"></script>
        <script src="js/jquery.fitvids.js"></script>
        <script src="js/bootstrap-select.min.js"></script>
      

        <!-- revolution slider js -->
        <script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>

        <!-- fancy box -->
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.polyglot.language.switcher.js"></script>
        <script src="js/nouislider.js"></script>
        <script src="js/jquery.bootstrap-touchspin.js"></script>
        <script src="js/SmoothScroll.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/jquery.countTo.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/imagezoom.js"></script>
        <script src="js/validation.js"></script>
        <script id="map-script" src="js/default-map.js"></script>
        <script src="js/custom.js"></script>

    </div>

</body>


<!-- Mirrored from wp.hostlin.com/kindergarten/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 Jun 2018 14:36:02 GMT -->
</html>