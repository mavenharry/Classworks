<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- Page title -->
      <title>SchoolMate</title>
     <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <!-- Icon fonts -->
      <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="fonts/flaticons/flaticon.css" rel="stylesheet" type="text/css">
      <link href="fonts/glyphicons/bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
      <!-- Google fonts -->
      <link href='https://fonts.googleapis.com/css?family=Lato:400,700,800' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:700,900' rel='stylesheet' type='text/css'>
      <!-- Theme CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- Color Style CSS -->
      <link href="styles/funtime.css" rel="stylesheet">
      <!-- Owl Slider & Prettyphoto -->
      <link rel="stylesheet" href="css/owl.carousel.css">
      <link rel="stylesheet" href="css/prettyPhoto.css">
      <!-- LayerSlider stylesheet -->
      <link rel="stylesheet" href="layerslider/css/layerslider.css">
      <!-- Favicons-->
      <link rel="apple-touch-icon" sizes="72x72" href="../../apple-touch-icon-72x72.html">
      <link rel="apple-touch-icon" sizes="114x114" href="../../apple-touch-icon-114x114.html">
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <!-- Switcher Only -->
      <link rel="stylesheet" id="switcher-css" type="text/css" href="switcher/css/switcher.css" media="all" />

   </head>
   <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
      <!-- Page width 'Boxed' of 'Full' -->
      <div class="full">
         <!-- Preloader -->
         <div id="preloader">
            <div class="preloader">
               <span></span>
               <span></span>
               <span></span>
               <span></span>
               <span></span>
            </div>
         </div>
         	  
         <!-- Navbar -->
         <nav class="navbar navbar-custom navbar-fixed-top">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-brand-centered">
               <i class="fa fa-bars"></i>
               </button>
               <div class="navbar-brand-centered page-scroll">
                  <a href="#page-top"><img src="img/logo.png"  alt=""></a>
               </div>
            </div>
            <div class="collapse navbar-collapse" id="navbar-brand-centered">
               <div class="container">
                  <ul class="nav navbar-nav page-scroll navbar-left">
                     <li><a href="#page-top">Home</a></li>
                     
                     <li><a href="#about">About</a></li>
                  </ul>
                  <ul class="nav navbar-nav page-scroll navbar-right">
                     <li><a href="#contact">Contact</a></li>
                     <li class="dropdown">
                        <a href="login.php">Login To Portal</a>
                        
                     </li>
                  </ul>
               </div>
            </div>
            </nav>
         <div id="layerslider">
            <!-- Slide 1 -->
            <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
               <!-- Background image -->
               <img src="img/slide3.jpg" class="ls-bg"  alt="Slide background"/>
             </div>
             
            <!-- Slide 2 -->
            <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
               <!-- Background image -->
               <img src="img/slide2.jpg" class="ls-bg"  alt="Slide background"/>
              </div>

            <!-- Slide 3 -->
            <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
               <!-- Background image -->
               <img src="img/slide3.jpg" class="ls-bg"  alt="Slide background"/>
               </div>

            <!-- Slide 4 -->
            <div class="ls-slide" data-ls="transition2d:104;timeshift:-2000;">
               <!-- Background image -->
               <img src="img/slide4.jpg" class="ls-bg"  alt="Slide background"/>
               </div>
            
            </div>
         </div>

         <div class="hidden-xs container-fluid cloud-divider">
            <svg id="deco-clouds1" class="head" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
               <path d="M-5 100 Q 0 20 5 100 Z
                  M0 100 Q 5 0 10 100
                  M5 100 Q 10 30 15 100
                  M10 100 Q 15 10 20 100
                  M15 100 Q 20 30 25 100
                  M20 100 Q 25 -10 30 100
                  M25 100 Q 30 10 35 100
                  M30 100 Q 35 30 40 100
                  M35 100 Q 40 10 45 100
                  M40 100 Q 45 50 50 100
                  M45 100 Q 50 20 55 100
                  M50 100 Q 55 40 60 100
                  M55 100 Q 60 60 65 100
                  M60 100 Q 65 50 70 100
                  M65 100 Q 70 20 75 100
                  M70 100 Q 75 45 80 100
                  M75 100 Q 80 30 85 100
                  M80 100 Q 85 20 90 100
                  M85 100 Q 90 50 95 100
                  M90 100 Q 95 25 100 100
                  M95 100 Q 100 15 105 100 Z">
               </path>
            </svg>
         </div>

         <section id="about">
            <div class="container">
               <div class="col-lg-8 col-lg-offset-2">
                  <!-- Section heading -->
                  <div class="section-heading">
                     <h2>About us</h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12 col-lg-5">

                        
                     <!-- Carousel -->
                     
                        <div class="item">
                           <img class="img-responsive" src="img/about1.jpg" alt="">
                        </div>
                       
                  
                  </div>
                  <!-- text -->
                  <div class="col-lg-7 col-sm-12">
                     <h3>About Us</h3>
                     <p>SchoolMate is a school management system including many Features to run a school
                        sodales felis tiam non Doloreiur qui commolu ptatemp dolupta orem retibusam 
                        andigen Ibu lum orci eget, viverra elit aliquam erat volut pat phas ellus ac sodales felis tiam non metus.
                        Placerat a ultricies a, posuere lorem ipseut lincas psuiem t volut pat phas ellus ac sodales Lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis a nibh fusce mollis imperdie tlorem ipuset campas fincas interdum donec eget metus auguen unc vel mauris ultricies, vest ibulum orci eget usce mollis imperdiet interdum donec eget metus auguen unc vel lorem ispuet Ibu lum orci eget, viverra elit liquam erat volut pat phas ellus ac sodales Lorem ipsum dolor sit amet, consectetur adipisicing elit uasi quidem minus id omnis.
                     </p>
                     <p>Lorem ipsum dolor sit amet, lorem ipsuet lora consectetur adipisicing elit Illo itaque ipsum sit ha Bu lum orci eget, viverra elit aliquam erat volut pat phas ellus ac
                        dolupta orem retibusam qui commolu les felis tiam non metus ali quam eros Pellentesque turpis lectus, placerat a ultricies a, posuere a nibh. Fusce mollis imperdiet interdum. 
                     </p>
                  </div>
                  <!-- /col-lg-8 -->
               </div>
               <!-- /row -->
              
               
               </div>
               <!-- /col-lg-7 -->
            </div>
            <!--/container-->	  
         </section>
         <!--/ Section ends -->
                <!-- Footer -->	
         <div class="container-fluid cloud-divider">
            <!-- Clouds SVG Divider -->	
            <svg id="deco-clouds" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
               <path d="M-5 100 Q 0 20 5 100 Z
                  M0 100 Q 5 0 10 100
                  M5 100 Q 10 30 15 100
                  M10 100 Q 15 10 20 100
                  M15 100 Q 20 30 25 100
                  M20 100 Q 25 -10 30 100
                  M25 100 Q 30 10 35 100
                  M30 100 Q 35 30 40 100
                  M35 100 Q 40 10 45 100
                  M40 100 Q 45 50 50 100
                  M45 100 Q 50 20 55 100
                  M50 100 Q 55 40 60 100
                  M55 100 Q 60 60 65 100
                  M60 100 Q 65 50 70 100
                  M65 100 Q 70 20 75 100
                  M70 100 Q 75 45 80 100
                  M75 100 Q 80 30 85 100
                  M80 100 Q 85 20 90 100
                  M85 100 Q 90 50 95 100
                  M90 100 Q 95 25 100 100
                  M95 100 Q 100 15 105 100 Z">
               </path>
            </svg>
         </div>
         <footer id="contact">
            <div class="container-fluid">
               <!-- Newsletter -->
               <div class="col-lg-4 col-md-6 text-center res-margin">
                  <h6 class="text-light">Contact Us</h6>
                  <p>Phone .</p>
                  <p><i class="flaticon-back"></i><a href="mailto:youremailaddress">info@schoolmate.com</a></p>
                     <p><i class="fa fa-phone margin-icon"></i>Call us +1 456 7890</p>
                     <div id="map-canvas"></div>
                  
                  		            
               </div>
               <!-- /col-lg-4 -->
               <!-- Bottom Credits -->
               <div class="col-lg-4 col-md-6 res-margin">
                  <a href="#page-top"><img src="img/logo.png"  alt="" class="center-block"></a>
                  <!-- social-icons -->	
                 
               </div>
               <!-- /col-lg-4 -->
               <!-- Opening Hours -->
               <div class="col-lg-4 col-md-12 text-center">
                  <!-- Sign-->
                  <h6 class="text-light">Opening Hours:</h6>
                  <!-- Table-->
                  <table class="table">
                     <tbody>
                        <tr>
                           <td class="text-left">Monday to Friday</td>
                           <td class="text-right">7 a.m. to 7 p.m.</td>
                        </tr>
                        <tr>
                           <td class="text-left">Weekends / Holidays</td>
                           <td class="text-right"><span class="label label-danger">Closed</span></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <!-- /col-lg-4 -->
            </div>
            <!-- / container -->
            <hr>
            <p>Copyright ©  2018 / Designed by  <a href="#">SchoolMate</a></p>
            <!-- /container -->
            <!-- Go To Top Link -->
            <div class="page-scroll hidden-sm hidden-xs">
               <a href="#page-top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
            </div>
         </footer>
         <!-- /footer ends -->
      </div>
      <!-- /page width -->
      <!-- Core JavaScript Files -->
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <!-- Google maps -->
      <script src="https://maps.googleapis.com/maps/api/js?v=3"></script>
      <!-- Main Js -->
      <script src="js/main.js"></script>
      <!-- Isotope -->	  
      <script src="js/jquery.isotope.js"></script>
      <!--Mail Chimp validator -->
      <script src='js/mc-validate.js'></script>
      <!--Other Plugins -->
      <script src="js/plugins.js"></script>
      <!-- Contact -->
      <script src="js/contact.js"></script>
      <!-- Prefix free CSS -->
      <script src="js/prefixfree.js"></script>	  	  
      <!-- GreenSock -->
      <script src="layerslider/js/greensock.js" ></script>
      <!-- LayerSlider script files -->
      <script src="layerslider/js/layerslider.transitions.js" ></script>
      <script src="layerslider/js/layerslider.kreaturamedia.jquery.js" ></script>
      <!-- Swicther -->
      <script src="switcher/js/dmss.js"></script>
   </body>
   </html>