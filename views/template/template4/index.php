<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SCHOOL MATE</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="css/common-style.css">
    <link rel="stylesheet" href="css/responsive.css">

</head>
<body class="home-one">
<div class="preloader"></div>
<header class="header clearfix">
    <div class="main-header stricky bubble">
        <div class="container">
            <div class="logo pull-left">
                <a href="index.html">
                    <img src="images/schlogo.png" alt="ClassWorks">
                </a>
            </div>

            <div class="nav-outer">
                <div class="header-top">
                    
                    <div class="register">
                        <a href="login" >Login to portal</a>
                        
                    </div>

                    <div id="polyglotLanguageSwitcher">
                        <div class="contact">
                            <span class="icon fa fa-phone"></span>Call :  +(234) 123 456 7890
                        </div>
                    </div>
                   
                <nav class="mainmenu-area">
                    <div class="navbar" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                        </div>

                        <div class="navbar-collapse collapse text-center">  
                            <ul>
                                <li class="current dropdown">
                                    <a href="index.html">Home</a>
                                    </li>
                                <li>
                                    <a href="#about">About</a>
                                     </li>
                                <li><a href="#contact">Contact</a></li>
                                <li>
                                    <a href="login">Portal</a>
                                </li>
                            </ul>  
                        </div> 
                    </div>
                </nav>
            </div>

        </div>
    </div>
</header>



<section class="rev_slider_wrapper">
    <h2 class="hidden">rev slider</h2>
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
            <li class="gradient-overlay" data-transition="slotzoom-horizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                <img src="images/1.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 

            <li class="gradient-overlay overly" data-transition="slotzoom-horizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                <img src="images/2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                
                
                
            <li class="gradient-overlay" data-transition="slotzoom-horizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                <img src="images/3.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                
                
               
        </ul>
    </div>
</section>



<section class="about" id="about">
    <div class="container">
                
        <div class="row">
            <div class="single-column col-md-6 col-sm-12">
                <div class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.5s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                    <div class="post-content">
                        <div class="section-title">
                            <h2>Welcome TO <span>School Mate</span> </h2>
                        </div>
                        <div class="text">
                            <p>We're a childcare centre with an engaging curriculum backed by qualified, experienced and passionate teachers! By learning about those of differing social, cultural and perspectives, young people </p>
                        </div>
                        
                    </div>
                </div>    
                    

            </div>
            <div class="single-column col-md-6 col-sm-12">  
                <div class="wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s" data-wow-offset="0" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                    <figure class="img-box">
                        <a href="#"><img src="images/1.png" alt=""></a>
                    </figure>
                </div> 
            </div>
        </div>
    </div>           
</section>



<section class="footer" id="contact">
    <div class="footer-upper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12 column">
                    <div class="item center">
                        <div class="icon">
                            <span class="icon-location"></span>
                        </div>
                        <div class="content">
                            <h4><a href="#">Address</a></h4>
                            <p>SchoolMate 09 First Floor Downtown <br> Port Harcourt</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 column">
                    <div class="item center">
                        <div class="icon">
                            <span class="icon-technology-2"></span>
                        </div>
                        <div class="content">
                            <h4><a href="#">Phone number</a></h4>
                            <p>+(234) 123 456 7890</p>
                            <p>+(234) 123 456 7893</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 column">
                    <div class="item center">
                        <div class="icon">
                            <span class="icon-interface"></span>
                        </div>
                        <div class="content">
                            <h4><a href="#">Email Address</a></h4>
                            <p>schoolmate@Support.com </p>
                            <p>school@info.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="footer-bottom">
        <div class="footer-bottom-bg">
            <div class="container">
                <div class="pull-left">
                    <figure><a href="#"><img src="images/schlogo.png" alt=""></a></figure>
                </div>
                <div class="pull-right">
                    <div class="menu">
                        <ul class="clearfix">
                            <li><a href="#">home</a></li>
                            <li><a href="#contact">contact us</a></li>
                            <li><a href="#about">About</a></li>
                            
                        </ul>
                    </div>
                    <div class="copy-right">
                        Copyright © <a href="mailto:support@schoolmate.ng"><span>SchoolMate</span></a> 2018
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>




        <!-- jQuery js -->
    <script src="assets/jquery/jquery-1.12.3.min.js"></script>
    <!-- bootstrap js -->
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery ui js -->
    <script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>

    <!-- wow js -->
    <script src="assets/wow.js"></script>

    <!-- owl carousel js -->
    <script src="assets/owl.carousel-2/owl.carousel.min.js"></script>              <!-- jquery.bxslider js -->
    <script src="assets/jquery.bxslider/jquery.bxslider.min.js"></script>
    <!-- jQuery validation -->
    <script src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- gmap.js helper -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script>
    <!-- gmap.js -->
    <script src="assets/gmaps.js"></script>
    
    <!-- mixit u -->
    <script src="assets/jquery.mixitup.min.js"></script>
    <script src="assets/isotope.pkgd.min.js"></script>
    <script src="assets/jquery.countdown.min.js"></script>
    <script src="assets/masterslider/masterslider.js"></script>
    <script src="assets/SmoothScroll.js"></script>

    <!-- revolution slider js -->
    <script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>

    <script src="assets/Polyglot-Language-Switcher-master/js/jquery.polyglot.language.switcher.js"></script>
    <script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="assets/scrollbar.js"></script>
    
    <script src="js/default-map-script.js"></script>
    <script src="js/script.js"></script>








</body>
</html>
