<!DOCTYPE html>
<html lang="en" >

<head>
<title>ClassWorks - School Automation Solution | Students</title>

<!-------------------- START - Meta -------------------->
<?php include("../../includes/meta.php") ?>
<!-------------------- END - Meta -------------------->
  
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css'>
<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Architects+Daughter|Roboto&subset=latin,devanagari'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>

<link rel="stylesheet" href="css/style.css">

  
</head>

<body>

  <body class="welcome">
  <!-- <span id="splash-overlay" class="splash"></span> -->
  <span id="welcome" class="z-depth-4"></span>
 
  <header class="navbar-fixed" style="background:#09acef"> 
    <nav class="row" style="background:#09acef">
      <div class="col s12">
        <ul class="left">
          <li class="right">
            <a href="" style="text-transform:uppercase; font-weight:bold;" target="_blank" class="">George International Academy</a>
          </li>
        </ul>
        <ul class="right">
          <li class="right">
            <a href="" target="_blank" class="">LOGOUT NOW</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <main class="valign-wrapper">
  <h2 id="hinter" style="position: absolute; font-weight:bold; top:70px; right:100px; color: #8095A0; font-size:1.5rem"></h2>
    <span style="width:100%; margin-left:150px;" class="container grey-text text-lighten-1 ">

      <div class="row">
      <img style="border-radius:100%;" src="http://www.informationng.com/wp-content/uploads/2017/06/acco1-600x472.jpg" height="150" width="150"/>
      <a href=""><img onmouseover="showHint('Students Chat Room')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIfuLHLnv_6rnCd5Es1vmgEZrMzdyMmUJ0fD8XiCUkF1ZRhE2-" height="150" width="150"/></a>      
      <a href=""><img onmouseover="showHint('Take a Quiz')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://cdn.freebiesupply.com/logos/large/2x/logosquiz-logo-png-transparent.png" height="150" width="150"/></a>
      <a href=""><img onmouseover="showHint('Read a Book')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://previews.123rf.com/images/iconicbestiary/iconicbestiary1602/iconicbestiary160200005/53122271-teacher-reading-big-book-to-a-young-little-students-flat-style-vector-illustration-isolated-on-white.jpg" height="150" width="150"/></a>
      <a href=""><img onmouseover="showHint('Talk with a Tutor')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://png.pngtree.com/element_origin_min_pic/16/12/10/277e5c245019622e32ef78adb05c099e.jpg" height="150" width="150"/></a>      
      <a href="#"><img onmouseover="showHint('Show More Options')" onmouseout="showHint('')" class="my_hover_up" id="moreBtn" onclick="showMore('1')" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://cdn3.iconfinder.com/data/icons/mobile-functions/154/phone-menu-512.png" height="150" width="150"/></a>
      <a href="#"><img onmouseover="showHint('Take me back home')" onmouseout="showHint('')" class="my_hover_up" id="closeBtn" onclick="showMore('2')" style="background:#09acef; display:none; margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="http://www.skf.com/binary/21-273584/Icon---right-size---04-login---login-3.jpg" height="150" width="150"/></a>      
      </div>

      <div id="moreModules" style="display:none; margin-top:20px">
      <a href=""><img onmouseover="showHint('Make Fee(s) Payment')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:0px; border: 3px dashed #09acef; border-radius:100%;" src="https://thumbs.dreamstime.com/b/education-cost-concept-invest-money-education-study-cash-education-cost-concept-invest-money-education-study-cash-tuition-103931184.jpg" height="150" width="150"/></a>      
      <a href=""><img onmouseover="showHint('View Lessons Schedule')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsWgbE1xaQCAgx-sisMUGRlt4bt3vIy93MsZpBPqW7ssPfKZy8" height="150" width="150"/></a>
      <a href=""><img onmouseover="showHint('View Grade Book')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://us.123rf.com/450wm/rastudio/rastudio1602/rastudio160204253/52688608-un-%C3%A9l%C3%A8ve-heureux-afro-am%C3%A9ricaine-tenant-une-feuille-avec-la-meilleure-note-sur-le-fond-de-vecteur-de-cla.jpg?ver=6" height="150" width="150"/></a>
      <a href=""><img onmouseover="showHint('Play a Game')" onmouseout="showHint('')" class="my_hover_up" style="margin-left:20px; border: 3px dashed #09acef; border-radius:100%;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGDhu9Da5mHIAFrCJVUdVIhBMLXIVG2xS-1b5uuBwAQKUrf8BA" height="150" width="150"/></a>      
      </div>

      <div id="studentInfo">
      <h1 style="margin-top:30px; color:#09acef" class="title text-lighten-3">Chukwuemeka Ogbonnaya</h1>
      <blockquote style="color:#8095A0; border-left: 5px solid #09acef;" class="flow-text">Grade 1 Student of Imperial College International, Lagos, Nigeria</blockquote>
      </div>

      </span>
  </main>

  <div class="fixed-action-btn">
    <a href="#message" style="background:#fff; margin-right: 50px;" class="modal-trigger btn btn-large btn-floating waves-effect waves-light">
      <i class="large material-icons" style="color:#039be5">message</i>
    </a>
  </div>

  <div id="message" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Contact</h4>
    </div>
    <div class="modal-footer">
      <a href="" class="modal-action modal-close waves-effect btn-flat">close</a>
    </div> 
  </div> 

  <footer class="page-footer" style="padding:0px; background:#09acef;">
    <div class="footer-copyright">
      <div class="container">
        <time>&copy; <?php echo date("Y") ?> George International Academy</time>
      </div>
    </div>
  </footer>
</body>
<?php include_once ("../../includes/scripts.php"); ?>
<script>
function showMore(id) {
  if(id == "1"){
    $("#moreModules").css("display", "block")
    $("#studentInfo").css("display", "none")
    $("#moreBtn").css("display", "none")
    $("#closeBtn").css("display", "inline")
  }else{
    $("#moreModules").css("display", "none")
    $("#studentInfo").css("display", "block")
    $("#moreBtn").css("display", "inline")
    $("#closeBtn").css("display", "none")
  }  
}

function showHint(hint) {
  $("#hinter").html(hint)
}

</script>
</body>

</html>
