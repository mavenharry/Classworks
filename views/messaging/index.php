<?php include_once ("messagingBtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>

    <!-------------------- START - MODAL -------------------->
    <?php 
    $schoolDetails = $messagingClass->getSchoolDetails();
    include ("../../includes/modals/index.php"); ?>
    <!-------------------- END - MODAL -------------------->

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Messaging", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">MESSAGING/NOTIFICATION PANEL</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="#" data-target="#smsPurchaseModal" data-toggle="modal">
                                                    <div class="label">TOTAL SMS UNITS LEFT</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-email-forward"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo $schoolDetails->smsUnit ?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to buy</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="allSms.php">
                                                    <div class="label">View all sent sms</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-cv-2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">ALL Sent SMS</div>
                                                </a>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>