<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class messagingClass extends logic {
    
    function getAllMessages(){
        $allMessages = array();
        $stmt = $this->uconn->prepare("SELECT * FROM messages ORDER BY id DESC LIMIT 50");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->broadcastDate = $row["broadcastDate"];
                $obj->broadcastMessage = $row["broadcastMessage"];
                $audience = implode(", ", json_decode($row["audienceBroadcast"], JSON_PRETTY_PRINT));
                $obj->audience = $audience;
                if($audience == "Specify"){
                    $obj->numbers = implode(", ", json_decode($row["logNumbers"], JSON_PRETTY_PRINT));
                }else{
                    $obj->numbers = "All ".$audience;
                }
                $obj->deliveryState = $row["state"];
                $allMessages[] = $obj;
            }
        }
        $stmt->close();
        return $allMessages;
    }
}
?>