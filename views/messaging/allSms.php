<?php include_once ("messagingBtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $messagingClass->getSchoolDetails();
$allMessages = $messagingClass->getAllMessages();

?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Messaging", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#broadcastModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Broadcast SMS notice</span>
                                    </a>
                                    <a data-target="#noticeModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Publish quick notice</span>
                                    </a>
                                    <a data-target="#messageSearchModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><?php if(isset($_GET['class_id'])){echo "CURRENT ".strtoupper($studentClass->getClassName($_GET['class_id']))." STUDENT(S)";}else{ if(isset($_GET['parent_id'])){ echo strtoupper($studentClass->split_full_name($studentClass->fetchSingleParent($_GET['parent_id'])->fullname)["salutation"])." ".strtoupper($studentClass->split_full_name($studentClass->fetchSingleParent($_GET['parent_id'])->fullname)["lname"])."'S STUDENT(S)"; }else{echo "CURRENT STUDENT(S)";} } ?></h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Broadcast Audience</th>
                                                <th>Broadcast Date</th>
                                                <th>Message</th>
                                                <!-- <th>Numbers</th> -->
                                                <th>Broadcast State</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allMessages) == 0){
                                                    echo "No message found in record";
                                                }else{
                                                    foreach($allMessages as $key => $message){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $key + 1 ?>.</span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $message->audience; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $message->broadcastDate; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $message->broadcastMessage; ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <span><?php echo $message->deliveryState; ?></span>
                                                            </td>
                                                        </tr>   
                                                    <?php }
                                                } ?>
                                            </tbody>
                                        </table>
                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allMessages) > 20){ ?>
                                                <div style="float: right;" class="element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->
                        </div>
                    </div>
                </div>
            </div> 
                      <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
               

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>