<?php include_once ("childpickupbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Child Pickup</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../addons/scanner/style.css" rel="stylesheet">
</head>
<?php
    $schoolDetails = $childpickupClass->getSchoolDetails();
?>


<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all" style="background-color:#FFFFFF; height:100%">

    <div class="layout-w">

<div class="content-w" style="background-color:#FFFFFF; height:100%">

<center>

<div style="position:absolute; left:50px; top:30px; ">
    <h3><span style="font-weight:800; color:#FB5574">CHILD PICKUP VERIFICATION</span></h3>
    <h3 style="float:left">Date: <span style="font-weight:600;" id="attendanceDate">00/00/0000</span></h3>
</div>

<div style="position:absolute; right:50px; top:30px; ">
    <h3>Time: <span style="font-weight:600;" id="attendanceTime">00:00</span></h3>
</div>

<div id="initialAttendStateChild" style="display:block">
    <div class="logo-w">
        <a class="logo">
            <img alt="" style="margin-top:120px; width:210px; height:200px" src="<?php echo $schoolDetails->logo;?>">
            <div class="logo-label" style="margin-top:0px; font-size:1.8rem; color:#08ACF0"><?php echo $schoolDetails->school_name?></div>
            <h5 style="font-weight:400; font-size:1.2rem; margin-top:0px; font-style:italic"><?php echo $schoolDetails->city.", ".$schoolDetails->state;?></h5>
        </a>
    </div>

    <h4 style="margin-top:20px;">WELCOME, USE THE BARCODE READER TO SCAN THE PICKUP/STUDENT CARD</h4>
    <div style="margin-left:-40px" id="pickUpIdScan">
    <?php include("../../addons/scanner/index.html") ?>
    </div>

</div>

<div id="resultAttendStateChild" style="display:none">

    <form class="" method="POST">

        <div id="display">
        
        </div>
    
        <div class="row col-sm-7">
            <!-- <div class="form-check" style="float:left; margin-top:30px;"><label class="form-check-label"><input class="form-check-input" type="checkbox">I hereby authenticate that this is a valid person.</label></div> -->
            <input type="submit" name="SignIn" value=" Sign in this person" class="my_hover_up btn btn-primary" style="margin-left:15px; margin-top:30px; margin-bottom: 150px; float:left; border-color:#08ACF0; background-color:#08ACF0">
        </div>

    </form>
</div>

</div>

</center>

<form id="barcodeScanForm" onsubmit="return false;">
    <div class="form-group">
        <input autocomplete="off" id="pickupCardNo" style="width:0px; background-color:#f2f4f8; position:absolute; left:-50px; bottom:0" class="form-control" placeholder="E.g; 376378363482046835" type="text" autofocus>
    </div>
</form>


<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

        <script language="javascript">
        var scan = true;
        function noLeftClick() {
        if (event.button==0 || event.button==2 || event.button==1) {
            return false;
            }
        }
        document.onmousedown=noLeftClick
        document.getElementById("pickupCardNo").addEventListener("input", function(){
            var number = $("#pickupCardNo").val();
            var splited = number.split("/").length;
            if(splited > 3){
                if(scan === true){
                    executeChildPickup();
                    scan = false;
                }
            }
        });
        </script>

</body>

</html>