<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");
date_default_timezone_set("Africa/lagos");
class childpickupClass extends logic {

    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return ucfirst($value);
        }else{
            return "N/A";
        }
    }

    function pickUpCount(){
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS totalPickup FROM childpickup");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $totalPickup = array_shift($stmt_result)["totalPickup"];
        $verification = 1;
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS enabledPickup FROM students WHERE pickup_verification = ?");
        $stmt->bind_param("i", $verification);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $enabledPickup = array_shift($stmt_result)["enabledPickup"];
        $datePicked = Date("Y-m-d");
        $stmt = $this->uconn->prepare("SELECT COUNT(id) AS todayPicks FROM pickuprecords WHERE SUBSTRING_INDEX(pickuprecords.datePicked, ' ', 1) = ?");
        $stmt->bind_param("s", $datePicked);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $todayPicks = array_shift($stmt_result)["todayPicks"];
        $obj = new stdClass();
        $obj->todayPicks = $todayPicks;
        $obj->enabledPickup = $enabledPickup;
        $obj->totalPickup = $totalPickup;
        $stmt->close();
        return $obj;
    }
    
    function getallPickupEnabledStudents($status, $class){
        if($class == "all"){
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE pickup_verification = ? LIMIT 100");
            $stmt->bind_param("i", $status);
        }else{
            $stmt = $this->uconn->prepare("SELECT * FROM students WHERE pickup_verification = ? AND present_class = ?");
            $stmt->bind_param("is", $status, $class);
        }
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $studentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->student_number = $this->outputRefine($row["student_number"]);
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->gender = $this->outputRefine($row["gender"]);
            $obj->present_class = $this->outputRefine($row["present_class"]);
            $obj->profilePhoto = $row["profilePhoto"];
            $obj->pickupStatus = $row["pickup_verification"];
            $studentArray[] = $obj;
        }
        $stmt->close();
        return $studentArray;
    }

    function recordPickup($studentNumber, $pkNumber){
        $date = date("Y-m-d");
        $stmt = $this->uconn->prepare("SELECT pickuprecords.pickupNumber, childpickup.fullname FROM pickuprecords JOIN childpickup ON childpickup.pickupNumber = pickuprecords.pickupNumber WHERE pickuprecords.studentNumber = ? AND pickuprecords.datePicked LIKE CONCAT(?,'%')");
        $stmt->bind_param("ss", $studentNumber, $date);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) == 0){
            $stmt = $this->uconn->prepare("INSERT INTO pickuprecords (studentNumber, pickupNumber) VALUES (?,?)");
            $stmt->bind_param("ss", $studentNumber, $pkNumber);
            if($stmt->execute()){
                // SEND MESSAGE
                if(strtolower(explode("/", $pkNumber)[1]) != "prnt"){
                    if($this->getSchoolDetails()->parentAlertOnPickup == 1){
                        $unitLeft = $this->checkSmsBalance();
                        if($unitLeft > 1){
                            $stmt = $this->uconn->prepare("SELECT students.father_phone, students.mother_phone, students.fullname, childpickup.fullname AS pickupName FROM students INNER JOIN childpickup ON childpickup.studentNumber = students.student_number WHERE students.student_number = ? AND childpickup.pickupNumber = ?");
                            $stmt->bind_param("ss", $studentNumber, $pkNumber);
                            $stmt->execute();
                            $stmt_result = $this->get_result($stmt);
                            $row = array_shift($stmt_result);
                            $date = date("Y-m-d");
                            $messageDate = date("Y-m-d H:i:s");
                            $row["father_phone"] != "" ? $parentsContact[] = $row["father_phone"] : "";
                            $row["mother_phone"] != "" ? $parentsContact[] = $row["mother_phone"] : "";
                            $fullname = $row["fullname"];
                            $pickupName = $row["pickupName"];
                            $message = "Good day, your ward ".$fullname." has just been picked up from school by ".$pickupName." at ".$messageDate;
                            $reply = $this->sendSMS($message, $this->getSchoolDetails()->domain_name, $parentsContact);
                            if($reply === true){
                                $reply = $this->logMessage(["specify"], $date, $message, $parentsContact, 1);
                                if($reply === true){
                                    $stmt->close();
                                    return "true";
                                }else{
                                    $stmt->close();
                                    return "messageError";
                                }
                            }else{
                                $stmt->close();
                                return "smsError";
                            }
                        }else{
                            $stmt->close();
                            return "insufficient";
                        }
                    }else{
                        $stmt->close();
                        return "true";
                    }
                }
            }
        }else{
            $fullname = array_shift($stmt_result)["fullname"];
            return $fullname;
        }
    }

    function updatePickupState($studentId, $val){
        $stmt = $this->uconn->prepare("UPDATE students SET pickup_verification = ? WHERE student_number = ?");
        $stmt->bind_param("is", $val, $studentId);
        if($stmt->execute()){   
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }

    function fetchAllPickupPerson(){
        $stmt = $this->uconn->prepare("SELECT * FROM childpickup");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $allPickupPerson = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->pickupNumber = $row["pickupNumber"];
                $obj->fullname = $row["fullname"];
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->gender = $row["gender"];
                $obj->phone = $row["phone"];
                $obj->relationship = $row["relation_student"];
                $allPickupPerson[] = $obj;
            }
        }
        $stmt->close();
        return $allPickupPerson;
    }

    function fetchAllPickedUpStudents(){
        $datePicked = date("Y-m-d");
        $stmt = $this->uconn->prepare("SELECT students.fullname AS studentName, childpickup.fullname, childpickup.phone, childpickup.relation_student, students.profilePhoto, students.gender, students.present_class, SUBSTRING_INDEX(pickuprecords.datePicked, ' ', -1) AS pickedupTime FROM pickuprecords INNER JOIN students ON students.student_number = pickuprecords.studentNumber INNER JOIN childpickup ON childpickup.pickupNumber = pickuprecords.pickupNumber WHERE SUBSTRING_INDEX(pickuprecords.datePicked, ' ', 1) = ?");
        $stmt->bind_param("s", $datePicked);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $pickedUpStudent = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->studentName = $row["studentName"];
                $obj->fullname = $row["fullname"];
                $obj->phone = $row["phone"];
                $obj->relation = $row["relation_student"];
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->gender = $row["gender"];
                $obj->present_class = $row["present_class"];
                $times = date("h:i:s A", strtotime($row["pickedupTime"]));
                $obj->pickedupTime = $times;
                $pickedUpStudent[] = $obj;
            }
        }
        $stmt->close();
        return $pickedUpStudent;
    }
}
?>