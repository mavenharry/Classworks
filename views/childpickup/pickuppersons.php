<?php include_once ("childpickupbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Child Pickup</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $childpickupClass->getSchoolDetails();
    $allPickupPerson = $childpickupClass->fetchAllPickupPerson();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Child Pickup", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#newPickupModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Add new person</span>
                                    </a>
                                    <a data-target="#newDeleteModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete person</span>
                                    </a>
                                    <a data-target="#searchPickPerson" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">STUDENT(S) PICKUP PERSONS</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Pickup number</th>
                                                <th>Person name</th>
                                                <th>Gender</th>
                                                <th>Telephone</th>
                                                <th>Relationship</th>
                                                <th class="text-center">Pickup Student(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allPickupPerson) == 0){
                                                    echo "No pickup person in record";
                                                }else{
                                                    foreach($allPickupPerson as $key => $value){ ?>
                                                    <tr class="my_hover_up">
                                                        <td class="nowrap">
                                                            <span><?php echo $key + 1?>.</span>
                                                        </td>
                                                        <td>

                                                            <?php if(!empty($value->profilePhoto)){ ?>
                                                            <div class="user-with-avatar">
                                                            <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto;?>">
                                                            </div>
                                                            <?php }else{ ?>
                                                                <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                class="badge"><?php echo $nameInitials = $childpickupClass->createAcronym($value->fullname); ?></a>
                                                            <?php } ?>

                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->pickupNumber; ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->fullname); ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->gender); ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->phone; ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->relationship); ?></span>
                                                        </td>
                                                        <td class="text-center">
                                                            <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                class="my_hover_up_bg badge" href="">View Student(s)</a>
                                                        </td>
                                                    </tr>
                                                <?php    }
                                                }
                                            ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allPickupPerson) > 20){ ?>
                                            <div style="float: right;" class="element-actions">
                                            <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <span style="font-size: .9rem;">Load more</span>
                                            </a>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include_once ("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>