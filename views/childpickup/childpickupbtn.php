<?php
    include_once ("childpickupClass.php");
    $childpickupClass = new childpickupClass;

    if(isset($_POST["SignIn"])){
        $studentNumber = htmlentities(trim($_POST["studentNumber"]));
        $pkNumber = htmlentities(trim($_POST["pkNumber"]));

        if(empty($pkNumber)){
            $err = "Please select a valid pickup person";
        }else{
            $reply = $childpickupClass->recordPickup($studentNumber, $pkNumber);
            if($reply == "true"){
                $mess = "Signed in. You can proceed to pick up child";
            }else{
                switch ($reply) {
                    case 'false':
                        # code...
                        $err = "Couldnt complete this operation. Please try again";
                        break;
                    case 'messageError':
                        $err =  "Pickup recorded but couldnt send message due to system error";
                        break;
                    case 'smsError':
                        $err = "Pickup recorded but system encountered an error while trying to send message.";
                        break;
                    case 'insufficient':
                        $err = "Pickup recorded but couldn't send message due to insufficient unit left. Please recharge";
                        break;
                    
                    default:
                        # code...
                        $err = "Child already picked up by ".$reply;
                        break;
                }
            }
        }
    }
?>