<?php include_once ("childpickupbtn.php");; ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Child Pickup</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $childpickupClass->getSchoolDetails();
    $allClass = $childpickupClass->getClasses();
    $allStudentsWithPickup = $childpickupClass->getallPickupEnabledStudents(1, "all");
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Child Pickup", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchPickupEnabledModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"> PICKUP VERIFICATION ENABLED STUDENTS</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Student no.</th>
                                                <th>Name</th>
                                                <th>Gender</th>
                                                <th>Class</th>
                                                <th class="text-center">Verification Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allStudentsWithPickup) == 0){
                                                    echo "No student with enabled verification found in record";
                                                }else{
                                                    for($i = 0; $i < count($allStudentsWithPickup); $i++){ ?>
                                                <tr class="my_hover_up" data-id="<?php echo $allStudentsWithPickup[$i]->student_number ?>">
                                                    <td class="nowrap">
                                                        <span><?php echo $i+1 ?>.</span>
                                                    </td>
                                                    <td>

                                                        <?php if(!empty($allStudentsWithPickup[$i]->profilePhoto)){ ?>
                                                        <div class="user-with-avatar">
                                                        <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$allStudentsWithPickup[$i]->profilePhoto;?>">
                                                        </div>
                                                        <?php }else{ ?>
                                                            <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                            class="badge"><?php echo $nameInitials = $childpickupClass->createAcronym($allStudentsWithPickup[$i]->fullname); ?></a>
                                                        <?php } ?>

                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStudentsWithPickup[$i]->student_number ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStudentsWithPickup[$i]->fullname ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStudentsWithPickup[$i]->gender ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allStudentsWithPickup[$i]->present_class ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="center c-toggle-btn">
                                                        <input id="enableChildPickup" <?php if($allStudentsWithPickup[$i]->pickupStatus == 1){ echo "checked"; } ?> type="checkbox">
                                                        <div>
                                                            <label class="on">On</label>
                                                            <label class="off">Off</label>
                                                            <span class="c-toggle-thumb"></span>
                                                        </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php    }
                                                }
                                            ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allStudentsWithPickup) > 20){ ?>
                                            <div style="float: right;" class-"element-actions">
                                            <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <span style="font-size: .9rem;">Load more</span>
                                            </a>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>