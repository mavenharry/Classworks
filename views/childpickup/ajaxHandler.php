<?php
include_once ("childpickupbtn.php");
$schoolDetails = $childpickupClass->getSchoolDetails();
if(isset($_GET["pickupCardNo"])){
    $pickupCardNo = htmlentities(trim($_GET["pickupCardNo"]));
    $reply = $childpickupClass->getPickupDetails($pickupCardNo); ?>
        <div class="logo-w">
            <a class="logo">
                <?php
                    if(!empty($reply[0]->studentProfilePhoto)){ ?>
                        <img alt="" style="border: 3px solid #FB5574; border-radius:100px; margin-top:120px; width:180px; height:180px" src="<?php echo $schoolDetails->pathToPassport."/".$reply[0]->studentProfilePhoto;?>">
                    <?php }else{ ?>
                        <img alt="" style="border: 3px solid #FB5574; border-radius:100px; margin-top:120px; width:180px; height:180px" src="../../images/male-user-profile-picture.png">
                    <?php }

                    if(empty($reply[0]->studentName)){ ?>
                    
                    <?php }else{ ?>
                        <div class="logo-label" style="font-size:1.5rem; margin-top:0px; color:#08ACF0"><?php echo $reply[0]->studentName; ?></div>
                        <h5 style="font-weight:400; font-size:1.2rem; margin-top:5px; font-style:italic">CLASS: <?php echo strtoupper($reply[0]->studentClass); ?></h5>
                        <input type="hidden" name="studentNumber" value="<?php echo $reply[0]->studentNumber;?>" id="">
                    <?php }
                ?>
            </a>
        </div>
        <input type="hidden" name="pkNumber" id="pknumber">
        <div class="row col-sm-7" style="margin-left: 0px; margin-top:10px;">
            <!-- PERSON 1 -->
            <?php

                if(count($reply) > 1){
                    foreach ($reply as $key => $value) {
                        # code...onclick="pickPerson('person1')"
                        if($key != 0){ ?>
                            <div class="my_hover_up" data-id="<?php echo $value->pickupNumber; ?>" id="pickupPerson" style="position: relative; left: 0; top: 0; border-radius:6px; cursor:pointer; margin-right:20px; margin-top: 20px;">
                                <img alt="" class="verified" id="person1" style="z-index:200000; position:absolute; top:60px; left:58px; width:60px; height:60px;" src="../../images/verified.png">
                                <img alt="" style="position: relative; top: 0; left: 0; margin:10px; padding:5px; width:150px; height:160px; border-radius:6px; border: 2px solid #d9d9d9" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto;?>">
                                <h6 style="margin-top:0px;"><span><?php echo $value->fullname; ?></span></h6> 
                            </div>
                    <?php    }
                    }
                }else{ ?>
                    <div class="my_hover_up" id="pickupPerson" style="position: relative; left: 0; top: 0; border-radius:6px; cursor:pointer; margin-right:20px; margin-top: 20px;">
                        <img alt="" style="position: relative; top: 0; left: 0; margin:10px; padding:5px; width:150px; height:160px; border-radius:6px; border: 2px solid #d9d9d9" src="../../images/male-user-profile-picture.png">
                        <h6 style="margin-top:0px;"><span>No Pickup Person added for this child</span></h6> 
                    </div>
                <?php }
            ?>
        </div>
<?php }

if(isset($_GET["studentId"]) && isset($_GET["val"])){
    $studentId = htmlentities(trim($_GET["studentId"]));
    $val = htmlentities(trim($_GET["val"]));
    $reply = $childpickupClass->updatePickupState($studentId, $val);
    echo $reply;
}
?>