<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class parentClass extends logic {

    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return $value;
        }else{
            return "N/A";
        }
    }

    function fetchAllParent(){
        $stmt = $this->uconn->prepare("SELECT * FROM parents");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $parentArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
            $obj->fullname = $this->outputRefine($row["fullname"]);
            $obj->phone = $this->outputRefine($row["phone"]);
            $obj->email = $this->outputRefine($row["email"]);
            $obj->relationship = $this->outputRefine($row["relation_student"]);
            $obj->profilePhoto = $row["ProfilePhoto"];
            $parentArray[] = $obj;
        }
        $stmt->close();
        return $parentArray;
    }

    function parentSort($sortClass, $sortRelationship, $searchKeyword){
        $parentArray = array();
        if(!empty($sortClass) && !empty($sortRelationship) && !empty($searchKeyword)){
            echo $sortRelationship;
            // ALL THREE IS SET
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE students.present_class = ? AND parents.relation_student = ? AND parents.guardian_number = ? OR parents.fullname = ?");
            $stmt->bind_param("ssss", $sortClass, $sortRelationship, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
        elseif(!empty($sortClass) && !empty($sortRelationship) && empty($searchKeyword)){
            // SORTCLASS AND SORTRELATIONSHIP
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE students.present_class = ? AND parents.relation_student = ?");
            $stmt->bind_param("ss", $sortClass, $sortRelationship);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
        elseif(!empty($sortClass) && empty($sortRelationship) && !empty($searchKeyword)){
            // SORTCLASS AND SEARCHKEYWORD
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE students.present_class = ? AND parents.guardian_number = ? OR parents.fullname = ?");
            $stmt->bind_param("sss", $sortClass, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
        elseif(!empty($sortClass) && empty($sortRelationship) && empty($searchKeyword)){
            // ONLY SORTCLASS
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE students.present_class = ?");
            $stmt->bind_param("s", $sortClass);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }

        }
        elseif(empty($sortClass) && !empty($sortRelationship) && !empty($searchKeyword)){
            // SORTRELATIONSHIP AND SEARCHKEYWORD
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE parents.relation_student = ? AND parents.fullname = ? OR parents.guardian_number = ?");
            $stmt->bind_param("sss", $sortRelationship, $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
        elseif(empty($sortClass) && empty($sortRelationship) && !empty($searchKeyword)){
            // ONLY SEARCHKEYWORD
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents INNER JOIN students ON parents.phone = students.father_phone OR parents.phone = students.mother_phone WHERE parents.fullname = ? OR parents.guardian_number = ?");
            $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
        elseif(empty($sortClass) && !empty($sortRelationship) && empty($searchKeyword)){
            // ONLY SORTRELATIONSHIP
            $stmt = $this->uconn->prepare("SELECT parents.fullname, parents.phone, parents.email, parents.relation_student, parents.guardian_number, parents.ProfilePhoto FROM parents WHERE parents.relation_student = ?");
            $stmt->bind_param("s", $sortRelationship);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->guardian_number = $this->outputRefine($row["guardian_number"]);
                    $obj->fullname = $this->outputRefine($row["fullname"]);
                    $obj->phone = $this->outputRefine($row["phone"]);
                    $obj->email = $this->outputRefine($row["email"]);
                    $obj->relationship = $this->outputRefine($row["relation_student"]);
                    $obj->profilePhoto = $row["ProfilePhoto"];
                    $parentArray[] = $obj;
                }
                $stmt->close();
                return $parentArray;
            }else{
                $stmt->close();
                return $parentArray;
            }
        }
    }

    function updateParentBioData($fullname, $address, $email, $phone, $occupation, $filename, $parentNumber){
        $formerPhone = $this->fetchSingleParent($parentNumber)->phone;
        $stmt = $this->uconn->prepare("UPDATE parents SET fullname = ?, address = ?, email = ?, phone = ?, occupation = ?, ProfilePhoto = ? WHERE guardian_number = ?");
        $stmt->bind_param("sssssss", $fullname, $address, $email, $phone, $occupation, $filename, $parentNumber);
        if($stmt->execute()){
            if(!empty($formerPhone)){
                $stmt = $this->uconn->prepare("UPDATE students SET father_phone = ? WHERE father_phone = ?");
                $stmt->bind_param("ss", $phone, $formerPhone);
                if($stmt->execute()){
                    $stmt = $this->uconn->prepare("UPDATE students SET mother_phone = ? WHERE mother_phone = ?");
                    $stmt->bind_param("ss", $phone, $formerPhone);
                    if($stmt->execute()){
                        $stmt->close();
                        return true;
                    }else{
                        $stmt->close();
                        return false;
                    }
                }else{
                    $stmt->close();
                    return false;
                }
            }else{
                $stmt->close();
                return true;
            }
        }else{
            $stmt->close();
            return false;
        }
    }

    function updateAccess($parentNumber, $ParentPortalAccess){
        $stmt = $this->uconn->prepare("UPDATE parents SET app_access = ? WHERE guardian_number = ?");
        $stmt->bind_param("is", $ParentPortalAccess, $parentNumber);
        if($stmt->execute()){
            $stmt->close();
            return true;
        }else{
            $stmt->close();
            return false;
        }
    }
    
}
?>