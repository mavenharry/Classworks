<?php
    include_once ("parentbtn.php");
    if(!isset($_GET["parent_id"])){
        echo "<script>window.location.assign('../authentication')</script>";
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<?php
$parentNumber = base64_decode($_GET["parent_id"]);
$schoolDetails = $parentClass->getSchoolDetails();
$parentDetails = $parentClass->fetchSingleParent($parentNumber);
$allStudent = $parentClass->fetchAllParentStudent($parentNumber); 
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Parents", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

            <br>

            <!--START - Profile-->
<div class="content-i" style="margin-top:-30px;">
<div class="content-box">
<div class="row">
    <div style="margin-left:13px; margin-bottom:20px; margin-top:-8px; float:right" class="element-actions">
        <a style="color:#FFFFFF; margin-top:10px; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="../timetable/Parent_timetable?Parent=<?php echo $parentNumber; ?>&session=<?php echo $schoolDetails->academic_session; ?>&term=<?php echo $schoolDetails->academic_term;?>">
            <i class="os-icon os-icon-link-3"></i>
            <span style="font-size: .9rem;">Send Payment Link</span>
        </a>
        <a data-target=".bd-example-modal-sm" data-id="<?php echo $parentNumber; ?>" id="delParent" data-toggle="modal" style="color:#FFFFFF; margin-top:10px; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-ui-15"></i>
            <span style="font-size: .9rem;">Delete Parent</span>
        </a>
        <a data-target="" data-toggle="modal" style="margin-top:10px; color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-mail-01"></i>
            <span style="font-size: .9rem;">Send SMS Message</span>
        </a>
        <a style="margin-top:10px; color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
            <i class="os-icon os-icon-grid"></i>
            <span style="font-size: .9rem;">Send Mobile App link</span>
        </a>
        <a style="margin-top:10px; color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="idcard.php?parent_id=<?php echo base64_encode($parentDetails->guardian_number)?>">
            <i class="os-icon os-icon-cv-2"></i>
            <span style="font-size: .9rem;">Print ID Card</span>
        </a>
    </div>
   <div class="col-sm-6">
      <div class="user-profile compact">
        <div class="profile-tile" style="padding-top:20px; padding-left:30px; border-top-right-radius:10px; border-top-left-radius:10px; background-color:#FFFFFF">
                <div class="my_hover_up pt-avatar-w" style="border-radius:0px;">
                    <?php if(!empty($parentDetails->profilePhoto)){ ?>
                    <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="<?php echo $schoolDetails->pathToPassport."/".$parentDetails->profilePhoto; ?>">
                    <?php }else { ?>
                        <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="../../images/male-user-profile-picture.png">            
                    <?php } ?>
                </div>
                <div class="profile-tile-meta">
                    <ul style="text-transform:capitalize">
                        <li style="color:#8095A0;">Name: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->fullname; ?></strong></li>
                        <li style="color:#8095A0">Parent Number: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->guardian_number; ?></strong></li>
                        <li style="color:#8095A0">Occupation: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->occupation; ?></strong></li>
                        <li style="color:#8095A0">Gender: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->gender; ?></strong></li>
                        <li style="color:#8095A0">Relation Type: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->relationship; ?></strong></li>
                        <li style="color:#8095A0">Marital Status: <strong style="color:#8095A0; font-weight:600"><?php echo $parentDetails->m_status; ?></strong></li>
                    </ul>
                </div>
            </div>

         <div class="up-contents">
            <div class="m-b">
               <div class="row m-b">
                  <div class="col-sm-6 b-r b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#1BB4AD"><?php echo ucwords($parentDetails->nationality); ?></div>
                        <div class="label" style="margin-top:10px; font-weight:600; color:#8095A0">Nationality</div>
                     </div>
                  </div>
                  <div class="col-sm-6 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#FE650B"><?php echo ucwords($parentDetails->state); ?></div>
                        <div class="label" style="margin-top:10px; font-weight:600; color:#8095A0">STATE</div>
                     </div>
                  </div>
                  <div class="col-sm-12 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#08ACF0"><?php echo ucwords($parentDetails->city); ?></div>
                        <div class="label" style="margin-top:10px; font-weight:600; color:#8095A0">CITY</div>
                     </div>
                  </div>
                  <div class="col-sm-12 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#FB5574"><?php echo ucwords($parentDetails->email); ?></div>
                        <div class="label" style="margin-top:10px; font-weight:600; color:#8095A0">Email</div>
                     </div>
                  </div>
                  <div class="col-sm-12 b-b">
                     <div class="el-tablo centered padded-v">
                        <div class="value" style="color:#1BB4AD"><?php echo ucwords($parentDetails->phone); ?></div>
                        <div class="label" style="margin-top:10px; font-weight:600; color:#8095A0">PHONE</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="element-wrapper" style="margin-top: 1rem;">
        
        <!-- STUDENTS -->
        <div class="element-wrapper" style="margin-top: 1rem;">
        <div class="element-box">
        
            <div class="form-group" style="margin-top:0px;">

            <legend style="margin-top:0px;"><span>STUDENTS</span></legend>
            <?php 
            if(count($allStudent) > 0){
            for ($i=0; $i < count($allStudent); $i++) { ?>
                <div class="form-group row" style="margin-top:20px; margin-left:0px;">
                <h4 for=""><?php echo ucwords($allStudent[$i]->fullname); ?> <span style="font-size:18px; font-weight:normal">(<?php echo ucwords($allStudent[$i]->present_class); ?>)</span></h4>
                </div>
            <?php }}else{ ?>
                <div class="form-group row" style="margin-top:20px; margin-left:0px;">
                <h4 style="font-size:18px" for="">No student associated to this parent</h4>
                </div>
            <?php } ?>
        </div>
        </div>
        </div>                


      </div>
   </div>
   
   <div class="col-sm-6">
      <div class="element-wrapper">
    
      <!-- Parent BIO DATA -->

      <div class="element-box" >
            <form id="formValidate" method="POST" action="" enctype="multipart/form-data">
               <div class="element-info">
                  <div class="element-info-with-icon">
                     <div class="element-info-icon">
                        <div class="os-icon os-icon-user"></div>
                     </div>
                     <div class="element-info-text">
                        <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Academic Parent Bio Data</h5>
                     </div>
                  </div>
               </div>
               <div class="row" style="margin-top:-10px; margin-bottom:20px;">
                <div class="pt-avatar-w" style="border-radius:0px;">
                    <img alt="" id="studentuploadPreview2" style="margin-left:20px; border-radius:5px; border: 1px solid #08ACF0; height:100px; width:90px" src="../../images/male-user-profile-picture.png">
                </div>
               <div class="form-group col-sm-7" style="margin-left:20px;">
                  <label for="studentupload-photo2">Change Parent passport</label>
                  <label style="cursor:pointer" class="btn btn-primary" for="studentupload-photo2"> Upload Image</label>
                  <input id="studentupload-photo2" name="profilePhoto" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1; " class="form-control" type="file">
                  <input type="hidden" name="profileChecker" value="<?php echo $parentDetails->profilePhoto; ?>" id="">
               </div>
               </div>
               <div class="form-group">
                  <label for=""> Parent's Registration No.</label><input name="parentNumber" class="form-control" readonly value="<?php echo $parentDetails->guardian_number; ?>" type="text">
               </div>
               <div class="form-group">
                  <label for=""> Parent's Full name</label><input name="fullname" class="form-control" value="<?php echo ucwords($parentDetails->fullname); ?>" type="text">
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Nationality</label><input class="form-control" value="<?php echo ucfirst($parentDetails->nationality); ?>" disabled type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> State/Province</label><input class="form-control" value="<?php echo ucfirst($parentDetails->state); ?>" disabled type="text">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> City/Region</label><input class="form-control" value="<?php echo ucfirst($parentDetails->city); ?>" disabled type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Gender</label><input class="form-control" value="<?php echo ucfirst($parentDetails->gender);?>" disabled type="text">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for=""> Marital Status</label>
                  <select class="form-control" name="m_status" <?php if($_SESSION["role"] == "Admin" || $_SESSION["role"] == "accountant"){echo ""; }else{ echo "readonly"; } ?> >
                    <?php
                        $marital_status = ["single", "married", "Divorced", "separated", "others"];
                        for($i = 0; $i < count($marital_status); $i++){ ?>
                            <option <?php echo $parentDetails->m_status == strtolower($marital_status[$i]) ? "selected" : ""; ?> value="<?php echo $marital_status[$i];?>"><?php echo $marital_status[$i]?></option>
                    <?php } ?>
                </select>
               </div>
               <div class="form-group">
                  <label for=""> Address</label><input name="address" class="form-control" value="<?php echo ucfirst($parentDetails->address); ?>" type="text">
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Email Address</label><input name="email" class="form-control" value="<?php echo $parentDetails->email; ?>" type="text">
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Phone Number</label><input name="phone" class="form-control" value="<?php echo ucfirst($parentDetails->phone);?>" type="text">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                <label for="">Occupation</label>
                <input name="occupation" placeHolder="Lawyer" class="form-control" value="<?php echo ucfirst($parentDetails->occupation);?>" type="text">
                </select>
                </div>
                <div class="form-check" style="margin-top:30px;"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I agree to terms and conditions</label></div>
                <div class="form-buttons-w" style="margin-top:15px; "><input name="bioData" type="submit" class="btn btn-primary" value=" Update Bio Data"></div>
            </form>
         </div>

         
        <!-- SETTINGS -->
        <div class="element-box" style="margin-top: 1rem;">
            <form id="formValidate" method="POST" action="#">
            <div class="element-info">
                <div class="element-info-with-icon">
                    <div class="element-info-icon">
                    <div class="os-icon os-icon-ui-46"></div>
                    </div>
                    <div class="element-info-text">
                    <h5 class="element-inner-header" style="vertical-align: middle; margin-bottom: 0px;">Parent Mobile App Settings</h5>
                    </div>
                </div>
            </div>
            <input type="hidden" name="parentNumber" value="<?php echo $parentNumber; ?>">
            
            <div class="form-group">
            <label for="">Parent Mobile App Access? <div class="value badge badge-pill badge-<?php echo $parentDetails->appAccess == 1 ? "success" : "danger";?>" style="padding-bottom:2px; padding-top:2px; vertical-align:middle; border-radius:5px; margin-top:-5px"><?php echo $parentDetails->appAccess == 1 ? "Yes" : "No";?></div></label>
            <select class="form-control" name="ParentPortalAccess" >
                <option value="1" <?php echo $parentDetails->appAccess == 1 ? "selected" : "";?>>Yes</option>
                <option value="0" <?php echo $parentDetails->appAccess == 0 ? "selected" : "";?>>No</option>
            </select>
            </div>

            <div class="form-buttons-w" style="margin-top:15px; "><input type="submit" name="saveSettings" value=" Update Settings" class="btn btn-primary" ></div>
            </form>
            </div>

      </div>
   </div>
</div>


</div><!--------------------
   START - Sidebar
   -------------------->
<!--------------------
   END - Sidebar
   -------------------->


            <!--END - Profile-->


<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->             

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>