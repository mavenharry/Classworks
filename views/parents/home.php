<?php include_once ("parentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Parents</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
    <!-------------------- START - MODAL -------------------->
    <?php include("../../includes/modals/index.php") ?>
    <!-------------------- END - MODAL -------------------->
<?php
    $schoolDetails = $parentClass->getSchoolDetails();
    if(isset($_POST["sortClass"]) || isset($_POST["sortRelationship"]) || isset($_POST["searchKeyword"])){
        @$sortClass = htmlentities(trim($_POST["sortClass"]));
        @$sortRelationship = htmlentities(trim($_POST["sortRelationship"]));
        @$searchKeyword = htmlentities(trim($_POST["searchKeyword"]));
        $allParent = $parentClass->parentSort($sortClass, $sortRelationship, $searchKeyword);
    }else{
        $allParent = $parentClass->fetchAllParent();
    } 
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Parents", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#newParentModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Add a new parent</span>
                                    </a>
                                    <a data-target="#newDeleteModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete a parent</span>
                                    </a>
                                    <a data-target="#searchParentsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL STUDENTS PARENT(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Parent number</th>
                                                <th>Name</th>
                                                <th>Telephone</th>
                                                <th>Relationship</th>
                                                <th class="text-center">Students</th>
                                                <th class="text-center">Profile</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allParent) == 0){
                                                    echo "No parent in record";
                                                }else{
                                                    for($i = 0; $i < count($allParent); $i++){ ?>
                                                        <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i + 1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <?php if(!empty($allParent[$i]->profilePhoto)){ ?>
                                                        <div class="user-with-avatar">
                                                        <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$allParent[$i]->profilePhoto;?>">
                                                        </div>
                                                        <?php }else{ ?>
                                                            <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                            class="badge"><?php echo $nameInitials = $parentClass->createAcronym($allParent[$i]->fullname); ?></a>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allParent[$i]->guardian_number; ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucwords($allParent[$i]->fullname); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allParent[$i]->phone; ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucwords($allParent[$i]->relationship); ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../students/home?parent_id=<?php echo $allParent[$i]->guardian_number; ?>">View Student(s)</a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="./profile?parent_id=<?php echo base64_encode($allParent[$i]->guardian_number); ?>">View Profile</a>
                                                    </td>
                                                </tr>
                                                <?php } 
                                                }?>
                                                
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allParent) > 20){ ?>
                                        <div style="float: right;" class-"element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>