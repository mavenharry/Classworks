<?php
include_once ("parentClass.php");
$parentClass = new parentClass();

if(isset($_POST["bioData"])){
    $fullname = htmlentities(trim($_POST["fullname"]));
    $address = htmlentities(trim($_POST["address"]));
    $email = htmlentities(trim($_POST["email"]));
    $phone = htmlentities(trim($_POST["phone"]));
    $occupation = htmlentities(trim($_POST["occupation"]));
    $profileChecker = htmlentities(trim($_POST["profileChecker"]));
    $parentNumber = htmlentities(trim($_POST["parentNumber"]));

    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $err = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $parentClass->getSchoolDetails()->pathToPassport;
            $filename = str_replace("/","_",$parentNumber).".".$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);

        }
    }else{
        $filename = $profileChecker;
    }

    $reply = $parentClass->updateParentBioData($fullname, $address, $email, $phone, $occupation, $filename, $parentNumber);
    if($reply === true){
        $mess = "Parent Bio data has been updated";
    }else{
        $err = "Could not complete this action. Please contact support";
    }
}

if(isset($_POST["saveSettings"])){
    $ParentPortalAccess = htmlentities(trim($_POST["ParentPortalAccess"]));
    $parentNumber = htmlentities(trim($_POST["parentNumber"]));

    $reply = $parentClass->updateAccess($parentNumber, $ParentPortalAccess);
    if($reply === true){
        $mess = "Parent app access has been updated";
    }else{
        $err = "Could not update parent app access. Please contact support";
    }
}
?>