<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Classworks - School Management Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="../../css/sales.css" rel="stylesheet">
</head>
<?php
    // COLLECT PARAMETER
    $session = htmlentities(trim($_POST["session"]));
    $term = htmlentities(trim($_POST["term"]));
    $student_id = htmlentities(trim($_POST["student_id"]));
    // $state = substr(htmlentities(trim($_POST["fee"])), 0, 1);
    // $fee = substr(htmlentities(trim($_POST["fee"])), 1);
    $allFees = $_POST["fee"];
    $feeArrays = array();
    $stateArray = array();
    foreach($allFees as $key => $value){
        $feeArrays[] = substr(htmlentities(trim($value)), 1);
        $stateArray[] = substr(htmlentities(trim($value)), 0, 1);
    }
    $impState = implode("*", $stateArray);
    $impFee = implode("*", $feeArrays);
    $inputCheck = count($allFees);
    $totalAmount = 0;
    foreach($allFees as $key => $value){
        $state = substr(htmlentities(trim($value)), 0, 1);
        $fee = substr(htmlentities(trim($value)), 1);
        $feeDetails = $paymentClass->getFeeDetails($fee, $state, $student_id);
        $totalAmount += $feeDetails->amount;
    }
    if($inputCheck == 1){
        $feeName = $paymentClass->getFeeDetails($fee, $state, $student_id)->fee_name;
    }else{
        $feeName = "Fees";
    }
    $schoolDetails = $paymentClass->getSchoolDetails();
    $feeDetails = $paymentClass->getFeeDetails($fee, $state, $student_id);
    $studentDetails = $paymentClass->getStudentDetails($student_id);
    $parentEmail = $paymentClass->getParentMail($student_id);
?>
<div id="overlay"></div>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:0px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                            <div style="margin-top:-15px;" class="element-actions">                                
                            </div>
                                
                                    <div class="sales__container">
                                        <div class="sales__top">
                                            <div class="sales__header"><span style="font-weight:800"><?php echo strtoupper($feeName) ?></span> PROCESSING FOR <span style="font-weight:800"><?php echo strtoupper($studentDetails->fullname) ?></span> - <?php echo strtoupper($studentDetails->present_class) ?></div>
                                            <div class="sales__display">
                                                <div class="sales__display__left">
                                                    <div class="sales__display__value">
                                                    <form id="form" method="post" action="">
                                                        <input value="<?php echo $schoolDetails->keys["paystack_public"]?>" type="hidden" id="pkey">
                                                        <input value="<?php echo count($parentEmail) > 0 ? $parentEmail[0] : $schoolDetails->email;?>" type="hidden" id="parentEmail">
                                                        <input value="<?php echo $student_id?>" type="hidden" id="student_id">
                                                        <input value="<?php echo $studentDetails->present_class;?>" type="hidden" id="student_class">
                                                        <input value="<?php echo $impFee ;?>" type="hidden" id="student_fee">
                                                        <input value="<?php echo $term ?>" type="hidden" id="student_term">
                                                        <input value="<?php echo $session ?>" type="hidden" id="student_session">
                                                        <input value="<?php echo $schoolDetails->school_name; ?>" type="hidden" id="schoolName">
                                                        <input value="<?php echo $impState; ?>" type="hidden" id="state">
                                                        <input value="" id="paymentReceiptNo" type="hidden">
                                                            <div class="flex">
                                                                <span class="currencyCode">&#8358;</span>
                                                                <input id="amount" name="amount" type="number" <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="v-money form__input form__input--sales" autofocus="autofocus" placeholder="0.00" value="<?php echo $totalAmount == 0 ? "" : $totalAmount;  ?>"/>
                                                            </div>
                                                   </form>
                                                    </div>

                                                </div>
                                                <div class="sales__display__right">
                                                    <div class="display__cell">
                                                        <button class="btn btn--check removeValue" <?php echo $inputCheck > 1 ? "disabled" : ""; ?>>
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                                                <defs>
                                                                    <path id="addde" d="M27.902 22.77H39a1 1 0 0 1 1 1v.46a1 1 0 0 1-1 1H27.902l4.335 4.669c.446.48.446 1.26 0 1.74-.447.481-1.17.481-1.617 0l-6.033-6.497A1 1 0 0 1 24 24.23v-.462a1 1 0 0 1 .587-.91l6.033-6.499c.447-.48 1.17-.48 1.617 0 .446.481.446 1.26 0 1.741l-4.335 4.668z"></path>
                                                                </defs>
                                                                <use fill="#8095A0" fill-rule="evenodd" transform="translate(-24 -16)" xlink:href="#addde"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <div class="select">
                                                        <select class="select__input select__input--sales currency" style="margin-top:3px">
                                                            <option <?php echo $schoolDetails->currency == "Naira" ? "Selected" : "" ?> value="Naira">Nigerian Naira (NGN)</option>
                                                            <option <?php echo $schoolDetails->currency == "Dollar" ? "Selected" : "" ?> value="Dollar">American Dollars (USD)</option>
                                                            <option <?php echo $schoolDetails->currency == "Rand" ? "Selected" : "" ?> value="Rand">South African Rand (ZAR)</option>
                                                            <option <?php echo $schoolDetails->currency == "Shilling" ? "Selected" : "" ?> value="Shilling">Kenyan Shilling (KES)</option>
                                                            <option <?php echo $schoolDetails->currency == "Cedi" ? "Selected" : "" ?> value="Cedi">Ghanaian Cedi (GHS)</option>
                                                            <option <?php echo $schoolDetails->currency == "Pounds" ? "Selected" : "" ?> value="Pounds">Pounds (GBP)</option>
                                                            <option <?php echo $schoolDetails->currency == "Euro" ? "Selected" : "" ?> value="Euro">Euro (EUR)</option>
                                                        </select>
                                                        <div class="select__icon select__icon--sales">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                                                                <path fill="#8095A0" fill-rule="evenodd" d="M6.03 3.03L3.025.025.02 3.03h6.01zM5.88 7.071L2.975 9.975.07 7.07H5.88z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="display__cell">
                                                        <button class="btn my_hover_up" style="margin-left:20px; margin-top:3px; color:#FFFFFF; background:#FB5574; font-size:18px">
                                                            Refund Money
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sales__bottom">
                                            <div class="sales__bottom__left">
                                                <div class="keypad">
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">1</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">2</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">3</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">4</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">5</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">6</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">7</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">8</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">9</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">0</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">00</button>
                                                    <button <?php echo $inputCheck > 1 ? "disabled" : ""; ?> class="keypad__btn">000</button>
                                                </div>
                                            </div>
                                            <div class="sales__bottom__right">
                                                <div class="sales__bottom__top">
                                                    <div class="sales__header sales__header--noBackground" style="color:#8095A0; font-size:18px;">Choose Payment Method</div>
                                                    <div class="sales__method">
                                                        <div class="sales__method__item">
                                                            <input id="card" data-id="card" type="radio" value="card" name="paymentMethod" checked="checked" class="sales__method__item__radio">
                                                            <label for="card" class="sales__method__item__label">Pay Online With Card</label>
                                                        </div>
                                                        <div class="sales__method__item">
                                                            <input id="bank" data-id="bank" type="radio" value="bank" name="paymentMethod" class="sales__method__item__radio">
                                                            <label for="bank" class="sales__method__item__label">Pay Online With Bank account</label>
                                                        </div>
                                                        <div class="sales__method__item" data-target="#cashPaymentReceiptNoModal" data-toggle="modal">
                                                            <input id="cash" data-id="cash" type="radio" value="cash" name="paymentMethod" class="sales__method__item__radio">
                                                            <label for="cash" class="sales__method__item__label">Cash</label>
                                                        </div>
                                                        <div class="sales__method__item" data-target="#selectPaymentMethod" data-toggle="modal">
                                                            <input id="other" type="radio" value="cash" name="paymentMethod" class="sales__method__item__radio">
                                                            <label for="other" class="sales__method__item__label">Other Options</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sales__bottom__bottom">
                                                    <div class="sales__button">
                                                        <button class="btn btn--primary btn--lg btn--block btn--bbw processPayment" <?php echo $totalAmount == 0 ? "disabled" : ""?>>PROCESS <span style="font-weight:700"><span class="currencyCode">&#8358;</span><span class="v-money"><?php echo $totalAmount == 0 ? 0.00 : number_format($totalAmount)?></span><span> NOW</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   
                                    </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>