<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class paymentClass extends logic {    

    function addFee($feeName, $classToPay, $feeAmount, $schoolFeePart){
        $academicTerm = $this->getSchoolDetails()->academic_term;
        $academicSession = $this->getSchoolDetails()->academic_session;
        foreach($classToPay as $key => $value){
            $stmt = $this->uconn->prepare("SELECT id FROM fees WHERE class_code = ? AND fee_name = ? AND academic_term = ? AND academic_session = ?");
            $stmt->bind_param("ssss", $value, $feeName, $academicTerm, $academicSession);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $stmt = $this->uconn->prepare("INSERT INTO fees (fee_name, amount, class_code, academic_term, academic_session, schoolFeePart) VALUES (?,?,?,?,?,?)");
                $stmt->bind_param("sisssi", $feeName, $feeAmount, $value, $academicTerm, $academicSession, $schoolFeePart);
                $stmt->execute();
            }
        }
        $stmt->close();
        return true;
    }

    function getPayments($classCode, $term, $session){
        $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE class_code = ? AND academic_term = ? AND academic_session = ?");
        $stmt->bind_param("sss", $classCode, $term, $session);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $payments = array();
        if(count($stmt_result) == 0){
            $stmt->close();
            return $payments;
        }else{
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fee_name = $this->outputRefine($row["fee_name"]);
                $obj->amount = $row["amount"];
                $obj->id = $row["id"];
                $payments[] = $obj;
            }
            $stmt->close();
            return $payments;
        }
    }

    function updateStructure($ids, $amount){
        foreach($ids as $key => $value){
            $stmt = $this->uconn->prepare("UPDATE fees SET amount = ? WHERE id = ?");
            $stmt->bind_param("ii", $amount[$key], $value);
            $stmt->execute();
        }
        $stmt->close();
        return true;
    }

    function fetchedPayedStudent($className, $whoToFetch){
        if($whoToFetch == "all" && $className != "all"){
            $stmt = $this->uconn->prepare("SELECT students.fullname, payed_student.id, fees.fee_name, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.transRef, payed_student.payDate FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.className = ? ORDER BY payed_student.payDate DESC");
            $stmt->bind_param("s", $className);
        }else{
            if($className == "all" && $whoToFetch == "all"){
                $stmt = $this->uconn->prepare("SELECT students.fullname, payed_student.id, fees.fee_name, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.transRef, payed_student.payDate FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId ORDER BY payed_student.payDate DESC");
            }else{
                $stmt = $this->uconn->prepare("SELECT students.fullname, payed_student.id, fees.fee_name, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.transRef, payed_student.payDate FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.className = ? AND payed_student.studentNumber = ? ORDER BY payed_student.payDate DESC");
                $stmt->bind_param("ss", $className, $whoToFetch);
            }
        }
        $stmt->execute();
        $payedStudent = array();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) < 0){
            $stmt->close();
            return $payedStudent;
        }else{
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fullname = $row["fullname"];
                $obj->feeName = $row["fee_name"];
                $obj->amount = $row["amount"];
                $obj->session = $row["session"];
                $obj->term = $row["term"];
                $obj->date = $row["payDate"];
                $obj->id = $row["id"];
                $payedStudent[] = $obj;
            }
            $stmt->close();
            return $payedStudent;
        }
    }

    function getClassFee($className, $term){
        $classCode = $this->getClassCode($className);
        $stmt = $this->uconn->prepare("SELECT DISTINCT id, fee_name FROM fees WHERE class_code = ? AND academic_term = ?");
        $stmt->bind_param("si", $classCode, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $fees = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->feeName = $row["fee_name"];
                $fees[] = $obj;
            }
            $stmt->close();
            return $fees;
        }else{
            $stmt->close();
            return $fees;
        }
    }

    function paymentStat(){
        $term = $this->getSchoolDetails()->academic_term;
        $session = $this->getSchoolDetails()->academic_session;
        $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT studentNumber) AS payedStudentNumber, SUM(amount) AS totalFeePayed FROM payed_student WHERE term = ? AND session = ?");
        $stmt->bind_param("is", $term, $session);
        $stmt2 = $this->uconn->prepare("SELECT COUNT(DISTINCT outstanding_fee.id) AS totalNum, students.fullname FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber WHERE outstanding_fee.term = ? AND outstanding_fee.session = ?");
        $stmt2->bind_param("is", $term, $session);
        $stmt2->execute();
        $stmt2_result = $this->get_result($stmt2);
        $row2 = array_shift($stmt2_result);
        if($stmt->execute()){
            $stmt_result = $this->get_result($stmt);
            $obj = new stdClass();
            $row = array_shift($stmt_result);
            $obj->totalFeePayed = $row["totalFeePayed"];
            $obj->totalDebtors = $row2["totalNum"];
            $obj->payedStudentNumber = $row["payedStudentNumber"];
            $stmt->close();
            return $obj;
        }
    }

    function assignOutstandingFee($assignStudent, $assignSession, $assignTerm, $assignFee, $assignAmount){
        $stmt = $this->uconn->prepare("SELECT id FROM outstanding_fee WHERE studentNumber = ? AND feeId = ? AND term = ?");
        $stmt->bind_param("sii", $assignStudent, $assignFee, $assignTerm);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            return "exist";
        }else{
            $date = date("Y-m-d");
            $stmt = $this->uconn->prepare("INSERT INTO outstanding_fee (studentNumber, feeId, amount, session, term, date_added, added_by) VALUES (?,?,?,?,?,?,?)");
            $stmt->bind_param("siiiiss", $assignStudent, $assignFee, $assignAmount, $assignSession, $assignTerm, $date, $_SESSION["fullname"]);
            if($stmt->execute()){
                $stmt->close();
                return "done";
            }else{
                $stmt->close();
                return "error";
            }
        }
    }

    function searchOutstandingPayment($sortClass, $sortTerm, $sortSession, $sortFee, $searchKeyword){
        // NUMBER GIVEN
        if(empty($searchKeyword)){
            // NOT USING KEYWORD
            if(empty($sortClass) && empty($sortTerm) && empty($sortSession) && empty($sortFee)){
                $academicTerm = $this->getSchoolDetails()->academic_term;
                $academicSession = $this->getSchoolDetails()->academic_session;
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.term = ?");
                $stmt->bind_param("si", $academicSession, $academicTerm);
            }
            elseif($sortClass == "all" && $sortTerm == "all" && $sortFee == "all"){
                // IF ALL FIELDS IS ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ?");
                $stmt->bind_param("s", $sortSession);
            }
            elseif($sortClass != "all" && $sortTerm == "all" && $sortFee == "all"){
                // SORT CLASS IS NOT ALL.....SPECIFIC CLASS
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("ss", $sortSession, $sortClass);
            }
            elseif($sortClass != "all" && $sortTerm != "all" && $sortFee == "all"){
                // SORT CLASS AND SORTTERM IS NOT ALL....
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.term = ? AND outstanding_fee.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("sis", $sortSession, $sortTerm, $sortClass);
            }
            elseif($sortClass != "all" && $sortTerm != "all" && $sortFee != "all"){
                // NONE OF THE FIELD IS ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.term = ? AND outstanding_fee.feeId = ? AND outstanding_fee.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("siis", $sortSession, $sortTerm, $sortFee, $sortClass);
            }
            elseif($sortClass == "all" && $sortTerm != "all" && $sortFee == "all"){
                // SORTTERM IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.term = ?");
                $stmt->bind_param("si", $sortSession, $sortTerm);
            }
            elseif($sortClass != "all" && $sortTerm == "all" && $sortFee != "all"){
                // SORTCLASS AND SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.feeId = ? AND outstanding_fee.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("sis", $sortSession, $sortFee, $sortClass);
            }
            elseif($sortClass == "all" && $sortTerm != "all" && $sortFee != "all"){
                // SORTTERM AND SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.term = ? AND outstanding_fee.feeId = ?");
                $stmt->bind_param("sii", $sortSession, $sortTerm, $sortFee);
            }
            elseif($sortClass == "all" && $sortTerm == "all" && $sortFee != "all"){
                // SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.session = ? AND outstanding_fee.feeId = ?");
                $stmt->bind_param("si", $sortSession, $sortFee);
            }
        }else{
            // USING KEYWORD
            $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, outstanding_fee.amount, outstanding_fee.session, outstanding_fee.term, fees.fee_name FROM outstanding_fee INNER JOIN students ON students.student_number = outstanding_fee.studentNumber INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE outstanding_fee.studentNumber = ? OR students.fullname = ?");
            $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
        }
        // EXECUTE STATEMENT
        if($stmt->execute()){
            $debtors = array();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->fullname = $row["fullname"];
                    $obj->presentClass = $row["present_class"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $obj->amount = $row["amount"];
                    $obj->session = $row["session"];
                    $obj->term = $row["term"];
                    $obj->feeName = $row["fee_name"];
                    $debtors[] = $obj;
                }
                $stmt->close();
                return $debtors;
            }else{
                $stmt->close();
                return $debtors;
            }
        }
    }

    function searchPayedStudent($sortClass, $sortTerm, $sortSession, $sortFee, $searchKeyword){
        // NUMBER GIVEN
        if(empty($searchKeyword)){
            // NOT USING KEYWORD
            if(empty($sortClass) && empty($sortTerm) && empty($sortSession) && empty($sortFee)){
                $academicTerm = $this->getSchoolDetails()->academic_term;
                $academicSession = $this->getSchoolDetails()->academic_session;
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ?");
                $stmt->bind_param("si", $academicSession, $academicTerm);
            }
            elseif($sortClass == "all" && $sortTerm == "all" && $sortFee == "all"){
                // IF ALL FIELDS IS ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ?");
                $stmt->bind_param("s", $sortSession);
            }
            elseif($sortClass != "all" && $sortTerm == "all" && $sortFee == "all"){
                // SORT CLASS IS NOT ALL.....SPECIFIC CLASS
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("ss", $sortSession, $sortClass);
            }
            elseif($sortClass != "all" && $sortTerm != "all" && $sortFee == "all"){
                // SORT CLASS AND SORTTERM IS NOT ALL....
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ? AND payed_student.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("sis", $sortSession, $sortTerm, $sortClass);
            }
            elseif($sortClass != "all" && !empty($sortClass) && $sortTerm != "all" && $sortFee != "all"){
                // NONE OF THE FIELD IS ALL AND SORTCLASS IS NOT EMPTY
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ? AND payed_student.feeId = ? AND payed_student.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("siis", $sortSession, $sortTerm, $sortFee, $sortClass);
            }
            elseif($sortClass == "all" && $sortTerm != "all" && $sortFee == "all"){
                // SORTTERM IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ?");
                $stmt->bind_param("si", $sortSession, $sortTerm);
            }
            elseif($sortClass != "all" && $sortTerm == "all" && $sortFee != "all"){
                // SORTCLASS AND SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.feeId = ? AND payed_student.studentNumber IN (SELECT student_number FROM students WHERE present_class = ?)");
                $stmt->bind_param("sis", $sortSession, $sortFee, $sortClass);
            }
            elseif($sortClass == "all" && $sortTerm != "all" && $sortFee != "all"){
                // SORTTERM AND SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ? AND payed_student.feeId = ?");
                $stmt->bind_param("sii", $sortSession, $sortTerm, $sortFee);
            }
            elseif($sortClass == "all" && $sortTerm == "all" && $sortFee != "all"){
                // SORTFEE IS NOT ALL
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.feeId = ?");
                $stmt->bind_param("si", $sortSession, $sortFee);
            }
            elseif(empty($sortClass) && $sortTerm != "all" && $sortFee != "all"){
                // SORT CLASS IS EMPTY
                $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.feeId = ? AND payed_student.term = ?");
                $stmt->bind_param("sii", $sortSession, $sortFee, $sortTerm);
            }
        }else{
            // USING KEYWORD
            $stmt = $this->uconn->prepare("SELECT students.fullname, students.student_number, students.present_class, students.profilePhoto, payed_student.amount, payed_student.session, payed_student.term, payed_student.payMode, payed_student.payDate, fees.fee_name FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.studentNumber = ? OR students.fullname = ?");
            $stmt->bind_param("ss", $searchKeyword, $searchKeyword);
        }
        // EXECUTE STATEMENT
        if($stmt->execute()){
            $payedStudent = array();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->fullname = $row["fullname"];
                    $obj->presentClass = $row["present_class"];
                    $obj->profilePhoto = $row["profilePhoto"];
                    $obj->amount = $row["amount"];
                    $obj->session = $row["session"];
                    $obj->term = $row["term"];
                    $obj->feeName = $row["fee_name"];
                    $obj->studentNumber = $row["student_number"];
                    $obj->payDate = $row["payDate"];
                    switch ($row["payMode"]){
                        case 1:
                            $mode = "Card";
                            break;
                        case 2:
                            $mode = "Bank Account";
                            break;
                        case 3:
                            $mode = "Cash";
                            break;
                        default:
                            $mode = "Not Available";
                            break;
                    }
                    $obj->payMode = $mode;
                    $payedStudent[] = $obj;
                }
                $stmt->close();
                return $payedStudent;
            }else{
                $stmt->close();
                return $payedStudent;
            }
        }
    }

    function feeSumary($session, $term){
        $stmt = $this->uconn->prepare("SELECT COUNT(DISTINCT payed_student.studentNumber) AS totalNumber, fees.fee_name, fees.id, SUM(payed_student.amount) AS amount FROM payed_student INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.session = ? AND payed_student.term = ? GROUP BY fees.fee_name");
        $stmt->bind_param("si", $session, $term);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $summary = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->feeName = $row["fee_name"];
                $obj->totalNumber = $row["totalNumber"];
                $obj->amount = $row["amount"];
                $obj->id = $row["id"];
                $summary[] = $obj;
            }
            $stmt->close();
            return $summary;
        }else{
            $stmt->close();
            return $summary;
        }
    }

    function recentPayment($name){
        if($name == "all"){
            $stmt = $this->uconn->prepare("SELECT students.fullname, fees.fee_name, payed_student.id FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId ORDER BY payed_student.id DESC LIMIT 10");
        }else{
            $stmt = $this->uconn->prepare("SELECT students.fullname, fees.fee_name, payed_student.id FROM payed_student INNER JOIN students ON students.student_number = payed_student.studentNumber INNER JOIN fees ON fees.id = payed_student.feeId WHERE students.fullname LIKE CONCAT('%',?,'%')");
            $stmt->bind_param("s", $name);
        }
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $payment = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fullname = $row["fullname"];
                $obj->feeName = $row["fee_name"];
                $obj->id = $row["id"];
                $payment[] = $obj;
            }
            $stmt->close();
            return $payment;
        }else{
            $stmt->close();
            return $payment;
        }
    }

    function receiptDetails($payId){
        $stmt = $this->uconn->prepare("SELECT students.gender, students.present_class, payed_student.session, payed_student.term, students.fullname, fees.fee_name, payed_student.payDate, payed_student.amount, payed_student.transRef FROM payed_student INNER JOIN students ON payed_student.studentNumber = students.student_number INNER JOIN fees ON fees.id = payed_student.feeId WHERE payed_student.id = ?");
        $stmt->bind_param("i", $payId);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $payDetails = array();
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->fullname = $row["fullname"];
            $obj->gender = $row["gender"];
            $obj->presentClass = $row["present_class"];
            $obj->session = $row["session"];
            switch($row["term"]){
                case 1:
                    $term = "1st Term";
                    break;
                case 2:
                    $term = "2nd Term";
                    break;
                case 3:
                    $term = "3rd Term";
                    break;
                default:
                    $term = "N/A";
            }
            $obj->term = $term;
            $payDetails[] = $obj;
            // PUT OTHER OBJECT CONTAINING PAYMENT DETAILS
            $obj = new stdClass();
            $obj->feeName = $row["fee_name"];
            $obj->amount = $row["amount"];
            $obj->payDate = $row["payDate"];
            $obj->receiptNo = $row["transRef"];
            $payDetails[] = $obj;

            $stmt->close();
            return $payDetails;
        }else{
            $stmt->close();
            return $payDetails;
        }
    }

    function getClassFeeWithSession($paymentSession, $paymentTerm, $paymentClass){
        $class_code = $this->getClassCode($paymentClass);
        $stmt = $this->uconn->prepare("SELECT * FROM fees WHERE academic_session = ? AND academic_term = ? AND class_code = ?");
        $stmt->bind_param("sis", $paymentSession, $paymentTerm, $class_code);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $fees = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->id = $row["id"];
                $obj->feeName = $row["fee_name"];
                $fees[] = $obj;
            }
            $stmt->close();
            return $fees;
        }else{
            $stmt->close();
            return $fees;
        }
    }

    function feesPayment($state, $totalAmount, $currency, $methodValue, $student_id, $student_class, $student_fee, $student_term, $student_session, $transRef, $fullname){
        $loopNum = count($state);
        if($loopNum == 1){
            $amount = $totalAmount;
            $reply = $this->logPayment($state[0], $amount, $currency, $methodValue, $student_id, $student_class, $student_fee[0], $student_term, $student_session, $transRef, $fullname);
        }else{
            for($i = 0; $i < $loopNum; $i++){
                $realObject = $this->getFeeDetails($student_fee[$i], $state[$i], $student_id);
                $amount = $realObject->amount;
                $reply = $this->logPayment($state[$i], $amount, $currency, $methodValue, $student_id, $student_class, $student_fee[$i], $student_term, $student_session, $transRef, $fullname);
            }
        }
        return "true";
    }

    function logPayment($state, $amount, $currency, $method, $student_id, $student_class, $student_fee, $student_term, $student_session, $ref, $fullname){
        $realObject = $this->getFeeDetails($student_fee, $state, $student_id);
        $realAmount = $realObject->amount;
        $feeName = $realObject->fee_name;
        $student_fee = $realObject->fee_id;
        $datee = date("Y-m-d");

        if($amount > $realAmount){
            return "excess";
        }else{
            if($state == 1){
                // SUBTRACT FROM OUTSTANDING FEE
                $outstandingAmount = $realAmount - $amount;
                if($outstandingAmount == 0){
                    $stmt = $this->uconn->prepare("DELETE FROM outstanding_fee WHERE studentNumber = ? AND feeId = ? AND session = ? AND term = ?");
                    $stmt->bind_param("siss", $student_id, $student_fee, $student_session, $student_term);
                    $stmt->execute();
                }else{
                    $stmt = $this->uconn->prepare("UPDATE outstanding_fee SET amount = ? WHERE feeId = ? AND studentNumber = ?");
                    $stmt->bind_param("iis", $outstandingAmount, $student_fee, $student_id);
                    $stmt->execute();
                }
            }else{
                // NORMAL PAYMENT
                // CALCULATE OUTSTANDING FEE AND PUT IN OUTSTANDING
                $outstandingAmount = $realAmount - $amount;
                if($outstandingAmount != 0){
                    // CHECK IF EXIST
                    $stmt = $this->uconn->prepare("SELECT id FROM outstanding_fee WHERE studentNumber = ? AND term = ? AND session = ? AND feeId = ?");
                    $stmt->bind_param("sisi", $student_id, $student_term, $student_session, $student_fee);
                    $stmt->execute();
                    $stmt_result = $this->get_result($stmt);
                    if(count($stmt_result) > 0){
                        $stmt = $this->uconn->prepare("UPDATE outstanding_fee SET amount = ? WHERE feeId = ? AND studentNumber = ?");
                        $stmt->bind_param("iis", $outstandingAmount, $student_fee, $student_id);
                        $stmt->execute();
                    }else{
                        $stmt = $this->uconn->prepare("INSERT INTO outstanding_fee (studentNumber, feeId, amount, session, term, date_added, added_by) VALUES (?,?,?,?,?,?,?)");
                        $stmt->bind_param("siissss", $student_id, $student_fee, $outstandingAmount, $student_session, $student_term, $datee, $_SESSION["fullname"]);
                        $stmt->execute();
                    }
                }else{
                    if($realAmount == $amount){
                        $stmt = $this->uconn->prepare("DELETE FROM outstanding_fee WHERE studentNumber = ? AND feeId = ? AND session = ? AND term = ?");
                        $stmt->bind_param("siss", $student_id, $student_fee, $student_session, $student_term);
                        $stmt->execute();
                    }else{
                        $stmt = $this->uconn->prepare("UPDATE outstanding_fee SET amount = ? WHERE feeId = ? AND studentNumber = ?");
                        $stmt->bind_param("iis", $outstandingAmount, $student_fee, $student_id);
                        $stmt->execute();
                    }
                }
            }
            $stmt = $this->uconn->prepare("INSERT INTO payed_student (studentNumber, className, feeId, amount, session, term, payMode, transRef, payDate, processed_by) VALUES (?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("ssiisiisss", $student_id, $student_class, $student_fee, $amount, $student_session, $student_term, $method, $ref, $datee, $fullname);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }
    }

    function getPaymentDetailsBulk($familyId){
        $reply = $this->getFamilyDetails($familyId);
        $obj = new stdClass();
        $key = $this->getSchoolDetails()->keys["paystack_public"];
        if(empty($reply->parentEmail)){
            $obj->email = $this->getSchoolDetails()->email;
            $obj->subAccount = $this->getSchoolDetails()->subAccount;
            $obj->pkey = $key;
            return $obj;
        }else{
            $obj->email = $reply->parentEmail;
            $obj->subAccount = $this->getSchoolDetails()->subAccount;
            $obj->pkey = $key;
            return $obj;
        }
    }

    function getFeeIdFromName($feeName, $currentClass, $currentSession, $currentTerm){
        $class_code = $this->getClassCode($currentClass);
        $stmt = $this->uconn->prepare("SELECT id FROM fees WHERE fee_name = ? AND class_code = ? AND academic_term = ? AND academic_session = ?");
        $stmt->bind_param("ssis", $feeName, $class_code, $currentTerm, $currentSession);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $feeId = array_shift($stmt_result)["id"];
        }else{
            $feeId = 0;
        }
        $stmt->close();
        return $feeId;
    }

    function updateBulkPayment($method, $currency, $students, $classes, $feeName, $amount, $session, $term, $verify, $fullname){;
        for($i = 0; $i < count($amount); $i++){
            if($amount[$i] != "" && $amount[$i] != 0){
                $currentStudent = $students[$i];
                $currentAmount = $amount[$i];
                $currentClass = $classes[$i];
                $state = 0; // 0 MEANS STUDENTS IS NOT PAYING AN OUTSTANDING FEE
                $feeId = $this->getFeeIdFromName($feeName, $currentClass, $session, $term);
                $this->logPayment($state, $currentAmount, $currency, $method, $currentStudent, $currentClass, $feeId, $term, $session, $verify, $fullname);
            }
        }
        return true;
    }

    function staffsOnPayroll(){
        $mark = date("Y-m-d");
        $date = new DateTime($mark);
        $date->modify('first day of this month');
        $startTime = $date->format('Y-m-d');
        $date->modify('last day of this month');
        $date->modify('+ 1 day');
        $endTime = $date->format('Y-m-d');
        $settingsId = 1;
        $stmt = $this->uconn->prepare("SELECT teachers.fullname, teachers.department, teachers.profilePhoto, teachers.staff_number, SUM(SUBSTRING_INDEX(attendancerecord.date_created, ' ', -1) > settings.staff_lateness_start AND attendancerecord.date_created BETWEEN CONCAT(?, '%') AND CONCAT(?, '%')) * settings.lateness_fee AS lateFee FROM teachers LEFT JOIN attendancerecord ON attendancerecord.attendanceNumber = teachers.staff_number LEFT JOIN settings ON settings.id = ? GROUP by teachers.staff_number");
        $stmt->bind_param("ssi", $startTime, $endTime, $settingsId);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $payroll = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fullname = $row["fullname"];
                $obj->department = $row["department"];
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->staffNumber = $row["staff_number"];
                $obj->lateFee = $row["lateFee"];
                $payroll[] = $obj;
            }
        }
        $stmt->close();
        return $payroll;
    }

    function notifyOutstandingFee($studentNumber){
        $stmt = $this->uconn->prepare("SELECT fullname, father_phone, mother_phone FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $parentContact = array();
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            if(!empty($row["father_phone"])){
                $parentContact[] = $row["father_phone"];
            }
            if(!empty($row["mother_phone"])){
                $parentContact[] = $row["mother_phone"];
            }
            $fullname = $row["fullname"];
        }
        if(count($parentContact) > 0){
            $stmt = $this->uconn->prepare("SELECT fees.fee_name, outstanding_fee.amount, outstanding_fee.term, outstanding_fee.session FROM outstanding_fee INNER JOIN fees ON fees.id = outstanding_fee.feeId WHERE studentNumber = ?");
            $stmt->bind_param("s", $studentNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $outstandingFees = array();
                while($row = array_shift($stmt_result)){
                    $obj = new stdClass();
                    $obj->feeName = $row["fee_name"];
                    $obj->amount = $row["amount"];
                    $obj->term = $row["term"];
                    $obj->session = ($row["session"] - 1)."/".$row["session"];
                    $outstandingFees[] = $obj;
                }
                $reply = $this->sendOutstandingFeeMessage($parentContact, $outstandingFees, $fullname);
                if($reply === true){
                    $reply = "true";
                }else{
                    switch($reply){
                        case 1702 :
                            $reply = "invalidparam";
                            break;
                        case 1703 :
                            $reply = "invalidlogin";
                            break;
                        case 1704:
                            $reply = "insufficientunit";
                            break;
                        case 1705:
                            $reply = "numbers";
                            break;
                        case 1706:
                            $reply = "internal";
                            break;
                        default:
                            $reply = "smserror";
                            break;
                    }
                }
                $stmt->close();
                return $reply;
            }
        }else{
            $stmt->close();
            return "noNumbers";
        }
    }

    function sendOutstandingFeeMessage($parentContact, $outstandingFees, $fullname){
        $message = "Good day, this is to remind you of your ward, ".ucwords($fullname)."'s outstanding payment in ".$this->getSchoolDetails()->school_name.". Details of the payment can be found below. \n";
        foreach($outstandingFees as $key => $value){
            $newFee = "Session: ".$value->session."\n Term: ".$value->term."\n Fee Name: ".$value->feeName."\n Amount: ".$value->amount;
            $message .= $newFee;
        }
        $sender = strtoupper($this->getSchoolDetails()->domain_name);
        $reply = $this->sendSMS($message, $sender, $parentContact);
        return $reply;
    }
}
?>