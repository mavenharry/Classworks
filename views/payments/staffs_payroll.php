<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    $payrollStaffs = $paymentClass->staffsOnPayroll();
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#salaryDisburseModal" data-toggle="modal" style="margin-left:10px; color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-pie-chart-3"></i>
                                        <span style="font-size: .9rem;">Disburse All Salaries</span>
                                    </a>
                                    <a data-target="#salarySearchModal" data-toggle="modal" style="margin-left:10px; color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL STAFF(S) SALARIES/PAYROLL</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Name</th>
                                                <th>Department</th>
                                                <th>Salary</th>
                                                <th>Deductions</th>
                                                <th>Last Pay</th>
                                                <th>Next Pay</th>
                                                <th class="text-center">Make Payment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($payrollStaffs) == 0){
                                                    echo "No staffs on payroll";
                                                }else{
                                                    foreach($payrollStaffs as $key => $value){ 
                                                        if(!empty($value->fullname)){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>
                                                                <?php
                                                                    if(empty($value->profilePhoto)){ ?>
                                                                        <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                        class="badge"><?php echo $nameInitials = $paymentClass->createAcronym(ucwords($value->fullname)); ?></a>
                                                                    <?php }else{ ?>
                                                                        <div class="user-with-avatar">
                                                                            <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto;?>">
                                                                        </div>
                                                                    <?php }
                                                                ?>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->fullname); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->department); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span>&#x20a6;120,000</span>
                                                                </td>
                                                                <td>
                                                                    <span>&#x20a6;<?php echo (int)$value->lateFee; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span>12/05/2018</span>
                                                                </td>
                                                                <td>
                                                                    <span>12/06/2018</span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                        class="my_hover_up_bg badge" href="">Pay This Person</a>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                }
                                            ?>  
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>