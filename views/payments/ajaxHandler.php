<?php include_once ("paymentbtn.php"); 


if(isset($_GET["className"])){
    $className = htmlentities(trim($_GET["className"]));
    $classStudents = $paymentClass->getallClassStudents($className);

    if(count($classStudents) == 0){ ?>
        <option value="">No student in this class</option>
    <?php }else{ ?>
        <option value="">All student</option>
        <?php foreach($classStudents as $key => $value){ ?>
            <option value="<?php echo $value->student_number?>"><?php echo ucwords($value->fullname); ?></option>
        <?php }
    }
}

if(isset($_GET["classFee"]) && isset($_GET["term"])){
    $className = htmlentities(trim($_GET["classFee"]));
    $term = htmlentities(trim($_GET["term"]));
    $fees = $paymentClass->getClassFee($className, $term);

    if(count($fees) == 0){ ?>
        <option value="">No fee for this class</option>
    <?php }else{
        foreach($fees as $key => $value){ ?>
            <option value="<?php echo $value->id?>"><?php echo ucwords($value->feeName); ?></option>
        <?php }
    }
}

if(isset($_GET["studentIdStudent"]) && isset($_GET["paymentSessionStudent"]) && isset($_GET["paymentTermStudent"]) && isset($_GET["paymentClassStudent"])){
    $paymentSession = htmlentities(trim($_GET["paymentSessionStudent"]));
    $paymentTerm = htmlentities(trim($_GET["paymentTermStudent"]));
    $selectedClass = htmlentities(trim($_GET["paymentClassStudent"]));
    $studentId = htmlentities(trim($_GET["studentIdStudent"]));

    $fees = $paymentClass->getStudentFees($paymentSession, $paymentTerm, $selectedClass, $studentId);

    if(count($fees) == 0){ ?>
        <option value="">No fee for this class</option>
    <?php }else{ 
        foreach($fees as $key => $value){ ?>
            <option value="<?php echo $value->state ?><?php echo $value->id ?>"><?php echo ucwords($value->feeName); ?></option>
        <?php }
     }
}

if(isset($_GET["paymentSession"]) && isset($_GET["paymentTerm"]) && isset($_GET["paymentClass"])){
    $paymentSession = htmlentities(trim($_GET["paymentSession"]));
    $paymentTerm = htmlentities(trim($_GET["paymentTerm"]));
    $selectedClass = htmlentities(trim($_GET["paymentClass"]));

    $fees = $paymentClass->getClassFeeWithSession($paymentSession, $paymentTerm, $selectedClass);

    if(count($fees) == 0){ ?>
        <option value="">No fee for this class</option>
    <?php }else{ 
        foreach($fees as $key => $value){ ?>
            <option value="<?php echo $value->id ?>"><?php echo ucwords($value->feeName); ?></option>
        <?php }
     }
}

if(isset($_GET["processPayment"]) && $_GET["processPayment"] == 1){
    $amount = htmlentities(trim($_GET["amount"]));
    $currency = htmlentities(trim($_GET["currency"]));
    $method = htmlentities(trim($_GET["method"]));
    $student_id = htmlentities(trim($_GET["student_id"]));
    $student_class = htmlentities(trim($_GET["student_class"]));
    $student_fee = explode("*", htmlentities(trim($_GET["student_fee"])));
    $student_term = htmlentities(trim($_GET["student_term"]));
    $student_session = htmlentities(trim($_GET["student_session"]));
    $transRef = htmlentities(trim($_GET["transRef"]));
    $state = explode("*", htmlentities(trim($_GET["state"])));
    $fullname = $_SESSION["fullname"];
    switch($method){
        case 'card':
            $methodValue = 1;
            break;
        case 'bank':
            $methodValue = 2;
            break;
        case 'cash':
            $methodValue = 3;
            break;
        case 'offlinebank':
            $methodValue = 4;
            break;
        case 'pos':
            $methodValue = 5;
            break;
        default:
            $methodValue = 6;
            break;
    }

    if($currency == $paymentClass->getSchoolDetails()->currency){
        // SAME CURRENCY
        if($method == "card" || $method == "bank"){
            $reply = $paymentClass->verifyRef($transRef);
        }else{
            $reply = true;
        }
        if($reply === true){
            $reply = $paymentClass->feesPayment($state, $amount, $currency, $methodValue, $student_id, $student_class, $student_fee, $student_term, $student_session, $transRef, $fullname);
        }else{
            $reply = "wrongRef";
        }
        switch($reply){
            case 'true':
                $mess = "done";
                break;
            case 'false':
                $mess = "error";
                break;
            case 'exist':
                $mess = "exist";
                break;
            case 'excess':
                $mess = "excess";
                break;
            case 'wrongRef':
                $mess = "wrongRef";
                break;
            default:
                $mess = $reply;
        }
        echo $mess;
    }
}

if(isset($_GET["getAcctCode"]) && $_GET["getAcctCode"] == 1){
    echo $paymentClass->getSchoolDetails()->subAccount;
}

if(isset($_GET["action"]) && $_GET["action"] == "update sms"){
    $amount = htmlentities(trim($_GET["amount"]));
    $smsEqiv = floor($amount/3);
    $smsLeft = $paymentClass->checkSmsBalance();
    $newBalance = $smsLeft + $smsEqiv;
    $reply = $paymentClass->updateSmsUnit($newBalance);
    if($reply === true){
        echo "true";
    }else{
        echo "false";
    }
}

if(isset($_GET["getChildren"])){
    $familyId = htmlentities(trim($_GET["getChildren"]));
    $reply = $paymentClass->getFamilyChildren($familyId);
    if(count($reply) == 0){
        echo "noStudent";
    }else{
        foreach($reply as $key => $value){ ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Student Name</label>
                    <input type="text" class="form-control" value="<?php echo ucwords($value->fullname); ?>">
                </div>
                <input type="hidden" class="form-control" name="students[]" value="<?php echo $value->studentNumber?>">
                <input type="hidden" class="form-control" name="classes[]" value="<?php echo $value->presentClass; ?>">
                <div class="form-group col-sm-6">
                    <label for="">Amount</label>
                    <input type="number" name="amount[]" class="form-control" placeholder="0.00">
                </div>
            </div>
        <?php }
    }
}

if(isset($_GET["getBulkPaymentDetails"]) && $_GET["getBulkPaymentDetails"] == 1){
    $getBulkPaymentDetails = htmlentities(trim($_GET["getBulkPaymentDetails"]));
    $familyId = htmlentities(trim($_GET["familyId"]));
    $reply = $paymentClass->getPaymentDetailsBulk($familyId);
    echo json_encode($reply);
}

if(isset($_GET["bulkPayment"]) && $_GET["bulkPayment"] == 1){
    $method = htmlentities(trim($_GET["method"]));
    $students = $_GET["students"];
    $classes = $_GET["classes"];
    $feeName = htmlentities(trim($_GET["feeName"]));
    $amount = $_GET["amount"];
    $session = htmlentities(trim($_GET["session"]));
    $term = htmlentities(trim($_GET["term"]));
    $verify = htmlentities(trim($_GET["verify"]));
    $currency = htmlentities(trim($_GET["currency"]));
    $fullname = $_SESSION["fullname"];
    switch($method){
        case 'card':
            $methodValue = 1;
            break;
        case 'bank':
            $methodValue = 2;
            break;
        case 'cash':
            $methodValue = 3;
            break;
        case 'offlinebank':
            $methodValue = 4;
            break;
        case 'pos':
            $methodValue = 5;
            break;
        default:
            $methodValue = 6;
            break;
    }
    if($method == "card" || $method == "bank"){
        $reply = $paymentClass->verifyRef($verify);
    }else{
        $reply = true;
    }

    if($reply === true){
        $reply = $paymentClass->updateBulkPayment($methodValue, $currency, $students, $classes, $feeName, $amount, $session, $term, $verify, $fullname);
        if($reply === true){
            echo "true";
        }else{
            echo "false";
        }
    }else{
        echo "wrongRef";
    }
}

if(isset($_GET["action"]) && $_GET["action"] == "notifyOutstandingFeeParent"){
    $studentNumber = htmlentities(trim($_GET["stdnNumber"]));
    $reply = $paymentClass->notifyOutstandingFee($studentNumber);
    echo $reply;
}

if(isset($_GET["action"]) && $_GET["action"] == "getStudentPayment"){
    $payingTerm = htmlentities(trim($_GET["payingTerm"]));
    $payingSession = htmlentities(trim($_GET["payingSession"]));
    $studentNumber = htmlentities(trim($_GET["studentNumber"]));
    $reply = $paymentClass->getStudentFeesPayment($studentNumber, $payingSession, $payingTerm);
    if(count($reply) > 0){
        foreach($reply as $key => $fee){ ?>
            <option value="<?php echo $fee->state?><?php echo $fee->id?>"><?php echo ucwords($fee->feeName); ?></option>
        <?php }
    }else{ ?>
        <option>No fee applicable</option>
    <?php }
}
?>