<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    if(isset($_POST["searchFeeDetails"])){
        $paidSession = htmlentities(trim($_POST["paidSession"]));
        $paidTerm = htmlentities(trim($_POST["paidTerm"]));
        $summary = $paymentClass->feeSumary($paidSession, $paidTerm);
    }else{
        $paidSession = $schoolDetails->academic_session;
        $paidTerm = $schoolDetails->academic_term;
        $summary = $paymentClass->feeSumary($paidSession, $paidTerm);
    }
    $term = isset($_POST["paidTerm"]) ? $_POST["paidTerm"] : $schoolDetails->academic_term;
    switch($term){
        case 1:
            $term = "1st Term";
            break;
        case 2:
            $term = "2nd Term";
            break;
        case 3;
            $term = "3rd Term";
            break;
        default:
            $term = "N/A";
    }
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                    <i class="os-icon os-icon-printer"></i>
                                    <span style="font-size: .9rem;">Print This</span>
                                </a>
                                </div>
                                <h6 class="element-header">PAID FEES BREAKDOWN/ANALYSIS</h6>

                                
                                <div class="row" style="margin-top:20px;">

                                <div class="col-lg-8">
                                    <form method="POST" style="margin-bottom:20px; margin-top:10px; margin-left:5px;">
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                                <select name="paidSession" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                                                    <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                                                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                                                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                                                    <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="paidTerm" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                                    <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                                                    <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                                                    <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="submit" class="btn btn-primary" name="searchFeeDetails" value="Search Fee Details">
                                            </div>
                                        </div>
                                    </form>

                                    <div class="element-box">
                                        <h5 class="form-header">
                                            PAID FEES DETAILS FOR <span style="font-Weight: 800"> <?php echo isset($_POST["paidSession"]) ? ($_POST["paidSession"] - 1).'/'.$_POST["paidSession"] : ($schoolDetails->academic_session - 1).'/'.$schoolDetails->academic_session?> <?php echo $term;?></span>
                                        </h5>
                                        <div class="form-desc">
                                            Get complete termly school fees payments details and statistics for effective fees management and accounting at the niche of time.
                                        </div>
                                
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>No. of students</th>
                                                    <th>Total</th>
                                                    <th>Students</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($summary) > 0){
                                                        foreach($summary as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->feeName) ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo number_format($value->totalNumber)?></span>
                                                                </td>
                                                                <td>
                                                                    <span>&#x20a6;<?php echo number_format($value->amount)?></span>
                                                                </td>
                                                                <td>
                                                                    <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="./paid_students?fee_id=<?php echo base64_encode($value->id)?>&pt=<?php echo base64_encode($paidTerm)?>&ps=<?php echo base64_encode($paidSession)?>">View Student(s)</a>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }else{
                                                        echo "No data to display yet";
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-lg-4">

                                    <div class="element-box">
                                        <h5 class="form-header">
                                            PAID FEES SUMMARY
                                        </h5>
                                        <div class="form-desc">
                                            Overall fees payment analysis showing various fess payments and gross amount for paid for each fee. 
                                        </div>

                                        <!-- STUDENTS SLIP -->

                                        <div class="table-responsive">
                                        
                                            <div class="">
                                                <div class="el-chart-w"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                    <canvas height="442" id="pieChart1" width="442" style="display: block; width: 221px; height: 221px;"></canvas>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); 
$chartData = json_encode($summary);
?>

<script>
    summaryChart('<?php echo $chartData ?>');
    function shuffle(datas) {
        var input = datas;
        
        for (var i = input.length-1; i >=0; i--) {
        
            var randomIndex = Math.floor(Math.random()*(i+1)); 
            var itemAtIndex = input[randomIndex]; 
            
            input[randomIndex] = input[i]; 
            input[i] = itemAtIndex;
        }
        return input;
    }

    function summaryChart(data){
            // init pie chart if element exists
        var colorArray = shuffle(["#5797fc", "#7e6fff", "#4ecc48", "#ffcc29", "#f37070"]);
        var data = JSON.parse(data);
        if(data.length > 0){
            var labels = [];
            var color = [];
            var dataa = [];
            for(var i = 0; i < data.length; i++){
                labels.push(data[i]["feeName"]);
                color.push(colorArray[i]);
                dataa.push(data[i]["totalNumber"]);
            }
        }
        if ($("#pieChart1").length) {
            var pieChart1 = $("#pieChart1");
                // pie chart data
                // PAID FEE SUMMARY
                var pieData1 = {
                    labels: labels,
                    datasets: [{
                    data: dataa,
                    backgroundColor: color,
                    hoverBackgroundColor: color,
                    borderWidth: 0
                    }]
                };

                // -----------------
                // init pie chart
                // -----------------
                new Chart(pieChart1, {
                    type: 'pie',
                    data: pieData1,
                    options: {
                    legend: {
                        position: 'bottom',
                        labels: {
                        boxWidth: 15,
                        fontColor: '#3e4b5b'
                        }
                    },
                    animation: {
                        animateScale: true
                    }
                    }
                });
        }
      }
</script>


</body>

</html>