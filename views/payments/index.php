<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Payment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
    <link href="../../addons/scanner/style.css" rel="stylesheet">
</head>
<!-------------------- START - MODAL -------------------->
<?php
$schoolDetails = $paymentClass->getSchoolDetails();
include ("../../includes/modals/index.php"); 
?>
    <!-------------------- END - MODAL -------------------->
<?php
    $allStaff = $paymentClass->getAllStaffs();
    $statistics = $paymentClass->paymentStat();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php"); 
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">FINANCE/PAYMENTS MANGEMENT/ANALYSIS PANEL</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./paid_students">
                                                    <div class="label">No. of Students with paid fees</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user-male-circle"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo $statistics->payedStudentNumber;?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>
                                            
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./unpaid_students">
                                                    <div class="label">Students with outstanding fees</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-pie-chart-3"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo $statistics->totalDebtors?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./staffs_payroll">
                                                    <div class="label">No. of staffs on payroll</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-user"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo count($allStaff) ?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./paid_fees_breakdown">
                                                    <div class="label">Total Term's Fees payment</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="" style="font-size:2rem"><?php echo $schoolDetails->currencySymbol; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="value2 hideThis"><?php echo number_format($statistics->totalFeePayed); ?></div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./fees_structure.php" style="border: 2px dashed #08ACF0">
                                                    <div class="label">Scheduled fees structure</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-hierarchy-structure-2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Fees Structure</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./payment_receipts" style="border: 2px dashed #08ACF0">
                                                    <div class="label">View/Print fees Payment Receipts</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-documents-03"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Fees Receipts</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./payment_slips" style="border: 2px dashed #08ACF0">
                                                    <div class="label">View/Print fees Payment Slips</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-newspaper"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Payment Slips</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo"  data-target="#assignUnpaidStudentsModal" data-toggle="modal" href="#" style="border: 2px dashed #08ACF0">
                                                    <div class="label">Add outstanding fee to student</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Assign a Fee</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#paymentSwitch" data-toggle="modal" href="" style="border: 2px dashed #08ACF0">
                                                    <div class="label">Process new fee payment</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Process Payment</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#paymentSwitchBulk" data-toggle="modal" href="" style="border: 2px dashed #08ACF0">
                                                    <div class="label">Process new fee payment</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1">Bulk Payment</div>
                                                </a>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-------------------- END - Chat Popup Box -------------------->
                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
</body>

</html>