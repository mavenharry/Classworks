<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    $classes = $paymentClass->getClasses();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                </div>
                                <h6 class="element-header">ALL/SPECIFIC FEES PAYMENTS SLIP</h6>

                                
                                <div class="row" style="margin-top:30px;">

                                <div class="col-lg-5">

                                <div class="element-box">
                                <form>
                                <h5 class="form-header">
                                GENERATE FEE PAYMENT SLIP
                                </h5>
                                <div class="form-desc">
                                Generate overall or specific payment slips for any fees payable at any bank branch nationwide without any hassle.
                                </div>
                               
                                <form style="margin-bottom:25px; margin-top:10px; margin-left:5px;">
                                
                                <div class="form-group row">
                                
                                    <div class="col-sm-5">
                                        <label for=""> Class</label>
                                        <select id="sortSlipClass" style="font-size:1rem; border-radius:5px;" class="form-control form-control-sm bright">
                                            <option value="">Select class</option>
                                            <?php foreach($classes as $key => $value){ ?>
                                                    <option><?php echo $value->class_name; ?></option>
                                                <?php }?>
                                        </select>
                                    </div>

                                    <div class="col-sm-7">
                                        <label for=""> Student</label>
                                        <select id="sortSlipStudent" style="font-size:1rem; border-radius:5px;" class="form-control form-control-sm bright">
                                            <option value="">Select student</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row" style="margin-top:30px;">
                                
                                    <div class="col-sm-6">
                                        <label for=""> Session</label>
                                        <select style="font-size:1rem; border-radius:5px;" class="form-control form-control-sm bright">
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>
                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <label for=""> Term</label>
                                        <select id="sortSlipTerm" style="font-size:1rem; border-radius:5px;" class="form-control form-control-sm bright">
                                            <option>Select term</option>   
                                            <option value="1">1st Term</option>
                                            <option value="2">2nd Term</option>
                                            <option value="3">3rd Term</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group" style="margin-top:30px; width:100%;">
                                <label for=""> Which fee(s) to be paid?</label>
                                <select id="sortSlipFee" class="form-control select2" name="classToPay" style="width:100%; border: 2px solid #08ACF0" multiple="true">
                                        
                                </select>
                                </div>

                                <div class="form-buttons-w" style="width:100%; margin-top:30px;">
                                    <button class="my_hover_up btn btn-primary" type="submit" style="margin-left:0px; border-color:#08ACF0; background-color:#08ACF0"> Generate payment slip now</button>
                                </div>
                                </form>


                                </div>
                                </div>

                                <div class="col-lg-7">

                                <form style="margin-bottom:20px; margin-top:5px; margin-left:5px;">
                                <div class="form-group row">
                                <div class="col-sm-7">
                                <input class="form-control" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" placeholder="E.g; Brown Banigo" type="text">
                                </div>
                                <div class="col-sm-2">
                                <button class="btn btn-primary my_hover_up" type="submit" style="background-color:#FB5574; border-color:#FB5574"> Search payment slip</button>
                                </div>
                                </div>
                                </form>

                                <div class="element-box">
                                <h5 class="form-header">
                                GENERATED/PENDING PAYMENTS SLIPS
                                </h5>
                                <div class="form-desc">
                                View, edit and delete all previously generated payment slips whose payments are still not yet been paid. 
                                </div>

                                <!-- STUDENTS SLIP -->

                                <div class="table-responsive">
                                    <table class="table table-padded">
                                        <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Student name</th>
                                                <th>Date</th>
                                                <th class="text-center">Delete</th>
                                                <th class="text-center">Print</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr class="my_hover_up">
                                                <td class="nowrap">
                                                    <span>1.</span>
                                                </td>
                                                <td>
                                                    <span>Maven Harry</span>
                                                </td>
                                                <td>
                                                    <span>12/06/2018</span>
                                                </td>
                                                <td class="text-center">
                                                    <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                        class="my_hover_up_bg badge" href="">Delete Slip</a>
                                                </td>
                                                <td class="text-center">
                                                    <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                        class="my_hover_up_bg badge" href="">View Slip</a>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>

                                </div>
                                </div>

                                </div>


                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>