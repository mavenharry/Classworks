<?php
include_once ("paymentClass.php");
$paymentClass = new paymentClass();

if(isset($_POST["addFee"])){
    @$feeName = htmlentities(trim($_POST["feeName"]));
    @$classToPay = $_POST["classToPay"];
    @$feeAmount = htmlentities(trim($_POST["feeAmount"]));
    @$schoolFeePart = htmlentities(trim($_POST["schoolFeePart"]));

    $reply = $paymentClass->addFee($feeName, $classToPay, $feeAmount, $schoolFeePart);
    if($reply === true){
        $mess = "Fees added successfully";
    }else{
        switch($reply){
            case 'exist':
                $err = "A fee currently exist with this name for this class";
                break;
            default :
                $err = "An error occurred while trying to add fees. Please try again later";
                break;  
        }
    }
}

if(isset($_POST["saveStructure"])){
    $ids = $_POST["id"];
    $amount = $_POST["amount"];
    $term = htmlentities(trim($_POST["term"]));
    $session = htmlentities(trim($_POST["session"]));

    if($term != $paymentClass->getSchoolDetails()->academic_term || $session != $paymentClass->getSchoolDetails()->academic_session){
        $err = "Sorry, Cannot edit fee other than current term and session fee";
    }else{
        $reply = $paymentClass->updateStructure($ids, $amount);
        if($reply === true){
            $mess = "Fee Structure has been updated";
        }else{
            $err = "Could not update fee structure. Please try again later";
        }
    }
}

if(isset($_POST["sortPaymentClass"]) && isset($_POST["sortPaymentStudent"]) && !empty($_POST["sortPaymentStudent"])){
    $class = htmlentities(trim($_POST["sortPaymentClass"]));
    $studentNumber = htmlentities(trim($_POST["sortPaymentStudent"]));
    $recentPayment = $paymentClass->fetchedPayedStudent($class, $studentNumber);
}else{
    if(isset($_POST["sortPaymentClass"]) && isset($_POST["sortPaymentStudent"]) && empty($_POST["sortPaymentStudent"])){
        $class = htmlentities(trim($_POST["sortPaymentClass"]));
        $recentPayment = $paymentClass->fetchedPayedStudent($class, "all");
    }else{
        $recentPayment = $paymentClass->fetchedPayedStudent("all", "all");
    }
}

if(isset($_POST["assignFeeBtn"])){
    $assignStudent = htmlentities(trim($_POST["assignStudent"]));
    $assignSession = htmlentities(trim($_POST["assignSession"]));
    $assignTerm = htmlentities(trim($_POST["assignTerm"]));
    $assignFee = htmlentities(trim($_POST["assignFee"]));
    $assignAmount = str_replace(",","",htmlentities(trim($_POST["assignAmount"])));

    $reply = $paymentClass->assignOutstandingFee($assignStudent, $assignSession, $assignTerm, $assignFee, $assignAmount);
    switch ($reply){
        case 'exist':
            $err = "This fee has already been assigned to this student. To modify this, delete it and add again";
            break;
        case true:
            $mess = "Fee has been assigned to this student successfully";
            break;
        case false:
            $err = "An error occurred while trying to assign fee. Please try again later";
            break;
        default :
            $err = "An unexpected error occurred. Please try again later";
            break;
    }
}
?>