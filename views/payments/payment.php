<html>

<head>
    <link href="../../css/sales.css" rel="stylesheet">
</head>
<title> Payments - School Automation System</title>
<body>
    <div class="sales">
        <div class="sales__container">
            <div class="sales__top">
                <div class="sales__header">Payment amount</div>
                <div class="sales__display">
                    <div class="sales__display__left">
                        <div class="sales__display__value">
                            <input type="tel" class="v-money form__input form__input--sales" autofocus="autofocus">
                        </div>
                    </div>
                    <div class="sales__display__right">
                        <div class="display__cell">
                            <button class="btn btn--check">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
                                    <defs>
                                        <path id="addde" d="M27.902 22.77H39a1 1 0 0 1 1 1v.46a1 1 0 0 1-1 1H27.902l4.335 4.669c.446.48.446 1.26 0 1.74-.447.481-1.17.481-1.617 0l-6.033-6.497A1 1 0 0 1 24 24.23v-.462a1 1 0 0 1 .587-.91l6.033-6.499c.447-.48 1.17-.48 1.617 0 .446.481.446 1.26 0 1.741l-4.335 4.668z"></path>
                                    </defs>
                                    <use fill="#222C36" fill-rule="evenodd" transform="translate(-24 -16)" xlink:href="#addde"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="select">
                            <select class="select__input select__input--sales">
                                <option value="NGN">Nigerian Naira (NGN)</option>
                                <option value="USD">American Dollars (USD)</option>
                            </select>
                            <div class="select__icon select__icon--sales">
                                <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                                    <path fill="#637381" fill-rule="evenodd" d="M6.03 3.03L3.025.025.02 3.03h6.01zM5.88 7.071L2.975 9.975.07 7.07H5.88z"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sales__bottom">
                <div class="sales__bottom__left">
                    <div class="keypad">
                        <button class="keypad__btn">1</button>
                        <button class="keypad__btn">2</button>
                        <button class="keypad__btn">3</button>
                        <button class="keypad__btn">4</button>
                        <button class="keypad__btn">5</button>
                        <button class="keypad__btn">6</button>
                        <button class="keypad__btn">7</button>
                        <button class="keypad__btn">8</button>
                        <button class="keypad__btn">9</button>
                        <button class="keypad__btn">0</button>
                        <button class="keypad__btn">000</button>
                    </div>
                </div>
                <div class="sales__bottom__right">
                    <div class="sales__bottom__top">
                        <div class="sales__header sales__header--noBackground">Payment Method</div>
                        <div class="sales__method">
                            <div class="sales__method__item">
                                <input id="card" type="radio" value="card" name="paymentMethod" checked="checked" class="sales__method__item__radio">
                                <label for="card" class="sales__method__item__label">Card</label>
                            </div>
                            <div class="sales__method__item">
                                <input id="bank" type="radio" value="bank" name="paymentMethod" class="sales__method__item__radio">
                                <label for="bank" class="sales__method__item__label">Bank account</label>
                            </div>
                            <div class="sales__method__item">
                                <input id="ussd" type="radio" value="USSD" name="paymentMethod" class="sales__method__item__radio">
                                <label for="ussd" class="sales__method__item__label">Phone (USSD)</label>
                                <!---->
                                <!---->
                            </div>
                            <div class="sales__method__item">
                                <input id="cash" type="radio" value="cash" name="paymentMethod" class="sales__method__item__radio">
                                <label for="cash" class="sales__method__item__label">Cash</label>
                            </div>
                        </div>
                    </div>
                    <div class="sales__bottom__bottom">
                        <div class="sales__button">
                            <button class="btn btn--primary btn--lg btn--block btn--bbw" disabled="disabled">Pay Fees</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>