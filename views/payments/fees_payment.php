<?php include_once ("paymentbtn.php");

$schoolDetails = $paymentClass->getSchoolDetails();
$classes = $paymentClass->getClasses();
if(isset($_GET["student_id"]) && isset($_GET["session"]) && isset($_GET["term"])){
  $studentNumber = htmlentities(trim($_GET["student_id"]));
  $term = htmlentities(trim($_GET["term"]));
  $session = htmlentities(trim($_GET["session"]));
  $studentDetails = $paymentClass->getStudentDetails($studentNumber);
}
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo ucwords($schoolDetails->school_name); ?> - Online Result Checker</title>
<!-------------------- START - Meta -------------------->
<?php include("../../includes/meta.php") ?>
<!-------------------- END - Meta -------------------->
<link href="paymentFolder/css/index.css" rel="stylesheet">
<link href="../../css/main.css" rel="stylesheet">
<?php include_once ("../../includes/styles.php"); ?>
</head>

<body>
        <div class="categories">

        <div class="category-block">
          <div class="category-icon-cont">

            <div class="icon"><img style="margin-top: -20px; margin-bottom: 20px; width: 140px; object-fit: scale-down;" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>"></div>

            <h2 style="color: #09acef; font-size: 1.8rem; margin-bottom:0px; margin-top:0px;" ><?php echo ucwords($schoolDetails->school_name); ?></h2>
            <h2 style="color: #b7b7b7; font-weight:normal; font-style: italic; font-size: 1rem;" ><?php echo ucwords($schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country); ?></h2>

            <h2 style="color: #8095A0; font-size: 1.5rem; margin-top:10px; margin-bottom:10px;" >Online Fees Payment</h2>            
           <!-- RESULT CHECKER -->
        <div id="checkResult">
          <form>
            <select class="pin" id="parentPayingSession" name="class" >
                <option value="">Session</option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>   
            </select>
            <select class="pin" id="parentPayingTerm" name="class" >
              <option value="">Term</option>
              <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
              <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
              <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option>     
            </select>
            <select class="pin" id="resultClass" name="class" >
              <option value="">Select Class</option>
              <?php
                foreach($classes as $key => $class){ ?>
                  <option value="<?php echo $class->class_name; ?>"><?php echo ucwords($class->class_name); ?></option>
                <?php }
              ?>   
            </select>
            <select class="pin outStudentPayment" id="resultStudentNumber" name="class" >
              <option value="">Select student</option>  
            </select>
              <div class="">
                <select required class="pin select2" id="parentFeePayment" name="fee[]" style="width:59%;" multiple="true">
                    <!-- STUDENTS FEE -->
                </select>
              </div>
              <button name="showResult" id="makePayment" class="button"><span>Proceed To Make Payment </span></button>
          </form>
        </div>
        <!-- RESULT CHECKER ENDS HERE -->


          </div>
        </div>

</body>
<?php include_once ("../../includes/scripts.php"); ?>
</html>