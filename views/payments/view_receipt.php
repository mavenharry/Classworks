<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>


<style>

p {
  text-align: center;
  opacity: 0.3;
  transition: 0.3s;
  position: absolute;
  bottom: 2vh;
  left: 0;
  right: 0;
  margin: 0 auto;
}

p:hover {
  opacity: 1;
}

.receipt {
  max-width: 600px;
  margin: auto;
}
.receipt_hoverable {
  transition: 0.3s;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);
}
.receipt_hoverable:hover {
  box-shadow: 0 5px 20px rgba(0, 0, 0, 0.3);
}

.header {
  width: 100%;
}

.header__top {
  display: flex;
  align-items: center;
  background: white;
  width: 100%;
  border-radius: 4px 4px 0 0;
}

.header__logo {
  width: 10%;
  padding: 30px;
}

.header__meta {
  position: relative;
  width: 90%;
  height: 100%;
  margin-left: 15px;
  line-height: 1.7rem;
  opacity: 0.3;
}

.header__serial {
  display: block;
}

.header__number {
  position: absolute;
  top: 7.5px;
  right: 0;
  -webkit-transform: rotate(270deg);
          transform: rotate(270deg);
  opacity: 0.2;
}

.header__greeting {
  clear: both;
}

.header__greeting {
  position: relative;
  background: white;
  padding: 0 15px;
  padding-left: 30px;
}

.header__name {
  display: block;
  font-weight: bold;
  font-size: 1.3rem;
  margin-bottom: 7.5px;
}

.header__count {
  opacity: 0.4;
  font-size: 90%;
}

.header__border {
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 3px;
  background-color: #429fff;
}

.header__spacing {
  display: block;
  background: white;
  width: 100%;
  height: 22.5px;
}

.cart {
  background: white;
  padding: 30px;
  padding-top: 15px;
  border-bottom: 2px dashed #ff84a1;
  border-radius: 0 0 5px 5px;
}

.cart__header {
  margin-top: 0;
  text-align: center;
}

.cart__hr {
  border: none;
  padding: 0;
  margin: 0;
  margin-bottom: 22.5px;
  border-bottom: 3px solid #fee469;
}

.list {
  margin: 0;
  padding: 0;
  counter-reset: item-counter;
}

.list__item_head {
  display: flex;
  width: 100%;
  font-weight: bold;
  padding-top: 22.5px;
  padding-bottom: 22.5px;
  padding-left:20px;
  padding-right:10px;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.1);
}
.list__item_head:last-child {
  border-bottom: none;
}
.list__item_head:before {
  margin-right: 15px;
}

.list__item {
  display: flex;
  width: 100%;
  padding-top: 22.5px;
  padding-bottom: 22.5px;
  border-bottom: 1px dashed rgba(0, 0, 0, 0.1);
}
.list__item:last-child {
  border-bottom: none;
}
.list__item:before {
  content: counter(item-counter);
  counter-increment: item-counter;
  margin-right: 15px;
}

.list__name {
  flex: 1;
  align-self: flex-start;
}

.list__price {
  align-self: flex-end;
  text-align: right;
  font-weight: bold;
}

.cart__total {
  display: flex;
  width: 100%;
}

.cart__total-label {
  margin: 0;
  flex: 1;
  text-transform: uppercase;
}

.cart__total-price {
  align-self: flex-end;
  font-weight: bold;
  text-align: right;
}

.bar-code {
  background: white;
  padding: 30px;
  border-radius: 6px 6px 4px 4px;
}

.button {
  position: fixed;
  bottom: 15px;
  right: 15px;
  background: #ff517a;
  border: none;
  border-radius: 3px;
  color: white;
  padding: 15px;
  transition: 0.3s;
}
.button:hover {
  box-shadow: 0 2px 10px #b7002b;
}

.link {
  display: block;
  margin: 25px auto 15px auto;
  text-align: center;
}

</style>

</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    if(isset($_GET["receiptStudent"])){
      if(!empty($_GET["receiptStudent"])){
        $receiptDetails = $paymentClass->receiptAllDetails($_GET["receiptStudent"],$_GET["receiptFee"],$_GET["receiptSession"],$_GET["receiptTerm"]);
      }
    }else{
      $paymentId = htmlentities(trim(base64_decode($_GET["payment_id"])));
      if(!empty($paymentId)){
        $receiptDetails = $paymentClass->receiptDetails($paymentId);
      }
    }
    
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

<div class="all-wrapper with-side-panel solid-bg-all">

    <div class="layout-w">

    <div class="content-w">

<div class="content-w" style="margin-top:0px">
    <div class="content-i">
        <div class="content-box">
            <!--START - Transactions Table-->
                <div style="float:right; margin-bottom:40px; margin-top:0px;" class="element-actions">
                <a onclick="window.history.back()" style="color:#FFFFFF; background-color:#974A6D; margin-right:10px; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                    <i class="os-icon os-icon-common-07"></i>
                    <span style="font-size: 0.9rem;">Go back to previous page</span>
                </a>
                <a style="color:#FFFFFF; background-color:#fb5574; border:#fb5574; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                    <i class="os-icon os-icon-email-2-at"></i>
                    <span style="font-size: .9rem;">Mail receipt to parents</span>
                </a>
                <a onclick="window.print()" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                    <i class="os-icon os-icon-printer"></i>
                    <span style="font-size: .9rem;">Print this receipt</span>
                </a>
                </div>

                <link href='https://fonts.googleapis.com/css?family=Raleway:600,400' rel='stylesheet' type='text/css'>

                <div class="receipt">
                    
                    <header class="header">
                        <div class="header__top">
                            <div class="header__logo">
                            <img height="60" width="60" alt="" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>"/>
                            </div>
                            <div class="header__meta" style="line-height:25px; margin-left:40px; font-size:1.05rem;">
                                <span class="header__date"><?php echo ucwords($schoolDetails->school_name) ?></span>
                                <span class="header__serial"><?php echo ucwords($schoolDetails->city)." - ".ucwords($schoolDetails->state) ?></span>
                                <span class="header__serial"><?php echo ucwords($schoolDetails->country) ?></span>
                                <span class="header__number">20180201</span>
                            </div>
                        </div>
                        <?php 
                        if(isset($_GET["receiptStudent"])){ ?>

                          <div class="header__greeting" style="padding-bottom:20px; padding-top:20px;">
                            <span class="header__name"><?php echo ucwords($paymentClass->getStudentDetails($_GET["receiptStudent"])->fullname); ?></span>
                            <span class="header__count" style="line-height:30px; font-size:1rem;"><b style="font-weight:700;">Gender:</b> <?php echo ucwords($paymentClass->getStudentDetails($_GET["receiptStudent"])->gender); ?>
                            <b style="margin-left:40px; font-weight:700">Class:</b> <?php echo ucwords($paymentClass->getStudentDetails($_GET["receiptStudent"])->present_class); ?>
                            <b style="margin-left:40px; font-weight:700">Receipt No.:</b> <?php echo "View Below"; ?><br>
                            <b style="font-weight:700">Session:</b> <?php echo $paymentClass->getCurrentAcademicSession(); ?>
                            <b style="margin-left:40px; font-weight:700">Term:</b> <?php echo $paymentClass->getCurrentAcademicTerm(); ?>
                            <b style="margin-left:40px; font-weight:700">Print Date:</b> <?php echo date("d.m.Y") ?><br>
                            </span>
                            <span class="header__border"></span>
                        </div>

                        <?php }else{ ?>
                          
                          <div class="header__greeting" style="padding-bottom:20px; padding-top:20px;">
                            <span class="header__name"><?php echo ucwords($receiptDetails[0]->fullname); ?></span>
                            <span class="header__count" style="line-height:30px; font-size:1rem;"><b style="font-weight:700;">Gender:</b> <?php echo ucwords($receiptDetails[0]->gender); ?>
                            <b style="margin-left:40px; font-weight:700">Class:</b> <?php echo ucwords($receiptDetails[0]->presentClass); ?>
                            <b style="margin-left:40px; font-weight:700">Receipt No.:</b> <?php echo $receiptDetails[1]->receiptNo;?><br>
                            <b style="font-weight:700">Session:</b> <?php echo ($receiptDetails[0]->session - 1)."/".$receiptDetails[0]->session; ?>
                            <b style="margin-left:40px; font-weight:700">Term:</b> <?php echo ucwords($receiptDetails[0]->term); ?>
                            <b style="margin-left:40px; font-weight:700">Print Date:</b> <?php echo date("d.m.Y") ?><br>
                            </span>
                            <span class="header__border"></span>
                        </div>

                        <?php }
                        ?>
                        <div class="header__spacing"></div>
                    </header>
                    
                    <section class="cart">
                            <h2 class="cart__header" style="margin-top:-20px; font-size:1.6rem" >Fee(s) Payment Receipt:</h2>
                            <ol class="list" style="margin-top:30px;">
                              <li class="list__item_head">
                                    <span class="list__name">Name</span>
                                    <span class="list__name">Date</span>
                                    <span class="list__name">Amount</span>
                                    <span class="list__name">Receipt No.</span>
                                </li>  
                                <?php
                                  $total = 0;
                                  if(count($receiptDetails) > 0){
                                    if(isset($_GET["receiptStudent"])){
                                      for ($i=0; $i<count($receiptDetails); $i++) {
                                        $total += $receiptDetails[$i]->amount; ?>
                                        <li class="list__item">
                                          <span class="list__name"><?php echo ucwords($receiptDetails[$i]->feeName); ?></span>
                                          <span class="list__name"><?php echo $receiptDetails[$i]->payDate; ?></span>
                                          <span class="list__name">&#8358;<?php echo number_format($receiptDetails[$i]->amount, 2) ?></span>
                                          <span class="list__name"><?php echo $receiptDetails[$i]->receiptNo; ?></span>
                                        </li>
                                      <?php } }else{
                                      $total = 0;
                                    foreach ($receiptDetails as $key => $value) {
                                      if($key != 0){
                                        $total += $value->amount; ?>
                                        <li class="list__item">
                                          <span class="list__name"><?php echo ucwords($value->feeName); ?></span>
                                          <span class="list__name"><?php echo $value->payDate; ?></span>
                                          <span class="list__name">&#8358;<?php echo number_format($value->amount, 2) ?></span>
                                          <span class="list__name"><?php echo $value->receiptNo; ?></span>
                                        </li>
                                    <?php }
                                      }
                                    }

                                  }
                                ?>
          
                            </ol>	
                            <hr class="cart__hr" />
                            <footer class="cart__total">
                                <h3 class="cart__total-label" style="font-size:1.5rem">Grand Total</h3>
                                <span class="cart__total-price" style="font-size:1.2rem">&#8358;<?php echo number_format($total, 2) ?></span>				
                            </footer>
                    </section>
                    
                    <footer class="bar-code">
                        <div class="bar-code__code">
                            <svg id="d17ac603-1444-403c-9daf-1afe6b3ad839" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 575.2 93.16"><defs><style>.\33 6de76d2-eb0f-431d-be12-6535d9fc4369,.\38 78966b7-abd4-4ef3-86d3-2321f80177f3,.b8e87e45-e06c-4d7b-92d7-30eed261c8dd,.e435b0ed-5ad4-4699-bad7-b1c15cfe3f86{fill:none;stroke:#000;stroke-miterlimit:10;}.\38 78966b7-abd4-4ef3-86d3-2321f80177f3{stroke-width:1.7px;}.e435b0ed-5ad4-4699-bad7-b1c15cfe3f86{stroke-width:3.7px;}.\33 6de76d2-eb0f-431d-be12-6535d9fc4369{stroke-width:7.7px;}.b8e87e45-e06c-4d7b-92d7-30eed261c8dd{stroke-width:5.7px;}</style></defs><title>Example_barcode</title><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M0.85,93.16V0m4,93.16V0m6,93.16V0"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="17.85" y1="93.16" x2="17.85"/><line class="36de76d2-eb0f-431d-be12-6535d9fc4369" x1="25.85" y1="93.16" x2="25.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="32.85" y1="93.16" x2="32.85"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="40.85" y1="93.16" x2="40.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="46.85" y1="93.16" x2="46.85"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="51.85" y1="93.16" x2="51.85"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M60.85,93.16V0m10,93.16V0m4,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0m4,93.16V0m4,93.16V0m6,93.16V0"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="116.85" y1="93.16" x2="116.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="122.85" y1="93.16" x2="122.85"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="130.85" y1="93.16" x2="130.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="138.85" y1="93.16" x2="138.85"/><path class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" d="M143.85,93.16V0m8,93.16V0m6,93.16V0m6,93.16V0"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M170.85,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M193.85,93.16V0m4,93.16V0m6,93.16V0"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="210.85" y1="93.16" x2="210.85"/><line class="36de76d2-eb0f-431d-be12-6535d9fc4369" x1="218.85" y1="93.16" x2="218.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="225.85" y1="93.16" x2="225.85"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="233.85" y1="93.16" x2="233.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="239.85" y1="93.16" x2="239.85"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="244.85" y1="93.16" x2="244.85"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M253.85,93.16V0m10,93.16V0m4,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0m4,93.16V0m4,93.16V0m6,93.16V0"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="309.85" y1="93.16" x2="309.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="315.85" y1="93.16" x2="315.85"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="323.85" y1="93.16" x2="323.85"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="331.85" y1="93.16" x2="331.85"/><path class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" d="M336.85,93.16V0m8,93.16V0m6,93.16V0m6,93.16V0"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M363.85,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M386.35,93.16V0m4,93.16V0m6,93.16V0"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="403.35" y1="93.16" x2="403.35"/><line class="36de76d2-eb0f-431d-be12-6535d9fc4369" x1="411.35" y1="93.16" x2="411.35"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="418.35" y1="93.16" x2="418.35"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="426.35" y1="93.16" x2="426.35"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="432.35" y1="93.16" x2="432.35"/><line class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" x1="437.35" y1="93.16" x2="437.35"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M446.35,93.16V0m10,93.16V0m4,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0m4,93.16V0m4,93.16V0m6,93.16V0"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="502.35" y1="93.16" x2="502.35"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="508.35" y1="93.16" x2="508.35"/><line class="b8e87e45-e06c-4d7b-92d7-30eed261c8dd" x1="516.35" y1="93.16" x2="516.35"/><line class="878966b7-abd4-4ef3-86d3-2321f80177f3" x1="524.35" y1="93.16" x2="524.35"/><path class="e435b0ed-5ad4-4699-bad7-b1c15cfe3f86" d="M529.35,93.16V0m8,93.16V0m6,93.16V0m6,93.16V0"/><path class="878966b7-abd4-4ef3-86d3-2321f80177f3" d="M556.35,93.16V0m6,93.16V0m8,93.16V0m4,93.16V0"/></svg>
                        </div>
                    </footer>
                    
                </div>

            </div>
            <!--END - Transactions Table-->


            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/support.php") ?>
            <!-------------------- END - Top Bar -------------------->

</body>

</html>