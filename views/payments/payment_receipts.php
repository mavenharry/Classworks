<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    $classes = $paymentClass->getClasses();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:0px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                <a data-target="#allReceipts" data-toggle="modal" style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                    <i class="os-icon os-icon-printer"></i>
                                    <span style="font-size: .9rem;">View All-Fees-In-1 Receipt</span>
                                </a>
                                </div>
                                <h6 class="element-header">ALL PAYMENTS RECEIPTS</h6>

                                
                                <div class="row" style="margin-top:20px;">

                                <div class="col-lg-11">

                                <form style="margin-bottom:25px; margin-top:10px; margin-left:5px;" method="POST">
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <select style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright" name="sortPaymentClass" id="sortPaymentClass">
                                                <option> Select Class</option>
                                                <?php
                                                    foreach($classes as $key => $value){ ?>
                                                        <option value="<?php echo $value->class_name?>"><?php echo $value->class_name; ?></option>
                                                    <?php }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-5">
                                            <select id="sortPaymentStudent" name="sortPaymentStudent" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                                <option>Select Student</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="submit" class="btn btn-primary" value="Search Receipts">
                                        </div>
                                    </div>
                                </form>

                                <div class="element-box">
                                <form>
                                <h5 class="form-header">
                                STUDENT'S FEES PAYMENT RECEIPT
                                </h5>
                                <div class="form-desc">
                                Get access to all student's payments history and print/reprint previous and current payment receipts with ease.
                                </div>
                               
                                <table class="table table-padded">
                                    <thead>
                                        <tr>
                                        <th>S/N</th>
                                        <th>Name</th>
                                        <th>Fee name</th>
                                        <th>Amount</th>
                                        <th>Session</th>
                                        <th>Term</th>
                                        <th>Date</th>
                                        <th>Receipt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(count($recentPayment) == 0){
                                                echo "No student to display yet";
                                            }else{
                                                foreach($recentPayment as $key => $value){ ?>
                                                    <tr class="my_hover_up">
                                                        <td class="nowrap">
                                                            <span><?php echo $key + 1?>.</span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->fullname); ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo ucwords($value->feeName); ?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $schoolDetails->currencySymbol; ?><?php echo number_format($value->amount)?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->session -1?>/<?php echo $value->session?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->term ;?></span>
                                                        </td>
                                                        <td>
                                                            <span><?php echo $value->date; ?></span>
                                                        </td>
                                                        <td>
                                                            <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="view_receipt?payment_id=<?php echo base64_encode($value->id); ?>">View Receipt</a>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            }
                                        ?>
                                        
                                    </tbody>
                                </table>

                                </div>
                                </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>