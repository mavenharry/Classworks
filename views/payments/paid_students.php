<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payments</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    if(isset($_POST["searchPayedStydent"])){
        @$sortClass = htmlentities(trim($_POST["sortClass"]));
        @$sortTerm = htmlentities(trim($_POST["sortTerm"]));
        @$sortSession = htmlentities(trim($_POST["sortSession"]));
        @$sortFee = htmlentities(trim($_POST["sortFee"]));
        @$searchKeyword = htmlentities(trim($_POST["searchKeyword"]));
        $payedStudent = $paymentClass->searchPayedStudent($sortClass, $sortTerm, $sortSession, $sortFee, $searchKeyword);
    }else{
        if(isset($_GET["fee_id"])){
            $sortFee = htmlentities(trim(base64_decode($_GET["fee_id"])));
            $sortClass = "";
            $sortTerm = htmlentities(trim(base64_decode($_GET["pt"])));
            $sortSession = htmlentities(trim(base64_decode($_GET["ps"])));
            $payedStudent = $paymentClass->searchPayedStudent($sortClass, $sortTerm, $sortSession, $sortFee, "");
        }else{
            $payedStudent = $paymentClass->searchPayedStudent("","","","","");
        }
    }
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchPaidStudentsModal" data-toggle="modal" style="margin-left:10px; color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL STUDENTS WITH PAID FEE(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Name</th>
                                                <th>Class</th>
                                                <th>Fee</th>
                                                <th>Amount</th>
                                                <th>Session</th>
                                                <th>Term</th>
                                                <th>Mode</th>
                                                <th>Date</th>
                                                <th class="text-center">Reciept</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach($payedStudent as $key => $value){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $key + 1?>.</span>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    if(empty($value->profilePhoto)){ ?>
                                                                        <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                        class="badge"><?php echo $nameInitials = $paymentClass->createAcronym(ucwords($value->fullname)); ?></a>
                                                                    <?php }else{ ?>
                                                                        <div class="user-with-avatar">
                                                                            <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto;?>">
                                                                        </div>
                                                                    <?php }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->fullname); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->presentClass); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->feeName); ?></span>
                                                            </td>
                                                            <td>
                                                                <span>&#x20a6;<?php echo number_format($value->amount); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->session - 1?>/<?php echo $value->session; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->term; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->payMode); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo str_replace("-","/",$value->payDate); ?></span>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                    class="my_hover_up_bg badge" href="../payments/payment_receipts?student_id=<?php echo $value->studentNumber ?>">View Payments</a>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                ?>
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class-"element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>