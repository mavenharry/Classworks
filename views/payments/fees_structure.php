<?php include_once ("paymentbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php")?>
</head>
<?php
    $schoolDetails = $paymentClass->getSchoolDetails();
    $allClasses = $paymentClass->getClasses();
    if(isset($_POST["searchRecentPayment"])){
        $searchName = htmlentities(trim($_POST["searchName"]));
        $recentPayment = $paymentClass->recentPayment($searchName);
    }else{
        $recentPayment = $paymentClass->recentPayment("all");
    }
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                <a data-target="#newFeeModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="#">
                                    <i class="os-icon os-icon-plus"></i>
                                    <span style="font-size: .9rem;">Add a new fee</span>
                                </a>
                                <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D; margin-right:10px;" class="my_hover_up btn-upper btn btn-sm" href="#">
                                    <i class="os-icon os-icon-printer"></i>
                                    <span style="font-size: .9rem;">Print This</span>
                                </a>
                                </div>
                                <h6 class="element-header">FEES STRUCTURE/FORMATION</h6>

                                
                                <div class="row" style="margin-top:20px;">

                                <div class="col-lg-6">
                                    <form style="margin-bottom:20px; margin-top:10px; margin-left:5px;" method="POST" action="">
                                        <div class="form-group row">
                                        <div class="col-sm-7">
                                            <select id="classSelector" name="classSelector" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                            <option value="">Select Class</option>
                                            <?php
                                                foreach ($allClasses as $key => $value) { ?>
                                                    <option value="<?php echo $value->class_code?>"><?php echo $value->class_name?></option>
                                                <?php }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select id="termSelector" name="termSelector" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                            <option <?php echo $schoolDetails->academic_term == 1 ? "selected" : ""?> value="1">1st Term</option>
                                            <option <?php echo $schoolDetails->academic_term == 2 ? "selected" : ""?> value="2">2nd Term</option>
                                            <option <?php echo $schoolDetails->academic_term == 3 ? "selected" : ""?> value="3">3rd Term</option>   
                                            </select>
                                        </div>
                                        <div class="col-sm-5" style="margin-top:20px">
                                            <select id="sessionSelector" name="sessionSelector" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('-1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('-1 year')) ?>"><?php echo date('Y', strtotime('-2 year')) ?>/<?php echo date('Y', strtotime('-1 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y") ? "selected" : ""?> value="<?php echo date("Y")?>"><?php echo date('Y', strtotime('-1 year')) ?>/<?php echo date("Y")?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+1 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+1 year')) ?>"><?php echo date('Y') ?>/<?php echo date('Y', strtotime('+1 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+2 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+2 year')) ?>"><?php echo date('Y', strtotime('+1 year')) ?>/<?php echo date('Y', strtotime('+2 year')) ?></option>
                                            <option <?php echo $schoolDetails->academic_session == date("Y", strtotime('+3 year')) ? "selected" : ""?> value="<?php echo date('Y', strtotime('+3 year')) ?>"><?php echo date('Y', strtotime('+2 year')) ?>/<?php echo date('Y', strtotime('+3 year')) ?></option>   
                                            </select>
                                        </div>
                                        <div class="col-sm-2" style="margin-top:20px">
                                            <button class="btn btn-primary" type="submit" name="searchFee"> Search fee structure</button>
                                        </div>
                                        </div>
                                    </form>
                                    <?php
                                        if(isset($_POST["classSelector"]) && !empty($_POST["classSelector"]) && isset($_POST["sessionSelector"]) && isset($_POST["termSelector"])){
                                            $classCode = htmlentities(trim($_POST["classSelector"]));
                                            $term = htmlentities(trim($_POST["termSelector"]));
                                            $session = htmlentities(trim($_POST["sessionSelector"]));
                                            $classPayment = $paymentClass->getPayments($classCode, $term, $session);
                                            if(count($classPayment) == 0){ ?>
                                                <div class="element-box">
                                                    <form>
                                                        <h5 class="form-header">
                                                           NO FEE STRUCTURE FOR <span style="font-Weight: 800"> <?php echo $paymentClass->getClassName($classCode)?></span>
                                                        </h5>
                                                        <div class="form-desc">
                                                            Set school fees structure and amount to automate your fees collection process seamlessly. Fill applicable fee, leave non-applicable blank.
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php }else{ ?>
                                                    <div class="element-box">
                                                        <form method="POST">
                                                            <h5 class="form-header">
                                                                FEE STRUCTURE FOR <span style="font-Weight: 800"> <?php echo $paymentClass->getClassName($classCode)?></span>
                                                            </h5>
                                                            <div class="form-desc">
                                                                Set school fees structure and amount to automate your fees collection process seamlessly. Fill applicable fee, leave non-applicable blank.
                                                            </div>
                                                            <input type="hidden" name="term" value="<?php echo $term; ?>" id="">
                                                            <input type="hidden" name="session" value="<?php echo $session; ?>" id="">
                                                            <?php foreach($classPayment as $key => $value){ ?>
                                                            <div class="form-group row">
                                                                <label class="col-form-label col-sm-4" for=""> <?php echo $value->fee_name; ?></label>
                                                                <input class="form-control" name="id[]" value="<?php echo $value->id?>" placeholder="&#8358;0.00" type="hidden">
                                                                <div class="col-sm-8">
                                                                    <input class="form-control" name="amount[]" value="<?php echo $value->amount == 0 ? "" : $value->amount?>" placeholder="&#8358;0.00" type="text">
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                            <div class="form-group row">
                                                                <label data-target="#newFeeModal" data-toggle="modal" class="col-form-label col-sm-12" for="" style="text-align:center; margin-top:15px; color:#08ACF0; text-decoration:underline; cursor:pointer"> CLICK HERE TO ADD NEW FEE</label>
                                                            </div>
                                                            <div class="form-buttons-w">
                                                                <input name="saveStructure" type="submit" value="Save Fees Structure" class="btn btn-primary">
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <?php } ?>
                                        <?php }else{ ?>
                                            <div class="element-box">
                                                <form>
                                                    <h5 class="form-header">
                                                        SELECT A CLASS TO VIEW FEE STRUCTURE
                                                    </h5>
                                                    <div class="form-desc">
                                                        Set school fees structure and amount to automate your fees collection process seamlessly. Fill applicable fee, leave non-applicable blank.
                                                    </div>
                                                </form>
                                            </div>
                                        <?php }
                                    ?>
                                </div>

                                <div class="col-lg-6">

                                <form style="margin-bottom:20px; margin-top:10px; margin-left:5px;" method="POST">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <input class="form-control" name="searchName" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" placeholder="E.g; Brown Banigo" type="text">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="submit" name="searchRecentPayment" value="Search a payment" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>

                                <div class="element-box">
                                    <h5 class="form-header">
                                        RECENT FEES PAYMENTS
                                    </h5>
                                    <div class="form-desc">
                                        View and print fee(s) payment slip for any student and forward to parent through mail or post box. 
                                    </div>

                                <!-- STUDENTS SLIP -->

                                <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Student name</th>
                                                    <th>Fee name</th>
                                                    <th class="text-center">Receipt</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($recentPayment) > 0){
                                                        foreach($recentPayment as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->fullname); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->feeName); ?></span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a href="view_receipt.php?payId=<?php echo base64_encode($value->id); ?>" style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                        class="my_hover_up_bg badge" href="">View Receipt</a>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }else{
                                                        echo "No record";
                                                    }
                                                ?>

                                            </tbody>
                                        </table>

                                </div>
                                </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>