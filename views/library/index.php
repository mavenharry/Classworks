<?php include_once ("librarybtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Virtual Library</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link rel="stylesheet" href="css/style.css">
</head>
<?php
$schoolDetails = $libraryClass->getSchoolDetails();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php //include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content center">
                <ul class="library"></ul>
                <div class="overlay-page"></div>
                <div class="overlay-summary"></div>
            </div>

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/support.php") ?>
            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
<script  src="js/index.js"></script>
</body>

</html>