<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class researchClass extends logic {


    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return $value;
        }else{
            return "N/A";
        }
    }

    function getReseachKeywords(){
        $stmt = $this->uconn->prepare("SELECT * FROM research_keywords ORDER BY id DESC LIMIT 30");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $keywordsArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->keyword = $row["keyword"];
            $keywordsArray[] = $obj;
        }
        $stmt->close();
        return $keywordsArray;
    }
    
}
?>