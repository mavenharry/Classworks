<?php include_once ("researchbtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | research</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
$schoolDetails = $researchClass->getSchoolDetails();
$researchKeywords = $researchClass->getReseachKeywords();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->
                    <br>
                    <html>
                    <head>
                    <meta charset="utf-8">

                    <title>Help Center</title>

                    <link rel="stylesheet" type="text/css" href="./css/style.css" />

                    </head>
                    <body class="community-enabled">

                    <main role="main">
                        <section class="section hero" id="homepage-hero">
                        <div class="hero-inner">
                        <p><h7>Search a Keyword?</h7></p>
                        <form class="search">
                        <input type="search" name="keywordQuery" id="keywordQuery" placeholder="E.g; Donald Trump"/>
                        </form>

                        <form class="" style="margin-top:15px;">
                        <input type="button" style="cursor:pointer; color:#FFF; border-radius:7px; background:#08ACF0; border:0px solid #FFF; width:250px; font-weight:bold; padding-top:10px; padding-bottom:10px; font-size:20px;" value="Search Now"/>
                        </form>
                        
                        <br><p style="color:white"><b>Popular searched keywords:</b> 
                        
                        <?php 
                        if(count($researchKeywords) > 0){
                        for ($i=0; $i < count($researchKeywords); $i++) { ?> 
                        <a style="font-size:30px; margin-left:10px;" href=""><?php echo ucwords($researchKeywords[$i]->keyword) ?></a>
                        <?php }}else{ ?>
                            <a style="font-size:30px;" href="#"> No searched keywords yet</a>                            
                        <?php } ?>
                        
                        
                        </p>
                        </div>
                        </section>
                    </main>

                    </body>
                    </html>

                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>