<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class subjectClass extends logic {    

    function outputRefine($value){
        if(isset($value) && !empty($value)){
            return $value;
        }else{
            return "N/A";
        }
    }

    function fetchAllSubject(){
        $stmt = $this->uconn->prepare("SELECT * FROM subjects");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $subjectArray = array();
        while($row = array_shift($stmt_result)){
            $obj = new stdClass();
            $obj->subject_code = $this->outputRefine($row["subjectCode"]);
            $obj->subject_name = $this->outputRefine($row["subjectName"]);
            $obj->subject_class = $this->outputRefine($row["subjectClass"]);
            $subjectArray[] = $obj;
        }
        $stmt->close();
        return $subjectArray;
    }


    function fetchAllSubject_staff($staffNo){
        $stmte = $this->uconn->prepare("SELECT * FROM subjectTeachers WHERE staffCode = ?");
        $stmte->bind_param("s", $staffNo);
        $stmte->execute();
        $stmte_result = $this->get_result($stmte);
        $subjectArray = array();
        while($rowe = array_shift($stmte_result)){
            $stmt = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectCode = ?");
            $stmt->bind_param("s", $rowe["subjectCode"]);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->subject_code = $this->outputRefine($row["subjectCode"]);
                $obj->subject_name = $this->outputRefine($row["subjectName"]);
                $obj->subject_class = $this->outputRefine($row["subjectClass"]);
                $obj->subject_shedule = $this->outputRefine($row["subjectClass"]);
                $subjectArray[] = $obj;
            }
        }
        $stmte->close();
        return $subjectArray;
    }

    function searchSubject($searchSubjects){
        $stmt = $this->uconn->prepare("SELECT * FROM subjects WHERE subjectCode = ?");
        $stmt->bind_param("s", $searchSubjects);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $subjectArray = array();
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->subject_code = $this->outputRefine($row["subjectCode"]);
            $obj->subject_name = $this->outputRefine($row["subjectName"]);
            $obj->subject_class = $this->outputRefine($row["subjectClass"]);
            $subjectArray[] = $obj;
            return $subjectArray;
        }
    }

    function getSubjectShedule($subjectCode){
        $stmt = $this->uconn->prepare("SELECT schedule_days.dayName, schedule_times.timeName FROM schedule_timings INNER JOIN schedule_days ON schedule_days.dayCode = schedule_timings.timeDay INNER JOIN schedule_times ON schedule_times.timeCode = schedule_timings.timeTime WHERE schedule_timings.timeSubject = ?");
        $stmt->bind_param("s", $subjectCode);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $timingArray = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->timeDay = $row["dayName"];
                $obj->timeTime = $row["timeName"];
                $timingArray[] = $obj;
            }
        }
        $stmt->close();
        return $timingArray;
    }

}
?>