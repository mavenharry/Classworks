<?php include_once ("subjectbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Subjects</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<?php
$schoolDetails = $subjectClass->getSchoolDetails();
if(isset($_GET["class_id"])){
$allSubject = $subjectClass->getallClassSubjects($subjectClass->getClassName($_GET['class_id']));
}else{
$allSubject = $subjectClass->fetchAllSubject_staff($_SESSION["uniqueNumber"]);
}
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar2.php"); ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target=".bd-example-modal-sm" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-newspaper"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL MY SUBJECT(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Subject code</th>
                                                <th>Subject name</th>
                                                <th>Subject class</th>
                                                <th class="text-center">Schedule</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allSubject) == 0){
                                                    echo "No subject in record";
                                                }else{
                                                    for($i = 0; $i < count($allSubject); $i++){ ?>
                                                <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i + 1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_code); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_name); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_class); ?></span>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            $timingArray = $subjectClass->getSubjectShedule($allSubject[$i]->subject_code);
                                                            foreach($timingArray as $key => $value){ ?>
                                                                <span><?php echo ucfirst($value->timeDay).", ".strtoupper($value->timeTime); ?></span><br>
                                                            <?php }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php } 
                                                }?>
                                                
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allSubject) > 20){ ?>
                                            <div style="float: right;" class="element-actions">
                                            <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <span style="font-size: .9rem;">Load more</span>
                                            </a>
                                            </div>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>