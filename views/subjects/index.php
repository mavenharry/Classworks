<?php include_once ("subjectbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Subjects</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    
</head>
<!-- MODALS -->
<!-------------------- START - MODAL -------------------->
<?php 
$schoolDetails = $subjectClass->getSchoolDetails();
include("../../includes/modals/index.php");
 ?>
<!-------------------- END - MODAL -------------------->
<?php 
if(isset($_GET["class_id"])){
$allSubject = $subjectClass->getallClassSubjects($subjectClass->getClassName($_GET['class_id']));
}else{
    if(isset($_POST["searchSubjectBtn"])){
        $searchSubjects = htmlentities(trim($_POST["searchSubject"]));
        $allSubject = $subjectClass->searchSubject($searchSubjects);
    }else{
        $allSubject = $subjectClass->fetchAllSubject();
    }
}
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Subjects", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->

                <br>
                <div class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    
                                    <a data-target="#newSubjectModal" data-toggle="modal" style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="#">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Add a new subject</span>
                                    </a>
                                    <a data-target="#newDeleteSubjectModal" data-toggle="modal" style="color:#FFFFFF; background-color:#FB5574; border:#FB5574" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-ui-15"></i>
                                        <span style="font-size: .9rem;">Delete a subject</span>
                                    </a>
                                    <a data-target="#searchSubjectsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search</span>
                                    </a>
                                    <a data-target=".bd-example-modal-sm" data-toggle="modal" style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><?php if(isset($_GET['class_id'])){echo "ALL ".strtoupper($subjectClass->getClassName($_GET['class_id']))." SUBJECT(S)";}else{ echo "ALL SUBJECT(S)"; } ?></h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Subject code</th>
                                                <th>Subject name</th>
                                                <th>Subject class</th>
                                                <th class="text-center">Tutor(s)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($allSubject) == 0){
                                                    echo "No subject found in record";
                                                }else{
                                                    for($i = 0; $i < count($allSubject); $i++){ ?>
                                                <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i + 1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_code); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_name); ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo ucfirst($allSubject[$i]->subject_class); ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                            class="my_hover_up_bg badge" href="../staffs/admin_staffs?subject_id=<?php echo $allSubject[$i]->subject_code; ?>&class_id=<?php echo $subjectClass->getClassCode($allSubject[$i]->subject_class); ?>">View Subject Tutor</a>
                                                    </td>
                                                </tr>
                                                <?php } 
                                                }?>
                                                
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <?php
                                            if(count($allSubject) > 20){ ?>
                                            <div style="float: right;" class="element-actions">
                                            <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                            <span style="font-size: .9rem;">Load more</span>
                                            </a>
                                            </div>
                                        <?php } ?>
                                        
                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>