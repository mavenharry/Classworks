<?php include_once ("documentationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Documentation</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $documentationClass->getSchoolDetails();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->

                            <!-- SEARCH BEGINS HERE -->
                            <div style="display:block;margin-bottom:10px">
                            <form>
                            <input id="searchKeyword" autofocus="true"  onkeyup="searchHandler()" class="form-control" style="display:table-cell; padding:10px; width:700px" placeholder="  E.g; How To Add A New Student" type="text">
                            </form>
                            </div>
                            <!-- SEARCH ENDS HERE -->

                            <br>
                            <div class="row">
                            <!-- STUDENTS DOCUMENTATION BEGINS HERE -->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD A NEW STUDENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT STUDENT id card</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO View STUDENTs list</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DELETE A STUDENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR A STUDENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../students/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT STUDENT DATA</div>
                            </a>
                            </div>

                            <!-- STUDENTS DOCUMENTATION ENDS HERE -->


                            <!-- PARENTS DOCUMENTATION BEGINS HERE -->

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW PARENT LIST</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT PARENT ID CARD</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD A NEW PARENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO CREATE A FAMILY</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DELETE A PARENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR A PARENT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../parents/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT PARENT DATA</div>
                            </a>
                            </div>

                            <!-- PARENTS DOCUMENTATION ENDS HERE -->

                            <!-- STAFF DOCUMENTATION BEGINS HERE -->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF LIST</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD NEW STAFF</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO QUERY A STAFF</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF ID CARD</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFFS CURRENTLY ON LEAVE</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/home">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF INCOMING LEAVE REQUEST</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/admin_staffs">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF LIST</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/admin_staffs">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DELETE A STAFF</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/admin_staffs">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR A STAFF</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../staffs/admin_staffs">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT STAFF DATA</div>
                            </a>
                            </div>

                            <!-- STAFF DOCUMENTATION ENDS HERE -->

                            <!-- CLASS DOCUMENTATION BEGINS HERE -->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW CLASSES</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD A NEW CLASS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO EDIT CLASS NAME</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DELETE A CLASS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR A CLASS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../classes">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT CLASS DATA</div>
                            </a>
                            </div>

                            <!-- CLASS DOCUMENTATION ENDS HERE -->

                            <!-- SUBJECT DOCUMENTATION END HERE -->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../subjects">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW SUBJECT LIST</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../subjects">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD A NEW SUBJECT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../subjects">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DELETE A SUBJECT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../subjects">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR A SUBJECT</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../subjects">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT A SUBJECT DATA</div>
                            </a>
                            </div>

                            <!-- SUBJECTS DOCUMENTATION ENDS HERE -->

                            <!-- PAYMENT DOCUMENTATION BEGINS HERE -->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/paid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STUDENTS WITH PAID FEES</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/paid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH FOR STUDENTS WITH PAID FEES</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/paid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT DATA FOR STUDENTS WITH PAID FEES</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/unpaid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STUDENTS WITH OUTSTANDING FEES</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/unpaid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ASSIGN OUTSTANDING FEE</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/unpaid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SEARCH STUDENT PAYMENT RECORD</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/unpaid_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PRINT STUDENT PAYMENT RECORD</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/staffs_payroll">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFFS ON PAYROLL</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/paid_fees_breakdown">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TOTAL TERM'S FEES PAYMENTS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/fees_structure">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW OR SCHEDULE FEE STRUCTURE</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW OR PRINT PAYMENT SLIP</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../payments/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PROCESS NEW FEE PAYMENT</div>
                            </a>
                            </div>

                            <!-- CHILD PICKUP DECOUMENTATION BEGINS HERE -->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup/childpickup_scanner">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">PROCESS CHILD PICKUP VERIFICATION</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">ADD NEW CHILD PICKUP PERSON</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW CHILD PICKUP PERSON ID CARD</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup/picked_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW PICKED UP STUDENTS TODAY</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup/pickuppersons">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW ALL CHILD PICKUP PERSONS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup/disabled_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW ALL CHILD PICKUP DISABLED STUDENTS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../childpickup/enabled_students">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW ALL CHILD PICKUP ENABLED STUDENTS</div>
                            </a>
                            </div>
                            <!-- CHILD PICKUP ENDS HERE -->


                            <!-- EDUCATIONAL GAMES DECOUMENTATION BEGINS HERE -->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../educational_games/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW ALL EDUCATIONAL GAMES</div>
                            </a>
                            </div>
                            <!-- EDUCATIONAL GAMES ENDS HERE -->
                             
                            <!-- REPORT DECOUMENTATION BEGINS HERE -->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../reports/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">VIEW ALL ACADEMIC/SCHOOL REPORTS</div>
                            </a>
                            </div>
                            <!-- REPORT ENDS HERE -->

                            <!-- PAYMENT DOCUMENTATION BEGINS HERE -->   

                            <!-- ATTENDANCE DOCUMENTATION BEGINS HERE -->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STUDENTS PRESENT IN SCHOOL TODAY</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW LATE STUDENTS</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STUDENTS ABSENT FROM SCHOOL</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO STAFF PRESENT IN SCHOOL TODAY</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW LATE STAFF</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF ABSENT FROM SCHOOL</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/student">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STUDENT ATTENDANCE REPORT</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/staff">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW STAFF ATTENDANCE REPORT</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../attendance/attendance_scanner">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO TAKE ATTENDANCE</div>
                            </a>
                            </div>
                       <!-- ATTENDANCE DOCUMENTATION ENDS HERE -->

                       <!-- ACCESSMENT DOCUMENTATION BEGINS HERE -->

                         <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../asseessment/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW ALL STUDENT ASSESSMENTS</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../assessment/configuration">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO CONFIGURE RESULT SHEET</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../assessment/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO PUBLISH ALL RESULTS TO PARENTS</div>
                            </a>
                            </div>

                              <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../assessment/grading_system">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW GRADING SYSTEM</div>
                            </a>
                            </div>

                            <!--ASSESSMENTS DOCUMENTATION ENDS HERE -->

                            
                            <!--TIMETABLE MANAGEMENT/ANALYSIS DOCUMENTATION ENDS HERE -->

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TIME SCHEDULES FOR CLASSES</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TIME SCHEDULES FOR TEACHERS</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TIME SCHEDULES FOR EXAMS</div>
                            </a>
                            </div>

                             <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/timetable_timing">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TIMETABLE SCHEDULE TIMIMG</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/timetable_days">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW TIMETABLE SCHEDULING DAYS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../timetable/public_holidays">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW SCHOOL HOLIDAYS AND EVENTS</div>
                            </a>
                            </div>

                       <!--TIMETABLE MANAGEMENT/ANALYSIS DOCUMENTATION ENDS HERE -->

                       <!--ONLINE EXAMINATION MANAGEMENT/ANALYSIS DOCUMENTATIOON STARTS HERE-->

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/exam_scores">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW EXAMINATION SCORES/RECORD</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/exam_questions_answers">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW EXAMINATION QUESTIONS AND ANSWERS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/ongoing_exams">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW ONGOING EXAMINATIONS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO SETUP NEW EXAMINATION</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/type_question">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO TYPE EXAM QUESTIONS AND ANSWERS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO UPLOAD EXAM QUESTIONS FROM EXCEL</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../examination/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO DOWNLAOD/PRINT EXAMINATION TEMPLATE</div>
                            </a>
                            </div>
                  <!--ONLINE EXAMINATION MANAGEMENT/ANALYSIS DOCUMENTATIOON ENDS HERE-->

                  <!--LIBRARY DOCUMENTATIOON STARTS HERE-->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../library/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW LIBRARY</div>
                            </a>
                            </div>
                            
                <!--LIBRARY DOCUMENTATIOON ENDS HERE-->
                            
                  <!--INVENTORY DOCUMENTATIOON STARTS HERE-->
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW INVENTORY</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/inventory_records?status=all">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW INVENTORY ALL STOCK ITEMS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/inventory_records?status=out">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW INVENTORY OUT OF STOCK ITEMS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/inventory_records?status=low">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW INVENTORY LOW IN STOCK ITEMS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO ADD INVENTORY ITEMS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/collected_items">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO VIEW INVENTORY COLLECTED ITEMS</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO CREATE NEW INVENTORY ITEM</div>
                            </a>
                            </div>

                            
                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO RECORD INVENTORY ITEMS cOLLECTED</div>
                            </a>
                            </div>

                            <div style="margin:10px" class="searchDiv">
                            <a class="element-box el-tablo" style="display:table-cell; padding-right: 35px;" href="../inventory/">
                                <div class="value1" style="color:#08ACF0; text-transform:uppercase">HOW TO CONFIGURE INVENTORY SETTINGS</div>
                            </a>
                            </div>

                   <!--INVENTORY DOCUMENTATIOON ENDS HERE-->

                            </div>

                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->


<script>
function searchHandler() {
    var keyword = $("#searchKeyword").val().toLowerCase()
    var divs = $(".value1")
    var outterDiv = $(".searchDiv")
    var divLength = $(".searchDiv").length
    
    for (var i = 0; i < divLength; i++) {
        var Divstate = divs[i].innerText.toLowerCase().includes(keyword)
        console.log(Divstate)
        if(Divstate == true){
            // SHOW
            outterDiv[i].style.display = "table-cell";
        }else{
            // HIDE
            outterDiv[i].style.display = "none";
        }
    }

}
</script>


</body>

</html>