<?php
    if(isset($_GET["lid"])){
        include_once ("linkClass.php");
        $linkClass = new linkClass();
        $linkId = htmlentities(trim(base64_decode($_GET["lid"])));
        $resp = $linkClass->verifyLinkId($linkId);
        if($resp != "../../error"){
            $page = "http://www.classworks.xyz/views/assessment/checkResult";
            echo "<script>window.location.assign('".$page."')</script>";
        }else{
            $page = "http://www.classworks.xyz/error";
            echo "<script>window.location.assign('".$page."')</script>";
        }
    }else{
        $page = "http://www.classworks.xyz/error";
        echo "<script>window.location.assign('".$page."')</script>";
    }
?>