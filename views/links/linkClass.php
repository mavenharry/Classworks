<?php
session_start();
    include_once ("../../logic/config.php");

    class linkClass extends logic {
        
        function verifyLinkId($linkId){
            $stmt = $this->dconn->prepare("SELECT * FROM links WHERE id = ?");
            $stmt->bind_param("i", $linkId);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) == 1){
                $row = array_shift($stmt_result);
                $_SESSION["folderName"] = $row["schoolName"];
                $address = $row["schoolname"];
            }else{
                // ERROR
                $address = "../../error";
            }
            return $address;
        }

    }
?>