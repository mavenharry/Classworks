<?php include_once ("tuckshopbtn.php"); 

    if(isset($_GET["getBalance"])){
        $owner = htmlentities(trim($_GET["getBalance"]));
        $reply = $tuckshopClass->getBalance($owner);
        echo $reply;
    }

    if(isset($_GET["verifyStudent"])){
        $owner = htmlentities(trim($_GET["verifyStudent"]));
        $reply = $tuckshopClass->verifyRechargeNumber($owner);
        $reply = json_encode($reply);
        echo $reply;
    }

    if(isset($_GET["getTuckshopDetails"]) && $_GET["getTuckshopDetails"] == 1){
        $reply = $tuckshopClass->getWalletDetails();
        echo json_encode($reply);
    }

    if(isset($_GET["action"]) && $_GET["action"] == "updateWallet"){
        $verifier = htmlentities(trim($_GET["verifier"]));
        $owner = htmlentities(trim($_GET["owner"]));
        $amount = htmlentities(trim($_GET["amount"]));
        
        $reply = $tuckshopClass->verifyRef($verifier);
        if($reply === true){
            $reply = $tuckshopClass->updateWallet($owner, $amount);
            echo $reply;
        }else{
            echo "wrongRef";
        }
    }
?>