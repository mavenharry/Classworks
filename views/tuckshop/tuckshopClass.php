<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class tuckshopClass extends logic {
    
    function getShopProduct(){
        $stmt = $this->uconn->prepare("SELECT * FROM shop_product");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $shopProduct = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->productId = $row["product_id"];
                $obj->productName = $row["product_name"];
                $obj->productQuantity = $row["product_quantity"];
                $obj->productPrice = $row["product_price"];
                $shopProduct[] = $obj;
            }
            $stmt->close();
            return $shopProduct;
        }else{
            $stmt->close();
            return $shopProduct;
        }
    }

    function getBalance($owner){
        $stmt = $this->uconn->prepare("SELECT walletAmount FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $owner);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $balance = array_shift($stmt_result)["walletAmount"];
            $stmt->close();
            return $balance;
        }else{
            $stmt->close();
            return "false";
        }
    }

    function verifyRechargeNumber($owner){
        $stmt = $this->uconn->prepare("SELECT fullname FROM students WHERE student_number = ?");
        $stmt->bind_param("s", $owner);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $obj = new stdClass();
        if(count($stmt_result) > 0){
            $name = array_shift($stmt_result)["fullname"];
            $obj->fullname = $name;
            $obj->status = 1;
        }else{
            $obj->fullname = "N/a";
            $obj->status = 0;
        }
        $stmt->close();
        return $obj;
    }

    function getWalletDetails(){
        $stmt = $this->uconn->prepare("SELECT accountCode FROM tuckshop_setting");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->subAccountCode = $row["accountCode"];
        $obj->email = $_SESSION["email"];
        $obj->school = $this->getSchoolDetails()->school_name;
        $obj->pkey = $this->getSchoolDetails()->keys["paystack_public"];
        $stmt->close();
        return $obj;
    }

    function updateWallet($owner, $amount){
        $stmt = $this->uconn->prepare("UPDATE students SET walletAmount = walletAmount + ? WHERE student_number = ?");
        $stmt->bind_param("is", $amount, $owner);
        if($stmt->execute()){
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }
}
?>