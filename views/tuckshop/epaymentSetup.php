<?php

function checkBusinessName($accountName, $key){
    // CHECK IF BUSINESS NAME EXIST
    $url = "https://api.paystack.co/subaccount";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer '.$key.'',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    $result = json_decode($result, false);
    if(isset($result->data)){
        $accounts = $result->data;
        $subAccountsName = array();
        foreach ($accounts as $key => $value) {
            # code...
            $subAccountsName[] = $value->business_name;
        }

        if(in_array($accountName, $subAccountsName)){
            return true;
        }else{
            return  false;
        }
    }
}


if(isset($action) && $action == 1){
    // CREATE SUBACCOUNT
    $businessName = $accountName;
    $reply = checkBusinessName($businessName, $key);
    if($reply === false){
        // SUBDOMAIN NOT FOUND
        $data = array (
            "business_name" => $businessName,
            "settlement_bank" => $bankName,
            "account_number" => $accountNumber,
            "percentage_charge" => 0,
            "settlement_schedule" => "auto"
        );
        $sent_data = json_encode($data);
        
        $url = "https://api.paystack.co/subaccount";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $sent_data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '.$key.'',
            'Content-Type: application/json',
         ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        $result = json_decode($result, false);
    }else{
        $result = new  stdClass();
        $result->status = 20;
        $result->message = "exist";
    }
}
?>