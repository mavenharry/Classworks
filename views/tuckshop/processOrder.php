<?php include_once ("tuckshopbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Payment</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php")?>
</head>
<?php
    $schoolDetails = $tuckshopClass->getSchoolDetails();
    $allProduct = $tuckshopClass->getShopProduct();

    if(isset($_POST['studentSearchBtn'])){
        if(!empty($_POST['orderStudentNumber'])){
            $studentNumber = $_POST['orderStudentNumber'];
            $studentDetails = $tuckshopClass->getStudentDetails($studentNumber);
        }
    }
    
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Payments", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <h6 class="element-header">PROCESS ORDER</h6>

                                
                                <div class="row" style="margin-top:20px;">

                                <div class="col-lg-6">
                                    <form style="margin-bottom:20px; margin-top:10px; margin-left:5px;" method="POST" action="">
                                        <div class="form-group row">
                                        <div class="col-sm-7">
                                            <input class="form-control" name="orderStudentNumber" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" placeholder="E.g; <?php echo date("Y").'/stdn/'.$schoolDetails->schoolFolder.'/001'?>" type="text">
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary" type="submit" name="studentSearchBtn"> Search Student</button>
                                        </div>
                                        </div>
                                    </form>
                                
                                    <div class="element-box">
                                        <?php
                                            if(isset($studentDetails)){ ?>
                                            <h5 class="form-header">
                                                STUDENT'S BIO
                                            </h5>
                                                <div class="profile-tile" style="padding-top:20px; padding-left:30px; border-top-right-radius:10px; border-top-left-radius:10px; background-color:#FFFFFF">
                                                    <div class="my_hover_up pt-avatar-w" style="border-radius:0px;">
                                                        <?php if(!empty($studentDetails->profilePhoto)){ ?>
                                                        <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="<?php echo $schoolDetails->pathToPassport."/".$studentDetails->profilePhoto; ?>">
                                                        <?php }else { ?>
                                                            <img alt="" style="margin:5px; border-radius:5px; border: 1px solid #08ACF0; width:110px; height:120px;" src="../../images/male-user-profile-picture.png">            
                                                        <?php } ?>
                                                    </div>
                                                    <div class="profile-tile-meta">
                                                        <ul style="text-transform:capitalize">
                                                            <li style="color:#8095A0;">Name: <strong style="color:#8095A0; font-weight:600"><?php echo ucwords($studentDetails->fullname); ?></strong></li>
                                                            <li style="color:#8095A0">Age: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->age; ?> Years</strong></li>
                                                            <li style="color:#8095A0">Class: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->present_class; ?></strong></li>
                                                            <li style="color:#8095A0">Gender: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->gender; ?></strong></li>
                                                            <li style="color:#8095A0">Year of enrollment: <strong style="color:#8095A0; font-weight:600"><?php echo $studentDetails->enrolment_year; ?></strong></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:15px;">
                                                    <div class="form-group col-lg-8" style="margin-top:0px;">
                                                        <h3 for="">Balance</h3>
                                                    </div>
                                                    <div class="" style="margin-top:0px;">
                                                        <h3 data-balance="<?php echo $studentDetails->walletAmount; ?>" class="balanceAmount" for="">&#8358;<?php echo number_format($studentDetails->walletAmount); ?></h3>
                                                    </div>
                                                </div>
                                            <?php }else{ ?>
                                                    <!-- NO STUDENT NUMBER ENTERED YET -->
                                                    <div class="" style="margin-top:0px;">
                                                        <h6 for="">Search for student using student number to view details</h6>
                                                    </div>
                                            <?php }
                                        ?>
                                        
                                    </div>
                                        
                                </div>

                                <div class="col-lg-6">

                                <form style="margin-bottom:20px; margin-top:10px; margin-left:5px;" method="POST">
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <select class="form-control select2 purchaseItemName" name="purchaseItemName[]" style="width:100%" multiple="multiple">
                                                <?php
                                                    foreach($allProduct as $key => $product){ ?>
                                                        <option data-price="<?php echo $product->productPrice; ?>" data-item-name="<?php echo ucwords($product->productName) ?>" value="<?php echo $product->productId; ?>"><?php echo ucwords($product->productName) ?></option>
                                                    <?php }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <input class="form-control" name="purchaseItemQuantity" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" placeholder="Quantity" type="number">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="submit" name="addPurchaseItem" value="Add Item" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>

                                <div class="element-box">
                                    <h5 class="form-header">
                                        ORDERED PRODUCTS
                                    </h5>

                                <!-- STUDENTS SLIP -->

                                <div class="table-responsive">
                                        <table class="table table-padded itemTable">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Product Name</th>
                                                    <th>Quantity</th>
                                                    <th class="text-center">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                        <div class="row" style="margin-top:30px; margin-bottom: 20px;">
                                            <div class="form-group col-lg-10" style="margin-top:0px;">
                                                <div class="form-check"><label class="form-check-label"><input required class="form-check-input" type="checkbox">I agree to terms and conditions</label></div>
                                            </div>
                                            <div class="form-group col-lgs-2" style="margin-top:0px;">
                                                <h5 for="" class="total">&#8358;0</h5>
                                            </div>
                                        </div>

                                        <div class="" style="margin-top: -30px; float:right;">
                                            <input type="submit" name="addPurchaseItem" value="Save" class="btn btn-primary">
                                        </div>

                                </div>
                                </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>