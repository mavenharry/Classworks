<?php include_once ("tuckshopbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Inventory</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $tuckshopClass->getSchoolDetails();
?>
<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("TuckShop", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-panel-toggler" style="margin-top:10px;">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <h6 class="element-header">TUCKSHOP MANAGEMENT/ANALYSIS</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./product_list" style="border: 0px dashed #08ACF0">
                                                    <div class="label">View TuckShop Product list</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-coins-4"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">All Product List</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-database"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="" data-target="#checkWalletBalance" data-toggle="modal">
                                                    <div class="label">Check student wallet Balance</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-pie-chart"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Wallet Balance</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" data-target="#rechargeWallet" data-toggle="modal" href="">
                                                    <div class="label">Recharge a student wallet</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-grid-18"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Recharge Wallet</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" target="_blank" data-target="#addNewProduct" data-toggle="modal" href="">
                                                    <div class="label">Create New Tuckshop Product</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Add Product</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="processOrder">
                                                    <div class="label">Process a product purchase</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-emoticon-smile"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Process Order</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" data-target="#tuckshopConfiguration" data-toggle="modal" href="">
                                                    <div class="label">Configure tuckshop settings</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-settings"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Configuration</div>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
<script>
    var selectedTimetableClass = $("#timetableClassSelector").val();
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableClass: selectedTimetableClass},
        cache: false,
        success: function(results){
            $(".timeTableSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
</script>
</body>

</html>