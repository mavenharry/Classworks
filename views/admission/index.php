<?php include_once ("admissionbtn.php");

$schoolDetails = $admissionClass->getSchoolDetails();
$state = $admissionClass->getEnrollmentState();
$admissionParam = json_decode($admissionClass->getAdmissionParamaters());
?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <title><?php echo ucwords($schoolDetails->school_name)?> - Student</title>

        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
            <!-------------------- END - Meta -------------------->
            <?php include_once ("../../includes/styles.php"); ?>

                <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,projection,print" />
                <link rel="stylesheet" type="text/css" href="./css/application.css" />
                <link rel="stylesheet" type="text/css" href="../../css/main.php" />
    </head>

    <body>
        <div id="container">

            <form method="post" action="">
                <div>
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div>
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div id="control_container">
                    <img src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" style="width:120px; height:120px; object-fit: scale-down" />
                    <div id="navigation">
                        <ul>
                            <li class="active"><a href="#">Summary</a></li>
                            <li><a href="#">Student Details</a></li>
                            <li><a href="#">Parent(s) Details</a></li>
                            <li><a href="#">Academic Details</a></li>
                            <li><a href="#">Admission Status</a></li>
                        </ul>
                    </div>
                    <br>
                    <h1><?php echo ucwords($schoolDetails->school_name)?> Admissions</h1>
                    <p>Welcome to the first stage of the <?php echo ucwords($schoolDetails->school_name)?> admission enrollment system. Please complete the requested information to submit form. Upon submitting this information, an account will be opened for you and you will be able to begin the application process.
                        <br/>
                        <br/>Please note that receipt of this application does not guarantee you admission in <?php echo ucwords($schoolDetails->school_name)?>. If do not apply for admission again if you have applied before. Please contact us at admissions@classworks.xyz if you need more information to continue with the application process.</p>
                        <?php
                            if($state == 1){
                                if($admissionParam->feeState == 1){ ?>
                                    <h5 style="text-align:left"><span style="font-weight:normal">Application fee: &#x20a6;</span><?php echo number_format($admissionParam->amount); ?></h5>
                                <?php }   
                            }
                        ?>
                    <br>

                </div>

                <div id="button_container">
                    <a id="btnSubmit" data-target="#admissionStatusModal" data-toggle="modal" href="">Check Admission Status</a>
                    <?php
                        if($state == 1){ ?>
                            <a id="btnSubmit" href="./student">Start A New Admission Application</a>
                        <?php }else{ ?>
                            <a id="btnSubmit" class="btnSubmitClose" href="#" >Admission Not In Progress</a>
                        <?php }
                    ?>
                </div>

            </form>
        </div>
    </body>

    <!-------------------- START - MODAL -------------------->
    <?php include("../../includes/modals/index.php"); ?>
        <!-------------------- END - MODAL -------------------->

        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/support.php") ?>
            <!-------------------- END - Top Bar -------------------->

            <?php include_once ("../../includes/scripts.php"); ?>

    </html>