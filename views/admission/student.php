<?php include_once ("admissionbtn.php");

$schoolDetails = $admissionClass->getSchoolDetails();

if(isset($_GET["ver"])){
    $studentNumber = htmlentities(trim(base64_decode($_GET["ver"])));
    $applicantDetails = $admissionClass->getApplicantDetails($studentNumber);
}
?>
    <!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
    <title><?php echo ucwords($schoolDetails->school_name)?> - Student</title>

        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
            <!-------------------- END - Meta -------------------->
            <?php include_once ("../../includes/styles.php"); ?>

                <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,projection,print" />
                <link rel="stylesheet" type="text/css" href="./css/application.css" />
                <link rel="stylesheet" type="text/css" href="../../css/main.php" />
    </head>

    <body>
        <div id="container">

                <div>
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div>
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div id="control_container">
                    <img src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" style="width:160px; height:160px; object-fit: scale-down" />
                    <div id="navigation">
                        <ul>
                            <li><a href="#">Summary</a></li>
                            <li class="active"><a href="#">Student Details</a></li>
                            <li><a href="#">Parent(s) Details</a></li>
                            <li><a href="#">Academic Details</a></li>
                            <li><a href="#">Admission Status</a></li>
                        </ul>
                    </div>
                    <br>
                    <h1>Student Details/Information</h1>
                    <p>Fill the form below with concise and accurate applicant student information. Note this information is secure and will only be used for admission considerations. Please information below is essential as we use it to shortlist admittable and qualified applicants for admission. Please feel free to contact us if you need more information on this matter.</p>

                    <!-- FORM -->

                    <div class="modal-centered" style="margin-top:-10px; text-align:left" role="document">
                        <div style="width:80%" class="center">
                            <div class="onboarding-content with-gradient">
                                <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                                    <div class="row form-group" style="margin-top:40px; margin-bottom:20px;">
                                        <div class="col-sm-6">
                                            <label style="cursor:pointer; margin-top:20px; border-color:#08ACF0; background-color:#08ACF0" class="my_hover_up btn btn-primary" for="studentupload-photo"> Student Photo from Camera</label>
                                            <label style="cursor:pointer; margin-top:5px; border-color:#FB5574; background-color:#FB5574" class="my_hover_up btn btn-primary" for="studentupload-photo"> Student Photo from Computer</label>
                                            <input required id="studentupload-photo" accept="image/png, image/jpeg, image/gif" style="opacity: 0; position: absolute; z-index: -1; " class="form-control" type="file" name="profilePhoto">
                                        </div>
                                        <div class="pt-avatar-w col-sm-6" style="border-radius:0px;">
                                            <img alt="" id="studentuploadPreview" style="margin-left:80px; border-radius:5px; border: 2px solid #08ACF0; height:130px; width:120px" src="../../images/male-user-profile-picture.png">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Student Previous School Reg. No.</label>
                                        <input name="previousRegNo" class="form-control" value="2018/Stdn/Geroge/007" type="text">
                                    </div>

                                    <h4 style="text-decoration:underline; margin-top:25px; margin-bottom:30px;">New Admission Application Procedure</h4>

                                    <div class="form-group">
                                        <label for=""> Student Application No.</label>
                                        <input name="studentNumber" class="form-control" readonly value="<?php echo isset($applicantDetails->applicant_no) && $applicantDetails->applicant_no != "" ? $applicantDetails->applicant_no : $admissionClass->newApplicationNumber(); ?>" type="text">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-7">
                                            <label for=""> Student Full name <span style="color:#F91F14">*</span></label>
                                            <input required name="studentName" class="form-control" value="<?php echo isset($applicantDetails->fullname) ? $applicantDetails->fullname : ""?>" placeholder="E.g; Kings Smart Amadi" type="text">
                                        </div>
                                        <div class="form-group col-sm-5">
                                            <label for="">Student Health Status</label>
                                            <select name="studentHealth" class="form-control">
                                            <option <?php echo isset($applicantDetails->health) && $applicantDetails->health == "healthy" ? "selected" : ""?> value="healthy">Healthy</option>
                                            <option <?php echo isset($applicantDetails->health) && $applicantDetails->health == "Sickler" ? "selected" : ""?> value="Sickler">Sickler</option>
                                            <option <?php echo isset($applicantDetails->health) && $applicantDetails->health == "test health status" ? "selected" : ""?> value="test health status">Test Health Status</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Nationality</label>
                                                <Select name="studentCountry" class="form-control">
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="algeria">Algeria</option>
                                                    <option value="angola">Angola</option>
                                                    <option value="benin">Benin</option>
                                                    <option value="botswana">Botswana</option>
                                                    <option value="burkina Faso">Burkina Faso</option>
                                                    <option value="burundi">Burundi</option>
                                                    <option value="cameroon">Cameroon</option>
                                                    <option value="cape-verde">Cape Verde</option>
                                                    <option value="central-african-republic">Central African Republic</option>
                                                    <option value="chad">Chad</option>
                                                    <option value="comoros">Comoros</option>
                                                    <option value="congo-brazzaville">Congo - Brazzaville</option>
                                                    <option value="congo-kinshasa">Congo - Kinshasa</option>
                                                    <option value="ivory-coast">Côte d’Ivoire</option>
                                                    <option value="djibouti">Djibouti</option>
                                                    <option value="egypt">Egypt</option>
                                                    <option value="equatorial-guinea">Equatorial Guinea</option>
                                                    <option value="eritrea">Eritrea</option>
                                                    <option value="ethiopia">Ethiopia</option>
                                                    <option value="gabon">Gabon</option>
                                                    <option value="gambia">Gambia</option>
                                                    <option value="ghana">Ghana</option>
                                                    <option value="guinea">Guinea</option>
                                                    <option value="guinea-bissau">Guinea-Bissau</option>
                                                    <option value="kenya">Kenya</option>
                                                    <option value="lesotho">Lesotho</option>
                                                    <option value="liberia">Liberia</option>
                                                    <option value="libya">Libya</option>
                                                    <option value="madagascar">Madagascar</option>
                                                    <option value="malawi">Malawi</option>
                                                    <option value="mali">Mali</option>
                                                    <option value="mauritania">Mauritania</option>
                                                    <option value="mauritius">Mauritius</option>
                                                    <option value="mayotte">Mayotte</option>
                                                    <option value="morocco">Morocco</option>
                                                    <option value="mozambique">Mozambique</option>
                                                    <option value="namibia">Namibia</option>
                                                    <option value="niger">Niger</option>
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="rwanda">Rwanda</option>
                                                    <option value="reunion">Réunion</option>
                                                    <option value="saint-helena">Saint Helena</option>
                                                    <option value="senegal">Senegal</option>
                                                    <option value="seychelles">Seychelles</option>
                                                    <option value="sierra-leone">Sierra Leone</option>
                                                    <option value="somalia">Somalia</option>
                                                    <option value="south-africa">South Africa</option>
                                                    <option value="sudan">Sudan</option>
                                                    <option value="south-sudan">Sudan</option>
                                                    <option value="swaziland">Swaziland</option>
                                                    <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                                                    <option value="tanzania">Tanzania</option>
                                                    <option value="togo">Togo</option>
                                                    <option value="tunisia">Tunisia</option>
                                                    <option value="uganda">Uganda</option>
                                                    <option value="western-sahara">Western Sahara</option>
                                                    <option value="zambia">Zambia</option>
                                                    <option value="zimbabwe">Zimbabwe</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                                                <input required name="studentState" value="<?php echo isset($applicantDetails->state) ? $applicantDetails->state : ""?>" class="form-control" placeholder="E.g; Lagos" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> City/Region <span style="color:#F91F14">*</span></label>
                                                <input required name="studentCity" value="<?php echo isset($applicantDetails->city) ? $applicantDetails->city : ""?>" class="form-control" placeholder="E.g; Ikeja" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Date of Birth <span style="color:#F91F14">*</span></label>
                                                <div class="date-input">
                                                    <input required name="studentDob" value="<?php echo isset($applicantDetails->dob) ? $applicantDetails->dob : ""?>" class="date_of_birth form-control" placeholder="Date of birth" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Gender</label>
                                                <Select id="studentGender" name="studentGender" class="form-control">
                                                    <option <?php echo isset($applicantDetails->gender) && $applicantDetails->gender == "Male" ? "selected" : ""?> value="Male">Male</option>
                                                    <option <?php echo isset($applicantDetails->gender) && $applicantDetails->gender == "Female" ? "selected" : ""?> value="Female">Female</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Blood Group</label>
                                                <Select name="studentBloodGroup" class="form-control">
                                                    <option <?php echo isset($applicantDetails->bloodGroup) && $applicantDetails->bloodGroup == "A" ? "selected" : ""?> value="A">A</option>
                                                    <option <?php echo isset($applicantDetails->bloodGroup) && $applicantDetails->bloodGroup == "B" ? "selected" : ""?> value="B">B</option>
                                                    <option <?php echo isset($applicantDetails->bloodGroup) && $applicantDetails->bloodGroup == "AB" ? "selected" : ""?> value="AB">AB</option>
                                                    <option <?php echo isset($applicantDetails->bloodGroup) && $applicantDetails->bloodGroup == "O" ? "selected" : ""?> value="O">O</option>
                                                    <option <?php echo isset($applicantDetails->bloodGroup) && $applicantDetails->bloodGroup == "Not Known" ? "selected" : ""?> value="Not Known">Not Known</option>
                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <br>

        </div>

        <div id="button_container">
            <a id="btnSubmit" href="./">Go Back To Summary</a>
            <input name="saveStudent" style="cursor:pointer; padding:10px; border-radius:3px; color:#08acf0; border: 1px solid #08acf0; background:transparent" type="submit" value="Save Student Information & Continue">
        </div>
    </form>
        <br>
        <br>
        </div>

        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/support.php") ?>
            <!-------------------- END - Top Bar -------------------->
            <?php include_once ("../../includes/scripts.php"); ?>

    </body>

    </html>