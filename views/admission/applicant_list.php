<?php include_once ("admissionbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Students</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
$schoolDetails = $admissionClass->getSchoolDetails();
$allApplicant = $admissionClass->getAllApplicant();
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Admission", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" href="./">
                                        <i class="os-icon os-icon-plus"></i>
                                        <span style="font-size: .9rem;">Make a new enrollment</span>
                                    </a>
                                    <a data-target="#searchStudentsModal" data-toggle="modal" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL <?php echo $schoolDetails->academic_session?> INCOMING ENROLLMENT(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Appl. number</th>
                                                <th>Name</th>
                                                <th>Gender</th>
                                                <th>Class</th>
                                                <th>School Type</th>
                                                <th class="text-center">View Application</th>
                                                <th class="text-center">Admission</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($allApplicant) == 0){
                                                        echo "No Applicant found for this session";
                                                    }else{
                                                        foreach($allApplicant as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?>.</span>
                                                                </td>
                                                                <td>

                                                                    <?php if(!empty($value->profilePhoto)){ ?>
                                                                    <div class="user-with-avatar">
                                                                    <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/applicants/".$value->profilePhoto;?>">
                                                                    </div>
                                                                    <?php }else{ ?>
                                                                        <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                        class="badge"><?php echo $nameInitials = $admissionClass->createAcronym($value->fullname); ?></a>
                                                                    <?php } ?>

                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->applicant_no; ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucfirst($value->fullname); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucfirst($value->gender); ?></span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <span><?php echo ucfirst($value->applyClass); ?></span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <span><?php echo ucfirst($value->applyType); ?></span>
                                                                </td>
                                                                <td class="text-center">
                                                                    <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                        class="my_hover_up_bg badge" href="#">View this form</a>
                                                                </td>
                                                                <td class="text-center">
                                                                        <?php
                                                                            if($value->applyState == 1){ ?>
                                                                                <!-- // ADMITTED -->
                                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                                    class="my_hover_up_bg badge" href="#">Admitted</a>
                                                                            <?php }else{ ?>
                                                                                <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF"
                                                                                    class="my_hover_up_bg badge admitApplicant" data-id="<?php echo $value->applicant_no; ?>" href="#">Admit now</a>
                                                                            <?php }
                                                                        ?>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <!-- LOAD MORE -->
                                        <?php
                                            if(2 > 20){ ?>
                                                <div style="float: right;" class="element-actions">
                                                <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                                <span style="font-size: .9rem;">Load more</span>
                                                </a>
                                                </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->
               

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>