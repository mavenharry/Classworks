<?php include_once ("admissionbtn.php");
$schoolDetails = $admissionClass->getSchoolDetails();
$enrolSettings = $admissionClass->getEnrolSettings();
?>

    <!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
    <title><?php echo ucwords($schoolDetails->school_name)?> - Student</title>

        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
            <!-------------------- END - Meta -------------------->
            <?php include_once ("../../includes/styles.php"); ?>

                <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,projection,print" />
                <link rel="stylesheet" type="text/css" href="./css/application.css" />
                <link rel="stylesheet" type="text/css" href="../../css/main.php" />
    </head>

    <body>
        <div id="container">

            <form method="post" action="">
                <div>
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div>
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div id="control_container">
                    <img src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" style="width:160px; height:160px; object-fit: scale-down" />
                    <div id="navigation">
                        <ul>
                            <li><a href="#">Summary</a></li>
                            <li><a href="#">Student Details</a></li>
                            <li><a href="#">Parent(s) Details</a></li>
                            <li class="active"><a href="#">Academic Details</a></li>
                            <li><a href="#">Admission Status</a></li>
                        </ul>
                    </div>
                    <br>
                    <h1>Academic's Details/Information</h1>
                    <p>Fill the form below with concise and accurate applicant academic's information. Note this information is secure and will only be used for admission considerations. Please information below is essential as we use it to shortlist admittable and qualified applicants for admission. Please feel free to contact us if you need more information on this matter.</p>

                    <br>
                    <br>
                    <!-- FORM -->

                    <div class="modal-centered" style="margin-top:-10px; text-align:left" role="document">
                        <div style="width:80%" class="center">
                            <div class="onboarding-content with-gradient">
                                <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Admission Class</label>
                                                <Select name="admissionClass" class="form-control">
                                                    <?php
                                                        foreach ($enrolSettings->classes as $key => $value) { ?>
                                                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                        <?php }
                                                    ?>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for=""> Admission Session <span style="color:#F91F14">*</span></label>
                                                    <input name="admissionSession" class="form-control" value="<?php echo ($enrolSettings->session - 1)."/".$enrolSettings->session;?>" readonly type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Admission Term/Season <span style="color:#F91F14">*</span></label>
                                                <Select name="admissionTerm" class="form-control">
                                                    <option value="1">First Term</option>
                                                    <option value="2">Second Term</option>
                                                    <option value="3">Third Term</option>
                                                    <option value="4">Any Term</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for=""> Admission Batch <span style="color:#F91F14">*</span></label>
                                                    <input name="admissionBatch" class="form-control" value="<?php echo $enrolSettings->batch;?>" readonly type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Schooling Type</label>
                                                <Select name="schoolType" class="form-control">
                                                    <option value="Day">Day</option>
                                                    <option value="Boarding">Boarding</option>
                                                    <option value="Any Batch">Any Type</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Learning Type</label>
                                                <Select name="LearnType" class="form-control">
                                                    <option value="In-Class">In-Class</option>
                                                    <option value="Virtual">Virtual</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 style="text-decoration:underline; margin-top:10px; margin-bottom:30px;">Previous School Information</h4>

                                    <div class="form-group">
                                        <label for=""> School Name <span style="color:#F91F14">*</span></label>
                                        <input required name="schoolName" class="form-control" placeholder="E.g; SkySurf International College" type="text">
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Country <span style="color:#F91F14">*</span></label>
                                                <Select required name="schoolCountry" class="form-control">
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="algeria">Algeria</option>
                                                    <option value="angola">Angola</option>
                                                    <option value="benin">Benin</option>
                                                    <option value="botswana">Botswana</option>
                                                    <option value="burkina Faso">Burkina Faso</option>
                                                    <option value="burundi">Burundi</option>
                                                    <option value="cameroon">Cameroon</option>
                                                    <option value="cape-verde">Cape Verde</option>
                                                    <option value="central-african-republic">Central African Republic</option>
                                                    <option value="chad">Chad</option>
                                                    <option value="comoros">Comoros</option>
                                                    <option value="congo-brazzaville">Congo - Brazzaville</option>
                                                    <option value="congo-kinshasa">Congo - Kinshasa</option>
                                                    <option value="ivory-coast">Côte d’Ivoire</option>
                                                    <option value="djibouti">Djibouti</option>
                                                    <option value="egypt">Egypt</option>
                                                    <option value="equatorial-guinea">Equatorial Guinea</option>
                                                    <option value="eritrea">Eritrea</option>
                                                    <option value="ethiopia">Ethiopia</option>
                                                    <option value="gabon">Gabon</option>
                                                    <option value="gambia">Gambia</option>
                                                    <option value="ghana">Ghana</option>
                                                    <option value="guinea">Guinea</option>
                                                    <option value="guinea-bissau">Guinea-Bissau</option>
                                                    <option value="kenya">Kenya</option>
                                                    <option value="lesotho">Lesotho</option>
                                                    <option value="liberia">Liberia</option>
                                                    <option value="libya">Libya</option>
                                                    <option value="madagascar">Madagascar</option>
                                                    <option value="malawi">Malawi</option>
                                                    <option value="mali">Mali</option>
                                                    <option value="mauritania">Mauritania</option>
                                                    <option value="mauritius">Mauritius</option>
                                                    <option value="mayotte">Mayotte</option>
                                                    <option value="morocco">Morocco</option>
                                                    <option value="mozambique">Mozambique</option>
                                                    <option value="namibia">Namibia</option>
                                                    <option value="niger">Niger</option>
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="rwanda">Rwanda</option>
                                                    <option value="reunion">Réunion</option>
                                                    <option value="saint-helena">Saint Helena</option>
                                                    <option value="senegal">Senegal</option>
                                                    <option value="seychelles">Seychelles</option>
                                                    <option value="sierra-leone">Sierra Leone</option>
                                                    <option value="somalia">Somalia</option>
                                                    <option value="south-africa">South Africa</option>
                                                    <option value="sudan">Sudan</option>
                                                    <option value="south-sudan">Sudan</option>
                                                    <option value="swaziland">Swaziland</option>
                                                    <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                                                    <option value="tanzania">Tanzania</option>
                                                    <option value="togo">Togo</option>
                                                    <option value="tunisia">Tunisia</option>
                                                    <option value="uganda">Uganda</option>
                                                    <option value="western-sahara">Western Sahara</option>
                                                    <option value="zambia">Zambia</option>
                                                    <option value="zimbabwe">Zimbabwe</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                                                <input required name="schoolState" class="form-control" placeholder="E.g; Lagos" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text" hidden id="parentEmail" value="<?php echo $_SESSION["parentEmail"]?>">
                                    <input type="text" id="schoolName" hidden value="<?php echo $schoolDetails->school_name?>">
                            </div>
                        </div>
                    </div>

                    <br>

                </div>
                <div id="button_container">
                    <a id="btnSubmit" href="./parent">Go Back To Parent Information</a>
                    <input style="cursor:pointer; padding:10px; border-radius:3px; color:#08acf0; border: 1px solid #08acf0; background:transparent" type="button" class="applicationPayment" value="Make Payment & Continue">
                    <input name="saveAcademic" id="finalForm" hidden style="cursor:pointer; padding:10px; border-radius:3px; color:#08acf0; border: 1px solid #08acf0; background:transparent" type="submit" value="Save Academic Information & Continue">
                </div>

                <br>
                <br>

                </form>
        </div>

        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/support.php") ?>
            <!-------------------- END - Top Bar -------------------->
            <?php include_once ("../../includes/scripts.php"); ?>

    </body>

    </html>