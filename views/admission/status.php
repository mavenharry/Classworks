<?php include_once ("admissionbtn.php");
$schoolDetails = $admissionClass->getSchoolDetails();
if(isset($_SESSION["studentNumber"]) && isset($_SESSION["studentName"])){
    $studentNumber = $_SESSION["studentNumber"];
    $reply = new stdClass();
    $reply->wardName = $_SESSION["studentName"];
    $reply->applyClass = $_SESSION["admissionClass"];
    $reply->parentName = $_SESSION["parentName"];
    $reply->applyState = 0;
}else{
    if(isset($_POST["applyNumber"])){
        $studentNumber = htmlentities(trim($_POST["applyNumber"]));
        $reply = $admissionClass->checkAdmitted($studentNumber);
        if($reply === false){
            echo "<script>window.location.assign('index')</script>";
        }
    }else{
        echo "<script>window.location.assign('index')</script>";
    }
}
?>
    <!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
    <title><?php echo ucwords($schoolDetails->school_name)?> - Student</title>

        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
            <!-------------------- END - Meta -------------------->
            <?php include_once ("../../includes/styles.php"); ?>

                <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,projection,print" />
                <link rel="stylesheet" type="text/css" href="./css/application.css" />
                <link rel="stylesheet" type="text/css" href="../../css/main.php" />
    </head>

    <body>
        <div id="container">

            <form method="post" action="">
                <div>
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div>
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div id="control_container">
                    <img src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" style="width:160px; height:160px; object-fit: scale-down" />
                    <div id="navigation">
                        <ul>
                            <li><a href="#">Summary</a></li>
                            <li><a href="#">Student Details</a></li>
                            <li><a href="#">Parent(s) Details</a></li>
                            <li><a href="#">Academic Details</a></li>
                            <li class="active"><a href="#">Admission Status</a></li>
                        </ul>
                    </div>
                    <br>
                    <h1><?php echo $reply->applyState == 0 ? "<span style='text-decoration:underline; font-weight:bold; color:#FB5574'>No Admission offered yet</span>" : "<span style='text-decoration:underline; font-weight:bold; color:#1bb4ad'>Congrats!. Admission has been offered</span>"; ?>. Your admission into <span style="color:#FC9933; font-weight:bold"><?php echo $reply->applyClass?></span>, has been applied successfully</h1>
                    <p style="margin-top:20px;">
                        Thank you <span style="color:#1BB4AD; font-weight:bold; text-transform:uppercase"> <?php echo strtoupper($reply->parentName); ?></span> we have joyfully received your admission application for <span style="color:#08ACF0; font-weight:bold; text-transform:uppercase"> <?php echo strtoupper($reply->wardName); ?></span>. The admission officer(s) will immediately look at it and inform you of any other requirements and actions to be taken. We will really love to meet
                        <span style="color:#08ACF0; font-weight:bold; text-transform:uppercase"> <?php echo strtoupper($reply->wardName); ?></span> soonest. Please in case of any suggestion or further inquiry you can contact us using the contact information below - Principal (<?php echo ucwords($schoolDetails->school_name); ?>).
                        <br/>
                    </p>
                    <h4 style="margin-bottom:40px; margin-top:20px;">For More Info, Call: +234 (0) <?php echo substr($schoolDetails->phone, 1); ?></h4>

                </div>

                <div id="button_container">

                    <!-- <a id="btnSubmit" href="./student?ver=<?php //echo base64_encode($studentNumber)?>">Edit Admission Information</a> -->
                    <!-- <a id="btnSubmit" href="">Print Admission Letter</a> -->
                    <a id="btnSubmit" onclick="window.history.back()" href="#">Go Back</a>
                    <a id="btnSubmit" href="<?php echo '../../'.$_SESSION["folderName"].'' ?>">Home</a>
                </div>

            </form>
        </div>
    </body>

    </html>