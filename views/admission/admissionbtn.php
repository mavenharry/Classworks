<?php
include_once ("admissionClass.php");
$admissionClass = new admissionClass();

if(isset($_POST["saveStudent"])){
    $_SESSION["studentNumber"] = $applicantNumber = htmlentities(trim($_POST["studentNumber"]));
    $_SESSION["studentName"] = htmlentities(trim($_POST["studentName"]));
    $_SESSION["studentHealth"] = htmlentities(trim($_POST["studentHealth"]));
    $_SESSION["studentCountry"] = htmlentities(trim($_POST["studentCountry"]));
    $_SESSION["studentState"] = htmlentities(trim($_POST["studentState"]));
    $_SESSION["studentCity"] = htmlentities(trim($_POST["studentCity"]));
    $dob = explode("/", htmlentities(trim($_POST["studentDob"])));
    $_SESSION["studentDob"] = $dob[2]."-".$dob[1]."-".$dob[0];
    $_SESSION["studentGender"] = htmlentities(trim($_POST["studentGender"]));
    $_SESSION["studentBloodGroup"] = htmlentities(trim($_POST["studentBloodGroup"]));

    // MOVE PICKUP PASSPORT
    if($_FILES["profilePhoto"]["size"] > 0){
        if($_FILES["profilePhoto"]["error"]){
            $error = "An error occured with the following error. ".$_FILES["profilePhoto"]["error"];
        }else{
            $extension = @end(explode(".",$_FILES["profilePhoto"]["name"]));
            $imageFolder = $admissionClass->getSchoolDetails()->pathToPassport."/applicants";
            $_SESSION["applicantFilename"] = $filename = str_replace("/","_",$applicantNumber).".".$extension;
            if(file_exists($imageFolder."/".$filename)){
                unlink($imageFolder."/".$filename);
            }
            move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $imageFolder."/".$filename);
            echo "<script>window.location.assign('parent')</script>";
        }
    }
}

if(isset($_POST["saveParent"])){
    $_SESSION["parentName"] = htmlentities(trim($_POST["parentName"]));
    $_SESSION["parentCountry"] = htmlentities(trim($_POST["parentCountry"]));
    $_SESSION["parentState"] = htmlentities(trim($_POST["parentState"]));
    $_SESSION["parentCity"] = htmlentities(trim($_POST["parentCity"]));
    $_SESSION["parentRelation"] = htmlentities(trim($_POST["parentRelation"]));
    $_SESSION["parentGender"] = htmlentities(trim($_POST["parentGender"]));
    $_SESSION["parentMaritalStatus"] = htmlentities(trim($_POST["parentMaritalStatus"]));
    $_SESSION["parentAddress"] = htmlentities(trim($_POST["parentAddress"]));
    $_SESSION["parentOccupation"] = htmlentities(trim($_POST["parentOccupation"]));
    $_SESSION["parentPhone"] = htmlentities(trim($_POST["parentPhone"]));
    $_SESSION["parentEmail"] = htmlentities(trim($_POST["parentEmail"]));
    echo "<script>window.location.assign('academic')</script>";
}

if(isset($_POST["saveAcademic"])){
    $_SESSION["admissionClass"] = htmlentities(trim($_POST["admissionClass"]));
    $_SESSION["admissionSession"] = explode("/", htmlentities(trim($_POST["admissionSession"])))[1];
    $_SESSION["admissionTerm"] = htmlentities(trim($_POST["admissionTerm"]));
    $_SESSION["admissionBatch"] = htmlentities(trim($_POST["admissionBatch"]));
    $_SESSION["schoolType"] = htmlentities(trim($_POST["schoolType"]));
    $_SESSION["LearnType"] = htmlentities(trim($_POST["LearnType"]));
    $_SESSION["schoolName"] = htmlentities(trim($_POST["schoolName"]));
    $_SESSION["schoolCountry"] = htmlentities(trim($_POST["schoolCountry"]));
    $_SESSION["schoolState"] = htmlentities(trim($_POST["schoolState"]));
    $reply = $admissionClass->submitNewApplication();
    if($reply === true){
        echo "<script>window.location.assign('status')</script>";
    }else{
        $err = "An error occurred while trying to complete your application process. Please contact support";
    }
}
?>