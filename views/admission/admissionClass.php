<?php
session_start();
include_once ("../../".$_SESSION["folderName"]."/config.php");

class admissionClass extends logic {
    
    function getEnrollmentState(){
        $stmt = $this->uconn->prepare("SELECT enrol_startDate, enrol_endDate, enrol_slot FROM enrol_settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $startDate = $row["enrol_startDate"];
        $endDate = $row["enrol_endDate"];
        $date = Date("Y-m-d");
        if($startDate <= $date && $date <= $endDate){
            $stmt = $this->uconn->prepare("SELECT COUNT(id) AS totalApplicant FROM applicants WHERE applySession = (SELECT enrol_settings.enrol_session FROM enrol_settings)");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $totalApplicant = array_shift($stmt_result)["totalApplicant"];
            if($row["enrol_slot"] > $totalApplicant){
                $stmt->close();
                return 1;
            }else{
                $stmt->close();
                return 0;
            }
        }else{
            $stmt->close();
            return 0;
        }
    }

    function getAdmissionParamaters(){
        $stmt = $this->uconn->prepare("SELECT enrol_settings.enrol_feeState, enrol_settings.enrol_amount, enrol_settings.enrol_slot, settings.subAccount FROM enrol_settings INNER JOIN settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $obj->pkey = $this->getSchoolDetails()->keys["paystack_public"];
        $obj->feeState = $row["enrol_feeState"];
        $obj->amount = $row["enrol_amount"];
        $obj->slot = $row["enrol_slot"];
        $obj->subAccount = $row["subAccount"];
        $stmt->close();
        return json_encode($obj);
    }

    function getEnrolSettings(){
        $stmt = $this->uconn->prepare("SELECT * FROM enrol_settings");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $obj = new stdClass();
        $classCodes = json_decode($row["enrol_classes"], JSON_PRETTY_PRINT);
        $classes = array();
        foreach($classCodes as $key => $value){
            $classes[] = $this->getClassName($value);
        }
        $obj->classes = $classes;
        $obj->session = $row["enrol_session"];
        $obj->batch = $row["enrol_batch"];
        $stmt->close();
        return $obj;
    }

    function newApplicationNumber(){
        $stmt = $this->uconn->prepare("SELECT id FROM applicants WHERE applySession = (SELECT enrol_settings.enrol_session FROM enrol_settings) ORDER BY id DESC LIMIT 1");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $lastNum = array_shift($stmt_result)["id"] + 1;
        if($lastNum < 10){
            $lastNum = "00".$lastNum;
        }else{
            if($lastNum > 9 && $lastNum < 100){
                $lastNum = "0".$lastNum;
            }
        }
        $newAppl = "Appl/".date("Y")."/".$this->getSchoolDetails()->domain_name."/".$lastNum;
        return $newAppl;
    }

    function submitNewApplication(){
        $stmt = $this->uconn->prepare("SELECT id FROM applicants WHERE applicant_no = ?");
        $stmt->bind_param("s", $_SESSION["studentNumber"]);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $newApplNumber = $this->newApplicationNumber();
        }else{
            $applyState = 0;
            $applyDate = Date("Y-m-d");
            $stmt = $this->uconn->prepare("INSERT INTO applicants (fullname, applicant_no, health_stat, nationality, state, city, dob, gender, bloodGroup, profilePhoto, guardianName, guardianNationality, guardianState, guardianCity, guardianRel, guardianGender, guardianMarital, guardianAddress, guardianOccupation, guardianPhone, guardianEmail, applyClass, applySession, applyTerm, applyBatch, applyType, learnType, prevSchoolName, prevSchoolCountry, prevSchoolState, applyDate, applyState) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->bind_param("sssssssssssssssssssssssssssssssi", $_SESSION["studentName"], $_SESSION["studentNumber"], $_SESSION["studentHealth"], $_SESSION["studentCountry"], $_SESSION["studentState"], $_SESSION["studentCity"], $_SESSION["studentDob"], $_SESSION["studentGender"], $_SESSION["studentBloodGroup"], $_SESSION["applicantFilename"], $_SESSION["parentName"], $_SESSION["parentCountry"], $_SESSION["parentState"], $_SESSION["parentCity"], $_SESSION["parentRelation"], $_SESSION["parentGender"], $_SESSION["parentMaritalStatus"], $_SESSION["parentAddress"], $_SESSION["parentOccupation"], $_SESSION["parentPhone"], $_SESSION["parentEmail"], $_SESSION["admissionClass"], $_SESSION["admissionSession"], $_SESSION["admissionTerm"], $_SESSION["admissionBatch"], $_SESSION["schoolType"], $_SESSION["LearnType"], $_SESSION["schoolName"], $_SESSION["schoolCountry"], $_SESSION["schoolState"], $applyDate, $applyState);
            if($stmt->execute()){
                $stmt->close();
                return true;
            }else{
                $stmt->close();
                return false;
            }
        }
    }

    function getApplicantDetails($studentNumber){
        $stmt = $this->uconn->prepare("SELECT * FROM applicants WHERE applicant_no = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->fullname = $row["fullname"];
            $obj->applicant_no = $row["applicant_no"];
            $obj->health = $row["health_stat"];
            $obj->state = $row["state"];
            $obj->city = $row["city"];
            $obj->dob = $row["dob"];
            $obj->gender = $row["gender"];
            $obj->bloodGroup = $row["bloodGroup"];
            $obj->guardianName = $row["guardianName"];
            $obj->guardianNationality = $row["guardianNationality"];
            $obj->guardianState = $row["guardianState"];
            $obj->guardianCity = $row["guardianCity"];
            $obj->guardianRel = $row["guardianRel"];
            $obj->guardianGender = $row["guardianGender"];
            $obj->guardianMarital = $row["guardianMarital"];
            $obj->guardianAddress = $row["guardianAddress"];
            $obj->guardianOccupation = $row["guardianOccupation"];
            $obj->guardianPhone = $row["guardianPhone"];
            $obj->guardianEmail = $row["guardianEmail"];
            $obj->applyClass = $row["applyClass"];
            $obj->applySession = $row["applySession"];
            $obj->applyTerm = $row["applyTerm"];
            $obj->applyBatch = $row["applyBatch"];
            $obj->applyType = $row["applyType"];
            $obj->learnType = $row["learnType"];
            $obj->prevSchoolName = $row["prevSchoolName"];
            $obj->prevSchoolCountry = $row["prevSchoolCountry"];
            $obj->prevSchoolState = $row["prevSchoolState"];
            $obj->applyState = $row["applyState"];
            $stmt->close();
            return $obj;
        }
    }

    function getAllApplicant(){
        $stmt = $this->uconn->prepare("SELECT profilePhoto, applicant_no, fullname, gender, applyClass, applyType, applyState FROM applicants WHERE applySession = (SELECT enrol_settings.enrol_session FROM enrol_settings)");
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $applicantsList = array();
        if(count($stmt_result) > 0){
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $obj->fullname = $row["fullname"];
                $obj->profilePhoto = $row["profilePhoto"];
                $obj->applicant_no = $row["applicant_no"];
                $obj->gender = $row["gender"];
                $obj->applyClass = $row["applyClass"];
                $obj->applyType = $row["applyType"];
                $obj->applyState = $row["applyState"];
                $applicantsList[] = $obj;
            }
            $stmt->close();
            return $applicantsList;
        }else{
            $stmt->close();
            return $applicantsList;
        }
    }

    function admitApplicant($applicantNumber){
        $applyState = 1;
        $stmt = $this->uconn->prepare("SELECT * FROM applicants WHERE applicant_no = ?");
        $stmt->bind_param("s", $applicantNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $studentNumber = $this->newStudentNumber();
        $fullname = $row["fullname"];
        $class = $row["applyClass"];
        $studentCountry = $row["nationality"];
        $studentState = $row["state"];
        $studentCity = $row["city"];
        $studentDob = $row["dob"];
        $studentGender = $row["gender"];
        $studentBloodGroup = $row["bloodGroup"];
        if($row["guardianRel"] == "Father"){
            $studentFatherContact = "0".substr(htmlentities(trim($row["guardianPhone"])), -10, 10);
        }
        if($row["guardianRel"] == "Mother"){
            $studentMotherContact = "0".substr(htmlentities(trim($row["guardianPhone"])), -10, 10);
        }
        $profilePhoto = $row["profilePhoto"];
        $admittedSession = ($row["applySession"] - 1)."/".$row["applySession"];
        $reply = $this->admitNewStudent($studentNumber, $fullname, $class, $studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, @$studentFatherContact, @$studentMotherContact, @$profilePhoto, $admittedSession);
        $formerPath = $this->getSchoolDetails()->pathToPassport."/applicants/".$profilePhoto;
        $destination = $this->getSchoolDetails()->pathToPassport."/".$profilePhoto;
        copy($formerPath, $destination);
        $stmt = $this->uconn->prepare("UPDATE applicants SET applyState = ? WHERE applicant_no = ?");
        $stmt->bind_param("is", $applyState, $applicantNumber);
        if($stmt->execute()){
            $stmt->close();
            return "true";
        }else{
            $stmt->close();
            return "false";
        }
    }

    function newStudentNumber(){
        $end = $this->getLastNumber("students") + 1;
        switch (strlen($end)) {
            case 1:
                # code...
                $end = "00".$end;
                break;
            case 2:
                # code...
                $end = "0".$end;
                break;
            case 3:
                # code
                $end = $end;
                break;
            
            default:
                # code...
                break;
        }
        $domain_name =  $this->getSchoolDetails()->domain_name;
        $newNum = date("Y")."/Stdn"."/".ucfirst($domain_name)."/".$end;
        return $newNum;
        
    }

    function admitNewStudent($studentNumber, $fullname, $class, $studentCountry, $studentState, $studentCity, $studentDob, $studentGender, $studentBloodGroup, $studentFatherContact, $studentMotherContact, $profilePhoto, $admittedSession){
        $stmt = $this->uconn->prepare("SELECT * FROM students WHERE fullname = ? AND father_phone = ? AND mother_phone = ?");
        $fullname = $this->removeAccents($fullname);
        $stmt->bind_param("sss", $fullname, $studentFatherContact, $studentMotherContact);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $stmt->close();
            return "exist";
        }else{
            $stmt1 = $this->uconn->prepare("INSERT INTO students (fullname, student_number, gender, profilePhoto, dob, nationality, state, city, blood_group, present_class, father_phone, mother_phone, enrolment_year) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt1->bind_param("sssssssssssss", strtolower($fullname), strtolower($studentNumber), strtolower($studentGender), strtolower($profilePhoto), strtolower($studentDob), strtolower($studentCountry), strtolower($studentState), strtolower($studentCity), strtolower($studentBloodGroup), $class, strtolower($studentFatherContact), strtolower($studentMotherContact), $admittedSession);
            if($stmt1->execute()){
                $stmt1->close();
                $stmt->close();
                // BACKGROUND PROCESS TO COMPLETE NEW STUDENT PROCESSES
                // $class = str_replace(" ", "*", $class);
                // shell_exec("C://xampp/php/php.exe C://xampp/htdocs/schoolmate/includes/modals/backgroundProcess.php 'addNewStudent' '".$studentNumber."' '".$class."' '".$_SESSION["folderName"]."' >> C://xampp/htdocs/schoolmate/includes/modals/log.log &");
                $reply = $this->backgroundProcessCreateAssessmentNewStudent($studentNumber, $class);
                return "true";
            }else{
                $stmt->close();
                return false;
            }
        }
    }

    function backgroundProcessCreateAssessmentNewStudent($studentNumber, $class){
        $stmt = $this->uconn->prepare("SELECT classes.class_code FROM classes WHERE classes.class_name = ?");
        $stmt->bind_param("s", $class);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        $row = array_shift($stmt_result);
        $academicTerm = $this->getSchoolDetails()->academic_term;
        $academicSession = $this->getSchoolDetails()->academic_session;
        $classCode = $row["class_code"];
        $stmt = $this->uconn->prepare("SELECT subjectCode  FROM subjects WHERE subjectClass = ?");
        $stmt->bind_param("s", $class);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        foreach($stmt_result as $key => $value){
            $stmt = $this->uconn->prepare("SELECT timeTutor FROM schedule_timings WHERE timeSubject = ? AND timeClass = ?");
            $stmt->bind_param("ss", $value["subjectCode"], $classCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $row = array_shift($stmt_result);
                $tutor = $row["timeTutor"];
            }else{
                $tutor = "";
            }
            $stmt = $this->uconn->prepare("SELECT id FROM assessments WHERE student_no = ? AND subject_code = ? AND academic_session = ? AND academic_term = ?");
            $stmt->bind_param("ssss", $studentNumber, $value["subjectCode"], $academicSession, $academicTerm);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) == 0){
                $date = Date("Y-m-d");
                $stmt = $this->uconn->prepare("INSERT INTO assessments (student_no, class_code, subject_code, academic_session, tutor, academic_term, date_created) VALUES (?,?,?,?,?,?,?)");
                $stmt->bind_param("sssssss", $studentNumber, $classCode, $value["subjectCode"], $academicSession, $tutor, $academicTerm, $date);
                $stmt->execute();
            }
        }
    }

    function checkAdmitted($studentNumber){
        $stmt = $this->uconn->prepare("SELECT applyClass, guardianName, fullname, applyState FROM applicants WHERE applicant_no = ?");
        $stmt->bind_param("s", $studentNumber);
        $stmt->execute();
        $stmt_result = $this->get_result($stmt);
        if(count($stmt_result) > 0){
            $row = array_shift($stmt_result);
            $obj = new stdClass();
            $obj->wardName = $row["fullname"];
            $obj->applyClass = $row["applyClass"];
            $obj->parentName = $row["guardianName"];
            $obj->applyState = $row["applyState"];
            $stmt->close();
            return $obj;
        }else{
            $stmt->close();
            return false;
        }
    }
}
?>