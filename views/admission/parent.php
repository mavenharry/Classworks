<?php include_once ("admissionbtn.php");
$schoolDetails = $admissionClass->getSchoolDetails();
?>
    <!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
    <title><?php echo ucwords($schoolDetails->school_name)?> - Student</title>

        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php") ?>
            <!-------------------- END - Meta -------------------->
            <?php include_once ("../../includes/styles.php"); ?>

                <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen,projection,print" />
                <link rel="stylesheet" type="text/css" href="./css/application.css" />
                <link rel="stylesheet" type="text/css" href="../../css/main.php" />
    </head>

    <body>
        <div id="container">

            <form method="post" action="">
                <div>
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div>
                    <input type="hidden" name="" id="" value="" />
                </div>

                <div id="control_container">
                    <img src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>" style="width:160px; height:160px; object-fit: scale-down" />
                    <div id="navigation">
                        <ul>
                            <li><a href="#">Summary</a></li>
                            <li><a href="#">Student Details</a></li>
                            <li class="active"><a href="#">Parent(s) Details</a></li>
                            <li><a href="#">Academic Details</a></li>
                            <li><a href="#">Admission Status</a></li>
                        </ul>
                    </div>
                    <br>
                    <h1>Guardian's Details/Information</h1>
                    <p>Fill the form below with concise and accurate applicant guardian's information. Note this information is secure and will only be used for admission considerations. Please information below is essential as we use it to shortlist admittable and qualified applicants for admission. Please feel free to contact us if you need more information on this matter.</p>

                    <br>
                    <br>
                    <!-- FORM -->

                    <div class="modal-centered" style="margin-top:-10px; text-align:left" role="document">
                        <div style="width:80%" class="center">
                            <div class="onboarding-content with-gradient">
                                <form id="formValidate" method="POST" action="#" enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label for=""> Guardian's Full name <span style="color:#F91F14">*</span></label>
                                        <input required name="parentName" class="form-control" placeholder="E.g; Mr. Mika Amadi" type="text">
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Nationality</label>
                                                <Select name="parentCountry" class="form-control">
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="algeria">Algeria</option>
                                                    <option value="angola">Angola</option>
                                                    <option value="benin">Benin</option>
                                                    <option value="botswana">Botswana</option>
                                                    <option value="burkina Faso">Burkina Faso</option>
                                                    <option value="burundi">Burundi</option>
                                                    <option value="cameroon">Cameroon</option>
                                                    <option value="cape-verde">Cape Verde</option>
                                                    <option value="central-african-republic">Central African Republic</option>
                                                    <option value="chad">Chad</option>
                                                    <option value="comoros">Comoros</option>
                                                    <option value="congo-brazzaville">Congo - Brazzaville</option>
                                                    <option value="congo-kinshasa">Congo - Kinshasa</option>
                                                    <option value="ivory-coast">Côte d’Ivoire</option>
                                                    <option value="djibouti">Djibouti</option>
                                                    <option value="egypt">Egypt</option>
                                                    <option value="equatorial-guinea">Equatorial Guinea</option>
                                                    <option value="eritrea">Eritrea</option>
                                                    <option value="ethiopia">Ethiopia</option>
                                                    <option value="gabon">Gabon</option>
                                                    <option value="gambia">Gambia</option>
                                                    <option value="ghana">Ghana</option>
                                                    <option value="guinea">Guinea</option>
                                                    <option value="guinea-bissau">Guinea-Bissau</option>
                                                    <option value="kenya">Kenya</option>
                                                    <option value="lesotho">Lesotho</option>
                                                    <option value="liberia">Liberia</option>
                                                    <option value="libya">Libya</option>
                                                    <option value="madagascar">Madagascar</option>
                                                    <option value="malawi">Malawi</option>
                                                    <option value="mali">Mali</option>
                                                    <option value="mauritania">Mauritania</option>
                                                    <option value="mauritius">Mauritius</option>
                                                    <option value="mayotte">Mayotte</option>
                                                    <option value="morocco">Morocco</option>
                                                    <option value="mozambique">Mozambique</option>
                                                    <option value="namibia">Namibia</option>
                                                    <option value="niger">Niger</option>
                                                    <option value="nigeria">Nigeria</option>
                                                    <option value="rwanda">Rwanda</option>
                                                    <option value="reunion">Réunion</option>
                                                    <option value="saint-helena">Saint Helena</option>
                                                    <option value="senegal">Senegal</option>
                                                    <option value="seychelles">Seychelles</option>
                                                    <option value="sierra-leone">Sierra Leone</option>
                                                    <option value="somalia">Somalia</option>
                                                    <option value="south-africa">South Africa</option>
                                                    <option value="sudan">Sudan</option>
                                                    <option value="south-sudan">Sudan</option>
                                                    <option value="swaziland">Swaziland</option>
                                                    <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                                                    <option value="tanzania">Tanzania</option>
                                                    <option value="togo">Togo</option>
                                                    <option value="tunisia">Tunisia</option>
                                                    <option value="uganda">Uganda</option>
                                                    <option value="western-sahara">Western Sahara</option>
                                                    <option value="zambia">Zambia</option>
                                                    <option value="zimbabwe">Zimbabwe</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> State/Province <span style="color:#F91F14">*</span></label>
                                                <input required name="parentState" class="form-control" placeholder="E.g; Lagos" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> City/Region <span style="color:#F91F14">*</span></label>
                                                <input required name="parentCity" class="form-control" placeholder="E.g; Ikeja" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Relation to Student</label>
                                                <Select name="parentRelation" class="form-control">
                                                    <option value="Grand Father">Grand Father</option>
                                                    <option value="Grand Mother">Grand Mother</option>
                                                    <option value="Father">Father</option>
                                                    <option value="Mother">Mother</option>
                                                    <option value="Uncle">Uncle</option>
                                                    <option value="Aunt">Aunt</option>
                                                    <option value="Brother">Brother</option>
                                                    <option value="Sister">Sister</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Gender</label>
                                                <Select name="parentGender" class="form-control">
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </Select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Marital Status</label>
                                                <Select name="parentMaritalStatus" class="form-control">
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Separated">Separated</option>
                                                    <option value="Others">Others</option>
                                                </Select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Residential Address</label>
                                        <input name="parentAddress" class="form-control" placeholder="E.g; No. 10, joyce crescent, Ikeja, Lagos" type="text">
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Occupation</label>
                                                <input name="parentOccupation" class="form-control" placeholder="E.g; Lawyer" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Phone Number <span style="color:#F91F14">*</span></label>
                                                <input required name="parentPhone" id="parentPhone" class="form-control" placeholder="E.g; +2348188376352" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for=""> Email Address</label>
                                                <input name="parentEmail" class="form-control" placeholder="E.g; James@gmail.com" type="email">
                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </div>

                    <br>

                </div>
                <div id="button_container">
                    <a id="btnSubmit" href="./student">Go Back To Student Information</a>
                    <input name="saveParent" style="cursor:pointer; padding:10px; border-radius:3px; color:#08acf0; border: 1px solid #08acf0; background:transparent" type="submit" value="Save Parent Information & Continue">
                </div>

                <br>
                <br>

                </form>
        </div>

        <!-------------------- START - Top Bar -------------------->
        <?php include("../../includes/support.php") ?>
        <!-------------------- END - Top Bar -------------------->
        <?php include_once ("../../includes/scripts.php"); ?>

    </body>

    </html>