<?php include_once ("./logic/logicbtn.php"); ?>
<html lang="en" itemscope="" itemtype="//schema.org/MobileApplication">
    <head>
        <title>ClassWorks - School Management Solution</title>
        <!-------------------- START - Meta -------------------->
        <?php include("../../includes/meta.php");
            if(isset($_GET["cde"])){
                $partner = htmlentities(trim(base64_decode($_GET["cde"])));
            }else{
                $partner = "";
            }
        ?>
        <!-------------------- END - Meta -------------------->
        <link rel="stylesheet" href="../../css/toastr.min.css">
        <link rel="stylesheet" href="./css/style.css">
    </head>
    <body class="common signup-new">
        <div class="navigation-backdrop" id="backdrop-nav"></div>
        <div class="main-content">
            <section class="site--body" id="512">
                <div class="row" id="components-signup-form">
                    <div class="col-lg-6">
                        <div class="container col-lg-6__top-bar">
                            <a class="main-nav__logo" href="/" tabindex="-1">
                            <a href="../../"><img style="width:75px; height:80px; margin-top:-10px; margin-bottom:-20px;" alt="" src="../../images/logo.png"></a>
                            </a>
                        </div>
                        <div class="components-signup-form padding-top-sm" style="margin-top:50px;">
                            <div class="tw-form">
                                <form class="form padding-md" id="components-signup-form-step-one" autocomplete="false" method="POST">
                                    <p class="form__focus-message">Power your school with a simplified solution!</p>
                                    <p class="form__focus-message-statement">Want to automate your school? Create an account below.</p>
                                    <div class="form-group">
                                        <input class="form-field" required placeholder="Administrator Full name" type="text" name="name" tabindex="1">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-field" required placeholder="Administrator Email" type="email" name="email" tabindex="1">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-field" required placeholder="Administrator Password" type="password" name="password" tabindex="1">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-field" <?php echo isset($partner) && !empty($partner) ? "readonly" : ""?> value="<?php echo $partner; ?>" id="patCode" placeholder="Partner Code (Optional)" type="text" name="referralCode" tabindex="1">
                                    </div>
                                    <div id="patMessage"></div>
                                    <button name="register" id="btnCeateAccount" class="button button_green button_large button_full-width margin-top-sm" type="submit" tabindex="0">
                                        <span class="ladda-label">Create account now</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-6--cartoon hidden-md-down">
                        <div class="container col-lg-6__top-bar">
                            <div class="pull-right">
                                <p class="col-lg-6__top-bar__paragraph">Already have an account?</p>
                                <a class="col-lg-6__top-bar__login-button" href="../authentication">Login here</a>
                            </div>
                        </div>
                        <div class="steps">
                            <div class="step-one">
                            <blockquote class="blockquote" style="text-align:justify; width:80%; margin-left:10%">
                              <h4 style="font-weight:normal; color:#ffffff; font-size:7rem; margin-bottom:-40px">“</h4>
                              <?php 
                              $json = file_get_contents('../../includes/quotes.json');
                              $json_data = json_decode($json,true); 
                              $random = rand(0,count($json_data) - 1); 
                              ?>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem;"><?php echo $json_data[$random]["text"]?></h4>
                              <h4 style="font-weight:normal; color:#ffffff; font-size:2rem; margin-top:20px; float:right">― <?php echo $json_data[$random]["author"]?></h4>                              
                            </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php include_once ("../../includes/scripts.php"); ?>
        
<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->

        </body>
</html>
