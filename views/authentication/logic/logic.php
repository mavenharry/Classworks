<?php
session_start();
    include_once ("../../logic/config.php");
    
    

    class auth extends logic {
        
        function register($fullname, $email, $password, $referralCode){
            $stmt = $this->dconn->prepare("SELECT fullname FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $stmt1 = $this->dconn->prepare("INSERT INTO users (fullname, email, password, refCode) VALUES (?,?,?,?)");
                $stmt1->bind_param("ssss", $fullname, $email, password_hash(strtolower($password), PASSWORD_BCRYPT), $referralCode);
                if($stmt1->execute()){
                    $reply = $this->sendMail($email, $fullname);
                    if($reply === true){
                        $stmt1->close();
                        return true;
                    }else{
                        $stmt1->close();
                        return false;
                    }
                }else{
                    $stmt1->close();
                    return false;
                }
            }
        }

        function sendMail($email, $fullname){
            // SEND MAIL
                // subject
                $subject = 'New ClassWorks Account Activation.';
                $message = "<!DOCTYPE html>
                <html>
                   <body style='background-color: #222533; padding: 20px; font-family: font-size: 14px; line-height: 1.43; font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif;'>
                      <div style='max-width: 600px; margin: 10px auto 20px; font-size: 12px; color: #8095A0; text-align: center;'>If you are unable to see this message, <a href='#' style='color: #8095A0; text-decoration: underline;'>click here to view in browser</a></div>
                      <div style='max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);'>
                         <table style='width: 100%;'>
                            <tr>
                               <td style='background-color: #fff;'><img alt='' src='http://www.classworks.xyz/images/logo.png' style='width: 70px; margin-left:25px; padding: 20px'></td>
                               <td style='padding-left: 50px; text-align: right; padding-right: 20px;'><a href='../../' style='color: #261D1D; text-decoration: underline; font-size: 14px; letter-spacing: 1px; font-weight:bold; background-color:#08ACF0; color:#FFF; padding:10px; border-radius:5px; text-decoration:none; margin-right:30px;'>Partner Program</a></td>
                            </tr>
                         </table>
                         <div style='padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);'>
                            <h1 style='margin-top: 0px; color:#8095A0'>Hi, ".ucwords($fullname)."</h1>
                            <div style='color: #8095A0; font-size: 14px;'>
                               <p>Thank you for creating account on ClassWorks.xyz, there is one more step before you can use it, you need to activate your account by clicking the link below. Once you click the button, just login to your account and you are set to go.</p>
                            </div>
                            <a href='https://classworks.xyz/views/setup?verifier=".base64_encode($email)."' style='padding: 8px 20px; background-color: #08ACF0; color: #fff; font-weight: bolder; font-size: 16px; display: inline-block; margin: 20px 0px; margin-right: 20px; text-decoration: none;'>Activate my account</a>
                            <h4 style='margin-bottom: 10px; color:#8095A0'>Need Help?</h4>
                            <div style='color: #8095A0; font-size: 12px;'>
                               <p>If you have any questions you can simply reply to this email or find our contact information below. Also contact us at <a href='#' style='text-decoration: underline; color: #8095A0;'>hello@ClassWorks.xyz</a></p>
                            </div>
                         </div>
                         <div style='background-color: #F5F5F5; padding: 40px; text-align: center;'>
                            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/twitter.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/facebook.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/linkedin.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/instagram.png' style='width: 28px;'></a></div>
                            <div style='color: #8095A0; font-size: 12px; margin-bottom: 20px; padding: 0px 50px;'>You are receiving this email because you signed up for Classworks.xyz. All emails are sent from Classworks systems.</div>
                            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/market-google-play.png' style='height: 33px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/market-ios.png' style='height: 33px;'></a></div>
                            <div style='margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);'>
                               <div style='color: #8095A0; font-size: 10px; margin-bottom: 5px;'>No. 10 NTA/Choba Road, Ada Odum Complex, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</div>
                               <div style='color: #8095A0; font-size: 10px;'>Copyright <?php echo date('Y') ?> ClassWorks.xyz. All rights reserved.</div>
                            </div>
                         </div>
                      </div>
                   </body>
                </html>";
                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ClassWorks.xyz <hello@classworks.xyz>' . "\r\n";
                
                // Mail it
                
                if(@mail($email, $subject, $message, $headers)){
                    return true;
                }else{
                    return false;
                }
        }

        function login($uniqueValue, $password){
            $stmt = $this->dconn->prepare("SELECT * FROM staffs WHERE email = ? OR uniqueNumber = ?");
            $stmt->bind_param("ss", $uniqueValue, $uniqueValue);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) == 1){
                $row = array_shift($stmt_result);
                if(password_verify(strtolower($password), $row["password"])){
                    $obj = new stdClass();
                    $obj->fullname = $row["fullname"];
                    $obj->uniqueNumber = $row["uniqueNumber"];
                    $obj->schoolDomain = $row["school_domain"];
                    $obj->role = $row["role"];
                    $obj->email = $row["email"];
                    $stmt->close();
                    return $obj;
                }else{
                    $stmt->close();
                    return "password";
                }
            }else{
                // CHECK PARTNERS
                $stmt = $this->dconn->prepare("SELECT * FROM partners WHERE email = ? OR partner_no = ?");
                $stmt->bind_param("ss", $uniqueValue, $uniqueValue);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) == 1){
                    $row = array_shift($stmt_result);
                    if(password_verify(strtolower($password), $row["password"])){
                        $obj = new stdClass();
                        $obj->fullname = $row["fullname"];
                        $obj->uniqueNumber = $row["partner_no"];
                        $obj->schoolDomain = "partners";
                        $obj->role = "partner";
                        $obj->email = $row["email"];
                        $stmt->close();
                        return $obj;
                    }else{
                        $stmt->close();
                        return "password";
                    }
                }else{
                    $stmt->close();
                    return "uniqueValue";
                }
            }
        }

        function verifyPatCode($patCode){
            $stmt = $this->dconn->prepare("SELECT id FROM partners WHERE partner_no = ?");
            $stmt->bind_param("s", $patCode);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }


        function passwordEmail($email, $fullname, $password){
            // SEND MAIL
                // subject
                $subject = 'ClassWorks Password Recovery';
                $message = "<!DOCTYPE html>
                <html>
                   <body style='background-color: #222533; padding: 20px; font-family: font-size: 14px; line-height: 1.43; font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif;'>
                      <div style='max-width: 600px; margin: 10px auto 20px; font-size: 12px; color: #8095A0; text-align: center;'>If you are unable to see this message, <a href='#' style='color: #8095A0; text-decoration: underline;'>click here to view in browser</a></div>
                      <div style='max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);'>
                         <table style='width: 100%;'>
                            <tr>
                               <td style='background-color: #fff;'><img alt='' src='http://www.classworks.xyz/images/logo.png' style='width: 70px; margin-left:25px; padding: 20px'></td>
                               <td style='padding-left: 50px; text-align: right; padding-right: 20px;'><a href='../../' style='color: #261D1D; text-decoration: underline; font-size: 14px; letter-spacing: 1px; font-weight:bold; background-color:#08ACF0; color:#FFF; padding:10px; border-radius:5px; text-decoration:none; margin-right:30px;'>Partner Program</a></td>
                            </tr>
                         </table>
                         <div style='padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);'>
                            <h1 style='margin-top: 0px; color:#8095A0'>Hi, ".ucwords($fullname)."</h1>
                            <div style='color: #8095A0; font-size: 14px;'>
                               <p>Your passwords on ClassWorks.xyz has been changed, You can login into your account with the below password. NB: Change password in your account after login.</p>
                            </div>
                            <h1 style='margin-top: 0px; color:#8095A0'>New Password:, ".($password)."</h1>
                            <h4 style='margin-bottom: 10px; color:#8095A0'>Need Help?</h4>
                            <div style='color: #8095A0; font-size: 12px;'>
                               <p>If you have any questions you can simply reply to this email or find our contact information below. Also contact us at <a href='#' style='text-decoration: underline; color: #8095A0;'>hello@ClassWorks.xyz</a></p>
                            </div>
                         </div>
                         <div style='background-color: #F5F5F5; padding: 40px; text-align: center;'>
                            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/twitter.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/facebook.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/linkedin.png' style='width: 28px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/instagram.png' style='width: 28px;'></a></div>
                            <div style='color: #8095A0; font-size: 12px; margin-bottom: 20px; padding: 0px 50px;'>You are receiving this email because you signed up for Classworks.xyz. All emails are sent from Classworks systems.</div>
                            <div style='margin-bottom: 20px;'><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/market-google-play.png' style='height: 33px;'></a><a href='#' style='display: inline-block; margin: 0px 10px;'><img alt='' src='http://www.classworks.xyz/images/market-ios.png' style='height: 33px;'></a></div>
                            <div style='margin-top: 20px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);'>
                               <div style='color: #8095A0; font-size: 10px; margin-bottom: 5px;'>No. 10 NTA/Choba Road, Ada Odum Complex, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</div>
                               <div style='color: #8095A0; font-size: 10px;'>Copyright <?php echo date('Y') ?> ClassWorks.xyz. All rights reserved.</div>
                            </div>
                         </div>
                      </div>
                   </body>
                </html>";
                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ClassWorks.xyz <hello@classworks.xyz>' . "\r\n";
                
                // Mail it
                
                if(@mail($email, $subject, $message, $headers)){
                    return true;
                }else{
                    return false;
                }
        }


        function recoverPassword($uniqueValue){
            
            $stmt2 = $this->dconn->prepare("SELECT * FROM staffs WHERE email = ? OR uniqueNumber = ?");
            $stmt2->bind_param("ss", $uniqueValue, $uniqueValue);
            $stmt2->execute();
            $stmt2_result = $this->get_result($stmt2);
            if(count($stmt2_result) == 1){

                $stmt = $this->dconn->prepare("SELECT * FROM staffs WHERE email = ? OR uniqueNumber = ?");
                $stmt->bind_param("ss", $uniqueValue, $uniqueValue);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $row = array_shift($stmt_result);
                    $randomVal = rand(1000000, 999999); //USE A RANDOM NUMBER
                    $newPassword = password_hash(strtolower($randomVal), PASSWORD_BCRYPT);
                    $stmt = $this->dconn->prepare("UPDATE staffs SET `password` = ? WHERE email = ? OR uniqueNumber = ?");
                    $stmt->bind_param("sss", $newPassword, $uniqueValue, $uniqueValue);
                    if($stmt->execute()){
                        $stmt->close();
                        $this->passwordEmail($row["email"], $row["fullname"], $randomVal);
                        return true; 
                    }else{
                        $stmt->close();
                        return false;
                    }

                }else{
                    $stmt->close();
                    return "email";
                }

            }else{

                $stmt = $this->dconn->prepare("SELECT * FROM partners WHERE email = ? OR partner_no = ?");
                $stmt->bind_param("ss", $uniqueValue, $uniqueValue);
                $stmt->execute();
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) > 0){
                    $stmt->close();
                    $row = array_shift($stmt_result);
                    $randomVal = rand(1000000, 999999); //USE A RANDOM NUMBER
                    $newPassword = password_hash(strtolower($randomVal), PASSWORD_BCRYPT);
                    $stmt = $this->dconn->prepare("UPDATE partners SET `password` = ? WHERE email = ? OR partner_no = ?");
                    $stmt->bind_param("sss", $newPassword, $uniqueValue, $uniqueValue);
                    if($stmt->execute()){
                        $stmt->close();
                        $this->passwordEmail($row["email"], $row["fullname"], $randomVal);
                        return true; 
                    }else{
                        $stmt->close();
                        return false;
                    }

                }else{
                    $stmt->close();
                    return "email";
                }

            }

        }

    }

?>