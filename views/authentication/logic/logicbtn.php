<?php
include_once ("logic.php");
$auth = new auth();

if(isset($_POST["register"])){
    $name = htmlentities(trim($_POST["name"]));
    $email = htmlentities(trim($_POST["email"]));
    $password = htmlentities(trim($_POST["password"]));
    $referralCode = htmlentities(trim($_POST["referralCode"]));

        $reply = $auth->register($name, $email, $password, $referralCode);
        if($reply === true){
            $mess = "An activation message has been sent to your mail, Click on the link to activate your account";
        }else{
            $err = "An error occurred while trying to send an activation link. Please try again later";
        }
}

if(isset($_POST["login"])){
    $uniqueValue = htmlentities(trim($_POST["unique"]));
    $password = htmlentities(trim($_POST["password"]));

    $reply = $auth->login($uniqueValue, $password);
    if(is_object($reply)){
        include_once ("session.php");
        switch($reply->role){
                case 'admin':
                case 'principal':
                case 'director':
                    $page = "../home";
                    break;
                case 'tutor':
                case 'form teacher':
                    $page = "../home/staff_dashboard";
                    break;
                case 'accountant':
                    $page = "../payments";
                    break;
                case 'store keeper':
                    $page = "../inventory";
                    break;
                case 'partner':
                    $page = "../partners/";
                    break;
                default:
                    $page = "../error";
            }
            echo "<script>window.location.assign('$page')</script>";
    }else{
        switch($reply){
            case 'password':
                $err = "Opps..Incorrect login details provided";
                break;
            case 'uniqueValue':
                $err = "Opps..Incorrect login details provided";
                break;
            default:
                $err = "An unexpected error occured. Please try again later";
                break;
        }
    }
}



if(isset($_POST["forgetBtn"])){
    $uniqueValue = htmlentities(trim($_POST["unique"]));
    
    $reply = $auth->recoverPassword($uniqueValue);
    if($reply == "true"){
        $mess = "A new password has been sent to your email address";
    }else{
        switch($reply){
            case 'email':
                $err = "Sorry this email/Reg No. is not recognized";
                break;
            default:
                $err = "An unexpected error occured. Please try again later";
                break;
        }
    }
}
?>