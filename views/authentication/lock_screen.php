<head>
    <title>ClassWorks - School Automation Solution | Lockscreen</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="../../addons/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="../../addons/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../../addons/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="../../addons/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../../addons/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="../../addons/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
  </head>
  <body oncontextmenu="return false" class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w wider centered">
        <div class="logo-w">
          <a href="index.html"><img alt="" src="../../images/logo-big.png"></a>
        </div>
        <h5 class="auth-header">
          Welcome Back
        </h5>
        <div class="logged-user-w">
          <div class="avatar-w">
            <img alt="" src="../../images/avatar.jpg">
          </div>
          <div class="logged-user-name">
            Maven Harry
          </div>
          <div class="logged-user-role">
            School Administrator
          </div>
        </div>
        <form action="">
          <div class="form-group">
            <label for="">Enter your password to access admin</label><input class="form-control" placeholder="Enter your password" type="password">
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
          </div>
          <div class="buttons-w">
            <a href="../index.php"><span class="my_hover_up btn btn-primary">Log me in</span></a>
            <a class="btn btn-link" href="./login.php">Login as different user</a>
          </div>
        </form>
      </div>
    </div>
     <!-------------------- START - Top Bar -------------------->
     <?php include("../../includes/support.php") ?>
     <!-------------------- END - Top Bar -------------------->

    <script src="../../js/app.js"></script>
  </body>
</html>
