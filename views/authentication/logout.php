<?php
    session_start();
    if($_SESSION["role"] == "partner"){
        session_destroy();
        echo "<script>window.location.assign('index')</script>";
    }else{
        $folderName = $_SESSION["folderName"];
        session_destroy();
        echo "<script>window.location.assign('../../".$folderName."')</script>";
    }
?>