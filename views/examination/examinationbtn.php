<?php
    include_once ("examinationClass.php");
    $examinationClass = new examinationClass;


    if(isset($_POST["addQuestion"])){
        $questionType = htmlentities(trim($_POST["questionType"]));
        $question = htmlentities(trim($_POST["question"]));
        $answer = htmlentities(trim($_POST["answer"]));
        $mark = htmlentities(trim($_POST["mark"]));
        $choice1 = htmlentities(trim($_POST["choice1"]));
        $choice2 = htmlentities(trim($_POST["choice2"]));
        $choice3 = htmlentities(trim($_POST["choice3"]));
        $choice4 = htmlentities(trim($_POST["choice4"]));
        $class = htmlentities(trim($_POST["qsClass"]));
        $subject = htmlentities(trim($_POST["qsSubject"]));
        $term = $examinationClass->getSchoolDetails()->academic_term;
        $session = $examinationClass->getSchoolDetails()->academic_session;        
    
        if(empty($questionType) && empty($question) && empty($answer) && empty($mark) && empty($class) && empty($subject)){
            $err = "Please some information are left blank";
        }else{
            $reply = $examinationClass->addQuestion($questionType, $question, $answer, $mark, $choice1, $choice2, $choice3, $choice4, $class, $subject, $session, $term);
            if($reply === true){
                $mess = "Question added successfully";
            }else{
                if($reply === "exist"){
                    $err = "Duplicate question cannot be added";
                }else{
                    $err = "Could not add question. Please try again later";
                }
            }
        }
    }


?>