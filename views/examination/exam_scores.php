<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Examination Scores</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $examinationClass->getSchoolDetails();
    $examClass = htmlentities(trim($_POST["examClass"]));
    $examinationSubject = htmlentities(trim($_POST["examinationSubject"]));
    $examinationTerm = htmlentities(trim($_POST["examinationTerm"]));
    $examinationSession = htmlentities(trim($_POST["examinationSession"]));
    $studentScores = $examinationClass->fetchStudentScores($examinationSession, $examinationTerm, $examClass, $examinationSubject);
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <?php
                if($_SESSION["role"] == "tutor"){ ?>
                    <!-------------------- START - Main Menu -------------------->
                    <?php include("../../includes/sidebar2.php") ?>
                    <!-------------------- END - Main Menu -------------------->
                <?php }else{ ?>
                    <!-------------------- START - Main Menu -------------------->
                    <?php include("../../includes/sidebar.php");
                        if(!in_array("Examination", $allowedPages)){
                            echo "<script>window.location.assign('../../error')</script>";
                        }
                    ?>
                    <!-------------------- END - Main Menu -------------------->
                <?php }
            ?>

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchStudentsExamModal" data-toggle="modal" style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a id="btnExport" onclick="exportTableToExcel('examResultTable', '<?php echo $examinationSession ?> Session <?php echo $examinationTerm ?> Term <?php echo strtoupper($examClass) ?> <?php echo strtoupper($examinationSubject) ?> CBT Result')" style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-book"></i>
                                        <span style="font-size: .9rem;">Export Result In Excel Sheet</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#FE650B; border:#FE650B" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <form>
                                
                                </form>
                                <h6 class="element-header"><span style="font-weight:bold"><?php echo strtoupper($examClass)." ".strtoupper($examinationSubject)?></span> EXAMINATION SCORE(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded" id="examResultTable">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Passport</th>
                                                <th>Name</th>
                                                <th>Class</th>
                                                <th>Subject</th>
                                                <th>Session</th>
                                                <th>Term</th>
                                                <th>Score</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(count($studentScores) == 0){
                                                        echo "No students found in record";
                                                    }else{
                                                        foreach($studentScores as $key => $value){ ?>
                                                            <tr class="my_hover_up">
                                                                <td class="nowrap">
                                                                    <span><?php echo $key + 1?> .</span>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                        if(empty($value->profilePhoto)){ ?>
                                                                            <a style="font-size:1rem; padding-top:10px;  border-radius:100px; width: 45px; height: 45px; text-decoration:none; background-color:#08ACF0; color:#FFFFFF"
                                                                                class="badge"><?php echo $nameInitials = strtoupper($examinationClass->createAcronym($value->fullname)); ?></a>
                                                                        <?php }else{ ?>
                                                                            <div class="user-with-avatar">
                                                                                <img style="border: 2px solid #8095A0" alt="" src="<?php echo $schoolDetails->pathToPassport."/".$value->profilePhoto;?>">
                                                                            </div>
                                                                        <?php }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->fullname)?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->presentClass); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ucwords($value->subjectName); ?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo ($examinationSession - 1)."/".$examinationSession?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $examinationTerm?></span>
                                                                </td>
                                                                <td>
                                                                    <span><?php echo $value->score?></span>
                                                                </td>
                                                            </tr>
                                                        <?php }
                                                    }
                                                ?>  
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <!-- <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div> -->

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

<script>
function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}

function sortTable(u) {
    var n = u.cellIndex
    var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("classSheetTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (Number(x.innerHTML) > Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (Number(x.innerHTML) < Number(y.innerHTML)) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

$("document").ready(function() {
    setTimeout(function() {
        $("#position").trigger('click');
    },10);
    
    $(window).on("load", function() {
        $("#loaderBg").css("display","none")
    });

});

</script>

</body>

</html>