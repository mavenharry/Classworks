<?php
include_once ("examinationbtn.php");

if(isset($_GET['getExamQuestions']) && $_GET["getExamQuestions"] == 1){
    $studentSubjectCode = htmlentities(trim($_GET["studentSubjectCode"]));
    $studentClassCode = htmlentities(trim($_GET["studentClassCode"]));
    $allQuestions = $examinationClass->fetchQuestions($studentClassCode, $studentSubjectCode, 0);
    echo $jsondata = json_encode($allQuestions);
    // echo $allQuestions;
}

?>