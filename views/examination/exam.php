<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>

    <title>ClassWorks - School Automation Solution | Online Examination System (OES)</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/exam.css" rel="stylesheet">

</head>
<?php
    $studentDetails = $examinationClass->getStudentExamDetails($_SESSION["examStudentNumber"]);
    
?>
<body onload="init()">

    <div id="wrapper" class="">

        <aside style="margin-top:0px;" class="right-sidebar" id="rightSidebar">

            <button class="sidebat-toggle" id="sidebarToggle"><i class="mdi mdi-menu"></i></button>

            <div class="panel panel-right-sidebar ">

                <div class="panel-heading">
                    <h2>Time Countdown</h2>
                </div>
                <div class="panel-body">

                    <div id="timerdiv" class="countdown-styled">
                        <span id="hours">00</span> :
                        <span id="mins">00</span> :
                        <span id="seconds">00</span>
                    </div>

                </div>
                <div class="panel-heading countdount-heading">
                    <h2>Total Time <span class="pull-right" id="totalTime">00:00:00</span></h2>
                </div>

                <div class="panel-body">
                    <div class="sub-heading">
                        <h3>Answer Tracker</h3>
                    </div>
                    <ul class="question-palette" id="pallete_list">
                    </ul>
                </div>
                <hr>
                <div class="panel-heading">
                    <h2>Tracker Keys</h2>
                </div>
                <div class="panel-body">
                    <ul class="legends">
                        <li class="palette answered"><span id="palette_total_answered"></span> Answered</li>
                        <li class="palette not-visited"><span id="palette_total_not_visited"></span> Not Answered</li>
                    </ul>
                </div>

            </div>

        </aside>

        <div id="page-wrapper" class="examform">

            <div class="container-fluid" style="margin-top:30px;">

                <!-- /.row -->

                <form method="POST" name="cbtExam" action="" accept-charset="UTF-8" id="onlineexamform">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="panel panel-custom">

                                <div class="panel-heading">

                                    <div class="pull-right">

                                        <h1 style="margin-right:20px;">
                                        <span class="text-uppercase"> <?php echo ucwords($studentDetails->fullname)." (".$studentDetails->studentClass.")"?></span>
                                        </h1>

                                    </div>

                                    <h1 style="color:#8095A0; margin-left:20px;">
                                    <span class="text-uppercase" style="color:#08ACF0;"><?php echo $studentDetails->subjectName; ?></span>: Question
                                    <span id="question_number_visible"></span> of <span id="totalQuestions"></span>
                                    </h1>

                                </div>

                                <div class="panel-body question-ans-box">

                                    <div id="questions_list">

                                        <div class="question_div" name="" id="" value="0">

                                            <div class="questions">
                                              <span class="" id="questionHere"></span>
                                                <div class="row">
                                                  <div class="col-md-8 text-center"></div>
                                                    <div class="col-md-4"><span class="pull-right"><span id="markHere"></span> Mark(s)</span></div>
                                              </div>
                                            </div>

                                            <hr>

                                            <div class="select-answer">
                                                <ul class="row list-style-none" id="answersHere"></ul>
                                            </div>

                                            </hr>

                                        </div>

                                    </div>

                                    <hr>

                                    <div class="row">

                                        <div class="col-md-12">

                                            <button class="btn btn-lg btn-success button prev" onclick="previousQuestion()" type="button">Previous Question</button>

                                            <button class="btn btn-lg btn-success button next" onclick="nextQuestion()" type="button">Next Question</button>

                                            <button class="btn btn-lg btn-dark button clear-answer" onclick="clearAnswer()" type="button">Clear Current Answer</button>

                                            <button class="btn btn-lg btn-danger button finish" onclick="endExam()" type="button">Submit Exam</button>

                                        </div>

                                    </div>

                                    </hr>

                                </div>

                            </div>

                        </div>

                </form>

                </div>

            </div>

        </div>

        <!-- /#page-wrapper -->

    </div>

    <?php //include_once ("../../includes/scripts.php"); ?>
    
    <script src="../../addons/jquery/dist/jquery.min.js"></script>
    <script src="../../addons/popper.min.js"></script>
    <script src="../../addons/moment/moment.js"></script>
    <script src="../../addons/chart.js/dist/Chart.min.js"></script>
    <script src="../../addons/select2/dist/js/select2.full.min.js"></script>
    <script src="../../addons/jquery.barrating.min.js"></script>
    <script src="../../addons/ckeditor/ckeditor.js"></script>
    <script src="../../addons/bootstrap-validator/dist/validator.min.js"></script>
    <script src="../../addons/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../../addons/ion.rangeSlider.min.js"></script>
    <script src="../../addons/dropzone/dist/dropzone.js"></script>
    <script src="../../addons/mindmup-editabletable.js"></script>
    <script src="../../addons/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../addons/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../../addons/fullcalendar/dist/fullcalendar.js"></script>
    <script src="../../addons/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../addons/tether/dist/js/tether.min.js"></script>
    <script src="../../addons/slick.min.js"></script>
    <script src="../../addons/bootstrap/js/dist/util.js"></script>
    <script src="../../addons/bootstrap/js/dist/alert.js"></script>
    <script src="../../addons/bootstrap/js/dist/button.js"></script>
    <script src="../../addons/bootstrap/js/dist/carousel.js"></script>
    <script src="../../addons/bootstrap/js/dist/collapse.js"></script>
    <script src="../../addons/bootstrap/js/dist/dropdown.js"></script>
    <script src="../../addons/bootstrap/js/dist/modal.js"></script>
    <script src="../../addons/bootstrap/js/dist/tab.js"></script>
    <script src="../../addons/bootstrap/js/dist/tooltip.js"></script>
    <script src="../../addons/bootstrap/js/dist/popover.js"></script>
    <script src="../../js/main.js"></script>
    <script src="../../addons/jquery-ui.min.js"></script>
    <script src="../../js/toastr.min.js"></script>
    <script src="../../js/jquery.timeAutocomplete.min.js"></script>
    <script src="https://github.com/kabachello/jQuery-Scanner-Detection"></script>
    <script src="../../js/app.js"></script>
    <script src="../../includes/nigeria_banks.json"></script>
    <script src="../../js/owl.carousel.js"></script>
    
    <?php include_once ("../../includes/modals/modalCaller.php"); ?>
    
    
    

    <!-- <script src="./js/mousetrap.js"></script> -->
    <!-- <script src="./js/exam.js"></script> -->
    <!-- <script src="./js/jquery.fullscreen-master/release/jquery.fullscreen.min.js"></script> -->

    <script type="text/javascript">

    var studentNumber = '<?php echo $_SESSION["examStudentNumber"]?>';
    var studentClassCode = '<?php echo $studentDetails->studentClassCode; ?>';
    var studentSubjectCode = '<?php echo $studentDetails->studentSubjectCode; ?>';

    function fetchData(){
        $.ajax({
            url: "getCBTData.php",
            method: "GET",
            data: {studentNumber: studentNumber, studentSubjectCode: studentSubjectCode, studentClassCode: studentClassCode, getExamQuestions : 1},
            cache: false,
            success: function(result){
                examEngine(result, 0)    
            },
            error : function (e) {
                toastr.error('Sorry an error occured!');
            }
        });
    }


    //EXAMINATION ENGINE

    var counter = setInterval("countdownTimer()", 1000)

    function examEngine(examData, id){
        // alert(examData.length);
        var jsonData = JSON.parse(examData)
        var question = jsonData[id].question
        var mark = jsonData[id].mark
        var totalQuestions = jsonData.length
        var examTime = jsonData[0].examTime
        count = examTime
        mapChoicesToViews(jsonData[id].choices, id)
        mapAnswerToTracker(jsonData)

        //Map to views
        $("#examData").val(examData)
        $("#question_number_visible").html(id+1)
        $("#question_number").val(id)
        $("#questionHere").html(question)
        $("#markHere").html(mark)
        $("#totalQuestions").html(totalQuestions)
        $("#totalTime").html(msToTime(examTime))
        counter
    }

    function nextQuestion(){
        //Get Exam data
        var questionNumber = $("#question_number").val()
        var examData = $("#examData").val()        
        var jsonData = JSON.parse(examData)
        var totalQuestions = jsonData.length
        if(Number(questionNumber) < Number(totalQuestions-1)){
            var rawData = $("#examData").val()
            var jsonData = JSON.parse(rawData)
            var questionNumber = $("#question_number").val()
            var questionNumberVisible = $("#question_number_visible").html()
            var question = jsonData[Number(questionNumber)+1].question
            var mark = jsonData[Number(questionNumber)+1].mark
            mapChoicesToViews(jsonData[Number(questionNumber)+1].choices, Number(questionNumber)+1)

            //Map to views
            $("#question_number").val(Number(Number(questionNumber)+1))
            $("#question_number_visible").html(Number(questionNumberVisible)+1)
            $("#questionHere").html(question)
            $("#markHere").html(mark)
        }
    }


    function previousQuestion(){
        //Get Exam data
        var questionNumber = $("#question_number").val()
        if(Number(questionNumber) > 0){
            var rawData = $("#examData").val()
            var jsonData = JSON.parse(rawData)
            var questionNumberVisible = $("#question_number_visible").html()
            var question = jsonData[Number(questionNumber)-1].question
            var mark = jsonData[Number(questionNumber)-1].mark
            mapChoicesToViews(jsonData[Number(questionNumber)-1].choices, Number(questionNumber)-1)

            //Map to views
            $("#question_number").val(Number(Number(questionNumber)-1))
            $("#question_number_visible").html(Number(questionNumberVisible)-1)
            $("#questionHere").html(question)
            $("#markHere").html(mark)
        }
    }


    function clearAnswer(){
        $('.checkbox').each(function () {
		$(this).removeAttr('checked');
		$('input[type="radio"]').prop('checked', false);
        })
        var questionNumber = $("#question_number").val()
        popQuestions(questionNumber)
        popAnswers()
    }


    function gotoQuestion(gotoNumber){
        //Get Exam data
        var rawData = $("#examData").val()
        var jsonData = JSON.parse(rawData)
        var questionNumber = gotoNumber
        var questionNumberVisible = $("#question_number_visible").html()
        var question = jsonData[Number(questionNumber)].question
        var mark = jsonData[Number(questionNumber)].mark
        mapChoicesToViews(jsonData[Number(questionNumber)].choices, gotoNumber)

        //Map to views
        $("#question_number").val(Number(Number(questionNumber)))
        $("#question_number_visible").html(Number(questionNumber)+1)
        $("#questionHere").html(question)
        $("#markHere").html(mark)
    }


    function mapChoicesToViews(choices, questionNumber){
        $("#answersHere").html("");
        for(var i = 0; i<choices.length; i++){
            var appendItem = '<li class="col-md-6" style="margin-bottom: 30px;"><label class="answer_label"><input id="choice'+i+'" type="radio" onclick="myAnswer('+"'"+i+"'"+')" name="pickedAnswer" class="option-input checkbox"> '+choices[i]+'</label></li>'
            $("#answersHere").append(appendItem);
        }
        //Remember Chosen Answer
        var objIndex = pickedAnswer.findIndex((obj => obj.question === questionNumber.toString()));
        if(objIndex > -1){
            var answerNumber = pickedAnswer[objIndex].answer
            $("#choice"+answerNumber).attr("checked", "checked")
        }
    }


    function mapAnswerToTracker(questions){
        $("#pallete_list").html("");
        for(var i = 0; i<questions.length; i++){
            var appendItem = '<li id="question'+i+'" onclick="gotoQuestion('+"'"+i+"'"+')" class="palette pallete-elements not-visited"><span>'+Number(i+1)+'</span></li>'
            $("#pallete_list").append(appendItem);
        }
    }


    var previousAnswerId = "";
    function myAnswer(answerid){
            var questionNumber = $("#question_number").val()
            pushQuestions(questionNumber)
            pushAnswers(answerid)
    }


    function countdownTimer(){
        if(count > 0){
            count = count - 1;

            var seconds = count % 60;
            var minutes = Math.floor(count / 60);
            var hours = Math.floor(minutes / 60);
            minutes %= 60;
            hours %= 60;

            $("#seconds").html(seconds == 0 ? "00" : seconds)
            $("#mins").html(minutes == 0 ? "00" : minutes)
            $("#hours").html(hours == 0 ? "00" : hours)

            if(count < 605){
                $("#timerdiv").css("color", "red")
                //speak("Please Hurry up. You have just 10 minutes left.");
            }
            
        }else{
            var activeQuest = $("#question_number").val()
            if(activeQuest > 1){
                endExam()
            }
            //Call End Exam Here
        }
    }


    function msToTime(duration) {
        var secondsTime = Number(duration)
        var seconds = secondsTime % 60;
        var minutes = Math.floor(secondsTime / 60);
        var hours = Math.floor(minutes / 60);
        minutes %= 60;
        hours %= 60;
        var retHours = hours == 0 ? "00" : hours
        var retMinutes = minutes == 0 ? "00" : minutes
        var retSeconds = seconds == 0 ? "00" : seconds
        return retHours + ":" + retMinutes + ":" + retSeconds;
    }


    $("body").keydown(function(e){
    if ((e.keyCode || e.which) == 37){   
        previousQuestion()
    }
    if ((e.keyCode || e.which) == 39){
        nextQuestion()
    }
    if ((e.keyCode || e.which) == 49){
        $("#choice0").click();
    }
    if ((e.keyCode || e.which) == 50){
        $("#choice1").click()
    }
    if ((e.keyCode || e.which) == 51){
        $("#choice2").click()
    }
    if ((e.keyCode || e.which) == 52){
        $("#choice3").click()
    }
    if ((e.keyCode || e.which) == 53){
        $("#choice4").click()
    }
    if ((e.keyCode || e.which) == 54){
        $("#choice5").click()
    }   
    });


    //Question Tracker Array
    var answeredQuestions = []
    
    function pushQuestions(questionNumber){
        var itemState = answeredQuestions.includes(questionNumber);
        if(itemState == false){
            answeredQuestions.push(questionNumber);
            $('#question'+questionNumber).removeClass("not-visited");
            $('#question'+questionNumber).addClass("answered");    
        }
    }


    function popQuestions(questionNumber){
        var itemState = answeredQuestions.includes(questionNumber);
        if(itemState == true){
            var index = answeredQuestions.indexOf(questionNumber);
            if (index > -1) {
                answeredQuestions.splice(index, 1);
            }
            $('#question'+questionNumber).removeClass("answered");
            $('#question'+questionNumber).addClass("not-visited");    
        }
    }


    //Answer Tracker Array
    var pickedAnswer = []

    function pushAnswers(answerNumber){
        var questionNumber = $("#question_number").val()
        var rawData = $("#examData").val()
        var jsonData = JSON.parse(rawData)
        var questionMark = jsonData[Number(questionNumber)].mark
        var userAnswer = jsonData[Number(questionNumber)].choices[answerNumber]
        var correctAnswer = jsonData[Number(questionNumber)].answer
        if(userAnswer.toLowerCase() === correctAnswer.toLowerCase()){
            var answerScore = Number(questionMark)
        }else{
            var answerScore = 0
        }
        var answerQuestionMapObject = {question:questionNumber, answer:answerNumber, score:answerScore}
        var answerState = pickedAnswer.filter(vendor => (vendor.question === questionNumber))
        if(answerState.length < 1){
            pickedAnswer.push(answerQuestionMapObject);   
        }else{
            var objIndex = pickedAnswer.findIndex((obj => obj.question === questionNumber.toString()));
            pickedAnswer[objIndex].answer = answerNumber
            pickedAnswer[objIndex].score = answerScore
        }
    }


    function popAnswers(){
        var questionNumber = $("#question_number").val()
        var objIndex = pickedAnswer.findIndex((obj => obj.question === questionNumber.toString()));
        if (objIndex > -1) {
            pickedAnswer.splice(objIndex, 1);
        }
    }


    function endExam(){
        console.log(count)
        var response = confirm("Are you sure you want to submit this exam? Note; this will end the exam completely.");
        if(response == true){
            clearInterval(counter)
            $("#seconds").html("00")
            $("#mins").html("00")
            $("#hours").html("00")

            //CALCULATE AND SUBMIT SCORE

            for(var key in pickedAnswer) {
                var answerObject = pickedAnswer[key];
                score = Number(score) + Number(answerObject.score)
            }
            // submit to DB
            $.ajax({
                url: "./ajaxHandler.php",
                method: "GET",
                data: {score: score, studentClassCode: studentClassCode, studentNumber: studentNumber, studentSubjectCode: studentSubjectCode},
                cache: false,
                success: function(results){
                    window.location.href="./examResult?score="+score+"&total=100"
                },
                error : function (e) {
                    toastr.error('Sorry an error occured!');  
                }
            });
        }

    }
    

    function init(){
        fetchData()
    }


    //Exam Total Time in seconds
    var count = 0;


    //Exam Total Time
    var score = 0;


    </script>


    <input type="hidden" id="examData" value=""/>
    <input type="hidden" id="question_number" value=""/>

</body>

</html>