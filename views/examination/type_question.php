<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Type Question</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php")?>
</head>
<?php
    $schoolDetails = $examinationClass->getSchoolDetails();
    $questionClass = htmlentities(trim($_GET["questionClass"]));
    $questionSubject = htmlentities(trim($_GET["questionSubject"]));
    $classCode = $examinationClass->getClassCode($questionClass);
    $subjectCode = $examinationClass->getSubjectCode($questionSubject, $questionClass);
    $allClasses = $examinationClass->getClasses();
    $recentQuestions = $examinationClass->fetchQuestions($classCode, $subjectCode, 10);
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php 
            if($_SESSION["role"] == "tutor"){
                include_once ("../../includes/sidebar2.php");
            }else{
                include_once ("../../includes/sidebar.php");
            }
                if(!in_array("Examination", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-w" style="margin-top:10px;">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <form method="GET" action="type_question">
                                    <div style="margin-top:-15px;" class="element-actions">
                                        <a style="" class="btn btn-sm btn-upper" href="#">
                                            <select id="questionClassSelector" name="questionClass" style="font-size:1rem; border-radius:5px; border: 2px solid #08ACF0; width:200px" class="form-control form-control-sm bright">
                                            <option value="">Select Class</option>
                                            <?php
                                                foreach ($allClasses as $key => $value) { ?>
                                                    <option value="<?php echo $value->class_name?>"><?php echo $value->class_name?></option>
                                                <?php }
                                            ?>
                                            </select>
                                        </a>

                                        <a style="" class="btn btn-sm btn-upper" href="#">
                                            <select id="questionSubjectSelector" name="questionSubject" style="margin-left:-20px; width:200px; font-size:1rem; border-radius:5px; border: 2px solid #08ACF0" class="form-control form-control-sm bright">
                                            <option value="">Select Subject</option>
                                                    
                                            </select>
                                        </a>
                                    </div>
                                    <input type="submit" hidden id="questionSubmitBtn" name="sdd">
                                </form>
                                <h6 class="element-header">EXAMINATION QUESTION ENTRY (TYPING)</h6>

                                
                                <div class="row" style="margin-top:20px;">

                                <div class="col-lg-6">
                    
                                    <div class="element-box">
                                    <form id="formValidate" method="POST">

                                    <input type="hidden" value="<?php echo $classCode?>" name="qsClass"/>
                                    <input type="hidden" value="<?php echo $subjectCode; ?>" name="qsSubject"/>

                                    <div class="form-group" style="margin-top:20px;">
                                    <label for=""> Question Type</label>
                                    <select required class="form-control" name="questionType">
                                    <option selected value="1">Multiple Choice</option>
                                    <option value="2">Fill The Blank</option>
                                    <option value="3">Write The Answer</option>
                                    </select>
                                    </div>

                                    <div class="form-group"><label> Question</label><textarea required name="question" class="form-control" rows="5" placeholder="A nuclear reaction where the nucleus of an atom splits into smaller parts is known as nuclear fission or nuclear fusion?"></textarea></div>

                                    <div class="form-group">
                                    <label for=""> Answer</label><input required name="answer" class="form-control" value="" placeholder="Hydrogen" type="text">
                                    </div>

                                    <div class="form-group">
                                    <label for=""> Mark</label><input required name="mark" class="form-control" value="" placeholder="5" type="number">
                                    </div>

                                    <div class="row" style="margin-top:30px">
                                    <div class="form-group col-sm-6">
                                        <label for="">Choice 1</label>
                                        <input required name="choice1" class="form-control" value="" placeholder="Argon" type="text">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="">Choice 2</label>
                                        <input required name="choice2" class="form-control" value="" placeholder="Alluminium" type="text">
                                    </div>
                                    </div>

                                    <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="">Choice 3</label>
                                        <input name="choice3" class="form-control" value="" placeholder="Sodium" type="text">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="">Choice 4</label>
                                        <input name="choice4" class="form-control" value="" placeholder="Silver" type="text">
                                    </div>
                                    </div>

                                    <div class="form-buttons-w" style="margin-top:30px;">
                                        <input class="my_hover_up btn btn-primary" type="submit" name="addQuestion" style="border-color:#08ACF0; background-color:#08ACF0" value="Add This Question"/>
                                    </div>
                                    </form>
                                    </div>
                                    
                                </div>

                                <div class="col-lg-6">

                                <div class="element-box">
                                <h5 class="form-header">
                                RECENT QUESTIONS ADDED
                                </h5>
                                <div class="form-desc">
                                View all recent questions added without refreshing or navigating out of this page. Track your progress on questions entry in real time. 
                                </div>

                                <!-- STUDENTS SLIP -->

                                <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Question</th>
                                                    <th>Answer</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php for($i = 0; $i < count($recentQuestions); $i++){ ?>
                                                <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i+1 ?>.</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo substr($recentQuestions[$i]->question, 0, 60) ?> ...</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $recentQuestions[$i]->answer ?></span>
                                                    </td>
                                                </tr>
                                                <?php } ?>

                                            </tbody>
                                        </table>

                                </div>
                                </div>

                                </div>

                                

                                </div>

                                
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>