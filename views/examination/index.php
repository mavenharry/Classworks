<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Online Examination System (OES)</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $examinationClass->getSchoolDetails();
    include ("../../includes/modals/index.php"); 
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Top Bar -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Examination", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Top Bar -------------------->

            <div class="content-w">

                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div class="content-panel-toggler" style="margin-top:10px;">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="element-wrapper">
                                    <div style="margin-top:-15px;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn btn-sm btn-upper" target="_blank" href="examLogin.php">
                                            <i class="os-icon os-icon-plus"></i>
                                            <span style="font-size: .9rem;">Start Examination</span>
                                        </a>
                                    </div>
                                    <h6 class="element-header">ONLINE EXAMINATION MANAGEMENT/ANALYSIS</h6>
                                    <div class="element-content" style="margin-top:30px;">
                                        <div class="col-sm-10 row">

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#subjectExamScoreModal" data-toggle="modal" href="" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Examination Scores/Records</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-tasks-checked"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Exam Scores</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" data-target="#subjectExamQuestionsModal" data-toggle="modal" href="" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Exam Questions and Answers</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-edit-1"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Question/Answer</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>

                                            
                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" href="./ongoing_exams" style="border: 0px dashed #08ACF0">
                                                    <div class="label">Currently Ongoing Examinations</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-calendar-time"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1 hideThis">Ongoing Exams</div>
                                                    <div style="display:none;" class="showThis">
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-ui-92"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0"> Click to view this</div>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" target="_blank" data-target="#setupNewExam" data-toggle="modal" href="">
                                                    <div class="label">Setup New Examination</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-clock"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Set New Exam</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" data-target="#newQuestionModal" data-toggle="modal"  href="">
                                                    <div class="label">Type Exam Question/Answer</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Type Question</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" href="allExams.php">
                                                    <div class="label">Activate/Deactivate Exams</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Activate Exams</div>
                                                </a>
                                            </div>


                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" target="_blank" href="./download.php">
                                                    <div class="label">Download/Print Excel Template</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Download Excel</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" target="_blank" href="">
                                                    <div class="label">Upload Question in Excel Format</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Upload Excel</div>
                                                </a>
                                            </div>

                                            <div class="col-sm-4 col-xxxl-3">
                                                <a class="element-box el-tablo" style="border: 2px dashed #08ACF0" target="_blank" href="examLogin.php">
                                                    <div class="label">Start Exam</div>
                                                    <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                                        <div class="icon-w">
                                                            <div class="os-icon os-icon-plus"></div>
                                                        </div>
                                                    </div>
                                                    <div class="value1" style="color:#08ACF0">Exam Login</div>
                                                </a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->

<?php include_once ("../../includes/scripts.php"); ?>
<script>
    var selectedTimetableClass = $("#timetableClassSelector").val();
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableClass: selectedTimetableClass},
        cache: false,
        success: function(results){
            $(".timeTableSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
</script>
</body>

</html>