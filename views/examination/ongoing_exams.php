<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Ongoing Examination</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $examinationClass->getSchoolDetails();
    $onGoingExams = $examinationClass->onGoingExams();
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Examination", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header">ALL ONGOING EXAMINATIONS</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Class</th>
                                                <th>Subject</th>
                                                <th>Questions</th>
                                                <th>Marks</th>
                                                <th>Duration</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if(count($onGoingExams) == 0){
                                                    echo "No ongoing exams";
                                                }else{
                                                    foreach($onGoingExams as $key => $value){ ?>
                                                        <tr class="my_hover_up">
                                                            <td class="nowrap">
                                                                <span><?php echo $key + 1?>.</span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->className); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo ucwords($value->subjectName); ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->questions; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->marks; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->duration; ?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->startTime?></span>
                                                            </td>
                                                            <td>
                                                                <span><?php echo $value->endTime; ?></span>
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                }
                                            ?>  
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class="element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>