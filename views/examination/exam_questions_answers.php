<?php include_once ("examinationbtn.php"); ?>
<!DOCTYPE html>
<html>

<head>
    <title>ClassWorks - School Automation Solution | Examination Questions & Answers</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
</head>
<?php
    $schoolDetails = $examinationClass->getSchoolDetails();
    $className = htmlentities(trim($_GET["eqc"]));
    $subjectName = htmlentities(trim($_GET["eqs"]));
    $classCode = $examinationClass->getClassCode($className);
    $subjectCode = $examinationClass->getSubjectCode($subjectName, $className);
    $allQuestions = $examinationClass->fetchQuestions($classCode, $subjectCode, 0);
?>
<body oncontextmenu="return false" class="menu-position-side menu-side-left full-screen with-content-panel">

    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">

            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php");
                if(!in_array("Examination", $allowedPages)){
                    echo "<script>window.location.assign('../../error')</script>";
                }
            ?>
            <!-------------------- END - Main Menu -------------------->

            <div class="content-w">
                <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar2.php") ?>
                <!-------------------- END - Top Bar -------------------->

                <div style="margin-top:10px;" class="content-w">
                    <div class="content-i">
                        <div class="content-box">
                            <!--START - Transactions Table-->
                            <div class="element-wrapper">
                                <div style="margin-top:-15px;" class="element-actions">
                                    <a data-target="#searchQuestionsAnswersModal" data-toggle="modal" style="color:#FFFFFF; background-color:#1BB4AD; border:#1BB4AD" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-filter"></i>
                                        <span style="font-size: .9rem;">Search/Filter</span>
                                    </a>
                                    <a style="color:#FFFFFF; background-color:#974A6D; border:#974A6D" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <i class="os-icon os-icon-printer"></i>
                                        <span style="font-size: .9rem;">Print This</span>
                                    </a>
                                </div>
                                <h6 class="element-header"><span style="font-weight:bold"><?php echo strtoupper($className)." ".strtoupper($subjectName);?></span> EXAMINATION QUESTION(S) & ANSWER(S)</h6>
                                <div class="element-box-tp">
                                    <div class="table-responsive">
                                        <table class="table table-padded">
                                            <thead>
                                                <tr>
                                                <th>S/N</th>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Mark(s)</th>
                                                <th>Session</th>
                                                <th>Term</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                    <?php for($i = 0; $i < count($allQuestions); $i++){ ?>
                                                    <tr class="my_hover_up">
                                                    <td class="nowrap">
                                                        <span><?php echo $i+1; ?>.</span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allQuestions[$i]->question ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allQuestions[$i]->answer ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allQuestions[$i]->mark ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allQuestions[$i]->session ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?php echo $allQuestions[$i]->term ?></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF" class="my_hover_up_bg badge" href="">Edit</a>
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="font-size:1rem; text-decoration:none; background-color:#c7d4da; color:#FFFFFF" class="my_hover_up_bg badge" href="">Delete</a>
                                                    </td>
                                                    </tr>
                                                    <?php } ?>
                                                
                                            </tbody>
                                        </table>

                                        <!-- LOAD MORE -->
                                        <div style="float: right;" class-"element-actions">
                                        <a style="color:#FFFFFF; background-color:#08ACF0; border:#08ACF0" class="my_hover_up btn-upper btn btn-sm" href="#">
                                        <span style="font-size: .9rem;">Load more</span>
                                        </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--END - Transactions Table-->


                            <!-------------------- START - Top Bar -------------------->
                            <?php include("../../includes/support.php") ?>
                            <!-------------------- END - Top Bar -------------------->


<!-- MODALS -->

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php") ?>
<!-------------------- END - MODAL -------------------->

<?php include_once ("../../includes/scripts.php"); ?>

</body>

</html>