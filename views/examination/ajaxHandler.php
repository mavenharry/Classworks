<?php include_once ("examinationbtn.php"); 

if(isset($_GET["getSubject"]) && $_GET["getSubject"] == 1){
    $className = $_GET["className"];
    $reply = $examinationClass->getClassSubject($className);
    if(count($reply) > 0){ 
        foreach($reply as $key => $value){ ?>
        <option value="<?php echo $value->subjectCode; ?>"><?php echo ucwords($value->subjectName) ?></option>
    <?php }
        }else{ ?>
            <option value="">No subject found in record</option>
        <?php }
}

if(isset($_GET["action"]) && $_GET["action"] == "updateExamState"){
    $value = htmlentities(trim($_GET["value"]));
    $subject = htmlentities(trim($_GET["subject"]));
    $reply = $examinationClass->updateExamState($subject, $value);
    echo $reply;
}

if(isset($_GET["examClass"])){
    $examClass = htmlentities(trim($_GET["examClass"]));
    $reply = $examinationClass->getClassSubject($examClass);
    if(count($reply) == 0){ ?>
        <option value="">No subject for this class</option>
    <?php }else{ ?>
        <option value="">Select Subject</option>
        <?php foreach($reply as $key => $value){ ?>
            <option value="<?php echo $value->subjectName;?>"><?php echo ucwords($value->subjectName); ?></option>
        <?php }
    }
}

if(isset($_GET["startExam"]) && $_GET["startExam"] == 1){
    unset($_SESSION["examStudentNumber"]);
    $studentNumber = htmlentities(trim($_GET["number"]));
    $reply = $examinationClass->getStudentExamDetails($studentNumber);
    if($reply->status == "true"){
        $_SESSION["examStudentNumber"] = $studentNumber;
        echo $reply->status;
    }else{
        echo $reply->status;
    }
}

if(isset($_GET["score"])){
    $reply = $examinationClass->submitScore($_GET["score"], $_GET["studentClassCode"], $_GET["studentSubjectCode"], $_GET["studentNumber"]);
    if($reply == "true"){
        echo $reply;
    }else{
        echo $reply;
    }
}

?>