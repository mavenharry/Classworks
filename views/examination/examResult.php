<?php
include_once ("examinationbtn.php");

if(!isset($_GET["score"])){
    header("location: ../../");
}

$studentDetails = $examinationClass->getStudentExamDetails($_SESSION["examStudentNumber"]);

?>

<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>

    <title>ClassWorks - School Automation Solution | Online Examination System</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->

    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/exam.css" rel="stylesheet">

     <link href="./css/examResult.css" rel="stylesheet">

</head>

<body style="overflow: hidden;">

    <div id="wrapper" class="no-right-sidebar mt-150 ">

        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="panel panel-custom">

                    <div class="panel-heading">

                    <h1><?php echo ucwords($studentDetails->fullname)." (".$studentDetails->subjectName.")"?></h1></div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">

                                <ul class="library-statistic mt-40">

                                    <li class="total-books">

                                        Score <span><?php echo $_GET["score"]?></span>

                                    </li>

                                    <li class="total-books">

                                        Total <span><?php echo $_GET["total"]?></span>

                                    </li>

                                    <li class="total-books">

                                        Percentage <span><?php echo round(($_GET["score"]/$_GET["total"])*100, 1); ?>%</span>

                                    </li>

                                </ul>
                            </div>
                        </div>

                        <br/>

                        <div class="row">

                            <div class="col-lg-12 text-center" style="margin-top: 30px;">

                                <a href="./examLogin" class="btn t btn-primary">EXAM SUBMITTED, GO BACK</a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- /.container-fluid -->

        </div>

        <!-- /#page-wrapper -->

    </div>

    <!-- /#wrapper -->

    </div>



    <?php include_once ("../../includes/scripts.php"); ?>

    <!-- <script src="./js/mousetrap.js"></script> -->
    <!-- <script src="./js/exam.js"></script> -->
    <!-- <script src="./js/jquery.fullscreen-master/release/jquery.fullscreen.min.js"></script> -->

    </body>

    </html>