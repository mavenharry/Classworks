<!DOCTYPE html>

<html lang="en" dir="ltr">

<head>

	<title>ClassWorks - School Automation Solution | CBT</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->

       <link href="./css/bootstrap.min.css" rel="stylesheet">
	   <link href="./css/exam.css" rel="stylesheet">
	 
</head>

<body>

	<div id="wrapper" class="no-right-sidebar">

		<!-- Navigation -->

		<nav role="navigation">
			
		

		</nav>

		 

		
		
		

<div id="page-wrapper">
			<div class="container-fluid">
				<!-- /.row -->
                <br><br>
	<div class="panel panel-custom col-lg-12" >
					<div class="panel-heading">
						<h1>Instructions for Mathematics <span class="pull-right text-italic">Please Read The Instructions Carefully</span></h1>

					</div>
					<div class="panel-body instruction no-arrow">

						<div class="row">
							<div class="col-md-12">
								<h2>Exam Name:   Fluid Mechanics </h2>
																<h3>General Instructions:</h3>
																 
								<p><strong>Please read these instructions carefully.</strong> A&nbsp;candidate who breaches any of the Examination Regulations will be liable to disciplinary action including (but not limited to) suspension or expulsion from the University.</p>

<p>&nbsp;</p>

<p><strong>Timings</strong></p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Examinations will be conducted during the allocated times shown in the examination timetable.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The examination hall will be open for admission <strong>10 minutes </strong>before the time scheduled for the commencement of the examination. You are to find your allocated seat but&nbsp;do<strong>&nbsp;NOT </strong>turn over the question paper until instructed at the time of commencement of the examination.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You will not be admitted for the examination after <strong>one hour</strong> of the commencement of the examination.</p>

<p><strong>Personal Belongings</strong></p>

<ul>
	<li>All your personal belongings (such as bags, pouches, ear/headphones, laptops etc.) must be placed at the designated area outside the examination hall. Please do not bring any valuable belongings except the essential materials required for the examinations.&nbsp;
	<ul>
		<li>Any unauthorised materials, such as books, paper, documents, pictures and electronic devices with communication and/or storage capabilities such as tablet PC, laptop, smart watch, portable audio/video/gaming devices etc. are not to be brought into the examination hall.</li>
	</ul>
	</li>
</ul>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Handphones brought into the examination hall must be switched off at ALL times.</strong> If your handphone is found to be switched on in the examination hall, the handphone will be confiscated and retained for investigation of possible violation of regulations.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Photography is&nbsp;<strong>NOT</strong> allowed in the examination hall at&nbsp;<strong>ALL </strong>times.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; All materials and/or devices which are found in violation of any examination regulations will be confiscated.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The University will not be responsible for the loss or damage of any belongings in or outside the examination hall.</p>

<p><strong>At the Start of the Examination</strong></p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Do&nbsp;NOT </strong>turn over the question paper placed on your desk until instructed to do so at the time of commencement of the examination.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please place your identification documents (such as matric card, identity card, passport, driving licence or EZ-Link concession card) at the top right corner of your examination desk for the marking of attendance and verification of identity during the examination.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check that you have the correct question paper and read the instructions printed on your examination question paper carefully.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; All examinations are anonymous. Therefore, do not write your name on the answer book. You should write only your matriculation number, correctly and legibly, in the space provided on the cover of each answer book. Providing incorrect/illegible matriculation number could risk your answer book being considered void.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p><strong>During Examination</strong></p>

<ul>
	<li>You are not allowed to communicate by word of mouth or otherwise with other candidates (this includes the time when answer scripts are being collected).</li>
	<li>Please raise your hand if you wish to communicate with an invigilator.</li>
</ul>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unless granted permission by an invigilator, you are not allowed to leave your seat.</p>

<ul>
	<li>Once you have entered the examination hall, you will not be allowed to leave the hall&nbsp;until<strong> one hour</strong><br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; after the examination has commenced.&nbsp;</li>
	<li>If, for any reason, you are given permission to leave the hall temporarily, you must be accompanied<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;by an invigilator throughout your absence from the examination hall. <strong>You are required to leave<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; your&nbsp;</strong><strong>handphone on your desk when you leave the hall temporarily.</strong></li>
</ul>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; All answers, with the exception of graphs, sketches, diagrams, etc. should be written in black or blue pen, unless otherwise specified. Answers written in pencil will not be marked. The blank pages in the answer book are to be used only for candidates&#39; rough work. Solutions or any other materials written on these blank pages will not be marked.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Do not write on, mark, highlight or deface any reference materials provided for the examination. If found doing so, the reference materials will be removed from your use for the rest of the examination and you will be made to pay for the cost of the materials that have to be replaced.</p>

<p><strong>At the End of the Examination</strong></p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You are&nbsp;<strong>NOT</strong> allowed to leave the examination hall during the last <strong>15 minutes</strong> of the examination and during the collection of the answer scripts. All candidates must remain seated throughout this period for invigilators to properly account for all answer scripts to be collected.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do <strong>NOT </strong>continue to write after the examination has ended. You are to <strong>remain seated quietly</strong>&nbsp;while your answer scripts are being collected and counted.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No papers, used or unused, may be removed from the examination hall. You may take your own question paper with you unless otherwise instructed.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You are to stay in the examination hall until the Chief Invigilator has given the permission to leave. Do <strong>NOT</strong> talk until you are outside of the examination hall.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You are responsible to ensure that your answer scripts are submitted at the end of the examination. If you are present for the examination and do not submit your answer script, you will be deemed to have sat for and failed the examination concerned. Any unauthorised removal of answer script or part of answer script from the examination hall would deem the answer script as null and void.</p>

<p>&middot;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Once dismissed, you should leave the examination hall quickly and quietly.&nbsp;Remember to take your personal belongings with you.&nbsp;</p>

								
								<ul class="guide">
									<li>
										<span class="answer"><i class="mdi mdi-check"></i></span> You have answered the question.
									</li>
									<li>
										<span class="notanswer"><i class="mdi mdi-close"></i></span> You have not answered the question.
									</li>
									<li>
										<span class="marked"><i class="mdi mdi-eye"></i></span> You have answered the question but have marked the question for review.
									</li>
									<li>
										<span class="notvisited"><i class="mdi mdi-eye-off"></i></span> You have not visited the question yet.
									</li>
								</ul>

							</div>

						</div>


						<hr>
												<div class="form-group row">
						<form method="POST" action="" accept-charset="UTF-8"><input name="_token" type="hidden" value="">
							<div class="col-md-12">
								
								<input type="checkbox" name="option" id="free" checked="" >
								<label for="free" > <span class="fa-stack checkbox-button"> <i class="mdi mdi-check active"></i> </span> The computer provided to me is in proper working condition. I have read and understood the instructions given above. </label>
								
								<br><span class="text-danger">Please Accept Terms And Conditions</span> 

																<div class="text-center">
									
									<button class="btn button btn-lg btn-success">Start Exam</button>
																	</div>

							</div>
					</form>

						</div>


					</div>
				</div>
			</div>

		</div>

	</div>

    <?php include_once ("../../includes/scripts.php"); ?>

	<script src="./js/mousetrap.js"></script>
    <script src="./js/exam.js"></script>

</body>

</html>