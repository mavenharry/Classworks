<?php
    session_start();
    include_once ("../../".$_SESSION['folderName']."/config.php");

    class examinationClass extends logic {

        function addQuestion($questionType, $question, $answer, $mark, $choice1, $choice2, $choice3, $choice4, $class, $subject, $session, $term){
            $stmt = $this->uconn->prepare("SELECT id FROM examination_question WHERE question = ? AND class = ? AND `subject` = ? AND `session` = ? AND `term` = ?");
            $stmt->bind_param("ssssi", $question, $class, $subject, $session, $term);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $stmt = $this->uconn->prepare("INSERT INTO examination_question (`type`, question, answer, mark, choice1, choice2, choice3, choice4, class, `subject`, `session`, term) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("issssssssssi", $questionType, $question, $answer, $mark, $choice1, $choice2, $choice3, $choice4, $class, $subject, $session, $term);
                $stmt->execute();
            }
            $stmt->close();
            return true;
        }


        function fetchQuestions($class, $subject, $limit){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            if($limit == 0){
                $stmt = $this->uconn->prepare("SELECT examination_question.*, cbt_subjects.duration FROM examination_question INNER JOIN cbt_subjects ON cbt_subjects.classCode = ? AND cbt_subjects.subjectCode = ? AND cbt_subjects.term = ? AND cbt_subjects.session = ? WHERE examination_question.class = ? AND examination_question.subject = ? AND examination_question.session = ? AND examination_question.term = ? ORDER BY id DESC");
            }else{
                $stmt = $this->uconn->prepare("SELECT examination_question.*, cbt_subjects.duration FROM examination_question INNER JOIN cbt_subjects ON cbt_subjects.classCode = ? AND cbt_subjects.subjectCode = ? AND cbt_subjects.term = ? AND cbt_subjects.session = ? WHERE examination_question.class = ? AND examination_question.subject = ? AND examination_question.session = ? AND examination_question.term = ? ORDER BY id DESC LIMIT $limit");
            }
            $stmt->bind_param("ssissssi", $class, $subject, $academicTerm, $academicSession, $class, $subject, $academicSession, $academicTerm);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $questions = array();
            while($row = array_shift($stmt_result)){
                $obj = new stdClass();
                $choices = array();
                $obj->questionType = $this->outputRefine($row["type"]);
                $obj->question = $this->outputRefine($row["question"]);
                $obj->answer = $this->outputRefine($row["answer"]);
                $obj->mark = $this->outputRefine($row["mark"]);
                $obj->choice1 = $this->outputRefine($row["choice1"]);
                $obj->choice2 = $this->outputRefine($row["choice2"]);
                $obj->choice3 = $this->outputRefine($row["choice3"]);
                $obj->choice4 = $this->outputRefine($row["choice4"]);
                $obj->examTime = $this->outputRefine($row["duration"]);
                array_push($choices, $row["choice1"], $row["choice2"], $row["choice3"], $row["choice4"]);
                $cleanedArray = array_filter($choices);
                $obj->choices = $cleanedArray;
                $obj->class = $this->outputRefine($row["class"]);
                $obj->subject = $this->outputRefine($row["subject"]);
                $obj->session = $this->outputRefine($row["session"]);
                $obj->term = $this->outputRefine($row["term"]);
                $questions[] = $obj;
            }
            $stmt->close();
            shuffle($questions);
            return $questions;
        }

        function onGoingExams(){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            $state = 1;
            $stmt = $this->uconn->prepare("SELECT cbt_subjects.startTime, cbt_subjects.endTime, classes.class_name, subjects.subjectName, COUNT(examination_question.id) as questions, SUM(examination_question.mark) AS marks, cbt_subjects.duration FROM cbt_subjects INNER JOIN classes ON cbt_subjects.classCode = classes.class_code INNER JOIN subjects on cbt_subjects.subjectCode = subjects.subjectCode LEFT JOIN examination_question ON cbt_subjects.classCode = examination_question.class AND examination_question.subject = cbt_subjects.subjectCode AND examination_question.session = ? AND examination_question.term = ? WHERE cbt_subjects.term = ? AND cbt_subjects.session = ? AND cbt_subjects.examState = ?");
            $stmt->bind_param("siisi", $academicSession, $academicTerm, $academicTerm, $academicSession, $state);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $exams = array();
            if(count($stmt_result) > 0){
                while($row = array_shift($stmt_result)){
                    if(!empty($row["class_name"])){
                        $obj = new stdClass();
                        $obj->className = $row["class_name"];
                        $obj->subjectName = $row["subjectName"];
                        $obj->questions = $row["questions"];
                        $obj->marks = (int)$row["marks"];
                        $obj->startTime = $row["startTime"];
                        $obj->endTime = $row["endTime"];
                        $obj->duration = date('H:i', mktime(0,0,$row["duration"]));
                        $exams[] = $obj;
                    }
                }
                $stmt->close();
                return $exams;
            }else{
                $stmt->close();
                return $exams;
            }
        }

        function allExams(){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            // $stmt = $this->uconn->prepare("SELECT cbt_subjects.id, cbt_subjects.startTime, cbt_subjects.examState, cbt_subjects.endTime, classes.class_name, subjects.subjectName, COUNT(examination_question.id) as questions, SUM(examination_question.mark) AS marks, cbt_subjects.duration FROM cbt_subjects INNER JOIN classes ON cbt_subjects.classCode = classes.class_code INNER JOIN subjects on cbt_subjects.subjectCode = subjects.subjectCode LEFT JOIN examination_question ON cbt_subjects.classCode = examination_question.class AND examination_question.subject = cbt_subjects.subjectCode AND examination_question.session = ? AND examination_question.term = ? WHERE cbt_subjects.term = ? AND cbt_subjects.session = ?");
            // $stmt->bind_param("siis", $academicSession, $academicTerm, $academicTerm, $academicSession);
            // $stmt->execute();

            $stmt = $this->uconn->prepare("SELECT * FROM cbt_subjects");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $exams = array();
            while($row = array_shift($stmt_result)){
                $classCode = $row["classCode"];
                $subjectCode = $row["subjectCode"];
                $className = $this->getClassName($classCode);
                $subjectName = $this->getSubjectName($subjectCode);
                $stmt2 = $this->uconn->prepare("SELECT COUNT(examination_question.id) AS totalQuestion, SUM(examination_question.mark) AS totalSum FROM examination_question WHERE class = ? AND subject = ? AND session = ? AND term = ?");
                $stmt2->bind_param("sssi", $classCode, $subjectCode, $academicSession, $academicTerm);
                $stmt2->execute();
                $stmt_result2 = $this->get_result($stmt2);
                if(count($stmt_result2) > 0){
                    while($row2 = array_shift($stmt_result2)){
                        $obj = new stdClass();
                        $obj->id = $row["id"];
                        $obj->className = $className;
                        $obj->subjectName = $subjectName;
                        $obj->questions = $row2["totalQuestion"];
                        $obj->marks = (int)$row2["totalSum"];
                        $obj->startTime = $row["startTime"];
                        $obj->endTime = $row["endTime"];
                        $obj->examState = $row["examState"];
                        $obj->duration = date('H:i', mktime(0,0,$row["duration"]));
                        $exams[] = $obj;
                    }
                }else{
                    while($row2 = array_shift($stmt_result)){
                        $obj = new stdClass();
                        $obj->id = $row["id"];
                        $obj->className = $className;
                        $obj->subjectName = $subjectName;
                        $obj->questions = 0;
                        $obj->marks = 0;
                        $obj->startTime = $row["startTime"];
                        $obj->endTime = $row["endTime"];
                        $obj->examState = $row["examState"];
                        $obj->duration = date('H:i', mktime(0,0,$row["duration"]));
                        $exams[] = $obj;
                    }
                }
            }
            $stmt->close();
            return $exams;


            // $stmt_result = $this->get_result($stmt);
            // $exams = array();
            // if(count($stmt_result) > 0){
            //     while($row = array_shift($stmt_result)){
            //         if(!empty($row["class_name"])){
            //             $obj = new stdClass();
            //             $obj->id = $row["id"];
            //             $obj->className = $row["class_name"];
            //             $obj->subjectName = $row["subjectName"];
            //             $obj->questions = $row["questions"];
            //             $obj->marks = (int)$row["marks"];
            //             $obj->startTime = $row["startTime"];
            //             $obj->endTime = $row["endTime"];
            //             $obj->examState = $row["examState"];
            //             $obj->duration = date('H:i', mktime(0,0,$row["duration"]));
            //             $exams[] = $obj;
            //         }
            //     }
            //     $stmt->close();
            //     return $exams;
            // }else{
            //     $stmt->close();
            //     return $exams;
            // }
        }

        function updateExamState($subject, $value){
            $stmt = $this->uconn->prepare("SELECT classCode FROM cbt_subjects WHERE id = ?");
            $stmt->bind_param("i", $subject);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $classCode = array_shift($stmt_result)["classCode"];
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;
            $oldState = 0;
            $stmt = $this->uconn->prepare("UPDATE cbt_subjects SET examState = ? WHERE classCode = ? AND term = ? AND session = ?");
            $stmt->bind_param("isis", $oldState, $classCode, $academicTerm, $academicSession);
            $stmt->execute();
            $stmt = $this->uconn->prepare("UPDATE cbt_subjects SET examState = ? WHERE id = ?");
            $stmt->bind_param("ii", $value, $subject);
            if($stmt->execute()){
                $stmt->close();
                return "true";
            }else{
                $stmt->close();
                return "false";
            }
        }

        function fetchStudentScores($examinationSession, $examinationTerm, $examClass, $examinationSubject){
            $subjectCode = $this->getSubjectCode($examinationSubject, $examClass);
            $classCode = $this->getClassCode($examClass);
            $studentScores = array();
            $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, students.profilePhoto, subjects.subjectName, exam_scores.score FROM exam_scores INNER JOIN students ON students.student_number = exam_scores.student_number INNER JOIN subjects ON subjects.subjectCode = exam_scores.subjectCode WHERE exam_scores.term = ? AND exam_scores.session = ? AND exam_scores.subjectCode = ? AND exam_scores.classCode = ?");
            $stmt->bind_param("isss", $examinationTerm, $examinationSession, $subjectCode, $classCode);
            if($stmt->execute()){
                $stmt_result = $this->get_result($stmt);
                if(count($stmt_result) == 0){
                    $stmt->close();
                    return $studentScores;
                }else{
                    while($row = array_shift($stmt_result)){
                        $obj = new stdClass();
                        $obj->fullname = $row["fullname"];
                        $obj->presentClass = $row["present_class"];
                        $obj->profilePhoto = $row["profilePhoto"];
                        $obj->subjectName = $row["subjectName"];
                        $obj->score = $row["score"];
                        $studentScores[] = $obj;
                    }
                    $stmt->close();
                    return $studentScores;
                }
            }else{
                $stmt->close();
                return $studentScores;
            }
        }

        function getStudentExamDetails($studentNumber){

            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;

            $examState = 1;
            $stmt = $this->uconn->prepare("SELECT students.fullname, students.present_class, cbt_subjects.examState, classes.class_code, subjects.subjectName, subjects.subjectCode FROM students INNER JOIN classes on classes.class_name = students.present_class INNER JOIN cbt_subjects ON cbt_subjects.classCode = classes.class_code AND cbt_subjects.examState = ? INNER JOIN subjects ON subjects.subjectCode = cbt_subjects.subjectCode WHERE students.student_number = ?");
            $stmt->bind_param("is", $examState, $studentNumber);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $row = array_shift($stmt_result);
                $stmt->close();
                $obj = new stdClass();
                $obj->studentNumber = base64_encode($studentNumber);
                $obj->studentClass = $row["present_class"];
                $obj->fullname = $row["fullname"];
                $obj->subjectName = $row["subjectName"];
                $obj->studentClassCode = $row["class_code"];
                $obj->studentSubjectCode = $row["subjectCode"];

                $stmt3 = $this->uconn->prepare("SELECT * FROM exam_scores WHERE student_number = ? AND subjectCode = ? AND classCode = ? AND `session` = ? AND `term` = ?");
                $stmt3->bind_param("sssss", $studentNumber, $row["subjectCode"], $row["class_code"], $academicSession, $academicTerm);
                $stmt3->execute();
                $stmt3_result = $this->get_result($stmt3);
                if(count($stmt3_result) > 0){
                    $obj->status = "written";
                    return $obj;
                }else{
                    $obj->status = "true";
                    return $obj;
                }

            }else{
                $stmt->close();
                $obj = new stdClass();
                $obj->status = "false";
                return $obj;
            }
        }


        function submitScore($score, $studentClassCode, $studentSubjectCode, $studentNumber){
            $academicTerm = $this->getSchoolDetails()->academic_term;
            $academicSession = $this->getSchoolDetails()->academic_session;

            $stmt = $this->uconn->prepare("INSERT INTO exam_scores (`score`, student_number, subjectCode, classCode, `session`, term) VALUES (?,?,?,?,?,?)");
            $stmt->bind_param("isssss",  $score, $studentNumber, $studentSubjectCode, $studentClassCode, $academicSession, $academicTerm);
            if($stmt->execute()){
                return "true";
            }else{
                return "false";
            }
        }

    }
?>