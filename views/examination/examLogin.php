<?php include_once ("examinationbtn.php");

unset($_SESSION["examStudentNumber"]);

$schoolDetails = $examinationClass->getSchoolDetails();
$classes = $examinationClass->getClasses();
?>

<!DOCTYPE html>
<html>
<head>
<title><?php echo ucwords($schoolDetails->school_name); ?> - Online Result Checker</title>
<!-------------------- START - Meta -------------------->
<?php include("../../includes/meta.php") ?>
<!-------------------- END - Meta -------------------->
<link href="../assessment/resultChecker/css/index.css" rel="stylesheet">
<link href="../../css/main.css" rel="stylesheet">
<?php include_once ("../../includes/styles.php"); ?>
</head>

<body>
        <div class="categories">

        <div class="category-block">
          <div class="category-icon-cont">

            <div class="icon"><img style="margin-top: 50px; margin-bottom: 20px; width: 140px; object-fit: scale-down;" src="../../<?php echo $schoolDetails->schoolFolder?>/<?php echo $schoolDetails->logo;?>"></div>

            <h2 style="color: #09acef; font-size: 1.8rem; margin-bottom:0px; margin-top:0px;" ><?php echo ucwords($schoolDetails->school_name); ?></h2>
            <h2 style="color: #b7b7b7; font-weight:normal; font-style: italic; font-size: 1rem;" ><?php echo ucwords($schoolDetails->city.", ".$schoolDetails->state.", ".$schoolDetails->country); ?></h2>

            <h2 style="color: #8095A0; font-size: 1.5rem; margin-top:10px; margin-bottom:10px;" >Computer Based Test</h2>            
           
        <form method="post">

          <div style="margin-top: 0px;">
          <input style="text-transform:uppercase; font-size:1.05rem;" class="pin" type="text" id="examNumber" name="examNumber" placeholder="Enter Your Student Number">         
          </div>
          
          <button class="button" type="button" id="proceedExam"><span> Proceed To Examination </span></button>
         </form>

          </div>
        </div>

</body>
<?php include_once ("../../includes/scripts.php"); ?>
</html>