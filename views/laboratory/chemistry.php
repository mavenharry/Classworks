<?php include_once ("laboratorybtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Laboratory</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
$schoolDetails = $laboratoryClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->
                    <div class="col-sm-12 row">

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=acidbase" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Acid Base Solution</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">How do strong and weak acids differ? Use lab tools on your computer to find out! Dip the paper or the probe into solution to measure the pH, or put in the electrodes to measure the conductivity. Then see how concentration and strength affect pH. Can a weak acid solution have the same pH as a strong acid solution?</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=atomic" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Atomic Interaction</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Explore the interactions between various combinations of two atoms. Observe the the total force acting on the atoms or the individual attractive and repulsive forces. Customize the attraction to see how changing the atomic diameter and interaction strength affects the interaction.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=balanceEqn" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Balance an Equation</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">How do you know if a chemical equation is balanced? What can you change to balance an equation? Play a game to test your ideas!</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=baloonAndStaticElectricity" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Balloons and Static Electricity</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Grab a balloon to explore concepts of static electricity such as charge transfer, attraction, repulsion, and induced charge.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=buildAnAtom" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Build a Chemical Atom</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Build an atom out of protons, neutrons, and electrons, and see how the element, charge, and mass change. Then play a game to test your ideas!</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=concentration" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Chemical Concentration</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Watch your solution change color as you mix chemicals with water. Then check molarity with the concentration meter. What are all the ways you can change the concentration of your solution? Switch solutes to compare different chemicals and find out how concentrated you can go before you hit saturation!</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=beerLaw" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Beer's Chemical Law</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">“The thicker the glass, the darker the brew, the less the light that passes through.” Make colorful concentrated and dilute solutions and explore how much light they absorb and transmit using a virtual spectrophotometer!</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=molarity" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2.1em;" class="value1 hideThis">Solution Molarity</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">What determines the concentration of a solution? Learn about the relationships between moles, liters, and molarity by adjusting the amount of solute and solution volume. Change solutes to compare different chemical compounds in water.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=moleculePolarity" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2.1em;" class="value1 hideThis">Molecule Polarity</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">When is a molecule polar? Change the electronegativity of atoms in a molecule to see how it affects polarity. See how the molecule behaves in an electric field. Change the bond angle to see how shape affects polarity.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=moleculeAndLight" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Molecules and Light</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Adjust light source slider and begin your observations of how different molecules react to different light sources. Note that the interactive elements in this sim have simple description that can be accessed using a screen reader.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=molecularShapes" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2.1em;" class="value1 hideThis">Molecular Shapes</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Explore molecule shapes by building molecules in 3D! How does molecule shape change with different numbers of bonds and electron pairs? Find out by adding single, double or triple bonds and lone pairs to the central atom. Then, compare the model to real molecules!</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=phScale" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Acid/Base PH Scale</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Test the pH of things like coffee, spit, and soap to determine whether each is acidic, basic, or neutral. Visualize the relative number of hydroxide ions and hydronium ions in solution. Switch between logarithmic and linear scales. Investigate whether changing the volume or diluting with water affects the pH. Or you can design your own liquid!</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=reactant" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Reactants, Products and Leftovers</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Create your own sandwich and then see how many sandwiches you can make with different amounts of ingredients. Do the same with chemical reactions. See how many products you can make with different amounts of reactants. Play a game to test your understanding of reactants, products and leftovers. Can you get a perfect score on each level?</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=rutherfordScatter" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Rutherford Scattering</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">How did Rutherford figure out the structure of the atom without being able to see it? Simulate the famous experiment in which he disproved the Plum Pudding model of the atom by observing alpha particles bouncing off atoms and determining that they must have a small core.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=stateofMatter" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2.1em;" class="value1 hideThis">States of a Matter</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Watch different types of molecules form a solid, liquid, or gas. Add or remove heat and watch the phase change. Change the temperature or volume of a container and see a pressure-temperature diagram respond in real time. Relate the interaction potential to the forces between molecules.</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistryLab?exp=waveString" style="border: 0px dashed #08ACF0">
                            <div style="font-size:2em;" class="value1 hideThis">Waves on a String</div>
                            <div style="display:none;" class="showThis">
                            <div class="value1" style="color:#08ACF0 font-size:2em">Explore the wonderful world of waves! Even observe a string vibrate in slow motion. Wiggle the end of the string and make waves, or adjust the frequency and amplitude of an oscillator.</div>
                            </div>
                        </a>
                    </div>

                    

                    </div>
                    
                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>