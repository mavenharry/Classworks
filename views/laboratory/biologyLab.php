<?php include_once ("laboratorybtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Laboratory</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
$schoolDetails = $laboratoryClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->
                    <div class="">

                    <?php 
                    if($_GET["exp"] == "heart"){ ?>
                        <iframe id="embedded-human" frameBorder="0" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" allowFullScreen="true" src="https://human.biodigital.com/widget/?be=2flp&camera=-2.838,138.944,27.119,-2.046,135.643,4.454,0.005,0.99,-0.144&initial.hand-hint=true&load-rotate=15&ui-fullscreen=false&ui-zoom=true&ui-share=false&ui-info=true&dk=45f67794f04b91c4a256ddd5ba4c00357e3f90f6"></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "spine"){ ?>
                        <iframe id="embedded-human" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" frameBorder="0" width="560" height="450" allowFullScreen="true" src="https://human.biodigital.com/widget/?be=2fm6&initial.hand-hint=true&load-rotate=15&ui-info=true&ui-fullscreen=false&ui-zoom=true&ui-share=false&dk=45f67794f04b91c4a256ddd5ba4c00357e3f90f6"></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "brain"){ ?>
                        <iframe id="embedded-human" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" frameBorder="0" width="600" height="450" allowFullScreen="true" src="https://human.biodigital.com/widget/?be=2hJM&camera=-1.942,164.102,28.568,-0.968,167.055,10.25,0.01,0.982,-0.187&ui-info=true&ui-fullscreen=false&ui-zoom=true&ui-share=false&dk=45f67794f04b91c4a256ddd5ba4c00357e3f90f6"></iframe>
                    <?php } ?>

                    </div>
                    
                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>