<?php include_once ("laboratorybtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Laboratory</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
$schoolDetails = $laboratoryClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <!-------------------- START - Main Menu -------------------->
            <?php include("../../includes/sidebar.php") ?>
            <!-------------------- END - Main Menu -------------------->
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->
                    <div class="col-sm-12 row">

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./chemistry" style="border: 0px dashed #08ACF0">
                            <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                <img src="./icons/chemistry.png" style="display:block; width:80px; height:80px;"/>
                            </div>
                            <div style="font-size:2em;" class="value1 hideThis">Chemistry</div>
                            <div style="display:none;" class="showThis">
                            <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                <div class="icon-w">
                                <img src="./icons/chemistry.png" style="display:block; width:40px; height:40px;"/>
                                </div>
                            </div>
                            <div class="value1" style="color:#08ACF0 font-size:2em">Chemistry Lab</div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="#" style="border: 0px dashed #08ACF0">
                            <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                <img src="./icons/physics.png" style="display:block; width:80px; height:80px;"/>
                            </div>
                            <div style="font-size:2em;" class="value1 hideThis">Physics</div>
                            <div style="display:none;" class="showThis">
                            <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                <div class="icon-w">
                                <img src="./icons/physics.png" style="display:block; width:40px; height:40px;"/>
                                </div>
                            </div>
                            <div class="value1" style="color:#08ACF0 font-size:2em">Physics Lab</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="./biology" style="border: 0px dashed #08ACF0">
                            <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                <img src="./icons/biology.png" style="display:block; width:80px; height:80px;"/>
                            </div>
                            <div style="font-size:2em;" class="value1 hideThis">Biology</div>
                            <div style="display:none;" class="showThis">
                            <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                <div class="icon-w">
                                <img src="./icons/biology.png" style="display:block; width:40px; height:40px;"/>
                                </div>
                            </div>
                            <div class="value1" style="color:#08ACF0 font-size:2em">Biology Lab</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="#" style="border: 0px dashed #08ACF0">
                            <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                <img src="./icons/geography.png" style="display:block; width:75px; height:75px;"/>
                            </div>
                            <div style="font-size:2em;" class="value1 hideThis">Geography</div>
                            <div style="display:none;" class="showThis">
                            <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                <div class="icon-w">
                                <img src="./icons/geography.png" style="display:block; width:40px; height:40px;"/>
                                </div>
                            </div>
                            <div class="value1" style="color:#08ACF0 font-size:2em">Geography Lab</div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-4 col-xxxl-3">
                        <a class="element-box el-tablo" href="#" style="border: 0px dashed #08ACF0">
                            <div style="margin-left:0; color:#08ACF0" class="hideThis trending trending-up-basic">
                                <img src="./icons/mathematics.png" style="display:block; width:80px; height:80px;"/>
                            </div>
                            <div style="font-size:2em;" class="value1 hideThis">Mathems</div>
                            <div style="display:none;" class="showThis">
                            <div style="margin-left:0; color:#08ACF0" class="trending trending-up-basic">
                                <div class="icon-w">
                                <img src="./icons/mathematics.png" style="display:block; width:40px; height:40px;"/>
                                </div>
                            </div>
                            <div class="value1" style="color:#08ACF0 font-size:2em">Mathematics Lab</div>
                            </div>
                        </a>
                    </div>

                    

                    </div>
                    
                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>