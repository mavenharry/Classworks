<?php include_once ("laboratorybtn.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>ClassWorks - School Automation Solution | Laboratory</title>
    <!-------------------- START - Meta -------------------->
    <?php include("../../includes/meta.php") ?>
    <!-------------------- END - Meta -------------------->
    <?php include_once ("../../includes/styles.php"); ?>
    <link href="./css/style.css" rel="stylesheet">
    
</head>
<?php
$schoolDetails = $laboratoryClass->getSchoolDetails();
?>

<body class="menu-position-side menu-side-left full-screen with-content-panel">
    <div class="all-wrapper with-side-panel solid-bg-all">

        <div class="layout-w">
            <!-------------------- START - Main Menu -------------------->
            
            <!-------------------- END - Main Menu -------------------->
    
            <div class="content-w">

            <!-------------------- START - Top Bar -------------------->
                <?php include("../../includes/topbar.php") ?>
            <!-------------------- END - Top Bar -------------------->
            
                <div style="margin-top:10px;" class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i>
                    <span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                        
                    <!-- START -->
                    <div class="">

                    <?php 
                    if($_GET["exp"] == "acidbase"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/acid-base-solutions/latest/acid-base-solutions_en.html" width="" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "atomic"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/atomic-interactions/latest/atomic-interactions_en.html" width="" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "balanceEqn"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/balancing-chemical-equations/latest/balancing-chemical-equations_en.html" width="" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "baloonAndStaticElectricity"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/balloons-and-static-electricity/latest/balloons-and-static-electricity_en.html" width="" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "buildAnAtom"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/build-an-atom/latest/build-an-atom_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "concentration"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/concentration/latest/concentration_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "IsotopesAtomicMass"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/isotopes-and-atomic-mass/latest/isotopes-and-atomic-mass_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "beerLaw"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/beers-law-lab/latest/beers-law-lab_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "molarity"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/molarity/latest/molarity_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "moleculePolarity"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/molecule-polarity/latest/molecule-polarity_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "moleculeAndLight"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/molecules-and-light/latest/molecules-and-light_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "molecularShapes"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/molecule-shapes/latest/molecule-shapes_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "phScale"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/ph-scale/latest/ph-scale_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "reactant"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/reactants-products-and-leftovers/latest/reactants-products-and-leftovers_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "rutherfordScatter"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/rutherford-scattering/latest/rutherford-scattering_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "stateofMatter"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/states-of-matter/latest/states-of-matter_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    <?php 
                    if($_GET["exp"] == "waveString"){ ?>
                    <iframe src="https://phet.colorado.edu/sims/html/wave-on-a-string/latest/wave-on-a-string_en.html" style="width:100%; position:absolute; right:0px; left:0px; top:60px" height="660" scrolling="no" allowfullscreen></iframe>
                    <?php } ?>

                    </div>
                    
                    <!-- END -->

                    </div>
      
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>

<!-------------------- START - MODAL -------------------->
<?php include("../../includes/modals/index.php"); ?>
<!-------------------- END - MODAL -------------------->
               
<?php include_once ("../../includes/scripts.php"); ?>

<!-------------------- START - Top Bar -------------------->
<?php include("../../includes/support.php") ?>
<!-------------------- END - Top Bar -------------------->
</body>

</html>