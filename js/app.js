$(document).ready(function(){

    // var rand = Math.floor(Math.random() * 2);
    // $("#studentuploadPreview").attr("src", "../../images/avatar/male"+rand+".png")
    // $("#studentupload-photo").attr("value", "../../images/avatar/male"+rand+".png")
    
    sortTable(2, 'allAssessments');

    var current = location.pathname;
    var page = current.split("/")[3];
    var page2 = current.split("/")[4];
    // var page = current.split("/")[2];
    var cid = page.split(".")[0];
    var cid2 = page2.split(".")[0];
    $("#"+cid).addClass("active");
    
    if(cid == "students"){
        $("#whoToDelete").val("Student").trigger('change');
    }else{
        if(cid == "parents"){
            $("#whoToDelete").val("Parent").trigger('change');
        }else{
            if(cid == "staffs"){
                $("#whoToDelete").val("Staff").trigger('change');
            }else{
                if(cid2 == "pickuppersons"){
                    $("#whoToDelete").val("Pickup person").trigger('change');
                }
            }
        }  
    }

    $(".updateFirst").on('click', function(){
        $('#newAcademicModal').modal({
            kayboard: false
        }, 'show');
        
    });
    
    $('#createSchoolModal').modal({
        backdrop: 'static',
        kayboard: false
    }, 'show');

    $(".date_of_birth").datepicker(
        {
            changeMonth: true, 
            changeYear: true, 
            dateFormat: "dd/mm/yy",
            yearRange: "-90:+01"
        }
    );


    $(".date_of_holiday").datepicker(
        {
            changeMonth: true, 
            changeYear: false, 
            dateFormat: "dd MM",
            yearRange: "-90:+01"
        }
    );
   
    verifyCheckState();

    var selectedClass = $("#staffSelectedClass").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {staffClass: selectedClass},
        cache: false,
        success: function(results){
            $("#allClassSubjects").html(results);
            loadAssessments();
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });


    var selectedClass = $("#assessmentClass").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {subjectClass: selectedClass},
        cache: false,
        success: function(results){
            $("#assessmentSubject").html(results);
            loadAssessmentsAdmin();
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });    

});

$('.create-school-btn').on('click', function () {
    $('#stampUploadModal, #assessmentSelect, #newMidTermModal, #messageSearchModal, #newAssessmentModal, #gradingSystemModal, #publishResultModal, #paymentSwitch, #examPrep, #classSheet, #admissionStatusModal, #staffIdcardSelector, #setupNewExam, #rechargeWallet, #allReceipts, #promoteStudent, #promoteClass, #tuckshopConfiguration, #checkWalletBalance, #newPickupModal, #addNewProduct, #searchItemModal, #searchProductModal, #deleteItemModal, #deleteProductModal, #processItemCollection, #addNewItem, #inventoryConfiguration,  #bulkPayment, #newFamilyModal, #idcardSelector, #smsPurchaseModal, #addExamQuestion, #searchStudentsExamModal, #searchQuestionsAnswersModal, #affliateForm, #timetableClassSwitch, #timetableStaffSwitch, #singleReportModal, #assessmentSwitch, #searchPickedStudents, #assignPickPersonModal, #searchPickupDisabledModal, #searchPickupEnabledModal, #newPickPersonModal, #searchPickPerson, #salarySearchModal, #salaryDisburseModal, #assignUnpaidStudentsModal, #searchUnpaidStudentsModal, #searchPaidStudentsModal, #editStaffTimetableModal, #newStaffTimetableModal, #newFeeModal, #newTimetableModal, #editTimetableModal, #searchEnrollModal, #searchStudentAttendanceModal, #searchStaffAttendanceModal, #VerifyChildPickupModal, #searchSubjectsModal, #searchClassModal, #newDeleteSubjectModal, #searchStaffsModal, #searchParentsModal, #newDeleteModal, #searchStudentsModal, #newStudentModal, #broadcastModal, #newDeleteClassModal, #newDeleteSubjectModal, #createSchoolModal, #show-on-load, #newStaffModal, #newClassModal, #newSubjectModal, #newParentModal, #newEnrollModal, #createSchoolModalClose, #successModal, #errorModal, #contactParentModal, #newAcademicModal, #accountModal, #noticeModal, #changePasswordModal, #selectPaymentMethod, #cashPaymentReceiptNoModal, #changeStaffPasswordModal, #paymentSwitchBulk, #newQuestionModal, #subjectExamScoreModal').click();
});


$('#school_currency').on('change', function () {
    var new_currency = $("#school_currency").val();
    $(".picked_currency").html(new_currency);
});

$("#formerName").on('change', function(){
    var formerClass = $(this).val();
    if(formerClass == ""){
        $("#editBtn").attr("disabled", "true");
        $("#newName").css("display", "none");
    }else{
        $("#editBtn").removeAttr("disabled", "false");
        $("#newName").css("display", "block");
    }
});

$("#addTiming").on('click', function(){
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: 'GET',
        data: {getTiming : 1},
        timeout: 10000,
        success: function(data) {
            $("#lastInput").append(data);
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$("#addAssessment").on('click', function(){
    $.ajax({
        url: '../../views/assessment/ajaxHandler.php',
        method: 'GET',
        data: {newInput : 1},
        timeout: 10000,
        success: function(data) {
            $("#lastInput").append(data);
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$("#addMidTerm").on('click', function(){
    $.ajax({
        url: '../../views/assessment/ajaxHandler.php',
        method: 'GET',
        data: {newInput : 1},
        timeout: 10000,
        success: function(data) {
            $("#lastMidTermInput").append(data);
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

// $("#addGradingSystem").on('click', function(){
//     $("#savegradebtn").removeAttr("hidden");
//     $.ajax({
//         url: '../../includes/modals/modalAjaxHandler.php',
//         method: 'GET',
//         data: {addGradingDiv : 1},
//         timeout: 10000,
//         success: function(data) {
//             $("#lastResultInput").append(data);
//         },
//         error: function(data){
//             toastr.error("An error occured. Please try again");
//         }
//     });
// });

$("#paymentTerm").on('change', function(){
    $('#paymentStudent').html("<option value=''>Select Student</option>");
    $('#paymentClass').prop('selectedIndex',0);
    $('#paymentFee').html("<option value=''>Select Fee</option>");
});


$("#paymentStudent").on('change', function(){
    var paymentSession = $("#paymentSession").val();
    var paymentTerm = $("#paymentTerm").val();
    var paymentClass = $("#paymentClass").val();
    var studentId = $(this).val();
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {paymentSessionStudent : paymentSession, paymentTermStudent : paymentTerm, paymentClassStudent : paymentClass, studentIdStudent : studentId},
        timeout: 10000,
        success: function(data) {
            $("#paymentFee").html(data);
        },
        error: function(data){
            toastr.error(JSON.stringify(data));
        }
    });
});

$("#paymentClass").on('change', function(){
    $('#paymentFee').html("<option value=''>Select Fee</option>");
    var paymentClass = $(this).val();
    // GET CLASS STUDENT
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {className : paymentClass},
        timeout: 10000,
        success: function(data) {
            $("#paymentStudent").html(data);
        },
        error: function(data){
            toastr.error(JSON.stringify(data));
        }
    });
});

$(document).on('click', '#rTiming', function(){
    $(this).closest('.timingDiv').remove();
});

$(document).on('click', '#timingBtn', function(){
    var btn = $(this);
    var id = $(this).closest('.schoolActivity').attr("data-id");
    $.ajax({
        url: 'ajaxHandler.php',
        method: 'GET',
        data: {deleteTiming : id},
        timeout: 10000,
        success: function(data) {
            if(data.trim() == "done"){
                btn.closest('.schoolActivity').remove();
            }else{
                toastr.error("An error occured. Please try again");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$(document).on('click', '#scoreBtn', function(){
    var btn = $(this);
    var id = $(this).closest('.assessmentRow').attr("data-id");
    $.ajax({
        url: '../../views/assessment/ajaxHandler.php',
        method: 'GET',
        data: {delAss : id},
        timeout: 10000,
        success: function(data) {
            if(data.trim() == "done"){
                btn.closest('.assessmentRow').remove();
                toastr.success("Assessment Deleted");
            }else{
                toastr.error("An error occured. Please try again");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$(document).on('click', '#saveBtn', function(){
    toastr.warning("Processing...");
    var btn = $(this);
    var id = $(this).closest('.assessmentRow').attr("data-id");
    var assName = $(this).closest(".assessmentRow").find("#assessmentName").val();
    var assMark = $(this).closest(".assessmentRow").find("#assessmentMark").val();
    $.ajax({
        url: '../../views/assessment/ajaxHandler.php',
        method: 'GET',
        data: {assName: assName, assMark: assMark, assId : id, action: "editAssessment"},
        timeout: 10000,
        success: function(data) {
            if(data.trim() == "done"){
                toastr.success("Assessment edited successfully");
            }else{
                toastr.error("An error occured. Please try again");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

function sortTable(index, tableId) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById(tableId);
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
      //start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /*Loop through all table rows (except the
      first, which contains table headers):*/
      for (i = 1; i < (rows.length - 1); i++) {
        //start by saying there should be no switching:
        shouldSwitch = false;
        /*Get the two elements you want to compare,
        one from current row and one from the next:*/
        x = rows[i].getElementsByTagName("TD")[index];
        y = rows[i + 1].getElementsByTagName("TD")[index];
        //check if the two rows should switch place:
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
      if (shouldSwitch) {
        /*If a switch has been marked, make the switch
        and mark that a switch has been done:*/
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
}

$('#whenBroadcast').on('change', function () {
    var whenBroadcast = $("#whenBroadcast").val();
    if(whenBroadcast != "Now"){
        $("#broadcastDate").css("display", "block");
    }else{
        $("#broadcastDate").css("display", "none");
    }
});

$('#audienceBroadcast').on('change', function () {
    var audienceBroadcast = $(this).val();
    if(audienceBroadcast.indexOf("Specify") != -1){
        $("#specifyNumbers").css("display", "block");
    }else{
        $("#specifyNumbers").css("display", "none");
    }
    if(audienceBroadcast.indexOf("classes") != -1){
        $("#BroadcastClasses").css("display", "block");
        $("#BroadcastCategory").css("display", "block");
    }else{
        $("#BroadcastClasses").css("display", "none");
        $("#BroadcastCategory").css("display", "none");
    }
});

$('#staffCheck').on("change", function(){
    if($(this).is(':checked')){
        $("#createStaff").removeAttr("disabled");
    }else{
        $("#createStaff").attr("disabled", "true");
    }
});

$('#studentCheck').on("change", function(){
    if ($(this).is(':checked')) {
        $("#createStudent").removeAttr("disabled");
    } else {
        $("#createStudent").attr("disabled", "true");
    }
});

$('.dholidays').on('change', function(){
    var defaultId = $(this).closest('tr').attr('data-id');
    var val;
    if($(this).is(':checked')){
        val = 1;
    }else{
        val = 0;
    }
    $.ajax({
        url: '../../views/timetable/ajaxHandler.php',
        method: 'GET',
        data: {defaultId : defaultId, val : val},
        timeout: 10000,
        success: function(data) {
            if(data == "true"){
                toastr.success("School's holiday has been updated");
            }else{
                toastr.error("Couldnt Complete this operation, Please try again later");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$("#contactParent").on('click', function(){
    var stdnNumber = $(this).attr("data-id");
    $.ajax({
        url: '../../views/students/ajaxHandler.php',
        method: 'GET',
        data: {student : stdnNumber},
        timeout: 10000,
        success: function(data) {
            $("#parentContact").html(data);
            $('#contactParentModal').modal({
                kayboard: false
            }, 'show');
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});


$("#messageCount").on('keyup', function(){
    $("#textCount").text($(this).val().length);
    $("#pages").text(Math.ceil($(this).val().length/160)+" Page(s)");
});

$("#newHoliday").on('click', function(){
    var addHol = 1;
    $.ajax({
        url: '../../views/timetable/ajaxHandler.php',
        method: 'GET',
        data: {addHol : addHol},
        timeout: 10000,
        success: function(data) {
            $("#holidays").append(data);
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });
});

$(document).on('click', '.delHolBtn', function(){
    $(this).closest('.holiday').remove();
});

$(document).on('click', '.delHol', function(){
    var delId = $(this).closest('.holiday').attr('data-id');
    var btn = $(this);
    $.ajax({
        url: '../../views/timetable/ajaxHandler.php',
        method: 'GET',
        data: {delId : delId},
        timeout: 10000,
        success: function(data) {
            if(data == "true"){
                btn.closest('.holiday').remove();
            }else{
                toastr.error("An error occured. Please try again");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });

});

$('.days').on("change", function(){
    var day = $(this).val();
    var val;
    if($(this).is(':checked')){
        val = 1;
    }else{
        val = 0;
    }
    $.ajax({
        url: '../../views/timetable/ajaxHandler.php',
        method: 'GET',
        data: {day : day, val : val},
        timeout: 10000,
        success: function(data) {
            if(data == "true"){
                if(val == 1){
                    toastr.success(day+" has been turned on");
                }else{
                    toastr.success(day+" has been turned off");
                }
            }else{
                toastr.error("Couldnt Complete this operation, Please try again later");
            }
        },
        error: function(data){
            toastr.error("An error occured. Please try again");
        }
    });

});

$("#teacherSortClass").on('change', function(){
    var classCode = $(this).val();
    if(classCode != "All"){
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {classCodeSort : classCode},
            timeout: 10000,
            success: function(data) {
                $("#teacherSortSubject").html(data);
            },
            error: function(data){
                toastr.error("An error occured. Please try again");
            }
        });
    }
});

$("#parentChecker").on("change", function(){
    if ($(this).is(":checked")){
        $("#createParent").removeAttr("disabled");
    }else{
        $("#createParent").attr("disabled", "true");
    }
});

$("#pickupStudent").on('blur', function(){
    var studentNumber = $(this).val();
    $("#pickupStudentName").val("Searching...");
    if(studentNumber == ""){
        $("#pickupStudentName").val("Student's Name");
    }else{
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {pickupStudentNumber : studentNumber},
            timeout: 10000,
            success: function(data) {
                $("#pickupStudentName").val(data);
                $("#createPickup").removeAttr("disabled");
            },
            error: function(data){
                $("#pickupStudentName").val("An error occured. Please continue");
            }
        });
    }
});

$("#studentMotherContact").on("blur", function(){
    var studentMotherContact = "0"+$(this).val().substr(-10, 10);
    $("#studentMotherName").val("Searching...");
    if(studentMotherContact == ""){
        $("#studentMotherName").val("Mother's Name");
    }else{
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {studentMotherContact : studentMotherContact},
            timeout: 10000,
            success: function(data) {
                $("#studentMotherName").val(data);
            },
            error: function(data){
                $("#studentMotherName").val("An error occured. Please continue");
            }
        });
    }
});

$("#studentFatherContact").on("blur", function(){
    var studentFatherContact = "0"+$(this).val().substr(-10, 10);
    $("#studentFatherName").val("Searching...");
    if(studentFatherContact == ""){
        $("#studentFatherName").val("Father's Name");
    }else{
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {studentFatherContact : studentFatherContact},
            timeout: 10000,
            success: function(data) {
                $("#studentFatherName").val(data);
            },
            error: function(data){
                $("#studentFatherName").val("An error occured. Please continue");
            }
        });
    }
});

$("#parentPhone").on("blur", function(){
    var parentPhone = "0"+$(this).val().substr(-10, 10);
    $("#parentStudentName").val("Searching...");
    if(studentFatherContact == ""){
        $("#parentStudentName").val("Student's Name");
    }else{
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {parentPhone : parentPhone},
            timeout: 10000,
            success: function(data) {
                $("#parentStudentName").val(data);
            },
            error: function(data){
                $("#parentStudentName").val("An error occured. Please continue");
            }
        });
    }
});

$("#patCode").on("keyup", function(){
    var patCode = $(this).val();
    if(patCode == ""){
        $("#btnCeateAccount").removeAttr("disabled", "false");
        $("#patMessage").html('');
    }else{
        $("#btnCeateAccount").attr("disabled", true);
        $.ajax({
            url: 'authAjax.php',
            method: 'GET',
            data: {patCode : patCode},
            timeout: 10000,
            success: function(data) {
                if(data == "false"){
                    $("#btnCeateAccount").attr("disabled", true);
                    $("#patMessage").html('<span class="text text-danger" style="font-size: 15px;">Invalid Partner Code</span>');
                }else{
                    $("#btnCeateAccount").removeAttr("disabled", "false");
                    $("#patMessage").html('');
                }
            },
            error: function(data){
                $("#patMessage").html('<span class="text text-danger" style="font-size: 15px;">Taking too long to verify code. Please refresh</span>');
            }
        });
    }
});

// $("#showPeriod").on('change', function(){
//     $(this).form.submit();
// });

document.onkeydown = function(e) {
    if(e.keyCode == 123) {
    return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
    return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
    return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
    return false;
    }
    if(e.keyCode == 224 || e.keyCode == 17 || e.keyCode == 93){
    return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
    return false;
    }         
};

$(document).ready(function(){
    // $(".setUp").on("click", function(e){
    //     e.preventDefault();
    //     $(".setUpBtn").removeAttr("disabled");
    // });

    $("#domainvAL").on("focus", function(){
        $("#message").empty();
    });

    $("#sortTermModal").on("change", function(){
        var className = $("#sortClassModal").val();
        var term = $(this).val();
        $.ajax({
            url: "../../views/payments/ajaxHandler.php",
            method: 'GET',
            data: {classFee : className, term : term},
            timeout: 10000,
            success: function(data) {
                $("#sortFeeModal").html(data);
                $("#sortFeeModal").prepend('<option value="all">All Fees</option>');
                $('#sortFeeModal').prop('selectedIndex',0);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#sortClassModal").on('change', function(){
        $('#sortTermModal').prop('selectedIndex',0);
    });

    $("#outstandingTerm").on("change", function(){
        var className = $("#outstandingClass").val();
        var term = $(this).val();
        $.ajax({
            url: "../../views/payments/ajaxHandler.php",
            method: 'GET',
            data: {classFee : className, term : term},
            timeout: 10000,
            success: function(data) {
                $("#outstandingFee").html(data);
                $("#outstandingFee").prepend('<option value="all">All Fees</option>');
                $('#outstandingFee').prop('selectedIndex',0);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#outstandingClass").on('change', function(){
        $('#outstandingTerm').prop('selectedIndex',0);
    });

    $("#delStudent").on('click', function(){
        var stdnNumber = $(this).attr("data-id");
        var whoToDelete = "Student";
        $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {delStudent : stdnNumber},
            timeout: 70000,
            success: function(data) {
                if(data == "true"){
                    toastr.success("Successfully deleted. Please wait while we redirect you");
                    var delay = 4000;
                    setTimeout(function() {
                        window.location.assign('home.php');
                    }, delay);
                }else{
                    alert(data);
                    toastr.error("An error occured while trying to delete this student");
                }
                //toastr.success(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#delStaff").on('click', function(){
        var stffNumber = $(this).attr("data-id");
        var whoToDelete = "Student";
        var reply = confirm("Are you sure you want to delete this staff?");
        if(reply === true){
            $.ajax({
            url: '../../includes/modals/modalAjaxHandler.php',
            method: 'GET',
            data: {delStaff : stffNumber},
            timeout: 70000,
            success: function(data) {
                if(data == "true"){
                    toastr.success("Successfully deleted. Please wait while we redirect you");
                    var delay = 4000;
                    setTimeout(function() {
                        window.location.assign('admin_staffs.php');
                    }, delay);
                }else{
                    alert(data);
                    toastr.error("An error occured while trying to delete this student");
                }
                //toastr.success(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
        }
    });

    $("#domainvAL").on("blur", function(){

        var value = $(this).val();
        $.ajax({
            url: '../../includes/ajaxHandler.php',
            method: 'GET',
            data: {domain : value},
            timeout: 10000,
            success: function(data) {
                if(data == "true"){
                    $("#CntBtn").attr("href","#stepContent2");
                }else{
                    $("#message").html("<h6 class='text-danger'>School prefix already used. Please try another one</h6>");
                }
            },
            error: function(data){
                toastr.error("An error occured while trying to verify your domain name");
            }
        });
    });

    // MODAL AJAX STARTS HERE
        $("#staffClasses").on('change', function(){
            var className = $("#staffClasses option:selected").last().val();
            $.ajax({
                url: '../../includes/modals/modalAjaxHandler.php',
                method: 'GET',
                data: {className : className},
                timeout: 10000,
                success: function(data) {
                    $("#selectedSubject").html(data);
                },
                error: function(data){
                    toastr.error(JSON.stringify(data));
                }
            });

        });
    // MODAL AJAX ENDS HERE

    $("#printClass").on('change', function(){
        var className = $(this).val();
        $.ajax({
            url: "../../views/assessment/ajaxHandler.php",
            method: 'GET',
            data: {className : className, action: "getClassStudent"},
            timeout: 20000,
            success: function(data) {
                $("#printStudent").html(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#sortPaymentClass").on('change', function(){
        var className = $(this).val();
        $.ajax({
            url: "../../views/payments/ajaxHandler.php",
            method: 'GET',
            data: {className : className},
            timeout: 10000,
            success: function(data) {
                $("#sortPaymentStudent").html(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#sortSlipClass, #sortSlipClassReceipt").on('change', function(){
        var className = $(this).val();
        $.ajax({
            url: "../../views/payments/ajaxHandler.php",
            method: 'GET',
            data: {className : className},
            timeout: 10000,
            success: function(data) {
                $(".sortSlipStudent").html(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    $("#sortSlipTerm").on('change', function(){
        var term = $(this).val();
        var className = $("#sortSlipClass").val();
        $.ajax({
            url: "../../views/payments/ajaxHandler.php",
            method: 'GET',
            data: {classFee : className, term : term},
            timeout: 10000,
            success: function(data) {
                $("#sortSlipFee").html(data);
            },
            error: function(data){
                toastr.error(JSON.stringify(data));
            }
        });
    });

    // WEB PAGE SELECTION
    $(document).on('click','.webpage',function(){
        var img = $(this);
        var template = $(this).attr('data-id');
        $("#templateId").val(template);
        var id = $(this).next('img').attr('id');
        $("#"+id).css("display", "block");
        $(".webpageMarker").not('[id='+id+']').css("display", "none");
    });
});

// function pickPerson(person_id){
//     $("#"+person_id).toggleClass("verified");
// }

$(document).on('click', '#pickupPerson', function(){
    $(this).find("#person1").toggleClass("verified");
    $("#pknumber").val($(this).attr('data-id'));
});



function updateTime(){
    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    if (seconds < 10){
        seconds = "0" + seconds;
    }
    if (minutes < 10){
        minutes = "0" + minutes;
    }
    var Twelvehours = ((hours + 11) % 12 + 1);
    var t_str = Twelvehours + ":" + minutes + ":" + seconds + " ";
    if(hours > 11){
        t_str += "PM";
    } else {
        t_str += "AM";
    }
    $("#attendanceTime").html(t_str);
}
setInterval(updateTime, 1000);

var d = new Date();
var day = d.getDate();
var month = d.getMonth()+1;
var year = d.getFullYear();
if (day < 10){
    day = "0" + day;
}
if (month < 10){
    month = "0" + month;
}
if (year < 10){
    year = "0" + year;
}
var date = day+"/"+month+"/"+year;
$("#attendanceDate").html(date);

    function executeChildPickup() {
        setTimeout(function(){
            var pickupCardNo = $("#pickupCardNo").val();
            if(pickupCardNo == ""){
                $("#resultAttendStateChild").css("display", "none");
                $("#initialAttendStateChild").css("display", "block");
            }else{
                $.ajax({
                    url: "../../views/childpickup/ajaxHandler.php",
                    method: "GET",
                    data: {pickupCardNo: pickupCardNo},
                    cache: false,
                    success: function(results){
                        $("#display").html(results);
                    },
                    error : function (e) {
                        toastr.error('Sorry an error occured!');  
                    }
                });
            }
            $("#initialAttendStateChild").css("display", "none");
            $("#resultAttendStateChild").css("display", "block");

            setTimeout(function () {
                $("#resultAttendStateChild").css("display", "none");
                $("#initialAttendStateChild").css("display", "block");
            }, 30000);
        }, 1000);
    }

function executeAttendance() {
    setTimeout(function(){
        var attendanceCode = $("#attendanceCode").val();
        if(attendanceCode == ""){
            toastr.error("Please use the barcode reader to scan your ID card");
        }else{
            
            $.ajax({
                url: "../../views/attendance/ajaxHandler.php",
                method: "GET",
                data: {attendanceNumber: attendanceCode},
                cache: false,
                success: function(results){
                    toastr.success(results);
                    $("#attendanceCode").val("");
                },
                error : function (e) {
                    toastr.error('Sorry an error occured!');  
                }
            });
            $("#attendanceCode").val("");
            scan = true;
        }
    }, 1000);
}

function selectSetting(id){
    $("#"+id).css("background-color", "#08ACF0");
    $(".settings").not('[id='+id+']').css("background-color", "#c7d4da");
    $("#"+id+"View").css("display", "block");
    $(".settingsView").not('[id='+id+'View]').css("display", "none");
}

$('input').on('click', function () {
 verifyCheckState();
});



function verifyCheckState(){
    if($('#staffAttendanceState').is(":checked")){
        $(".attendanceStaffOption").css("display", "flex");
    }else{
        $(".attendanceStaffOption").css("display", "none");
    }
    
    if($('#studentAttendanceState').is(":checked")){
        $(".attendanceStudentOption").css("display", "flex");
    }else{
        $(".attendanceStudentOption").css("display", "none");
    }
    
    if($('#studentChildPickupState').is(":checked")){
        $(".childPickupOption").css("display", "flex");
    }else{
        $(".childPickupOption").css("display", "none");
    }
    
    if($('#Salaryautomaticdisbursement').is(":checked")){
        $(".salaryOption").css("display", "flex");
    }else{
        $(".salaryOption").css("display", "none");
    }
    
    if($('#outstandingFeeAlert').is(":checked")){
        $(".whenToNotifyFee").css("display", "flex");
    }else{
        $(".whenToNotifyFee").css("display", "none");
    }
    
    if($('#autoRenew').is(":checked")){
        $(".autoRenewElements").css("display", "flex");
        $(".manualPayment").css("display", "none");
        $("#othersNumber2").css("display", "none");
    }else{
        $(".autoRenewElements").css("display", "none");
        $(".manualPayment").css("display", "flex");
        $("#othersNumber2").css("display", "none");
    }

    if($('#onlinePayment').is(":checked")){
        $(".selectFeeBank").css("display", "flex");
    }else{
        $(".selectFeeBank").css("display", "none");
    }

}

function previewFile1(){
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("studentupload-photo2").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("studentuploadPreview2").src = oFREvent.target.result;
    };
}
$("#studentupload-photo2").change(function () {
    previewFile1();
});


function previewFile2(){
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("studentupload-photo").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("studentuploadPreview").src = oFREvent.target.result;
    };
}
$("#studentupload-photo").change(function () {
    previewFile2();
});


function previewFile3(){
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("staffupload-photo").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("staffuploadPreview").src = oFREvent.target.result;
    };
}
$("#staffupload-photo").change(function () {
    previewFile3();
});


function previewFile4(){
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("parentupload-photo").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("parentuploadPreview").src = oFREvent.target.result;
    };
}
$("#parentupload-photo").change(function () {
    previewFile4();
});

function previewFile5(){
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("pickupUpload-photo").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("pickupUploadPreview").src = oFREvent.target.result;
    };
}
$("#pickupUpload-photo").change(function () {
    previewFile5();
});

function paymentAmount(amount){
    var amount = amount;
    if(amount < 2500){
        var charge = 0.0157 * amount;
        return charge;
    }else{
        var charge = (0.0157 * amount) + 100;
        return charge;
    }
}

function idcardClick(id){
    $("#"+id).css("display", "block");
    $(".idcardMarker").not('[id='+id+']').css("display", "none");
}

$('#revieverNotify').on('change', function () {
    var reciever = $('#revieverNotify').val();
    if(reciever == "Other"){
        $(".othersNumber").css("display", "flex");
    }else{
        $(".othersNumber").css("display", "none");
    }
});


$('#selectedBankOption').on('change', function () {
    var reciever = $('#selectedBankOption').val();
    if(reciever == "Other"){
        $(".contactSupport").css("display", "flex");
    }else{
        $(".contactSupport").css("display", "none");
    }
});


$('#subScriptionWhoToNotify').on('change', function () {
    var reciever = $('#subScriptionWhoToNotify').val();
    if(reciever == "Other"){
        $("#othersNumber4").css("display", "flex");
    }else{
        $("#othersNumber4").css("display", "none");
    }
});


$('.timetableTime').on('change', function () {
    var reciever = $('.timetableTime').val();
    if(reciever == "Other"){
        $("#othersNumber3").css("display", "flex");
    }else{
        $("#othersNumber3").css("display", "none");
    }
});


function removeTime(id){
$("#"+id).remove();
}

$(".addTime").on('click', function(){
    var id = $("#setTime").val().replace(":", "").replace(" ", "");
    var time = $("#setTime").val();
    var title = $("#setTitle").val();
    $("#timeSheetDiv").append('<div class="form-group row timingDiv"><div class="col-sm-4"><select class="form-control timetableTime"><option value="'+title+'">'+title+'</option><option value="Devotion">Devotion time</option><option value="Class">Class time</option><option value="Break">Break time</option><option value="Sports">Sports time</option><option value="Lunch">Lunch time</option><option value="Lab">Lab time</option></select></div><div class="col-sm-5"><input class="form-control timeselect" value="'+time+'" placeholder="8:00 AM" type="text"></div><div class="col-sm-3"><button class="btn btn-primary" onclick="removeTime('+"'"+id+"'"+')" style="background-color:#FB5574; border-color:#FB5574" type="button"> Remove</button></div></div>');
});


$('#whoToDelete').on('change', function () {
    var who = $('#whoToDelete').val();
    $(".agreeWho").html(who.toLowerCase());
    if(who == "Student"){
        $("#whichIdToDelete").html(" Student number");
        $("#idToDelete").attr("placeholder", "2017/Stdn/GeorgeEl/016");
    }else{
        if(who == "Staff"){
            $("#whichIdToDelete").html(" Staff number");
            $("#idToDelete").attr("placeholder", "2017/Stff/GeorgeEl/016");
        }else{
            if(who == "Parent"){
                $("#whichIdToDelete").html(" Parent number");
                $("#idToDelete").attr("placeholder", "2017/Prnt/GeorgeEl/016");
            }else{
                if(who == "Pickup person"){
                    $("#whichIdToDelete").html(" Pickup number");
                    $("#idToDelete").attr("placeholder", "2017/Pickup/GeorgeEl/016");
                }
            }
        }
    }
});


// $("#studentGender").on('change', function(){
//     var selectedGender = $("#studentGender").val()
//     var upload = document.getElementById("studentuploadPreview").src
//     if(upload.substr(0,16) == "http://localhost"){
//         if(selectedGender == "Male"){
//             var rand = Math.floor(Math.random() * 2);
//             $("#studentuploadPreview").attr("src", "../../images/avatar/male"+rand+".png")
//             $("#studentupload-photo").attr("value", "../../images/avatar/male"+rand+".png") 
//         }else{
//             var rand = Math.floor(Math.random() * 4);
//             $("#studentuploadPreview").attr("src", "../../images/avatar/female"+rand+".png")
//             $("#studentupload-photo").attr("value", "../../images/avatar/male"+rand+".png") 
//         }   
//     }
// })


$("#staffSelectedClass").on('change', function(){
    var selectedClass = $(this).val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {staffClass: selectedClass},
        cache: false,
        success: function(results){
            $("#allClassSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});


function loadAssessments(){
    var selectedClass = $("#staffSelectedClass").val();
    var selectedSubject = $("#allClassSubjects").val();
    var part = $("#partOf").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {assessmentClass: selectedClass, assessmentSubject: selectedSubject, partToSelect: part},
        cache: false,
        success: function(results){
            $("#allAssessments").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
}

$("#assessmentType").on('change', function(){
    $('#assessmentClass').prop('selectedIndex',0);
    $('#assessmentSubject').prop('selectedIndex',0);
});


$("#switchAssessmentType").on('change', function(){
    var assessmentType = $(this).val();
    var pageURL = $(location).attr("href");
    var splitted;
    var newURL;
    if(pageURL.indexOf("&part") > 1){
        splitted = pageURL.split("&part");
        newURL = splitted[0]+"&part="+assessmentType;
        window.location.assign(newURL);
    }else{
        splitted = pageURL.split("#");
        newURL = splitted[0]+"&part="+assessmentType;
        window.location.assign(newURL);
    }
});

function loadAssessmentsAdmin(){
    var selectedClass = $("#assessmentClass").val();
    var selectedSubject = $("#assessmentSubject").val();
    var assessmentTerm = $("#assessmentTerm").html();
    var assessmentSession = $("#assessmentSession").html();
    var assessmentType = $("#assessmentType").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {assessmentSession: assessmentSession, assessmentTerm: assessmentTerm, assessmentClassAdmin: selectedClass, assessmentSubjectAdmin: selectedSubject, assessmentType: assessmentType},
        cache: false,
        success: function(results){
            if(results != "none"){
                $(".table").css("display", "table");
                $(".error_state").css("display", "none");
                $("#allAssessmentsAdmin").html(results);
            }else{
                $(".table").css("display", "none");
                $(".error_state").css("display", "block");
            }
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
}


// function updateAssessment(student_no, subject_code, column_name, trcounter){
//     alert(column_name);
//     var contrcounter = column_name+trcounter;
//     // var input_val = $("#"+contrcounter).val();
//     var input_val = $("#"+contrcounter).val();
//     $.ajax({
//         url: "../../views/assessment/ajaxHandler.php",
//         method: "GET",
//         data: {student_no: student_no, subject_code: subject_code, column_name: column_name, input_val: input_val},
//         cache: false,
//         success: function(results){
//             $("#total"+trcounter).html(results);
//             toastr.success('Mark updated successfully!');              
//         },
//         error : function (e) {
//             toastr.error('Sorry an error occured!');               
//         }
//     });
// }

$(document).on("blur", "#scoresInput", function(){
    var thisInput = $(this);
    var student_no = thisInput.attr("data-student");
    var subject_code = thisInput.attr("data-subject");
    var column_name = thisInput.attr("data-assName");
    var trcounter = thisInput.attr("data-row");
    var input_val = thisInput.val();
    var isMidTerm = $("#partOf").val();
    var scoreTerm = $("input[name=editAssessmentTerm]").val();
    var scoreSession = $("input[name=editAssessmentSession]").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {student_no: student_no, subject_code: subject_code, column_name: column_name, input_val: input_val, term: scoreTerm, session: scoreSession, pathToUpdate: isMidTerm},
        cache: false,
        success: function(results){
            $("#total"+trcounter).html(results);
            toastr.success('Mark updated successfully!');              
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');               
        }
    });
});


function setDomain(domain, session, term, student_no, attr_id, value, currentUser){
    if(currentUser != "form teacher" && currentUser != "admin"){
        toastr.error("This action can only be performed be the class form teacher");
    }else{
        $.ajax({
            url: "../../views/assessment/ajaxHandler.php",
            method: "GET",
            data: {domain: domain, session: session, term: term, student_no: student_no, attr_id: attr_id, value: value},
            cache: false,
            success: function(results){
                toastr.success('Rating updated successfully!');             
            },
            error : function (e) {
                toastr.error('Sorry an error occured!');               
            }
        });
    }
}

$(document).on('change', '#enableChildPickup', function(){
    var studentId = $(this).closest('tr').attr('data-id');
    var val;
    if($(this).is(':checked')){
        val = 1;
    }else{
        val = 0;
    }
    $.ajax({
        url: "../../views/childpickup/ajaxHandler.php",
        method: "GET",
        data: {studentId: studentId, val: val},
        cache: false,
        success: function(results){
            if(results == "true"){
                toastr.success('Pickup updated!');  
            }else{
                toastr.error('An error occurred. Please try again later!');  
            }
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');               
        }
    });
});


function updateResultComment(session, term, student_no, currentUser){
    var comment;
    if(currentUser != "form teacher" && currentUser != "admin"){
        comment = $("#formerComment").val();
        $("#resultComment").val(comment);
        toastr.error("This action can only be performed by the class form teacher");
    }else{
        comment = $("#resultComment").val();
        $.ajax({
            url: "../../views/assessment/ajaxHandler.php",
            method: "GET",
            data: {resultComment: comment, session: session, term: term, student_no: student_no},
            cache: false,
            success: function(results){
                toastr.success('Comment updated successfully!');             
            },
            error : function (e) {
                toastr.error('Sorry an error occured!');               
            }
        });
    }
}


$("#assessmentClass").on('change', function(){
    var selectedClass = $(this).val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {subjectClass: selectedClass},
        cache: false,
        success: function(results){
            $("#assessmentSubject").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});


$("#assessmentSubject, #assessmentClass").on('change', function(){
    loadAssessmentsAdmin();
});

$("#staffSelectedClass, #allClassSubjects").on('change', function(){
    loadAssessments();
});


//TIMETABLE

function activeTimeTd(classCode, dayCode, timeCode){
    $(".timetableModalClass").val(classCode);
    $(".timetableModalDay").val(dayCode);
    $(".timetableModalTime").val(timeCode);

    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableModalClass: classCode, timetableModalDay: dayCode, timetableModalTime: timeCode},
        cache: false,
        success: function(results){
            $(".timetableTitle").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
}

$("#idClassSelector").on('change', function(){
    var className = $(this).val();
    $.ajax({
        url: "../../views/students/ajaxHandler.php",
        method: "GET",
        data: {className: className},
        cache: false,
        success: function(results){
            $("#idClassStudent").html(results);   
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});


$(".stafftimeTableclasses").on('change', function(){
    var selectedClass = $(this).val();
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableClass: selectedClass},
        cache: false,
        success: function(results){
            $(".stafftimeTableSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$(".timetableStaffSelector").on('change', function(){
    var selectedStaff = $(this).val();
    $("#timetableStaff").html(selectedStaff.toUpperCase());
});


function activeTimeTdStaff(staffCode, dayCode, timeCode){
    $(".timetableModalStaffCode").val(staffCode);
    $(".timetableModalDay").val(dayCode);
    $(".timetableModalTime").val(timeCode);

    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableModalStaffCode: staffCode, timetableModalDay: dayCode, timetableModalTime: timeCode},
        cache: false,
        success: function(results){
            $(".timetableTitle").html(results);
            
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });


    var selectedClass = $(".stafftimeTableclasses").val();
    $.ajax({
        url: '../../includes/modals/modalAjaxHandler.php',
        method: "GET",
        data: {timetableClass: selectedClass},
        cache: false,
        success: function(results){
            $(".stafftimeTableSubjects").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });

}

$(".domain").on('change', function(){
    var newVal;
    var stateId = $(this).attr("data-id");
    var motor = $(this).attr("data-domain");
    if($(this).is(':checked')){
        newVal = 1;
    }else{
        newVal = 0;
    }
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {newDomainState: newVal, DomainNameId: stateId, motorType: motor},
        cache: false,
        success: function(results){
            toastr.success("Domain changed successfully!"); 
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

function goto_classsheet(page){
    var classname = document.getElementById("classAssessmentAssignClass").value;
    var session = document.getElementById("classAssessmentAssignSession").value;
    var term = document.getElementById("classAssessmentAssignTerm").value;
    window.location.assign("./"+page+"?session="+session+"&term="+term+"&class="+classname);
}

function goto(page){
    var session = document.getElementById("assessmentAssignSession").value;
    var term = document.getElementById("assessmentAssignTerm").value;
    window.location.assign("./"+page+"?session="+session+"&term="+term);
}

function goto_receipts(page){
    var receiptStudent = document.getElementById("receiptStudent").value;
    var receiptFee = document.getElementById("receiptFee").value;
    var receiptSession = document.getElementById("receiptSession").value;
    var receiptTerm = document.getElementById("receiptTerm").value;
    if(receiptStudent.length > 0 && receiptFee.length > 0){
        window.location.assign("./"+page+"?receiptStudent="+receiptStudent+"&receiptFee="+receiptFee+"&receiptSession="+receiptSession+"&receiptTerm="+receiptTerm);        
    }    
}

function goto_timetable(page){
    var session = document.getElementById("timetableAssignSession").value;
    var term = document.getElementById("timetableAssignTerm").value;
    var Stdnclass = document.getElementById("timetableAssignClass").value;
    window.location.assign("./"+page+"?session="+session+"&term="+term+"&class="+Stdnclass);
}


function goto_timetable_staff(page){
    var session = document.getElementById("timetableAssignSessionStaff").value;
    var term = document.getElementById("timetableAssignTermStaff").value;
    var Stdnstaff = document.getElementById("timetableAssignStaff").value;
    window.location.assign("./"+page+"?session="+session+"&term="+term+"&staff="+Stdnstaff);
}


function goto_payment(page){
    var session = document.getElementById("paymentSession").value;
    var term = document.getElementById("paymentTerm").value;
    var student = document.getElementById("paymentStudent").value;
    var fee = document.getElementById("paymentFee").value;
    window.location.assign(page+"?session="+session+"&term="+term+"&student_id="+student+"&fee="+fee);
}

function goto_admission_status(page){
    var application_number = document.getElementById("applyNumber").value;
    window.location.assign(page+"?appl_no="+application_number);
}

function get_value(value){
    var formatVal = $(".v-money").val()+value;
    $(".v-money").val(formatVal);
    $(".v-money").html(formatVal);
    
}

$("#selectedMethod").on('click', function(){
    $("#selectPaymentMethod").click();
    var method = $("#paymentMethod").val();
    var receiptNo = $("#receiptNo").val();
    $("#paymentReceiptNo").val(receiptNo);
    $("#other").attr("data-id", method);
});

$("#cashReceiptBtn").on('click', function(){
    $("#cashPaymentReceiptNoModal").click();
    var receiptNo = $("#cashReceiptNo").val();
    $("#paymentReceiptNo").val(receiptNo);
});

$("#familyId").on('change', function(){
    var familyId = $(this).val();
    $.ajax({
        url: '../../views/payments/ajaxHandler.php',
        method: 'GET',
        data: {getChildren : familyId},
        timeout: 10000,
        }).done(function(data){
            if(data == ""){
                $("#familyStudent").html("No students found for this family");
            }else{
                $("#familyStudent").html(data);
            }
        }).fail(function(data){
            toastr.error("The system encountered an error completing this action. Please contact support");
        });
});

function process_payment(){
    var amount = $(".v-money").val();
    var currency = $(".currency").val();
    var method = $("input[name=paymentMethod]:checked").attr("data-id");
    var student_id = $("#student_id").val();
    var student_class = $("#student_class").val();
    var student_fee = $("#student_fee").val();
    var student_term = $("#student_term").val();
    var student_session = $("#student_session").val();
    var parentEmail = $("#parentEmail").val();
    var publicKey = $("#pkey").val();
    var SchoolName = $("#schoolName").val();
    var state = $("#state").val();
    var receiptNo = $("#paymentReceiptNo").val();
    if(method == "cash" || method == "pos" || method == "offlinebank"){
        finalPayment(state, amount, currency, method, student_id, student_class, student_fee, student_term, student_session, receiptNo);
    }else{
        var paystackMethods;
        var charge =  paymentAmount(Number(amount));
        var amountToPay = Number(charge) + Number(amount);
        var reply;
        if(method == "bank"){
            paystackMethods = ["bank"];
            //var reply = payModal(methods, publicKey, parentEmail, amount, SchoolName);
            reply = payModal(state, publicKey, parentEmail, SchoolName, amountToPay, currency, method, student_id, student_class, student_fee, student_term, student_session, paystackMethods);
        }else{
            if(method == "card"){
                paystackMethods = ["card"];
                reply = payModal(state, publicKey, parentEmail, SchoolName, amountToPay, currency, method, student_id, student_class, student_fee, student_term, student_session, paystackMethods);
            }
        }
        // $("#overlay").css("display", "none");
    }
}

// INITIAL PAYMENT
$(".planPay").on('click', function(e){
    e.preventDefault();
    var planBtn = $(this);
    planBtn.val("Please wait");
    var module = $(this).prev("input").val();
    var customerEmail = $("#cusEmail").val();
    var phoneNumber = $("#phone").val();
    var schoolName = $("#schoolName").val();
    var subBtn = $(this).next("input");
    var action = "Initial Plan Payment";
    $.ajax({
        url: '../../includes/ajaxHandler.php',
        method: 'GET',
        data: {package : module, email : customerEmail},
        timeout: 10000,
    }).done(function(data){
        var resp = JSON.parse(data);
        if(resp.subAccountCode == null){
            var handler = PaystackPop.setup({
                key: resp.pkey,
                email: customerEmail,
                amount: resp.price * 100,
                metadata: {
                   custom_fields: [
                      {
                          display_name: "Mobile Number",
                          variable_name: "mobile_number",
                          value: phoneNumber
                      }
                   ]
                },
                callback: function(response){
                    if(response.reference){
                        $.ajax({
                            url: "../../includes/refChecker.php",
                            method: 'GET',
                            data: {ref : response.reference, action : action, SchoolName: schoolName},
                            timeout: 50000,
                        }).done(function(data){
                            if(data == "true"){
                                // UPDATE PLAN
                                planBtn.val("Processing...");
                                subBtn.click();
                            }else{
                                var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                toastr.error(message);
                                planBtn.val("Choose this plan");
                            }
                        })
                        .fail(function(data){
                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            planBtn.val("Choose this plan");
                        });
                        // AJAX ENDS
                    }else{
                        alert("Could not complete transaction. Please contact support");
                        planBtn.val("Choose this plan");
                    }
                },
                onClose: function(){
                    alert('window closed');
                }
            });
            handler.openIframe();
        }else{
            var handler = PaystackPop.setup({
                key: resp.pkey,
                email: customerEmail,
                amount: resp.price * 100,
                subaccount: resp.subAccountCode,
                metadata: {
                   custom_fields: [
                      {
                          display_name: "Mobile Number",
                          variable_name: "mobile_number",
                          value: phoneNumber
                      }
                   ]
                },
                callback: function(response){
                    if(response.reference){
                        $.ajax({
                            url: "../../includes/refChecker.php",
                            method: 'GET',
                            data: {ref : response.reference, action : action, SchoolName: schoolName},
                            timeout: 50000,
                        }).done(function(data){
                            if(data == "true"){
                                // UPDATE PLAN
                                planBtn.val("Processing...");
                                subBtn.click();
                            }else{
                                var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                toastr.error(message);
                                planBtn.val("Choose this plan");
                            }
                        })
                        .fail(function(data){
                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            planBtn.val("Choose this plan");
                        });
                        // AJAX ENDS
                    }else{
                        alert("Could not complete transaction. Please contact support");
                        planBtn.val("Choose this plan");
                    }
                },
                onClose: function(){
                    alert('window closed');
                }
            });
            handler.openIframe();
        }
    }).fail(function(data){
        toastr.error("An error occurred while trying to process payment "+data);
    });
});

// RENEW PLAN
$(".planRenew").on('click', function(e){
    e.preventDefault();
    var renewBtn = $(this);
    renewBtn.val("Please wait");
    var module = $(this).prev("input").val();
    var customerEmail = $("#cusEmail").val();
    var phoneNumber = $("#phone").val();
    var action = "Plan Renew";
    var schoolName = $("#schoolName").val();
    $.ajax({
        url: '../../views/settings/ajaxHandler.php',
        method: 'GET',
        data: {packageRenew : module},
        timeout: 10000,
    }).done(function(data){
        var resp = JSON.parse(data);
        if(resp.subAccountCode == null){
            var handler = PaystackPop.setup({
                key: resp.pkey,
                email: customerEmail,
                amount: resp.price * 100,
                metadata: {
                   custom_fields: [
                      {
                          display_name: "Mobile Number",
                          variable_name: "mobile_number",
                          value: phoneNumber
                      }
                   ]
                },
                callback: function(response){
                    if(response.reference){
                        $.ajax({
                            url: "../../includes/refChecker.php",
                            method: 'GET',
                            data: {ref : response.reference, action : action, SchoolName: schoolName},
                            timeout: 50000,
                        }).done(function(data){
                            if(data == "true"){
                                // UPDATE PLAN
                                renewBtn.val("Processing...");
                                planRenew(module, response.reference);
                            }else{
                                var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                toastr.error(message);
                                renewBtn.val("Choose this plan");
                            }
                        })
                        .fail(function(data){
                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            renewBtn.val("Choose this plan");
                        });
                        // AJAX ENDS
                    }else{
                        alert("Could not complete transaction. Please contact support");
                        renewBtn.val("Choose this plan");
                    }
                },
                onClose: function(){
                    alert('window closed');
                    renewBtn.val("Choose this plan");
                }
              });
              handler.openIframe();
        }else{
            // SEND TO REFEREAL
            var handler = PaystackPop.setup({
                key: resp.pkey,
                email: customerEmail,
                amount: resp.price * 100,
                subaccount: resp.subAccountCode,
                metadata: {
                   custom_fields: [
                      {
                          display_name: "Mobile Number",
                          variable_name: "mobile_number",
                          value: phoneNumber
                      }
                   ]
                },
                callback: function(response){
                    if(response.reference){
                        $.ajax({
                            url: "../../includes/refChecker.php",
                            method: 'GET',
                            data: {ref : response.reference, action : action, SchoolName: schoolName},
                            timeout: 50000,
                        }).done(function(data){
                            if(data == "true"){
                                // UPDATE PLAN
                                renewBtn.val("Processing...");
                                planRenew(module, response.reference);
                            }else{
                                var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                toastr.error(message);
                                renewBtn.val("Choose this plan");
                            }
                        })
                        .fail(function(data){
                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            renewBtn.val("Choose this plan");
                        });
                        // AJAX ENDS
                    }else{
                        alert("Could not complete transaction. Please contact support");
                        renewBtn.val("Choose this plan");
                    }
                },
                onClose: function(){
                    alert('window closed');
                    renewBtn.val("Choose this plan");
                }
              });
              handler.openIframe();
        }
    }).fail(function(data){
        toastr.error("An error occurred while trying to process payment "+data);
    });
});


function payModal(state, pkey, customerEmail, SchoolName, amount, currency, method, student_id, student_class, student_fee, student_term, student_session, paystackMethods){
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {getAcctCode : 1},
        timeout: 50000,
    }).done(function(data){
        if(data == "" || data == null){
            $("#overlay").css("display", "none");
            toastr.error("Please add account details in setting before you can process payment");
        }else{
            var action = "School Payment";
            var handler = PaystackPop.setup({
                key: pkey,
                email: customerEmail,
                amount: amount * 100,
                channels : paystackMethods,
                subaccount : data,
                bearer : "subaccount",
                metadata: {
                   custom_fields: [
                      {
                          display_name: "User Email",
                          variable_name: "User_email",
                          value: customerEmail
                      },
                      {
                        display_name: "Action",
                        variable_name: "payment_for",
                        value: action
                    }
                   ]
                },
                callback: function(response){
                    if(response.reference){
                        $.ajax({
                            url: "../../includes/refChecker.php",
                            method: 'GET',
                            data: {ref : response.reference, action : action, SchoolName: SchoolName},
                            timeout: 50000,
                        }).done(function(data){
                            if(data == "true"){
                                finalPayment(state, amount, currency, method, student_id, student_class, student_fee, student_term, student_session, response.reference);
                            }else{
                                $("#overlay").css("display", "none");
                                var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                toastr.error(message);
                            }
                        })
                        .fail(function(data){
                            $("#overlay").css("display", "none");
                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                        });
                    }else{
                        $("#overlay").css("display", "none");
                        alert("Transaction was not completed");
                    }
                    // alert('success. transaction ref is ' + response.reference);
                },
                onClose: function(){
                    $("#overlay").css("display", "none");
                    alert('window closed');
                }
            });
            handler.openIframe();
        }
    })
    .fail(function(data){
        toastr.error("An error occurred while trying to complete this request");
    });
}

$("#buySmsBtn").on('click', function(e){
    e.preventDefault();
    $(this).html("Processing...");
    var amount = $("#smsAmount").val();
    var action = "purchase sms";
    var customerEmail = $("#smsEmail").val();
    var SchoolName = $("#smsSchoolName").val();
    $.ajax({
        url: '../../includes/ajaxHandler.php',
        method: 'GET',
        data: {action : action},
        timeout: 10000,
    }).done(function(data){
        var resp = JSON.parse(data);
        var handler = PaystackPop.setup({
            key: resp.pkey,
            email: customerEmail,
            amount: amount * 100,
            callback: function(response){
                if(response.reference){
                    $.ajax({
                        url: "../../includes/refChecker.php",
                        method: 'GET',
                        data: {ref : response.reference, action : action, SchoolName: SchoolName},
                        timeout: 50000,
                    }).done(function(data){
                        if(data == "true"){
                            // PAYMENT VERIFIED....UPDATE SCHOOLS SMS STARTS HERE
                        var action = "update sms";
                            $.ajax({
                                url: "../../views/payments/ajaxHandler.php",
                                method: 'GET',
                                data: {amount : amount, action : action},
                                timeout: 50000,
                            }).done(function(data){
                                if(data == "true"){
                                    // PAYMENT VERIFIED
                                    toastr.success("Sms purchase successful. Please wait while we refresh");
                                    var delay = 4000;
                                    setTimeout(function() {
                                        window.location.reload();
                                    }, delay);
                                }else{
                                    $("#overlay").css("display", "none");
                                    var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                    toastr.error(message);
                                }
                            })
                            .fail(function(data){
                                $("#overlay").css("display", "none");
                                alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            });
                            // UPDATE SCHOOLS SMS ENDS HERE
                        }else{
                            $("#overlay").css("display", "none");
                            var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                            toastr.error(message);
                        }
                    })
                    .fail(function(data){
                        $("#overlay").css("display", "none");
                        alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                    });
                }else{
                    toastr.error("Could not complete this transaction. Please try again later");
                }
            },
            onClose: function(){
                alert('window closed');
            }
            });
            handler.openIframe();
    }).fail(function(data){
        toastr.error("An error occurred while trying to process payment "+data);
    });
});


// function getSubCode(){
    
// }

// function getSubCode(){
//     $.ajax({
//         url: "../../views/payments/ajaxHandler.php",
//         method: 'GET',
//         data: {getAcctCode : 1},
//         timeout: 10000,
//         success: function(data) {
//            return data;
//         },
//         error: function(data){
//             toastr.error("An error occurred while trying to process your request. Please try again later");
//         }
//     });
// }

function finalBulkPayment(currency, method, students, classes, feeName, amount, session, term, ref){
    var action = "Bulk Payment";
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {bulkPayment: 1, method: method, students : students, classes: classes, feeName: feeName,  amount : amount, session : session, term : term, verify : ref, currency: currency},
        timeout: 40000,
        success: function(data) {
            if(data == "true"){
                $("#overlay").css("display", "none");
                toastr.success("Payment Successful");
            }else{
                if(data == "false"){
                    $("#overlay").css("display", "none");
                    alert("An unexpected error occured while trying to process this payment. Please contact support with ref: "+ref);
                }else{
                    if(data == "wrongRef"){
                        $("#overlay").css("display", "none");
                        alert("Wrong transaction ref passed. Please make payment and try again");
                    }else{
                        $("#overlay").css("display", "none");
                        toastr.error(data);
                    }
                }
            }
        },
        error: function(data){
            toastr.error(JSON.stringify(data));
        }
    });
}


function finalPayment(state, amount, currency, method, student_id, student_class, student_fee, student_term, student_session, transRef){
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {state: state, processPayment : 1, amount : amount, currency : currency, method : method, student_id : student_id, student_class : student_class, student_fee : student_fee, student_term : student_term, student_session : student_session, transRef : transRef},
        timeout: 40000,
        success: function(data) {
            if(data == "exist"){
                $("#overlay").css("display", "none");
                toastr.error("This Payment has already been made. Please confirm details and try again");
            }else{
                if(data == "error"){
                    $("#overlay").css("display", "none");
                    toastr.error("An unexpected error occured while trying to process this payment. Please try again later");
                }else{
                    if(data == "done"){
                        $("#overlay").css("display", "none");
                        alert("Payment Successful... Click ok to continue");
                        history.go(-1);
                        
                    }else{
                        if(data == "excess"){
                            $("#overlay").css("display", "none");
                            toastr.warning("Amount entered exceed original charge. Please verify amount and try again");
                        }else{
                            if(data == "wrongRef"){
                                $("#overlay").css("display", "none");
                                alert("Wrong transaction ref passed. Please make payment and try again");
                            }
                        }
                    }
                }
            }
        },
        error: function(data){
            toastr.error(JSON.stringify(data));
        }
    });
}

function planRenew(module, ref){
    $.ajax({
        url: "../../views/settings/ajaxHandler.php",
        method: 'GET',
        data: {module : module, verify : ref, planAction: "renew"},
        timeout: 50000,
    }).done(function(data){
        if(data == "true"){
            // UPDATE PLAN
            alert("Plan activation successful. Please wait while we redirect you");
            var delay = 4000;
            setTimeout(function() {
                window.location.assign('../../views/authentication/logout.php');
            }, delay);
        }else{
            var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
            toastr.error(message);
        }
    })
    .fail(function(data){
        alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
    });
    // console.log("here")
}


$(".keypad__btn").on("click", function(){
    var value = $(this).html();
    get_value(value);
    if($(".v-money").val().length > 0){
        $(".processPayment").attr("disabled", false);
        $(".processBulkPayment").attr("disabled", false);
    }
});


$(".removeValue").on("click", function(){
    value = $(".v-money").val().substring(0, $(".v-money").val().length - 1);
    $(".v-money").val('');
    get_value(value);
    if($(".v-money").val().length < 1){
        $(".processPayment").attr("disabled", true);
        $(".v-money").html("0.00");
    }
});


$(".removeValueBulk").on("click", function(){
    value = $(".v-money").val().substring(0, $(".v-money").val().length - 1);
    $(".v-money").val('');
    get_value(value);
    if($(".v-money").val().length < 1){
        $(".processBulkPayment").attr("disabled", true);
        $(".v-money").html("0.00");
    }
});


$(".v-money").on("keyup", function(){
    $(".v-money").html($(".v-money").val());
    if($(".v-money").val().length < 1){
        $(".v-money").html("0.00");
    }
});

$(".processPayment").on("click", function(){
    $("#overlay").css("display", "block");
    $(this).html("Processing Payment");
    process_payment();
});

$(".processBulkPayment").on("click", function(){
    $("#overlay").css("display", "block");
    $(this).html("Processing Payment");
    var classes = $("#bulkClasses").val().split("*");
    var students = $("#bulkStudent").val().split("*");
    var amountArray = $("#bulkAmount").val().split("*");
    var amount = Number($("#amount").val().split("*"));
    var familyId = $("#familyId").val();
    var method = $("input[name=paymentMethod]:checked").attr("data-id");
    var SchoolName = $("#schoolName").val();
    var feeName = $("#feeName").val();
    var session = $("#session").val();
    var term = $("#term").val();
    var currency = $(".currency").val();
    var receiptNo = $("#paymentReceiptNo").val();
    var getBulkPaymentDetails = 1;
    if(method == "cash" || method == "pos" || method == "offlinebank"){
        // OFFLINE PAYMENT
        finalBulkPayment(currency, method, students, classes, feeName, amountArray, session, term, receiptNo);
    }else{
        var charge = paymentAmount(amount);
        var payAmount = amount + charge;
        // alert(payAmount)
        //alert(payAmount);
        var action =  "School Payment";
        // ONLINE PAYMENT
        $.ajax({
            url: '../../views/payments/ajaxHandler.php',
            method: 'GET',
            data: {getBulkPaymentDetails : getBulkPaymentDetails, familyId : familyId},
            timeout: 10000,
        }).done(function(data){
            var resp = JSON.parse(data);
            if(resp.subAccount == "" || resp.subAccount == null){
                toastr.error("Please update payment details in order to process online payment");
            }else{
                var handler = PaystackPop.setup({
                    key: resp.pkey,
                    email: resp.email,
                    amount: payAmount * 100,
                    subaccount: resp.subAccount,
                    callback: function(response){
                        if(response.reference){
                            $.ajax({
                                url: "../../includes/refChecker.php",
                                method: 'GET',
                                data: {ref : response.reference, action : action, SchoolName: SchoolName},
                                timeout: 50000,
                            }).done(function(data){
                                if(data == "true"){
                                    // $("#finalForm").click();
                                    finalBulkPayment(currency, method, students, classes, feeName, amountArray, session, term, response.reference);
                                }else{
                                    $("#overlay").css("display", "none");
                                    alert("An error occurred while trying to process payment. Please contact support with transaction ref "+ response.reference);
                                }
                            })
                            .fail(function(data){
                                $("#overlay").css("display", "none");
                                alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            });
                        }else{
                            $("#overlay").css("display", "none");
                            alert("Transaction was not completed");
                        }
                    },
                    onClose: function(){
                        alert('window closed');
                    }
                });
                handler.openIframe();
            }
            
        }).fail(function(data){
            toastr.error("The system encountered an error completing this action. Please contact support");
        });
    }
    // process_bulk_payment();
});

$(".currency").on("change", function(){
    var currency = $(this).val();
    switch (currency) {
    case "Naira":
        currencyCode = "&#8358;";
        $(".currencyCode").html(currencyCode);
        break;
    case "Dollar":
        currencyCode = "&#x24;";
        $(".currencyCode").html(currencyCode);
        break;
    case "Cedi":
        currencyCode = "&#8373;";
        $(".currencyCode").html(currencyCode);
        break;
    case "Shilling":
        currencyCode = "K";
        $(".currencyCode").html(currencyCode);
        break;
    case "Rand":
        currencyCode = "R";
        $(".currencyCode").html(currencyCode);
        break;
    case "Euro":
        currencyCode = "&#128;";
        $(".currencyCode").html(currencyCode);
        break;
    case "Pounds":
        currencyCode = "&#163;";
        $(".currencyCode").html(currencyCode);
        break;
    }
});

function speak(text) {
    const awaitVoices = new Promise(resolve=> 
        window.speechSynthesis.onvoiceschanged = resolve)  
    .then(()=> {
        const synth = window.speechSynthesis;
        var voices = synth.getVoices();
        console.log(voices);
        const utterance = new SpeechSynthesisUtterance();
        utterance.voice = voices[4];        
        utterance.text = text;
        utterance.volume = 1;
        utterance.rate = 1;
        utterance.pitch = 1;
        synth.speak(utterance);
    });
}


$("#smsAmount").on('keyup', function(){
    var costPerSms = 3;
    var smsAmount = $("#smsAmount").val();
    var smsUnits;
    if(smsAmount > costPerSms-1){
        smsUnits = Math.trunc(smsAmount/costPerSms);
    }else{
        smsUnits = 0;
    }
    $("#equivSMS").html(smsUnits);
});

$(".btnSubmitClose").on('click', function(){
    toastr.error("Admission not in progress. Please check back later");
});

$(".applicationPayment").on('click', function(){
    var getAdmissionFeeState = 1;
    var customerEmail = $("#parentEmail").val();
    $.ajax({
        url: '../../views/admission/ajaxHandler.php',
        method: 'GET',
        data: {getAdmissionFeeState : getAdmissionFeeState},
        timeout: 10000,
    }).done(function(data){
        var resp = JSON.parse(data);
        if(resp.feeState == 1){
            // PAY TO SUBMIT FORM
            if(resp.subAccount == null || resp.subAccount == ""){
                toastr.error("Please contact school to update payment details");
            }else{
                var action = "Application Fee";
                var SchoolName = $("#schoolName").val();
                var handler = PaystackPop.setup({
                    key: resp.pkey,
                    email: customerEmail,
                    amount: resp.amount * 100,
                    subaccount: resp.subAccount,
                    callback: function(response){
                        if(response.reference){
                            $.ajax({
                                url: "../../includes/refChecker.php",
                                method: 'GET',
                                data: {ref : response.reference, action : action, SchoolName: SchoolName},
                                timeout: 50000,
                            }).done(function(data){
                                if(data == "true"){
                                    $("#finalForm").click();
                                }else{
                                    $("#overlay").css("display", "none");
                                    var message ="An unexpected error occurred while trying to process payment. Error Message" + data;
                                    toastr.error(message);
                                }
                            })
                            .fail(function(data){
                                $("#overlay").css("display", "none");
                                alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                            });
                        }else{
                            $("#overlay").css("display", "none");
                            alert("Transaction was not completed");
                        }
                    },
                    onClose: function(){
                        alert('window closed');
                    }
                });
                    handler.openIframe();

            }
        }else{
            $("#finalForm").click();
        }
    }).fail(function(data){
        toastr.error("An error occurred while trying to process payment "+data);
    });
});

$(".admitApplicant").on('click', function(){
    var btn = $(this);
    var value = btn.attr('data-id');
    toastr.warning("Processing...");
    $.ajax({
        url: '../../views/admission/ajaxHandler.php',
        method: 'GET',
        data: {admit : value},
        timeout: 10000,
    }).done(function(data){
        if(data == "true"){
            btn.html("Admitted");
            toastr.success("Applicant admitted successfully");
        }else{
            toastr.error("An error occurred while trying to admit applicant. Please contact support");
        }
    }).fail(function(data){
        toastr.error("The system encountered an error completing this action. Please contact support");
    });
});

$("#itemName").on('change', function(){
    var getItemProp = $(this).val();
    $.ajax({
        url: '../../views/inventory/ajaxHandler.php',
        method: 'GET',
        data: {getItemProp : getItemProp},
        timeout: 10000,
    }).done(function(data){
        $("#itemSize").html(data);
            $.ajax({
                url: '../../views/inventory/ajaxHandler.php',
                method: 'GET',
                data: {getItemColor : getItemProp},
                timeout: 10000,
            }).done(function(data){
                $("#itemColor").html(data);
            }).fail(function(data){
                toastr.error("The system encountered an error completing this action. Please contact support");
            });
    }).fail(function(data){
        toastr.error("The system encountered an error completing this action. Please contact support");
    });
});

$("#checkBalance").on('click', function(){
    var btn =  $(this);
    btn.html("Processing...");
    var owner = $("#walletOwner").val();
    $("#walletResult").html("");
    if(owner == ""){
        $("#walletResult").html('<div class="form-group"> <label for=""> Student Number cannot be empty</label></div>');
    }else{
        $.ajax({
            url: '../../views/tuckshop/ajaxHandler.php',
            method: 'GET',
            data: {getBalance : owner},
            timeout: 10000,
        }).done(function(data){
            if(data == "false"){
                btn.html("Check wallet balance");
                $("#walletResult").html('<div class="form-group"> <label for=""> Student was not found in record. Please confirm the number</label></div>');
            }else{
                btn.html("Check wallet balance");
                $("#walletResult").html('<div style="margin-top:20px;" class="form-group">'+
                                            '<label for=""> Wallet Balance is:</label>'+
                                            '<h4 class="onboarding-title" style="font-size:2rem; color:#8095A0"> &#x20A6;<span>'+data+'</span></h4>'+              
                                           '</div>');
            }
        }).fail(function(data){
            toastr.error("The system encountered an error completing this action. Please contact support");
        });
    }
});

$("#rechargeStudentNumber").on('focus', function(){
    $("#rechargeCheck").prop( "checked", false );
    $("#rechargeBtn").attr("disabled", "true");
    $("#studentName").html("");
});

$("#rechargeCheck").on('change', function(){
    var rechargeStudentNumber = $("#rechargeStudentNumber").val();
    if($(this).is(':checked')){
        if(rechargeStudentNumber == ""){
            toastr.error("Please enter student number");
        }else{
            $.ajax({
            url: '../../views/tuckshop/ajaxHandler.php',
            method: 'GET',
            data: {verifyStudent : rechargeStudentNumber},
            timeout: 10000,
            }).done(function(data){
                var resp = JSON.parse(data);
                if(resp.status == 1){
                    $("#studentName").html('<div style="margin-top:20px;" class="form-group"><label for=""> '+resp.fullname+'</label></div>');
                    $("#rechargeBtn").removeAttr("disabled");
                }else{
                    toastr.error("Could not verify student number. Please try again");
                }
            }).fail(function(data){
                toastr.error("The system encountered an error completing this action. Please contact support");
            });
        }
    }else{
        $("#rechargeBtn").attr("disabled", "true");
    }
})

$("#rechargeBtn").on('click', function(){
    var amount = Number($("#rechargeAmount").val());
    var rechargeStudentNumber = $("#rechargeStudentNumber").val();
    var rechargeAmount = amount + paymentAmount(amount);
    var SchoolName;
    if(rechargeStudentNumber == "" || rechargeAmount == ""){
        toastr.error("All fields are required");
    }else{
        var getTuckshopDetails = 1;
        $.ajax({
            url: '../../views/tuckshop/ajaxHandler.php',
            method: 'GET',
            data: {getTuckshopDetails : getTuckshopDetails},
            timeout: 10000,
            }).done(function(data){
                var resp = JSON.parse(data);
                if(resp.subAccountCode == "" || resp.subAccountCode == null){
                    toastr.error("Please update tuckshop account details in configuration panel");
                }else{
                    SchoolName = resp.school;
                    var action = "Wallet recharge";
                    var handler = PaystackPop.setup({
                        key: resp.pkey,
                        email: resp.email,
                        amount: rechargeAmount * 100,
                        subaccount : resp.subAccountCode,
                        bearer : "subaccount",
                        metadata: {
                        custom_fields: [
                            {
                                display_name: "User Email",
                                variable_name: "User_email",
                                value: resp.email
                            },
                            {
                                display_name: "Action",
                                variable_name: "payment_for",
                                value: action
                            }
                        ]
                        },
                        callback: function(response){
                            if(response.reference){
                                $.ajax({
                                    url: "../../includes/refChecker.php",
                                    method: 'GET',
                                    data: {ref : response.reference, action : action, SchoolName: SchoolName},
                                    timeout: 50000,
                                }).done(function(data){
                                    // UPDATE STUDENT AMOUNT
                                    if(data == "true"){
                                        var action = "updateWallet";
                                        $.ajax({
                                            url: "../../views/tuckshop/ajaxHandler.php",
                                            method: 'GET',
                                            data: { verifier: response.reference, action: action, owner: rechargeStudentNumber, amount: rechargeAmount},
                                            timeout: 50000,
                                        }).done(function(data){
                                            // UPDATE STUDENT AMOUNT
                                            if(data == "true"){
                                                toastr.success("Student wallet has been recharged");
                                            }else{
                                                if(data == "wrongRef"){
                                                    alert("Wrong transaction ref passed. Please make payment and try again");
                                                }else{
                                                    alert("An error occurred while trying to recharge student wallet. Please contact support with transaction ref: "+response.reference);
                                                }
                                            }
                                            
                                        })
                                        .fail(function(data){
                                            $("#overlay").css("display", "none");
                                            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                                        });   
                                    }else{
                                        // WRONG TRANSACTION
                                    }
                                    
                                })
                                .fail(function(data){
                                    $("#overlay").css("display", "none");
                                    alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                                });
                            }else{
                                $("#overlay").css("display", "none");
                                alert("Transaction was not completed");
                            }
                            // alert('success. transaction ref is ' + response.reference);
                        },
                        onClose: function(){
                            $("#overlay").css("display", "none");
                            alert('window closed');
                        }
                    });
                    handler.openIframe();
                }
            }).fail(function(data){
                toastr.error("The system encountered an error completing this action. Please contact support");
            });
    }
})

$("#examClass").on('change', function(){
    var className = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: 'GET',
        data: { className: className, getSubject: 1},
        timeout: 50000,
    }).done(function(data){
        $("#examSubject").html(data);
    })
    .fail(function(data){
        alert("An error occurred. Please contact support with error message: "+data);
    }); 
});

$(document).on('change', '#examState', function(){
    var subject = $(this).closest("td").attr("data-id");
    var value = 0;
    if($(this).is(':checked')){
        value = 1;
    }else{
        value = 0;
    }
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: 'GET',
        data: { value: value, subject: subject, action: "updateExamState"},
        timeout: 50000,
    }).done(function(data){
        if(data == "true"){
            toastr.success("Exam state has been updated successfully");
        }else{
            toastr.error("An unexpected error occurred while trying to update exam state, Please contact support");
        }
    })
    .fail(function(data){
        $("#overlay").css("display", "none");
        alert("An unexpected error occurred with error message " +data);
    });
})

$("#examinationClass").on('change', function(){
    var examClass = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: "GET",
        data: {examClass: examClass},
        cache: false,
        success: function(results){
            $("#examinationSubject").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$("#examinationQuestionClass").on('change', function(){
    var examClass = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: "GET",
        data: {examClass: examClass},
        cache: false,
        success: function(results){
            $("#examinationQuestionSubject").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$("#examinationClassModal").on('change', function(){
    var examClass = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: "GET",
        data: {examClass: examClass},
        cache: false,
        success: function(results){
            $("#examinationSubjectModal").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$("#questionClass").on('change', function(){
    var examClass = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: "GET",
        data: {examClass: examClass},
        cache: false,
        success: function(results){
            $("#questionSubject").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
})

$("#questionClassSelector").on('change', function(){
    var className = $(this).val();
    $.ajax({
        url: "../../views/examination/ajaxHandler.php",
        method: "GET",
        data: {examClass: className},
        cache: false,
        success: function(results){
            $("#questionSubjectSelector").html(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$("#questionSubjectSelector").on('change', function(){
    $("#questionSubmitBtn").click();
});

$(document).on('change', '#studentState', function(){
    var currentTd = $(this).closest('td');
    var studentId = $(this).closest('tr').attr('data-id');
    if($(this).is(':checked')){
        var reply = confirm("Do you want to move this student to alumni forum ?");
        if(reply === true){
            $.ajax({
                url: "../../views/students/ajaxHandler.php",
                method: "GET",
                data: {moveStudentId: studentId},
                cache: false,
                success: function(results){
                    if(results == "true"){
                        toastr.success("Student has been moved to alumni forum successfully");
                        currentTd.html("Alumni");
                    }else{
                        toastr.error("An error occurred while trying to perform action. Please contact support");
                    }
                },
                error : function (e) {
                    toastr.error('Sorry an error occured!');  
                }
            });
        }else{
            $(this).prop('checked', false); // Unchecks it
        }
    }
});

$("#proceedExam").on('click', function(){
    var btn = $(this);
    var number = $("#examNumber").val();
    btn.html("<span> Verifying... </span>");
    if(number == ""){
        btn.html("<span> Proceed To Examination </span>");
    }else{
        $.ajax({
            url: "../../views/examination/ajaxHandler.php",
            method: "GET",
            data: {number: number, startExam : 1},
            cache: false,
            success: function(results){
                if(results == "true"){
                    window.location.assign("../../views/examination/exam");
                }else{
                    if(results == "false"){
                        btn.html("<span> Proceed To Examination </span>");
                        toastr.error("No ongoing exam for your class");
                    }else{
                        if(results == "written"){
                            btn.html("<span> Proceed To Examination </span>");
                            toastr.error("Exams has already been written");
                        }
                    }
                }
            },
            error : function (e) {
                toastr.error('Sorry an error occured!');  
            }
        });
    }
});

$("#notifyOutstandingFee").on('click', function(){
    var btn = $(this);
    var stdnNumber = btn.attr("data-id");
    btn.html("Please Wait...");
    toastr.warning("Notify parent of outstanding fee");
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: "GET",
        data: {stdnNumber: stdnNumber, action : "notifyOutstandingFeeParent"},
        cache: false,
        success: function(results){
            if(results == "true"){
                toastr.success("Notification sent successfully");
            }else{
                if(results == "noNumbers"){
                    toastr.error("Error: Parents contact not found");
                }else{
                    toastr.error("An unexpected error occurred while trying to perform thus action. Please contact support");
                }
            }
            btn.html("Notify Parents");
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$(".publishModal").on('submit', function(e){
    e.preventDefault();
    toastr.warning('Processing...');
    var publishClass = $("#publishClasses").val();
    var pinVending = $("#pinVending").val();
    var pinAmount = $("#pinAmount").val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {classes: publishClass, vending: pinVending,  pinAmount: pinAmount, action : "publishResult"},
        cache: false,
        success: function(results){
            alert(results);
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
    // toastr.warning('Publishing result...');
    // $("#publishResultModal").toggle();
});

$(document).on("click", "#rGrade", function(){
    toastr.warning('Processing');
    var row = $(this).closest("tr");
    var gradeId = row.attr("data-id");
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {gradeId: gradeId, action : "deleteGrade"},
        cache: false,
        success: function(results){
            if(results == "true"){
                row.toggle();
                toastr.success('Grade successfully deleted');
            }else{
                toastr.error('Sorry an error occured!');
            }
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});


$('.timeselect').timeAutocomplete();
// $("html").on("contextmenu",function(){return false;});
//$("#pickupCardNo, #attendanceCode").on("keydown keypress keyup", false);


var myVar;
$(document).ready(function(){
    myVar = setTimeout(showPage, 3000);
});

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$("#resultClass").on('change', function(){
    var className = $(this).val();
    $.ajax({
        url: "../../views/assessment/ajaxHandler.php",
        method: "GET",
        data: {allow: 1, className : className, action : "getClassStudent"},
        cache: false,
        success: function(results){
            $("#resultStudentNumber").html(results);
            //$("#showResult").attr("disabled", "false");
        },
        error : function (e) {
            toastr.error('Sorry an error occured!');  
        }
    });
});

$("#showResult").on('click', function(e){
    e.preventDefault();
    var userPin =  $("#verifypin").val();
    var studentNumber = $("#resultStudentNumber").val();
    var resultTerm = $("#resultTerm").val();
    var resultSession = $("#resultSession").val();
    var proceed = false;
    if(userPin == undefined){
        userPin = "";
        proceed = true;
    }else{
        if(userPin == ""){
            prceed = false;
            toastr.error("Please enter result pin to view result");
        }else{
            // PIN ENTERED
            proceed = true;
        }
    }
    if(proceed){

        $.ajax({
            url: "../../views/assessment/ajaxHandler.php",
            method: "GET",
            data: {allow: 1, userPin: userPin, studentNumber: studentNumber, resultSession: resultSession, resultTerm: resultTerm, action : "checkResult"},
            cache: false,
            success: function(results){
                var resp = JSON.parse(results);
                if(resp.message == "proceed"){
                    window.location.assign("../../views/assessment/"+resp.link);
                }else{
                    toastr.error(resp.message);
                }
            },
            error : function (e) {
                toastr.error('Sorry an error occured!');  
            }
        });

    }
});

$(".outStudentPayment").on("change", function(){
    var paymentSession = $("#parentPayingSession").val();
    var paymentTerm = $("#parentPayingTerm").val();
    var paymentClass = $("#resultClass").val();
    var studentId = $(this).val();
    $.ajax({
        url: "../../views/payments/ajaxHandler.php",
        method: 'GET',
        data: {allow: 1, action: "getStudentPayment", payingSession : paymentSession, payingTerm : paymentTerm, paymentClassStudent : paymentClass, studentNumber : studentId},
        timeout: 10000,
        success: function(data) {
            $("#parentFeePayment").html(data);
        },
        error: function(data){
            toastr.error(JSON.stringify(data));
        }
    });
});

// $("#resultStudentNumber, #paymentStudentNumber").on("keyup", function(){
//     var studentNumber = $(this).val();
//     if(studentNumber == ""){
//         $("#showResult").removeAttr("disabled", "false");
//         $("#studentName").val('Student Name');
//     }else{
//         $("#showResult").attr("disabled", true);
//         $("#studentName").val("Verifying Number");
//         $.ajax({
//             url: "../../views/assessment/ajaxHandler.php",
//             method: 'GET',
//             data: {allow: 1, studentNumber : studentNumber, action: "getName"},
//             timeout: 10000,
//             success: function(data) {
//                 if(data != "false"){
//                     $("#showResult").removeAttr("disabled", true);
//                     $("#studentName").val(data);
//                 }else{
//                     $("#showResult").attr("disabled", "false");
//                     $("#studentName").val('Student number not valid');
//                 }
//             },
//             error: function(data){
//                 toastr.error("Taking too long to verify student number. Please refresh");
//             }
//         });
//     }
// });

$("#payPin").on('click', function(){
    var thisBtn = $(this);
    var getPaymentParam = 1;
    var customerEmail = $("#pinEmail").val();
    thisBtn.html("Processing...");
    if(customerEmail == ""){
        toastr.error("Please enter your email");
    }else{
        $.ajax({
            url: "../../views/assessment/ajaxHandler.php",
            method: 'GET',
            data: {allow: 1, getPaymentParam : getPaymentParam, action : "getSubaccountCode"},
            timeout: 50000,
        }).done(function(data){
            var resp = JSON.parse(data);
            if(resp.accountCode == null || resp.accountCode == ""){
                toastr.error("Couldnt get payment parameters, Please contact school");
                thisBtn.html("Proceed");
            }else{
                // GET PAYMENT DETAILS ABOVE
                var charge =  paymentAmount(Number(resp.pinAmount));
                var amountToPay = Number(charge) + Number(resp.pinAmount);
                var action = "Pin Purchase";
                var schoolName = resp.schoolName;
                var handler = PaystackPop.setup({
                    key: resp.pkey,
                    email: customerEmail,
                    amount: amountToPay * 100,
                    subaccount : resp.accountCode,
                    bearer : "subaccount",
                    metadata: {
                        custom_fields: [
                            {
                                display_name: "User Email",
                                variable_name: "User_email",
                                value: customerEmail
                            },
                            {
                            display_name: "Action",
                            variable_name: "payment_for",
                            value: action
                        }
                        ]
                    },
                    callback: function(response){
                        if(response.reference){
                            $.ajax({
                                url: "../../includes/refChecker.php",
                                method: 'GET',
                                data: {ref : response.reference, action : action, SchoolName: schoolName},
                                timeout: 50000,
                            }).done(function(data){
                                if(data == "true"){
                                    // GENERATE PIN
                                    $.ajax({
                                        url: "../../views/assessment/ajaxHandler.php",
                                        method: 'GET',
                                        data: {allow: 1, action: "generatePin"},
                                        timeout: 60000,
                                    }).done(function(data){
                                        $("#pinDisplay").html("YOUR PIN IS "+data);
                                        thisBtn.html("Proceed");
                                    })
                                    .fail(function(data){
                                        alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                                        thisBtn.html("Proceed");
                                    });
                                }else{
                                    alert("An unexpected error occurred while trying to process payment. Please contact support with ref Number "+response.reference);
                                    thisBtn.html("Proceed");
                                }
                            })
                            .fail(function(data){
                                alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
                                thisBtn.html("Proceed");
                            });
                        }else{
                            alert("Transaction was not completed");
                            thisBtn.html("Proceed");
                        }
                        // alert('success. transaction ref is ' + response.reference);
                    },
                    onClose: function(){
                        alert('window closed');
                        thisBtn.html("Proceed");
                    }
                });
                handler.openIframe();
            }//second else
        })//first done
        .fail(function(data){
            $("#overlay").css("display", "none");
            alert("Could not verify your transaction ref. Please contact support with ref Number: "+ response.reference);
        });

    } //first else
});

$("#purchasePin").on('click', function(){
    $("#checkResult").css("display", "none");
    $("#pinDiv").css("display", "block");
});

$("#havePin").on('click', function(){
    $("#checkResult").css("display", "block");
    $("#pinDiv").css("display", "none");
});

$("#makePayment").on('click', function(){
    
});

// TUCKSHOP PROCESS PAYMENT

var purchaseItemArray = Array();

$("input[name=addPurchaseItem]").on('click', function(e){
    e.preventDefault();
    
    var quantity = $("input[name=purchaseItemQuantity]").val();
    $.each($(".purchaseItemName option:selected"), function(index){
        var newObject = {productId: $(this).val(), quantity: quantity, productName: $(this).attr("data-item-name"), price: $(this).attr("data-price")};
        purchaseItemArray.push(newObject);
    });
    $(".itemTable").find("tr:gt(0)").remove();
    $.each(purchaseItemArray, function(index, item){
        var newTr = "<tr data-product-id='"+item.productId+"' class='my_hover_up'>"+
                        "<td class='nowrap'>"+
                            "<span> "+ (index + 1) +".</span>"+
                        "</td>"+
                        "<td>"+
                            "<span>"+item.productName+"</span>"+
                        "</td>"+
                        "<td contenteditable class='quantity'>"+
                            "<span>"+item.quantity+"</span>"+
                        "</td>"+
                        "<td>"+
                            "<span>&#8358;"+(item.price * item.quantity) +"</span>"+
                        "</td>"+
                    "</tr>";
        $(".itemTable tbody").append(newTr);
    });
    updateTotal();
    $(".purchaseItemName").val([]).trigger('change');
    $("input[name=purchaseItemQuantity]").val("");
});

$(document).on('blur', '.quantity', function(){
    var newQuantity = Number($(this).html());
    var editedProducId = $(this).closest('tr').attr("data-product-id");
    var editedProductIndex = purchaseItemArray.findIndex(product => product.productId === editedProducId);
    purchaseItemArray[editedProductIndex].quantity = newQuantity;
    // var test = $(this).next('td').html();
    $(this).next('td').html("<span>&#8358;"+(purchaseItemArray[editedProductIndex].price * newQuantity) +"</span>");
    updateTotal();
    //$(this).closest('td .totalPrice').html("sdsd");
});


function updateTotal(){
    var total = 0;
    $.each(purchaseItemArray, function(index, item){
        total += (item.price * item.quantity)
    });
    $(".total").html("<span>&#8358;"+ total +"</span>");

    var balance = $(".balanceAmount").attr("data-balance");

    if(balance != 0){
        if(balance > total){
            var balanceLeft = balance - total;
            $(".balanceAmount").html("<span>&#8358;"+ balanceLeft +"</span>");
            $(".balanceAmount").attr("<span>&#8358;"+ balanceLeft +"</span>")
        }
    }
}


