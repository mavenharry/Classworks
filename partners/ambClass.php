<?php
    include_once ("logic/config.php");

    class ambClass extends logic {
        
        function newNumber(){
            $stmt = $this->dconn->prepare("SELECT id FROM partners ORDER BY id DESC LIMIT 1");
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            $lastNum = array_shift($stmt_result)["id"];
            $lastNum += 1;
            if($lastNum <= 9){
                $lastNum = "00".$lastNum;
            }
            elseif($lastNum > 9 && $lastNum < 100){
                $lastNum = "0".$lastNum;
            }
            $newNum = "Classworks/Ptnr/".$lastNum;
            $stmt->close();
            return $newNum;
        }

        function newAmb($number, $name, $nationality, $state, $city, $gender, $address, $email, $phone){
            $dates = date("Y-m-d");
            $acctState = 0;
            $stmt = $this->dconn->prepare("SELECT id FROM partners WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt_result = $this->get_result($stmt);
            if(count($stmt_result) > 0){
                $stmt->close();
                return "exist";
            }else{
                $stmt = $this->dconn->prepare("INSERT INTO partners (fullname, partner_no, email, nationality, sor, city, gender, address, number, date_created, account_state) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->bind_param("ssssssssssi", $name, $number, $email, $nationality, $state, $city, $gender, $address, $phone, $dates, $acctState);
                if($stmt->execute()){
                    $stmt->close();
                    return true;
                }else{
                    $stmt->close();
                    return false;
                }
            }
        }
    }
?>