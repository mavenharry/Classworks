<?php
    include_once ("ambClass.php");
    $amb = new ambClass();

if(isset($_POST["partnerBtn"])){
    $number = htmlentities(trim($_POST["partnerNumber"]));
    $name = htmlentities(trim($_POST["partnerName"]));
    $nationality = htmlentities(trim($_POST["partnerCountry"]));
    $state = htmlentities(trim($_POST["partnerState"]));
    $city = htmlentities(trim($_POST["partnerCity"]));
    $gender = htmlentities(trim($_POST["partnerGender"]));
    $address = htmlentities(trim($_POST["partnerAddress"]));
    $email = htmlentities(trim($_POST["partnerEmail"]));
    $phone = htmlentities(trim($_POST["partnerPhone"]));

    $reply = $amb->newAmb($number, $name, $nationality, $state, $city, $gender, $address, $email, $phone);
    if($reply === true){
            // multiple recipients
        $to = 'marymatthew2000@gmail.com, ';
        $to .= 'estherchinwendu055@gmail.com';

        // subject
        $subject = 'New ClassWorks Partner Program Application.';
        $subject_client = 'Partnership Request Received Successfully.';

        // message
        $message = '
        <html>
        <head>
        <title>'.$subject.'</title>
        </head>
        <body>
        <p>Hello i hereby apply to be a partner with ClassWorks.xyz!</p>
        <p>Below are my details!</p>
        <h4>Number: '.$number.'</h4>
        <h4>Name: '.ucwords($name).'</h4>
        <h4>Nationality: '.ucwords($nationality).'</h4>
        <h4>State: '.ucwords($state).'</h4>
        <h4>City: '.ucwords($city).'</h4>
        <h4>Gender: '.ucwords($gender).'</h4>
        <h4>Address: '.$address.'</h4>
        <h4>Email: '.ucwords($email).'</h4>
        <h4>Telephone: '.$phone.'</h4>
        </body>
        </html>
        ';


        $message_client = "
        <html>
        <head>
        <title></title>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
        <style type='text/css'>
            /* FONTS */
            @media screen {
                @font-face {
                font-family: Lato;
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
                }
                
                @font-face {
                font-family: Lato;
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
                }
                
                @font-face {
                font-family: Lato;
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
                }
                
                @font-face {
                font-family: Lato;
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
                }
            }
            
            /* CLIENT-SPECIFIC STYLES */
            body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
            table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
            img { -ms-interpolation-mode: bicubic; }

            /* RESET STYLES */
            img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
            table { border-collapse: collapse !important; }
            body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

            /* iOS BLUE LINKS */
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }

            /* ANDROID CENTER FIX */
            div[style*='margin: 16px 0;'] { margin: 0 !important; }
        </style>
        </head>
        <body style='background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;'>

        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
            <!-- LOGO -->
            <tr>
                <td bgcolor='#08ACF0' align='center'>
                    <table border='0' cellpadding='0' cellspacing='0' width='480' >
                        <tr>
                            <td align='center' valign='top' style='padding: 40px 10px 40px 10px;'>
                                <a href='http://litmus.com' target='_blank'>
                                    <img alt='Logo' src='http://classworks.xyz/home/assets/images/logo.png' width='150' height='150' style='display: block;  font-family: Lato, Helvetica, Arial, sans-serif; color: #ffffff; font-size: 18px;' border='0'>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- HERO -->
            <tr>
                <td bgcolor='#08ACF0' align='center' style='padding: 0px 10px 0px 10px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='480' >
                        <tr>
                            <td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #8095A0; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;'>
                            <h1 style='color:#08ACF0; font-size: 2rem; font-weight: 600; margin: 0;'>Partnership</h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- COPY BLOCK -->
            <tr>
                <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='480' >
                    <!-- COPY -->
                    <tr>
                        <td bgcolor='#ffffff' align='center' style='padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;' >
                        <p style='color: #8095A0; margin: 0; line-height:30px; text-align:justify'>Thank you ".ucwords($name).", We have recieved your partnership request, 
                        Our partnership team will contact you within the next working hour(s). meanwhile,
                        Our partnership program enables you work with us wherever you are in the world, 
                        it's seamless and stress free. Help automate, innovate your country's education and get paid 40% per 
                        signed up school's payment monthly. If Interested we will have you up and running in no time.</p>
                        
                        <h4 style='font-size:1.3rem; color: #8095A0;'>Do you need more information?</h4>
                        </td>
                    </tr>
                    <!-- BULLETPROOF BUTTON -->
                    <tr>
                        <td bgcolor='#ffffff' align='center'>
                        <table border='0' style='margin-bottom:40px; margin-top: -30px;' cellspacing='0' cellpadding='0'>
                            <tr>
                            <td bgcolor='#ffffff' align='center' style='padding: -30px 30px 60px 30px;'>
                                <table border='0' cellspacing='0' cellpadding='0'>
                                <tr>
                                    <td align='center' style='border-radius: 3px;' bgcolor='#08ACF0'><a href='mailto:contact@classworks.xyz' style='font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #08ACF0; display: inline-block;'>Send us a mail</a></td>
                                </tr>
                                </table>
                            </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
            <!-- SUPPORT CALLOUT -->
            <tr>
                <td bgcolor='#f4f4f4' align='center' style='padding: 30px 10px 0px 10px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='480' >
                        <!-- HEADLINE -->
                        <tr>
                        <td bgcolor='#08ACF0' align='center' style='padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;' >
                            <a style='text-decoration-color: #FFF' href='http://classworks.xyz'><h2 style='font-size: 2rem; font-weight: 800; color: #FFFFFF; margin: 0;'>ClassWorks.xyz</h2></a>
                            <p style='margin-top: 10; color: #FFFFFF; font-size:1rem; font-style:italic; font-weight:300'>Schooling at its best</p>
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- FOOTER -->
            <tr>
                <td bgcolor='#f4f4f4' align='center' style='padding: 20px 10px 0px 10px;'>
                    <table border='0' cellpadding='0' cellspacing='0' width='480' >
                    
                    <!-- PERMISSION REMINDER -->
                    <tr>
                        <td bgcolor='#f4f4f4' align='left' style='padding: 0px 30px 30px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;' >
                        <p style='margin: 0;'>You received this email because you requested and filled a partnership form. If you did not, <a href='http://classworks.xyz' target='_blank' style='color: #8095A0; font-weight: 700;'>Please contact us.</a>.</p>
                        </td>
                    </tr>
                    
                    <!-- ADDRESS -->
                    <tr>
                        <td bgcolor='#f4f4f4' align='left' style='padding: 0px 30px 30px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;' >
                        <p style='margin: 0;'>CW10, Ada Odum Complex, NTA/Choba Road, Rumuokwuta, Port Harcourt, Rivers State, Nigeria.</p>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>

        </body>
        </html>

        ";

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ClassWorks.xyz <contact@classworks.xyz>' . "\r\n";
        
        // Mail it
        mail($to, $subject, $message, $headers);
        if(mail($email, $subject_client, $message_client, $headers)){
            $mess = "Thank you!. Your application has been sent successfully, Someone will contact you within the next working hour(s).";
        }else{
            $err = "An error occurred while trying to send you a mail. Please try again later";
        }
    }else{
        if($reply == "exist"){
            $err = "A user already exist with this email.";
        }else{
            $err = "An error occurred while trying to process your request. Please try again later";
        }
    }

}
?>