<?php
    // $ini_array = parse_ini_file("../../includes/.env");
    // include_once ("../../logic/db.php");
    include_once ("db.php");

    class logic {
        
        // private $defaultHost = host;
        // private $defaultUser = user;
        // private $defaultDatabase = database;
        // private $defaultPassword = password;
        private $defaultHost = "localhost";
        private $defaultUser = "root";
        private $defaultDatabase = "classworks";
        private $defaultPassword = "";
        private $ini_array;
        public $uconn;
        public $dconn;
        private $error;
        

        public function __construct(){
            //$this->ini_array = parse_ini_file("../../includes/.env");
            $this->defaultConnection();
           // $this->userConnection($this->ini_array);
        }

        private function defaultConnection(){
            $this->dconn = new mysqli($this->defaultHost, $this->defaultUser, $this->defaultPassword, $this->defaultDatabase);
            if(!$this->dconn){
                $this->error = "Fatal Error: Can't connect to database ".$this->dconn->connect_error;
                return false;
            }
        }

        // private function userConnection($ini_array){
        //      if(empty($ini_array["user"])){
        //         //  DO NOTHING
        //      }else{
        //         $this->uconn = new mysqli($ini_array["host"], $ini_array["user"], $ini_array["password"], $ini_array["database"]);
        //         if(!$this->uconn){
        //             $this->error = "Fatal Error: Can't connect to database ".$this->uconn->connect_error;
        //             return false;
        //         }
        //      }
        // }

        function get_result( $Statement ) {
            $RESULT = array();
            $Statement->store_result();
            for ( $i = 0; $i < $Statement->num_rows; $i++ ) {
                $Metadata = $Statement->result_metadata();
                $PARAMS = array();
                while ( $Field = $Metadata->fetch_field() ) {
                    $PARAMS[] = &$RESULT[ $i ][ $Field->name ];
                }
                call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
                $Statement->fetch();
            }
            $object = (object) $RESULT;
            return $RESULT;
        }
        
    }

    // $logic = new logic($ini_array);

?>
